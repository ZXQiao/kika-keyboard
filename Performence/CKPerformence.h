//
//  CKPerformence.h
//  Emoji Keyboard
//
//  Created by 新美 on 15/8/19.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   (-[startTime timeIntervalSinceNow]) * 1000

@interface CKPerformence : NSObject
+(void)StartClock;
+(void)StopClock:(NSString *)key;
+(void)GetExcuteTime:(NSString *)key duration:(double)duration;
@end
