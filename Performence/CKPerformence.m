//
//  CKPerformence.m
//  Emoji Keyboard
//
//  Created by 新美 on 15/8/19.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKPerformence.h"

//#define TICK   NSDate *startTime = [NSDate date]
//#define TOCK   (-[startTime timeIntervalSinceNow])

@implementation CKPerformence

NSDate *startTime;
NSMutableDictionary * performenceDict;

+(void)StartClock
{
    startTime = [NSDate date];
}

+(void)StopClock:(NSString *)key
{
    double duration = (-[startTime timeIntervalSinceNow]) * 1000;
    CKLog(@"key == %@, duration == %f", key, duration);
    NSNumber *durationNumber = [NSNumber numberWithDouble:duration];
    [performenceDict setObject:durationNumber forKey:key];
}

+(void)GetExcuteTime:(NSString *)key duration:(double)duration
{
    CKLog(@"key == %@, duration == %f", key, duration);

    if ([key isEqualToString:@"engineMgrPredict"]) {
        [self UpdatePerformanceDict:duration key:key CountName:@"engineMgrPredictCount" AvgName:@"engineMgrPredictAvg"];
    }
    else if([key isEqualToString:@"engineMgrSelect"])
    {
        [self UpdatePerformanceDict:duration key:key CountName:@"engineMgrSelectCount" AvgName:@"engineMgrSelectAvg"];
    }
    else if ([key isEqualToString:@"UpdateUITime"])
    {
        [self UpdatePerformanceDict:duration key:key CountName:@"UpdateUITimeCount" AvgName:@"UpdateUITimeAvg"];
    }
    else if ([key isEqualToString:@"NextWordUpdateUITime"])
    {
        [self UpdatePerformanceDict:duration key:key CountName:@"NextWordUpdateUITimeCount" AvgName:@"NextWordUpdateUITimeAvg"];
    }
    else if([key isEqualToString:@"CKPredictOperationTime"])
    {
        [self UpdatePerformanceDict:duration key:key CountName:@"CKPredictOperationTimeCount" AvgName:@"CKPredictOperationTimeAvg"];
    }
    else //
    {
        NSNumber *durationNumber = [NSNumber numberWithFloat:duration];
        [performenceDict setObject:durationNumber forKey:key];
    }
}

+(void)UpdatePerformanceDict:(CGFloat)duration key:(NSString*)key CountName:(NSString*)CountName AvgName:(NSString*)AvgName
{
    double sumValue = 0.0f;
    NSInteger count = 0;
    key = [key stringByAppendingString:@"Sum"];
    NSString *oldValue = [performenceDict valueForKey:key];
    NSNumber *number = [performenceDict valueForKey:CountName];
    if (oldValue != nil && number != nil) {
        sumValue = oldValue.floatValue;
        count =  number.integerValue;
    }
    
    count = count + 1;
    sumValue  = sumValue + duration;
    double avgTime = sumValue / count;
    
    NSNumber *avgNumber = [NSNumber numberWithDouble:avgTime]; //平均值
    NSNumber *sumNumber = [NSNumber numberWithDouble:sumValue];//总值
    NSNumber *countNumber = [NSNumber numberWithInteger:count];//总次数
    
    [performenceDict setObject:countNumber forKey:CountName];
    [performenceDict setObject:sumNumber forKey:key];
    [performenceDict setObject:avgNumber forKey:AvgName];
}

@end
