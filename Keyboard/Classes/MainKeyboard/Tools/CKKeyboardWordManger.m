//
//  CKWordTool.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/23.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKKeyboardWordManger.h"
#import <UIKit/UIKit.h>


@interface CKKeyboardWordManger ()
@property (nonatomic,strong) NSArray * shouldCapsArray;
@end
@implementation CKKeyboardWordManger
singleM(WordManger);

- (int)selectedCoolFontIndex
{
    if (!_selectedCoolFontIndex) {
        // coolFontTableView的选中行号
        NSUserDefaults * usrInfo = [NSUserDefaults standardUserDefaults];
        NSString * indexStr = [usrInfo objectForKey:@"CoolFontSelectedIndex"];
        return indexStr.intValue;
    }
    else{
        return _selectedCoolFontIndex;
    }
}

- (void)updateSelectedCoolFontIndex:(int)index
{
    _selectedCoolFontIndex = index;
}

// 字典输入
- (NSArray *)shouldCapsArray
{
    NSArray * array = [[NSArray alloc] initWithObjects:@".",@"?",@"!",@" ",@"\n",nil];
    return array;
}
- (BOOL )textIsEmptyWithPreStr:(NSString *)preStr afterStr:(NSString *)afterStr{
    BOOL textEmpty = NO;
    if (preStr.length == 0 && afterStr.length == 0) {
        textEmpty = YES;
    }
    return textEmpty;
}

- (BOOL)isSymbolOrNumberWithChar:(char)commitChar
{// 大写字母  小写字母 数字
    return (((commitChar>64)&&(commitChar<91)) || ((commitChar>96)&&(commitChar<123)) || ((commitChar>47)&&(commitChar<58)) );
}
- (BOOL)isLetterWithChar:(char)commitChar{
    return (((commitChar>64)&&(commitChar<91)) || ((commitChar>96)&&(commitChar<123)));
}
-(BOOL)shouldCapsWithPreStr:(NSString *)preStr afterStr:(NSString *)afterStr
{
    BOOL shouldCaps = NO;
    /*
     前有字母／数字——小写
     前有字母／数字＋1个或2个空格——小写
     前有字母／数字＋标点——小写
     前有(  "  '以外所有标点——小写
     前有数字/非. ? !＋空格——小写

     前有标点(  "  '——大写
     前有字母/数字/半句标点(非. ? !)＋3个以上空格——大写
     前有. ? !＋空格——大写
     */

    // 大写条件下返回YES,非大写条件全部返回NO
    if (preStr.length > 0) {
        // 先拿到最后一个字符,如果最后一个标点是(  "  '就返回YES,不用判断字符长度,提高效率
        unichar lastChar = [preStr characterAtIndex:preStr.length - 1];
        if (lastChar == '(' || lastChar == '"' || lastChar == '\'' || lastChar == '\n') {
            return YES;
        }
        else{ // 如果最后一个字符不是大写字符
            if (preStr.length == 1) { // 如果是单个字符,YES的情况已经判断过了,剩下的统一返回NO;
                return NO;
            }
            else if(preStr.length == 2){ // 如果有两个字符,拿倒数第二字字符来判断
                // 能到这里说明最后一个字符不是一票决定权的大写字符,那么只有当最后一个字符是空格的时候才有可能大写,否则肯定是小写,直接返回
                if (lastChar == ' ') { // 这是有可能是大写,决定权在倒数第二个字符手中
                    unichar commitChar2 = [preStr characterAtIndex:preStr.length - 2];
                  //  前有. ? !＋空格——大写
                    if (commitChar2 == '.' || commitChar2 == '?' || commitChar2 == '!') {
                        return YES;
                    }
                    return  NO;
                }
                else return NO;
            }
            else{ // >= 3
            // 前有字母/数字/半句标点(非. ? !)＋3个以上空格——大写
            // 上面一句话证明了最后三个字符必须全都是空格,只要有一个不是就返回NO,一个一个来
                // 最后一个
                if (lastChar == ' ') { // 最后一个是空格才有可能大写
                    unichar commitChar2 = [preStr characterAtIndex:preStr.length - 2];
                    //  前有. ? !＋空格——大写
                    if (commitChar2 == '.' || commitChar2 == '?' || commitChar2 == '!') {
                        return YES;
                    }
                    else if ([preStr characterAtIndex:preStr.length - 2] == ' ') { // 倒数第二个是空格才可能大写
                        if ([preStr characterAtIndex:preStr.length - 3] == ' ' || [preStr characterAtIndex:preStr.length - 3] == '.') {
                            return YES;
                        }else return NO;
                    }else return NO;
                }else return NO;
            }
            
        }
    }
    else { // 这里的else是指字符串长度 == 0,长度等于0的字符其实有两种,一种是nil字符,另外一种是@""empty字符
        return YES;
    }
    return shouldCaps;
}

- (BOOL)shouldInputFULL_STOPWithSB:(NSString *)str // 是否要在输入空格后输入句号的判断
{
    //判断光标前的字符尾部,因为上面已经输了一个空格,所以假如尾部有且仅有两个空格,且字符长度大于2,应当输入标点.
    int length = (int)str.length;
    /*
     1.至少为1,因为已经输入了一个空格
     2.如果小于等于2,不管第一个字符是什么,都应该直接返回
     */
    if (length <= 2) {
        return NO;
    }
   else if (length > 2) {
        NSString * s2 = [str substringWithRange:NSMakeRange(length - 2, 1)];
        NSString * s3 = [str substringWithRange:NSMakeRange(length - 3, 1)];
        /*
         如果大于2,要看后三位,而最后一位肯定是空格
         */
        if (![s2 isEqualToString:@" "]) {// 如果倒数第二个不是,说明用户刚输入一个空格
            return NO;
        }
        else{ // 如果倒数第二个是空格,就要判断倒数第三个是什么
            if ([s3 isEqualToString:@" "]) { // 如果倒数第三个也是空格,说明用户在想输入一堆空格
                return NO;
            }
            else{ // 如果倒数第三个不是空格,要判断第三个是否是标点符号
                NSRange range = [SENTENCE_END_CHARS rangeOfString:s3];
                if (range.location ==NSNotFound)//不包含
                {
                    return YES;                }
                else
                {
                    return NO;
                }
            }
        }
    }
    else
    {
        return NO;
    }
}


@end
