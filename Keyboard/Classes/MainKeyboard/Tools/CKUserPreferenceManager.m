//
//  CKUserPreferenceManager.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/21.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKUserPreferenceManager.h"
#import "KeyboardHeader.h"
#import "CKCommonTools.h"
@implementation CKUserPreferenceManager
singleM(UserPreferenceManager);

+(NSURL *)baseAppGroupURL
{
    return [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS];
}

- (NSString *)language
    {
        if (_language == nil) {
            NSUserDefaults * userLanguageInfo = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
            NSString * languageName = [userLanguageInfo objectForKey:@"Language"];
            if (languageName.length == 0 || languageName == nil) {
                languageName = @"English";
            }
            _language = languageName;
        }
        return _language;
}

- (void)updateLanguage
{
    NSUserDefaults * userLanguageInfo = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    NSString * languageName = [userLanguageInfo objectForKey:@"Language"];
    if (languageName.length == 0 || languageName == nil) {
        languageName = @"English";
    }
    _language = languageName;
    
}


- (BOOL)preferenceOFuserPredict
{
    NSUserDefaults * userInfo = [NSUserDefaults standardUserDefaults];
    BOOL userAllowPredict = [userInfo integerForKey:@"PredictSetting"];
    NSString * click = [userInfo objectForKey:@"UserClickPredict"];
    if (![click isEqualToString:@"UserClickPredict"]) {
        [self initWithYES:userPrediction];
        userAllowPredict = YES;
    }
    return userAllowPredict;
}

- (BOOL)preferenceOFAutoCorrect
{
    NSUserDefaults * userInfo = [NSUserDefaults standardUserDefaults];
    BOOL userAllowAutoCorrect = [userInfo boolForKey:@"AutoCorrectSetting"];
    NSString * click = [userInfo objectForKey:@"UserClickAutoCorrect"];
    if (![click isEqualToString:@"UserClickAutoCorrect"]) {
        [self initWithYES:userAutoCorrect];
        userAllowAutoCorrect = YES;
    }
    return userAllowAutoCorrect;
}

- (BOOL)preferenceOFAllowFullAccess
{
    BOOL userAllowFullAccess;
   
    if ([CKCommonTools isOpenAccessGranted])
    {
        userAllowFullAccess = CKUerAlowFullAccessYES;
    }
    else{
        userAllowFullAccess = CKUerAlowFullAccessNO;
    }

    return userAllowFullAccess;
}

-(void)initWithYES:(InitYesObjectType) type
{
    NSUserDefaults * userInfo = [NSUserDefaults standardUserDefaults];
    if (type == userPrediction) {
        [CKSaveTools saveInteger:1 forKey:@"PredictSetting" inRegion:CKSaveRegionExtensionKeyboard];
        [CKSaveTools saveString:@"UserClickPredict" forKey:@"UserClickPredict" inRegion:CKSaveRegionExtensionKeyboard];
    }
    else //userAutoCorrect
    {
        [CKSaveTools saveInteger:1 forKey:@"AutoCorrectSetting" inRegion:CKSaveRegionExtensionKeyboard];
        [CKSaveTools saveString:@"UserClickAutoCorrect" forKey:@"UserClickAutoCorrect" inRegion:CKSaveRegionExtensionKeyboard];
    }
    [userInfo synchronize];
}


@end
