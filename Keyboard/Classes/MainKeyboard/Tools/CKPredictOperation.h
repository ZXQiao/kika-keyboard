//
//  CKPredictOperation.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/14.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CKPredictionView.h"
/**
 *  标识是谁触发Predict的,如果是正常输入字符触发的,不需要去检测后面的字符是否有空格标点符号什么的
 *  如果是Delete的话就需要
 */
typedef NS_ENUM(int, CKPredictStyle) {
    CKPredictStyleNormal = 0,
    CKPredictStyleDelete,
};


@class CKPredictOperation;
@protocol CKPredictOperationDelegate <NSObject>


@required
/**
 *  代理方法回调
 *
 *  @param predictResultArray 返回的单词预测数组
 *  @param emojiArray         返回的emoji预测数组
 */

- (void)predictOperation:(CKPredictOperation *)currentOperation ReturnPredictWordArray:(NSMutableArray *)predictResultArray originPredictWordArray:(NSMutableArray *)originPredictResultArray  emojiArray:(NSMutableArray *)emojiArray sameWord:(BOOL)same hasPrefer:(BOOL)hasPrefer;

@end

@interface CKPredictOperation : NSOperation
/**
 *  用来标记当前的Opreration对应的是当前预测单词的第几个字符
 */
@property (nonatomic, assign) int predictIndex;

/**
 *  空格以后,光标之前的所有字符
 */
@property (nonatomic, strong) NSString * sourceStr;


/**
 *  代理
 */
@property (nonatomic, weak) id <CKPredictOperationDelegate> delegate;

@property (nonatomic, assign) CKPredictStyle  predictStyle;

@property (nonatomic, weak) NSDictionary * oppositeCoolFontDict;

@property (nonatomic, assign) CKPreviewCapsType previewCapsType;



@end
