//
//  CKUserPreferenceManager.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/21.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
/**
 *  这个类用来管理用户是否允许完全访问,是否允许自动大小写,自动纠错等  用户偏好设置
 */

@interface CKUserPreferenceManager : NSObject
singleH(UserPreferenceManager);

-(BOOL)preferenceOFuserPredict;
-(BOOL)preferenceOFAutoCorrect;
-(BOOL)preferenceOFAllowFullAccess;
+(NSURL *)baseAppGroupURL;
-(void)updateLanguage;
@property (nonatomic, copy) NSString * language;

@end
