//
//  CKKeyboardUImanger.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/24.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKKeyboardUImanger.h"
#import "UIImage+Tools.h"
#import "CKPredictionView.h"
#import "CKMainThemeModel.h"
@implementation CKKeyboardUImanger
singleM(UImanger);

// 1.设置一堆图片
- (void)changeCurrentImagesWithtopToolView:(CKKeyboardTopView *)topToolView predictionView:(CKPredictionView *)predictionView rootView:(UIImageView *)rootView
{
    UIImage * topToolViewImage;
    UIImage * backgroundImage;
    UIImage * coolFontImage;
    UIImage * coolFontPressedImage;
    UIImage * themeImage;
    UIImage * themePressedImage;
    UIImage * keyboardImage;
    UIImage * keyboardPressedImage;
    UIImage * settingImage;
    UIImage * settingPressedImage;
    UIImage * dismissImage;
    UIImage * dismissPressedImage;
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
    UIImage * memeImage;
    UIImage * memePressedImage;
#endif
    
    CKMainThemeModel * nowUseThemeModel = [CKMainThemeModel sholdUseThisModel];
    if (nowUseThemeModel) {
        predictionView.font_normal_color = [UIColor colorFromHexString:nowUseThemeModel.font_normal_color]; ;
        predictionView.font_highlight_color = [UIColor colorFromHexString:nowUseThemeModel.font_highlight_color];
        if (nowUseThemeModel.isOnline) {
            topToolViewImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_emojibar"resize:YES];
            backgroundImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_background" resize:NO
];
            themeImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_skin" resize:NO];
            themePressedImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_skin_pressed" resize:NO];
            keyboardImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_keyboard" resize:NO];
            keyboardPressedImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_keyboard_pressed" resize:NO];
            settingImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_setting" resize:NO];
            settingPressedImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_setting_pressed"resize:NO];
            dismissImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_dismiss"resize:NO];
            dismissPressedImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_dismiss_pressed"resize:NO];
            coolFontImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_coolfont"resize:NO];
            coolFontPressedImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_coolfont_pressed"resize:NO];
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
            memeImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_meme" resize:NO];
            memePressedImage = [UIImage imageWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_meme_pressed" resize:NO];
#endif
        }
        else
        {
            topToolViewImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_emojibar" resize:YES];
            backgroundImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_background" resize:NO];
            themeImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_skin" resize:NO];
            themePressedImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_skin_pressed" resize:NO];
            keyboardImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_keyboard" resize:NO];
            keyboardPressedImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_keyboard_pressed" resize:NO];
            settingImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_setting" resize:NO];
            settingPressedImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_setting_pressed" resize:NO];
            dismissImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_dismiss" resize:NO];
            dismissPressedImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_dismiss_pressed" resize:NO];
            coolFontImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_coolfont" resize:NO];
            coolFontPressedImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_coolfont_pressed" resize:NO];
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
            memeImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_meme" resize:NO];
            memePressedImage = [UIImage imageFromMainBundleWithThemeName:nowUseThemeModel.themeName ImageNameExtension:@"_meme_pressed" resize:NO];
#endif
        }
    }
    if (!nowUseThemeModel || topToolViewImage == nil)
    {
        topToolViewImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_emojibar@2x.png",Default_Theme_Name]ofType:nil]]];
        backgroundImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_background@2x.png",Default_Theme_Name] ofType:nil]]];
        themeImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_skin@2x.png",Default_Theme_Name] ofType:nil]]];
        themePressedImage= [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_skin_pressed@2x.png",Default_Theme_Name] ofType:nil]]];
        keyboardImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_keyboard@2x.png",Default_Theme_Name] ofType:nil]]];
        keyboardPressedImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_keyboard_pressed@2x.png",Default_Theme_Name] ofType:nil]]];
        settingImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_setting@2x.png",Default_Theme_Name] ofType:nil]]];
        settingPressedImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_setting_pressed@2x.png",Default_Theme_Name] ofType:nil]]];;
        dismissImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_dismiss@2x.png",Default_Theme_Name] ofType:nil]]];
        dismissPressedImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_dismiss_pressed@2x.png",Default_Theme_Name] ofType:nil]]];
        coolFontImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_coolfont@2x.png",Default_Theme_Name] ofType:nil]]];
        coolFontPressedImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_coolfont_pressed@2x.png",Default_Theme_Name] ofType:nil]]];
        
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
        memeImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_meme@2x.png",Default_Theme_Name] ofType:nil]]];
        memePressedImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_meme_pressed@2x.png",Default_Theme_Name] ofType:nil]]];
#endif
        predictionView.font_normal_color = [UIColor colorFromHexString:Default_Predict_Normal_Color];
        predictionView.font_highlight_color = [UIColor colorFromHexString:Default_Predict_Highted_Color];
    }

    topToolView.image = topToolViewImage;
    topToolView.userInteractionEnabled = YES;
    predictionView.image = topToolViewImage;
    predictionView.userInteractionEnabled = YES;
    
    rootView.image = backgroundImage;
    CATransition *anim = [CATransition animation];
//    fade 交叉淡化过渡
//    push 新视图把旧视图推出去
//    moveIn 新视图移到旧视图上面
//    reveal 将旧视图移开,显示下面的新视图
//    cube 立方体翻滚效果
//    oglFlip 上下左右翻转效果
//    suckEffect 收缩效果，如一块布被抽走
//    rippleEffect 水滴效果
//    pageCurl 向上翻页效果
//    pageUnCurl 向下翻页效果
//    cameraIrisHollowOpen 相机镜头打开效果
//    cameraIrisHollowClose 相机镜头关闭效果
    // 就水滴这个好看,别的都不好看,不用试了
    anim.type = @"rippleEffect";
//    anim.subtype = kCATransitionFromLeft; // 不是所有的动画都有方向
    anim.duration = 1;
    [rootView.layer addAnimation:anim forKey:nil];
    rootView.contentMode = UIViewContentModeScaleAspectFill;
    
    [self button:topToolView.themeButton image:themeImage pressedImage:themePressedImage];
    [self button:topToolView.keyboardButton image:keyboardImage pressedImage:keyboardPressedImage];
    [self button:topToolView.coolFontButton image:coolFontImage pressedImage:coolFontPressedImage];
    [self button:topToolView.dismissBtnOnVC image:dismissImage pressedImage:dismissPressedImage];
    [self button:topToolView.settingButton image:settingImage pressedImage:settingPressedImage];
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
    [self button:topToolView.memeButton image:memeImage pressedImage:memePressedImage];
#endif
    topToolView.coolFontImage = coolFontImage;
    topToolView.coolFontImagePressed = coolFontPressedImage;
}

- (void)button:(UIButton *)button image:(UIImage *)image pressedImage:(UIImage *)pressedImage
{
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:pressedImage forState:UIControlStateHighlighted];
    [button setImage:pressedImage forState:UIControlStateSelected];
}
// 2.设置约束
- (void)strangeSettingWithGlobalButton:(UIImageView *)imageView view:(UIView *)view{
    
    NSLayoutConstraint *leftSideConstraint = [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0];
    
    NSLayoutConstraint *topSideConstraint = [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    
    [view addConstraints:@[leftSideConstraint,rightConstraint, topSideConstraint,bottomConstraint]];
}

- (void)heightSettings
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGFloat screenH = screenSize.height;
    CGFloat screenW = screenSize.width;
    BOOL isLandscape = screenW > screenH;
    if ((isLandscape && screenW == 480) || (!isLandscape && screenH == 480)) { // iPhone4
        self.portraitHeightTotal = 256;
        self.landscapeHeightTotal = 194;
        self.portraitHeightKeyboard = KEYBOARD_PORTRAIT_HEIGHT;
        self.landscapeHeightKeyboard = 162;
        self.currentiPhoneModel = CKCurrentiPhoneModeliPhone4;
    }
    if ((isLandscape && screenW == 568) || (!isLandscape && screenH == 568)) { // iPhone5
        self.portraitHeightTotal = 255;
        self.landscapeHeightTotal = 194;
        self.portraitHeightKeyboard = 215;
        self.landscapeHeightKeyboard = 162;
        self.currentiPhoneModel = CKCurrentiPhoneModeliPhone5;
    }
    if ((isLandscape && screenW == 667) || (!isLandscape && screenH == 667)) { // iPhone6
        self.portraitHeightTotal = 256;
        self.landscapeHeightTotal = 194;
        self.landscapeHeightKeyboard = 162;
        self.portraitHeightKeyboard = KEYBOARD_PORTRAIT_HEIGHT;
        self.currentiPhoneModel = CKCurrentiPhoneModeliPhone6;
    }
    if ((isLandscape && screenW == 736) || (!isLandscape && screenH == 736)) { // iPhone6P
        self.portraitHeightTotal = 260;
        self.landscapeHeightTotal = 580.0 / 3;
        self.landscapeHeightKeyboard = 490.0 / 3;
        self.portraitHeightKeyboard = KEYBOARD_PORTRAIT_HEIGHT;
        self.currentiPhoneModel = CKCurrentiPhoneModeliPhone6Plus;
    }
    if (iPad){
        self.portraitHeightTotal = 300;
        self.landscapeHeightTotal = 291;
        self.landscapeHeightKeyboard = 243;
        self.portraitHeightKeyboard = 250;
        self.currentiPhoneModel = CKCurrentiPhoneModeliPhone6Plus;
    }
}

- (NSDictionary *)getRealDimensionsByModelAndDirection:(NSDictionary *)dimensions
{
    NSDictionary * model_dimensions;
    switch (self.currentiPhoneModel) {
        case CKCurrentiPhoneModeliPhone4:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE4];
            break;
        case CKCurrentiPhoneModeliPhone5:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE5];
            break;
        case CKCurrentiPhoneModeliPhone6:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE6];
            break;
        case CKCurrentiPhoneModeliPhone6Plus:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE6PLUS];
            break;
        default:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE6];
            break;
    }
    NSDictionary * direction_dimensions;
    if ([CKCommonTools screenDirectionIsLand]) {
        direction_dimensions = model_dimensions[PHONE_SCREEN_DIRECTION_LANDSCAPE];
    }
    else {
        direction_dimensions = model_dimensions[PHONE_SCREEN_DIRECTION_PORTRAIT];
    }
    return direction_dimensions;
}

- (NSDictionary *)getDimensionsByModel:(NSDictionary *)dimensions
{
    NSDictionary * model_dimensions;
    switch (self.currentiPhoneModel) {
        case CKCurrentiPhoneModeliPhone4:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE4];
            break;
        case CKCurrentiPhoneModeliPhone5:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE5];
            break;
        case CKCurrentiPhoneModeliPhone6:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE6];
            break;
        case CKCurrentiPhoneModeliPhone6Plus:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE6PLUS];
            break;
        default:
            model_dimensions = dimensions[PHONE_MODEL_IPHONE6];
            break;
    }
    return model_dimensions;
}

- (CGFloat)getFunctionFontSize:(NSString *)fontName
{
    CGFloat fontSize = 12.0f;

    if(_currentiPhoneModel == CKCurrentiPhoneModeliPhone4 || _currentiPhoneModel == CKCurrentiPhoneModeliPhone5) {
        fontSize = 16.0f;
        if ([fontName isEqualToString:@"LithosPro-Black"]) {
            fontSize = 12.0f;
        }
    } else if (_currentiPhoneModel == CKCurrentiPhoneModeliPhone6){
        fontSize = 15.0f;
        if ([fontName isEqualToString:@"LithosPro-Black"]) {
            fontSize = 12.0f;
        }
    } else if(_currentiPhoneModel == CKCurrentiPhoneModeliPhone6Plus){
        fontSize = 16.0f;
        if ([fontName isEqualToString:@"LithosPro-Black"]) {
            fontSize = 16.0f;
        }
    }
    return fontSize;
}

@end
