//
//  CKKeyboardUImanger.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/24.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyboardHeader.h"
#import "Singleton.h"

@class CKPredictionView;


@interface CKKeyboardUImanger : NSObject
singleH(UImanger);
- (void)changeCurrentImagesWithtopToolView:(CKKeyboardTopView *)topToolView predictionView:(CKPredictionView *)predictionView rootView:(UIImageView *)rootView;
- (void)strangeSettingWithGlobalButton:(UIImageView *)imageView view:(UIView *)view;
- (void)heightSettings;
/**
 *  当coolFont选中后,需要显示一个"XXXX Chose" 来提示
 *
 */
- (NSDictionary *)getRealDimensionsByModelAndDirection:(NSDictionary *)dimensions;
- (NSDictionary *)getDimensionsByModel:(NSDictionary *)dimensions;
- (CGFloat)getFunctionFontSize:(NSString *)fontName;

@property (nonatomic,assign) CGFloat portraitHeightTotal;
@property (nonatomic,assign) CGFloat landscapeHeightTotal;
@property (nonatomic,assign) CGFloat portraitHeightKeyboard;
@property (nonatomic,assign) CGFloat landscapeHeightKeyboard;
@property (nonatomic,assign) CKCurrentiPhoneModel currentiPhoneModel;
@end
