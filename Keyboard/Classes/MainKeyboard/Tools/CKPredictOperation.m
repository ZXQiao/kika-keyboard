//
//  CKPredictOperation.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/14.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKPredictOperation.h"
#import "CKEmojiPredictKing.h"
#import "CKPredictionKing.h"
#import "CKPerformence.h"

@interface CKPredictOperation()
@end

@implementation CKPredictOperation

- (void)main
{
    
    if ([self isCancelled]) {
        CKLog(@"** predict operation cancelled when start **");
        return;
    }
    
    NSMutableArray * predictResultArray = [NSMutableArray array];
    NSMutableArray * emojiPredictArray = [NSMutableArray array];
    /**
     *  传进来的光标以前的所有文字
     */
    /**
     *  如果有coolFont的话,把coolFont转为正常字符,成为checkStr
     *  有的时候会有一些符号什么的,是不需要预测的,成为checkRealStr
     *  如果是输入字符后触发这个方法的话,不需要去检测乱七八糟字符什么的
     *  如果是delete触发的这个方法,需要去检测后面的字符是不是正常的英文字符
     */
    NSString * checkStr;
    NSString * checkRealStr;
    NSString * lastStr;
    switch (self.predictStyle) {
        case CKPredictStyleNormal:
            /**
             *  截取光标以前,空格以后的字符段,也就是将要拿来预测的字符串
             *  这里不用担心单词有一堆空格的情况
             */
            if ([NSString lastStrWithStr:self.sourceStr]) {
                lastStr = [NSString lastStrWithStr:self.sourceStr];
            }
            else
            {
                return;
            }
            break;
        case CKPredictStyleDelete:
            lastStr = self.sourceStr;
            if (!lastStr) {
                return;
            }
            break;
        default:
            break;
    }
    
    if ([lastStr isEqualToString:@""]) {
        return;
    }
    
    
    NSString *__block realString = @"";
    int __block characterCount = 0;
    [lastStr enumerateSubstringsInRange:NSMakeRange(0, [lastStr length])
                                     options:NSStringEnumerationByComposedCharacterSequences
                                  usingBlock: ^(NSString* substring, NSRange substringRange, NSRange enclosingRange, BOOL* stop) {
                                      NSString * realCharacter = [self.oppositeCoolFontDict[substring] lowercaseString] ;
                                      if (realCharacter.length == 0 || [realCharacter isEqualToString:substring.lowercaseString]) {
                                          realCharacter = substring;
                                      }
                                      characterCount ++;

                                      realString = [realString stringByAppendingString:realCharacter];
                                  }];
    BOOL hasPrefer = NO;
    if (lastStr.length <= Predict_MAX_Count) {
        predictResultArray = (NSMutableArray *)[[CKPredictionKing sharePrediction] predictionArrayFromInputStr:nil  withLastString:realString.lowercaseString];
        if (predictResultArray == nil) {
            return;
        }
        NSString * firstCandidate = predictResultArray[0];
        if ([firstCandidate isEqualToString:@"--prefer"]) {
            [predictResultArray removeObjectAtIndex:0];
            hasPrefer = YES;
        }
//        CKLog(@"%@===predictR",predictResultArray);
        if (realString.length >= 2) {
            emojiPredictArray = (NSMutableArray *)[[CKEmojiPredictKing alloc] queryEmojisWithSearchText:[realString lowercaseString]];
        }
//        CKLog(@"%@===emoji",emojiPredictArray);
    }
    NSArray * copyArray = predictResultArray.copy;
    NSMutableArray * originPredictResultArray = [NSMutableArray arrayWithArray:copyArray];
    checkStr = [realString lowercaseString];
    checkRealStr = realString;
    if (checkStr) {
        self.predictIndex = (int)characterCount;
    }
    BOOL sameWord = NO;
    NSUInteger input_word_cap_type = -1;// -1:invalid 0:lowercase 1:uppercase 2:capitalized
    for (int i = 0; i < checkRealStr.length; i ++) {
        unichar curChar = [checkRealStr characterAtIndex:i];
        if (islower(curChar)) {
            if (input_word_cap_type == -1) {
                input_word_cap_type = 0;
                break;
            } else if (input_word_cap_type == 1) {
                input_word_cap_type = 2;
                break;
            }
        } else {
            if (input_word_cap_type == -1) {
                input_word_cap_type = 1;
            }
        }
    }

    for (int i = 1;i < predictResultArray.count; i ++) {
        NSString * str = predictResultArray[i];
        if ([str hasSuffix:@" "]) {
            str = [str stringByReplacingCharactersInRange:NSMakeRange(str.length - 1, 1) withString:@""];
        }
        if ([str isEqualToString:checkRealStr] || [str isEqualToString:checkRealStr.lowercaseString]) {// 如果有相同的字符串,注意多个空格
            // 2.去除相同位置的单词
            [predictResultArray removeObjectAtIndex:i]; // 移除的顺序不能换,如果先移除0的话,i的索引就变了
            // 1. 去除第一个单词
            [predictResultArray removeObjectAtIndex:0];
            [originPredictResultArray removeObjectAtIndex:i];
            [originPredictResultArray removeObjectAtIndex:0];
            sameWord = YES;
            break;
        }
    }

    for (int i = 0;i < predictResultArray.count; i ++) {
        NSString * sourceWord = predictResultArray[i];
        if (input_word_cap_type == 1 && checkRealStr.length > 1) {
            predictResultArray[i] = [sourceWord uppercaseString];
        }
        else if (islower([sourceWord characterAtIndex:0])) {
            if ((input_word_cap_type == 2) || (input_word_cap_type == 1 && checkRealStr.length == 1)) {
                NSInteger firstSpacePosition = [sourceWord indexOfFirstOccurrenceOfNeedle:@" "];
                if (firstSpacePosition != -1) {
                    NSString * firstWord = [sourceWord substringToIndex:firstSpacePosition];
                    predictResultArray[i] = [sourceWord stringByReplacingCharactersInRange:NSMakeRange(0, firstSpacePosition) withString:firstWord.capitalizedString];
                }
                else {
                    predictResultArray[i] = sourceWord.capitalizedString;
                }
            }
        }
        if (i == 0) {
            if (!sameWord) {
                predictResultArray[i] = checkRealStr;
            }
            continue;
        }
    }
    
    NSMutableDictionary * check_duplicate_dict = [[NSMutableDictionary alloc] init];
    NSUInteger arr_len = predictResultArray.count;
    for (int i = 0;i < arr_len; i ++) {
        NSString * word = predictResultArray[i];
        BOOL existed = [[check_duplicate_dict objectForKey:word] boolValue];
        if (!existed) {
            check_duplicate_dict[word] = @(YES);
        }
        else {
            [predictResultArray removeObjectAtIndex:i];
            [originPredictResultArray removeObjectAtIndex:i];
            i--;
            arr_len--;
        }
    }
    
    if ([self isCancelled]) {
        CKLog(@"** operation cancelled **");
        return;
    }
//        CKLog(@"%@===predictR,%@===emoji",predictResultArray,emojiPredictArray);
    if ([self.delegate respondsToSelector:@selector(predictOperation:ReturnPredictWordArray:originPredictWordArray:emojiArray:sameWord:hasPrefer:)]) {
        [self.delegate predictOperation:self ReturnPredictWordArray:predictResultArray originPredictWordArray:originPredictResultArray emojiArray:emojiPredictArray sameWord:sameWord hasPrefer:hasPrefer];
    }
    
}

@end
