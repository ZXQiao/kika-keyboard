//
//  CKWordTool.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/23.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "KeyboardHeader.h"
/**
 *  管理键盘输入内容的单例
 */
@interface CKKeyboardWordManger : NSObject
singleH(WordManger);

// 当前的输入框内文本是否为空
//-(BOOL)textIsEmptyWithPreStr:(NSString *)preStr afterStr:(NSString *)afterStr;
// 是否应该大写
-(BOOL)shouldCapsWithPreStr:(NSString *)preStr afterStr:(NSString *)afterStr;
- (BOOL )textIsEmptyWithPreStr:(NSString *)preStr afterStr:(NSString *)afterStr;
- (BOOL)shouldInputFULL_STOPWithSB:(NSString *)str;

- (void)updateSelectedCoolFontIndex:(int)index;

//@property (nonatomic, strong) NSDictionary * oppositeCoolFontDict;
@property (nonatomic, assign) int selectedCoolFontIndex;
@end
