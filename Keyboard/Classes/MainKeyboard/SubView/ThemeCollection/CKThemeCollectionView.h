//
//  CKThemeCollectionView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/14.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CKThemeCollectionViewDelegate <NSObject>
- (void)skinTableViewChooseNewTheme;
@end
@interface CKThemeCollectionView : UIView
@property (nonatomic,weak) id<CKThemeCollectionViewDelegate> delegate;
@end
