//
//  CKThemeCollectionView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/14.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKThemeCollectionView.h"
#import "CKThemeCollectionCell.h"
#import "CKKeyboardUImanger.h"

static NSString * const reuseIdentifier = @"CKThemeCollectionCell";
#define Online_Theme_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS]URLByAppendingPathComponent:@"OnlineThemes"]

@interface CKThemeCollectionView()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    CGFloat collectionCellSize;
    CGFloat collectionCellPadding;
    CGFloat collectionLinePadding;
    CGFloat collectionViewLeftPadding;
    CGFloat collectionViewRightPadding;
    CGFloat collectionViewTopPadding;
    CGFloat collectionViewBottomPadding;
    CKMainThemeModel * _nowUseThemeModel;
}

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) NSArray * defaultThemeArray;
@property (nonatomic, strong) NSMutableArray * onlineThemeArray;

@end

@implementation CKThemeCollectionView

- (NSArray *)defaultThemeArray
{
    if (_defaultThemeArray == nil) {
        _defaultThemeArray = [CKMainThemeModel defaultThemeModels];
    }
    return _defaultThemeArray;
}

- (NSMutableArray *)onlineThemeArray
{
    if (_onlineThemeArray == nil) {
        _onlineThemeArray = [NSMutableArray array];
        NSArray * array = [NSArray arrayWithContentsOfURL:[Online_Theme_URL URLByAppendingPathComponent:@"downloadOnlineThemeArray.plist"]];
        for (NSMutableDictionary * dict in array) {
            if ([dict[@"themeName"] isEqualToString:@"rainyday"]) {
                dict[@"themeName"] = @"lollipop";
                dict[@"setting_icon_highlight"] = @"#ececec";
            }
            CKMainThemeModel * mainTheme = [CKMainThemeModel mainThemeModelWithDict:dict];
            [_onlineThemeArray addObject:mainTheme];
        }
    }
    return _onlineThemeArray;
}



- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"theme_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * realDimensions = [[CKKeyboardUImanger shareUImanger] getRealDimensionsByModelAndDirection:dimensions];
    
    collectionCellSize = [realDimensions[@"collectionCellSize"] floatValue];
    collectionLinePadding = [realDimensions[@"collectionLinePadding"] floatValue];
    collectionCellPadding = [realDimensions[@"collectionCellPadding"] floatValue];
    collectionViewLeftPadding = [realDimensions[@"collectionViewLeftPadding"] floatValue];
    collectionViewRightPadding = [realDimensions[@"collectionViewRightPadding"] floatValue];
    collectionViewTopPadding = [realDimensions[@"collectionViewTopPadding"] floatValue];
    collectionViewBottomPadding = [realDimensions[@"collectionViewBottomPadding"] floatValue];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDimensions];
        [self addThemeViewWithFrame:frame];
}
    return self;
}

- (void)addThemeViewWithFrame:(CGRect)frame
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    UICollectionView * collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:flowLayout];
    self.collectionView = collectionView;
    [self addSubview:collectionView];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    UINib *nib = [UINib nibWithNibName:reuseIdentifier bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:reuseIdentifier];
    self.backgroundColor  = [UIColor clearColor];
    self.collectionView.backgroundColor = [UIColor clearColor];
    collectionView.allowsSelection = YES;
    [self initDefaultModel];
    [self.collectionView reloadData];

}

- (void)initDefaultModel
{
    _nowUseThemeModel = [CKMainThemeModel sholdUseThisModel];
    if (!_nowUseThemeModel) {
        _nowUseThemeModel = self.defaultThemeArray[Default_Theme_Index];
        [CKSaveTools saveMainTheme:_nowUseThemeModel toRegion:CKSaveRegionAppGroup];
    }
}


#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.defaultThemeArray.count + self.onlineThemeArray.count + 1;
}
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{return 1;}

-(BOOL)IsLocalTheme:(NSString*)FileName CurThemeName:(NSString *)curThemeName
{
    BOOL isLocalTheme = NO;
    NSArray *localThemeName = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle]pathForResource:FileName ofType:nil]];
    for (int i = 0; i < localThemeName.count; i++) {
        if ([localThemeName[i] isEqualToString:curThemeName]) {
            isLocalTheme = YES;
            break;
        }
    }
    return isLocalTheme;
}

//每个UICollectionView展示的内容
-(CKThemeCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CKThemeCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[CKThemeCollectionCell alloc] init];
    }
    if (indexPath.item == self.defaultThemeArray.count + self.onlineThemeArray.count) { // 最后一个
        cell.themeView.image = [UIImage imageNamed:@"universal_add_normal@2x"];
        cell.backImageView.hidden = YES;
        cell.selectedImageView.hidden = YES;
        return cell;
    }
    _nowUseThemeModel = [CKMainThemeModel sholdUseThisModel];
    if (!_nowUseThemeModel) {
        _nowUseThemeModel = _defaultThemeArray[0];
    }
    CKMainThemeModel * mainthemeModel;
    if (indexPath.row < _defaultThemeArray.count) {
        mainthemeModel = _defaultThemeArray[indexPath.row];
        mainthemeModel.onUse = NO;
        if (!mainthemeModel.isOnline) {
            if ([mainthemeModel.themeName isEqualToString:_nowUseThemeModel.themeName]) {
                mainthemeModel.onUse = YES;
            }
        }
    }
    else{
        mainthemeModel = _onlineThemeArray[indexPath.row - _defaultThemeArray.count];
        mainthemeModel.online = YES;
        mainthemeModel.onUse = NO;
        if (mainthemeModel.isOnline) {
            if ([mainthemeModel.themeName isEqualToString:_nowUseThemeModel.themeName]) {
                mainthemeModel.onUse = YES;
            }
        }
    }
    cell.mainThemeModel = mainthemeModel;
    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout
//定义每个UICollectionView 的大小

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionCellSize, collectionCellSize);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return collectionLinePadding; // 行间距
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return collectionCellPadding; // cell间距
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(collectionViewTopPadding, collectionViewLeftPadding,collectionViewBottomPadding, collectionViewRightPadding); // 上下左右的偏移量
}

-(BOOL)Switch2HostApp:(NSIndexPath *)indexPath
{
    if (indexPath.item == self.defaultThemeArray.count + self.onlineThemeArray.count) { // 最后一个跳转
            NSString *msg = [NSString stringWithFormat:@"Open\n%@ App\nfor more!",APP_NAME];
        [CKCommonTools showInfoByFittingParentView:self msg:msg];
//        UIResponder * responder = self;
//        while ((responder = [responder nextResponder]) != nil) {
//            if ([responder respondsToSelector:@selector(openURL:)] == YES) {
//                [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@.themes", APP_ROOT_URL]]];
//            }
//        }
        return TRUE;
    }
    return FALSE;
}

#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter] postNotificationName:UserHaveDoneSomethingNotification object:nil];
    BOOL status = [self Switch2HostApp:indexPath];// 最后一个跳转
    if (status) {
        [CKSaveTools saveClickWithEventType:KB_Jump_To_MainApp_Click key:@"Theme"];
        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                           action:KB_Jump_To_MainApp_Click
                                            label:@"Theme"];
        return;
    }
    CKMainThemeModel * themeModel;
    if (indexPath.row < _defaultThemeArray.count) {
        themeModel = _defaultThemeArray[indexPath.row];
    }
    else if(indexPath.row < _defaultThemeArray.count + _onlineThemeArray.count){
        themeModel = _onlineThemeArray[indexPath.row - _defaultThemeArray.count];
    
        NSString * imageName = [NSString stringWithFormat:@"%@/%@_background@2x.png",themeModel.themeName,themeModel.themeName];
        UIImage * backImage = [[UIImage alloc] initWithContentsOfFile: [Online_Theme_URL URLByAppendingPathComponent:imageName].path];
        if (!backImage) {
            [_onlineThemeArray removeObjectAtIndex:indexPath.row - _defaultThemeArray.count];
    //        if (indexPath.item < _onlineThemeArray.count + _defaultThemeArray.count + 1) {
    //            [collectionView deleteItemsAtIndexPaths:@[indexPath]];
    //        }
    //        else{
    //        }
            // 删除
    //        NSError * error;
    //        if ([[NSFileManager defaultManager] fileExistsAtPath:[Online_Theme_URL URLByAppendingPathComponent:themeModel.themeName].path]) {
    //            [[NSFileManager defaultManager] removeItemAtPath:[Online_Theme_URL URLByAppendingPathComponent:themeModel.themeName].path error:&error];
    //        }
            if ([CKCommonTools isOpenAccessGranted]) {
                NSURL * arrayURL = [Online_Theme_URL URLByAppendingPathComponent:@"downloadOnlineThemeArray.plist"];
                NSMutableArray * downloadArrayM = [NSMutableArray arrayWithContentsOfURL:arrayURL];
                downloadArrayM = (downloadArrayM == nil)?[NSMutableArray array]:downloadArrayM;
                if (indexPath.row - _defaultThemeArray.count < downloadArrayM.count) {
                    [downloadArrayM removeObjectAtIndex:(indexPath.row - _defaultThemeArray.count)];
                }
                [downloadArrayM writeToURL:arrayURL atomically:YES];
            }
            [collectionView reloadData];
            return;
        }
    }
    
    themeModel.onUse = YES;

    if ([CKCommonTools isOpenAccessGranted]) { // 如果允许完全访问
        [CKSaveTools saveMainTheme:themeModel toRegion:CKSaveRegionAppGroup];
        NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
        NSDate * date = [NSDate date];
        [sharedDefaults setObject:date forKey:Last_Save_Theme_Date];
        [sharedDefaults synchronize];
    }
    else{ // 不允许完全访问的时候放到沙盒里
        [CKSaveTools saveMainTheme:themeModel toRegion:CKSaveRegionExtensionKeyboard];
        NSDate * date = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:date forKey:Last_Save_Theme_Date];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    _nowUseThemeModel = themeModel;
    [_collectionView reloadData];
     if ([self.delegate respondsToSelector:@selector(skinTableViewChooseNewTheme)]) {
        [self.delegate skinTableViewChooseNewTheme];
    }
    [CKSaveTools saveClickWithEventType:KB_Theme_Click key:themeModel.themeName];
    
    // 这里@{KB_Theme_Click:themeModel.themeName ? themeModel.themeName : @"error" }是因为查内存泄露的时候报了一句"Dictionary value cannot be nil"
    [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                       action:KB_Theme_Click
                                        label:(themeModel.themeName ? themeModel.themeName : @"error")];
}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
@end
