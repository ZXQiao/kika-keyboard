//
//  CKThemeCollectionCell.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/14.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKMainThemeModel.h"

@interface CKThemeCollectionCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView * backImageView;
@property (nonatomic, strong) IBOutlet UIImageView * themeView;
@property (nonatomic, strong) IBOutlet UIImageView * selectedImageView;

@property (nonatomic, strong)CKMainThemeModel * mainThemeModel;

@end
