//
//  CKThemeCollectionCell.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/14.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKThemeCollectionCell.h"
#import "UIImage+Tools.h"
#import "UIImageView+WebCache.h"
@implementation CKThemeCollectionCell

- (void)setMainThemeModel:(CKMainThemeModel *)mainThemeModel
{
    _mainThemeModel = mainThemeModel;
    if (mainThemeModel.isOnline) {
        NSURL * url = [UIImage URLWithThemeName:mainThemeModel.themeName ExtensionString:@"_thumbnail"];
//        [self.themeView sd_setImageWithURL:url placeholderImage:nil];
        self.themeView.image = [UIImage imageWithContentsOfFile:url.path];
    }
    else{
        NSString * thumbnailName = [NSString stringWithFormat:@"%@_thumbnail@2x.png",mainThemeModel.themeName];
//        NSURL * fileURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:thumbnailName ofType:nil]];
//        [self.themeView sd_setImageWithURL:fileURL placeholderImage:nil];
        self.themeView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:thumbnailName ofType:nil]];
    }
    if (mainThemeModel.isOnUse) {
        self.selectedImageView.hidden = NO;
    }
    else
    {
        self.selectedImageView.hidden = YES;
    }
    
}


@end

