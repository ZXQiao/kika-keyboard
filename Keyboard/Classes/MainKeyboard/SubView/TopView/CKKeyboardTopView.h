//
//  CKKeyboardTopView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/24.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CKKeyboardTopViewDelegate <NSObject>
- (void)topViewDelegateButtonClicked:(UIButton *)button sameButton:(BOOL)same;
@end

@interface CKKeyboardTopView : UIImageView

@property (nonatomic, strong) UIButton * themeButton;// emoji按钮
@property (nonatomic, strong) UIButton * coolFontButton;// gif
@property (nonatomic, strong) UIButton * keyboardButton;// emojiArt

/*
 注:meme的tag值这里设置为6,没有按照按钮的顺序来设置,是因为meme是在后来才加的,如果改tag值的话其他的按钮关联的响应事件都要改
 */
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
@property (nonatomic, strong) UIButton * memeButton;
#endif
@property (nonatomic ,strong) UIButton * dismissBtnOnVC;// dismiss
@property (nonatomic, strong) UIButton * settingButton;
@property (nonatomic,strong) UIImage * coolFontImage;
@property (nonatomic,strong) UIImage * coolFontImagePressed;

@property (nonatomic, strong) UIButton * selectedCategoryButton; // 当前选中的类目按钮

@property (nonatomic,weak) id <CKKeyboardTopViewDelegate> topViewDelegate;


@end
