//
//  CKKeyboardTopView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/24.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKKeyboardTopView.h"
#import "Consetting.h"
#import "CKKeyboardUImanger.h"

//#define RED_DOT_W 7
@interface CKKeyboardTopView ()
{
    NSDictionary * dimensions;
    UIImageView * dotImage;
    BOOL _userHaveDoneSth;
    int _lastPressedButtonTag;
}
@property (nonatomic, strong) NSString * language;

//@property (nonatomic, strong) UIView * memeRedDotView;
@end

@implementation CKKeyboardTopView

- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"topview_dimensions.plist" ofType:nil];
    dimensions = [[CKKeyboardUImanger shareUImanger] getDimensionsByModel:[NSDictionary dictionaryWithContentsOfFile:resourceTypePath]];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDimensions];
        [self buildRedDotImage];
        NSUserDefaults * userLanguageInfo = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
        NSString * language = [userLanguageInfo objectForKey:@"Language"];
        if (language.length == 0 || language == nil) {
            language = @"English";
        }
        
        self.language = language;

        [self addSubview:self.themeButton];
        [self addSubview:self.coolFontButton];
        [self addSubview:self.keyboardButton];
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
        [self addSubview:self.memeButton];
#endif
        [self addSubview:self.settingButton];
        [self addSubview:self.dismissBtnOnVC];
        _lastPressedButtonTag = 3; // 默认最后点击的按钮为返回键盘的按钮

        /**
         *  思路说明:
         *  由于topToolView始终存在,所以由它来监听我们要统计的事件是比较合适的
         *  1.如果是Theme,Font,Setting这些页面,用户只能靠点击topToolView的Keyboard按钮来返回
              所以当点击Keyboard按钮的时候要判断一下_lastedPressedBtn的值
              进入这些页面后,随便点击一个地方,topToolView就会收到一个通知,来更改变量_lastedPressedBtn为YES
              这样就证明用户点了东西了
            2.Meme和Emoji页面可以通过点击底部的ABC按钮返回,在点击这个按钮时,topToolView会受到另外一个通知
              UserClickBottomBackButtonNotification(下面注册的第二个通知),来告诉topToolView用户返回了
              而跟1中相同,如果用户点击界面中任何一条内容,用户会收到1中相同的通知,来改变_lastedPressedBtn的值,以此来作为判断
         *
         *
         */

        // 用来接收用户是否做了一些事情的通知,然后发送给友盟
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userHaveDoneSth) name:UserHaveDoneSomethingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userClickBottomBackButton:) name:UserClickBottomBackButtonNotification object:nil];
        // UserClickBottomBackButtonNotification
        _userHaveDoneSth = NO;
    }
    return self;
}

- (void)userHaveDoneSth{
    _userHaveDoneSth = YES;
}

- (void)userClickBottomBackButton:(NSNotification *)notification
{
    if (_userHaveDoneSth) { // 如果用户已经输出东西的话就直接返回
        _userHaveDoneSth = NO;
        return;
    }
    else{
        _userHaveDoneSth = NO;
        NSString * type = notification.object;
        if (type == nil) return;
        [CKSaveTools saveClickWithEventType:KB_UserPressButDoNothing key:type];
        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_UserPressButDoNothing label:type];
    }
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    NSDictionary * realDimensions;
    if ([CKCommonTools screenDirectionIsLand]) {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_LANDSCAPE];
    }
    else {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_PORTRAIT];
    }

    CGFloat leftPadding = [realDimensions[@"topViewLeftPadding"] floatValue];
    CGFloat topPadding = [realDimensions[@"topViewTopPadding"] floatValue];
    CGFloat BtnPadding = [realDimensions[@"topViewButtonPadding"] floatValue];
    CGFloat btnW = [realDimensions[@"topViewButtonWidth"] floatValue];
    CGFloat btnH = [realDimensions[@"topViewButtonHeight"] floatValue];
    
    self.themeButton.frame =  CGRectMake(leftPadding, topPadding, btnW ,btnH);
    if (self.coolFontButton.hidden) { // 如果隐藏的话
        self.keyboardButton.frame = CGRectMake(CGRectGetMaxX(self.themeButton.frame) + BtnPadding,topPadding , btnW,btnH);
    }
    else
    {
        self.coolFontButton.frame = CGRectMake(CGRectGetMaxX(self.themeButton.frame) + BtnPadding,topPadding , btnW,btnH);
        self.keyboardButton.frame = CGRectMake(CGRectGetMaxX(self.coolFontButton.frame) + BtnPadding,topPadding , btnW,btnH);
    }
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
    self.memeButton.frame = CGRectMake(CGRectGetMaxX(self.keyboardButton.frame) + BtnPadding,topPadding , btnW,btnH);
    self.settingButton.frame = CGRectMake(CGRectGetMaxX(self.memeButton.frame) + BtnPadding,topPadding , btnW,btnH);
#else
    self.settingButton.frame = CGRectMake(CGRectGetMaxX(self.keyboardButton.frame) + BtnPadding,topPadding , btnW,btnH);
#endif
    self.dismissBtnOnVC.frame = CGRectMake(self.frame.size.width - leftPadding - btnW, topPadding, btnW  , btnH);
    
    if (self.selectedCategoryButton == nil) {
        self.keyboardButton.selected = YES;
        self.selectedCategoryButton = self.keyboardButton;
    }
    [self freshRedDotImage];
}

-(void)buildRedDotImage{
    //为避免用户没有打开 full access 将用户类型保存到本地extension中
    [CKSaveTools saveAppGroupUserTypeToExtension];
    [CKSaveTools saveAppGroupEvalteStatusToExtension];
    if( [CKSaveTools determineNowUserType] && ![[NSUserDefaults standardUserDefaults]objectForKey:RED_DOT_CLICKED]){
        //新用户且没点击过coolfont
        [CKSaveTools saveInteger:LockStatusLock forKey:LOCK_STATUS inRegion:CKSaveRegionExtensionKeyboard];
        //锁默认关闭
        dotImage=[[UIImageView alloc]init];
        dotImage.backgroundColor=[UIColor colorWithRed:1.0 green:0.09 blue:0.09 alpha:1.0];
        [self addSubview:dotImage];
    }
}

-(void)freshRedDotImage{
    if(dotImage != nil && [CKSaveTools determineNowUserType]){
        //新用户没点击过coolfont
        dotImage.frame = [self currentDotImageFram];
        dotImage.layer.masksToBounds=YES;
        dotImage.layer.cornerRadius=CGRectGetWidth(dotImage.bounds)/2;
        [self bringSubviewToFront:dotImage];
    }else if (dotImage != nil ){
        [dotImage removeFromSuperview];
        dotImage = nil;
    }
}

-(CGRect)currentDotImageFram{
    CGRect coolFontBtnFram=self.coolFontButton.frame;
    float xPosition=0,yPosition=0;
    if([CKCommonTools screenDirectionIsLand]){//横屏
       xPosition = ceilf(0.8*CGRectGetMidX(coolFontBtnFram)+CGRectGetWidth(coolFontBtnFram));
       yPosition = ceilf(0.3*CGRectGetMidY(coolFontBtnFram));
        dotImage.frame=CGRectMake(xPosition, yPosition,5, 5);
    }else{//竖屏布局
        xPosition = ceilf(0.7*CGRectGetMidX(coolFontBtnFram)+CGRectGetWidth(coolFontBtnFram));
        yPosition = ceilf(0.3*CGRectGetMidY(coolFontBtnFram));
    }
    return CGRectMake(xPosition, yPosition, 5, 5);
}
-(void)touchesEnded:(NSSet *)touches withEvent: (UIEvent *)event{
    
    CGFloat location_x = [[touches anyObject] locationInView:self].x;
    CGFloat theme_left_x = 0;
    CGFloat theme_right_x = (self.themeButton.frame.origin.x + self.themeButton.frame.size.width + self.coolFontButton.frame.origin.x) / 2;
    CGFloat coolfont_left_x = theme_right_x;
    CGFloat coolfont_right_x = (self.coolFontButton.frame.origin.x + self.coolFontButton.frame.size.width + self.keyboardButton.frame.origin.x) / 2;
    CGFloat keyboard_left_x = coolfont_right_x;
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
    CGFloat keyboard_right_x = (self.keyboardButton.frame.origin.x + self.keyboardButton.frame.size.width + self.memeButton.frame.origin.x) / 2;
    CGFloat meme_left_x = keyboard_right_x;
    CGFloat meme_right_x = (self.memeButton.frame.origin.x + self.memeButton.frame.size.width + self.settingButton.frame.origin.x) / 2;
    CGFloat setting_left_x = meme_right_x;
#else
    CGFloat keyboard_right_x = (self.keyboardButton.frame.origin.x + self.keyboardButton.frame.size.width + self.settingButton.frame.origin.x) / 2;
    CGFloat setting_left_x = keyboard_right_x;
#endif
    CGFloat setting_right_x = self.settingButton.frame.origin.x + self.settingButton.frame.size.width + (self.settingButton.frame.origin.x - setting_left_x);
    if (location_x >= theme_left_x && location_x < theme_right_x) {
        [self topToolViewbuttonClicked:self.themeButton];
    }
    else if (location_x >= coolfont_left_x && location_x < coolfont_right_x) {
        [self topToolViewbuttonClicked:self.coolFontButton];
    }
    else if (location_x >= keyboard_left_x && location_x < keyboard_right_x) {
        [self topToolViewbuttonClicked:self.keyboardButton];
    }
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
    else if (location_x >= meme_left_x && location_x < meme_right_x) {
        [self topToolViewbuttonClicked:self.memeButton];
    }
#endif
    else if (location_x >= setting_left_x && location_x < setting_right_x) {
        [self topToolViewbuttonClicked:self.settingButton];

    }
}
- (void)userDoNothingButPressed:(int)buttonTag{
    _userHaveDoneSth = NO;
    NSString * buttonType = @"keyboard";
    if (buttonTag == 1) {
        buttonType = @"theme";
    }
    else if (buttonTag == 2) {
       buttonType = @"coolFont";
    }
    else if (buttonTag == 6) {
         buttonType = @"meme";
    }
    else if (buttonTag == 4) {
        buttonType = @"setting";
    }
    
    [CKSaveTools saveClickWithEventType:KB_UserPressButDoNothing key:buttonType];
    [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_UserPressButDoNothing label:buttonType];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIButton *)buttonAllocWithAction:(SEL)action{
    UIButton * button = [[UIButton alloc] init];
//    button.showsTouchWhenHighlighted = YES;
    button.adjustsImageWhenDisabled = YES;
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    button.contentMode = UIViewContentModeScaleAspectFit;
    return button;
}
- (UIButton *)themeButton{
    if (!_themeButton) {
        _themeButton = [self buttonAllocWithAction:@selector(topToolViewbuttonClicked:)];
        _themeButton.tag = 1;
        _themeButton.adjustsImageWhenHighlighted = NO;
    }
    return _themeButton;
}

#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
- (UIButton *)memeButton
{
    if (!_memeButton) {
        _memeButton = [self buttonAllocWithAction:@selector(topToolViewbuttonClicked:)];
        _memeButton.tag = 6;
        _memeButton.adjustsImageWhenHighlighted = NO;
//        _memeRedDotView = [[UIView alloc] init];
//        [_memeButton addSubview:_memeRedDotView];
//        _memeRedDotView.backgroundColor = [UIColor redColor];
//        _memeRedDotView.layer.masksToBounds = YES;
//        _memeRedDotView.layer.cornerRadius = RED_DOT_W / 2;
//        NSUserDefaults * userDefauts = [NSUserDefaults standardUserDefaults];
//        NSString * memeHasClicked = [userDefauts objectForKey:@"memeHasClicked"];
//        if ([memeHasClicked isEqualToString:@"memeHasClicked"]) { // 如果meme已经被点击了,就不再显示
//            _memeRedDotView.hidden =YES;
//        }
//        else
//        {
//            _memeRedDotView.hidden = NO;
//        }
    }
    return _memeButton;
}
#endif

- (UIButton *)coolFontButton
{
    if (!_coolFontButton) {
        _coolFontButton = [self buttonAllocWithAction:@selector(topToolViewbuttonClicked:)];
        _coolFontButton.tag = 2;
    }
    if (![self.language.lowercaseString isEqualToString:@"english"]) {
        _coolFontButton.hidden = YES;
    }
    else
    {
        _coolFontButton.hidden = NO;
    }
    _coolFontButton.adjustsImageWhenHighlighted = NO;
    
    return _coolFontButton;
    
}
- (UIButton *)keyboardButton
{
    if (!_keyboardButton) {
        _keyboardButton = [self buttonAllocWithAction:@selector(topToolViewbuttonClicked:)];
        _keyboardButton.tag = 3;
        _keyboardButton.adjustsImageWhenHighlighted = NO;
    }
    return _keyboardButton;
}
- (UIButton *)settingButton{
    if (_settingButton == nil) {
        _settingButton = [self buttonAllocWithAction:@selector(topToolViewbuttonClicked:)];
        _settingButton.tag = 4;
    }
    return _settingButton;
}
- (UIButton *)dismissBtnOnVC
{
    if (!_dismissBtnOnVC)
    {
        _dismissBtnOnVC = [self buttonAllocWithAction:@selector(topToolViewbuttonClicked:)];
        _dismissBtnOnVC.tag = 5;
    }
    return _dismissBtnOnVC;
}

- (void)topToolViewbuttonClicked:(UIButton *)button
{
    if (button.tag == 3) {
        // 如果_lastedPressedBtn 不是 self.keyboardButton并且_userHaveDoneSth是NO,满足统计事件要求,发给友盟
        if (_lastPressedButtonTag != 3 && !_userHaveDoneSth) {
            [self userDoNothingButPressed:_lastPressedButtonTag];
        }
    }

    _userHaveDoneSth = NO;
    _lastPressedButtonTag = button.tag;
    
    BOOL same = (button.tag == self.selectedCategoryButton.tag);
    if (same) {// 点击的是同一个按钮
        return;
    }
    else{
        //点击coolFont按钮移除红点
        if(dotImage != nil){
            if(button.tag == 2){
                NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
                if([userDefaults objectForKey:RED_DOT_CLICKED] == nil){
                    [dotImage removeFromSuperview];
                    dotImage=nil;
                    [CKSaveTools saveString:@"redDotClick" forKey:RED_DOT_CLICKED inRegion:CKSaveRegionExtensionKeyboard];
                }
            }
        }
        button.selected = YES;
        self.selectedCategoryButton.selected = NO;
        self.selectedCategoryButton = button;
        
        if ([self.topViewDelegate respondsToSelector:@selector(topViewDelegateButtonClicked:sameButton:)]) {
            [self.topViewDelegate topViewDelegateButtonClicked:button sameButton:same];
        }
    }
}
@end
