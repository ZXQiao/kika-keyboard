//
//  CKSettingModel.m
//  Emoji Keyboard
//
//  Created by 新美 on 15/7/20.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKSettingModel.h"

@implementation CKSettingModel

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dict];
        NSUserDefaults * userInfo = [NSUserDefaults standardUserDefaults];
        if ([dict[@"title"] isEqualToString:@"Prediction"]) {
            if ([[userInfo objectForKey:@"UserClickPredict"] isEqualToString:@"UserClickPredict"]) {
                self.userSelected = [userInfo integerForKey:@"PredictSetting"];
            }
            else{ // 此时为空,默认应该是允许预测的
                self.userSelected = YES;
                [userInfo setInteger:YES forKey:@"PredictSetting"];
                [userInfo setObject:@"UserClickPredict" forKey:@"UserClickPredict"];
                [userInfo synchronize];
            }
        }
        else if([dict[@"title"] isEqualToString:@"Correction"])
        {
            if ([[userInfo objectForKey:@"UserClickAutoCorrect"] isEqualToString:@"UserClickAutoCorrect"]) {
                self.userSelected = [userInfo boolForKey:@"AutoCorrectSetting"];
            }
            else{// 此时为空,默认应该是允许纠错的
                self.userSelected = YES;
                [userInfo setBool:YES forKey:@"AutoCorrectSetting"];
                [userInfo setObject:@"UserClickAutoCorrect" forKey:@"UserClickAutoCorrect"];
                [userInfo synchronize];
            }
        }
        else if([dict[@"title"] isEqualToString:@"More"])
        {
            
        }
    }
    return self;
}

+ (NSArray *)settingModelArray
{
    NSArray * soureArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settingViewImage.plist" ofType:nil]];
    NSMutableArray * modelArray = [NSMutableArray array];
    
    for (NSDictionary * dict in soureArray) {
        CKSettingModel * settingModel = [[CKSettingModel alloc] initWithDict:dict];
        [modelArray addObject:settingModel];
    }
    
    return (NSArray *)modelArray;
}
@end
