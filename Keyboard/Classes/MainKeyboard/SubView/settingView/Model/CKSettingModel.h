//
//  CKSettingModel.h
//  Emoji Keyboard
//
//  Created by 新美 on 15/7/20.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKSettingModel : NSObject
@property (nonatomic, copy) NSString * normalImage;
@property (nonatomic, copy) NSString * selectedImage;
//@property (nonatomic, copy) NSString * normalBackImage;
//@property (nonatomic, copy) NSString * selectedBackImage;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, assign) BOOL userSelected;


+ (NSArray *)settingModelArray;

@end
