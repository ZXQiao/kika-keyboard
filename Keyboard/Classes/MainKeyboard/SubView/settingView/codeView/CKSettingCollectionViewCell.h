//
//  CKSettingCollectionViewCell.h
//  Emoji Keyboard
//
//  Created by 新美 on 15/7/20.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CKSettingModel;
@interface CKSettingCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) CKSettingModel * settingModel;
@property (nonatomic, strong) UIButton * settingButton;
@property (nonatomic, strong) UILabel * settingLabel;
// 因为button会将Cell的点击事件拦截,所以用一个View遮盖button
@property (nonatomic, strong) UIView * buttonCoverView;
@end
