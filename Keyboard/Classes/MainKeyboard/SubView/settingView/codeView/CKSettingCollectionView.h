//
//  CKSettingCollectionView.h
//  Emoji Keyboard
//
//  Created by 新美 on 15/7/20.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CKSettingCollectionViewDelegate <NSObject>

-(void)userClickedAutoCollection;


@end

@interface CKSettingCollectionView : UIView
@property (nonatomic,weak) id<CKSettingCollectionViewDelegate> delegate;
@end
