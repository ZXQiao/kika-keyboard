//
//  CKSettingCollectionView.m
//  Emoji Keyboard
//
//  Created by 新美 on 15/7/20.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKSettingCollectionView.h"
#import "CKSettingCollectionViewCell.h"
#import "CKSettingModel.h"

static NSString * const reuseIdentifier = @"CKSettingCollectionViewCell";

@interface CKSettingCollectionView()<UICollectionViewDataSource, UICollectionViewDelegate>
{
    UICollectionView *_collectionView;
    CGFloat collectionCellSize;
    CGFloat collectionCellPadding;
    CGFloat collectionLinePadding;
    CGFloat collectionViewLeftPadding;
    CGFloat collectionViewRightPadding;
    CGFloat collectionViewTopPadding;
    CGFloat collectionViewBottomPadding;
}
@property (nonatomic, strong) NSArray * settingModelArray;

@end
@implementation CKSettingCollectionView
- (NSArray *)settingModelArray
{
    if (_settingModelArray == nil) {
        return [CKSettingModel settingModelArray];
    }
    else
    {
        return _settingModelArray;
    }
}

- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"setting_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * portraitDimensions = dimensions[PHONE_SCREEN_DIRECTION_PORTRAIT];
    NSDictionary * realDimensions;
    CGFloat fullWidth = self.frame.size.width;
    CGFloat fullHeight = self.frame.size.height;
    if ([CKCommonTools screenDirectionIsLand]) {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_LANDSCAPE];
        collectionCellSize = [portraitDimensions[@"collectionCellSize"] floatValue] * [[UIScreen mainScreen] bounds].size.height;
    }
    else {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_PORTRAIT];
        collectionCellSize = [portraitDimensions[@"collectionCellSize"] floatValue] * fullWidth;
    }
    collectionLinePadding = [realDimensions[@"collectionLinePadding"] floatValue] * fullHeight;
    collectionCellPadding = [realDimensions[@"collectionCellPadding"] floatValue] * fullWidth;
    collectionViewLeftPadding = [realDimensions[@"collectionViewLeftPadding"] floatValue] * fullWidth;
    collectionViewRightPadding = [realDimensions[@"collectionViewRightPadding"] floatValue] * fullWidth;
    collectionViewTopPadding = [realDimensions[@"collectionViewTopPadding"] floatValue] * fullHeight;
    collectionViewBottomPadding = [realDimensions[@"collectionViewBottomPadding"] floatValue] * fullHeight;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDimensions];
        UICollectionViewFlowLayout *flowlayout = [[UICollectionViewFlowLayout alloc] init];
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:flowlayout];
        _collectionView = collectionView;
        [self addSubview:_collectionView];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[CKSettingCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
        
        self.backgroundColor = [UIColor clearColor];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.allowsSelection = YES;
        self.userInteractionEnabled = YES;
    }
    return self;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.settingModelArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个UICollectionView展示的内容
-(CKSettingCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CKSettingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CKSettingCollectionViewCell alloc] init];
    }
    cell.settingModel = self.settingModelArray[indexPath.row];
  
    return cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionCellSize, collectionCellSize);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return collectionLinePadding; // 行间距
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return collectionCellPadding;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(collectionViewTopPadding, collectionViewLeftPadding,collectionViewBottomPadding, collectionViewRightPadding - 1); // 上左下右的偏移量
    
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter] postNotificationName:UserHaveDoneSomethingNotification object:nil];
    CKSettingModel * model = self.settingModelArray[indexPath.row];
    if (![model.title isEqualToString:@"More"]) {
        model.userSelected = !model.userSelected;
        
        [_collectionView reloadData];
        
        // 保存到UserDefualts
        if([model.title isEqualToString:@"Prediction"])
        {
            [CKSaveTools saveInteger:model.userSelected forKey:@"PredictSetting" inRegion:CKSaveRegionExtensionKeyboard];
            [CKSaveTools saveString:@"UserClickPredict" forKey:@"UserClickPredict" inRegion:CKSaveRegionExtensionKeyboard];
        }
        else if ([model.title isEqualToString:@"Correction"])
        {
            [CKSaveTools saveInteger:model.userSelected forKey:@"AutoCorrectSetting" inRegion:CKSaveRegionExtensionKeyboard];
            [CKSaveTools saveString:@"UserClickAutoCorrect" forKey:@"UserClickAutoCorrect" inRegion:CKSaveRegionExtensionKeyboard];
            if ([self.delegate respondsToSelector:@selector(userClickedAutoCollection)]) {
                [self.delegate userClickedAutoCollection];
            }
        }
    } else { // 跳转
        [CKSaveTools saveClickWithEventType:KB_Jump_To_MainApp_Click key:@"Setting"];
        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                           action:KB_Jump_To_MainApp_Click
                                            label:@"Setting"];
            NSString *msg = [NSString stringWithFormat:@"Open\n%@ App\nfor more!",APP_NAME];
        [CKCommonTools showInfoByFittingParentView:self msg:msg];
        
//        UIResponder * responder = self;
//        while ((responder = [responder nextResponder]) != nil) {
//            if ([responder respondsToSelector:@selector(openURL:)] == YES) {
//                [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:[NSString stringWithFormat: @"%@.setting", APP_ROOT_URL]]];
//            }
//        }
    }
}

@end
