//
//  CKSettingCollectionViewCell.m
//  Emoji Keyboard
//
//  Created by 新美 on 15/7/20.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKSettingCollectionViewCell.h"
#import "CKSettingModel.h"
#import "CKKeyboardUImanger.h"

@interface CKSettingCollectionViewCell()
{
    UIButton * _settingButton;
    UILabel * _settingLabel;
    UIView  * _buttonCoverView;

    UIColor * _highLightColor;
    UIColor * _normalColor;
    UIColor * _fontColor;

    CGFloat collectionCellPicSize;
    CGFloat collectionCellPicTextPadding;
}
@end

@implementation CKSettingCollectionViewCell
- (void)initDimensions
{
    CGFloat fullSize = 0;
    if ([CKCommonTools screenDirectionIsLand]) {
        fullSize = self.frame.size.width;
    }
    else {
        fullSize = self.frame.size.height;
    }
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"setting_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * portraitDimensions = dimensions[PHONE_SCREEN_DIRECTION_PORTRAIT];
    collectionCellPicSize = [portraitDimensions[@"collectionCellPicSize"] floatValue] * fullSize;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDimensions];

        _settingButton = [[UIButton alloc] init];
        [self.contentView addSubview:_settingButton];

        _settingLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_settingLabel];
        _settingButton.adjustsImageWhenDisabled = NO;
        
        _buttonCoverView = [[UIView alloc] init];
        [self.contentView addSubview:_buttonCoverView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat w = self.frame.size.width;
    CGFloat h = self.frame.size.height;

    _settingButton.frame = CGRectMake((w - collectionCellPicSize) / 2, 0, collectionCellPicSize, collectionCellPicSize);
    _settingLabel.frame = CGRectMake(0, collectionCellPicSize, w, h - collectionCellPicSize);
    _buttonCoverView.frame = _settingButton.frame;
}

- (void)setSettingModel:(CKSettingModel *)settingModel
{
    _settingModel = settingModel;
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    NSString * fontName;
    if (nowUseTheme) {
        _normalColor = [UIColor colorFromHexString:nowUseTheme.setting_icon_normal];
        _highLightColor = [UIColor colorFromHexString:nowUseTheme.setting_icon_highlight];
        _fontColor = [UIColor colorFromHexString:nowUseTheme.setting_fontcolor];
        fontName = nowUseTheme.fontName;
    }
    else{
        _normalColor = [UIColor colorFromHexString:Default_Setting_Normal_Color];
        _highLightColor = [UIColor colorFromHexString:Default_Setting_Selected_Color];
        _fontColor = [UIColor colorFromHexString:Default_Setting_Font_Color];
        fontName = Default_FontName;
    }
    if (![fontName isEqualToString:@"LithosPro-Black"] && ![fontName isEqualToString:@"Chalkboard SE"]) {
        fontName = buttonDefaultFontName;
    }

    UIImage * OffImage =[UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:settingModel.normalImage ofType:nil]]];
    UIImage* OnImage =[UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:settingModel.selectedImage ofType:nil]]];
    
    if (![settingModel.title isEqualToString:@"More"]) {
        [_settingButton setImage:[OffImage imageWithTintColor:_normalColor] forState:UIControlStateNormal];
        [_settingButton setImage:[OnImage imageWithTintColor:_highLightColor] forState:UIControlStateSelected];
        _settingLabel.textColor = settingModel.userSelected ? _highLightColor : _normalColor;
    }
    else{  
        _settingLabel.textColor =  _normalColor;
        [_settingButton setImage:[OffImage imageWithTintColor:_normalColor] forState:UIControlStateNormal];
        [_settingButton setImage:[OnImage imageWithTintColor:_highLightColor] forState:UIControlStateHighlighted];
    }
    
    _settingLabel.text = settingModel.title;
    _settingLabel.textAlignment = NSTextAlignmentCenter;
    CGFloat defaultFontSize = [[CKKeyboardUImanger shareUImanger] getFunctionFontSize:fontName];
    CKCurrentiPhoneModel currentiPhoneModel = [CKKeyboardUImanger shareUImanger].currentiPhoneModel;
    if(currentiPhoneModel == CKCurrentiPhoneModeliPhone4 || currentiPhoneModel == CKCurrentiPhoneModeliPhone5) {
        if (![fontName isEqualToString:@"LithosPro-Black"]) {
            defaultFontSize = 14.0f;
        }
    }
    _settingLabel.font = [UIFont fontWithName:fontName size:defaultFontSize];
    _settingButton.selected = settingModel.userSelected;
}
@end
