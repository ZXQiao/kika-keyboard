//
//  KeyboardViewController.m
//  iKeyboard_mine
//
//  Created by 张赛 on 14-9-7.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "CKKeyboardWordManger.h"
#import "CKKeyboardUImanger.h"
#import "CKCoolFonts.h"
#import <objc/runtime.h>
#import "CKGuideView.h"

@interface KeyboardViewController ()<IKeyoardEnglishDelegate,UITextViewDelegate,UITextFieldDelegate,UIApplicationDelegate,UIGestureRecognizerDelegate,CKEmojiViewDelegate,CKKeyboardTopViewDelegate,CKPredictionViewDelegate,CKThemeCollectionViewDelegate,CKPredictionKingDelegate, CKKeyboardMemeViewDelegate,CKPredictOperationDelegate,CoolFontViewDelegate,CKSettingCollectionViewDelegate,CKGuideViewDelegate>

{
    CKEmojiView * emojiView;
    NSString * lastEmojiResourceType;
    SoundManager * _soundMgr;
    NSString * _soundName;
    CKSoundSType _currentSoundsType;
    NSArray * _musicalArray;
    int _musicalIndex ;
    BOOL _FirstTimeIn;
    NSString * _beforeInputInTextWillChange;
    NSString * _afterInputInTextWillChange;
//    NSUInteger _KeyboardType;
    CKGuideView * _guideView;
    UIView * _backView;
}

@property (nonatomic, assign) BOOL isInputSymbols;
@property (nonatomic, assign) BOOL predictionWordClicked;

@property (nonatomic, strong) UIImageView * rootView;// 底层视图,用来显示background
@property (nonatomic, strong) CKMainKeyboard * keyboard;// 字母键盘
@property (nonatomic, strong) CKKeyboardTopView * topToolView;// 上边按钮工具栏
@property (nonatomic, strong) CKSettingCollectionView * settingView;
@property (nonatomic, strong) CKKeyboardMemeView * memeView;

@property (nonatomic, strong) NSTimer * spaceTimer;
@property (nonatomic, assign) BOOL isSpaceTimerPhase;

@property (nonatomic, strong) NSTimer * themeChangeTimer;
@property (nonatomic, strong) NSTimer * coolfontChangeTimer;

@property (nonatomic, strong) NSTimer * deleteTimer;// 删除按钮的计时器
@property (nonatomic,assign) CGFloat deleteDeltaTime;
@property (nonatomic, assign) int deleteCount; // 记录delete方法执行了几次

/*---------------------------emojiArt---------------------------------*/
@property (nonatomic, assign) CGFloat currentViewWidth;

/*----------------------------键盘换肤-------------------------------*/
@property (nonatomic, strong) CKThemeCollectionView * themeView;
@property (nonatomic, strong) UIButton * coverButton;

/*---------------------------coolFont--------------------------------*/
@property (nonatomic, strong) CKCoolFontView * coolFontView;
@property (nonatomic,strong) NSLayoutConstraint * heightConstraint;
@property (nonatomic,assign) CGFloat heightKeyboard;
@property (nonatomic,assign) BOOL isLandscape;
@property (nonatomic,strong) CKKeyboardWordManger * wordManger;
@property (nonatomic,strong) CKKeyboardUImanger * UIManger;
@property (nonatomic,strong) CKPredictionKing * predictionKing;

// 输入预测
@property (nonatomic,strong) CKPredictionView * predictionView;
/** NSOperation操作队列 */
@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic,strong) NSBlockOperation * currentPredictionOperation;// 预测的操作
@property (nonatomic,strong) NSBlockOperation * currentUpdateUIOperation;// 更新UI的操作
@property (nonatomic, strong) CKPredictOperation * predictOperation;



/**
 *  全局变量来记录当前输入的从上一个空格到光标之前的字符----后面注释为标签字符
 *  在键盘弹出的时候就要初始化
 *  输入空格,回车,数字,符号要清零
 *  delete时清零
 */
@property (nonatomic,strong) NSString * currentInputStr;
@property (nonatomic,assign) CKPreviewCapsType previewCapsType; // preview栏上的单词需要大写;
@property (nonatomic,assign) BOOL userAllowPredict;
@property (nonatomic,assign) BOOL userAllowAutoCorrect;
@property (nonatomic,strong) CKUserPreferenceManager * userPreferenceMgr;
@property (nonatomic,strong) NSDictionary * selectedCoolFontDict;
@property (nonatomic, strong) NSDate * launchDate;

@end


@implementation KeyboardViewController


- (void)testUmeng
{
    for (int i = 0; i < 100; i ++) {
        NSString * st = [NSString stringWithFormat:@"张赛测试:键盘弹起%d次",i];
        [CKSaveTools saveClickWithEventType:KB_Lanch key:st];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        USER_PREFERENCES_MGR = [CKUserPreferenceManager shareUserPreferenceManager];
        UIMANGER = [CKKeyboardUImanger shareUImanger]; // UI管理
        [UIMANGER heightSettings]; // 高度
    }
    return self;
}



- (void)sendInfoWhenLaunch
{
    
    [CKSaveTools saveClickWithEventType:KB_Lanch key:KB_Lanch];
    [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_Lanch];
    if (!self.userAllowPredict) {
        [CKSaveTools saveClickWithEventType:KB_Lanch key:@"userAllowPredict"];
    }
    if (!self.userAllowAutoCorrect) {
        [CKSaveTools saveClickWithEventType:KB_Lanch key:@"userAllowAutoCorrect"];
    }
    if ([CKCommonTools isOpenAccessGranted]) {
        [CKSaveTools saveClickWithEventType:KB_Lanch key:@"fullAccessGranted"];
        NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
        CKFullAccessType fullAccessType = [sharedDefaults integerForKey:Full_Access_Type];
        if (fullAccessType == CKFullAccessTypeTutorial) {
            [CKSaveTools saveClickWithEventType:KB_Lanch key:@"fullAccessTypeTutorial"];
        }
        else if (fullAccessType == CKFullAccessTypeVideo) {
            [CKSaveTools saveClickWithEventType:KB_Lanch key:@"fullAccessTypeVideo"];
        }
        else if (fullAccessType == CKFullAccessTypeHelp) {
            [CKSaveTools saveClickWithEventType:KB_Lanch key:@"fullAccessTypeHelp"];
        }
    }
    [CKBackend sendMetaDataToBackend];
}

- (void)basicSettings
{
    [self updateUserPreference]; // 偏好设置
    self.predictionKing = [CKPredictionKing sharePrediction]; // 预测工具
    self.predictionKing.delegate = self;
    self.predictionWordClicked = NO;
//    WORDMANGER = [CKKeyboardWordManger shareWordManger]; // 词语管理
        if ([CKCommonTools isOpenAccessGranted]) {
        _soundMgr = [SoundManager sharedManager];
        _soundMgr.allowsBackgroundMusic = YES;
        [_soundMgr prepareToPlay];
        _soundMgr.soundVolume = .003; // 设置声音大小
        [self soundInfo];
    }
}

- (void)soundInfo{
    NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    NSUserDefaults *Defaults = [NSUserDefaults standardUserDefaults];
    int VoiceIndex;
    int MusicIndex;
    //是否首次开启
    BOOL firstin = [Defaults integerForKey:@"FirstInExtention"];
    _FirstTimeIn = firstin;
    if (!_FirstTimeIn) {
        NSString * soundName = @"Sounds off";
        VoiceIndex = 1;
        _soundName = soundName;
        MusicIndex = (int)[Defaults integerForKey:@"MusicIndex"];
        _FirstTimeIn = YES;
        //存入app_group
        [Defaults setInteger:1 forKey:@"FirstInExtention"];
        [sharedDefaults setInteger:VoiceIndex forKey:@"VoiceIndex"];
        [sharedDefaults setInteger:0 forKey:@"MusicIndex"];
        [sharedDefaults setObject:soundName forKey:@"SoundsName"];
    }
    else{
        NSString * soundName = [sharedDefaults objectForKey:@"SoundsName"];
        _soundName = soundName;
        VoiceIndex = (int)[sharedDefaults integerForKey:@"VoiceIndex"];
        MusicIndex = (int)[sharedDefaults integerForKey:@"MusicIndex"];
    }
    if (VoiceIndex == 0 && MusicIndex == 0 ) { // 为空
        _currentSoundsType = CKSoundSTypeNone;
    }
    else{
        if (VoiceIndex) {
            _currentSoundsType = CKSoundSTypeVoice;
        }
        else
        {
            _currentSoundsType = CKSoundSTypeMusic;
            NSError * error;
            NSString * musicString = [[NSString alloc] initWithContentsOfFile:
                                      [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@.txt",_soundName] ofType:nil ]encoding:NSUTF8StringEncoding error:&error];
            NSArray * musicArray = [musicString componentsSeparatedByString:@","];
            _musicalArray = musicArray;
            if (_musicalArray == nil) {
                _musicalArray = [NSArray array];
            }
        }
    }
}
- (void)playSongs{
    if ([CKCommonTools isOpenAccessGranted]) {
        if (_currentSoundsType == CKSoundSTypeVoice) {
            if ([_soundName isEqualToString:@"Sounds off"]) {
                return;
            }
            NSString * bundlePath =[[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:@"Sounds.bundle"];
            NSString * soundPath = [NSString stringWithFormat:@"%@/%@.caf",bundlePath,_soundName];
         
            [[SoundManager sharedManager] playSound:soundPath looping:NO];
        }
        else if (_currentSoundsType == CKSoundSTypeMusic){
            
            if (_musicalIndex == _musicalArray.count) {
                _musicalIndex = 0;
            }
            if (_musicalArray.count == 0) {
                return;
            }
            NSString * musical = _musicalArray[_musicalIndex];
            NSString * currentMusical = [NSString stringWithFormat:@"%@.mp3",musical];
            NSString * bundlePath =[[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:@"Piano.bundle"];
            NSString * musicPath = [NSString stringWithFormat:@"%@/%@",bundlePath,currentMusical];
            [[SoundManager sharedManager] playSound:musicPath looping:NO];
            _musicalIndex ++;
        }
        else{
            return;
        }
    }
    else
    {
        return;
    }
    
    
}
- (void)updateUserPreference{
    self.userAllowPredict = [USER_PREFERENCES_MGR preferenceOFuserPredict];
    self.userAllowAutoCorrect = [USER_PREFERENCES_MGR preferenceOFAutoCorrect];
    
//    self.predictionView.allowAutoCorrect = self.userAllowAutoCorrect;
}


#pragma mark - 初始化方法
#pragma mark viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView * rootView = [[UIImageView alloc] init];
    [self.view addSubview:rootView];
    self.rootView = rootView;
    rootView.userInteractionEnabled = YES;
    rootView.translatesAutoresizingMaskIntoConstraints = NO;
    WORDMANGER = [CKKeyboardWordManger shareWordManger]; // 词语管理

    [self altPressedNumber:nil];
    [self.rootView insertSubview:self.topToolView belowSubview:self.keyboard];
    [self addLongPressRecognize];
    [self changeCurrentImages];
    
    [UIMANGER strangeSettingWithGlobalButton:self.rootView view:self.inputView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self basicSettings];
    [self sendInfoWhenLaunch];
    BOOL island = [CKCommonTools screenDirectionIsLand];
    CGFloat _expandedHeight = island ? UIMANGER.landscapeHeightTotal:UIMANGER.portraitHeightTotal;
    NSLayoutConstraint * h =
    [NSLayoutConstraint constraintWithItem: self.view
                                 attribute: NSLayoutAttributeHeight
                                 relatedBy: NSLayoutRelationEqual
                                    toItem: nil
                                 attribute: NSLayoutAttributeNotAnAttribute
                                multiplier: 0.0
                                  constant: _expandedHeight];
    [self.inputView addConstraint:h];
    self.heightConstraint = h;
}

- (void)viewDidAppear:(BOOL)animated
{
    [USER_PREFERENCES_MGR updateLanguage];
    HEADSTR = @"";
    [super viewDidAppear:animated];
    self.keyboard.returnType = (long)self.textDocumentProxy.returnKeyType;
    
    // 当FullAccess打开的时候,取出键盘里存储的主题和AppGroup里的对比,如果两个地方不同的话,要让group里的追随键盘里的
    // 放在viewDidAppear里做这件事是为了不给键盘初始化增加负担
    if ([CKCommonTools isOpenAccessGranted]) {
        [CKMainThemeModel sysThemeModelInKeyboardAndGroup];
    }
//    CKLog(@"========viewDidAppear========");
    
    
    // 把这些东西挪到ViewdidAppear里,提高弹起效率
    
    if (!self.selectedCoolFontDict) {
        int selectedCoolFontIndex = WORDMANGER.selectedCoolFontIndex;
        NSArray * coolFontNameArray = [CKCoolFonts shareCoolFonts].coolFontNameArray;
        NSString * coolFont;
        if (selectedCoolFontIndex < [coolFontNameArray count]) {
            coolFont = coolFontNameArray[selectedCoolFontIndex];
        }
        else {
            coolFont = @"-Normal Font-";
            [CKSaveTools saveString:@"0" forKey:@"CoolFontSelectedIndex" inRegion:CKSaveRegionExtensionKeyboard];
        }
        self.selectedCoolFontDict = [[CKCoolFonts shareCoolFonts] selectedCoolFontDictWithCoolFontName:coolFont];
    }
    [CKTracker sharedTraker];
}

- (CGRect)currentKeyboardFrame
{
    CGFloat y = 0,h = 0;
    if ([CKCommonTools screenDirectionIsLand]) {
        h = UIMANGER.landscapeHeightKeyboard;
        y = UIMANGER.landscapeHeightTotal - h;
    }
    else{
        h = UIMANGER.portraitHeightKeyboard;
        y = UIMANGER.portraitHeightTotal - h;
    }
    return CGRectMake(0,y, self.rootView.frame.size.width,h);
}

- (CGRect)currentTopToolViewFame
{
    CGFloat h = 0;
    if ([CKCommonTools screenDirectionIsLand]) {
        h = UIMANGER.landscapeHeightTotal - UIMANGER.landscapeHeightKeyboard;
    }
    else{
        h = UIMANGER.portraitHeightTotal - UIMANGER.portraitHeightKeyboard;
    }
    return CGRectMake(0,0, self.rootView.frame.size.width,h);
}

-(CGRect)currentGuideFram{
    CGFloat w = 0,h = 0;
    if ( [CKCommonTools screenDirectionIsLand] ) {
        //横屏
        h = UIMANGER.landscapeHeightKeyboard;
    }else{
        //竖屏
        h = 0.8*UIMANGER.portraitHeightKeyboard;
    }
    w = h * 1.5;
    return CGRectMake(0,0,w,h);
    
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if (self.view.frame.size.width == 0 || self.view.frame.size.height == 0)return;
    BOOL island = [CKCommonTools screenDirectionIsLand];
    self.heightConstraint.constant = island ? UIMANGER.landscapeHeightTotal:UIMANGER.portraitHeightTotal;
    
    self.keyboard.frame = [self currentKeyboardFrame];
    self.topToolView.frame = [self currentTopToolViewFame];
    self.predictionView.frame = [self currentTopToolViewFame];
    _backView.frame = [self currentKeyboardFrame];
    _guideView.bounds = [self currentGuideFram];
    _guideView.center = self.keyboard.center;
    if (self.currentViewWidth != _topToolView.frame.size.width) {
        // 如果有旋转屏幕的操作的话,要把定时器置空
        [self.deleteTimer invalidate];
        self.deleteTimer = nil;
        self.currentViewWidth = _topToolView.frame.size.width;
        self.topToolView.selectedCategoryButton.selected = !self.topToolView.selectedCategoryButton.selected;
        self.topToolView.selectedCategoryButton = nil;
#if (APP_VARIANT_NAME == APP_VARIANT_NEWMEMOJI)
        if (!emojiView) {
            [self emojiPressedEnglish:nil];
            return;
        }
        if (emojiView && !self.keyboard) {
            [self backToBoard];
        }
#else
        if (!self.keyboard) {
            [self backToBoard];
        }
#endif
    }
}
#pragma mark - 上工具栏
- (CKKeyboardTopView *)topToolView
{
    if (_topToolView == nil) {
        self.rootView.frame = self.view.bounds;
        CGFloat h = self.heightKeyboard ;
        CGFloat y = self.rootView.frame.size.height - h;
        CGFloat w = self.rootView.frame.size.width;
        _topToolView = [[CKKeyboardTopView alloc] initWithFrame:CGRectMake(0, 0, w, y)];
        _topToolView.topViewDelegate = self;
    }
    return _topToolView;
}

- (CKPredictionView *)predictionView
{
    if (_predictionView == nil && self.userAllowPredict) {
        _predictionView = [[CKPredictionView alloc] initWithFrame:self.topToolView.frame];
        _predictionView.predictionDelegate = self;
    }
    return _predictionView;
}

- (void)memeView:(UIButton *)button same:(BOOL)same
{
    if (self.userAllowPredict) {
        [[CKPredictionKing sharePrediction] clearAssList];
    }
    if (same) {
        [self backToBoard];
        return;
    }
    [self removeLeftView];
    [self clearView];
    CKKeyboardMemeView * memeView = [[CKKeyboardMemeView alloc] initWithFrame:[self currentKeyboardFrame]];
    [self.rootView addSubview:memeView];
    self.memeView = memeView;
    [memeView.deleteButton addGestureRecognizer:self.longPressGestureRecognizer];
    memeView.delegate = self;
}


- (void)showThemeView:(UIButton *)button same:(BOOL)same
{
    if (self.userAllowPredict) {
        [[CKPredictionKing sharePrediction] clearAssList];
    }
    if (same) {
        [self backToBoard];
        return;
    }
    [self removeLeftView];
    [self clearView];
    CKThemeCollectionView * themeView = [[CKThemeCollectionView alloc] initWithFrame:[self currentKeyboardFrame]];
    [self.rootView addSubview:themeView];
    self.themeView = themeView;
    themeView.delegate = self;
}

// topViewDelegate same是用来标记是不是同一个按钮
- (void)topViewDelegateButtonClicked:(UIButton *)button sameButton:(BOOL)same
{   [CKSaveTools saveEventForEmojiMeme];
    int buttonTAG = (int)button.tag;
    switch (buttonTAG) {
        case 1:
            [CKSaveTools saveClickWithEventType:KB_Table_Click key:@"Theme"];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_Table_Click label:@"Theme"];
            [self showThemeView:button same:NO];
            break;
        case 2:
            [CKSaveTools saveClickWithEventType:KB_Table_Click key:@"CoolFont"];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_Table_Click label:@"CoolFont"];
            [self showCoolFontView:button same:NO];
            break;
        case 3:
            [self backToBoard];
            [self updateUserPreference]; // 偏好设置
            break;
        case 4:
            [CKSaveTools saveClickWithEventType:KB_Table_Click key:@"Setting"];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_Table_Click label:@"Setting"];
            [self showSettingView:button same:same];
            [self updateUserPreference]; // 偏好设置
            break;
        case 5:
            [self dismissKeyboard];
            break;
        case 6: // 显示meme
            [CKSaveTools saveClickWithEventType:KB_Table_Click key:@"Meme"];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_Table_Click label:@"Meme"];
            [CKSaveTools saveInteger:SelectedMeme forKey:SELECT_INPUT inRegion:CKSaveRegionExtensionKeyboard];
            [self memeView:button same:NO];
            break;
        default:
            break;
    }
    if (self.textDocumentProxy.autocorrectionType == UITextAutocorrectionTypeNo) {
        [self.predictionView ClearButtonArray];
        self.predictionView.hidden = YES;
        self.topToolView.hidden = NO;
    }
}

- (void)memeBottomBarButtonClicked:(UIButton *)button
{
    switch (button.tag) {
        case 0: // ABC
            [CKSaveTools saveEventForEmojiMeme];
            [self backToBoard];
            break;
        case 4: // backspace
            [self.textDocumentProxy deleteBackward];
            break;
        default:
            break;
    }
}

- (void)showSettingView:(UIButton *)button same:(BOOL)same
{
    if (self.userAllowPredict) {
        [[CKPredictionKing sharePrediction] clearAssList];
    }
    if (same) {
        [self backToBoard];
        return;
    }
    [self removeLeftView];
    [self clearView];
    
    CKSettingCollectionView * settingView = [[CKSettingCollectionView alloc] initWithFrame:[self currentKeyboardFrame]];
    self.settingView = settingView;
    self.settingView.delegate = self;
    [self.rootView addSubview:settingView];
}

- (void)userClickedAutoCollection{
    NSUserDefaults * userInfo = [NSUserDefaults standardUserDefaults];
    self.predictionView.allowAutoCorrect = [userInfo boolForKey:@"AutoCorrectSetting"];
}

- (void)changeCurrentImages
{
    [UIMANGER changeCurrentImagesWithtopToolView:self.topToolView predictionView:self.predictionView rootView:self.rootView];
}

#pragma mark 添加长按识别
- (void)addLongPressRecognize
{
    // 添加长按手势监听
    self.longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGestures:)];
    self.longPressGestureRecognizer.delegate = self;
    self.longPressGestureRecognizer.numberOfTouchesRequired = 1;
    self.longPressGestureRecognizer.allowableMovement = 100.0f;
}


#pragma mark ------输入特殊字符的处理
- (void)predictKingFindSybomlsInWordShouldInput:(NSString *)inputWord shouldPredictWith:(NSString *)predictWorld sourceString:(NSString *)sourceString
{
//    if (self.previewCapsType == CKPreviewCapsTypeAllUpper ) { // 如果锁定大写
//        inputWord = inputWord.uppercaseString;
//    }
//    else{ // 判断是否需要首字母大写
//        BOOL shouldCaps = [WORDMANGER shouldCapsWithPreStr:STR_BEFORE afterStr:STR_AFTER];
//        if (shouldCaps) {
//            inputWord = inputWord.capitalizedString;
//        }
//        else{
//            inputWord = inputWord.lowercaseString;
//        }
//    }
    
    [self predictWithHeadStr:predictWorld atOneTime:YES];
    
}
#pragma mark ----------------输入预测------------
- (NSOperationQueue *)queue
{
    if (!_queue) _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 1;
    return _queue;
}

#pragma predictionOperation Delegate
- (void)predictOperation:(CKPredictOperation *)currentOperation ReturnPredictWordArray:(NSMutableArray *)predictResultArray originPredictWordArray:(NSMutableArray *)originPredictResultArray emojiArray:(NSMutableArray *)emojiArray sameWord:(BOOL)same hasPrefer:(BOOL)hasPrefer
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self checkPreviewWordCapsWithHeadStr];
        //  在这里要判断一下Operation的predictIndex
        NSString *tempStr = [NSString lastStrWithStr:STR_BEFORE];
        NSString * nowLastString = [NSString lastStrAfterSymbolsWithStr:tempStr dict:self.selectedCoolFontDict oppositeDict:[CKCoolFonts shareCoolFonts].coolFontDict];
        if (!nowLastString) {
            //如果现在的最后单词为空,肯定不需要上一个预测了,故这个Operation作废
            return ;
        }
        else{
            //如果不为空,但是长度跟predictIndex不一样,证明这个Operation也应该作废
            if (currentOperation.predictIndex != [NSString totalCharacterCount:nowLastString]) {
                [currentOperation cancel];
                return;
            }
        }
        [self.predictionView predictionArrayHasSameWord:same predictWithCharacter:YES];
        [self.predictionView.emojiPredictArray removeAllObjects];
        [self.predictionView.emojiPredictArray addObjectsFromArray:emojiArray];
        [self.predictionView setPredictionArray:predictResultArray originPredictResultArray:originPredictResultArray hasPrefer:hasPrefer];
        if (predictResultArray.count == 0) {
            self.predictionView.hidden = YES;
            self.topToolView.hidden = NO;
        }
        else{
            self.predictionView.hidden = NO;
            if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"]) {
                self.topToolView.hidden = YES;
            }
        }
        if (self.keyboard == nil) {
            if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"]) {
                self.topToolView.hidden = YES;
            }
        }
        //#ifdef CKRunPerformenceTest
        //        double duration = TOCK;
        //        [CKPerformence GetExcuteTime:@"UpdateUITime" duration:duration];
        //#endif
    }];
}


- (void)predictWithHeadStr:(NSString *)headStr atOneTime:(BOOL)oneTime
{
    if (self.predictOperation) {
        [self.predictOperation cancel];
    }
    CKPredictOperation * predictOperation = [[CKPredictOperation alloc] init];
    self.predictOperation = predictOperation;
//    predictOperation.oppositeCoolFontDict = WORDMANGER.oppositeCoolFontDict;
    predictOperation.oppositeCoolFontDict = [CKCoolFonts shareCoolFonts].coolFontDict;
    predictOperation.previewCapsType = self.previewCapsType;
    predictOperation.delegate = self;
    if (oneTime) {
        predictOperation.predictStyle = CKPredictStyleDelete;
        predictOperation.sourceStr = headStr;
    }
    else
    {
        predictOperation.predictStyle = CKPredictStyleNormal;
        predictOperation.sourceStr = headStr;//STR_BEFORE;
    }
    [self.queue addOperation:predictOperation];
}

- (void)checkPreviewWordCapsWithHeadStr
{
    if (self.keyboard.shiftStatus == CKShiftStatusCapitalized) {// 首字母大写
        self.previewCapsType = CKPreviewCapsTypeCaps;
    }
    else if (self.keyboard.shiftStatus == CKShiftStatusAllUpper) { // 全部大写
        self.previewCapsType = CKPreviewCapsTypeAllUpper;
    }
    else if (self.keyboard.shiftStatus == CKShiftStatusLongPressedLower) { // 全部大写
        self.previewCapsType = CKShiftStatusAllLower;
    }
    else if (self.keyboard.shiftStatus == CKShiftStatusLongPressedUpper) { // 全部大写
        self.previewCapsType = CKShiftStatusAllUpper;
    }
    // 2.如果为小写,则要判断输入HeadStr之前是什么状态,而在输入headerStr之前的caps状态已做了记录
}

// 设置preview栏目上的单词是否首字母大写
- (void)setPreviewCapsType:(CKPreviewCapsType)previewCapsType
{
    if (self.userAllowPredict) {
        _previewCapsType = previewCapsType;
        
//        if (previewCapsType == CKPreviewCapsTypeLongPressedLower)
//            previewCapsType = CKPreviewCapsTypeAllLower;
//        else if(previewCapsType == CKPreviewCapsTypeLongPressedUpper)
//            previewCapsType = CKPreviewCapsTypeAllUpper;
    }
}

// 根据前一个单词查询下一个单词
- (void)receiveNextWordWithBeforeWord:(NSString *)beforeWord
{
    // 1.先断掉上一个Operation
    [self.currentUpdateUIOperation cancel];
    [self.currentPredictionOperation cancel];
    [[NSOperationQueue mainQueue] cancelAllOperations];
    [self.queue cancelAllOperations];
    self.currentPredictionOperation = nil;
    self.currentUpdateUIOperation = nil;
    // 2.开启一个新的线程
    // 查询操作
    __block NSMutableArray * predictResultArray = [NSMutableArray array];
    NSBlockOperation *predictOP = [NSBlockOperation blockOperationWithBlock:^{
        if (beforeWord.length <= 20) {
            predictResultArray = (NSMutableArray *)[[CKPredictionKing sharePrediction] predictionNextWordArrayFromBeforeWord:beforeWord];
        }
        HEADSTR = @"";
    }];
    // 更新UI的操作
    NSBlockOperation * updateUI = [NSBlockOperation blockOperationWithBlock:^{
        //#ifdef CKRunPerformenceTest
        //        TICK;
        //#endif
        [self.predictionView predictionArrayHasSameWord:YES predictWithCharacter:NO];
        [self.predictionView.emojiPredictArray removeAllObjects];
        [self.predictionView setPredictionArray:predictResultArray originPredictResultArray:predictResultArray hasPrefer:NO];
        if (predictResultArray.count == 0) {
            self.predictionView.hidden = YES;
            self.topToolView.hidden = NO;
        }
        else{
            self.predictionView.hidden = NO;
            if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"]) {
                self.topToolView.hidden = YES;
            }
        }
        //#ifdef CKRunPerformenceTest
        //        double duration = TOCK;
        //        [CKPerformence GetExcuteTime:@"NextWordUpdateUITime" duration:duration];
        //#endif
    }];
    [updateUI addDependency:predictOP]; // 后一个操作要添加依赖,等上一个操作完成才可以执行
    [self.queue addOperation:predictOP];
    self.currentPredictionOperation = predictOP;
    self.currentUpdateUIOperation = updateUI;
    // 主线程更新UI
    [[NSOperationQueue mainQueue] addOperation:updateUI];
}

-(NSString*)InputUserChosenWord:(CKPredictButton *)button sameWords:(BOOL)same firstButton:(BOOL)firsrt
{
    //    [self playSongs];
    if (button.titleLabel.text == nil) {
        return nil;
    }
    int count = 0;
#warning --todo----这里这样去判断是因为,用户输入了一个Emoji,然后又输入了一个错词,当点击空格时,会将Emoji删掉 \
    而这不是我们想要的,所以这里用HEADSTR去判断,删除掉的是用户真正输入的单词,但是HEADSTR又不是百分之百可靠的,可能会有问题
    
    if (HEADSTR.length) {
        count = [NSString totalCharacterCount:HEADSTR];
    }
    else{
        count = [NSString totalCharacterCount:[NSString GetLastInputStr:STR_BEFORE]];
    }
    

    
    [self deleteWithCount:count];
    NSString * sourceWord = button.sourceWord; // 原始的单词包含了大小写以及空格
    NSString * buttonWord = button.currentTitle;
    NSString * inputWord = buttonWord;
    if (firsrt && !same && inputWord.length > 1) { // 如果是第一个按钮,并且没有去除相同单词,要去除引号
        inputWord = [inputWord stringByReplacingCharactersInRange:NSMakeRange(inputWord.length - 1, 1) withString:@""];
        inputWord = [inputWord stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
    }
    
    if ([sourceWord hasSuffix:@" "]) { // 有空格,去除空格,用来查询
        sourceWord = [sourceWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    if (![inputWord hasSuffix:@" "]) { // 如果按键上的字符没有空格,要加上空格
        inputWord = [inputWord stringByAppendingString:@" "];
    }
    NSString * insertStr = @"";

    if (WORDMANGER.selectedCoolFontIndex) {
        insertStr = [NSString translateNormalFontString:inputWord ToCoolFontStrWithDict:self.selectedCoolFontDict];
    }
    else{
        insertStr = inputWord;
    }
    [TXT_M insertText:insertStr];
    return sourceWord;
}

#pragma mark ---------------预测栏代理方法
// 预测单词按钮点击了
- (void)predictionViewWordButtonClicked:(CKPredictButton *)button sameWords:(BOOL)same firstButton:(BOOL)firsrt
{
    [self playSongs];
    [self.keyboard backToCharacter];
    // 1.输入用户选中的词语
    NSString* sourceWord = [self InputUserChosenWord:button sameWords:same firstButton:firsrt];
    if (sourceWord == nil) {
        return;
    }
    self.predictionWordClicked = YES;
    // 2.清空标签字符串
    HEADSTR = @"";
    // 设置preview栏的大小写
    if (self.keyboard.shiftStatus == CKShiftStatusAllUpper) {// 如果是锁定大写,则继续锁定
        self.previewCapsType = CKPreviewCapsTypeAllUpper;
    }
    else if (self.keyboard.shiftStatus == CKShiftStatusLongPressedUpper) { // 全部大写
        self.previewCapsType = CKPreviewCapsTypeAllUpper;
    }
    else{
        self.previewCapsType = CKPreviewCapsTypeAllLower;
    }
    [self receiveNextWordWithBeforeWord:button.originWord];
}

- (void)predictionViewWordButtonClickedFromSpace:(CKPredictButton *)button sameWords:(BOOL)same firstButton:(BOOL)firsrt
{
    [self predictionViewWordButtonClicked:button sameWords:same firstButton:firsrt];
    self.predictionWordClicked = NO;
}

- (void)deleteWithCount:(int)count
{
    for (int i = 0 ; i < count; i ++){[TXT_M deleteBackward];}
}
- (void)predictionViewEmojiButtonClicked:(UIButton *)emojiButton
{
    //    [self playSongs];
    int count = (int)HEADSTR.length;
    [self deleteWithCount:count];
    HEADSTR = @"";
    [TXT_M insertText:emojiButton.titleLabel.text];
    [[CKPredictionKing sharePrediction] clearAssList];
    [self.predictionView ClearButtonArray];
    self.predictionView.hidden = YES;
    self.topToolView.hidden = NO;
    self.previewCapsType = CKPreviewCapsTypeAllLower;
}


#pragma mark - 长按删除
#pragma mark 长按手势监听方法
-(void)handleLongPressGestures:(UILongPressGestureRecognizer *)paramSender
{
    if (paramSender.state == UIGestureRecognizerStateBegan){
        self.deleteDeltaTime = 0.05f;
        // 开始删除定时
        [self deleteTimerStartWithAction:@selector(keepDelete)];
    }
    if(paramSender.state == UIGestureRecognizerStateEnded)
    {
        [self.deleteTimer invalidate];
        self.deleteTimer = nil;
    }
}

#pragma mark 定时器启动
- (void)deleteTimerStartWithAction:(SEL)action
{    NSTimer * deleteTimer = [NSTimer scheduledTimerWithTimeInterval:self.deleteDeltaTime target:self selector:action userInfo:nil repeats:YES];
    self.deleteTimer = deleteTimer;
}

- (void)spaceTimerEnd
{
    self.isSpaceTimerPhase = NO;
}

- (void)spaceTimerStart
{
    self.isSpaceTimerPhase = YES;
    self.spaceTimer = [NSTimer scheduledTimerWithTimeInterval:1.2f target:self selector:@selector(spaceTimerEnd) userInfo:nil repeats:NO];
}

- (void)themeChangeTimerEnd
{
    [self backToBoard];
    [self changeCurrentImages];
}

- (void)themeChangeTimerStart
{
    self.themeChangeTimer = [NSTimer scheduledTimerWithTimeInterval:0.05f target:self selector:@selector(themeChangeTimerEnd) userInfo:nil repeats:NO];
}

- (void)coolfontChangeTimerEnd
{
    [self backToBoard];
}

- (void)coolfontChangeTimerStart
{
    self.coolfontChangeTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(coolfontChangeTimerEnd) userInfo:nil repeats:NO];
}

#pragma mark 文字删除
- (void)deleteButtonLongPressed:(CKMainButton *)deleteButton gesture:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CKLog(@"%@===timer",self.deleteTimer);
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
        self.deleteDeltaTime = 0.1f;
        // 开始删除定时
        [self deleteTimerStartWithAction:@selector(keepDelete)];
        CKLog(@"ddddddddddddddddddd");
    }
    else if(gestureRecognizer.state == UIGestureRecognizerStateChanged)
    {
        CKLog(@"jdaljfaljfdas;dfa");
    }
    else if(gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        [self.deleteTimer invalidate];
        CKLog(@"invalidate111");
        self.deleteTimer = nil;
        self.deleteCount = 0;
        CKLog(@"endendendendend");
    }

    [CKSaveTools saveClickWithEventType:KB_LongDel_Click key:KB_LongDel_Click];
    
}

#pragma mark shift长按
-(void)shiftButtonLongPressed:(UIButton *)shiftButton gesture:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
        [self shiftButtonChangeWithTag:3];
    }
    if(gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        [self shiftButtonChangeWithTag:1];
    }
}

- (void)shiftButtonChangeWithTag:(int)buttonTag
{
    switch (buttonTag) {
        case 1:
            self.keyboard.shifted = NO;
            self.keyboard.shiftStatus = CKShiftStatusAllLower;
            break;
        case 2:
            self.keyboard.shiftStatus = CKShiftStatusCapitalized;
            self.keyboard.shifted = YES;
            break;
        case 3:
            self.keyboard.shiftStatus = CKShiftStatusAllUpper;
            break;
        default:
            break;
    }
}
- (void)keepDelete
{
    CKLog(@"keepkeepkeepkeepkeepkeepkeepkeepkeep");
    if (STR_BEFORE == nil || [STR_BEFORE isEqualToString:@""]) {
        [self.deleteTimer invalidate];
      [TXT_M deleteBackward];
        CKLog(@"empty++++++++++++++++++++++++++++++empty");
        return;
    }
    
    self.deleteCount ++;
    if (!self.predictionView.hidden) {
        self.predictionView.hidden = YES;
        self.topToolView.hidden = NO;
    }
    if (self.deleteCount >= 20) {
        [self.deleteTimer invalidate];
        CKLog(@"invalidate222");
        self.deleteTimer = nil;
        self.deleteDeltaTime = .35f;
        [self deleteTimerStartWithAction:@selector(deleteWordOneByOne)];
        return;
    }
    else{
        if (self.deleteCount > 0) {
            [self shouldPlaySounds];
        }
        [self ChangePreviewCapsStatusWhenDelete];
    }
}

- (void)deleteWordOneByOne
{
    if (STR_BEFORE == nil || [STR_BEFORE isEqualToString:@""]) {
        [self.deleteTimer invalidate];
        return;
    }
    CKLog(@"oneoneoneoneoneoneoneoneoneoneoneoneoneone");
    self.deleteCount = 0;
    /*if (self.deleteCount >= 30) {
        [self.deleteTimer invalidate];
        CKLog(@"invalidate333");
        self.deleteTimer = nil;
        self.deleteDeltaTime = 0.5f;
        self.deleteCount = 0;
        [self deleteTimerStartWithAction:@selector(deleteWordsLineByLine)];
        return;
    }
    else{*/
        NSString * headerStr = [NSString lastStrIncludeLastSpacesWithStr:STR_BEFORE];    // 获取光标之前最后一个空格之后的子符,查询
        if (!headerStr){
            return;
        }
        [self shouldPlaySounds];
        int length = (int)headerStr.length;
        if (length != 0) {
            for (int i = 0;i < length; i ++) {
                [TXT_M deleteBackward];
            }
        }
        else{
            [self ChangePreviewCapsStatusWhenDelete];
        }
    //}
}

- (void)deleteWordsLineByLine
{
    if (STR_BEFORE == nil || [STR_BEFORE isEqualToString:@""]) {
        [self.deleteTimer invalidate];
        return;
    }
    CKLog(@"linelinelinelineline");
    [self shouldPlaySounds];
    int length = (int)STR_BEFORE.length;
    if (length != 0) {
        for (int i = 0;i < length; i ++) {
            [TXT_M deleteBackward];
        }
    }
    else{
        [self ChangePreviewCapsStatusWhenDelete];
    }
}

-(void)emojiPressedEnglish:(UIButton *)button
{
    //#ifdef CKRunPerformenceTest
    //    TICK;
    //#endif
    //	[self playSongs];
    
    [CKSaveTools saveEventForEmojiMeme];
    [CKSaveTools saveInteger:SelectedEmoji forKey:SELECT_INPUT inRegion:CKSaveRegionExtensionKeyboard];
    
    self.predictionWordClicked = NO;
    if (self.userAllowPredict) {
        [[CKPredictionKing sharePrediction] clearAssList];
        [self.predictionView removeFromSuperview];
        [self.predictionView ClearButtonArray];
        self.currentInputStr = @"";
    }
    [CKSaveTools saveClickWithEventType:KB_Table_Click key:@"Emoji"];
    
    
    [self removeLeftView];
    [self clearView];
    self.predictionView.hidden = YES;
    self.topToolView.hidden = YES;
    
    CGRect emojiFrame;
#if (APP_VARIANT_NAME == APP_VARIANT_NEWMEMOJI)
    BOOL island = [CKCommonTools screenDirectionIsLand];
    CGFloat h = island ? UIMANGER.landscapeHeightTotal:UIMANGER.portraitHeightTotal;
    emojiFrame = CGRectMake(0, 0, self.rootView.frame.size.width, h);
#else
    emojiFrame = CGRectMake(0, 0, self.rootView.frame.size.width, self.heightConstraint.constant);
#endif
    emojiView = [[CKEmojiView alloc]initWithFrame:emojiFrame lastResourceType:lastEmojiResourceType delegate:self];
    self.currentViewWidth = emojiView.frame.size.width;
    [self.rootView addSubview:emojiView];
}

- (void)coverButtonClicked
{
    [self.themeView removeFromSuperview];
    [self.coverButton removeFromSuperview];
}

- (void)skinTableViewChooseNewTheme
{
    [self.themeChangeTimer invalidate];
    self.themeChangeTimer = nil;
    [self themeChangeTimerStart];
}

#pragma mark - 返回输入键盘
- (void)backToBoard
{
    self.topToolView.selectedCategoryButton.selected = NO;
    self.topToolView.selectedCategoryButton = self.topToolView.keyboardButton;
    self.topToolView.keyboardButton.selected = YES;
    [self altPressedNumber:nil];
}


#pragma mark - 数字键盘代理方法
#pragma mark  移除剩余的view
- (void)removeLeftView
{
    for (UIView * view in self.rootView.subviews){
        if (view != self.topToolView && view != self.predictionView) {
            [view removeFromSuperview];
        }
    }
    
    if (self.deleteTimer) {
        [self.deleteTimer invalidate];
        self.deleteTimer = nil;
    }
}

- (void)clearView
{   self.keyboard = nil;
    self.themeView = nil;
    self.coolFontView = nil;
    self.settingView = nil;
    emojiView = nil;
    if (self.deleteTimer) {
        [self.deleteTimer invalidate];
        self.deleteTimer = nil;
    }
    
}

-(void)showTopView
{
    if (!self.predictionView.hidden) {
        self.predictionView.hidden = YES;
        self.topToolView.hidden = NO;
        [self.predictionView ClearButtonArray];
        self.currentInputStr = @"";
    }
    else if (self.topToolView.hidden) {
        self.topToolView.hidden = NO;
        [self.predictionView ClearButtonArray];
        self.currentInputStr = @"";
    }
}

#pragma mark 数字键盘global键按下
-(void)globalPressedNumber:(UIButton *)button
{
    [CKSaveTools saveClickWithEventType:KB_SwitchKeyboard_Click key:KB_SwitchKeyboard_Click];
    [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_SwitchKeyboard_Click];
    [self advanceToNextInputMode];
}

#pragma mark 字母键按下alt
-(void)altPressedNumber:(UIButton *)button
{
    [self removeLeftView];
    [self updateUserPreference]; // 偏好设置
    BOOL island = [CKCommonTools screenDirectionIsLand];
    CGFloat h = island?UIMANGER.landscapeHeightKeyboard:UIMANGER.portraitHeightKeyboard;
    CGFloat y = island? (UIMANGER.landscapeHeightTotal - h):(UIMANGER.portraitHeightTotal - h);
    CKMainKeyboard *keyboardEnglish = [[CKMainKeyboard alloc] initWithFrame:CGRectMake(0, y, self.inputView.frame.size.width, h)];
    keyboardEnglish.englishboardDelegate = self;
    self.keyboard = keyboardEnglish;
    [self.rootView insertSubview:self.topToolView aboveSubview:self.predictionView];
    
    //#warning 添加predictionView
    if (self.userAllowPredict) {
            [self.rootView insertSubview:self.predictionView belowSubview:self.keyboard];
        // 让预测栏先隐藏
        if ([self.currentInputStr isEqualToString:@""]) {
            self.predictionView.hidden = YES;
            self.topToolView.hidden = NO;
        } else {
            self.predictionView.hidden = NO;
            if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"]) {
                self.topToolView.hidden = YES;
            }
        }
    }
    [self.rootView addSubview:keyboardEnglish];
    self.currentViewWidth = self.rootView.frame.size.width;
    
    [self changeShiftStatusByViewController];

        self.predictionView.hidden = YES;
        self.topToolView.hidden = NO;
        self.keyboard.returnType = self.textDocumentProxy.returnKeyType;
        if(self.textDocumentProxy.autocorrectionType == UITextAutocorrectionTypeNo)
        {
            // 如果是不需要预测的,直接让predictBar隐藏,topToolView出现
#pragma mark ---- 这里问什么要判断返回的数组长度呢?  Asked by Sam
//            if (self.predictionView != nil && self.predictionView.buttonArray.count != 0) {
                [self.predictionView ClearButtonArray];
                self.predictionView.hidden = YES;
                self.topToolView.hidden = NO;
//            }
            
        }
}


- (void)changeShiftStatusByViewController
{
    CKLog(@"=====changeShiftStatusByViewController=====");
    if (self.keyboard.shiftStatus == CKShiftStatusAllUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedLower)
        return;
    BOOL shouldCaps = [WORDMANGER shouldCapsWithPreStr:STR_BEFORE afterStr:STR_AFTER];
    if (shouldCaps) {// 如果为空，shit键应发生变化
        self.keyboard.shiftStatus = CKShiftStatusCapitalized;
        self.keyboard.shifted = YES;
        self.previewCapsType = CKPreviewCapsTypeCaps;         // 判断preview上的单词是否需要大写
    }
    else{
        self.keyboard.shiftStatus = CKShiftStatusAllLower; // 如果有文字的话,应该小写
        self.keyboard.shifted = NO ;
    }
}


#pragma mark - ------------------------coolFont---------------------------------------------
//显示弹出框
-(void)showRateView{
    //设置背景
    _backView=[[UIView alloc]initWithFrame:self.keyboard.frame];
    _backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self.view addSubview:_backView];
    // 设置弹出框
     _guideView = [[CKGuideView alloc] initWithAppType:CKAppTypeExtensionApp guideType:CKGuideTypeRateFromKeyBoard fram:CGRectMake(0, 0, 0, 0)];
    _guideView.delegate = self;
    [self.view addSubview:_guideView];
}
#pragma mark -- 评分弹出框的代理方法

-(void)CKGuideView:(CKGuideView*)guideView clickBtnType:(CKClickType)clickType{
    [guideView removeFromSuperview];
    [_backView removeFromSuperview];
    if(clickType == CKClickTypeRate){
        //添加评分的点击统计
        [CKSaveTools saveString:@"EVALUATE_STATUS_GROUP" forKey:EVALUATE_STATUS_GROUP inRegion:CKSaveRegionAppGroup];
        [CKSaveTools saveClickWithEventType:KB_CoolFont_KeyBoard_Rate_Clicked  key:@"KB_CoolFont_KeyBoard_Rate_Clicked"];
        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_CoolFont_KeyBoard_Rate_Clicked];
        //评分
        [CKSaveTools switchToURL:[NSString stringWithFormat:@"%@%@",AppStore_URL,APPID]  withResponder:self.view];
        //保存评价状态
        [CKSaveTools saveInteger:AlreadyEvaluated forKey:EVALUATE_STATUS inRegion:CKSaveRegionExtensionKeyboard];
    }else if (clickType == CKClickTypeCancel){
        //添加取消按钮统计
        [CKSaveTools saveClickWithEventType:KB_CoolFont_KeyBoard_Rate_Cancel_Clicked  key:@"KB_CoolFont_KeyBoard_Rate_Cancel_Clicked"];
        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_CoolFont_KeyBoard_Rate_Cancel_Clicked];
    }
}
- (void)showCoolFontView:(UIButton *)button same:(BOOL)same
{
    if (self.userAllowPredict) {
        [[CKPredictionKing sharePrediction] clearAssList];
    }
    if (same) {
        [self backToBoard];
        return;
    }
    [self removeLeftView];
    [self clearView];
    CKCoolFontView * coolFontView = [[CKCoolFontView alloc] initWithFrame:[self currentKeyboardFrame]];
    self.currentViewWidth = coolFontView.frame.size.width;
    [self.rootView addSubview:coolFontView];
    self.coolFontView = coolFontView;
    coolFontView.delegate = self;
    
//    [coolFontView.coolFontTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:WORDMANGER.selectedCoolFontIndex inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)coolFontViewChoseFontStyleNumber:(int)index coolFont:(NSString *)coolFont
{
    [WORDMANGER updateSelectedCoolFontIndex:index];
    self.selectedCoolFontDict = [[CKCoolFonts shareCoolFonts] selectedCoolFontDictWithCoolFontName:coolFont];
    [self.coolfontChangeTimer invalidate];
    self.coolfontChangeTimer = nil;
    [self coolfontChangeTimerStart];
}

- (void)textWillChange:(id<UITextInput>)textInput
{
    _beforeInputInTextWillChange = self.textDocumentProxy.documentContextBeforeInput;
    _afterInputInTextWillChange = self.textDocumentProxy.documentContextAfterInput;
}

- (void)textDidChange:(id<UITextInput>)textInput
{
    CKLog(@"=====textDidChange=====");
    NSString * beforeInputInTextDidChange = self.textDocumentProxy.documentContextBeforeInput;
    NSString * afterInputInTextDidChange = self.textDocumentProxy.documentContextAfterInput;
    if (beforeInputInTextDidChange == nil) {
        [self showTopView];
        if (afterInputInTextDidChange == nil && self.keyboard != nil) {
            [self.keyboard backToCharacter];
        }
        return;
    }
    BOOL changed = NO;
    if (beforeInputInTextDidChange == nil) {
        if (_beforeInputInTextWillChange != nil) {
            changed = YES;
        }
    }
    else if (![beforeInputInTextDidChange isEqualToString:_beforeInputInTextWillChange]) {
            changed = YES;
    }
    if (afterInputInTextDidChange == nil) {
        if (_afterInputInTextWillChange != nil) {
            changed = YES;
        }
    }
    else if (![afterInputInTextDidChange isEqualToString:_afterInputInTextWillChange]) {
        changed = YES;
    }

    if (!changed) {
        return;
    }
    if (self.userAllowPredict == NO || self.textDocumentProxy.autocorrectionType == UITextAutocorrectionTypeNo) { // 要考虑到用户不允许预测的情况
        self.topToolView.hidden = NO;
        if (self.predictionView != nil) {
            [self.predictionView ClearButtonArray];
            self.predictionView.hidden = YES;
        }
        return;
    }
    
    if (self.keyboard == nil) {
        if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"]) {
            self.topToolView.hidden = YES;
        }
        if (self.settingView || self.coolFontView || self.memeView || self.themeView) {
            self.topToolView.hidden = NO;
        }
        return;
    }
    // 大小写
    [self changeShiftStatusByViewController];
//    if (!beforeInputInTextDidChange.length && !afterInputInTextDidChange.length) {
//        return;
//    }
    if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"] && self.userAllowPredict && self.textDocumentProxy.autocorrectionType != UITextAutocorrectionTypeNo)
    {
        // 2. 清除HEARD_STR
        HEADSTR = @"";
        /*
         3.拿到最后一个单词,判断大小写并查询
         要判断最后一个单词后面是否有空格,如果有空格的话则预测下一个单词,如果没有空格则预测单个单词
         */
        if ([STR_BEFORE isEqualToString: @""] || STR_BEFORE == nil)
        { // 如果没有字符了,就不用预测了
            if (self.predictionView.hidden == NO) {
                [self.predictionView ClearButtonArray];
                self.predictionView.hidden = YES;
                self.topToolView.hidden = NO;
            }
            return;
        }
        else
        { // 如果有字符,看是否需要预测
            // 如果最后一个字符是特殊字符就不需要预测
            // 取出lastStr
            NSString * last = STR_BEFORE;
            NSString * s1 = [last substringWithRange:NSMakeRange(last.length - 1, 1)];
            unichar c1 = [last characterAtIndex:last.length - 1];
            //only for english, it will be changed later
            if (c1 > 0xff) {
                return;
            }
            NSRange capsRange = [SHOULD_CAPS rangeOfString:s1];
            if ((capsRange.location ==NSNotFound && ![s1 isEqualToString:@" "]))
            {//不包含,并且证明此时没有空格 ,并且最后一个不是特殊字符,直接拿来预测
                NSRange symbolRange = [SPECIAL_STR rangeOfString:s1];
                if (symbolRange.location ==NSNotFound)
                { // 如果不是标点符号
                    last = [NSString lastStrWithStr:last];
                    NSString * __block realString = @"";
                    
                    [last enumerateSubstringsInRange:NSMakeRange(0, last.length)
                                             options:NSStringEnumerationByComposedCharacterSequences
                                          usingBlock: ^(NSString* substring, NSRange substringRange, NSRange enclosingRange, BOOL* stop) {
                                              NSString * valueStirng = [[CKCoolFonts shareCoolFonts].coolFontDict[substring] lowercaseString];
                                              if (valueStirng.length == 0 || [valueStirng isEqualToString:substring.lowercaseString])
                                              {
                                                  valueStirng = substring;
                                              }
                                              
                                              realString = [realString stringByAppendingString:valueStirng];
                                          }];
                    if (self.userAllowPredict) {
                            [self predictWithHeadStr:realString atOneTime:YES];
                    }
                }
                else // 如果是标点符号
                {
                    self.predictionView.hidden = YES;
                    self.topToolView.hidden = NO;
                    [self.predictionView ClearButtonArray];
                }
                if(self.predictionView.buttonArray.count == 0)
                {
                    self.predictionView.hidden = YES;
                    self.topToolView.hidden = NO;
                }
            }
            else
            {
                // 如果有空格,则先让toolBar显示,这里输入一个@" "是因为之前已经删除了一个字符
                if ([last hasSuffix:@" "]) {
                    if (self.predictionView.hidden == NO) {
                        [self.predictionView ClearButtonArray];
                        self.predictionView.hidden = YES;
                        self.topToolView.hidden = NO;
                    }
                }
            }
        }
    }
}

#pragma mark - 代理方法
#pragma mark - 纯英文字母键盘的代理方法
#pragma mark 字母按键按下
- (void)shiftChangeStatusTo:(CKShiftStatus)shiftStatus
{
    self.previewCapsType = (CKPreviewCapsType)shiftStatus;
}

-(void)shouldPlaySounds
{
    [self playSongs];
}

-(void)characterPressedEnglish:(CKMainButton *)button Str:(NSString *)str
{

    if (self.deleteTimer) {
        [self.deleteTimer invalidate];
        self.deleteTimer = nil;
    }
    if (!str) {
        return; // 出错处理
    }
    if ([str isEqualToString:@"."]) {
        [self symbolButtonClicked:button];
        return;
    }
    self.isInputSymbols = NO;
    self.predictionWordClicked = NO;
    if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"] && ![str isEqualToString:@","]) {
        // 先判断输入框是否为空,在这里判断是因为用户可能做了剪切操作
        BOOL isEmpty = [WORDMANGER textIsEmptyWithPreStr:STR_BEFORE afterStr:STR_AFTER];
        if (isEmpty) { // 如果为空,则将标签字符清空
            HEADSTR = @"";
        }
        HEADSTR = [HEADSTR stringByAppendingString:str];
        // 字母按键按下,首先要判断是否为coolFont
        int index = WORDMANGER.selectedCoolFontIndex;

        if(index != 0){
            NSString * insertStr = self.selectedCoolFontDict[str.lowercaseString];
            if (insertStr.length != 0 || insertStr != nil) {
                str = insertStr;
            }
        }
    }
    
    [TXT_M insertText:str];
    
//    if (self.textDocumentProxy.autocorrectionType == UITextAutocorrectionTypeNo) {
//        [TXT_M insertText:str.lowercaseString];
//    }
//    else{
//        [TXT_M insertText:str];
//    }

    [CKSaveTools saveClickWithEventType:KB_Char_Click key:KB_Char_Click];
    
    if (self.userAllowPredict && [USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"]) {
        if (self.textDocumentProxy.autocorrectionType == UITextAutocorrectionTypeNo) {
            return;
        }
        
        
        //获取光标前第一个空格后的字符
        NSString *checkStr = [NSString lastStrAfterSymbolsWithStr:STR_BEFORE dict:self.selectedCoolFontDict oppositeDict:[CKCoolFonts shareCoolFonts].coolFontDict];
        [self predictWithHeadStr:checkStr atOneTime:NO];
    }
}

// 数字键盘输入
- (void)characterPressedNumber:(CKMainButton *)button Str:(NSString *)str
{
    //     [self playSongs];
    if (self.deleteTimer) {
        [self.deleteTimer invalidate];
        self.deleteTimer = nil;
    }
    
    self.isInputSymbols = YES;
    
    NSString *Symbols = @"($@[{#%^*+=_<£¥•0123456789";
    NSRange range = [Symbols rangeOfString:str];
    if (range.location != NSNotFound) {
        if (self.predictionView.buttonArray.count > 1) {
            [self.predictionView ClearButtonArray];
            self.currentInputStr = @"";
            if (self.predictionView.hidden == NO)
            {
                self.predictionView.hidden = YES;
                self.topToolView.hidden = NO;
            }
        }
        [TXT_M insertText:str];
    }
    else if([str isEqualToString:@"'"] || [str isEqualToString:@"&"]){
        [TXT_M insertText:str];
        if ([str isEqualToString:@"'"]) {
            [self backToBoard];
        }
    }
    else {
        [self InsertInputStr];
        if (self.predictionWordClicked) {
            NSString *Symbols = @"\"\\|~>";
            NSRange range = [Symbols rangeOfString:str];
            if (range.location == NSNotFound) {
                [TXT_M deleteBackward];
            }
        }
        [TXT_M insertText:str];
    }
    self.predictionWordClicked = NO;
}


// emoji输入
- (void)iKeyboardEmojiButtonClicked:(CKMainButton *)button
{
    NSString * str = button.outPutToTextFiledStr;
    if (str == nil) {
        str = button.currentTitle;
    }

    [CKBackend setEmojiUsedInApp:YES];
    [CKSaveTools saveClickWithEventType:KB_Emoji_Click key:str];
    
    //    [self testUmengEmoji];
    [TXT_M insertText:str];
}

- (void)testUmengEmoji
{
    for (int i = 0; i < 100; i ++) {
        NSString * st = [NSString stringWithFormat:@"张赛测试:�%d次",i];
        [CKSaveTools saveClickWithEventType:KB_Emoji_Click key:st];
    }
}


- (void)iKeyboardEmojiBottomViewButtonClicked:(NSInteger)tag
{
    [self playSongs];
    switch (tag) {
        case 0:
            [CKSaveTools saveEventForEmojiMeme];
            lastEmojiResourceType = [emojiView getResourceType];
            [self removeLeftView];
            [self clearView];
            [self backToBoard];
            self.topToolView.hidden = NO;
            break;
        case 1:
            [CKSaveTools saveClickWithEventType:KB_SwitchKeyboard_Emoji_Click key:KB_SwitchKeyboard_Emoji_Click];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_SwitchKeyboard_Emoji_Click];
            [self globalPressedNumber:nil];
            //[self.textDocumentProxy insertText:@"\n"];
            break;
        case 2:
            [TXT_M deleteBackward];
            //            [self.textDocumentProxy deleteBackward];
            break;
        default:
            break;
    }
}

-(BOOL)inputPredictionWordAfterSpacePressed
{
    if (self.predictionView.buttonArray.count > 1) {
        CKPredictButton * button = self.predictionView.buttonArray[1];
        if ([button titleColorForState:UIControlStateNormal] == self.predictionView.font_highlight_color) {
            [self.predictionView predictButtonClickedFromSpace:button];
            HEADSTR = @"";
            return YES;
        }
    }
    return NO;
}

-(void)DecideShouldInputSymbolAndPreviewCapsState
{
    // 3.判断是否要输入标点以及文字和预测栏的大小写问题
    //if (STR_AFTER.length != 0)
    //    return;
    //else{// 如果字符串不为空,在是否输入空格的逻辑上与锁定大写完全一致,区别只是大小写
        
        // 3.1判断是否要输入句号
        BOOL should = ([WORDMANGER shouldInputFULL_STOPWithSB:STR_BEFORE]) && (self.isSpaceTimerPhase);
        if (should) {
            [TXT_M deleteBackward];
            [TXT_M deleteBackward];
            [TXT_M insertText:@". "];
            if (self.userAllowPredict) {
                self.predictionView.hidden = YES;
            }
            if (self.keyboard.shiftStatus == CKShiftStatusAllUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedLower){
                return;
            }
            self.previewCapsType = CKPreviewCapsTypeCaps; // preview
            [self shiftButtonChangeWithTag:2]; // shiftButton
            
            return;
        }
        // 3.2下面是大小写的判断
        // 3.2.1锁定大写,没有大小写判断,直接返回
        if (self.keyboard.shiftStatus == CKShiftStatusAllUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedLower){
            return;
        }
        else { //
            if (should) { // 如果在上面输入了句号,则要首字母大写
                self.previewCapsType = CKPreviewCapsTypeCaps; // preview
                [self shiftButtonChangeWithTag:2]; // shiftButton
            }
            else{ // 如果没有输入句号,只是输了一个空格
                // 如果前边是正常单词的话,要正常输入,如果是特殊字符,则要首字母大写
                // 只需要判断倒数第二位字符(此时已经肯定大于2)
                if (STR_BEFORE.length >= 2) {
                    NSString * s2 = [STR_BEFORE substringWithRange:NSMakeRange(STR_BEFORE.length - 2, 1)];
                    NSRange capsRange = [SHOULD_CAPS rangeOfString:s2];
                    if (capsRange.location == NSNotFound || [s2 isEqualToString:@"n"]){//不包含,n与\n冲突了
                        self.previewCapsType = CKPreviewCapsTypeAllLower; // preview
                        [self shiftButtonChangeWithTag:1]; // shiftButton
                        //若前面全为空格或换行符则需首字母大写
                        NSString *beforeStr = STR_BEFORE;
                        if ([[beforeStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length] == 0) {
                            self.previewCapsType = CKPreviewCapsTypeCaps; // preview
                            [self shiftButtonChangeWithTag:2]; // shiftButton
                        }
                    }
                    else{ // 如果包含,则大写
                        self.previewCapsType = CKPreviewCapsTypeCaps; // preview
                        [self shiftButtonChangeWithTag:2]; // shiftButton
                    }
                }
            }
        }
    //}
}

- (void)spacePressedEnglish:(id)sender
{
    [self.spaceTimer invalidate];
    self.spaceTimer = nil;
    CKLog(@"%@===headString",HEADSTR);
    if (self.predictionWordClicked) {
        self.predictionWordClicked = NO;
        [self spaceTimerStart];
        return;
    }
    
    self.predictionWordClicked = NO;
    //    [self playSongs];
    BOOL status = [self inputPredictionWordAfterSpacePressed];
    if (status) {
        [self spaceTimerStart];
        return;
    }
    // 2.输入空格
    [TXT_M insertText:@" "]; // 空格是一定要输的

    [self DecideShouldInputSymbolAndPreviewCapsState];
    //判断光标之前标点符号后是否有多个空格
    NSInteger SpaceCount = [self checkSpaceCountBeforeCursor];
    [self spaceTimerStart];
    if (self.isInputSymbols && self.textDocumentProxy.keyboardType == UIKeyboardTypeDefault) {
        [self altPressedNumber:nil];
    }
    if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"] && (self.textDocumentProxy.autocorrectionType != UITextAutocorrectionTypeNo))
    {
        // 1.首先输入预测,比较耗时,放前边
        if (SpaceCount < 2) {
            if (self.userAllowPredict){
                
//                CKLog(@"%@ === isFinished",self.predictOperation);
//                if (!self.predictOperation) { // 如果点击空格的时候正在预测,要稍等一下,如果输入得特别特别快,输入work为例,要是空格比rk先响应,会造成rk返回的预测数组失效,输出变成no了
                    [self predictLastWord];
//                }
//                else{
//                    CKLog(@"空格输得特别特别快");
//                }
            }
        }
        else {
            [self showTopView];
        }
    }
}// 空格


-(NSInteger)checkSpaceCountBeforeCursor
{
    NSString * nowBeforetStr = STR_BEFORE;
    NSInteger SpaceCount = 0;
    NSString *lastChar;
    if ([nowBeforetStr hasSuffix:@" "] && nowBeforetStr.length != 0) {
        for (int i = (int)nowBeforetStr.length - 2; i >= 0; i--) {
            NSString *ch = [nowBeforetStr substringWithRange:NSMakeRange(i, 1)];
            if ([ch isEqualToString:@" "]) {
                SpaceCount ++;
            }
            else{
                lastChar = ch;
                SpaceCount ++;
                break;
            }
        }
        if (lastChar != nil) {
            NSRange range = [EndingSymbol rangeOfString:lastChar];
            if (range.location != NSNotFound && self.keyboard.shiftStatus != CKShiftStatusAllUpper) {
                self.previewCapsType = CKPreviewCapsTypeCaps;
                [self shiftButtonChangeWithTag:2];
            }
            else{
                if (SpaceCount > 2 && self.keyboard.shiftStatus != CKShiftStatusAllUpper) {
                    self.previewCapsType = CKPreviewCapsTypeCaps;
                    [self shiftButtonChangeWithTag:2];
                }
            }
        }
    }
    return SpaceCount;
}

- (void)predictLastWord
{
    // 1.1空格输入一次,要输入最后一个单词的联想
    NSString * nowBeforetStr = STR_BEFORE;
    while ([nowBeforetStr hasSuffix:@" "] && nowBeforetStr.length != 0) {     // 如果STR_BEFORE不为空,但是后面却有一堆空格
        nowBeforetStr = [nowBeforetStr stringByReplacingCharactersInRange:NSMakeRange(nowBeforetStr.length - 1, 1) withString:@""];
    }
    if (nowBeforetStr.length) {
        NSString * lastStr = [NSString lastStrAfterSymbolsWithStr:nowBeforetStr dict:self.selectedCoolFontDict oppositeDict:[CKCoolFonts shareCoolFonts].coolFontDict];
        if ([lastStr isEqualToString:@""]) {
            [self showTopView];
            return;
        }
        NSString * __block realString = @"";
//        for (int i = 0; i < lastStr.length; i ++) {
//            NSString * everyCharacter = [lastStr substringWithRange:NSMakeRange(i, 1)];
//            NSString * valueStirng = WORDMANGER.oppositeCoolFontDict[everyCharacter];
//            if (valueStirng.length == 0 || valueStirng == nil) {
//                valueStirng = everyCharacter;
//            }
//            realString = [realString stringByAppendingString:valueStirng];
//        }
        [lastStr enumerateSubstringsInRange:NSMakeRange(0, lastStr.length)
                                 options:NSStringEnumerationByComposedCharacterSequences
                              usingBlock: ^(NSString* substring, NSRange substringRange, NSRange enclosingRange, BOOL* stop) {
                                  NSString * valueStirng = [[CKCoolFonts shareCoolFonts].coolFontDict[substring] lowercaseString];
                                  if (valueStirng.length == 0 || [valueStirng isEqualToString:substring.lowercaseString])
                                  {
                                      valueStirng = substring;
                                  }
                                  realString = [realString stringByAppendingString:valueStirng];
                              }];
#warning ---To Do 这里用lowercaseString是为了让coolFont以及锁定大写能联想,但是如果正常输入的字符就不能大小写敏感地去联想了
        [self receiveNextWordWithBeforeWord:realString.lowercaseString];
        HEADSTR = @"";
        // 1.2要清空predictKing的表单
        //[[CKPredictionKing sharePrediction] clearAssList];
    }
}

// 修改PreviewCaps当长安删除键
- (void)ChangePreviewCapsStatusWhenDelete
{
    
//    CKLog(@"%d====currentStatus",self.keyboard.shiftStatus);
    // 0. 删除操作
    [TXT_M deleteBackward];
    if (HEADSTR.length && ![HEADSTR isEqualToString:@""]) {
        HEADSTR = [HEADSTR stringByReplacingCharactersInRange:NSMakeRange(HEADSTR.length - 1, 1) withString:@""];
    }
    if ([WORDMANGER textIsEmptyWithPreStr:STR_BEFORE afterStr:STR_AFTER]) { // 如果当前文本框为空,则直接返回
        [TXT_M insertText:@""];
        self.predictionView.hidden = YES;
        if (self.keyboard == nil) {
            if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"]) {
                self.topToolView.hidden = YES;
            }
        }
        if (self.keyboard.shiftStatus == CKShiftStatusAllUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedLower)
            return;
        self.previewCapsType = CKPreviewCapsTypeCaps; // preview
        [self shiftButtonChangeWithTag:2]; // shiftButton
        return;
    }
    // 1. 大小写
    if (self.keyboard.shiftStatus == CKShiftStatusAllUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedLower)
        return;
    else{
        if (STR_BEFORE.length == 0) { // 如果没有字符了,就首字母大写
            self.previewCapsType = CKPreviewCapsTypeCaps; // preview
            [self shiftButtonChangeWithTag:2]; // shiftButton
            self.predictionView.hidden = YES;
            if (self.keyboard != nil) {
                self.topToolView.hidden = NO;
            }
            return;
        }
        else if (STR_BEFORE.length == 1 && [STR_BEFORE isEqualToString:@" "]) { // 如果等于1,只有当是空格的时候才会大写
            self.previewCapsType = CKPreviewCapsTypeCaps; // preview
            [self shiftButtonChangeWithTag:2]; // shiftButton
            return;
        }else if (STR_BEFORE.length > 1) { // 如果大于1,只有在最后一个字符为空格,且倒数第二个字符为结束类字符时,才需要大写
            // STR_BEFORE是唯一的一个单词,也需要大写
            NSString * last = [NSString lastStrWithStr:STR_BEFORE];
            if ([last isEqualToString:STR_BEFORE]) {
                self.previewCapsType = CKPreviewCapsTypeCaps; // preview
                [self changeShiftStatusByViewController];
//                                [self shiftButtonChangeWithTag:2]; // shiftButton
                return;
            }
            NSString * s1 = [STR_BEFORE substringWithRange:NSMakeRange(STR_BEFORE.length - 1, 1)];
            NSString * s2 = [STR_BEFORE substringWithRange:NSMakeRange(STR_BEFORE.length - 2, 1)];
            
            
            NSRange capsRange = [SHOULD_CAPS rangeOfString:s2];
            if ((capsRange.location != NSNotFound || [s2 isEqualToString:@" "])&& [s1 isEqualToString:@" "]){
                self.previewCapsType = CKPreviewCapsTypeCaps; // preview
                [self shiftButtonChangeWithTag:2]; // shiftButton
                if (self.keyboard == nil) {
                    if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"]) {
                        self.topToolView.hidden = YES;
                    }
                }
                return;
            }
            else{
                self.previewCapsType = CKPreviewCapsTypeAllLower; // preview
                [self shiftButtonChangeWithTag:1]; // shiftButton
                if (self.keyboard == nil) {
                    if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"]) {
                        self.topToolView.hidden = YES;
                    }
                }
                return;
            }
        }
    }
}

- (void)deletePressedEnglish:(id)sender
{
    if (self.predictionWordClicked) {
        self.predictionWordClicked = NO;
    }
    
    CKLog(@"%@===b,%@==a",STR_BEFORE,self.textDocumentProxy.documentContextAfterInput);
    if (STR_BEFORE.length == 0 && self.textDocumentProxy.documentContextAfterInput.length == 0 ) {// 如果前后都没有字符的话,输入一个空字符
        // 这里是为了解决全选删除后两行还是两行的bug
        [TXT_M insertText:@" "];
        [TXT_M deleteBackward];
    }

    [self ChangePreviewCapsStatusWhenDelete];
    // 1.删除最后一个字符
    if ([USER_PREFERENCES_MGR.language.lowercaseString isEqualToString:@"english"] && self.userAllowPredict) {
        // 2. 清除HEARD_STR
        HEADSTR = @"";
        /*
         3.拿到最后一个单词,判断大小写并查询
         要判断最后一个单词后面是否有空格,如果有空格的话则预测下一个单词,如果没有空格则预测单个单词
         */
        
//        if ([STR_BEFORE isEqualToString: @""] || STR_BEFORE == nil || self.keyboard.returnType == 1 || self.keyboard.returnType == 6) { // 如果没有字符了,就不用预测了

        if ([STR_BEFORE isEqualToString: @""] || STR_BEFORE == nil || self.textDocumentProxy.autocorrectionType == UITextAutocorrectionTypeNo) { // 如果没有字符了,就不用预测了
            [self.predictionView ClearButtonArray];
            self.predictionView.hidden = YES;
            self.topToolView.hidden = NO;
            return;
        }
        else{ // 如果有字符,看是否需要预测
            // 如果最后一个字符是特殊字符就不需要预测
            // 取出lastStr
            NSString * last = STR_BEFORE;
            NSString * s1 = [last substringWithRange:NSMakeRange(last.length - 1, 1)];
            NSRange capsRange = [SHOULD_CAPS rangeOfString:s1];
            if ((capsRange.location ==NSNotFound && ![s1 isEqualToString:@" "])){//不包含,并且证明此时没有空格 ,并且最后一个不是特殊字符,直接拿来预测
                NSRange symbolRange = [SPECIAL_STR rangeOfString:s1];
                if (symbolRange.location ==NSNotFound) { // 如果不是标点符号
                    last = [NSString lastStrWithStr:last];
                    NSString * realString = [NSString translateCoolFontSting:last dict:self.selectedCoolFontDict oppositeDict:[CKCoolFonts shareCoolFonts].coolFontDict];
                    if (self.userAllowPredict) {
                        HEADSTR = realString;
                        if (![realString isEqualToString:@""]) {
                            [self predictWithHeadStr:realString atOneTime:YES];
                        }
                        else {
                            [self showTopView];
                        }
                    }
                }
                else // 如果是标点符号
                {
                    self.predictionView.hidden = YES;
                    self.topToolView.hidden = NO;
                    [self.predictionView ClearButtonArray];
                }
            }
            else{ // 如果有空格或者特殊字符,需要拿到最后一个单词联想下一个单词
                NSString* retStr = [last deleteTailSpaceOfStr:last];
                if ([retStr isEqualToString:@""]) {
                    if (self.predictionView.buttonArray.count != 0) {
                        [self.predictionView ClearButtonArray];
                        self.predictionView.hidden = YES;
                        self.topToolView.hidden = NO;
                    }
                    return;
                }
                retStr = [NSString lastStrWithStr:retStr];
                [self receiveNextWordWithBeforeWord:retStr];
                // 如果有空格,则先让toolBar显示,这里输入一个@" "是因为之前已经删除了一个字符
            }
        }
    }
    [CKSaveTools saveClickWithEventType:KB_Del_Click key:KB_Del_Click];
}

-(void)showRateViewOnRoot{
    if(WORDMANGER.selectedCoolFontIndex != 0){
        if([CKSaveTools determineNowUserType] && ![CKSaveTools lockStatus] && ![CKSaveTools determineUserHaveEvaluated]){
            //新用户，锁没开，没评价过
            [CKSaveTools saveClickCountToNum:4 Key:@"sendCount" triggerAction:@selector(showRateView) withHandle:self];
        }
    }

}
- (void)returnPressedEnglish:(id)sender
{

#pragma mark -----崩溃测试 测试看友盟能收到崩溃统计不
//    NSArray * array = @[@"1",@"2"];
//    NSMutableArray * mutabA = [array copy];
//    [mutabA insertObject:@"3" atIndex:2];
    [self showRateViewOnRoot];
    [self InsertInputStr];
    self.predictionWordClicked = NO;
    [TXT_M insertText:@"\n"];
    HEADSTR = @"";// 清空标签字符
    NSString * strAfter = TXT_M.documentContextAfterInput;
    if (self.keyboard.shiftStatus == CKShiftStatusAllUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedUpper || self.keyboard.shiftStatus == CKShiftStatusLongPressedLower || strAfter.length != 0)
        return;
    else{[self shiftButtonChangeWithTag:2];}
    self.predictionView.hidden = YES; // 隐藏PreviewBar
    self.topToolView.hidden = NO;
    if (self.isInputSymbols && self.textDocumentProxy.keyboardType == UIKeyboardTypeDefault) {
        [self altPressedNumber:nil];
    }
}// 回车

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];//即使没有显示在window上，也不会自动的将self.view释放。
    if (self.isViewLoaded && !self.view.window){
        self.view = nil;// 目的是再次进入时能够重新加载调用viewDidLoad函数。
    }
}

-(void)InsertInputStr
{
    if (self.predictionView.buttonArray.count > 1) {
        CKPredictButton * button = self.predictionView.buttonArray[1];
        if ([button titleColorForState:UIControlStateNormal] == self.predictionView.font_highlight_color) {
            NSString *sourceWord = button.titleLabel.text;
            NSString *lastInputStr = [NSString GetLastInputStr:STR_BEFORE];
//            int count = (int)lastInputStr.length;
            if ([sourceWord hasSuffix:@" "]) { // 有空格,去除空格,用来查询
                sourceWord = [sourceWord stringByReplacingOccurrencesOfString:@" " withString:@""];
            }
            [self deleteWithCount:[NSString totalCharacterCount:lastInputStr]];
            [TXT_M insertText:[NSString translateNormalFontString:sourceWord ToCoolFontStrWithDict:self.selectedCoolFontDict]];
        }
        if (self.predictionView.hidden == NO) {
            self.predictionView.hidden = YES;
            self.topToolView.hidden = NO;
            [self.predictionView ClearButtonArray];
            self.currentInputStr = @"";
        }
    }
}

- (void)symbolButtonClicked:(CKMainButton *)button
{
    [self InsertInputStr];
    if (self.predictionWordClicked) {
        [TXT_M deleteBackward];
        self.predictionWordClicked = NO;
    }
    [self.textDocumentProxy insertText:@"."];
        [self changeShiftStatusByViewController];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIResponder *ur = self;
    while (ur != nil) {
        if ([NSStringFromClass([ur class]) rangeOfString:@"UIViewServiceViewController"].location != NSNotFound) {
            Ivar hostAppIvar = class_getInstanceVariable([ur class], "_hostBundleID");
            if (NULL != hostAppIvar)
            {
                id hostAppString = object_getIvar(ur, hostAppIvar);
                if (hostAppString && [[hostAppString class] isSubclassOfClass:[NSString class]]) {
                    NSLog(@"The bundle id of host app is %@", (NSString *)hostAppString);
                    [CKSaveTools saveClickWithEventType:KB_Keyboard_Use_App key:(NSString *)hostAppString];
                    [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                                       action:KB_Keyboard_Use_App
                                                        label:(NSString *)hostAppString];
                    if ([CKBackend getEmojiUsedInApp]) {
                        [CKSaveTools saveClickWithEventType:KB_Emoji_Use_App key:(NSString *)hostAppString];
                        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                                           action:KB_Emoji_Use_App
                                                            label:(NSString *)hostAppString];
                        [CKBackend setEmojiUsedInApp:NO];
                    }
                    if ([CKBackend getStickerUsedInApp]) {
                        [CKSaveTools saveClickWithEventType:KB_Sticker_Use_App key:(NSString *)hostAppString];
                        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                                           action:KB_Sticker_Use_App
                                                            label:(NSString *)hostAppString];
                        [CKBackend setStickerUsedInApp:NO];
                    }
                    if ([CKBackend getGifUsedInApp]) {
                        [CKSaveTools saveClickWithEventType:KB_Gif_Use_App key:(NSString *)hostAppString];
                        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                                           action:KB_Gif_Use_App
                                                            label:(NSString *)hostAppString];
                        [CKBackend setGifUsedInApp:NO];
                    }
                    if ([CKBackend getMemeUsedInApp]) {
                        [CKSaveTools saveClickWithEventType:KB_Meme_Use_App key:(NSString *)hostAppString];
                        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                                           action:KB_Meme_Use_App
                                                            label:(NSString *)hostAppString];
                        [CKBackend setMemeUsedInApp:NO];
                    }
                    break;
                }
            }
        }
        ur = [ur nextResponder];
    }
    [[CKPredictionKing sharePrediction] storeUDB];
    [CKBackend sendEventDataToBackend];
}


@end

