//
//  KeyboardViewController.h
//  Keyboard
//
//  Created by 张赛 on 14-9-20.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface KeyboardViewController : UIInputViewController

@property(nonatomic, strong)UILongPressGestureRecognizer *longPressGestureRecognizer;// 长按手势监听

-(void)shouldPlaySounds;

@end

