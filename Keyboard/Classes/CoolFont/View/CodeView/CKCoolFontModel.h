//
//  CKCoolFontModel.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/29.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKCoolFontModel : NSObject
@property (nonatomic,copy) NSString * fontName;
@property (nonatomic,assign) BOOL selected;
@property (nonatomic,assign) BOOL locked;
+ (NSArray * )allCoolFonts;
@end
