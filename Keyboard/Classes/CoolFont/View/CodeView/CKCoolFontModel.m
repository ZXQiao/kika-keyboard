//
//  CKCoolFontModel.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/29.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKCoolFontModel.h"
#import "CKCoolFonts.h"

@implementation CKCoolFontModel
- (instancetype)initWithCoolFontName:(NSString *)coolFontName
{
    self = [super init];
    if (self) {
        self.fontName = coolFontName;
    }
    return self;
}

+ (instancetype)coolFontModelWithName:(NSString *)coolFontName
{
    return [[self alloc] initWithCoolFontName:coolFontName];
}


+ (NSArray * )allCoolFonts
{
    NSMutableArray * coolFontModelArray = [NSMutableArray array];
    NSArray * coolFontsArray = [CKCoolFonts shareCoolFonts].coolFontNameArray;
    for (NSString * coolFontName in coolFontsArray) {
        [coolFontModelArray addObject:[CKCoolFontModel coolFontModelWithName:coolFontName]];
    }
    return (NSArray *)coolFontModelArray;
}
@end
