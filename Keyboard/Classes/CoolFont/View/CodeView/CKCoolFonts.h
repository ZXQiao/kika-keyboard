//
//  CKCoolFonts.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/29.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
@interface CKCoolFonts : NSObject
singleH(CoolFonts);

@property (nonatomic, strong) NSArray * coolFontNameArray;
//@property (nonatomic, strong) NSArray * coolFontJsonArray;
@property (nonatomic, strong) NSDictionary * coolFontDict;

- (NSDictionary *)selectedCoolFontDictWithCoolFontName:(NSString *)coolFontName;
@end
