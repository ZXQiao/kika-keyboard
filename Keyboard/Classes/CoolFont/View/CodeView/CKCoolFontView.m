//
//  CKCoolFontView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/29.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKCoolFontView.h"
#import "CKCoolFontViewCell.h"
#import "CKKeyboardUImanger.h"
#import "CKCoolFontModel.h"
#import "CKGuideView.h"

@interface CKCoolFontView()<UICollectionViewDataSource,UICollectionViewDelegate,CKGuideViewDelegate>
{
    CGFloat _cellheight;
    CGFloat _buttonToboder;
    NSArray * _allCoolFontsArray;
    int _selectedIndex;
    NSString * scoreSelectCoolFont;
    NSInteger  scoreSelectIndex;
    
}
@property (nonatomic,strong) UICollectionView * collectionView;


@end
static NSString * const reuseIdentifier = @"CKCoolFontViewCell";

@implementation CKCoolFontView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _allCoolFontsArray = [CKCoolFontModel allCoolFonts];
        NSUserDefaults * usrInfo = [NSUserDefaults standardUserDefaults];
        NSString * indexStr = [usrInfo objectForKey:@"CoolFontSelectedIndex"];
        _selectedIndex = indexStr.intValue;
        [self initDimensions];
        [self setUpCollectionViewWithFrame:frame];
    }
    return self;
}

- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"coolFont_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * realDimensions = [[CKKeyboardUImanger shareUImanger] getRealDimensionsByModelAndDirection:dimensions];
    _cellheight = [realDimensions[@"cellheight"] floatValue];
    _buttonToboder = [realDimensions[@"buttonToboder"] floatValue];
}

- (void)setUpCollectionViewWithFrame:(CGRect)frame{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    CGSize size = CGSizeMake(0, 0);
    [flowLayout setItemSize:size];//设置cell的尺
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);//设置其边界
    
    UICollectionView * collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:flowLayout];
    self.collectionView = collectionView;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    UINib *nib = [UINib nibWithNibName:reuseIdentifier bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:reuseIdentifier];
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.collectionView];
    [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:_selectedIndex inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
}

- (CKCoolFontViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CKCoolFontViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CKCoolFontViewCell alloc]init];
    }
    CKCoolFontModel * coolFont = _allCoolFontsArray[indexPath.row];
    if (indexPath.row == _selectedIndex) {
        coolFont.selected = YES;
    }
    else {
        coolFont.selected = NO;
    }
    coolFont.locked = NO;
    if( ![CKSaveTools lockStatus] && [CKSaveTools determineNowUserType]){
        //新用户，锁没开
        if(indexPath.row >= LOCK_START_INDEX){
            coolFont.locked = YES;//加锁对应的cell
        }else{
            coolFont.locked = NO;
        }
    }
    cell.coolFont = _allCoolFontsArray[indexPath.row];
    return cell;
}

- (void)showToastViewOnRootView:(UIView *)rootView coolFont:(NSString *)coolFont
{
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    UIImage * pop_bg_Image = nil;
    NSString * fontName = nil;
    UIColor * textColor = nil;
    if (nowUseTheme) {
        if (nowUseTheme.isOnline) {
            pop_bg_Image = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_bg" resize:YES];
        }
        else{
            pop_bg_Image = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_bg" resize:YES];
        }
        fontName = nowUseTheme.fontName;
        textColor = [UIColor colorFromHexString:nowUseTheme.FontColor];
    }
    if (pop_bg_Image == nil ) {
        pop_bg_Image = [UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_popup_bg",Default_Theme_Name]]];
        fontName = Default_FontName;
        textColor = [UIColor colorFromHexString:Default_Font_Color];
    }

    CGFloat labelH = rootView.frame.size.height / 4;
    CGFloat labelW = rootView.frame.size.width * 4 / 5;
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, labelW, labelH)];
    [button setBackgroundImage:pop_bg_Image forState:UIControlStateNormal];
    NSString * coolFontTip = [NSString stringWithFormat:@"%@  is chosen",coolFont];
    [button setTitle:coolFontTip forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    UIFont * font = [UIFont fontWithName:buttonDefaultFontName size:16];
    if ([fontName isEqualToString:@"LithosPro-Black"]) {
        font = [UIFont fontWithName:fontName size:16];
    }else if([fontName isEqualToString:@"Chalkboard SE"])
    {
        font = [UIFont fontWithName:fontName size:16];
    }
    button.titleLabel.font = font;
    [button setTitleColor:textColor forState:UIControlStateNormal];
    [rootView addSubview:button];
    button.center = rootView.center;
    
    [UIView animateWithDuration:.6 animations:^{
        button.alpha = 1;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:2.0 animations:^{
            button.alpha = .1;
        } completion:^(BOOL finished) {
            [button removeFromSuperview];
        }];
    }];
}
//弹出评分界面
-(void)showRateViewOnRootView:(UIView *)rootView coolFontName:(NSString *) coolFont fontNumber:(NSInteger)coolNum{
    scoreSelectCoolFont=coolFont;
    scoreSelectIndex= coolNum;
    [self buildBackView];
    [self buildGuideViewType:CKGuideTypeRate];
    
}
//弹出分享界面
-(void)showShareViewOnRootView:(UIView *)rootView coolFontName:(NSString *) coolFont fontNumber:(NSInteger)coolNum{
    scoreSelectCoolFont=coolFont;
    scoreSelectIndex= coolNum;
    [self buildBackView];
    [self buildGuideViewType:CkGuideTypeShare];
    
}
//根据引导类型创建对用的页面
-(void)buildGuideViewType:(CKGuideType)guideType{
    //对不同机型适配
    CGFloat h =self.frame.size.height;
    CGFloat w =0;
    if(![CKCommonTools screenDirectionIsLand]){
        h = h*0.8;
        w = h*1.55;
    }else{
        w = h*1.5;
    }
    CGRect fram=CGRectMake(0, 0, w, h);
    CKGuideView * rateView = [[CKGuideView alloc] initWithAppType:CKAppTypeExtensionApp guideType:guideType fram:fram];
    rateView.center =CGPointMake(self.center.x, self.center.y-self.frame.origin.y);
    rateView.delegate = self;
    [self addSubview:rateView];
}

-(void)buildBackView{
    UIView *view=[[UIView alloc]initWithFrame:self.bounds];
    view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    view.tag = 312;
    [self addSubview:view];
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UserHaveDoneSomethingNotification object:nil];
    if ([self.delegate respondsToSelector:@selector(shouldPlaySounds)]) {
        [self.delegate shouldPlaySounds];
    }
    
    if( ![CKSaveTools lockStatus] ){//锁未打开
        if(indexPath.row >= LOCK_START_INDEX){
           
            if( ![CKSaveTools determineUserHaveEvaluated] ){
                //没有评价过，则弹出评价引导页
                 //评价锁点击锁事件
                [CKSaveTools saveClickWithEventType:KB_CoolFont_Lock_Rate_Clicked key:[_allCoolFontsArray[indexPath.row] fontName]];
                [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                                   action:KB_CoolFont_Lock_Rate_Clicked
                                                    label:[_allCoolFontsArray[indexPath.row] fontName]];
                [self showRateViewOnRootView:self coolFontName:[_allCoolFontsArray[indexPath.row] fontName] fontNumber:indexPath.row];
                return;
            }else{
                //评价过，则跳到分享页面
                //分享🔒点击事件
                [CKSaveTools saveClickWithEventType:KB_CoolFont_Lock_Share_Clicked key:[_allCoolFontsArray[indexPath.row] fontName]];
                [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                                   action:KB_CoolFont_Lock_Share_Clicked
                                                    label:[_allCoolFontsArray[indexPath.row] fontName]];
                [self showShareViewOnRootView:self  coolFontName: [_allCoolFontsArray[indexPath.row]fontName] fontNumber:indexPath.row];
                return;
            }
        }
    }
    
    if (indexPath.row != _selectedIndex) {
        NSIndexPath * selectIndexPath = [NSIndexPath indexPathForRow:_selectedIndex inSection:0];
        _selectedIndex = (int)indexPath.row;
        [collectionView reloadItemsAtIndexPaths:@[selectIndexPath, indexPath]];
    }
    [CKSaveTools saveString:[NSString stringWithFormat:@"%d",(int)indexPath.row] forKey:@"CoolFontSelectedIndex" inRegion:CKSaveRegionExtensionKeyboard];
    // 在这里不需要用到AppGroup,用代理就可以了
    if ([self.delegate respondsToSelector:@selector(coolFontViewChoseFontStyleNumber:coolFont:)]) {
        NSString * coolFont = [_allCoolFontsArray[indexPath.row] fontName];
        [self showToastViewOnRootView:self coolFont:coolFont];
        [self.delegate coolFontViewChoseFontStyleNumber:(int)indexPath.row coolFont:coolFont];
    }
    
    [CKSaveTools saveClickWithEventType:KB_CoolFont_Click key:[_allCoolFontsArray[indexPath.row] fontName]];
    [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                       action:KB_CoolFont_Click
                                        label:[_allCoolFontsArray[indexPath.row] fontName]];
    
}
#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _allCoolFontsArray.count;
}



#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat w = self.frame.size.width / 2;
    return CGSizeMake(w, _cellheight);
}



- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0); // 上下左右的偏移量
    
}
#pragma mark -- 评分框代理
-(void)CKGuideView:(CKGuideView*)guideView clickBtnType:(CKClickType)clickType{
    //移除弹出框
    [guideView removeFromSuperview];
    for (UIView *view in self.subviews) {
        if(view.tag == 312){
            [view removeFromSuperview];
            break;
        }
    }
    if(clickType == CKClickTypeRate){
    //评分
        [CKSaveTools saveClickWithEventType:KB_CoolFont_Font_Rate_Clicked key:@"KB_CoolFont_Font_Rate_Clicked"];
        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                           action:KB_CoolFont_Font_Rate_Clicked];
        
        [CKSaveTools switchToURL:[NSString stringWithFormat:@"%@%@",AppStore_URL,APPID] withResponder:self];
         [self saveGuideStatus];
       
    }else if (clickType == CKClickTypeShare){
    //分享
        [CKSaveTools saveClickWithEventType:KB_CoolFont_Share_Clicked  key:@"KB_CoolFont_Share_Clicked"];
        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                           action:KB_CoolFont_Share_Clicked];
        [CKCommonTools showInfoByFittingParentView:self msg:@"Open\nKika Keyboard App\nfor more!"];
//        [CKSaveTools switchToURL:[NSString stringWithFormat:@"%@.share",APP_ROOT_URL] withResponder:self];
        [self saveGuideStatus];
        
    }else if (clickType == CKClickTypeCancel){
    //退出
        [CKSaveTools saveClickWithEventType:KB_CoolFont_Cancel_Clicked key:@"KB_CoolFont_Cancel_Clicked"];
        [[CKTracker sharedTraker] logWithCategory:KB_CoolFont_Cancel_Clicked
                                           action:KB_CoolFont_Share_Clicked];
    }
    
    
}
-(void)saveGuideStatus{
    if ([self.delegate respondsToSelector:@selector(coolFontViewChoseFontStyleNumber:coolFont:)]) {
        [self.delegate coolFontViewChoseFontStyleNumber:(int)scoreSelectIndex coolFont:scoreSelectCoolFont];
    }
    
    [CKSaveTools saveString:[NSString stringWithFormat:@"%d",(int)scoreSelectIndex] forKey:@"CoolFontSelectedIndex" inRegion:CKSaveRegionExtensionKeyboard];
    //设置该用户为老用户
    [CKSaveTools saveString:OLD_USER forKey:LOCAL_USER_TYPE inRegion:CKSaveRegionExtensionKeyboard];
    //保存评价状态
    [CKSaveTools saveInteger:AlreadyEvaluated forKey:EVALUATE_STATUS inRegion:CKSaveRegionExtensionKeyboard];
    //保存锁状态
    [CKSaveTools saveInteger:LockStatusOpen forKey:LOCK_STATUS inRegion:CKSaveRegionExtensionKeyboard];
    //保存评价状态到group
    [CKSaveTools saveString:@"EVALUATE_STATUS_GROUP" forKey:EVALUATE_STATUS_GROUP inRegion:CKSaveRegionAppGroup];
    
}



@end
