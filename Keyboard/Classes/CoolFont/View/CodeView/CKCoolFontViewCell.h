//
//  CKCoolFontViewCell.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/29.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKCoolFontModel.h"
@interface CKCoolFontViewCell : UICollectionViewCell
@property (nonatomic, strong) CKCoolFontModel * coolFont;
@end
