//
//  CKCoolFontView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/29.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CoolFontViewDelegate <NSObject>

- (void)coolFontViewChoseFontStyleNumber:(int)index coolFont:(NSString *)coolFont;
- (void)shouldPlaySounds;

@end

@interface CKCoolFontView : UIView

@property (nonatomic,weak) id <CoolFontViewDelegate>delegate;
@end
