//
//  CKCoolFontViewCell.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/29.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKCoolFontViewCell.h"
@interface CKCoolFontViewCell()
{
    UIColor * _normalColor;
    UIColor * _selectedColor;
}
@property (weak, nonatomic) IBOutlet UILabel *fontNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectedButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonToRightContraint;
@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (weak, nonatomic) IBOutlet UIImageView *lockImage;


@end
#define COOL_FONT_SIZE 14
@implementation CKCoolFontViewCell
- (void)setCoolFont:(CKCoolFontModel *)coolFont
{
    _coolFont = coolFont;
    
    self.fontNameLabel.text = coolFont.fontName;
    self.selectedButton.hidden = !coolFont.selected;
    [self setNeedsUpdateConstraints];
    if (!self.selectedButton.hidden)
        self.fontNameLabel.textColor = _selectedColor;
    else
        self.fontNameLabel.textColor = _normalColor;
    self.coverView.hidden = !coolFont.locked;
    self.lockImage.hidden = !coolFont.locked;
}

- (void)updateConstraints
{
    [super updateConstraints];
    
    // 设计师要求,选中图片的最左边要以文字的最右边为起点
    // 1.算出文字的一半长度
    CGSize size = [self.fontNameLabel.text sizeWithFont:[UIFont systemFontOfSize:COOL_FONT_SIZE] maxSize:self.frame.size];
    CGFloat textHalfW = size.width * .5;
    _buttonToRightContraint.constant = self.frame.size.width * .5 - _selectedButton.frame.size.width  - textHalfW;
    self.coverView.layer.masksToBounds=YES;
    self.coverView.layer.cornerRadius = 2;
}
- (void)awakeFromNib
{
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];

    UIImage * selectedImage;
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    if (nowUseTheme) {
        _normalColor = [UIColor colorFromHexString:nowUseTheme.setting_icon_normal];
        _selectedColor = [UIColor colorFromHexString:nowUseTheme.setting_icon_highlight];
    }
    if (!nowUseTheme) {
        _normalColor = [UIColor colorFromHexString:Default_Setting_Normal_Color];
        _selectedColor = [UIColor colorFromHexString:Default_Setting_Selected_Color];
    }
    selectedImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"universal_coolfont_select@2x.png" ofType:nil]]];
    
    self.fontNameLabel.textColor = _normalColor;
    self.fontNameLabel.textAlignment = NSTextAlignmentCenter;
    self.fontNameLabel.backgroundColor  = [UIColor clearColor];
    self.fontNameLabel.font = [UIFont systemFontOfSize:COOL_FONT_SIZE];

    [_selectedButton setImage:[selectedImage tintImageWithColor:_selectedColor] forState:UIControlStateNormal];
    _selectedButton.backgroundColor = [UIColor clearColor];
    _selectedButton.hidden = YES;
    
    self.lockImage.image=[self.lockImage.image tintImageWithColor:_normalColor];
    self.coverView.backgroundColor=_normalColor;
    self.coverView.alpha=0.3;
    
    self.contentView.clipsToBounds = NO;
    self.clipsToBounds = NO;
}

@end
