//
//  CKCoolFonts.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/29.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKCoolFonts.h"
#import "FMDB.h"

@interface CKCoolFonts()
@property (nonatomic, strong) FMDatabase *db;
@property (nonatomic, strong) FMDatabaseQueue *queue;
@end
@implementation CKCoolFonts

singleM(CoolFonts);

- (instancetype)init
{
    NSString * fileName = [[NSBundle mainBundle] pathForResource:@"t_coolFonts.sqlite" ofType:nil];
    self.db = [FMDatabase databaseWithPath:fileName];
    self.queue = [FMDatabaseQueue databaseQueueWithPath:fileName];
    
    [self.queue inDatabase:^(FMDatabase *db) {
        // 1.查询
        FMResultSet *set = [db executeQuery:@"SELECT NAME,MAP FROM FONTSMAP"];
        // 2.取出数据
        NSMutableArray * nameArray = [NSMutableArray array];
        NSMutableArray * mapJsonArray = [NSMutableArray array];
        NSMutableDictionary * dictM = [NSMutableDictionary dictionary];
        while ([set next]) {
            NSString * name = [set stringForColumn:@"NAME"];
            NSString * JSON = [set stringForColumn:@"MAP"];
            NSDictionary * dict = [NSString dictionaryWithJsonString:JSON];
            for (NSString * key in dict.allKeys) {
                dictM[dict[key]] = key;
            }
            if (name) {
                [nameArray addObject:name];
                [mapJsonArray addObject:JSON];
            }
        }
        self.coolFontDict = dictM;
        self.coolFontNameArray = (NSArray *)nameArray;
    }];

    return self;
}

- (NSDictionary *)selectedCoolFontDictWithCoolFontName:(NSString *)coolFontName
{
    NSDictionary * __block dict;
    [self.queue inDatabase:^(FMDatabase *db) {
        // 1.查询
        FMResultSet *set = [db executeQuery:@"SELECT MAP FROM FONTSMAP WHERE NAME=?",coolFontName];
        // 2.取出数据
        while ([set next]) {
            NSString *json = [set stringForColumn:@"MAP"];
            dict = [NSString dictionaryWithJsonString:json];
        }
    }];
    return dict;
}
@end
