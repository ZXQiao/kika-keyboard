//
//  CKAllowFullAccessView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/6.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CKAllowFullAccessView : UIView

// 在allowfullAccessView的后面有一张灰色的视图,将其设置为allowFullAccessView的属性,便于处理
@property (nonatomic, strong) UIView * backView;
// 加这个东西是因为在点击黑色遮罩后会不断累加图层,所以当产生一个accessView后,会禁用gif等view的UserInteration,等移除图层的时候要还原
@property (nonatomic, strong) UIView * frameView;

@end
