//
//  CKAllowFullAccessView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/6.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKAllowFullAccessView.h"
@interface CKAllowFullAccessView()
@property (weak, nonatomic) IBOutlet UIView *bottomView;


@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *lineLabel;

@end

@implementation CKAllowFullAccessView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"CKAllowFullAccessView" owner:self options:nil] lastObject];
        self.frame = frame;
        self.backgroundColor = [UIColor clearColor];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 10;
        
        self.bottomView.layer.masksToBounds = YES;
        self.bottomView.layer.cornerRadius = 10;
        self.secondLabel.text = @"Due to iOS restrictions, please\n allow Full Access to enable\nthemes, smileys and stickers.";
        
        [self bringSubviewToFront:self.lineLabel];
    }
    
    //  @"Due to iOS restrictions, please\n allow Full Access to enable\nthemes👙,smileys😀 and stickers🎑.";
    return self;
}

- (IBAction)buttonClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
            [self removeFromSuperview];
            [self.backView removeFromSuperview];
            self.frameView.userInteractionEnabled = YES;
            for (UIView * view in self.frameView.superview.subviews) {
                view.userInteractionEnabled = YES;
            }
            break;
        case 2:
        {
            NSString *msg= [NSString stringWithFormat:@"Open\n%@ App\nfor more!",APP_NAME];
            [CKCommonTools showInfoByFittingParentView:self msg:msg];
//            UIResponder * responder = self;
//            while ((responder = [responder nextResponder]) != nil) {
//                if ([responder respondsToSelector:@selector(openURL:)] == YES) {
//                    [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@.guide", APP_ROOT_URL]]];
//                }
//            }
//            [self removeFromSuperview];
//            [self.backView removeFromSuperview];
//            self.frameView.userInteractionEnabled = YES;
//            for (UIView * view in self.frameView.superview.subviews) {
//                view.userInteractionEnabled = YES;
//            }
        }
            break;
        default:
            break;
    }
    
}

@end
