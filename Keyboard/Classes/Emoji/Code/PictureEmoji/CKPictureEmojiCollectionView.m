//
//  CKPictureEmojiCollectionView.m
//
//  Created by 张赛 on 14/12/12.
//  Copyright (c) 2014年 xinmeihutong. All rights reserved.
//

#import "CKPictureEmojiCollectionView.h"
#import "CKMemeKeyboardCollectionCell.h"

static NSString * const reuseIdentifier = @"CKMemeKeyboardCollectionCell";

@interface CKPictureEmojiCollectionView ()

@end


@implementation CKPictureEmojiCollectionView

- (UICollectionView *)createCollectionView:(CGRect)frame collectionViewLayout:(UICollectionViewFlowLayout *)flowLayout
{
    UICollectionView * cv = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:flowLayout];
    UINib *nib = [UINib nibWithNibName:reuseIdentifier bundle:nil];
    [cv registerNib:nib forCellWithReuseIdentifier:reuseIdentifier];
    return cv;
}

#pragma mark -- UICollectionViewDataSource

-(CKMemeKeyboardCollectionCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CKMemeKeyboardCollectionCell * cell = [cv dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CKMemeKeyboardCollectionCell alloc] init];
    }

    cell.memeImageView.image = [UIImage imageNamed:emojiArray[indexPath.item]];
    cell.memeImageView.backgroundColor = [UIColor clearColor];
    
    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout
- (void)testUmengEmoji
{
    for (int i = 0; i < 100; i ++) {
        NSString * st = [NSString stringWithFormat:@"张赛测试:Sticker%d次",i];
        [CKSaveTools saveClickWithEventType:[NSString stringWithFormat:KB_Click_With_Format, resourceType] key:st];
    }
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    [self testUmengEmoji];
    [CKSaveTools saveInteger:SelectedEmojiInput forKey:SELECT_INPUT inRegion:CKSaveRegionExtensionKeyboard];
    [[NSNotificationCenter defaultCenter] postNotificationName:UserHaveDoneSomethingNotification object:nil];
    if (![CKCommonTools isOpenAccessGranted]) {
        float h = self.frame.size.height;
        if (iPhone4Land || iPhone5Land || iPhone6Land) {
            h = h * 1.3;
        }
        float w = h / 2 * 3;
        [CKCommonTools showFullAccessViewWithView:self frame:CGRectMake(0, 0, w, h)];
        return;
    }

    NSString * pictureName = emojiArray[indexPath.item];
//    [[NSNotificationCenter defaultCenter] postNotificationName:CK_UM_SEND_MESSAGE_TO_CONTANING_APP_NOTIFICATION object:@{UM_Event_Key : keyString,UM_Event_Value:pictureName}];
  NSString * keyName = [pictureName stringByReplacingOccurrencesOfString:@"@2x.jpg" withString:@""];
  keyName = [keyName stringByReplacingOccurrencesOfString:@".jpg" withString:@""];
  keyName = [keyName stringByReplacingOccurrencesOfString:@"@2x.png" withString:@""];
  keyName = [keyName stringByReplacingOccurrencesOfString:@".png" withString:@""];

    [CKSaveTools saveClickWithEventType:[NSString stringWithFormat:KB_Click_With_Format, resourceType] key:keyName];
    [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                       action:[NSString stringWithFormat:KB_Click_With_Format, resourceType]
                                        label:keyName];
    
    NSString * path = [[NSBundle mainBundle] pathForResource:pictureName ofType:nil];
    UIImage * image = [UIImage imageWithContentsOfFile:path];
    if (image) {
        [CKBackend setStickerUsedInApp:YES];
        [CKCommonTools showInfoByFittingParentView:self msg:@"Now paste it \nin a message!"];
        [self updateRecentArray:pictureName];
        UIPasteboard *pasteBoard=[UIPasteboard generalPasteboard];
        pasteBoard.image = image;
    }
    else
    {
        return;
    }
}

@end
