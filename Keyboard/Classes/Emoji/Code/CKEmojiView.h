//
//  CKEmojiView.h
//  emojiButtonView
//
//  Created by 张赛 on 14/12/11.
//  Copyright (c) 2014年 xinmeihutong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CKMainButton,CKEmojiCollectionView;


@protocol CKEmojiViewDelegate <NSObject>

- (void)iKeyboardEmojiButtonClicked:(CKMainButton *)button;
- (void)iKeyboardEmojiBottomViewButtonClicked:(NSInteger)tag;
- (void)shouldPlaySounds;
@end


@interface CKEmojiView : UIView

@property (nonatomic, weak) id <CKEmojiViewDelegate> iKeyboardEmojiDelegate;

- (instancetype)initWithFrame:(CGRect)frame lastResourceType:(NSString*)lastResourceType delegate:(id <CKEmojiViewDelegate>)delegate;

- (NSString *)getResourceType;

@end
