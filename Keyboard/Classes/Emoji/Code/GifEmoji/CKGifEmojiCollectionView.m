//
//  CKGifEmojiCollectionView.m
//
//  Created by 张赛 on 14/12/12.
//  Copyright (c) 2014年 xinmeihutong. All rights reserved.
//

#import "CKGifEmojiCollectionView.h"
#import "CKGifEmojiCell.h"
#import "YLGIFImage.h"
#import "YLImageView.h"

static NSString * const reuseIdentifier = @"CKGifEmojiCell";

@interface CKGifEmojiCollectionView ()

@end


@implementation CKGifEmojiCollectionView

- (UICollectionView *)createCollectionView:(CGRect)frame collectionViewLayout:(UICollectionViewFlowLayout *)flowLayout
{
    UICollectionView * cv = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:flowLayout];
    UINib *nib = [UINib nibWithNibName:@"CKGifEmojiCell" bundle:nil];
    [cv registerNib:nib forCellWithReuseIdentifier:reuseIdentifier];
    //[cv registerClass:[CKGifEmojiCell class] forCellWithReuseIdentifier:reuseIdentifier];
    return cv;
}

#pragma mark -- UICollectionViewDataSource

-(CKGifEmojiCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CKGifEmojiCell * cell = [cv dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[CKGifEmojiCell alloc] init];
    }
    
    NSString * gifName = emojiArray[indexPath.item];
    cell.gifView.image = [YLGIFImage imageNamed:gifName];
    // gifView设置为可交互
    cell.gifView.userInteractionEnabled = YES;
    
    return cell;
}

#pragma mark --UICollectionViewDelegate
- (void)testUmengEmoji
{
    for (int i = 0; i < 100; i ++) {
        NSString * st = [NSString stringWithFormat:@"测试:Gif%d次",i];
        [CKSaveTools saveClickWithEventType:[NSString stringWithFormat:KB_Click_With_Format, resourceType] key:st];
    }
}


//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [CKSaveTools saveInteger:SelectedEmojiInput forKey:SELECT_INPUT inRegion:CKSaveRegionExtensionKeyboard];
    [[NSNotificationCenter defaultCenter] postNotificationName:UserHaveDoneSomethingNotification object:nil];
    
    if (![CKCommonTools isOpenAccessGranted]) {
        float h = self.frame.size.height;
        if (iPhone4Land || iPhone5Land || iPhone6Land){
            h = h * 1.3;
        }
        float w = h / 2 * 3;
        [CKCommonTools showFullAccessViewWithView:self frame:CGRectMake(0, 0, w, h)];
        return;
    }
    
    NSString * gifName = emojiArray[indexPath.item];
    NSFileManager * myFileManger = [NSFileManager defaultManager];
    NSRange range = [gifName rangeOfString:@"small"];
    NSURL  * gifURL;
    if(range.location != NSNotFound)
    {
        gifName = [gifName stringByReplacingOccurrencesOfString:@"small" withString:@"big"];
        gifURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:gifName ofType:nil]];
        if (![myFileManger fileExistsAtPath:gifURL.absoluteString]) {
            gifName = [gifName stringByReplacingOccurrencesOfString:@"big" withString:@"small"];
        }
    }
    else if([gifName rangeOfString:@"minion"].location != NSNotFound){
        gifName = [gifName stringByReplacingOccurrencesOfString:@".jpg" withString:@".gif"];
        gifURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:gifName ofType:nil]];
    }
    NSData *data = [NSData dataWithContentsOfURL:gifURL];
    if (data != nil) {
        [CKBackend setGifUsedInApp:YES];
        NSString * keyName = [gifName stringByReplacingOccurrencesOfString:@".gif" withString:@""];
        [CKSaveTools saveClickWithEventType:[NSString stringWithFormat:KB_Click_With_Format, resourceType] key:keyName];
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY
                                           action:[NSString stringWithFormat:KB_Click_With_Format, resourceType]
                                            label:keyName];

        [CKCommonTools showInfoByFittingParentView:self msg:@"Now paste it \nin a message!"];
        if([gifName rangeOfString:@"minion"].location != NSNotFound)
        {
            gifName = [gifName stringByReplacingOccurrencesOfString:@".gif" withString:@".jpg"];
            [self updateRecentArray:gifName];
        }
        else{
           [self updateRecentArray:gifName];
        }

        UIPasteboard *pasteBoard=[UIPasteboard generalPasteboard];
        [pasteBoard setData:data forPasteboardType:@"com.compuserve.gif"];
    }
//    [[NSNotificationCenter defaultCenter] postNotificationName:CK_UM_SEND_MESSAGE_TO_CONTANING_APP_NOTIFICATION object:@{UM_Event_Key : UM_KB_Sticker_Click,UM_Event_Value:gifName}];
}

@end
