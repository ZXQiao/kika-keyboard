//
//  CKGifEmojiCell.h
//
//  Created by 张赛 on 14-9-5.
//  Copyright (c) 2014年 com.xinmei365.ikey. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YLImageView;


@interface CKGifEmojiCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet YLImageView * gifView;

@end
