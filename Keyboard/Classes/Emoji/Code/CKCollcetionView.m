//
//  CKCollcetionView.m
//
//  Created by 张赛 on 15/5/15.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import "CKCollcetionView.h"
#import "CKButtonEmojiCell.h"
#import "CKMainButton.h"

@interface CKCollcetionView()

@end


@implementation CKCollcetionView

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint location = [[touches anyObject] locationInView:self];
    NSArray * cellArray =  self.visibleCells;
    for (CKButtonEmojiCell * cell in cellArray) {
        if(CGRectContainsPoint(cell.frame, location)){
            CKMainButton * button = cell.emojiButton;
            button.outPutToTextFiledStr = button.currentTitle;
            if ([self.CKDelegate respondsToSelector:@selector(cellButtonClicked:)]) {
                [self.CKDelegate cellButtonClicked:button];
            }
            break;
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([self.CKDelegate respondsToSelector:@selector(tellEmojiPlaySongs)] ) {
        [self.CKDelegate tellEmojiPlaySongs];
    }
    
    CGPoint location = [[touches anyObject] locationInView:self];
    NSArray * cellArray =  self.visibleCells;
    for (CKButtonEmojiCell * cell in cellArray) {
        if(CGRectContainsPoint(cell.frame, location)){
            CKMainButton * button = cell.emojiButton;
            if (!button.inputLabel) {
                UIView * view = [[UIView alloc] init];
                view.bounds = CGRectMake(0, 0, button.frame.size.width, button.frame.size.height);
                button.inputLabel = view;
                [cell.contentView insertSubview:view belowSubview:button];
                view.center = button.center;
                view.layer.masksToBounds = YES;
                view.layer.cornerRadius = 5;
                view.backgroundColor = [UIColor colorFromHexString:@"#ffffff"];
                view.alpha = .2;
                [self performSelector:@selector(backViewRemoved:) withObject:button afterDelay:.2];
            }

//            button.transform = CGAffineTransformMakeScale(0.8, 0.8);
//            [UIView animateWithDuration:.3 animations:^{
//                button.transform = CGAffineTransformMakeScale(1.7, 1.7);
//            } completion:^(BOOL finished) {
//                button.transform = CGAffineTransformMakeScale(1, 1);
//            }];
            break;
        }
    }
}

- (void)backViewRemoved:(CKMainButton *)button{
    [button.inputLabel removeFromSuperview];
    button.inputLabel = nil;
    CKLog(@"%@===button",button.inputLabel);
}


- (void)drawRect:(CGRect)rect
{

}
@end
