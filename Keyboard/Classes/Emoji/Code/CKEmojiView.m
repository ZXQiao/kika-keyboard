//
//  CKEmojiView.m
//  emojiButtonView
//
//  Created by 张赛 on 14/12/11.
//  Copyright (c) 2014年 xinmeihutong. All rights reserved.
//

#import "CKEmojiView.h"
#import "CKEmojiCollectionView.h"
#import "CKButtonEmojiCollectionView.h"
#import "CKPictureEmojiCollectionView.h"
#import "CKGifEmojiCollectionView.h"
#import "CKEmojiBottomView.h"
#import "CKKeyboardUImanger.h"


@interface CKEmojiView () <ButtonEmojiCollectionViewDelegate, EmojiBottomViewDelegate>

{
    NSArray * resourceTypes;
    NSString * resourceType;
    NSMutableArray * categoryButtons;
    NSArray * emojiCategories;
    CKKeyboardUImanger * UIManger;
    UIImageView * topImageView;
    UIButton * selectedBackButton;
    CKEmojiCollectionView * emojiCollectionView;
    CKEmojiBottomView * bottomView;
    UIButton * selectedCategoryButton;
    CGFloat topBarHeight;
    CGFloat bottomViewHeight;
}

@end


@implementation CKEmojiView

- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"emoji_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * realDimensions = [[CKKeyboardUImanger shareUImanger] getRealDimensionsByModelAndDirection:dimensions];
    bottomViewHeight = [realDimensions[@"bottomViewHeight"] floatValue];
}

- (void)initResourceTypes:(NSString*)lastResourceType
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"resourceTypes.plist" ofType:nil];
    resourceTypes = [NSArray arrayWithContentsOfFile:resourceTypePath];
    if (lastResourceType == nil || [lastResourceType isEqualToString:@""]) {
#if (APP_VARIANT_NAME == APP_VARIANT_NEWMEMOJI)
        resourceType = resourceTypes[1];
#else
        resourceType = resourceTypes[0];
#endif
    }
    else {
        resourceType = lastResourceType;
    }
    if ([resourceType isEqualToString:@"Emoji"]) {
        if([CKCommonTools systemVersion] - 8.2 < FLOAT_EPSINON) {
            resourceType = @"Emoji8_2";
        }
        else if([CKCommonTools systemVersion] - 9.1 > -FLOAT_EPSINON) {
            resourceType = @"Emoji9_1";
        }
    }
}

- (void)refreshCategory
{
    NSString * categoryPath = [[NSBundle mainBundle] pathForResource:[resourceType stringByAppendingString:@"Category.plist"] ofType:nil];
    emojiCategories = [NSArray arrayWithContentsOfFile:categoryPath];
}

- (void)refreshCategoryButtonArray
{
    if (categoryButtons.count > 0) {
        for (int i = 0 ; i < categoryButtons.count; i ++) {
            [categoryButtons[i] removeFromSuperview];
        }
        [categoryButtons removeAllObjects];
    }
    else {
        categoryButtons = [NSMutableArray array];
    }
    for (int i = 0 ; i < emojiCategories.count; i ++) {
        UIButton * button = [[UIButton alloc] init];
        button.tag = i;
        [self addSubview:button];
        [categoryButtons addObject:button];
    }
}

- (void)refreshSelectedBackgroundButton
{
    if (selectedBackButton != nil) {
        [selectedBackButton removeFromSuperview];
        selectedBackButton = nil;
    }
    selectedBackButton = [[UIButton alloc] init];
    selectedBackButton.enabled = NO;
    UIImage * selectBackImage;
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    if (nowUseTheme) {
        if (nowUseTheme.isOnline) {
            selectBackImage = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emoji_select" resize:NO];
        }
        else{
            selectBackImage = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emoji_select" resize:NO];
        }
    }
    if (!nowUseTheme) {
        selectBackImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_emoji_select@2x.png",Default_Theme_Name] ofType:nil]]];
    }
    [selectedBackButton setImage:selectBackImage forState:UIControlStateNormal];
    [selectedBackButton setImage:selectBackImage forState:UIControlStateHighlighted];
    selectedBackButton.adjustsImageWhenDisabled = NO;
    [self addSubview:selectedBackButton];
}

- (void)refreshTopView
{
    if (topImageView != nil) {
        [topImageView removeFromSuperview];
        topImageView = nil;
    }
    topImageView = [[UIImageView alloc] init];
    [self addSubview:topImageView];
    [self refreshSelectedBackgroundButton];
    [self refreshCategoryButtonArray];

    UIImage * topImage;
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    if (nowUseTheme) {
        if (nowUseTheme.isOnline) {
            topImage = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emojibar" resize:YES];
        }else
        {
            topImage = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emojibar" resize:YES];
        }
    }
     topImageView.image = topImage;
    topImageView.frame = CGRectMake(0, 0, self.frame.size.width,topBarHeight);

    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray * recentArray = [userDefaults objectForKey:[resourceType stringByAppendingString:@"RecentArray"]];
    int categoryIndex = [[userDefaults objectForKey:[resourceType stringByAppendingString:@"CategoryIndex"]] intValue];
    if (categoryIndex == 0 && recentArray.count == 0) {
        categoryIndex = 1;
    }
    UIButton * lastClickButton = categoryButtons[categoryIndex];
    [self categoryButtonClickedWithoutSound:lastClickButton];
    CGFloat Y = 0;
    CGFloat W = self.frame.size.width / emojiCategories.count;
    CGFloat H = topBarHeight;
    selectedBackButton.frame = CGRectMake(categoryIndex * W, Y, W, H);
}

- (void)refreshCollectionView
{
    if (emojiCollectionView != nil) {
        [emojiCollectionView removeFromSuperview];
        emojiCollectionView = nil;
    }
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:[resourceType stringByAppendingString:@"Misc.plist"] ofType:nil];
    NSDictionary * misc = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSString * emojiStyle = misc[@"EmojiStyle"];
    if ([emojiStyle isEqualToString:@"ButtonEmoji"]) {
        CKButtonEmojiCollectionView * buttonEmojiCollectionView = [[CKButtonEmojiCollectionView alloc] initWithFrame:CGRectMake(0, topBarHeight, self.frame.size.width, self.frame.size.height - topBarHeight - bottomViewHeight) type:resourceType];
        buttonEmojiCollectionView.emojiButtonDelegate = self;
        emojiCollectionView = buttonEmojiCollectionView;
    }
    else if ([resourceType isEqualToString:@"Gif"]) {
    emojiCollectionView = [[CKGifEmojiCollectionView alloc] initWithFrame:CGRectMake(0, topBarHeight, self.frame.size.width, self.frame.size.height - topBarHeight - bottomViewHeight) type:resourceType];
    }
    else if ([emojiStyle isEqualToString:@"PictureEmoji"]) {
        emojiCollectionView = [[CKPictureEmojiCollectionView alloc] initWithFrame:CGRectMake(0, topBarHeight, self.frame.size.width, self.frame.size.height - topBarHeight - bottomViewHeight) type:resourceType];
    }
    emojiCollectionView.backgroundColor = [UIColor clearColor];
    [self addSubview:emojiCollectionView];
}

- (void)emojiShouldPlaySounds
{
    if ([self.iKeyboardEmojiDelegate respondsToSelector:@selector(shouldPlaySounds)]) {
        [self.iKeyboardEmojiDelegate shouldPlaySounds];
    }
}
- (void)refreshAll
{
    [CKSaveTools saveClickWithEventType:KB_Emoji_Class_Click key:resourceType];
    [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                       action:KB_Emoji_Class_Click
                                        label:resourceType];
    // refresh data
    [self refreshCategory];
    // refresh views
    [self refreshCollectionView];
    [self refreshTopView];
}

- (void)addBottomView
{
    bottomView = [[CKEmojiBottomView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - bottomViewHeight, self.frame.size.width, bottomViewHeight) types:resourceTypes type:resourceType];
    [self addSubview:bottomView];
    //todo: bad smell here
    KeyboardViewController * controller = (KeyboardViewController *)(self.iKeyboardEmojiDelegate);
    [bottomView.deleteButton addGestureRecognizer:controller.longPressGestureRecognizer];
    bottomView.delegate = self;
}

- (instancetype)initWithFrame:(CGRect)frame lastResourceType:(NSString*)lastResourceType delegate:(id <CKEmojiViewDelegate>)delegate
{
    self = [super initWithFrame:frame];
    if (self) {
        [CKBackend setSwitchKeyboardType:CKSwitchKeyboardTypeEmoji];
        UIManger = [CKKeyboardUImanger shareUImanger];
        if (![CKCommonTools screenDirectionIsLand]) { // 竖屏
            topBarHeight = self.frame.size.height - UIManger.portraitHeightKeyboard;
        }
        else {
            topBarHeight = self.frame.size.height - UIManger.landscapeHeightKeyboard;
        }

        self.backgroundColor = [UIColor clearColor];
        self.iKeyboardEmojiDelegate = delegate;

        [self initDimensions];
        [self initResourceTypes:lastResourceType];
        [self refreshAll];
        [self addBottomView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    UIColor * normalColor;
    UIColor * highLightColor;
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    if (nowUseTheme) {
            normalColor = [UIColor colorFromHexString:nowUseTheme.emoji_icon_normal];
            highLightColor = [UIColor colorFromHexString:nowUseTheme.emoji_icon_highlight];
}
    else{
        normalColor = [UIColor colorFromHexString:Default_Emoji_Normal_Color];
        highLightColor = [UIColor colorFromHexString:Default_Emoji_Highed_Color];
    }

    CGFloat Y = 0;
    CGFloat W = self.frame.size.width / categoryButtons.count;
    CGFloat H = topBarHeight;

    for (int i = 0; i < categoryButtons.count; i ++) {
        UIButton * button = categoryButtons[i];
        button.frame = CGRectMake(W * i, Y, W, H);
        NSString * imageName = emojiCategories[i];
        if ([imageName isEqualToString:@"history"] && ([resourceType isEqualToString:@"Gif"] || [resourceType isEqualToString:@"Sticker"])) {
            imageName = @"gif_history";
        }
        UIImage * image = [UIImage imageNamed:imageName];
        if ([resourceType isEqualToString:@"Emoji"]) {
            [button setImage:[image imageWithTintColor:normalColor] forState:UIControlStateNormal];
            [button setImage:[image imageWithTintColor:highLightColor] forState:UIControlStateSelected];
        }
        else {
            [button setImage:image forState:UIControlStateNormal];
            [button setImage:image forState:UIControlStateSelected];
        }
        [button addTarget:self action:@selector(categoryButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)play
{
    KeyboardViewController * controller = (KeyboardViewController *)(self.iKeyboardEmojiDelegate);
    [controller shouldPlaySounds];
}

- (void)emojiBottomViewButtonClicked:(NSInteger)tag
{
    if (tag < BOTTOM_VIEW_FUNCTION_BUTTON_NUM) {
        if ([self.iKeyboardEmojiDelegate respondsToSelector:@selector(iKeyboardEmojiButtonClicked:)]) {
            [self.iKeyboardEmojiDelegate iKeyboardEmojiBottomViewButtonClicked:tag];
        }
    }
    else {
        if ([resourceTypes[tag - BOTTOM_VIEW_FUNCTION_BUTTON_NUM] isEqualToString:resourceType]) {
            return;
        }
        [self play];
        resourceType = resourceTypes[tag - BOTTOM_VIEW_FUNCTION_BUTTON_NUM];
        if ([resourceType isEqualToString:@"Emoji"]) {
            if([CKCommonTools systemVersion] - 8.2 < FLOAT_EPSINON) {
                resourceType = @"Emoji8_2";
            }
            else if([CKCommonTools systemVersion] - 9.1 > -FLOAT_EPSINON) {
                resourceType = @"Emoji9_1";
            }
        }
        [self refreshAll];
    }
}

- (void)categoryButtonClickedWithoutSound:(UIButton *)button
{
    if (button == selectedCategoryButton) {
        return;
    }
    [self realCategoryButtonClicked:button];
}

- (void)categoryButtonClicked:(UIButton *)button
{
    if (button == selectedCategoryButton) {
        return;
    }
    [self play];
    [self realCategoryButtonClicked:button];
}

- (void)realCategoryButtonClicked:(UIButton *)button
{
    selectedCategoryButton.selected = NO;
    button.selected = YES;
    selectedCategoryButton = button;
    
    NSInteger buttonTag = button.tag;
    NSString * buttonTagStr = [NSString stringWithFormat:@"%d",(int)buttonTag];
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:buttonTagStr forKey:[resourceType stringByAppendingString:@"CategoryIndex"]];
    [userDefaults synchronize];
    
    NSString * categoryStr = emojiCategories[buttonTag];
    UIButton * currentButton = categoryButtons[buttonTag];
    [UIView animateWithDuration:.1f animations:^{
        selectedBackButton.frame = currentButton.frame;
    }];
    [CKSaveTools saveClickWithEventType:[NSString stringWithFormat:KB_Category_Click_With_Format, resourceType] key:categoryStr];
    [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                       action:[NSString stringWithFormat:KB_Category_Click_With_Format, resourceType]
                                        label:categoryStr];
    emojiCollectionView.categoryStr = categoryStr;
}

- (void)buttonEmojiCollectinoViewClicked:(CKMainButton *)button
{
    if ([self.iKeyboardEmojiDelegate respondsToSelector:@selector(iKeyboardEmojiButtonClicked:)]) {
        [self.iKeyboardEmojiDelegate iKeyboardEmojiButtonClicked:button];
    }
}
- (NSString *)getResourceType
{
    return resourceType;
}

@end
