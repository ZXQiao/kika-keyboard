//
//  CKEmojiBottomView.m
//
//  Created by 张赛 on 15/5/12.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKEmojiBottomView.h"
#import "CKKeyboardUImanger.h"

@interface CKEmojiBottomView ()
{
UIButton * backButton;
UIButton * globalButton;
UIButton * selectedButton;
NSMutableArray * emojiButtons;
CGFloat bottomViewHeight;
CGFloat bottomViewButtonHeight;
CGFloat bottomViewTopPadding;
CGFloat bottomViewResFuncPadding;
CGFloat bottomViewFuncButtonWidth;
CGFloat emojiButtonMinWidth;
UIScrollView * wordScrollView;
UIButton * _selectedBackButton;
}

@end

@implementation CKEmojiBottomView

- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"emoji_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * realDimensions = [[CKKeyboardUImanger shareUImanger] getRealDimensionsByModelAndDirection:dimensions];
    bottomViewHeight = [realDimensions[@"bottomViewHeight"] floatValue];
    bottomViewButtonHeight = [realDimensions[@"bottomViewButtonHeight"] floatValue];
    bottomViewTopPadding = [realDimensions[@"bottomViewTopPadding"] floatValue];
    bottomViewResFuncPadding = [realDimensions[@"bottomViewResFuncPadding"] floatValue];
    bottomViewFuncButtonWidth = [realDimensions[@"bottomViewFuncButtonWidth"] floatValue];
    emojiButtonMinWidth = bottomViewHeight * 1.2;
}

- (instancetype)initWithFrame:(CGRect)frame types:(NSArray *)resourceTypes type:(NSString *)resType
{
    self = [super initWithFrame:frame];

    if (self) {
        [self initDimensions];
        [self refreshSelectedBackgroundButton];
        CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
        UIImage * bottomBarBackImage;
        UIImage * deleteImage;
        UIImage * globalImage;
        NSString * fontName;

        UIColor * normalColor;
        UIColor * highLightColor;

        if (nowUseTheme) {
            if (nowUseTheme.isOnline) {
                bottomBarBackImage = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emojibar" resize:YES];
            }
            else
            {
                bottomBarBackImage = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emojibar" resize:YES];
            }
            fontName = nowUseTheme.fontName;
            normalColor = [UIColor colorFromHexString:nowUseTheme.emoji_icon_normal];
            highLightColor = [UIColor colorFromHexString:nowUseTheme.emoji_icon_highlight];

        }
        if (!nowUseTheme || !bottomBarBackImage) {
            bottomBarBackImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_emojibar@2x.png",Default_Theme_Name] ofType:nil]]];
            fontName = Default_FontName;
            normalColor = [UIColor colorFromHexString:Default_Emoji_Normal_Color];
            highLightColor = [UIColor colorFromHexString:Default_Emoji_Highed_Color];
        }

        deleteImage = [UIImage imageNamed:@"universal_delete_in"];
        globalImage = [UIImage imageNamed:@"universal_global_in"];

        self.userInteractionEnabled = YES;
        self.image = bottomBarBackImage;
        
        UIImage * functionButtonBgImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"function_button_bg@2x.png" ofType:nil]]];
        UIImage * functionButtonPressedImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"function_button_pressed@2x.png" ofType:nil]]];

        backButton = [[UIButton alloc] init];
        backButton.adjustsImageWhenHighlighted = NO;
        [self addSubview:backButton];
        backButton.tag = 0;
        [backButton setBackgroundImage:functionButtonPressedImage forState:UIControlStateHighlighted];
        [backButton setBackgroundImage:functionButtonBgImage forState:UIControlStateNormal];
        [backButton setTitle:@"ABC" forState:UIControlStateNormal];
       
        CGFloat defaultFontSize = [[CKKeyboardUImanger shareUImanger] getFunctionFontSize:fontName];
        UIFont * font = [UIFont fontWithName:buttonDefaultFontName size:defaultFontSize];
        if ([fontName isEqualToString:@"LithosPro-Black"]) {
            font = [UIFont fontWithName:fontName size:defaultFontSize];
        }else if([fontName isEqualToString:@"Chalkboard SE"])
        {
            font = [UIFont fontWithName:fontName size:defaultFontSize];
        }
        backButton.titleLabel.font = font;
//        [backButton setTitleColor:[UIColor colorFromHexString:FontColor] forState:UIControlStateNormal];
         [backButton setTitleColor:normalColor forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        globalButton = [[UIButton alloc] init];
        [self addSubview:globalButton];
        globalButton.adjustsImageWhenHighlighted = NO;
        globalButton.tag = 1;
        [globalButton setImage:[globalImage tintImageWithColor:normalColor] forState:UIControlStateNormal];
        [globalButton setBackgroundImage:functionButtonPressedImage forState:UIControlStateHighlighted];
        [globalButton setBackgroundImage:functionButtonBgImage forState:UIControlStateNormal];
        [globalButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

        UIButton * deleteButton = [[UIButton alloc] init];
        self.deleteButton = deleteButton;
        [self addSubview:deleteButton];
        deleteButton.adjustsImageWhenHighlighted = NO;
        deleteButton.tag = 2;
        [deleteButton setBackgroundImage:functionButtonPressedImage  forState:UIControlStateHighlighted];
        [deleteButton setBackgroundImage:functionButtonBgImage forState:UIControlStateNormal];
        [deleteButton setImage:[deleteImage tintImageWithColor:normalColor] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

        CGFloat resourceTypeAreaWidth = self.frame.size.width - bottomViewFuncButtonWidth * BOTTOM_VIEW_FUNCTION_BUTTON_NUM - bottomViewResFuncPadding * 2;
        if (resourceTypes.count * emojiButtonMinWidth > resourceTypeAreaWidth) {
            wordScrollView = [[UIScrollView alloc] init];
            wordScrollView.backgroundColor = [UIColor clearColor];
            [self addSubview:wordScrollView];
            wordScrollView.bounces = NO;
            wordScrollView.pagingEnabled = YES;
            wordScrollView.showsHorizontalScrollIndicator = NO;
            wordScrollView.scrollEnabled = NO;
            UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
            rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
            UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
            leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
            [wordScrollView addGestureRecognizer:leftRecognizer];
            [wordScrollView addGestureRecognizer:rightRecognizer];
        }
        else {
            wordScrollView = nil;
        }
        emojiButtons = [NSMutableArray array];
        for (int i = 0; i < resourceTypes.count; i ++) {
            UIButton * button = [[UIButton alloc] init];
            if (wordScrollView != nil) {
                [wordScrollView addSubview:button];
            }
            else {
                [self addSubview:button];
            }
            if ([resType containsString:resourceTypes[i]]) {
                button.selected = YES;
                selectedButton = button;
//                [self bottomButtonClicked:button];
            }
            [emojiButtons addObject:button];
            button.adjustsImageWhenHighlighted = NO;
            button.tag = BOTTOM_VIEW_FUNCTION_BUTTON_NUM + i;
            [button addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            NSString * resourceType = resourceTypes[i];
            UIImage * buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"universal_%@@2x.png", resourceType.lowercaseString] ofType:nil]];
            UIImage * buttonImagePressed = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"universal_%@_pressed@2x.png", resourceType.lowercaseString] ofType:nil]];
            [button setImage:[buttonImage tintImageWithColor:normalColor] forState:UIControlStateNormal];
            [button setImage:[buttonImagePressed tintImageWithColor:highLightColor] forState:UIControlStateSelected];
            
        }
    }
    return self;
}

- (void)refreshSelectedBackgroundButton
{
    if (_selectedBackButton != nil) {
        [_selectedBackButton removeFromSuperview];
        _selectedBackButton = nil;
    }
    _selectedBackButton = [[UIButton alloc] init];
    _selectedBackButton.enabled = NO;
    UIImage * selectBackImage;
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    if (nowUseTheme) {
        if (nowUseTheme.isOnline) {
            selectBackImage = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emoji_select" resize:NO];
        }
        else{
            selectBackImage = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emoji_select" resize:NO];
        }
    }
    if (!nowUseTheme) {
        selectBackImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_emoji_select@2x.png",Default_Theme_Name] ofType:nil]]];
    }
    [_selectedBackButton setImage:selectBackImage forState:UIControlStateNormal];
    [_selectedBackButton setImage:selectBackImage forState:UIControlStateHighlighted];
    _selectedBackButton.adjustsImageWhenDisabled = NO;
    [self addSubview:_selectedBackButton];
}

- (void)bottomButtonClicked:(UIButton *)button
{
    
    if (button.tag > 2) {
        if (button == selectedButton) {
            return;
        }
        selectedButton.selected = NO;
        selectedButton = button;
        button.selected = YES;
        _selectedBackButton.frame = CGRectMake(0, 0, button.frame.size.width, button.frame.size.height);
        _selectedBackButton.center = button.center;
    }
    if (button.tag == 0) { // 如果按下了ABC按钮,要跟topToolView说一声,让topToolView检查一下Emoji有没有按下东西
        [[NSNotificationCenter defaultCenter] postNotificationName:UserClickBottomBackButtonNotification object:@"emoji"];
    }
    if ([self.delegate respondsToSelector:@selector(emojiBottomViewButtonClicked:)]) {
        [self.delegate emojiBottomViewButtonClicked:button.tag];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    backButton.frame = CGRectMake(0, bottomViewTopPadding, bottomViewFuncButtonWidth, bottomViewButtonHeight);
    
    CGFloat deleteX = self.frame.size.width - bottomViewFuncButtonWidth;
    self.deleteButton.frame = CGRectMake(deleteX, bottomViewTopPadding, bottomViewFuncButtonWidth, bottomViewButtonHeight);
    
    CGFloat globalX = deleteX - bottomViewFuncButtonWidth;
    globalButton.frame = CGRectMake(globalX, bottomViewTopPadding, bottomViewFuncButtonWidth, bottomViewButtonHeight);

    CGFloat emojiButtonW;
    CGFloat emojiButtonX;
    if (wordScrollView != nil) {
        emojiButtonW = emojiButtonMinWidth;
        emojiButtonX = 0;
    }
    else {
        emojiButtonW = (globalX - bottomViewFuncButtonWidth - bottomViewResFuncPadding * 2) / emojiButtons.count;
        emojiButtonX = bottomViewFuncButtonWidth + bottomViewResFuncPadding;
    }
    for (int i = 0; i < emojiButtons.count; i ++) {
        UIButton * button = emojiButtons[i];
        if (i > 0) {
            UIButton * preButton = emojiButtons[i - 1];
            emojiButtonX = CGRectGetMaxX(preButton.frame);
        }
        button.frame = CGRectMake(emojiButtonX, bottomViewTopPadding, emojiButtonW, bottomViewButtonHeight);
    }
    if (wordScrollView != nil) {
        wordScrollView.frame = CGRectMake(bottomViewFuncButtonWidth + bottomViewResFuncPadding, bottomViewTopPadding, globalX - CGRectGetMaxX(backButton.frame) - bottomViewResFuncPadding * 2, bottomViewButtonHeight);
        wordScrollView.contentSize = CGSizeMake(emojiButtons.count * emojiButtonMinWidth, 0);
        wordScrollView.contentOffset = CGPointMake(0, 0);
    }
    _selectedBackButton.frame = CGRectMake(0, 0, selectedButton.frame.size.width, selectedButton.frame.size.height);
    _selectedBackButton.center = selectedButton.center;
}

- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
    CGFloat frameWidth =  wordScrollView.frame.size.width;
    if(recognizer.direction==UISwipeGestureRecognizerDirectionRight) {
        CGFloat offsetX = wordScrollView.contentOffset.x - frameWidth / 2;
        if (offsetX >= 0) {
            wordScrollView.contentOffset = CGPointMake(offsetX, 0);
        }
        else {
            wordScrollView.contentOffset = CGPointMake(0, 0);
        }
    }

    if(recognizer.direction==UISwipeGestureRecognizerDirectionLeft) {
        CGFloat offsetX = wordScrollView.contentOffset.x + frameWidth / 2;
        CGFloat maxAllX = emojiButtons.count * emojiButtonMinWidth;
        if (maxAllX - offsetX >= wordScrollView.frame.size.width) {
            wordScrollView.contentOffset = CGPointMake(offsetX, 0);
        }
        else {
            wordScrollView.contentOffset = CGPointMake(maxAllX - frameWidth, 0);
        }
    }
}

@end
