//
//  CKButtonEmojiCollectionView.h
//
//  Created by 张赛 on 14/12/12.
//  Copyright (c) 2014年 xinmeihutong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKEmojiCollectionView.h"

@class CKMainButton;


@protocol ButtonEmojiCollectionViewDelegate <NSObject>

- (void)buttonEmojiCollectinoViewClicked:(CKMainButton *)button;
- (void)emojiShouldPlaySounds;

@end


@interface CKButtonEmojiCollectionView : CKEmojiCollectionView

@property (nonatomic, weak) id <ButtonEmojiCollectionViewDelegate> emojiButtonDelegate;

- (instancetype)initWithFrame:(CGRect)frame type:(NSString *)resourceType;

@end
