//
//  CKButtonEmojiCell.m
//
//  Created by 张赛 on 14-9-5.
//  Copyright (c) 2014年 com.xinmei365.ikey. All rights reserved.
//

#import "CKButtonEmojiCell.h"
#import "CKMainButton.h"


@interface CKButtonEmojiCell()

@end


@implementation CKButtonEmojiCell

-(CKMainButton *)emojiButton
{
    if (!_emojiButton)
    {
        _emojiButton = [[CKMainButton alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:_emojiButton];
        _emojiButton.buttonStyle = CKEnglishButtonStyleEmoji;
    }
    return _emojiButton;
}

@end
