//
//  CKButtonEmojiCollectionView.m
//
//  Created by 张赛 on 14/12/12.
//  Copyright (c) 2014年 xinmeihutong. All rights reserved.
//

#import "CKButtonEmojiCollectionView.h"
#import "CKButtonEmojiCell.h"
#import "CKMainButton.h"
#import "CKMainKeyboardExpandedView.h"
#import "CKCollcetionView.h"
#import "UIImageView+WebCache.h"

static NSString * const reuseIdentifier = @"CKButtonEmojiCell";


@interface CKButtonEmojiCollectionView ()<UIGestureRecognizerDelegate,CKCollectionViewDelegate,CKEnglishExpandedLabelViewEmojiDelegate>

{
    UIPanGestureRecognizer *panGestureRecognizer;
    NSString * emojiDestination;
    CKMainButton *  firstButton;
    CKMainButton * currentLongPressButton;
    int currentLongPressButtonSelectedIndex;
}


@property (nonatomic, strong) NSMutableArray * mutiSkinEmojiArray;
@property (nonatomic, strong) UIView * coverView;
@property (nonatomic, assign, getter=isScreenLand) BOOL screenLand;
@property (nonatomic, assign,getter=isLongPressOn) BOOL longPressOn;

@end


@implementation CKButtonEmojiCollectionView

- (UICollectionView *)createCollectionView:(CGRect)frame collectionViewLayout:(UICollectionViewFlowLayout *)flowLayout
{
    CKCollcetionView * cv = [[CKCollcetionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:flowLayout];
    cv.CKDelegate = self;
    cv.userInteractionEnabled = YES;
    [cv registerClass:[CKButtonEmojiCell class] forCellWithReuseIdentifier:reuseIdentifier];
    return cv;
}

- (NSMutableArray *)mutiSkinEmojiArray
{
    if (_mutiSkinEmojiArray == nil) {
        _mutiSkinEmojiArray = [NSMutableArray array];
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        NSMutableArray * tempArray = [userDefaults objectForKey:@"Emoji_mutiSkinEmojiArray"];
        if (tempArray != nil) {
            [_mutiSkinEmojiArray addObjectsFromArray:tempArray];
        }
    }
    return _mutiSkinEmojiArray;
}

- (void)dealloc
{
    NSArray * cellArray =  collectionView.visibleCells;
    for (CKButtonEmojiCell * cell in cellArray) {
        CKMainButton * button = cell.emojiButton;
        if (button.expandedView) {
            [button.expandedView removeFromSuperview];
        }
    }
    [self saveRecentArray];
}

- (void)handleFisrtButtonView
{
    if (firstButton) {
        NSLog(@"handleFisrtButtonView");
        if (firstButton.expandedView) {
            [firstButton.expandedView removeFromSuperview];
            firstButton.expandedView = nil;
            firstButton = nil;
            [self.coverView removeFromSuperview];
        }
    }
}

- (void)setCategoryStr:(NSString *)categoryStr
{
    [super setCategoryStr:categoryStr];
    [self handleFisrtButtonView];
}

- (void)initEmojiDict
{
    NSString * emojiPath = [[NSBundle mainBundle] pathForResource:[resourceType stringByAppendingString:@"List.plist"] ofType:nil];
    NSString *docDir = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:resourceType];
    NSString * destination = [docDir stringByAppendingPathComponent:[resourceType stringByAppendingString:@".plist"]];
    emojiDestination = destination;
    NSFileManager * myFileManger = [NSFileManager defaultManager];
    NSError *err = nil;
    
    NSString * finalPath;
    if ([myFileManger fileExistsAtPath:emojiDestination]) {
        finalPath = emojiDestination;
        NSMutableDictionary * savedDict = [NSMutableDictionary dictionaryWithContentsOfFile:finalPath];
        NSMutableDictionary * newEmojiDict = [NSMutableDictionary dictionaryWithContentsOfFile:finalPath];
        if (savedDict.count == newEmojiDict.count) {
            NSError * err = nil;
            [myFileManger removeItemAtPath:emojiDestination  error:&err];
            [myFileManger copyItemAtPath:emojiPath toPath:emojiDestination error:&err];
        }
    }
    else {
        finalPath = emojiPath;
        BOOL exist = false;
        if (![myFileManger fileExistsAtPath:docDir]) {
            exist = [myFileManger createDirectoryAtPath:docDir withIntermediateDirectories:YES attributes:nil error:nil];
        }
        if (([myFileManger fileExistsAtPath:docDir]|| exist)) {
            [myFileManger copyItemAtPath:emojiPath toPath:emojiDestination error:&err];
        }
    }
    emojiDict = [NSMutableDictionary dictionaryWithContentsOfFile:finalPath];
}

- (instancetype)initWithFrame:(CGRect)frame type:(NSString *)type
{
    self = [super initWithFrame:frame type:type];
    if (self) {
        UILongPressGestureRecognizer *longPressGestureRecognizer =
        [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(buttonShuoldshowExpandedInputView:)];
        longPressGestureRecognizer.minimumPressDuration = 0.6;
        longPressGestureRecognizer.delegate = self;
        [self addGestureRecognizer:longPressGestureRecognizer];
        longPressGestureRecognizer.allowableMovement = 50.0;
        
        panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(buttonFiredPanGesture:)];
        panGestureRecognizer.delegate = self;
        [self addGestureRecognizer:panGestureRecognizer];
        
        self.screenLand = [CKCommonTools screenDirectionIsLand];
    }
    return self;
}

- (void)addExpandedViewWhenLongPressed:(CKMainButton *)b
{
    if (b.inputOptions == nil) {
        return;
    }
    if (b.expandedView) { // 如果view已经存在,
        return;
    }
    CKMainKeyboardExpandedView * expandView = [[CKMainKeyboardExpandedView alloc] init];

    if (CGRectGetMaxX(b.superview.frame) >= [UIScreen mainScreen].bounds.size.width / 2) { // 向左边伸展
        b.position = CKEnglishButtonPositionRight;
    }
    else {
        b.position = CKEnglishButtonPositionLeft;
    }
    
    b.expandedView = expandView;
    [expandView setEnglishButton:b dimensions:nil];
    expandView.backgroundColor = [UIColor clearColor];
    
    
    /**
     *  如果按钮的最大X值小于View的centerX,popUp应该向右边伸展,反之向左边
     */
    UIView * controllerInputView = self.superview.superview;
    CGFloat centerY = b.superview.frame.origin.y - collectionView.contentOffset.y + self.frame.origin.y - expandView.frame.size.height / 2;
    CGFloat expandViewCenterX = 0;
    if (b.position == CKEnglishButtonPositionRight) { // 向左边伸展
        expandViewCenterX = CGRectGetMaxX(b.superview.frame) - expandView.frame.size.width / 2;
    }
    else {
        expandViewCenterX = CGRectGetMinX(b.superview.frame) + expandView.frame.size.width / 2;
    }
    expandView.center = CGPointMake(expandViewCenterX, centerY);
//    expandView.center = self.superview.center;

    // 这个是KeyboardViewController的View
    CKLog(@"%@==expandView,%@===frame",expandView,NSStringFromCGRect(expandView.frame));
    [controllerInputView addSubview:expandView];
}

- (void)buttonShuoldshowExpandedInputView:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint location = [gestureRecognizer locationInView:collectionView];

    collectionView.scrollEnabled = NO;
    self.longPressOn = YES;
    CKMainButton * currentButton;
    NSArray * cellArray =  collectionView.visibleCells;
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        for (CKButtonEmojiCell * cell in cellArray) {
            if(CGRectContainsPoint(cell.frame, location)){
                CKMainButton * button = cell.emojiButton;
                currentButton = button;
                break;
            }
        }
        
        if(!currentButton) { // 如果按钮为空,直接返回
            self.longPressOn = NO;
            currentLongPressButton = nil;
            return;
        }
        if (currentButton.inputOptions != nil) {
            [self addExpandedViewWhenLongPressed:currentButton];
            [self addBackViewForTouchedButton:currentButton];
        }

        currentLongPressButton = currentButton;
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateCancelled || gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        self.longPressOn = NO;
        for (CKButtonEmojiCell * cell in cellArray) {
            CKMainButton * button = cell.emojiButton;
            if (button.expandedView != nil) {
                [button.expandedView removeFromSuperview];
                button.expandedView = nil;
                [button.inputLabel removeFromSuperview];
                button.inputLabel = nil;
                break;
            }
        }
        
        if (panGestureRecognizer.state != UIGestureRecognizerStateRecognized) { // 如果滑动了
            if ([self.categoryStr isEqualToString:@"people"] || [self.categoryStr isEqualToString:@"activity"]) {
                if (![currentLongPressButton.inputOptions[0] isEqualToString:currentLongPressButton.currentTitle] && currentLongPressButton.inputOptions.count != 0 && currentLongPressButton.inputOptions != nil ) {
                    currentLongPressButton.outPutToTextFiledStr = currentLongPressButton.currentTitle;
                    emojiArray[currentLongPressButton.tag] = currentLongPressButton.currentTitle;
                    [collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:currentLongPressButton.tag inSection:0]]];
                    [self saveDict];
                }
            }
            else {
                currentLongPressButton.outPutToTextFiledStr = currentLongPressButton.currentTitle;
            }
            if(currentLongPressButton) {
                [self cellButtonClicked:currentLongPressButton];
            }
            else {
                return;
            }
        }
        else {
            self.longPressOn = YES;
            collectionView.scrollEnabled = YES;
            [currentLongPressButton.expandedView removeFromSuperview];
            currentLongPressButton.expandedView = nil;
            [currentLongPressButton.inputLabel removeFromSuperview];
            currentLongPressButton.inputLabel = nil;
        }
    }
}

- (void)addBackViewForTouchedButton:(CKMainButton *)currentButton
{
    if (!currentButton.inputLabel) {
        UIView * view = [[UIView alloc] init];
        view.bounds = CGRectMake(0, 0, currentButton.frame.size.width, currentButton.frame.size.height);
        currentButton.inputLabel = view;
        [currentButton.superview insertSubview:view belowSubview:currentButton];
        view.center = currentButton.center;
        view.backgroundColor = [UIColor whiteColor];
        view.layer.masksToBounds = YES;
        view.layer.cornerRadius = 5;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}


- (void)buttonFiredPanGesture:(UIPanGestureRecognizer *)gestureRecognizer
{
    NSArray * cellArray =  collectionView.visibleCells;
    CKMainKeyboardExpandedView * expand;
    for (CKButtonEmojiCell * cell in cellArray) {
        CKMainButton * button = cell.emojiButton;
        if (button.expandedView) {
            expand = button.expandedView;
            break;
        }
    }
    
    if (expand == nil) {
        collectionView.scrollEnabled = YES;
    }
    
    if (self.isLongPressOn) {
        CGPoint location = [gestureRecognizer locationInView:collectionView];
        if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled) {
            self.longPressOn = NO;
            collectionView.scrollEnabled = YES;
            NSString * final = currentLongPressButton.inputOptions[currentLongPressButtonSelectedIndex];
            currentLongPressButton.outPutToTextFiledStr = final;
            if (currentLongPressButton.outPutToTextFiledStr == nil) {
                currentLongPressButton.outPutToTextFiledStr = currentLongPressButton.currentTitle;
            }
            if(currentLongPressButton) {
                /**
                 *  判断是不是多皮肤按钮的第一次点击
                 */
                if ([self.categoryStr isEqualToString:@"people"] || [self.categoryStr isEqualToString:@"activity"]) {
                    if (currentLongPressButton.inputOptions.count != 0) { // 多皮肤按钮
                        // 不能取input,因为input是会变的,但是inputOptions[0]是不会变的
                        NSString * firstInputOption = currentLongPressButton.inputOptions[0];
                        if (![self.mutiSkinEmojiArray containsObject:firstInputOption]) { // 如果是多皮肤按钮的第一次点击
                            
                            if ([self.emojiButtonDelegate respondsToSelector:@selector(buttonEmojiCollectinoViewClicked:)]) {
                                [self.emojiButtonDelegate buttonEmojiCollectinoViewClicked:currentLongPressButton];
                            }
                            NSUserDefaults * userDefauts = [NSUserDefaults standardUserDefaults];
                            [self.mutiSkinEmojiArray addObject:firstInputOption];
                            [userDefauts setObject:self.mutiSkinEmojiArray forKey:@"Emoji_mutiSkinEmojiArray"];
                            [userDefauts synchronize];
                        }
                        else {
                            [self cellButtonClicked:currentLongPressButton];
                        }
                    }
                }
            }
            else {
                return;
            }
            // 修改显示数组
            if ([self.categoryStr isEqualToString:@"people"] || [self.categoryStr isEqualToString:@"activity"]) {
                if (currentLongPressButton.inputOptions.count != 0 && currentLongPressButton.inputOptions != nil ) {
                    
                    emojiArray[currentLongPressButton.tag] = final ? final : currentLongPressButton.currentTitle;
                    [collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:currentLongPressButton.tag inSection:0]]];
                    [self saveDict];
                }
                
                if (!collectionView.scrollEnabled) {
                    collectionView.scrollEnabled = YES;
                }
                
                [self removeCoverView];
            }
        }
        else { // 触发手势
            if (currentLongPressButton.expandedView != nil) {
                currentLongPressButton.expandedView.currentPoint = location;
                currentLongPressButtonSelectedIndex = currentLongPressButton.expandedView.selectedIndex;
            }
        }
    }
    else {
        collectionView.scrollEnabled = YES;
        [self removeCoverView];
        return;
    }
}

- (void)selectedEmojiWithIndex:(int)selectedIndex button:(CKMainButton *)button
{
    NSString * final = button.inputOptions[selectedIndex];
    if (!final) {
        final = button.currentTitle;
    }
    emojiArray[button.tag] = final;
    [collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:button.tag inSection:0]]];
    
    button.outPutToTextFiledStr = final;
    [self cellButtonClicked:button];
    [self saveDict];
    collectionView.scrollEnabled = YES;
    [self removeCoverView];
    button.expandedView.emojiFisrtTouch = NO;
}

- (void)saveDict
{
    // 写数组
    emojiDict[self.categoryStr] = emojiArray;
    [emojiDict writeToFile:emojiDestination atomically:YES];
}

#pragma mark -- UICollectionViewDataSource

//每个UICollectionView展示的内容
-(CKButtonEmojiCell *)collectionView:(CKCollcetionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CKButtonEmojiCell * cell = [cv dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[CKButtonEmojiCell alloc] init];
    }

    cell.emojiButton.titleLabel.font = [UIFont systemFontOfSize:33];
    cell.emojiButton.enabled = NO;
    cell.emojiButton.tag = indexPath.item;
    cell.emojiButton.outPutToTextFiledStr = nil;
    NSString * buttonStr = emojiArray[indexPath.item];
    [cell.emojiButton setTitle:buttonStr forState:UIControlStateNormal];

    NSDictionary * dict = emojiDict[@"faceDict"][self.categoryStr];
    if (dict != nil) {
        NSString * keyStr = [NSString stringWithFormat:@"%@%d",self.categoryStr,(int)indexPath.row];
        cell.emojiButton.inputOptions = dict[keyStr];
    }
    else {
        cell.emojiButton.inputOptions = nil;
    }

    return cell;
}

- (void)tellEmojiPlaySongs
{
    if ([self.emojiButtonDelegate respondsToSelector:@selector(emojiShouldPlaySounds)]) {
        [self.emojiButtonDelegate emojiShouldPlaySounds];
    }
}

- (void)cellButtonClicked:(CKMainButton *)button
{   //表情页操作统计标示
    [CKSaveTools saveInteger:SelectedEmojiInput forKey:SELECT_INPUT inRegion:CKSaveRegionExtensionKeyboard];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UserHaveDoneSomethingNotification object:nil];
    
    NSString * buttonContent = button.outPutToTextFiledStr;
    if (buttonContent == nil) {
        buttonContent = button.currentTitle;
    }

    if (button.inputOptions.count != 0) { // 多皮肤按钮
        // 不能取input,因为input是会变的,但是inputOptions[0]是不会变的
        NSString * firstInputOption = button.inputOptions[0];
        if (![self.mutiSkinEmojiArray containsObject:firstInputOption] && firstButton == nil) {
            [self.mutiSkinEmojiArray addObject:firstInputOption];

            button.buttonStyle = CKEnglishButtonStyleEmoji;
            [self addExpandedViewWhenLongPressed:button];
            button.expandedView.emojiFisrtTouch = YES;
            button.expandedView.delegate = self;
            firstButton = button;

            [self addSubview:self.coverView];

            NSUserDefaults * userDefauts = [NSUserDefaults standardUserDefaults];
            [userDefauts setObject:self.mutiSkinEmojiArray forKey:@"Emoji_mutiSkinEmojiArray"];
            [userDefauts synchronize];
            collectionView.scrollEnabled = NO;
            return;
        }
    }

    if ([self.emojiButtonDelegate respondsToSelector:@selector(buttonEmojiCollectinoViewClicked:)]) {
        [self.emojiButtonDelegate buttonEmojiCollectinoViewClicked:button];
        [self updateRecentArray:buttonContent];
    }
    collectionView.scrollEnabled = YES;
}

- (UIView *)coverView
{
    if (_coverView == nil) {
        UIButton * coverView = [[UIButton alloc] initWithFrame:self.superview.bounds];
        CGFloat y = self.frame.size.height - coverView.frame.size.height;
        self.coverView = coverView;
        coverView.frame = CGRectMake(0, y, coverView.frame.size.width, coverView.frame.size.height);
        [coverView addTarget:self action:@selector(removeCoverView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _coverView;
}

- ( void)removeCoverView {
    if (firstButton && firstButton.expandedView) {
        [firstButton.expandedView removeFromSuperview];
        firstButton.expandedView = nil;
        firstButton = nil;
    }
    [self.coverView removeFromSuperview];
}

- (void)layoutSubviews
{
    BOOL screenLand = [CKCommonTools screenDirectionIsLand];
    if (screenLand != self.isScreenLand) { // 方向发生改变
        NSArray * cellArray =  collectionView.visibleCells;
        for (CKButtonEmojiCell * cell in cellArray) {
            CKMainButton * button = cell.emojiButton;
            if (button.expandedView) {
                [button.expandedView removeFromSuperview];
            }
        }
        self.screenLand = screenLand;
    }
}

#pragma mark --UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(8, 0, 0, 0);
}

@end
