//
//  CKEmojiBottomView.h
//
//  Created by 张赛 on 15/5/12.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

#define BOTTOM_VIEW_FUNCTION_BUTTON_NUM 3
#define BOTTOM_VIEW_FUNCTION_BUTTON_FONT_COLOR @"#f3f3f3"

@protocol EmojiBottomViewDelegate <NSObject>

- (void)emojiBottomViewButtonClicked:(NSInteger)tag;

@end


@interface CKEmojiBottomView : UIImageView

@property (nonatomic, weak) id<EmojiBottomViewDelegate> delegate;
@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, assign) UIButton * deleteButton;

- (instancetype)initWithFrame:(CGRect)frame types:(NSArray *)resourceTypes type:(NSString *)resType;

@end
