//
//  CKEmojiCollectionView.m
//
//  Created by 张赛 on 14/12/12.
//  Copyright (c) 2014年 xinmeihutong. All rights reserved.
//

#import "CKEmojiCollectionView.h"

@interface CKEmojiCollectionView ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableArray * _recentArray;
    NSInteger _cellSize;
    NSInteger _recentMaxCount;
}

@end


@implementation CKEmojiCollectionView

- (void)initRecentArray
{
    _recentArray = [NSMutableArray array];
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray * tempArray = (NSMutableArray *)[userDefaults objectForKey:[resourceType stringByAppendingString:@"RecentArray"]];
    if (tempArray != nil) {
        [_recentArray addObjectsFromArray:tempArray];
    }
}

- (void)saveRecentArray
{
    [CKSaveTools saveArray:_recentArray forKey:[resourceType stringByAppendingString:@"RecentArray"] inRegion:CKSaveRegionExtensionKeyboard];
}

- (void)updateRecentArray:(NSString *)currentItem
{
    if ([_recentArray containsObject:currentItem]) {
        int currentIndex = (int)[_recentArray indexOfObject:currentItem];
        if (currentIndex != 0) {
            [_recentArray removeObjectAtIndex:currentIndex];
            [_recentArray insertObject:currentItem atIndex:0];
        }
    }
    else {
        [_recentArray insertObject:currentItem atIndex:0];
        if (_recentArray.count > _recentMaxCount) {
            [_recentArray removeLastObject];
        }
    }
    [self saveRecentArray];
}

- (void)setCategoryStr:(NSString *)categoryStr
{
    _categoryStr = categoryStr;
    emojiArray = nil;
    if ([categoryStr isEqualToString:@"history"]) {
        [self saveRecentArray];
        emojiArray = _recentArray;
    }
    else {
        emojiArray = (NSMutableArray *)emojiDict[categoryStr];
    }

    [collectionView reloadData];

    if (emojiArray.count) {
        [collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }

    collectionView.scrollEnabled = YES;
    collectionView.contentOffset = CGPointMake(0, 5);
}

- (void)initEmojiDict
{
    NSString * emojiPath = [[NSBundle mainBundle] pathForResource:[resourceType stringByAppendingString:@"List.plist"] ofType:nil];
    emojiDict = [NSMutableDictionary dictionaryWithContentsOfFile:emojiPath];
}

- (UICollectionView *)createCollectionView:(CGRect)frame collectionViewLayout:(UICollectionViewFlowLayout *)flowLayout
{
    return [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - 40) collectionViewLayout:flowLayout];
}

- (instancetype)initWithFrame:(CGRect)frame type:(NSString *)type
{
    self = [super initWithFrame:frame];
    if (self) {
        resourceType = type;
        [self initEmojiDict];
        [self initRecentArray];

        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];

        CGRect collectionViewFrame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        collectionView = [self createCollectionView:collectionViewFrame collectionViewLayout:flowLayout];
        [self addSubview:collectionView];
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.backgroundColor = [UIColor clearColor];
        collectionView.userInteractionEnabled = YES;

        NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:[resourceType stringByAppendingString:@"Misc.plist"] ofType:nil];
        NSDictionary * misc = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
        _cellSize = [misc[@"CellSize"] integerValue];
        _recentMaxCount = [misc[@"RecentMaxCount"] integerValue];
        self.backgroundColor  = [UIColor clearColor];
    }
    return self;
}

#pragma mark -- UICollectionViewDataSource

//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return emojiArray.count;
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionView *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark --UICollectionViewDelegateFlowLayout

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(_cellSize, _cellSize);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end
