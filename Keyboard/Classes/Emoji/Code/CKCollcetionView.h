//
//  CKCollcetionView.h
//  新Emoji的POP
//
//  Created by 张赛 on 15/5/15.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CKMainButton;

@protocol CKCollectionViewDelegate <NSObject>

- (void)cellButtonClicked:(CKMainButton *)button;
- (void)tellEmojiPlaySongs;


@end


@interface CKCollcetionView : UICollectionView

@property (nonatomic,weak) id<CKCollectionViewDelegate> CKDelegate;

@end
