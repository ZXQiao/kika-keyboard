//
//  CKEmojiCollectionView.h
//
//  Created by 张赛 on 14/12/12.
//  Copyright (c) 2014年 xinmeihutong. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CKEmojiCollectionView : UIView

{
    UICollectionView * collectionView;
    NSMutableDictionary * emojiDict;
    NSMutableArray * emojiArray;
    NSString * resourceType;
}

@property (nonatomic, strong) NSString * categoryStr;

- (instancetype)initWithFrame:(CGRect)frame type:(NSString *)resourceType;
- (void)saveRecentArray;
- (void)updateRecentArray:(NSString *)currentItem;
@end
