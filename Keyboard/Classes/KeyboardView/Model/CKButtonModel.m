//
//  CKKeyboardModel.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/8.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKButtonModel.h"

@interface CKButtonModel()


@end
@implementation CKButtonModel

+(NSArray *)buttonModelArrayWithArray:(NSArray *)array
{
    NSMutableArray * modelArray = [NSMutableArray array];
    for (NSDictionary * dict in array) {
        CKButtonModel * buttonModel = [[CKButtonModel alloc] initWithDict:dict];
        [modelArray addObject:buttonModel];
    }
    return (NSArray *)modelArray;
}

- (instancetype)initWithDict:(NSDictionary *)dict;
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
    
}

@end
