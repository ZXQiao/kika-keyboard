//
//  CKKeyboardPageInfo.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/8.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CKButtonModel.h"
@interface CKKeyboardPageInfo : NSObject

@property (nonatomic, strong) NSArray * character;

@property (nonatomic, strong) NSArray * numberONE;

@property (nonatomic, strong) NSArray * numberTWO;

@property (nonatomic, strong) NSArray * offsetArray;

@property (nonatomic, strong) NSArray * offsetArray_l;

@property (nonatomic, strong) NSNumber * maxLineNum;

@property (nonatomic, strong) NSNumber * maxLineNum_l;

@property (nonatomic, strong) NSString * languageName;

@property (nonatomic, strong) NSNumber * button_gap;

@property (nonatomic, strong) NSNumber * button_gap_l;


@end
