
//
//  CKEnglishButtonPopUp.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/6.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMainButtonPopUp.h"
#import "CKMainButton.h"


@interface CKMainButtonPopUp()
@property (nonatomic, strong) UIView * currentPopView;
@property (nonatomic, strong) CKMainButton * currentButton;
@end
@implementation CKMainButtonPopUp
// 添加动画
- (void)button:(CKMainButton *)button addPopupView:(UIView *)popView withStyle:(CMainButtonRemovePopUpStyle)addStyle
{
    
}

- (void)removePopView:(UIView *)popView
{
    [popView removeFromSuperview];
}

// 移除动画
- (void)button:(CKMainButton *)button removePopupView:(UIView *)popView withStyle:(CMainButtonRemovePopUpStyle)removeStyle
{
    [self performSelector:@selector(removePopView:) withObject:popView afterDelay:.08f];
    
//    if ([button.style isEqualToString:@"letter"] ||[button.style isEqualToString:@"symbol"]){
////    button.isAnimating = YES;
////    [self popUpThreeD:popView];
////    self.currentPopView = popView;
////    self.currentButton = button;
//    [UIView animateWithDuration:0.1 delay:0.1f options:UIViewAnimationOptionCurveEaseInOut animations:^{
//        [button setBackgroundImage:button.buttonImage forState:UIControlStateNormal];
//        [button setTitleColor:button.keyColor forState:UIControlStateNormal];
//    } completion:^(BOOL finished) {
//        
//    }];
//    }
}

- (void)popUpThreeD:(UIView *)popView
{
    CAKeyframeAnimation *boundsOvershootAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    boundsOvershootAnimation.delegate = self;
    CATransform3D startScale = popView.layer.transform;
    CATransform3D endingScale1 = CATransform3DScale (popView.layer.transform,0,0, 1.0);
    NSArray *boundsValues = [NSArray arrayWithObjects:
                             [NSValue valueWithCATransform3D:startScale],
                             [NSValue valueWithCATransform3D:endingScale1],nil];
    [boundsOvershootAnimation setValues:boundsValues];
    

    NSArray *times = [NSArray arrayWithObjects:
                      [NSNumber numberWithFloat:0.2f],
                      [NSNumber numberWithFloat:0.25f], nil];
    [boundsOvershootAnimation setKeyTimes:times];
    
    
    NSArray *timingFunctions = [NSArray arrayWithObjects:
                                [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                nil];
    [boundsOvershootAnimation setTimingFunctions:timingFunctions];
    boundsOvershootAnimation.fillMode = kCAFillModeForwards;
    boundsOvershootAnimation.removedOnCompletion = NO;
    
    [popView.layer addAnimation:boundsOvershootAnimation forKey:nil];
    
    self.currentPopView = popView;
    
    //1.创建动画
//    CABasicAnimation *anima=[CABasicAnimation animation];
//    //1.1告诉系统要执行什么样的动画
//    anima.keyPath=@"position";
//    //设置通过动画，将layer从哪儿移动到哪儿
//    anima.fromValue=[NSValue valueWithCGPoint:CGPointMake(0, 0)];
//    anima.toValue=[NSValue valueWithCGPoint:CGPointMake(200, 300)];
//    //1.2设置动画执行完毕之后不删除动画
//    anima.removedOnCompletion=NO;
//    //1.3设置保存动画的最新状态
//    anima.fillMode=kCAFillModeForwards;
//    //2.添加核心动画到layer
//    [popView.layer addAnimation:anima forKey:nil];
    
//    CABasicAnimation *anima=[CABasicAnimation animationWithKeyPath:@"transform"];
//    //1.1设置动画执行时间
//    anima.duration=2.0;
//    //1.2修改属性，执行动画
//    anima.toValue=[NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2+M_PI_4, 1, 1, 0)];
//    //1.3设置动画执行完毕后不删除动画
//    anima.removedOnCompletion=NO;
//    //1.4设置保存动画的最新状态
//    anima.fillMode=kCAFillModeForwards;
//    //2.添加动画到layer
//    [popView.layer addAnimation:anima forKey:nil];
}

-(void)animationDidStart:(CAAnimation *)anim
{

}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    self.currentPopView.hidden = YES;
    [self.currentPopView removeFromSuperview];
    CKLog(@"%@=====anim",anim);
    self.currentPopView = nil;
    self.currentButton.isAnimating = NO;
}

@end
