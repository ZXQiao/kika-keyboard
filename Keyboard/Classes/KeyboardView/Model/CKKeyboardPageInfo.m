//
//  CKKeyboardPageInfo.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/8.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKKeyboardPageInfo.h"

#define sameAsEnglishArray @[@"estonian",@"hungarian",@"indonesian",@"italian",@"latvian",@"lithuanian",@"malay",@"polish",@"portuguese(brazil)",@"portuguese(portugal)",@"romanian",@"slovenian",@"vietnamese",@"croatian",@"dutch"]

@interface CKKeyboardPageInfo()
@property (nonatomic,strong) NSDictionary * pageInfoDict;
@end

@implementation CKKeyboardPageInfo

- (NSDictionary *)pageInfoDict
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    NSString * languageName = [[userDefaults objectForKey:@"Language"] lowercaseString];
    if (languageName.length == 0 || languageName == nil) {
        languageName = @"english";
        [userDefaults setObject:@"English" forKey:@"Language"];
        [userDefaults synchronize];
    }
    self.languageName = languageName;
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGFloat screenH = screenSize.height;
    CGFloat screenW = screenSize.width;
    BOOL isLandscape = screenW > screenH;

    NSString * plistName;
    if ((isLandscape && screenW == 480) || (!isLandscape && screenH == 480)) { // iPhone4
        plistName = [NSString stringWithFormat:@"%@%@",languageName,@"4"];
    }
    else if ((isLandscape && screenW == 568) || (!isLandscape && screenH == 568)) { // iPhone5
        plistName = [NSString stringWithFormat:@"%@%@",languageName,@"5"];
    }
    else if ((isLandscape && screenW == 667) || (!isLandscape && screenH == 667)) { // iPhone6
        plistName = [NSString stringWithFormat:@"%@%@",languageName,@"6"];
    }
    else if ((isLandscape && screenW == 736) || (!isLandscape && screenH == 736)) { // iPhone6P
        plistName = [NSString stringWithFormat:@"%@%@",languageName,@"6p"];
    }
    else{
        plistName = [NSString stringWithFormat:@"%@%@",languageName,@"6p"];
    }
    NSError *err;
    NSData *dimensionData =[NSData dataWithContentsOfFile:[[NSBundle mainBundle]pathForResource:plistName ofType:@"json"]];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:dimensionData options:NSJSONReadingMutableContainers error:&err];
    if(err){//解析出错处理
        return nil;
    }
    return dict;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        self.character = [CKButtonModel buttonModelArrayWithArray:self.pageInfoDict[@"character"]];
        self.numberONE = [CKButtonModel buttonModelArrayWithArray:self.pageInfoDict[@"number1"]];
        self.numberTWO = [CKButtonModel buttonModelArrayWithArray:self.pageInfoDict[@"number2"]];
        self.offsetArray = self.pageInfoDict[@"offsetArray"];
        self.offsetArray_l = self.pageInfoDict[@"offsetArray_l"];
        self.maxLineNum = self.pageInfoDict[@"maxLineNum"];
        self.maxLineNum_l = self.pageInfoDict[@"maxLineNum_l"];
        self.button_gap = self.pageInfoDict[@"button_gap"];
        self.button_gap_l = self.pageInfoDict[@"button_gap_l"];
    }
    return self;
}

@end
