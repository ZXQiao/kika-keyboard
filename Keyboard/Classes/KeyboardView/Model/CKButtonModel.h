//
//  CKKeyboardModel.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/8.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKButtonModel : NSObject
// 按钮文字
@property (nonatomic, copy)     NSString * key;
// 按钮frame
@property (nonatomic, copy)     NSString * buttonArea;
// 按钮横屏frame
@property (nonatomic, copy)     NSString * buttonArea_l;
// 按钮有效点击区域
@property (nonatomic, copy)     NSString * touchArea;
// 按钮横屏有效点击区域
@property (nonatomic, copy)     NSString * touchArea_l;
// 按钮触发的方法对应的索引
@property (nonatomic, strong)   NSNumber * funcIndex;
// 按钮长按后出现的字母数组
@property (nonatomic, strong)   NSArray * inputOptions;
// 按钮长按触发的方法对应的索引,如果是0的话表示没有长按状态
@property (nonatomic, strong)   NSNumber * longPressedFuncIndex;
// 按钮的类型
@property (nonatomic, copy)     NSString * style;
@property (nonatomic, strong) NSNumber * row;
@property (nonatomic, strong) NSNumber * column;
@property (nonatomic, strong) NSNumber * isLeftFirst; // 左边第一个
@property (nonatomic, strong) NSNumber * isRightLast; // 右边最后一个


+(NSArray *)buttonModelArrayWithArray:(NSArray *)array;
@end
