//
//  CKEnglishButtonPopUp.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/6.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CKMainButton;

// popUp动画出现的样式
typedef NS_ENUM(NSUInteger, CKMainButtonAddPopUpStyle) {
    //默认样式:
    CKMainButtonAddPopUpStyleDefault = 0,
};

// popUp动画消失的样式
typedef NS_ENUM(NSUInteger, CMainButtonRemovePopUpStyle) {
    //默认消失样式: 缩小为0
    CKMainButtonRemovePopUpStyleDefault = 0,
};



@interface CKMainButtonPopUp : NSObject

- (void)button:(CKMainButton *)button addPopupView:(UIView *)popView withStyle:(CMainButtonRemovePopUpStyle)addStyle;
- (void)button:(CKMainButton *)button removePopupView:(UIView *)popView withStyle:(CMainButtonRemovePopUpStyle)addStyle;

@end
