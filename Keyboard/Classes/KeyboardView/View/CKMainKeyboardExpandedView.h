//
//  CKEnglishExpandedLabelView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/30.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CKMainButton;


#define Memoji_SCALE [[UIScreen mainScreen] scale]
#define __UPPER_WIDTH   (183.0/105.0 * button.frame.size.width  * [[UIScreen mainScreen] scale])
#define __LOWER_WIDTH   (button.frame.size.width * [[UIScreen mainScreen] scale])

#define __PAN_UPPER_RADIUS  (10.0 * [[UIScreen mainScreen] scale])
#define __PAN_LOWER_RADIUS  (5.0 * [[UIScreen mainScreen] scale])

#define __PAN_UPPDER_WIDTH   (__UPPER_WIDTH-__PAN_UPPER_RADIUS*2)
#define __PAN_UPPER_HEIGHT    (52.0 * [[UIScreen mainScreen] scale])

#define __PAN_LOWER_WIDTH     (__LOWER_WIDTH-__PAN_LOWER_RADIUS*2)
#define __PAN_LOWER_HEIGHT    (47.0 * [[UIScreen mainScreen] scale])

#define __PAN_UL_WIDTH        ((__UPPER_WIDTH-__LOWER_WIDTH)/2)

#define __PAN_MIDDLE_HEIGHT    (2.0 * [[UIScreen mainScreen] scale])

#define __PAN_CURVE_SIZE      (10.0 * [[UIScreen mainScreen] scale])

#define __PADDING_X     (15.0 * [[UIScreen mainScreen] scale])
#define __PADDING_Y     (10.0 * [[UIScreen mainScreen] scale])
#define __WIDTH   (__UPPER_WIDTH + __PADDING_X*2)
#define __HEIGHT   (__PAN_UPPER_HEIGHT + __PAN_MIDDLE_HEIGHT + __PAN_LOWER_HEIGHT + __PADDING_Y*2)


typedef NS_ENUM(int, CKPopUpImagePositon) {
    CKPopUpImageLeft = 0,
    CKPopUpImageInner,
    CKPopUpImageRight,
    CKPopUpImageMax
};


@protocol CKEnglishExpandedLabelViewEmojiDelegate <NSObject>
@optional
- (void)selectedEmojiWithIndex:(int)selectedIndex button:(CKMainButton *)button;

@end

@interface CKMainKeyboardExpandedView : UIImageView

/**
 *  传入button,设置Frame,Label字体颜色,Label个数等
 */
@property (nonatomic, strong) CKMainButton * englishButton;

/**
 *  设置selectedIndex移动光标
 */
@property (nonatomic, assign) int selectedIndex;

/**
 *  根据手势当前的位置来调整光标位置
 */
@property (nonatomic, assign) CGPoint currentPoint;

@property (nonatomic, assign) BOOL emojiFisrtTouch;
@property (nonatomic, assign) CGFloat popFontSize;
@property (nonatomic, weak) id<CKEnglishExpandedLabelViewEmojiDelegate> delegate;
@property (nonatomic, strong) NSString * fontName;
//@property (nonatomic, assign) CGFloat fontSize;

- (void)setEnglishButton:(CKMainButton *)englishButton dimensions:(NSDictionary *)realDimensions;
+ (UIImage *)createKeytopImageWithKind:(CKPopUpImagePositon)kind button:(CKMainButton *)button;
+ (UIImageView *)popupClassicalWithKind:(CKPopUpImagePositon)kind button:(CKMainButton *)button;
@end
