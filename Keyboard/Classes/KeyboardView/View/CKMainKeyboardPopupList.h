#import <UIKit/UIKit.h>


@interface CKMainKeyboardPopupList : UIImageView

- (void)layoutWithImage:(UIImage *)globalImage skin:(UIImage *)emojiImage;
- (void)highlightItem:(CGPoint)location;
- (NSInteger)selectItem:(CGPoint)location;

@property (nonatomic, strong) UIColor * textColor;
@end
