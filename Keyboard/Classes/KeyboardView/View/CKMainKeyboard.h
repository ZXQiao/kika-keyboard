//

#import <UIKit/UIKit.h>
// shift按钮对应的大写状态
typedef NS_ENUM(int, CKShiftStatus) {
    CKShiftStatusAllLower = 0,   // 全部小写
    CKShiftStatusCapitalized,      // 首字母大写
    CKShiftStatusAllUpper,
    CKShiftStatusLongPressedLower, // 长按小写
    CKShiftStatusLongPressedUpper // 长按大写
};

typedef NS_ENUM(int, CKKeyboardType) {
    CKKeyboardTypeCharacter = 0,   // 全部小写
    CKKeyboardTypeNumberONE,      // 首字母大写
    CKKeyboardTypeNumber2
};

// popUp的样式
typedef NS_ENUM(int, CKPopStyle) {
    CKPopStyleClassical = 0, // 经典款(铲子形状)
    CKPopStyleCuteGum, // 可爱橡皮糖效果
    CKPopStyleOther
};

//// popUp的样式
typedef NS_ENUM(int, CKRemovePopStyle) {
    CKRemovePopStyleDirectly = 0, // 直接移除
    CKRemovePopStyleAnimated, // 动画效果
};

#define Online_Theme_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS]URLByAppendingPathComponent:@"OnlineThemes"]

@class CKMainButton;
@protocol IKeyoardEnglishDelegate <NSObject>
- (void)characterPressedEnglish:(CKMainButton *)button Str:(NSString *)str;// 字母按键
-(void)characterPressedNumber:(CKMainButton *)button Str:(NSString *)str;
- (void)deletePressedEnglish:(id)sender;// 删除
- (void)spacePressedEnglish:(id)sender;// 空格
- (void)returnPressedEnglish:(id)sender;// 回车
- (void)globalPressedNumber:(UIButton *)button;// global

- (void)emojiPressedEnglish:(UIButton *)button;
- (void)deleteButtonLongPressed:(CKMainButton *)deleteButton gesture:(UILongPressGestureRecognizer *)gestureRecognizer;
- (void)shiftButtonLongPressed:(UIButton *)shiftButton gesture:(UILongPressGestureRecognizer *)gestureRecognizer;
- (void)symbolButtonClicked:(CKMainButton *)button;
- (void)shiftChangeStatusTo:(CKShiftStatus)shiftStatus;
- (void)shouldPlaySounds;

- (void)changeShiftStatusByViewController;
@end

@interface CKMainKeyboard : UIView <UIInputViewAudioFeedback>
@property (nonatomic, assign, getter=isShifted) BOOL shifted;
@property (nonatomic, weak) id <IKeyoardEnglishDelegate>englishboardDelegate ;
@property (nonatomic, assign) CKShiftStatus shiftStatus;
@property (nonatomic, assign) UIReturnKeyType returnType;

- (void)backToCharacter;

@end


