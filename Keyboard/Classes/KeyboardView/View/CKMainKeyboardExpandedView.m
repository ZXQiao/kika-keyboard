//
//  CKEnglishExpandedLabelView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/30.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMainKeyboardExpandedView.h"
#import "CKMainButton.h"

@interface CKMainKeyboardExpandedView()
@property (nonatomic, strong) NSMutableArray * optionLabelArray;
@property (nonatomic, strong) NSMutableArray * backImageViewArray;
@property (nonatomic, strong) UIImageView * selectedBackImageView;
@property (nonatomic, strong) UIImage * pop_bg_Image;
@property (nonatomic, strong) UIImage * pop_selectedImage;

@end
@implementation CKMainKeyboardExpandedView

- (NSMutableArray *)optionLabelArray
{
    if (_optionLabelArray == nil) {
        _optionLabelArray = [NSMutableArray array];
    }
    return _optionLabelArray;
}

- (NSMutableArray *)backImageViewArray
{
    if (_backImageViewArray == nil) {
        _backImageViewArray = [NSMutableArray array];
    }
    return _backImageViewArray;
}

- (UIImage *)pop_bg_Image
{
    
    if (_pop_bg_Image == nil) {
        CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
        if (nowUseTheme) {
            if (nowUseTheme.isOnline) {
                _pop_bg_Image = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_bg" resize:YES];
            }
            else{
                _pop_bg_Image = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_bg" resize:YES];
            }
        }
        if (_pop_bg_Image == nil ) {
            _pop_bg_Image = [UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_popup_bg",Default_Theme_Name]]];
        }
    }
    return _pop_bg_Image;
}

- (UIImage *)pop_selectedImage
{
    if (_pop_selectedImage == nil) {
        CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
        if (nowUseTheme) {
            if (nowUseTheme.isOnline) {
             _pop_selectedImage = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_select" resize:YES];
            }
            else{
            _pop_selectedImage = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_select" resize:YES];
            }
        }
        if (_pop_selectedImage == nil) {
            _pop_selectedImage = [UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_popup_select",Default_Theme_Name]]];
        }
    }
    return _pop_selectedImage;
}

/**
 *  根据传入的Button设置
 *
 *  @param englishButton 传入的按钮
 */
- (void)setEnglishButton:(CKMainButton *)englishButton dimensions:(NSDictionary *)realDimensions
{
    _englishButton = englishButton;
    NSArray * optionsArray = englishButton.inputOptions;
    int count = (int)optionsArray.count;

    CGFloat buttonW = englishButton.frame.size.width * 1.1;
    CGFloat buttonH = englishButton.frame.size.height * 1.1;

	CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    BOOL ios_style = (nowUseTheme.popupstyle_index.intValue == 0);

    CGFloat ratio;
    if (ios_style) {
    ratio = 1;
	}
	else {
    ratio = 0.75;
	}
    if (englishButton.buttonStyle == CKEnglishButtonStyleEmoji) {
        buttonW = 33;//englishButton.superview.frame.size.width * .8;
        buttonH = 46;//englishButton.superview.frame.size.height * .8;
        ratio = 1;
    }
    
	if (ios_style && (englishButton.buttonStyle != CKEnglishButtonStyleEmoji)) {
        buttonW = englishButton.frame.size.width;
        buttonH = englishButton.frame.size.height;
	}
    CGFloat popup_content_edge_bottom = 0;
    CGFloat popup_height = 0;
    CGFloat popup_content_left_padding = 0;
    CGFloat popup_content_right_padding = 0;
    if (realDimensions != nil) {
        popup_content_edge_bottom = [realDimensions[@"popup_content_edge_bottom"] floatValue];
        popup_height = [realDimensions[@"popup_height"] floatValue];
        popup_content_left_padding = [realDimensions[@"popup_content_left_padding"] floatValue];
        popup_content_right_padding = [realDimensions[@"popup_content_right_padding"] floatValue];
    }
    CGFloat singleLabelW = buttonW;
    CGFloat singleLabelH = buttonH;
    CGFloat viewW = buttonW * count;
    CGFloat left_padding = popup_content_left_padding;
    CGFloat right_padding = popup_content_right_padding;
	if (ios_style && (englishButton.buttonStyle != CKEnglishButtonStyleEmoji)) {
    self.frame = CGRectMake(0, 0, viewW + left_padding + right_padding + englishButton.buttonGap * (count - 1), popup_height);
	}
	else {
        if (englishButton.buttonStyle != CKEnglishButtonStyleEmoji) {
            self.frame = CGRectMake(0, 0, viewW, buttonH);
        }
        else {
            self.frame = CGRectMake(0, 0, viewW + 12 + 2 * (count - 1), buttonH);
        }
	}
    
    int currentSelectedIdx = 0;
    if (englishButton.position == CKEnglishButtonPositionLeft) { // 如果按钮在左
        currentSelectedIdx = 0;
    }
    else
    {
        currentSelectedIdx = count - 1;
    }
    
    for (int i = 0; i < optionsArray.count; i ++) {
        NSString * optionString;
        if (englishButton.position == CKEnglishButtonPositionLeft) { // 如果按钮在左边,popUp上的文字正向取出,如果在右边,反向取出,Emoji例外
            optionString = optionsArray[i];
            if ([englishButton.currentTitle isEqualToString:optionString]) {
                currentSelectedIdx = i;
            }
        }
        else
        {
            int currentIdx;
            if (self.englishButton.buttonStyle != CKEnglishButtonStyleEmoji) {
                currentIdx = count - 1 - i;
            }
            else {
                currentIdx = i;
            }
            
            optionString = optionsArray[currentIdx];
            if ([englishButton.currentTitle isEqualToString:optionString]) {
                currentSelectedIdx = currentIdx;
            }
        }

        CGFloat singLabelX = singleLabelW * i;
		if (ios_style && (englishButton.buttonStyle != CKEnglishButtonStyleEmoji)) {
            singLabelX += left_padding + englishButton.buttonGap * i;
		}
        else if (englishButton.buttonStyle == CKEnglishButtonStyleEmoji) {
            singLabelX += 6 + 2 * i;
        }
        UILabel * label = [[UILabel alloc] init];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = optionString;
        if (count == 1) {
            label.font = [UIFont fontWithName:self.fontName size:self.popFontSize];
        }
        else {
            label.font = englishButton.titleLabel.font;
        }
        label.textColor = englishButton.keyColor;
		if (ios_style && (englishButton.buttonStyle != CKEnglishButtonStyleEmoji)) {
        label.frame = CGRectMake(singLabelX, popup_content_edge_bottom, singleLabelW, singleLabelH);
		}
		else {
            if (englishButton.buttonStyle != CKEnglishButtonStyleEmoji) {
                label.frame = CGRectMake(singLabelX, 0, singleLabelW, singleLabelH);
            }
            else {
                label.frame = CGRectMake(singLabelX, 5, 33, 33);
            }
		}
        [self addSubview:label];
        label.backgroundColor = [UIColor clearColor];
        [self.optionLabelArray addObject:label];
        
        UIImageView * backImageView = [[UIImageView alloc] init];
        backImageView.backgroundColor = [UIColor clearColor];
		if (ios_style && (englishButton.buttonStyle != CKEnglishButtonStyleEmoji)) {
        backImageView.frame = CGRectMake(left_padding, popup_content_edge_bottom, singleLabelW * ratio, singleLabelH * ratio);
		}
		else {
            if (englishButton.buttonStyle != CKEnglishButtonStyleEmoji) {
                backImageView.frame = CGRectMake(0, 0, singleLabelW * ratio, singleLabelH * ratio);
            }
            else {
                backImageView.frame = CGRectMake(0, -1, 33, 33);
            }
		}
        backImageView.center = label.center;
        [self insertSubview:backImageView belowSubview:label];
        [self.backImageViewArray addObject:backImageView];
        
        backImageView.hidden = YES;
        backImageView.image = self.pop_selectedImage;
        
        if ([optionString isEqualToString:englishButton.currentTitle]) {
            backImageView.hidden = NO;
            self.selectedBackImageView = backImageView;
            if (optionsArray.count == 1) {
                backImageView.hidden = YES;
            }
        }
    }
    
    self.selectedIndex = currentSelectedIdx;
    self.userInteractionEnabled = YES;
    self.image = self.pop_bg_Image;
}



/**
 *
 *  @param currentPoint 传入当前手指所在的位置
 *  根据手指Point设置光标的位置
 */
- (void)setCurrentPoint:(CGPoint)currentPoint
{
    if (self.optionLabelArray.count == 1) {
        return;
    }
    _currentPoint = currentPoint;
    CGFloat singW = self.frame.size.width / self.optionLabelArray.count;
    CGRect expandViewFrameOnKB;
    if (self.englishButton.buttonStyle == CKEnglishButtonStyleEmoji) {
        expandViewFrameOnKB = self.frame;

    }
    else
    {
        expandViewFrameOnKB  = [self.englishButton convertRect:self.frame toView:self.englishButton.superview]; // 转换
    }
    
    
    CGFloat x = currentPoint.x - expandViewFrameOnKB.origin.x;
    int selectedIndex = (int)(x / singW);
    if (selectedIndex < 0) {
        selectedIndex = 0;
    }
    if (selectedIndex >= self.optionLabelArray.count) {
        selectedIndex = (int)self.optionLabelArray.count - 1;
    }
    
    self.selectedIndex = selectedIndex;
    self.selectedBackImageView.hidden = YES;
    UIImageView * selectedLabel = self.backImageViewArray[selectedIndex];
    selectedLabel.hidden = NO;
//    [self bringSubviewToFront:selectedLabel];
    self.selectedBackImageView = selectedLabel;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!self.emojiFisrtTouch) {
        return;
    }
    CGPoint location = [[touches anyObject] locationInView:self];
    for (int i = 0 ; i < self.backImageViewArray.count; i ++) {
        UILabel * titleLabel = self.optionLabelArray[i];
        UIImageView * backImagaView = self.backImageViewArray[i];
        backImagaView.contentMode = UIViewContentModeScaleAspectFit;
        if (CGRectContainsPoint(titleLabel.frame, location)) {
            self.selectedIndex = i ;
            self.selectedBackImageView.hidden = YES;
            backImagaView.hidden = NO;
            self.selectedBackImageView = backImagaView;
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(selectedEmojiWithIndex:button:)]) {
        [self.delegate selectedEmojiWithIndex:self.selectedIndex button:self.englishButton];
    }
    
    [self removeFromSuperview];
}

+ (UIImageView *)popupClassicalWithKind:(CKPopUpImagePositon)kind button:(CKMainButton *)button{
    UIImageView * keyPop = [[UIImageView alloc]initWithImage:[CKMainKeyboardExpandedView createKeytopImageWithKind:kind button:button]];
    UILabel * label = [[UILabel alloc] initWithFrame:button.bounds];
    label.text = button.currentTitle;
    label.font = button.titleLabel.font;
    label.center = CGPointMake(keyPop.frame.size.width / 2.0, __PAN_UPPER_HEIGHT * 1.0/ Memoji_SCALE / 2.0);
    [keyPop addSubview:label];
    button.inputLabel = keyPop;
    
    keyPop.layer.shadowColor = [UIColor colorWithWhite:0.1 alpha:1.0].CGColor;
    keyPop.layer.shadowOffset = CGSizeMake(0, 2.0);
    keyPop.layer.shadowOpacity = 0.30;
    keyPop.layer.shadowRadius = 3.0;
    keyPop.clipsToBounds = NO;
    return keyPop;
}

+ (UIImage *)createKeytopImageWithKind:(CKPopUpImagePositon)kind button:(CKMainButton *)button
{
    CGMutablePathRef path = CGPathCreateMutable();
    CGPoint p = CGPointMake(__PADDING_X, __PADDING_Y);
    CGPoint p1 = CGPointZero;
    CGPoint p2 = CGPointZero;
    
    
    p.x += __PAN_UPPER_RADIUS;
    CGPathMoveToPoint(path, NULL, p.x, p.y);
    
    
    p.x += __PAN_UPPDER_WIDTH;
    CGPathAddLineToPoint(path, NULL, p.x, p.y);
    
    p.y += __PAN_UPPER_RADIUS;
    CGPathAddArc(path, NULL,
                 p.x, p.y,
                 __PAN_UPPER_RADIUS,
                 3.0*M_PI/2.0,
                 4.0*M_PI/2.0,
                 false);
    
    p.x += __PAN_UPPER_RADIUS;
    p.y += __PAN_UPPER_HEIGHT - __PAN_UPPER_RADIUS - __PAN_CURVE_SIZE;
    CGPathAddLineToPoint(path, NULL, p.x, p.y);
    
    p1 = CGPointMake(p.x, p.y + __PAN_CURVE_SIZE);
    switch (kind) {
        case CKPopUpImageRight:
            p.x -= __PAN_UL_WIDTH*2;
            break;
            
        case CKPopUpImageInner:
            p.x -= __PAN_UL_WIDTH;
            break;
            
        case CKPopUpImageLeft:
            break;
    }
    
    p.y += __PAN_MIDDLE_HEIGHT + __PAN_CURVE_SIZE*2;
    p2 = CGPointMake(p.x, p.y - __PAN_CURVE_SIZE);
    CGPathAddCurveToPoint(path, NULL,
                          p1.x, p1.y,
                          p2.x, p2.y,
                          p.x, p.y);
    
    p.y += __PAN_LOWER_HEIGHT - __PAN_CURVE_SIZE - __PAN_LOWER_RADIUS;
    CGPathAddLineToPoint(path, NULL, p.x, p.y);
    
    p.x -= __PAN_LOWER_RADIUS;
    CGPathAddArc(path, NULL,
                 p.x, p.y,
                 __PAN_LOWER_RADIUS,
                 4.0*M_PI/2.0,
                 1.0*M_PI/2.0,
                 false);
    
    p.x -= __PAN_LOWER_WIDTH;
    p.y += __PAN_LOWER_RADIUS;
    CGPathAddLineToPoint(path, NULL, p.x, p.y);
    
    p.y -= __PAN_LOWER_RADIUS;
    CGPathAddArc(path, NULL,
                 p.x, p.y,
                 __PAN_LOWER_RADIUS,
                 1.0*M_PI/2.0,
                 2.0*M_PI/2.0,
                 false);
    
    p.x -= __PAN_LOWER_RADIUS;
    p.y -= __PAN_LOWER_HEIGHT - __PAN_LOWER_RADIUS - __PAN_CURVE_SIZE;
    CGPathAddLineToPoint(path, NULL, p.x, p.y);
    
    p1 = CGPointMake(p.x, p.y - __PAN_CURVE_SIZE);
    
    switch (kind) {
        case CKPopUpImageRight:
            break;
            
        case CKPopUpImageInner:
            p.x -= __PAN_UL_WIDTH;
            break;
            
        case CKPopUpImageLeft:
            p.x -= __PAN_UL_WIDTH*2;
            break;
    }
    
    p.y -= __PAN_MIDDLE_HEIGHT + __PAN_CURVE_SIZE*2;
    p2 = CGPointMake(p.x, p.y + __PAN_CURVE_SIZE);
    CGPathAddCurveToPoint(path, NULL,
                          p1.x, p1.y,
                          p2.x, p2.y,
                          p.x, p.y);
    
    p.y -= __PAN_UPPER_HEIGHT - __PAN_UPPER_RADIUS - __PAN_CURVE_SIZE;
    CGPathAddLineToPoint(path, NULL, p.x, p.y);
    
    p.x += __PAN_UPPER_RADIUS;
    CGPathAddArc(path, NULL,
                 p.x, p.y,
                 __PAN_UPPER_RADIUS,
                 2.0*M_PI/2.0,
                 3.0*M_PI/2.0,
                 false);
    
    CGContextRef context;
    UIGraphicsBeginImageContext(CGSizeMake(__WIDTH,
                                           __HEIGHT));
    context = UIGraphicsGetCurrentContext();
    
    switch (kind) {
        case CKPopUpImageRight:
            CGContextTranslateCTM(context, 6.0, __HEIGHT);
            break;
        case CKPopUpImageInner:
            CGContextTranslateCTM(context, 0.0, __HEIGHT);
            break;
        case CKPopUpImageLeft:
            CGContextTranslateCTM(context, -6.0, __HEIGHT);
            break;
    }
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextAddPath(context, path);
    CGContextClip(context);
    CGRect frame = CGPathGetBoundingBox(path);
    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
    // [UIColor colorFromHexString:@"#a9a9ab"]
    CGContextFillRect(context, frame);
//    CGContextSetShadowWithColor(context, CGSizeMake(50,60), 0, [UIColor redColor].CGColor);
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    UIImage * image = [UIImage imageWithCGImage:imageRef scale:[[UIScreen mainScreen] scale] orientation:UIImageOrientationDown];
    CGImageRelease(imageRef);
    UIGraphicsEndImageContext();
    CFRelease(path);
    
    return image;
}

@end
