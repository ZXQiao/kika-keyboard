#import "CKMainKeyboardPopupList.h"
#import "CKMainButton.h"
#import "CKKeyboardUImanger.h"


@interface CKMainKeyboardPopupList()
{
    UIImageView * globalMaskView;
    UIImageView * emojiMaskView;
}
@end

@implementation CKMainKeyboardPopupList

- (void)layoutWithImage:(UIImage *)globalImage skin:(UIImage *)emojiImage
{
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    UIImage * pop_bg_Image = nil;
    UIImage * pop_selectedImage = nil;
    if (nowUseTheme) {
        if (nowUseTheme.isOnline) {
            pop_bg_Image = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_bg" resize:YES];
            pop_selectedImage = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_select" resize:YES];
        }
        else{
            pop_bg_Image = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_bg" resize:YES];
            pop_selectedImage = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_popup_select" resize:YES];
        }
    }
    if (pop_bg_Image == nil ) {
        pop_bg_Image = [UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_popup_bg",Default_Theme_Name]]];
    }
    if (pop_selectedImage == nil) {
        pop_selectedImage = [UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_popup_select",Default_Theme_Name]]];
    }

    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"popup_list_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * realDimensions = [[CKKeyboardUImanger shareUImanger] getRealDimensionsByModelAndDirection:dimensions];
    CGFloat fullWidth = [realDimensions[@"fullWidth"] floatValue];
    CGFloat fullHeight = [realDimensions[@"fullHeight"] floatValue];
    CGFloat horizonOffset = [realDimensions[@"horizonOffset"] floatValue];
    CGFloat verticalOffset = [realDimensions[@"verticalOffset"] floatValue];
    CGFloat shadeHeight = [realDimensions[@"shadeHeight"] floatValue];
    CGFloat maskHorizonPadding = [realDimensions[@"maskHorizonPadding"] floatValue];
    CGFloat maskVerticalPadding = [realDimensions[@"maskVerticalPadding"] floatValue];
    CGFloat maskHeight = [realDimensions[@"maskHeight"] floatValue];
    CGFloat iconLeftPadding = [realDimensions[@"iconLeftPadding"] floatValue];
    CGFloat iconRightPadding = [realDimensions[@"iconRightPadding"] floatValue];
    CGFloat iconSize = [realDimensions[@"iconSize"] floatValue];
    CGFloat fontSize = [realDimensions[@"fontSize"] floatValue];

    CGFloat iconTopPadding = (maskHeight - iconSize) / 2 + maskVerticalPadding;
    CGFloat maskWidth = fullWidth - maskHorizonPadding * 2;
    CGFloat labelLeft = iconLeftPadding + iconSize + iconRightPadding;
    CGFloat labelWidth = fullWidth - maskHorizonPadding - labelLeft;

    self.image = pop_bg_Image;
    self.frame = CGRectMake(0, 0, fullWidth, fullHeight);
    self.center = CGPointMake(fullWidth / 2 - horizonOffset, 0 - fullHeight / 2 + verticalOffset);

    UIImageView * globalIcon;
    UILabel * globalLabel;
    UIImageView * emojiIcon;
    UILabel * emojiLabel;

    UIFont * labelFont = [UIFont fontWithName:buttonDefaultFontName size:fontSize];

    globalMaskView = [[UIImageView alloc] init];
    globalMaskView.image = pop_selectedImage;
    globalMaskView.frame = CGRectMake(maskHorizonPadding, maskVerticalPadding, maskWidth, maskHeight);
    [self addSubview:globalMaskView];
    globalMaskView.hidden = YES;
    
    CGFloat emojiMaskTop = fullHeight - shadeHeight - maskHeight - maskVerticalPadding;

    emojiMaskView = [[UIImageView alloc] init];
    emojiMaskView.image = pop_selectedImage;
    emojiMaskView.frame = CGRectMake(maskHorizonPadding, emojiMaskTop, maskWidth, maskHeight);
    [self addSubview:emojiMaskView];
    emojiMaskView.hidden = YES;

    globalIcon = [[UIImageView alloc] init];
    globalIcon.image = globalImage;
    globalIcon.frame = CGRectMake(iconLeftPadding, iconTopPadding, iconSize, iconSize);
    [self addSubview:globalIcon];

    globalLabel = [[UILabel alloc] init];
    globalLabel.text = @"Switch keyboard";
    globalLabel.font = labelFont;
    globalLabel.frame = CGRectMake(labelLeft, iconTopPadding, labelWidth, iconSize);
    [self addSubview:globalLabel];
    globalLabel.textColor = _textColor;

    emojiIcon = [[UIImageView alloc] init];
    emojiIcon.image = emojiImage;
    emojiIcon.frame = CGRectMake(iconLeftPadding, emojiMaskTop + (maskHeight - iconSize) / 2, iconSize, iconSize);
    [self addSubview:emojiIcon];

    emojiLabel = [[UILabel alloc] init];
    emojiLabel.text = @"Emojis";
    emojiLabel.font = labelFont;
    emojiLabel.frame = CGRectMake(labelLeft, emojiMaskTop + (maskHeight - iconSize) / 2, labelWidth, iconSize);
    emojiLabel.textColor = _textColor;
    [self addSubview:emojiLabel];
}

- (void)highlightItem:(CGPoint)location
{

    CGRect topHalfFrame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height / 2.0);
    CGRect bottomHalfFrame = CGRectMake(0, self.bounds.size.height / 2.0, self.bounds.size.width, self.bounds.size.height / 2.0);
    if (!CGRectContainsPoint(topHalfFrame,location) && !CGRectContainsPoint(bottomHalfFrame,location)) {
        globalMaskView.hidden = YES;
        emojiMaskView.hidden = YES;
        return;
    }
    if (CGRectContainsPoint(topHalfFrame,location)) {
        globalMaskView.hidden = NO;
        emojiMaskView.hidden = YES;
    }
    else {
        globalMaskView.hidden = YES;
        emojiMaskView.hidden = NO;
    }
}

- (NSInteger)selectItem:(CGPoint)location
{
    CGRect bounds = self.bounds;
    if (location.x < bounds.origin.x || location.x > bounds.origin.x + bounds.size.width
        || location.y < bounds.origin.y || location.y > bounds.origin.y + bounds.size.height) {
        return -1;
    }
    if (location.y < bounds.origin.y + bounds.size.height / 2) {
        return 0;
    }
    else {
        return 1;
    }
}

@end
