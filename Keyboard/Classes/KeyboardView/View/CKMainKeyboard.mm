#import "CKMainKeyboard.h"
#import "CKPredictionKing.h"
#import "CKMainButton.h"
#import "CKMainKeyboardExpandedView.h"
#import "CKMainButtonPopUp.h"
#import "CKKeyboardUImanger.h"
#import "CKKeyboardPageInfo.h"
#import "CKCoverView.h"
#import "CKMainThemeModel.h"
#import "CKMainKeyboardPopupList.h"


#define popUpWRatioToButtonW 1.3
#define popUpHRatioToButtonH 1.1
#define POP_TO_BTN  6 // popup与button的间距

@interface CKMainKeyboard ()<UIGestureRecognizerDelegate>
{
    CKMainThemeModel * _nowUsemodel;
    CGFloat _buttonGap;
    NSDictionary * dimensions;
    BOOL isIOS9;
    CKCurrentiPhoneModel currentiPhoneModel;
    CGFloat _popup_height;
    CGFloat _popup_content_left_padding;
    CGFloat _popup_content_right_padding;
    CGFloat _popup_content_edge_bottom;
    CGFloat _firstline_popup_bottom_to_button_bottom;
    CGFloat _otherline_popup_bottom_to_button_top;
    UIImage * _globalImage;
    UIImage * _emojiImage;
    UIImage * _skinImage;
    CGFloat _popUpLineOneBottomY;
    CGFloat _popUpLineOtherBottomY;
}
@property (nonatomic, strong) NSString * fontName;
@property (nonatomic, strong) UIColor * textColor;
@property (nonatomic, assign) CGFloat fontSize;
@property (nonatomic, strong) UIColor * popUpColor;
// shift按键添加双击手势处理
@property (nonatomic, strong) UITapGestureRecognizer * tapTwoGestureRecognizer;
// shift的三种状态图片
@property (nonatomic,strong) UIImage * smallShiftImage;
@property (nonatomic,strong) UIImage * shiftImage;
@property (nonatomic,strong) UIImage * shiftCapImage;
@property (nonatomic, strong) UILongPressGestureRecognizer * optionsViewRecognizer;
@property (nonatomic, assign,getter=isLongPressOn) BOOL longPressOn;
@property (nonatomic, strong) CKMainButton * currentLongPressButton;

@property (nonatomic, strong) CKMainButton * firstSingleTouchButton; // 当前单击选中的按钮
@property (nonatomic, assign) BOOL firstButtonHasInput; // 第一个点击的按钮已经输入了
@property (nonatomic, strong) CKMainButton * secondSingleTouchButton; // 第二个单击的按钮
/**
 *  第二个按钮已经抬起,但是第一个按钮没有抬起,这个时候当TouchMove的时候不应该有PopUp的响应,定义一个变量来记录这种状态
 */
@property (nonatomic, assign) BOOL secondButtonEndButFirstButtonExit;

/**
 *  在长按的时候用户是不是再次单击了,如果是的话当长按手势结束的时候就不能再次输入字符了
 */
@property (nonatomic, assign) BOOL whenLongPressedUserTouchedAgain;
@property (nonatomic, strong) UIImage * singlePopUpImage;
@property (nonatomic,strong) CKPredictionKing * predictKing;
@property (nonatomic, assign) BOOL userAllowPredict;
@property (nonatomic, assign) BOOL userAllowAutoCorrect;

@property (nonatomic, strong) NSMutableArray * allButtonsArray;
@property (nonatomic, strong) NSMutableArray * curPageInfoModelArray;
@property (nonatomic, strong) CKKeyboardPageInfo * pageInfo;
@property (nonatomic, assign) CKKeyboardType keyboardType;
@property (nonatomic, strong) CKCoverView * coverView;
@property (nonatomic, assign) BOOL userKeepPressedShitNow;
// 记录功能按钮是否触发了touchBegan,如果触发了,delete.return.global这些按钮才需要在end的时候去触发对应方法,否则说明
// 只是经过了这些按钮,不需要在最终触发方法
@property (nonatomic, assign) BOOL funcButtonHasClicked;
@property (nonatomic, strong) NSDate * fisrtFireShiftDate;
@property (nonatomic, strong) CKMainButton * shiftButton;
@property (nonatomic, strong) CKMainButton * num_shiftButton;
@property (nonatomic, strong) CKMainButton * spaceButton;
@property (nonatomic, strong) CKMainButton * deleteButton;
@property (nonatomic, strong) CKMainButton * altButton;
@property (nonatomic, strong) CKMainButton * emojiButton;
@property (nonatomic, strong) CKMainButton * globalButton;
@property (nonatomic, strong) CKMainButton * returnButton;

@property (nonatomic, assign) CKPopStyle  popSytle;
@property (nonatomic, assign) CKRemovePopStyle  removeStyle;

@property (nonatomic, strong) CKMainKeyboardPopupList * popupList;
@property (nonatomic, strong) CKCoverView * popupListCoverView;
@property (nonatomic, assign) CGFloat currentWidth;
@property (nonatomic, assign) BOOL popupAdded;
@property (nonatomic, strong) UIFont * popUpFont;
@property (nonatomic, assign) CGFloat popFontSize;
@property (nonatomic, strong) NSString * popupFontName;

@end

@implementation CKMainKeyboard
- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"key_popup_dimensions.plist" ofType:nil];
    dimensions = [[CKKeyboardUImanger shareUImanger] getDimensionsByModel:[NSDictionary dictionaryWithContentsOfFile:resourceTypePath]];
    if ([CKCommonTools systemVersion] - 9.0 > -FLOAT_EPSINON) {
        isIOS9 = YES;
    }
    currentiPhoneModel = [CKKeyboardUImanger shareUImanger].currentiPhoneModel;
}

- (void)resetAllFuncButtonStatus
{
    self.funcButtonHasClicked = NO;
    self.spaceButton.highlighted = NO;
    self.deleteButton.highlighted = NO;
    self.altButton.highlighted = NO;
    self.emojiButton.highlighted = NO;
    self.globalButton.highlighted = NO;
    self.spaceButton.highlighted = NO;
    self.returnButton.highlighted = NO;
}

- (BOOL)oneOrMoreFuncButtonIsHighlighted
{
    return _shiftButton.isHighlighted || _spaceButton.isHighlighted || _deleteButton.isHighlighted || _emojiButton.isHighlighted || _globalButton.isHighlighted || _spaceButton.isHighlighted || _returnButton.isHighlighted;
}

// 在切换界面的时候需要杀掉所有的状态变量
- (void)killAllStatusVaribale
{
    _longPressOn = _secondButtonEndButFirstButtonExit = _whenLongPressedUserTouchedAgain = _userKeepPressedShitNow = _funcButtonHasClicked = NO;
    self.firstSingleTouchButton = nil;
    self.secondSingleTouchButton = nil;
    if (self.shiftButton && _keyboardType != CKKeyboardTypeCharacter) {
        [self.shiftButton setImage:nil forState:UIControlStateNormal];
        self.shiftButton = nil;
    }
    self.returnType = _returnType;
}


- (NSMutableArray *)allButtonsArray
{
    if (_allButtonsArray == nil) {
        _allButtonsArray = [NSMutableArray array];
    }
    return _allButtonsArray;
}

- (void)setShiftStatus:(CKShiftStatus)shiftStatus
{
    self.returnType = _returnType;
    if (self.userKeepPressedShitNow) {
        return;
    }
    [self realSetShiftStatus:shiftStatus];
}

- (void)realSetShiftStatus:(CKShiftStatus)shiftStatus
{
    switch (shiftStatus) {
        case CKShiftStatusAllLower: // 小写
            [self.shiftButton setImage:self.smallShiftImage forState:UIControlStateNormal];
            break;
        case CKShiftStatusCapitalized: // 首字母大写
            [self.shiftButton setImage:self.shiftImage forState:UIControlStateNormal];
            break;
        case CKShiftStatusAllUpper: // 锁定大写
            [self.shiftButton setImage:self.shiftCapImage forState:UIControlStateNormal];
            break;
        case CKShiftStatusLongPressedLower: // 长按小写
            [self.shiftButton setImage:self.smallShiftImage forState:UIControlStateNormal];
            break;
        case CKShiftStatusLongPressedUpper: // 长按小写
            [self.shiftButton setImage:self.smallShiftImage forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    if (_shiftStatus != shiftStatus) {
        _shiftStatus = shiftStatus;
        if (self.keyboardType == CKKeyboardTypeCharacter) {
            [self SwicthCharacterLowerAndUpper];
        }
    }
}

-(void)SwicthCharacterLowerAndUpper
{
    NSDictionary * realDimensions;
    if ([CKCommonTools screenDirectionIsLand]) {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_LANDSCAPE];
    }
    else {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_PORTRAIT];
    }

    CGFloat button_upper_content_edge_bottom;
    CGFloat button_lower_content_edge_bottom;
    CGFloat button_upper_content_edge_left;
    CGFloat button_lower_content_edge_left;
    UIEdgeInsets upperContentEdgeInsets;
    UIEdgeInsets lowerContentEdgeInsets;

    if (currentiPhoneModel != CKCurrentiPhoneModeliPhone5 || [CKCommonTools screenDirectionIsLand]) {
        button_upper_content_edge_bottom = [realDimensions[@"button_upper_content_edge_bottom"] floatValue];
        button_lower_content_edge_bottom = [realDimensions[@"button_lower_content_edge_bottom"] floatValue];
        upperContentEdgeInsets = UIEdgeInsetsMake(0, 0, button_upper_content_edge_bottom, 0);
        lowerContentEdgeInsets = UIEdgeInsetsMake(0, 0, button_lower_content_edge_bottom, 0);
    }
    else {
        if (isIOS9) {
            button_upper_content_edge_bottom = [realDimensions[@"button_upper_content_edge_bottom_ios9"] floatValue];
            button_lower_content_edge_bottom = [realDimensions[@"button_lower_content_edge_bottom_ios9"] floatValue];
            upperContentEdgeInsets = UIEdgeInsetsMake(0, 0, button_upper_content_edge_bottom, 0);
            lowerContentEdgeInsets = UIEdgeInsetsMake(0, 0, button_lower_content_edge_bottom, 0);
        }
        else {
            button_upper_content_edge_bottom = [realDimensions[@"button_upper_content_edge_bottom_ios8"] floatValue];
            button_lower_content_edge_bottom = [realDimensions[@"button_lower_content_edge_bottom_ios8"] floatValue];
            button_upper_content_edge_left = [realDimensions[@"button_upper_content_edge_left_ios8"] floatValue];
            button_lower_content_edge_left = [realDimensions[@"button_lower_content_edge_left_ios8"] floatValue];
            upperContentEdgeInsets = UIEdgeInsetsMake(0, button_upper_content_edge_left, button_upper_content_edge_bottom, 0);
            lowerContentEdgeInsets = UIEdgeInsetsMake(0, button_lower_content_edge_left, button_lower_content_edge_bottom, 0);
        }
    }

    CGFloat ios_buttonFontSize = [realDimensions[@"buttonFontSize"] floatValue];
    CGFloat ios_buttonFontSmallSize = [realDimensions[@"buttonFontSmallSize"] floatValue];

    UIFont * upperTitleFont = nil;
    UIFont * lowerTitleFont = nil;

    BOOL needChangeFont = YES;
    if ([self.fontName isEqualToString:@"LithosPro-Black"] || [self.fontName isEqualToString:@"Chalkboard SE"]) {
        needChangeFont = NO;
    }

    for (CKMainButton * button in self.allButtonsArray) {
        if (![button.style isEqualToString:@"letter"]) {
            continue;
        }
        else{
            if (upperTitleFont == nil) {
                upperTitleFont = [UIFont fontWithName:button.titleLabel.font.fontName size:ios_buttonFontSize];
                lowerTitleFont = [UIFont fontWithName:button.titleLabel.font.fontName size:ios_buttonFontSmallSize];
            }
            if(self.shiftStatus == CKShiftStatusCapitalized || self.shiftStatus == CKShiftStatusLongPressedUpper || self.shiftStatus == CKShiftStatusAllUpper){
                    [button setTitle:button.currentTitle.uppercaseString forState:UIControlStateNormal];
                    if (needChangeFont && [button.style isEqualToString:@"letter"]) {
                        button.titleLabel.font = upperTitleFont;
                        button.contentEdgeInsets = upperContentEdgeInsets;
                    }
                }
                else if (self.shiftStatus == CKShiftStatusLongPressedLower || self.shiftStatus == CKShiftStatusAllLower){
                    [button setTitle:button.currentTitle.lowercaseString forState:UIControlStateNormal];
                    if (needChangeFont && [button.style isEqualToString:@"letter"]) {
                        button.titleLabel.font = lowerTitleFont;
                        button.contentEdgeInsets = lowerContentEdgeInsets;
                    }
                }
        }
    }
}

- (void)addPopupListForEmojiLongPressed
{
    self.popupListCoverView = [[CKCoverView alloc] init];
    self.popupListCoverView.backgroundColor = [UIColor clearColor];
    self.popupListCoverView.frame = self.bounds;
    [self addSubview:self.popupListCoverView];
    self.popupList = [[CKMainKeyboardPopupList alloc] init];
    self.popupList.textColor = _textColor;
    [self.popupList layoutWithImage:_globalImage skin:_emojiImage];
    [self.emojiButton addSubview:self.popupList];
    [self bringSubviewToFront:self.emojiButton];
    self.emojiButton.enabled = NO;
    self.emojiButton.adjustsImageWhenDisabled = NO;
    [self.emojiButton setImage:nil forState:UIControlStateNormal];
}

- (void)dismissPopupList
{
    if (self.popupListCoverView != nil) {
        [self.popupListCoverView removeFromSuperview];
        self.popupListCoverView = nil;
        [self.popupList removeFromSuperview];
        self.popupList = nil;
        self.emojiButton.highlighted = NO;
        self.emojiButton.enabled = YES;
        [self.emojiButton setImage:_skinImage forState:UIControlStateNormal];
    }
}

- (void)selectFunctionForEmojiButton:(NSInteger)selectIdx
{
    if (selectIdx == 0) {
        [self globalPressedNow:nil];
    }
    else if (selectIdx == 1) {
        [self emojiPressed:nil];
    }
    [self dismissPopupList];
}

- (UILongPressGestureRecognizer *)optionsViewRecognizer
{
    if (_optionsViewRecognizer == nil) {
        UILongPressGestureRecognizer *longPressGestureRecognizer =
        [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(buttonShouldshowExpandedInputView:)];
        longPressGestureRecognizer.minimumPressDuration = 0.4;
        longPressGestureRecognizer.delegate = self;
        longPressGestureRecognizer.allowableMovement = 50.0;
        _optionsViewRecognizer = longPressGestureRecognizer;
    }
    return _optionsViewRecognizer;
}


- (void)buttonShouldshowExpandedInputView:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (self.popupListCoverView) {
        if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
            return;
        }
        else if(gestureRecognizer.state == UIGestureRecognizerStateChanged) {
            CGPoint location = [gestureRecognizer locationInView:self.popupList];
            [self.popupList highlightItem:location];
            return;
        }
        else {
            CGPoint location = [gestureRecognizer locationInView:self.popupList];
            NSInteger selectIdx = [self.popupList selectItem:location];
            if (selectIdx != -1) {
                [self selectFunctionForEmojiButton:selectIdx];
            }
            self.longPressOn = NO;
            self.currentLongPressButton = nil;
            self.firstSingleTouchButton = nil;
            return;
        }
    }
    CGPoint location = [gestureRecognizer locationInView:self];
    self.longPressOn = YES;
    self.returnType = _returnType;
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) { // 开始
        BOOL isLand = [CKCommonTools screenDirectionIsLand];
        for (int i = 0; i < self.allButtonsArray.count; i ++) {
            CKMainButton * longPressedButton = self.allButtonsArray[i];
            CGRect currentRect = isLand ? CGRectFromString(longPressedButton.touchArea_l) :CGRectFromString(longPressedButton.touchArea);
            if(CGRectContainsPoint(currentRect, location)){
                if ([longPressedButton.style isEqualToString:@"letter"] || [longPressedButton.style isEqualToString:@"symbol"]){
                    if ([longPressedButton.style isEqualToString:@"symbol"]) {
                        longPressedButton.inputOptions = @[@".",@","];
                    }
                    if (longPressedButton.inputOptions.count != 1) {
                        if (longPressedButton.inputLabel) { // 如果inputLabel仍然存在,先移除
                            [longPressedButton.inputLabel removeFromSuperview];
                            longPressedButton.inputLabel = nil;
                        }
                        [self addExpandedViewWhenLongPressed:longPressedButton];
                    }
                    else{
                        [self addPopupToButton:longPressedButton WithPopStyle:_popSytle];
                    }
                }
                else if([longPressedButton.style isEqualToString:@"shift"]){
                    self.userKeepPressedShitNow = YES;
                    if (self.shiftStatus == CKShiftStatusCapitalized) {
                        self.shiftStatus = CKShiftStatusLongPressedUpper;
                        [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusLongPressedLower];
                    }
                    else if (self.shiftStatus == CKShiftStatusAllLower)
                    {
                        self.shiftStatus = CKShiftStatusLongPressedLower;
                        [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusLongPressedLower];
                    }
                }
                else if([longPressedButton.style isEqualToString:@"delete"]){
                    self.returnType = _returnType;
                    longPressedButton.highlighted = YES;
                    if ([self.englishboardDelegate respondsToSelector:@selector(deleteButtonLongPressed:gesture:)]) {
                        [self.englishboardDelegate deleteButtonLongPressed:longPressedButton gesture:gestureRecognizer];
                    }
                    
                }
                else if([longPressedButton.style isEqualToString:@"emoji"]){
                    longPressedButton.highlighted = YES;
                    [self addPopupListForEmojiLongPressed];
                }
                else if([longPressedButton.style isEqualToString:@"global"]){
                    longPressedButton.highlighted = YES;
                }
                else if([longPressedButton.style isEqualToString:@"space"]){
                    longPressedButton.highlighted = YES;
                }
                else if([longPressedButton.style isEqualToString:@"return"]){
                    longPressedButton.highlighted = YES;
                }
                else if([longPressedButton.style isEqualToString:@"alt"]){
                    longPressedButton.highlighted = YES;
                }
                self.currentLongPressButton = longPressedButton;
                break;
            }
        }
    }
    else if(gestureRecognizer.state == UIGestureRecognizerStateChanged)
    {
        self.currentLongPressButton.expandedView.currentPoint = location;
    }
    else // end
    {
        /**
         * 剩余三种状态
         * UIGestureRecognizerStateEnded,
         * UIGestureRecognizerStateCancelled,
         * UIGestureRecognizerStateFailed,
         */
        self.longPressOn = NO;
        if (self.userKeepPressedShitNow) {
            self.userKeepPressedShitNow = NO;
            if (![self.firstSingleTouchButton.style isEqualToString:@"shift"]) {
                if (self.shiftStatus == CKShiftStatusAllLower) {
                    [self realSetShiftStatus:CKShiftStatusCapitalized];
                }
                else if (self.shiftStatus == CKShiftStatusCapitalized) {
                    [self realSetShiftStatus:CKShiftStatusAllLower];
                }
            }
        }
        if (self.shiftStatus == CKShiftStatusLongPressedLower){
            self.shiftStatus = CKShiftStatusAllLower;
            [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusAllLower];
        }
        else if (self.shiftStatus == CKShiftStatusLongPressedUpper){
            self.shiftStatus = CKShiftStatusCapitalized;
            [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusCapitalized];
        }
        if (self.popupListCoverView == nil) {
            self.currentLongPressButton.highlighted = NO;
        }
        if([self.currentLongPressButton.style isEqualToString:@"delete"]){
            self.currentLongPressButton.highlighted = YES;
            if ([self.englishboardDelegate respondsToSelector:@selector(deleteButtonLongPressed:gesture:)]) {
                [self.englishboardDelegate deleteButtonLongPressed:self.currentLongPressButton gesture:gestureRecognizer];
            }
        }
#pragma mark - 输出内容
        if (self.whenLongPressedUserTouchedAgain) {
            self.currentLongPressButton = nil;
            self.whenLongPressedUserTouchedAgain = NO;
            return;
        }
        else
        {
            [self longPressedButtonInPutWithLocation:location];
            
            [self removeUnExpectedInputLabel];
        }
    }
}

- (void)addExpandedViewWhenLongPressed:(CKMainButton *)b
{

    CKMainKeyboardExpandedView * expandView = [[CKMainKeyboardExpandedView alloc] init];
    expandView.popFontSize = self.popFontSize;
    expandView.fontName = self.popupFontName;
    if (CGRectGetMaxX(b.frame) >= self.center.x) { // 向左边伸展
        b.position = CKEnglishButtonPositionRight;
    }
    else
    {
        b.position = CKEnglishButtonPositionLeft;
    }
    
    b.expandedView = expandView;
    expandView.tag = 5201314;
    NSMutableArray *tmpArray = [NSMutableArray arrayWithCapacity:b.inputOptions.count];
    if(self.shiftStatus == CKShiftStatusCapitalized || self.shiftStatus == CKShiftStatusLongPressedUpper || self.shiftStatus == CKShiftStatusAllUpper){
        for (int i = 0; i < b.inputOptions.count; i++) {
            NSString *str = b.inputOptions[i];
            tmpArray[i] = str.uppercaseString;
        }
    }
    else if (self.shiftStatus == CKShiftStatusLongPressedLower || self.shiftStatus == CKShiftStatusAllLower){
        for (int i = 0; i < b.inputOptions.count; i++) {
            NSString *str = b.inputOptions[i];
            tmpArray[i] = str.lowercaseString;
        }
    }
    b.inputOptions = tmpArray;
    
    //    expandView.fontName = self.fontName;
    //    if ([_nowUsemodel.themeName isEqualToString:@"ios"]) {
    //        expandView.fontSize = ios_popupFontSize;
    //    }
    //    else {
    //        expandView.fontSize = popupFontSize;
    //    }
    //
    //    expandView.fontSize = if
    //expandView.englishButton = b;
    NSDictionary * realDimensions;
    if ([CKCommonTools screenDirectionIsLand]) {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_LANDSCAPE];
    }
    else {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_PORTRAIT];
    }
    CGFloat popup_content_left_padding = [realDimensions[@"popup_content_left_padding"] floatValue];
    CGFloat firstline_popup_bottom_to_button_bottom = [realDimensions[@"firstline_popup_bottom_to_button_bottom"] floatValue];
    CGFloat otherline_popup_bottom_to_button_top = [realDimensions[@"otherline_popup_bottom_to_button_top"] floatValue];
    
    [expandView setEnglishButton:b dimensions:realDimensions];
    expandView.backgroundColor = [UIColor clearColor];
    
    
    /**
     *  如果按钮的最大X值小于View的centerX,popUp应该向右边伸展,反之向左边
     */
    CGFloat expandViewCenterX = 0;
    if (CGRectGetMaxX(b.frame) >= self.center.x) { // 向左边伸展
        expandViewCenterX = -(expandView.frame.size.width / 2- b.frame.size.width );
        expandView.selectedIndex = b.inputOptions.count - 1;
        if (_nowUsemodel.popupstyle_index.intValue == 0 && (b.buttonStyle != CKEnglishButtonStyleEmoji)) {
            expandViewCenterX += popup_content_left_padding;
        }
    }
    else
    {
        expandView.selectedIndex = 0;
        expandViewCenterX = expandView.center.x;
        if (_nowUsemodel.popupstyle_index.intValue == 0 && (b.buttonStyle != CKEnglishButtonStyleEmoji)) {
            expandViewCenterX -= popup_content_left_padding;
        }
    }
    
    if (_nowUsemodel.popupstyle_index.intValue == 0 && (b.buttonStyle != CKEnglishButtonStyleEmoji)) {
        if (b.rowTag == 1 || b.frame.origin.y < b.frame.size.height) {
            expandView.center = CGPointMake(expandViewCenterX, expandView.center.y - expandView.frame.size.height + b.frame.size.height - firstline_popup_bottom_to_button_bottom);
        }
        else {
            expandView.center = CGPointMake(expandViewCenterX, expandView.center.y - expandView.frame.size.height - otherline_popup_bottom_to_button_top);
        }
    }
    else {
        CGFloat deltaX = 0;
        if (b.position == CKEnglishButtonPositionLeft) {
            deltaX = (popUpWRatioToButtonW - 1) * .5 * b.frame.size.width;
        }
        else{
            deltaX = -(popUpWRatioToButtonW - 1) * .5 * b.frame.size.width;
        }
         expandView.center = CGPointMake(expandViewCenterX - deltaX, expandView.center.y - b.frame.size.height - POP_TO_BTN);
    }
    [b addSubview:expandView];
    [self bringSubviewToFront:b];
}

- (void)longPressedButtonInPutWithLocation:(CGPoint)location
{
    NSString * buttonStyle = self.currentLongPressButton.style;
    if ([buttonStyle isEqualToString:@"letter"] || [buttonStyle isEqualToString:@"symbol"]) {
        int selectedIndex = self.currentLongPressButton.expandedView.selectedIndex;
        int index = selectedIndex;
        if (self.currentLongPressButton.position == CKEnglishButtonPositionRight) {
            index = (int)self.currentLongPressButton.inputOptions.count - 1 - selectedIndex;
        }
        NSString * outPutStirng = self.currentLongPressButton.inputOptions[index];
        self.currentLongPressButton.outPutToTextFiledStr = outPutStirng;
        BOOL needPredict = [self.currentLongPressButton.currentTitle isEqualToString:outPutStirng];
        [self inputIfItIsLetterButton:_currentLongPressButton location:location needPredict:needPredict];
        [self.currentLongPressButton.expandedView removeFromSuperview];
        [self.currentLongPressButton setBackgroundImage:self.currentLongPressButton.buttonImage forState:UIControlStateNormal];
        [self.currentLongPressButton setTitleColor:self.currentLongPressButton.keyColor forState:UIControlStateNormal];
    }
    else{ // 如果是功能按钮长按
        self.currentLongPressButton.highlighted = NO;
        if ([buttonStyle isEqualToString:@"shift"]) {
            
        }
        else if ([buttonStyle isEqualToString:@"delete"]) {
            
        }
        else if ([buttonStyle isEqualToString:@"emoji"]) {
            if (self.popupListCoverView != nil) {
                self.currentLongPressButton.highlighted = YES;
            }
        }
        else if ([buttonStyle isEqualToString:@"global"]) {
            
        }
        else if ([buttonStyle isEqualToString:@"space"]) {
            [self spacePressed:nil];
        }
        else if ([buttonStyle isEqualToString:@"return"]) {
            [self returnPressed:nil];
        }
    }
    
    /**
     * 在TouchBegan的时候,CurrentLongPressButton与CurrentSingleButton会同时生成,但是CurrentSingleButton是我们不需要的
     * 所以在这里如果发现CurrentSingleButton与LongPressButton是相同的话,要将SingleButton干掉
     */
    self.firstSingleTouchButton = nil;
    self.currentLongPressButton = nil;
}
- (void)play
{
    if ([self.englishboardDelegate respondsToSelector:@selector(shouldPlaySounds)]) {
        [self.englishboardDelegate shouldPlaySounds];
    }
}

- (void)touchBegan:(UITouch *)touch
{
    if (self.firstSingleTouchButton && !self.firstButtonHasInput) {
        /**
         *  如果之前已经有按键按下但是还没有输入
         *  这时应该输入对应字符
         *
         */
        if ([self.firstSingleTouchButton.style isEqualToString:@"letter"] || [self.firstSingleTouchButton.style isEqualToString:@"symbol"]) { // 如果第一个按钮是字母按钮,将第一个按钮输出
            NSString * outPutString;
            if (self.firstSingleTouchButton.expandedView) { // 如果存在,证明是长按按钮
                int selectedIndex = self.firstSingleTouchButton.expandedView.selectedIndex;
                int index = selectedIndex;
                if (self.firstSingleTouchButton.position == CKEnglishButtonPositionRight) {
                    index = (int)self.firstSingleTouchButton.inputOptions.count - 1 - selectedIndex;
                }
                outPutString = self.currentLongPressButton.inputOptions[index];
            }
            else
            {
                outPutString = self.firstSingleTouchButton.currentTitle;
            }
            self.firstSingleTouchButton.outPutToTextFiledStr = outPutString;
            [self inputIfItIsLetterButton:_firstSingleTouchButton location:_firstSingleTouchButton.center needPredict:YES];
            self.firstButtonHasInput = YES;
            
            [self removeBtnInputViewIfItIsLetterButton:_firstSingleTouchButton];
            
        }
        else if ([self.firstSingleTouchButton.style isEqualToString:@"space"]) {
            [self spacePressed:self.firstSingleTouchButton];
            self.firstButtonHasInput = YES;
            self.firstSingleTouchButton.highlighted = NO;
        }
        else if ([self.firstSingleTouchButton.style isEqualToString:@"shift"]) {
            self.userKeepPressedShitNow = YES;
            if (self.shiftStatus == CKShiftStatusCapitalized) {
                self.shiftStatus = CKShiftStatusLongPressedUpper;
                [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusLongPressedLower];
            }
            else if (self.shiftStatus == CKShiftStatusAllLower)
            {
                self.shiftStatus = CKShiftStatusLongPressedLower;
                [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusLongPressedLower];
            }
        }
        else {
            // Do nothing....
            self.firstSingleTouchButton.highlighted = NO;
        }
    }
    
    if (self.currentLongPressButton && self.isLongPressOn) { // 如果之前有长按的按钮
        if (self.currentLongPressButton.inputOptions.count != 1) {
            [self.currentLongPressButton.expandedView removeFromSuperview];
        }
        else{
            if (self.currentLongPressButton.subviews.count > 2) {
                for (UIView * view in self.currentLongPressButton.subviews) {
                    if (view.tag == 5201314) {
                        [view removeFromSuperview];
                    }
                }
            }
        }
        self.whenLongPressedUserTouchedAgain = YES;
        self.longPressOn = NO;
        /**
         *  干掉长按手势,不让他继续响应
         *  但是干掉长按手势之后要重新添加一下,不然该手势会被彻底废除
         *  同时将LongPressedButton置为空
         */
        self.optionsViewRecognizer = nil;
        self.currentLongPressButton = nil;
        [self addGestureRecognizer:self.optionsViewRecognizer];
        self.firstButtonHasInput = NO;
        self.firstSingleTouchButton = nil;
    }
    
    /**
     *  如果用户上来就长按某个按钮,是会触发TouchBegan的,但是TouchBegan永远在任何手势之前触发
     *  所以会导致下边的遍历按钮方法仍然会被触发,也就是会有一个不应该出现的firstSingleTouchButton
     */
    
    BOOL isLand = [CKCommonTools screenDirectionIsLand];
    CGPoint location = [touch locationInView:self];
    for (int i = 0; i < self.allButtonsArray.count; i ++) {
        CKMainButton * b = self.allButtonsArray[i];
        CGRect currentRect = isLand ? CGRectFromString(b.touchArea_l) :CGRectFromString(b.touchArea);
        if(CGRectContainsPoint(currentRect, location)){
            if (self.firstSingleTouchButton) { // 如果第一个按下的按钮不为空
                if(self.firstSingleTouchButton != b){
                    // 并且第一个按钮不能等于b
                    // 记录这个按钮的目的是最终输出,所以除了字母键,逗号键,空格键之外不需要记录
                    if (([b.style isEqualToString:@"letter"] || [b.style isEqualToString:@"symbol"] || [b.style isEqualToString:@"space"])) {
                        self.secondButtonEndButFirstButtonExit = NO;
                        if (!([b.style isEqualToString:@"space"])) { // 除了空格键应该popUp
                            [self addPopupToButton:b WithPopStyle:_popSytle];
                        }
                    }
                    if (!([b.style isEqualToString:@"letter"] || [b.style isEqualToString:@"symbol"] || [b.style isEqualToString:@"shift"])) { // 设置highlighted
                        b.highlighted = YES;
                    }
                    self.secondSingleTouchButton = b;
                }
                else{} // 如果等于b,应该不存在这种情况,第一个按钮没放手,然后这个按钮又触发了touchBagan方法,不存在这种情况
            }
            else
            {
                self.firstSingleTouchButton = b;
                self.firstButtonHasInput = NO;
                if ([b.style isEqualToString:@"letter"]) {
                    // 如果按下的按钮是letter或者symbol的时候才需要弹起popUp
                    [self addPopupToButton:b WithPopStyle:_popSytle];
                }
                else if([b.style isEqualToString:@"shift"]) {
                    double timeInterval = [[NSDate date] timeIntervalSinceDate:self.fisrtFireShiftDate];
                    TMPLog(@"%lf===now===%d",timeInterval,self.userKeepPressedShitNow);
                    self.fisrtFireShiftDate = [NSDate date];
                    if (timeInterval < .22 && timeInterval > 0) { // 说明双击shift按键了
                        self.shiftStatus = CKShiftStatusAllUpper;
                        return;
                    }
                    else {
                        if (self.shiftStatus == CKShiftStatusAllUpper || self.shiftStatus == CKShiftStatusCapitalized) {
                            self.shiftStatus = CKShiftStatusAllLower;
                            [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusAllLower];
                        }
                        else {
                            self.shiftStatus = CKShiftStatusCapitalized;
                            [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusCapitalized];
                        }
                        self.funcButtonHasClicked = YES;
                    }
                }
                else if([b.style isEqualToString:@"delete"]){
                    b.highlighted = YES;
                    [self deletePressed:b];
                    self.funcButtonHasClicked = YES;
                }
                else if([b.style isEqualToString:@"emoji"]){
                    b.highlighted = YES;
                    self.funcButtonHasClicked = YES;
                }
                else if([b.style isEqualToString:@"global"]){
                    b.highlighted = YES;
                    self.funcButtonHasClicked = YES;
                }
                else if([b.style isEqualToString:@"symbol"]){
                    [self addPopupToButton:b WithPopStyle:_popSytle];
                }
                else if([b.style isEqualToString:@"space"]){
                    b.highlighted = YES;
                    self.funcButtonHasClicked = YES;
                }
                else if([b.style isEqualToString:@"return"]){
                    b.highlighted = YES;
                    self.funcButtonHasClicked = YES;
                    self.returnType = _returnType;
                }
                else if([b.style isEqualToString:@"alt"]){
                    CKKeyboardType changeToKeyboardType = CKKeyboardTypeCharacter;
                    if (self.keyboardType == CKKeyboardTypeCharacter) {
                        changeToKeyboardType = CKKeyboardTypeNumberONE;
                    }
                    else if (self.keyboardType == CKKeyboardTypeNumberONE || self.keyboardType == CKKeyboardTypeNumber2) {
                        changeToKeyboardType = CKKeyboardTypeCharacter;
                        
                    }
                    [self changeKeyboardWithCurrentKeyboardType:changeToKeyboardType];
                    [self killAllStatusVaribale];
                }
                else if([b.style isEqualToString:@"num_shift"]){
                    CKKeyboardType changeToKeyboardType = CKKeyboardTypeCharacter;
                    if (self.keyboardType == CKKeyboardTypeNumber2) {
                        changeToKeyboardType = CKKeyboardTypeNumberONE;
                    }
                    else if (self.keyboardType == CKKeyboardTypeNumberONE) {
                        changeToKeyboardType = CKKeyboardTypeNumber2;
                    }
                    [self changeKeyboardWithCurrentKeyboardType:changeToKeyboardType];
                    [self killAllStatusVaribale];
                }
            }
            /**
             *  这个break很重要哦,不然的话会做很多没必要的循环,下面的touchMoved和TouchEnded都有这个操作
             */
            break;
        }
    }
}

- (void)touchesBegan: (NSSet *)touches withEvent: (UIEvent *)event
{
    if (self.popupListCoverView) {
        CGPoint location = [[touches anyObject] locationInView:self.popupList];
        [self.popupList highlightItem:location];
        return;
    }
    [self play];
    for (UITouch* touch in touches) {
        [self touchBegan:touch];
    }
}

- (void)touchMoved:(UITouch *)touch
{
    BOOL isLand = [CKCommonTools screenDirectionIsLand];
    CGPoint location = [touch locationInView:self];
    NSInteger buttonCount = self.allButtonsArray.count;
    for (int i = 0; i < buttonCount; i ++) {
        CKMainButton * b = self.allButtonsArray[i];
        CGRect currentRect = isLand ? CGRectFromString(b.touchArea_l) :CGRectFromString(b.touchArea);
        if (CGRectContainsPoint(currentRect, location))
        {
            /**
             *  如果单个按钮,正常显示
             */
            if (!self.secondSingleTouchButton) {
                /**
                 *  第二个按钮不存在的话,说明当前只有一个按钮按下
                 */
                if (b == self.firstSingleTouchButton) {
                    /**
                     *  当按钮等同于第一个按钮的时候,说明用户在同一个按钮上做小幅度的滑动
                     *  此时直接返回即可
                     */
                    return;
                }
                else
                {
                    BOOL isfuncbtn = [self resetFuncButtonStateToNormal:self.firstSingleTouchButton];
                    if (isfuncbtn) { // 如果是除了symbol之外的funcBtn,需要设置funcBtn的状态
                        self.firstSingleTouchButton.highlighted = NO;
                    }
                    else
                    {
                        if ([_nowUsemodel.themeName isEqualToString:Default_Theme_Name]) {
                            if(_firstSingleTouchButton) {
                                if([_firstSingleTouchButton.style isEqualToString:@"letter"] || [_firstSingleTouchButton.style isEqualToString:@"symbol"] ){
                                    [_firstSingleTouchButton.inputLabel removeFromSuperview];
                                    _firstSingleTouchButton.highlighted = NO;
                                    _firstSingleTouchButton.inputLabel = nil;
                                }
                            }
                        }
                        else{
                            [self removeBtnInputViewIfItIsLetterButton:_firstSingleTouchButton];
                        }
                    }
                    
                    
                    if (([b.style isEqualToString:@"letter"] || [b.style isEqualToString:@"symbol"]) ) {
                        /**
                         *  如果不等同于第一个按钮,第一个按钮的popUp应该消失,并且当前的按钮成为第一个按钮
                         */
                        
                        [self addPopupToButton:b WithPopStyle:_popSytle];
                        self.firstSingleTouchButton = b;
                        self.firstButtonHasInput = NO;
                    }
                    if ([b.style isEqualToString:@"space"]) { // 如果是空格键,要记录一下并且设置space的highlight
                        self.firstSingleTouchButton = b;
                        self.firstButtonHasInput = NO;
                        b.highlighted = YES;
                    }
                }
            }
            else
            {
                /**
                 *  第二个按钮存在,第一个按钮一定存在
                 *  如果b等于第一个按钮的话,在第二个按钮TouchBegan的时候b已经输入字符了,此时不应该有响应
                 */
                if (b == self.firstSingleTouchButton) {
                    return;
                }
                else
                {
                    /**
                     *  如果b不等于第一个按钮的时候
                     *  若b等于第二个按钮,说明第二个按钮在做小范围的移动,也不应该有反馈
                     */
                    if (b == self.secondSingleTouchButton) {
                        return;
                    }
                    else
                    {
                        /**
                         *  如果b及不等于第一个,也不是第二个
                         *  说明b在两个按钮同时按下的状态下,b在做滑动
                         *  此时secondButton应该被切换,上一个secondButton应该去除动画
                         */
                        CGPoint prevLocation = [touch previousLocationInView:self];
                        CKMainButton * prevButton = nil;
                        for (int j = 0; j < buttonCount; j ++) {
                            CKMainButton * button = self.allButtonsArray[j];
                            CGRect currentRect = isLand ? CGRectFromString(button.touchArea_l) :CGRectFromString(button.touchArea);
                            if(CGRectContainsPoint(currentRect, prevLocation)) {
                                prevButton = button;
                                break;
                            }
                        }
                        if (prevButton == self.firstSingleTouchButton)  {
                            if (([b.style isEqualToString:@"letter"] || [b.style isEqualToString:@"symbol"]) ) {
                                [self removePopUpWithPopUpstyle:_removeStyle button:self.firstSingleTouchButton  popupView:self.firstSingleTouchButton.inputLabel];
                                [self addPopupToButton:b WithPopStyle:_popSytle];
                            }
                            if (([b.style isEqualToString:@"letter"] || [b.style isEqualToString:@"symbol"] || [b.style isEqualToString:@"space"])){
                                self.firstSingleTouchButton.highlighted = NO;
                                self.firstSingleTouchButton = b;
                                self.firstButtonHasInput = YES;
                            }
                        }
                        else {
                            if (([b.style isEqualToString:@"letter"] || [b.style isEqualToString:@"symbol"]) ) {
                                [self removePopUpWithPopUpstyle:_removeStyle button:self.secondSingleTouchButton  popupView:self.secondSingleTouchButton.inputLabel];
                                [self addPopupToButton:b WithPopStyle:_popSytle];
                            }
                            if (([b.style isEqualToString:@"letter"] || [b.style isEqualToString:@"symbol"] || [b.style isEqualToString:@"space"])){
                                self.secondSingleTouchButton.highlighted = NO;
                                self.secondSingleTouchButton = b;
                            }
                        }
                    }
                }
            }
            [self removeUnExpectedInputLabel];
            break;
        }
    }
}

-(void)touchesMoved: (NSSet *)touches withEvent: (UIEvent *)event
{
    if (self.popupListCoverView) {
        CGPoint location = [[touches anyObject] locationInView:self.popupList];
        [self.popupList highlightItem:location];
        return;
    }
    if (self.secondButtonEndButFirstButtonExit) { // 如果secondSingleButton已经输出了,但是第一个按钮仍然存在,应该直接返回
        return;
    }
    for (UITouch* touch in touches) {
        [self touchMoved:touch];
    }
}

-(void)touchEnded:(UITouch *)touch
{
    CKMainButton * finalButton;
    CGPoint location = [touch locationInView:self];
    BOOL isLand = [CKCommonTools screenDirectionIsLand];
    NSInteger buttonCount = self.allButtonsArray.count;
    for (int i = 0; i < buttonCount; i ++) {
        CKMainButton * b = self.allButtonsArray[i];
        CGRect currentRect = isLand ? CGRectFromString(b.touchArea_l) :CGRectFromString(b.touchArea);
        if(CGRectContainsPoint(currentRect, location)){
            finalButton = b;
            break;
        }
    }
    
    if (!finalButton) {
        /**
         *  如果把数组中所有的按钮都遍历一遍还没找到location对应的按钮,说明Touch
         *  的结束点不在所有按钮的内部,比如第一排按钮往上滑,或者最后一排按钮往下滑的操作
         *  这个时候要处理fistTouch和SecondTouch
         */
        if (self.secondSingleTouchButton) {
            CGPoint prevLocation = [touch previousLocationInView:self];
            CKMainButton * prevButton = nil;
            for (int j = 0; j < buttonCount; j ++) {
                CKMainButton * button = self.allButtonsArray[j];
                CGRect currentRect = isLand ? CGRectFromString(button.touchArea_l) :CGRectFromString(button.touchArea);
                if(CGRectContainsPoint(currentRect, prevLocation)) {
                    prevButton = button;
                    break;
                }
            }
            if (prevButton == self.secondSingleTouchButton) {
                [self removeBtnInputViewIfItIsLetterButton:self.secondSingleTouchButton];
                [self inputIfItIsLetterButton:_secondSingleTouchButton location:_secondSingleTouchButton.center needPredict:YES];
                self.secondButtonEndButFirstButtonExit = YES;
                self.secondSingleTouchButton = nil;
            }
            else if (_firstSingleTouchButton) { //todo: maybe need to check if prevButton is _firstSingleTouchButton
                [self removeBtnInputViewIfItIsLetterButton:self.firstSingleTouchButton];
                [self inputIfItIsLetterButton:_firstSingleTouchButton location:_firstSingleTouchButton.center needPredict:YES];
                [self killAllStatusVaribale];
                [self resetAllFuncButtonStatus];
            }
        }
        else if (_firstSingleTouchButton)
        {
            [self removeBtnInputViewIfItIsLetterButton:self.firstSingleTouchButton];
            [self inputIfItIsLetterButton:_firstSingleTouchButton location:_firstSingleTouchButton.center needPredict:YES];
            [self killAllStatusVaribale];
            [self resetAllFuncButtonStatus];
        }
        [self removeUnExpectedInputLabel];
    }
    else{ // 如果找到按钮的话正常处理
        if (self.secondSingleTouchButton) {
            //  如果第二个按钮存在,此时的secondSingleTouch
            if (finalButton == self.secondSingleTouchButton) {
                // 如果第二个按钮存在且等于SeconSingTouchButton,第二个按钮应当输入,
                // 第一个按钮不动,因为第一个按钮还没有抬起
                if ([finalButton.style isEqualToString:@"letter"] || [finalButton.style isEqualToString:@"symbol"]){
                    
                    [self removePopUpWithPopUpstyle:_removeStyle button:finalButton popupView:finalButton.inputLabel];
                    finalButton.outPutToTextFiledStr = finalButton.currentTitle;
                    [self inputIfItIsLetterButton:finalButton location:location needPredict:YES];
                }
                else if ([finalButton.style isEqualToString:@"space"]){
                    [self spacePressed:finalButton];
                }
                else if ([finalButton.style isEqualToString:@"delete"]){
                    [self deletePressed:finalButton];
                }
                else if ([finalButton.style isEqualToString:@"return"]){
                    [self returnPressed:finalButton];
                }
                finalButton.highlighted = NO;
                //  第二个按钮已经抬起但是第一个按钮仍然存在
                self.secondButtonEndButFirstButtonExit = YES;
                self.secondSingleTouchButton = nil;
                [self removeUnExpectedInputLabel];
                return;
            }
            else if (finalButton == self.firstSingleTouchButton) {
                if (self.firstButtonHasInput) {
                    /**
                     *  检查一下第一个按钮抬起的时候是否第一第二个按钮的inPutLabel还在
                     *  如果有的话,把它们重新删除一次
                     */
                    if ([finalButton.style isEqualToString:@"shift"]) {
                        if (self.shiftStatus == CKShiftStatusLongPressedLower) {
                            self.shiftStatus = CKShiftStatusAllLower;
                            [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusAllLower];
                        }
                        else if (self.shiftStatus == CKShiftStatusLongPressedUpper) {
                            self.shiftStatus = CKShiftStatusCapitalized;
                            [self tellPredictbarByViewControllerWithShiftStatus:CKShiftStatusCapitalized];
                        }
                        if (self.userKeepPressedShitNow) {
                            if (self.shiftStatus == CKShiftStatusAllLower) {
                                [self realSetShiftStatus:CKShiftStatusCapitalized];
                            }
                            else if (self.shiftStatus == CKShiftStatusCapitalized) {
                                [self realSetShiftStatus:CKShiftStatusAllLower];
                            }
                        }
                    }
                    if (self.firstSingleTouchButton.inputLabel)
                        [self removeBtnInputViewIfItIsLetterButton:self.firstSingleTouchButton];
                }
                else {
                    if ([finalButton.style isEqualToString:@"letter"]|| [finalButton.style isEqualToString:@"symbol"]){
                        [self removePopUpWithPopUpstyle:_removeStyle button:finalButton popupView:finalButton.inputLabel];
                        finalButton.outPutToTextFiledStr = finalButton.currentTitle;
                        [self inputIfItIsLetterButton:finalButton location:location needPredict:YES];
                        [self removeBtnInputViewIfItIsLetterButton:_firstSingleTouchButton];
                        [self resetAllFuncButtonStatus];
                    }
                    else {
                        if([finalButton.style isEqualToString:@"emoji"]){
                            if (self.funcButtonHasClicked) { // 如果进入了Touchbegan才触发方法
                                [self emojiPressed:finalButton];
                            }
                        }
                        else if([finalButton.style isEqualToString:@"global"]){
                            if (self.funcButtonHasClicked) { // 如果进入了Touchbegan才触发方法
                                [self globalPressedNow:finalButton];
                            }
                        }
                        else if([finalButton.style isEqualToString:@"symbol"]){
                            [self symbolPressed:finalButton];
                            [self removePopUpWithPopUpstyle:_removeStyle button:finalButton popupView:finalButton.inputLabel];
                        }
                        else if([finalButton.style isEqualToString:@"space"]){
                            [self spacePressed:finalButton];
                        }
                        else if([finalButton.style isEqualToString:@"return"]){
                            if (self.funcButtonHasClicked) { // 如果进入了Touchbegan才触发方法
                                [self returnPressed:finalButton];
                            }
                        }
                        self.funcButtonHasClicked = NO;
                    }
                }
                finalButton.highlighted = NO;
                self.firstSingleTouchButton = self.secondSingleTouchButton;
                self.firstButtonHasInput = NO;
                self.secondSingleTouchButton = nil;
                [self removeUnExpectedInputLabel];
            }
            else
            {
                /**
                 *  检查一下第一个按钮抬起的时候是否第一第二个按钮的inPutLabel还在
                 *  如果有的话,把它们重新删除一次
                 */
                if (self.firstSingleTouchButton.inputLabel)
                    [self removeBtnInputViewIfItIsLetterButton:self.firstSingleTouchButton];
                if (self.secondSingleTouchButton.inputLabel)
                    [self removeBtnInputViewIfItIsLetterButton:self.secondSingleTouchButton];
                [self killAllStatusVaribale];
                return;
            }
        }
        else
            
        {
            /**
             *  如果只有一个按钮,这时一定是firstButton
             */
            if (self.firstSingleTouchButton != nil) {
                if (self.firstButtonHasInput) {
                    [self removeUnExpectedInputLabel];
                    self.firstSingleTouchButton = nil;
                    return;
                }
                else {
                    finalButton = self.firstSingleTouchButton;
                }
            }
            if ([finalButton.style isEqualToString:@"letter"]|| [finalButton.style isEqualToString:@"symbol"]){
                [self removePopUpWithPopUpstyle:_removeStyle button:finalButton popupView:finalButton.inputLabel];
                finalButton.outPutToTextFiledStr = finalButton.currentTitle;
                [self inputIfItIsLetterButton:finalButton location:location needPredict:YES];
                [self removeBtnInputViewIfItIsLetterButton:_firstSingleTouchButton];
                [self resetAllFuncButtonStatus];
            }
            else {
                if([finalButton.style isEqualToString:@"emoji"]){
                    if (self.funcButtonHasClicked) { // 如果进入了Touchbegan才触发方法
                        [self emojiPressed:finalButton];
                    }
                }
                else if([finalButton.style isEqualToString:@"global"]){
                    if (self.funcButtonHasClicked) { // 如果进入了Touchbegan才触发方法
                        [self globalPressedNow:finalButton];
                    }
                }
                else if([finalButton.style isEqualToString:@"symbol"]){
                    [self symbolPressed:finalButton];
                    [self removePopUpWithPopUpstyle:_removeStyle button:finalButton popupView:finalButton.inputLabel];
                }
                else if([finalButton.style isEqualToString:@"space"]){
                    [self spacePressed:finalButton];
                }
                else if([finalButton.style isEqualToString:@"return"]){
                    if (self.funcButtonHasClicked) { // 如果进入了Touchbegan才触发方法
                        [self returnPressed:finalButton];
                    }
                }
                self.funcButtonHasClicked = NO;
                finalButton.highlighted = NO;
            }
            [self removeUnExpectedInputLabel];
            self.firstSingleTouchButton = nil;
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent: (UIEvent *)event
{
    if (self.popupListCoverView) {
        CGPoint location = [[touches anyObject] locationInView:self.popupList];
        NSInteger selectIdx = [self.popupList selectItem:location];
        [self selectFunctionForEmojiButton:selectIdx];
        return;
    }
    for (UITouch* touch in touches) {
        [self touchEnded:touch];
    }
}

-(void)touchCancelled:(UITouch *)touch
{
    /**
     *  目前知道的可以触发touchesCancel方法的事件有:
     1.长按事件被触发,原来的touch就会被Cancel
     2.在没有触发长按手势的时候,
     以P按钮为例,往上缓慢滑动,滑出键盘区域,会触发Cancel,但是由于此时获得的nowBtn和preBtn(见下方变量)
     都为null,所以没有办法定位是哪个按钮,所以只能遍历一遍所有按钮,把有inputLabel的全部移除掉
     */
    
    CGPoint location = [touch locationInView:self];
    CGPoint preLocation = [touch previousLocationInView:self];
    CKMainButton * nowBtn, * preBtn;
    BOOL isLand = [CKCommonTools screenDirectionIsLand];
    for (int i = 0; i < self.allButtonsArray.count; i ++) {
        CKMainButton * b = self.allButtonsArray[i];
        CGRect currentRect = isLand ? CGRectFromString(b.touchArea_l) :CGRectFromString(b.touchArea);
        if(CGRectContainsPoint(currentRect, location)){
            nowBtn = b;
        }
        if(CGRectContainsPoint(currentRect, preLocation)){
            preBtn = b;
        }
    }
    
    if (nowBtn == nil && preBtn == nil) {
        [self removeUnExpectedInputLabel];
        return;
    }
    
    if (nowBtn == self.currentLongPressButton && self.longPressOn) { // 长按会出发cancel方法
        [nowBtn.inputLabel removeFromSuperview];
        nowBtn.inputLabel = nil;
        return;
    }
    // 获得到上一个按钮和当前按钮
    if (self.secondSingleTouchButton) {
        [_secondSingleTouchButton.inputLabel removeFromSuperview];
        [_firstSingleTouchButton.inputLabel removeFromSuperview];
        _secondSingleTouchButton = nil;
        _firstSingleTouchButton = nil;
        [preBtn.inputLabel removeFromSuperview];
        [nowBtn.inputLabel removeFromSuperview];
        _secondButtonEndButFirstButtonExit = NO;
    }
    else{
        [_firstSingleTouchButton.inputLabel removeFromSuperview];
        [preBtn.inputLabel removeFromSuperview];
        [nowBtn.inputLabel removeFromSuperview];
        self.firstSingleTouchButton = nil;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch* touch in touches) {
        [self touchCancelled:touch];
    }
}


- (void)backToCharacter
{
    if (self.keyboardType == CKKeyboardTypeNumberONE || self.keyboardType == CKKeyboardTypeNumber2) {
        [self changeKeyboardWithCurrentKeyboardType:CKKeyboardTypeCharacter];
        [self killAllStatusVaribale];
    }
    self.returnType = _returnType;
}

- (BOOL)resetFuncButtonStateToNormal:(CKMainButton *)button
{
    BOOL isFuncBtn = NO;
    if(![button.style isEqualToString:@"letter"]){
        button.highlighted = NO;
        isFuncBtn = YES;
    }
    self.returnType = _returnType;
    return isFuncBtn;
}

- (void)removeBtnInputViewIfItIsLetterButton:(CKMainButton *)button
{   if(button)
    if([button.style isEqualToString:@"letter"] || [button.style isEqualToString:@"symbol"] )
    [self removePopUpWithPopUpstyle:_removeStyle button:button popupView:button.inputLabel];
}

- (void)inputIfItIsLetterButton:(CKMainButton *)button location:(CGPoint)location needPredict:(BOOL)needPredict
{   if(button)
    if([button.style isEqualToString:@"letter"] || [button.style isEqualToString:@"symbol"])
        [self characterPressed:button location:location needPredict:needPredict];
}

- (void)removeUnExpectedInputLabel
{
    for (CKMainButton * button in self.allButtonsArray) {
        if (button.subviews.count > 2) {
            // button本身有两个subView,一个是imageView,一个是titleLable,所以当subview的数量大于2的时候,要移除
            if (button != _firstSingleTouchButton && button != _secondSingleTouchButton) {
                button.highlighted = NO;
                for (UIView * view in button.subviews) {
                    if (view.tag == 5201314) {
                        [view removeFromSuperview];
                    }
                }
            }
        }
        if ([button.style isEqualToString:@"letter"] || [button.style isEqualToString:@"symbol"]) {
            if (button != _firstSingleTouchButton && button != _secondSingleTouchButton && button.highlighted) {
                button.highlighted = NO;
            }
        }
    }
}


- (void)addPopupToButton:(CKMainButton *)button WithPopStyle:(CKPopStyle)popStyle
{
    self.popupAdded = YES;
    if (popStyle == CKPopStyleClassical) {
        CKPopUpImagePositon imagePosition = CKPopUpImageInner;
        if (button.isleftFirst) {
            imagePosition = CKPopUpImageLeft;
        }
        else if(button.isRightLast){
            imagePosition = CKPopUpImageRight;
        }
        else{
            imagePosition = CKPopUpImageInner;
        }
    
        UIImageView * keyPop = [CKMainKeyboardExpandedView popupClassicalWithKind:imagePosition  button:button];
        CGFloat x = -(__PADDING_X / Memoji_SCALE + __PAN_UL_WIDTH / Memoji_SCALE);
        CGFloat y = button.bounds.size.height + __PADDING_Y / Memoji_SCALE - __HEIGHT * 1.0 / Memoji_SCALE;
 
        keyPop.frame = CGRectMake(x,y, keyPop.frame.size.width, keyPop.frame.size.height);
        [button addSubview:keyPop];
    }
    else if(popStyle == CKPopStyleOther){
        UIButton * popLabel = [[UIButton alloc] init];
        if (_nowUsemodel.popupstyle_index.intValue == 0) {
            popLabel.contentEdgeInsets = UIEdgeInsetsMake(0, 0, _popup_content_edge_bottom, 0);
            popLabel.frame = CGRectMake(0, 0, button.bounds.size.width + _popup_content_left_padding + _popup_content_right_padding, _popup_height);
        }
        else {
            popLabel.frame = CGRectMake(0, 0, button.bounds.size.width * popUpWRatioToButtonW, button.bounds.size.height * popUpHRatioToButtonH);
        }
        CGFloat originY = popLabel.center.y;
        CGAffineTransform originTranform = popLabel.transform;
        popLabel.center = CGPointMake(button.frame.size.width / 2, 0);
        popLabel.transform = CGAffineTransformScale(originTranform, .8, .8);
        [button addSubview:popLabel];
        if (_nowUsemodel.popupstyle_index.intValue == 0) {
            [UIView animateWithDuration:0 animations:^{
                popLabel.transform = originTranform;
                if (button.rowTag == 1 || button.frame.origin.y < button.frame.size.height) {
                    popLabel.center = CGPointMake(popLabel.center.x, originY - _popup_height + button.frame.size.height - _firstline_popup_bottom_to_button_bottom);
                    _popUpLineOneBottomY = CGRectGetMaxY(popLabel.frame);
                }
                else {
                    popLabel.center = CGPointMake(popLabel.center.x,originY - _popup_height - _otherline_popup_bottom_to_button_top);
                    _popUpLineOtherBottomY = CGRectGetMaxY(popLabel.frame);
                }
            }];
        }
        else {
            [UIView animateWithDuration:.1 animations:^{
                popLabel.transform = originTranform;
                popLabel.center = CGPointMake(popLabel.center.x, originY - button.frame.size.height - POP_TO_BTN);
            }];
        }
        [popLabel setBackgroundImage:self.singlePopUpImage forState:UIControlStateNormal];
        popLabel.tag = 5201314;
        [popLabel setTitle:button.currentTitle forState:UIControlStateNormal];
        popLabel.titleLabel.font = self.popUpFont;
        [popLabel setTitleColor:button.currentTitleColor forState:UIControlStateNormal] ;
        popLabel.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.inputLabel = popLabel;
        button.highlighted = YES;
    }
    [self bringSubviewToFront:button];
}

- (void)removePopUpWithPopUpstyle:(CKRemovePopStyle)removeStyle button:(CKMainButton *)button popupView:(UIView *)popupView
{
    button.highlighted = NO;
    self.popupAdded = NO;
    [[CKMainButtonPopUp alloc] button:button removePopupView:button.inputLabel withStyle:CKMainButtonRemovePopUpStyleDefault];
}


- (void)symbolPressed:(CKMainButton *)button
{
    if ([self.englishboardDelegate respondsToSelector:@selector(symbolButtonClicked:)]) {
        [self.englishboardDelegate symbolButtonClicked:button];
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    if (self.currentWidth != self.frame.size.width) {
        self.currentWidth = self.frame.size.width;

        BOOL isLand = [CKCommonTools screenDirectionIsLand];
        NSDictionary * realDimensions;
        if (isLand) {
            realDimensions = dimensions[PHONE_SCREEN_DIRECTION_LANDSCAPE];
        }
        else {
            realDimensions = dimensions[PHONE_SCREEN_DIRECTION_PORTRAIT];
        }
        _popup_height = [realDimensions[@"popup_height"] floatValue];
        _popup_content_left_padding = [realDimensions[@"popup_content_left_padding"] floatValue];
        _popup_content_right_padding = [realDimensions[@"popup_content_right_padding"] floatValue];
        _popup_content_edge_bottom = [realDimensions[@"popup_content_edge_bottom"] floatValue];
        _firstline_popup_bottom_to_button_bottom = [realDimensions[@"firstline_popup_bottom_to_button_bottom"] floatValue];
        _otherline_popup_bottom_to_button_top = [realDimensions[@"otherline_popup_bottom_to_button_top"] floatValue];
        CGFloat ios_popupFontSize = [realDimensions[@"popupFontSize"] floatValue];
        CGFloat realPopupFontSize;
        if (_nowUsemodel.popupstyle_index.intValue == 0) {
            realPopupFontSize = ios_popupFontSize;
        }
        else {
            realPopupFontSize = popupFontSize;
        }
        
        self.popFontSize = realPopupFontSize;

        UIFont *font = [UIFont fontWithName:buttonDefaultFontName size:realPopupFontSize];
        self.popupFontName = buttonDefaultFontName;
        if ([self.fontName isEqualToString:@"LithosPro-Black"] || [self.fontName isEqualToString:@"Chalkboard SE"]) {
            font = [UIFont fontWithName:self.fontName size:realPopupFontSize];
            self.popupFontName = self.fontName;
        }
        self.popUpFont = font;
        
        [self dismissPopupList];
        self.coverView.frame = self.bounds;
        [self layoutButtons:isLand];
    }
    [self bringSubviewToFront:self.coverView];
    if (self.popupListCoverView) {
        [self bringSubviewToFront:self.popupListCoverView];
        self.popupListCoverView.frame = self.bounds;
        [self bringSubviewToFront:self.currentLongPressButton];
    }
}
- (void)resizeiPadAndOtherDevices
{
    if (![[UIDevice currentDevice].model isEqualToString: @"iPad"]){return;} // 如果不是iPad直接返回
    float buttonX = 0;
    float buttonY = 0;
    float buttonW = 0;
    float buttonH = 0;
    
    float touchX = 0;
    float touchY = 0;
    float touchW = 0;
    float touchH = 0;
    
    float ratioW = 0;
    float ratioH = 0;
    
    for (CKMainButton * button in self.allButtonsArray) {
        if (![CKCommonTools screenDirectionIsLand]) { // 竖屏
            ratioW = [UIScreen mainScreen].bounds.size.width / 414.0;
            ratioH = 250.0 / KEYBOARD_PORTRAIT_HEIGHT;
            CGRect buttonRect = CGRectFromString(button.buttonArea);
            buttonX = buttonRect.origin.x * ratioW;
            buttonY = buttonRect.origin.y * ratioH;
            buttonW = buttonRect.size.width * ratioW;
            buttonH = buttonRect.size.height * ratioH;
            button.buttonArea = NSStringFromCGRect(CGRectMake(buttonX, buttonY, buttonW, buttonH));
            
            CGRect touchRect = CGRectFromString(button.touchArea);
            touchX = touchRect.origin.x * ratioW;
            touchY = touchRect.origin.y * ratioH;
            touchW = touchRect.size.width * ratioW;
            touchH = touchRect.size.height * ratioH;
            
            button.touchArea = NSStringFromCGRect(CGRectMake(touchX, touchY, touchW, touchH));
        }
        else
        {
            ratioW = [UIScreen mainScreen].bounds.size.width / 736.0;
            ratioH = 243 / 243;
            CGRect buttonRect = CGRectFromString(button.buttonArea_l);
            buttonX = buttonRect.origin.x * ratioW;
            buttonY = buttonRect.origin.y * ratioH;
            buttonW = buttonRect.size.width * ratioW;
            buttonH = buttonRect.size.height * ratioH;
            
            button.buttonArea_l = NSStringFromCGRect( CGRectMake(buttonX, buttonY, buttonW, buttonH));
            
            CGRect touchRect = CGRectFromString(button.touchArea_l);
            touchX = touchRect.origin.x * ratioW;
            touchY = touchRect.origin.y * ratioH;
            touchW = touchRect.size.width * ratioW;
            touchH = touchRect.size.height * ratioH;
            button.touchArea_l = NSStringFromCGRect(CGRectMake(touchX, touchY, touchW, touchH));
        }
    }
}
- (void)changeKeyboardWithCurrentKeyboardType:(CKKeyboardType)currentKeyboardType
{
    self.keyboardType = currentKeyboardType;
    UIImage * backGroundImageNormal, * backGroundImageHighlight, * spaceBackgroundImageNormal,* spaceBackgroundImageHighted,* deleteImage,* skinImage,* globalImage,* buttonImage,* buttonPressedImage;
    if (_nowUsemodel) {
        if (_nowUsemodel.isOnline) {
            backGroundImageNormal = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_special" resize:YES];
            backGroundImageHighlight = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_special_pressed" resize:YES];
            spaceBackgroundImageNormal = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_space" resize:YES];
            spaceBackgroundImageHighted = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_space_pressed" resize:YES];
            deleteImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_delete" resize:NO];
            skinImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_emoji_global" resize:NO];
            globalImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_global" resize:NO];
            buttonImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_button" resize:YES];
            buttonPressedImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_button_pressed" resize:YES];
            _emojiImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_emojis" resize:NO];
        }
        else
        {
            backGroundImageNormal = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_special" resize:YES];
            backGroundImageHighlight = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_special_pressed" resize:YES];
            spaceBackgroundImageNormal = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_space" resize:YES];
            spaceBackgroundImageHighted = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_space_pressed" resize:YES];
            deleteImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_delete" resize:NO];
            skinImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_emoji_global" resize:NO];
            globalImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_global" resize:NO];
            buttonImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_button" resize:YES];
            buttonPressedImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_button_pressed" resize:YES];
            _emojiImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_emojis" resize:NO];

        }
    }
    if (!_nowUsemodel || backGroundImageNormal == nil)
    {
        backGroundImageNormal =[UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special",Default_Theme_Name]]] ;
        backGroundImageHighlight =[UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special_pressed",Default_Theme_Name]]] ;
        spaceBackgroundImageNormal =[UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_space",Default_Theme_Name]]];
        spaceBackgroundImageHighted = [UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_space_pressed",Default_Theme_Name]]];
        deleteImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_delete",Default_Theme_Name]];
        skinImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_emoji_global",Default_Theme_Name]];
        globalImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_global",Default_Theme_Name]];
        buttonImage = [UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_button",Default_Theme_Name]]];
        buttonPressedImage = [UIImage resizableImageWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_button_pressed",Default_Theme_Name]]];
        _emojiImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_emojis",Default_Theme_Name]];
    }

    if (!skinImage) {
        skinImage = _emojiImage;
    }
    
    _skinImage = skinImage;
    _globalImage = globalImage;
    NSMutableArray * currentPageInfoModelArray = [NSMutableArray array];
    switch (currentKeyboardType) {
        case CKKeyboardTypeCharacter:
            [currentPageInfoModelArray addObjectsFromArray:self.pageInfo.character];
            break;
        case CKKeyboardTypeNumberONE:
            [currentPageInfoModelArray addObjectsFromArray:self.pageInfo.numberONE];
            break;
        case CKKeyboardTypeNumber2:
            [currentPageInfoModelArray addObjectsFromArray:self.pageInfo.numberTWO];
            break;
        default:
            break;
    }
    
    if (self.allButtonsArray.count) {
        [self.allButtonsArray removeAllObjects];
    }
    if (self.subviews.count) {
        for (UIView * sub in self.subviews) {
            if ([sub isKindOfClass:[CKMainButton class]]) {
                [sub removeFromSuperview];
            }
        }
    }
    
    BOOL isLand = [CKCommonTools screenDirectionIsLand];
    
    NSDictionary * realDimensions;
    if (isLand) {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_LANDSCAPE];
    }
    else {
        realDimensions = dimensions[PHONE_SCREEN_DIRECTION_PORTRAIT];
    }

    UIEdgeInsets altContentEdgeInsets;
    UIEdgeInsets spaceReturnContentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    if (currentiPhoneModel != CKCurrentiPhoneModeliPhone5 || isLand) {
        altContentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        spaceReturnContentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    else {
        if (isIOS9) {
            CGFloat alt_content_edge_bottom = [realDimensions[@"alt_content_edge_bottom_ios9"] floatValue];
            altContentEdgeInsets = UIEdgeInsetsMake(0, 0, alt_content_edge_bottom, 0);
            CGFloat space_return_content_edge_bottom = [realDimensions[@"space_return_content_edge_bottom_ios9"] floatValue];
            spaceReturnContentEdgeInsets = UIEdgeInsetsMake(0, 0, space_return_content_edge_bottom, 0);
        }
        else {
            CGFloat alt_content_edge_left = [realDimensions[@"alt_content_edge_left_ios8"] floatValue];
            altContentEdgeInsets = UIEdgeInsetsMake(0, alt_content_edge_left, 0, 0);
        }
    }

    CGFloat ios_buttonFontSize = [realDimensions[@"buttonFontSize"] floatValue];
    UIFont *font;
    if ([self.fontName isEqualToString:@"LithosPro-Black"] || [self.fontName isEqualToString:@"Chalkboard SE"]) {
        font = [UIFont fontWithName:self.fontName size:buttonFontSize];
    }
    else {
        if (_nowUsemodel.popupstyle_index.intValue == 0) {
            font = [UIFont fontWithName:buttonDefaultFontName size:ios_buttonFontSize];
        }
        else {
            font = [UIFont fontWithName:buttonDefaultFontName size:buttonFontSize];
        }
    }

    CGFloat defaultFontSize = [[CKKeyboardUImanger shareUImanger] getFunctionFontSize:self.fontName];

    self.curPageInfoModelArray = currentPageInfoModelArray;
    int k = 0;
    for (CKButtonModel * model in currentPageInfoModelArray) {
        CKMainButton * button = [[CKMainButton alloc] init];
//        CKMainButton * button = self.allButtonsArray[k];
        [self addSubview:button];
        [self.allButtonsArray addObject:button];
        k ++;
        button.style = model.style;
        button.longPressedFuncIndex = model.longPressedFuncIndex;
        button.buttonArea = model.buttonArea;
        button.touchArea = model.touchArea;
        button.buttonArea_l = model.buttonArea_l;
        button.touchArea_l = model.touchArea_l;
        button.inputOptions = model.inputOptions;
        button.rowTag = model.row.intValue;
        button.columnTag = model.column.intValue;
        button.isleftFirst = model.isLeftFirst.intValue;
        button.isRightLast = model.isRightLast.intValue;
        
        if (button.superview != self) {
            [self addSubview:button];
        }
        
        if ([model.key isEqualToString:@"empty"] && button.currentTitle.length) {
            [button setTitle:nil forState:UIControlStateNormal];
        }
        [self bringSubviewToFront:button];
        if (model.key != nil && ![model.key isEqualToString:@"empty"] ) {
            if ([model.key isEqualToString:@"return"]) {
                [button setTitleColor:self.textColor forState:UIControlStateNormal];
            }
            else{
                [button setTitle:model.key forState:UIControlStateNormal];
                [button setTitleColor:self.textColor forState:UIControlStateNormal];
            }
        }
        if ([model.style isEqualToString:@"letter"]) {
            [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
            [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
            button.keyColor = self.textColor;
            button.buttonImage = buttonImage;
            button.buttonPressedImage = buttonPressedImage;
            button.rowTag = model.row.intValue;
            button.columnTag = model.column.intValue;
            
            [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
            [button setTitleColor:[UIColor clearColor] forState:UIControlStateHighlighted];
            
            
            button.titleLabel.font = font;//[UIFont fontWithName:self.fontName size:fontSize];
        }
        else if([model.style isEqualToString:@"shift"]){
            if (button.currentTitle) {
                [button setTitle:nil forState:UIControlStateNormal];
            }
            self.shiftButton = button;
            self.shiftStatus = CKShiftStatusCapitalized;
            [button setBackgroundImage:backGroundImageNormal forState:UIControlStateNormal];
            [button setBackgroundImage:backGroundImageHighlight forState:UIControlStateHighlighted];
        }
        else if([model.style isEqualToString:@"delete"]){
            self.deleteButton = button;
            [button setImage:deleteImage forState:UIControlStateNormal];
            [button setBackgroundImage:backGroundImageNormal forState:UIControlStateNormal];
            [button setBackgroundImage:backGroundImageHighlight forState:UIControlStateHighlighted];
        }
        else if([model.style isEqualToString:@"emoji"]){
            self.emojiButton = button;
            [button setImage:skinImage forState:UIControlStateNormal];
            button.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [button setBackgroundImage:backGroundImageNormal forState:UIControlStateNormal];
            [button setBackgroundImage:backGroundImageHighlight forState:UIControlStateHighlighted];
        }
        else if([model.style isEqualToString:@"symbol"]){
            
            button.keyColor = self.textColor;
            button.buttonImage = backGroundImageNormal;
            button.buttonPressedImage = buttonPressedImage;
            [button setBackgroundImage:backGroundImageNormal forState:UIControlStateNormal];
            [button setBackgroundImage:backGroundImageHighlight forState:UIControlStateHighlighted];
            UIFont *tmpFont;
            if (_nowUsemodel.popupstyle_index.intValue == 0) {
                tmpFont = [font fontWithSize:ios_buttonFontSize];
            }
            else {
                tmpFont = [font fontWithSize:buttonFontSize];
            }
            button.titleLabel.font = tmpFont;
        }
        else if([model.style isEqualToString:@"alt"]){
            self.altButton = button;
            [button setBackgroundImage:backGroundImageNormal forState:UIControlStateNormal];
            [button setBackgroundImage:backGroundImageHighlight forState:UIControlStateHighlighted];
            UIFont *tmpFont = [font fontWithSize:defaultFontSize];
            button.titleLabel.font = tmpFont;
            button.contentEdgeInsets = altContentEdgeInsets;
        }
        else if([model.style isEqualToString:@"space"]){
            self.spaceButton = button;
            [button setBackgroundImage:spaceBackgroundImageNormal forState:UIControlStateNormal];
            [button setBackgroundImage:spaceBackgroundImageHighted forState:UIControlStateHighlighted];
            UIFont *tmpFont = [font fontWithSize:defaultFontSize];
            button.titleLabel.font = tmpFont;
            button.contentEdgeInsets = spaceReturnContentEdgeInsets;
        }
        else if([model.style isEqualToString:@"return"]){
            self.returnButton = button;
            [button setBackgroundImage:backGroundImageNormal forState:UIControlStateNormal];
            [button setBackgroundImage:backGroundImageHighlight forState:UIControlStateHighlighted];
            UIFont *tmpFont = [font fontWithSize:defaultFontSize];
            button.titleLabel.font = tmpFont;
            button.contentEdgeInsets = spaceReturnContentEdgeInsets;
        }
        else if([model.style isEqualToString:@"num_shift"]){
            [button setTitle:model.key forState:UIControlStateNormal];
            [button setBackgroundImage:backGroundImageNormal forState:UIControlStateNormal];
            [button setBackgroundImage:backGroundImageHighlight forState:UIControlStateHighlighted];
            UIFont *tmpFont = [font fontWithSize:defaultFontSize];
            button.titleLabel.font = tmpFont;
            [button setTitleColor:self.textColor forState:UIControlStateNormal];
        }
    }
    
    [self resizeiPadAndOtherDevices];
    [self setNeedsLayout];
    [self layoutButtons:isLand];
    if ([self.englishboardDelegate respondsToSelector:@selector(changeShiftStatusByViewController)]) {
        [self.englishboardDelegate changeShiftStatusByViewController];
    }
}


- (void)setReturnType:(UIReturnKeyType)returnType
{
    _returnType = returnType;
    switch (returnType) {
        case UIReturnKeyDefault:
            [_returnButton setTitle:@"return" forState:UIControlStateNormal];
            [_returnButton setTitle:@"return" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeyGo:
            [_returnButton setTitle:@"Go" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Go" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeyGoogle:
            [_returnButton setTitle:@"Google" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Google" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeyJoin:
            [_returnButton setTitle:@"Join" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Join" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeyNext:
            [_returnButton setTitle:@"Next" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Next" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeyRoute:
            [_returnButton setTitle:@"Route" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Route" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeySearch:
            [_returnButton setTitle:@"Search" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Search" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeySend:
            [_returnButton setTitle:@"Send" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Send" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeyYahoo:
            [_returnButton setTitle:@"Yahoo" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Yahoo" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeyDone:
            [_returnButton setTitle:@"Done" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Done" forState:UIControlStateHighlighted];
            break;
        case UIReturnKeyEmergencyCall:
            [_returnButton setTitle:@"Call" forState:UIControlStateNormal];
            [_returnButton setTitle:@"Call" forState:UIControlStateHighlighted];
            break;
        default:
            [_returnButton setTitle:@"return" forState:UIControlStateNormal];
            [_returnButton setTitle:@"return" forState:UIControlStateHighlighted];
            break;
    }
}

// 从AppGroup获得文字颜色,文字大小,文字名称,popUp颜色信息
- (void)primaryInfo
{
    
    _nowUsemodel = [CKMainThemeModel sholdUseThisModel];
    if ([_nowUsemodel.themeName isEqualToString:Default_Theme_Name]) {
        // 添加动画的样式
        _popSytle = CKPopStyleOther;
        _removeStyle = CKRemovePopStyleDirectly;
    }
    else
    {
        // 添加动画的样式
        _popSytle = CKPopStyleOther;
        _removeStyle = CKRemovePopStyleAnimated;
    }

    if (_nowUsemodel) { // 如果能取到这个model,则直接使用它
        _fontName = _nowUsemodel.fontName;
        _popUpColor = [UIColor colorFromHexString:_nowUsemodel.PopColor];
        _textColor = [UIColor colorFromHexString:_nowUsemodel.FontColor];
        _fontSize = 24.0f;
        if (_nowUsemodel.isOnline) {
            _smallShiftImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_shift_small" resize:NO];
            
            _shiftImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_shift" resize:NO];
            
            _shiftCapImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_shift_cap" resize:NO];
            _singlePopUpImage = [UIImage imageWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_popup" resize:YES];
        }
        else
        {
            _smallShiftImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_shift_small" resize:NO];
            
            _shiftImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_shift" resize:NO];
            
            _shiftCapImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_shift_cap" resize:NO];
            _singlePopUpImage = [UIImage imageFromMainBundleWithThemeName:_nowUsemodel.themeName ImageNameExtension:@"_popup" resize:YES];
            
        }
    }
    if (!_nowUsemodel || !_smallShiftImage) {
        _fontName = Default_FontName;
        _textColor = [UIColor colorFromHexString:Default_Font_Color];
        _popUpColor = [UIColor colorFromHexString:@"#686868"];
        _fontSize = 20.0f;
        _smallShiftImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_shift_small",Default_Theme_Name]];
        _shiftImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_shift",Default_Theme_Name]];
        _shiftCapImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_shift_cap",Default_Theme_Name]];
        _singlePopUpImage = [UIImage imageFromMainBundleWithThemeName:Default_Theme_Name ImageNameExtension:@"_popup" resize:YES];
    }
    
    CKKeyboardPageInfo * pageInfo = [[CKKeyboardPageInfo alloc] init];
    self.pageInfo = pageInfo;
    [self changeKeyboardWithCurrentKeyboardType:CKKeyboardTypeCharacter];
    self.coverView = [[CKCoverView alloc] init];
    self.coverView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.coverView];
    [self addGestureRecognizer:self.optionsViewRecognizer];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDimensions];
        self.frame = frame;
#pragma mark - 多点触控
        self.multipleTouchEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        // 从AppGroup获得文字颜色,文字大小,文字名称,popUp颜色信息
        [self primaryInfo];
    }
    
    NSUserDefaults * userInfo = [NSUserDefaults standardUserDefaults];

    self.userAllowPredict = [userInfo boolForKey:@"PredictSetting"];
    
    self.userAllowAutoCorrect = [userInfo boolForKey:@"AutoCorrectSetting"];
    
    if (self.userAllowPredict) {
        self.predictKing = [CKPredictionKing sharePrediction];
    }
    
    return self;
}


- (void)layoutButtons:(BOOL)isLand
{
    if (isLand) {
        _buttonGap = self.pageInfo.button_gap_l.floatValue;
        [self.predictKing receiveMaxRowNum:3 MaxLineNum:self.pageInfo.maxLineNum_l.intValue offsetArray:self.pageInfo.offsetArray_l];
    }
    else {
        _buttonGap = self.pageInfo.button_gap.floatValue;
        [self.predictKing receiveMaxRowNum:3 MaxLineNum:self.pageInfo.maxLineNum.intValue offsetArray:self.pageInfo.offsetArray];
    }
    for (CKMainButton * button in self.allButtonsArray) {
        CGRect buttonRect = CGRectFromString(isLand ? button.buttonArea_l : button.buttonArea);
        button.frame = buttonRect;
        button.buttonGap = _buttonGap;
        if ([button.style isEqualToString:@"letter"]) {
            if ([self.pageInfo.languageName isEqualToString:@"english"] && (self.keyboardType == CKKeyboardTypeCharacter)) {
                CGRect touchRect = CGRectFromString(isLand ? button.touchArea_l : button.touchArea);
                float centerX = touchRect.origin.x + touchRect.size.width / 2.0;
                float centerY = touchRect.origin.y + touchRect.size.height / 2.0;
                [self.predictKing generateKeyPositionWithButton:button centerX:centerX centerY:centerY ];
            }
        }
    }
}

- ( void)returnPressed:(id)sender {
    if ([self.englishboardDelegate respondsToSelector:@selector(returnPressedEnglish:)]) {
        [self.englishboardDelegate returnPressedEnglish:sender];
    }
}
- (void)tellPredictbarByViewControllerWithShiftStatus:(CKShiftStatus)shiftStatus
{
    if ([self.englishboardDelegate respondsToSelector:@selector(shiftChangeStatusTo:)]) {
        [self.englishboardDelegate shiftChangeStatusTo:shiftStatus];
    }
}
- ( void)spacePressed:(id)sender {
    if ([self.englishboardDelegate respondsToSelector:@selector(spacePressedEnglish:)]) {
        [self.englishboardDelegate spacePressedEnglish:sender];
    }
}
- ( void)globalPressedNow:(UIButton *)sender {
    if ([self.englishboardDelegate respondsToSelector:@selector(globalPressedNumber:)]) {
        if ([CKBackend getSwitchKeyboardType] == CKSwitchKeyboardTypeEmoji) {
            [CKSaveTools saveClickWithEventType:KB_SwitchKeyboard_EmojiKeyboard_Click key:KB_SwitchKeyboard_EmojiKeyboard_Click];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_SwitchKeyboard_EmojiKeyboard_Click];
        } else if ([CKBackend getSwitchKeyboardType] == CKSwitchKeyboardTypeMeme) {
            [CKSaveTools saveClickWithEventType:KB_SwitchKeyboard_MemeKeyboard_Click key:KB_SwitchKeyboard_MemeKeyboard_Click];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY action:KB_SwitchKeyboard_MemeKeyboard_Click];
        }
        [self.englishboardDelegate globalPressedNumber:sender];
    }
}

- ( void)emojiPressed:(UIButton *)sender
{
    if ([self.englishboardDelegate respondsToSelector:@selector(emojiPressedEnglish:)]) {
        [self.englishboardDelegate emojiPressedEnglish:sender];
    }
}

- ( void)deletePressed:(id)sender {
    if ([self.englishboardDelegate respondsToSelector:@selector(deletePressedEnglish:)]) {
        [self.englishboardDelegate deletePressedEnglish:sender];
    }
}


- ( void)characterPressed:(CKMainButton *)sender location:(CGPoint)location needPredict:(BOOL)needPredict
{
    NSString * str = sender.outPutToTextFiledStr;
    if (str == nil ) {
        str = sender.inputOptions[0];
    }
    if (self.keyboardType == CKKeyboardTypeCharacter && [sender.style isEqualToString:@"symbol"]) {
        [self.englishboardDelegate characterPressedNumber:sender Str:str];
        return;
    }
    if (self.keyboardType != CKKeyboardTypeCharacter && [self.englishboardDelegate respondsToSelector:@selector(characterPressedNumber:Str:)]) {
        [self.englishboardDelegate characterPressedNumber:sender Str:str];
        return;
    }
    if (self.userKeepPressedShitNow) {
        if (self.shiftStatus == CKShiftStatusAllLower || self.shiftStatus == CKShiftStatusLongPressedLower)
            str = str.lowercaseString;
        else if(self.shiftStatus == CKShiftStatusCapitalized || self.shiftStatus == CKShiftStatusLongPressedUpper)
            str = str.uppercaseString;
    }
    else
    {
        if (self.shiftStatus == CKShiftStatusAllLower)
            str = str.lowercaseString;
        else if(self.shiftStatus == CKShiftStatusAllUpper)
            str = str.uppercaseString;
        else if (self.shiftStatus == CKShiftStatusCapitalized) {
            self.shiftStatus = CKShiftStatusAllLower;
        }
    }

    if ([self.englishboardDelegate respondsToSelector:@selector(characterPressedEnglish:Str:)]) {
        [self.englishboardDelegate characterPressedEnglish:sender Str:str];
    }
}

@end
