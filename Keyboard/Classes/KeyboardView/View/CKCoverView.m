//
//  CKCoverView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/9.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKCoverView.h"

@interface CKCoverView()<UIGestureRecognizerDelegate>

@end

@implementation CKCoverView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    return self.superview;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

@end
