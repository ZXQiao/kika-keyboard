//
//  CKEnglishButton.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/29.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CKEnglishButtonStyle) {
    // 字母按钮
    CKEnglishButtonStyleCharacter = 0,
    // Emoji按钮
    CKEnglishButtonStyleEmoji
};

typedef NS_ENUM(NSUInteger, CKEnglishButtonPosition) {
    CKEnglishButtonPositionLeft = 0,
    CKEnglishButtonPositionRight
};

@class CKMainKeyboardExpandedView;
@interface CKMainButton : UIButton
/**按钮对应的长安数组 */
@property (nonatomic, strong) NSArray *inputOptions;

/**行号 */
@property (nonatomic, assign) int rowTag;

/**列号 */
@property (nonatomic, assign) int columnTag;

/**在键盘按钮数组中的序列号*/
@property (nonatomic,assign) int idxTag;

/** 有效点击区域的宽度*/
@property (nonatomic,assign) CGFloat tapWidth;

/** 有效点击区域的高度*/
@property (nonatomic,assign) CGFloat tapHeight;

/**
 *  按钮类型
 */
@property (nonatomic,assign) CKEnglishButtonStyle buttonStyle;
/**
 *  按钮在键盘中心点的左边还是右边
 */
@property (nonatomic,assign) CKEnglishButtonPosition position;
/**
 *  长按popUpView
 */
@property (nonatomic, weak) CKMainKeyboardExpandedView * expandedView;
/**
 *  单击PopUpLabel
 */
@property (nonatomic, weak) UIView * inputLabel;

/**
 *  最终要输入字符
 */
@property (nonatomic, strong) NSString  *outPutToTextFiledStr;

@property (nonatomic, assign) BOOL isAnimating; // 正在执行动画

@property (nonatomic, strong) UIImage * buttonImage;
@property (nonatomic, strong) UIImage * buttonPressedImage;
//@property (nonatomic, copy) NSString * buttonTitle;

@property (nonatomic, strong) UIColor * keyColor;

// 按钮frame
@property (nonatomic, copy)     NSString * buttonArea;
// 按钮横屏frame
@property (nonatomic, copy)     NSString * buttonArea_l;
// 按钮有效点击区域
@property (nonatomic, copy)     NSString * touchArea;
// 按钮横屏有效点击区域
@property (nonatomic, copy)     NSString * touchArea_l;
// 按钮长按触发的方法对应的索引,如果是0的话表示没有长按状态
@property (nonatomic, strong)   NSNumber * longPressedFuncIndex;
// 按钮的类型
@property (nonatomic, copy)     NSString * style;
@property (nonatomic, assign) BOOL isleftFirst; // 左边第一个
@property (nonatomic, assign) BOOL isRightLast; // 右边最后一个
@property (nonatomic, assign) CGFloat buttonGap; // button间距

//@property (nonatomic, strong) UIView * touchView;

@property (nonatomic, strong) NSString * mainFontName;
@end
