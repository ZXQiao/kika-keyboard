//
//  CKEmojiPredictKing.h
//  predictEmoji
//
//  Created by 张赛 on 15/4/19.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"


@interface CKEmojiPredictKing : NSObject
singleH(EmojiPredictKing);
- (NSArray *)queryEmojisWithSearchText:(NSString *)searchText;

@end
