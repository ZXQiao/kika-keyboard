//
//  CKEmojiPredictKing.m
//  predictEmoji
//
//  Created by 张赛 on 15/4/19.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import "CKEmojiPredictKing.h"
#import "FMDB.h"

@interface CKEmojiPredictKing()
@property (nonatomic, strong) FMDatabase *db;
@property (nonatomic, strong) FMDatabaseQueue *queue;
@end
@implementation CKEmojiPredictKing
singleM(EmojiPredictKing);

- (instancetype)init
{
    NSString *fileName = nil;
    if([CKCommonTools systemVersion] - 9.1 > -FLOAT_EPSINON) {
        fileName = [[NSBundle mainBundle] pathForResource:@"t_emoji_iOS9_1.sqlite" ofType:nil];
    } else {
        fileName = [[NSBundle mainBundle] pathForResource:@"t_emoji_iOS8_4.sqlite" ofType:nil];
    }
    
    self.db = [FMDatabase databaseWithPath:fileName];
    self.queue = [FMDatabaseQueue databaseQueueWithPath:fileName];
        return self;
}

- (NSArray *)queryEmojisWithSearchText:(NSString *)searchText
{
    NSMutableArray * predictEmojiArray = [NSMutableArray array];
    [self.queue inDatabase:^(FMDatabase *db) {
        // 1.查询
        FMResultSet *set = [db executeQuery:@"SELECT emoji FROM t_predictEmoji WHERE tag=?",searchText];
        // 2.取出数据
        while ([set next]) {
            NSString *emoji = [set stringForColumn:@"emoji"];
            if (emoji.length > 1) {
                NSArray * temp = [emoji componentsSeparatedByString:@","];
                [predictEmojiArray addObjectsFromArray:temp];
            }
            else{
                [predictEmojiArray addObject:emoji];
            }
        }
    }];
    return (NSArray *)predictEmojiArray;
    
}
@end
