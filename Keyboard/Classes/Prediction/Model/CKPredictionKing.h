//
//  CKPredictionKing.h
//  C++先编译通过
//
//  Created by 张赛 on 15/3/26.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import <UIKit/UIKit.h>


@class CKMainButton;
@protocol CKPredictionKingDelegate <NSObject>

- (void)predictKingFindSybomlsInWordShouldInput:(NSString *)inputWord shouldPredictWith:(NSString *)predictWorld sourceString:(NSString *)sourceString;

@end

@interface CKPredictionKing : NSObject
singleH(Prediction);

- (NSArray *)predictionArrayFromInputStr:(NSString *)inputStr withLastString:(NSString *)lastString;
// 依据上一个单词预测下一个单词
- (NSArray *)predictionNextWordArrayFromBeforeWord:(NSString *)beforeWord;
// 清空表单
- (void)storeUDB;
- (void)clearAssList;
//- (void)kbdListDeleteLastCharacter;

- (void)receiveMaxRowNum:(int)rowNum MaxLineNum:(int)lineNum offsetArray:(NSArray *)offsetArray;
- (void)generateKeyPositionWithButton:(CKMainButton *)button centerX:(float )centerX centerY:(float)centerY;
@property (nonatomic, weak) id<CKPredictionKingDelegate> delegate;
@end
