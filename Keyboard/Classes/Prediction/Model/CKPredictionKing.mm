//
//  CKPredictionKing.m
//  C++先编译通过
//
//  Created by 张赛 on 15/3/26.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import "CKPredictionKing.h"
#include <fstream>
#include "patricia_2.h"
#import "keyboard_associate.h"
#import "CKMainButton.h"


using namespace std;
@interface CKPredictionKing()
{
    PredictEngineManager engineMgr;// 定义一个引擎;
    vector<string> cand; // 定义一个容器K
    keyboard_associate<unsigned char, float> kbd;
}

@property (nonatomic,strong) NSString * userPath;

@end


@implementation CKPredictionKing
singleM(Prediction);

- (void)receiveMaxRowNum:(int)rowNum MaxLineNum:(int)lineNum offsetArray:(NSArray *)offsetArray
{
    if (rowNum == 3) {
        float offset[3] = {0.0,[offsetArray[1] floatValue],[offsetArray[2] floatValue]};
        kbd.initKeyMap(3, lineNum, offset);
    }
    if (rowNum == 4) {
        float offset[4] = {0.0,[offsetArray[1] floatValue],[offsetArray[2] floatValue],[offsetArray[3] floatValue]};
        kbd.initKeyMap(4, lineNum, offset);
    }
}
- (void)generateKeyPositionWithButton:(CKMainButton *)button centerX:(float)centerX centerY:(float)centerY
{
    NSString * inputStr = button.currentTitle;
    const char * charStr = [inputStr cStringUsingEncoding:NSISOLatin1StringEncoding];
    kbd.generateKeyPosition(* charStr, centerX, centerY, button.rowTag, button.columnTag);
}

- (instancetype)init
{   // 文件操作
    //NSString * userPath = [[NSBundle mainBundle] pathForResource:@"user_english_1.mp3" ofType:nil];
    NSString * sysPath = [[NSBundle mainBundle] pathForResource:@"sys_english_1.mp3" ofType:nil];
    NSString * cellsPath = [[NSBundle mainBundle] pathForResource:@"cells_english_1.mp3" ofType:nil];
    NSString *docDir = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Dictionary"];
    //    NSLog(@"%@",docDir);
    //NSString * userPathDestination = [docDir stringByAppendingPathComponent:@"user_english_1.mp3"];
    NSString * sysPathDestination = [docDir stringByAppendingPathComponent:@"sys_english_1.mp3"];
    NSString * userPathDestination = [sysPathDestination stringByReplacingOccurrencesOfString:@"sys_english_1.mp3" withString:@"user_english_1.mp3"];
    NSString * cellsPathDestination = [sysPathDestination stringByReplacingOccurrencesOfString:@"sys_english_1.mp3" withString:@"cells_english_1.mp3"];
    NSFileManager * myFileManger = [NSFileManager defaultManager];
    BOOL exist = NO;
    
    if (![myFileManger fileExistsAtPath:docDir]) {
        exist = [myFileManger createDirectoryAtPath:docDir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    else {
        exist = YES;
    }

    // 1.获取沙盒中的版本号
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // 在这里要判断版本号,如果版本号不同的话要删除字典重新copy
    NSString *sandBoxDictVersion = [defaults valueForKey:@"dict_version"];
    // 2.获取当前软件的版本号
    string version;
    engineMgr.getDictVersion(sysPath.UTF8String, version);
    NSString * currentDictVersion = [NSString stringWithCString:version.c_str() encoding:NSUTF8StringEncoding];

    if (exist) {
        NSError *err = nil;
        if ([currentDictVersion compare:sandBoxDictVersion] !=  NSOrderedSame) { // 只要版本号不一样就替换
            //[myFileManger removeItemAtPath:userPathDestination error:&err];
            [myFileManger removeItemAtPath:sysPathDestination  error:&err];
            [defaults setObject:currentDictVersion forKey:@"dict_version"]; // 存储当前版本号
            [defaults synchronize];
        }
        [myFileManger copyItemAtPath:sysPath toPath:sysPathDestination error:&err];
        [myFileManger removeItemAtPath:cellsPathDestination  error:&err];
        [myFileManger copyItemAtPath:cellsPath toPath:cellsPathDestination error:&err];

        self.userPath = userPathDestination;

//#ifdef CKRunPerformenceTest
//    TICK;
//#endif
        engineMgr.LoadLocale(userPathDestination.UTF8String, sysPathDestination.UTF8String, cellsPathDestination.UTF8String);
//#ifdef CKRunPerformenceTest
//    double duration = TOCK;
//    [CKPerformence GetExcuteTime:@"engineMgr.LoadLocale" duration:duration];
//#endif
    }
    return self;
}

- (void)storeUDB
{
    engineMgr.StoreUDB(self.userPath.UTF8String);
}

- (void)clearAssList
{
    kbd.resetAssList();
}


// 一次性导入

- (void)findSpecialSymbolStringInInputString:(NSString *)inputStr
{
    NSString * beforeString;
    NSString * afterString;
    for (int i = (int)inputStr.length - 1; i >= 0; i --) {
        NSString * single = [inputStr substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [SPECIAL_STR rangeOfString:single];
        if (range.location != NSNotFound) {
            beforeString = [inputStr substringToIndex:i + 1];
            afterString = [inputStr substringFromIndex:i + 1];
            
            if ([self.delegate respondsToSelector:@selector(predictKingFindSybomlsInWordShouldInput:shouldPredictWith:sourceString:)]) {
                [self.delegate predictKingFindSybomlsInWordShouldInput:beforeString shouldPredictWith:afterString sourceString:inputStr];
            }
            cand.clear();
            break;
        }
    }
}

- (NSArray *)atOneTimepredictionArrayFromInputStr:(NSString *)inputStr
{
    cand.clear(); // 容器清空
    kbd.resetAssList();
    for (int i = 0; i < inputStr.length; i ++) {
        NSString * single = [inputStr substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [SPECIAL_STR rangeOfString:single];
        if (range.location != NSNotFound) {
            kbd.resetAssList();
            [self findSpecialSymbolStringInInputString:inputStr];
            return nil;
        }
        else
        {
            const char * inputUnicode = [single.uppercaseString cStringUsingEncoding:NSISOLatin1StringEncoding];
            if (inputUnicode) {
                kbd.clickHandle( * inputUnicode);
            }
        }
    }
    
//#ifdef CKRunPerformenceTest
//    TICK;
//#endif
    BOOL hasPrefer = engineMgr.predict(kbd.getAssList(), cand);
//#ifdef CKRunPerformenceTest
//    double duration = TOCK;
//    [CKPerformence GetExcuteTime:@"engineMgrPredict" duration:duration];
//#endif
    unsigned long length = cand.size();
    NSMutableArray * predictionArray = [NSMutableArray array];
    for (int i = 0 ; i < length; i ++) {
        string tempStr = cand.at(i);
        NSString * finalStr = [NSString stringWithCString:tempStr.c_str() encoding:NSISOLatin1StringEncoding];
        [predictionArray addObject:finalStr];
    }
    [predictionArray insertObject:inputStr atIndex:0]; // 导入用户输入的原始单词
    if (hasPrefer) {
        [predictionArray insertObject:@"--prefer" atIndex:0];
    }
    return (NSArray *)predictionArray;
}

- (NSArray *)predictionArrayFromInputStr:(NSString *)inputStr withLastString:(NSString *)lastString
{
    return  [self atOneTimepredictionArrayFromInputStr:lastString];
}

- (NSArray *)predictionNextWordArrayFromBeforeWord:(NSString *)beforeWord
{
    
    NSMutableArray * predictionArray = [NSMutableArray array];
    const char * inputUnicode = [beforeWord cStringUsingEncoding:NSISOLatin1StringEncoding];
    if (!inputUnicode) {
        return predictionArray;
    }
    string tempStr;
    tempStr.append(inputUnicode);
//    BOOL tmp = NO;
    
    cand.clear(); // 容器清空
//#ifdef CKRunPerformenceTest
//    TICK;
//#endif
    engineMgr.select(tempStr, cand, 0, 0);
//#ifdef CKRunPerformenceTest
//    double duration = TOCK;
//    [CKPerformence GetExcuteTime:@"engineMgrSelect" duration:duration];
//#endif
    
    unsigned long length = cand.size();
    for (int i = 0 ; i < length; i ++) {
        string tempStr = cand.at(i);
        NSString * finalStr = [NSString stringWithCString:tempStr.c_str() encoding:NSISOLatin1StringEncoding];
        [predictionArray addObject:finalStr];
    }
    
    return (NSArray *)predictionArray;
}

@end
