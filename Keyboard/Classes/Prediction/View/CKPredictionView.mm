//
//  CKPredictionView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/26.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKPredictionView.h"
#import "CKPredictionKing.h"
#import "CKPredictButton.h"
#import "CKKeyboardUImanger.h"

@interface CKPredictionView()<UIGestureRecognizerDelegate>
{
    NSMutableArray * _fixedButtonArray;
    CGFloat suggestion_padding;
    CGFloat font_min_size;
    CGFloat font_max_size;
    CGFloat _currentWidth;
    CGFloat _maxAllX;
    BOOL _sameWord; // 传进来的数组是处理了相同的单词
    BOOL _isCharacter;
    BOOL _buttonPressedPhase;
}


@end

#define PAGE_WORD_COUNT 3
#define EMOJI_BUTTON_TAG_START 100

@implementation CKPredictionView

- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"prediction_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * realDimensions = [[CKKeyboardUImanger shareUImanger] getRealDimensionsByModelAndDirection:dimensions];
    suggestion_padding = [realDimensions[@"suggestion_padding"] floatValue];
    font_min_size = [realDimensions[@"font_min_size"] floatValue];
    font_max_size = [realDimensions[@"font_max_size"] floatValue];
}

- (NSArray *)emojiPredictArray
{
    if (_emojiPredictArray == nil) {
        _emojiPredictArray = [NSMutableArray array];
    }
    return _emojiPredictArray;
}

- (void)initFixedButtonArray
{
    if (_fixedButtonArray == nil) {
        _fixedButtonArray = [NSMutableArray array];
        UIImage * suggestion_middle_bg = [UIImage resizableImageWithImage:[UIImage imageNamed:@"ios_suggestion_middle_bg"]];
        UIImage * suggestion_pressed_bg = [UIImage resizableImageWithImage:[UIImage imageNamed:@"ios_suggestion_pressed_bg"]];
        UIImage * suggestion_side_bg = [UIImage imageNamed:@"ios_suggestion_side_bg"];
        UIFont * btnFont = [UIFont fontWithName:@"SF UI Text" size:font_max_size];
        for (int i = 0; i < PAGE_WORD_COUNT; i ++) { // 生成3个按钮
            CKPredictButton * predictionButton = [[CKPredictButton alloc] init];
            predictionButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            predictionButton.titleLabel.font = btnFont;
            predictionButton.titleLabel.adjustsFontSizeToFitWidth = YES;
            predictionButton.titleLabel.minimumScaleFactor = font_min_size / font_max_size;
            [predictionButton addTarget:self action:@selector(predictButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            predictionButton.tag = i;
            predictionButton.backgroundColor = [UIColor clearColor];
            if (i % PAGE_WORD_COUNT == 1) {
                [predictionButton setBackgroundImage:suggestion_middle_bg forState:UIControlStateNormal];
            }
            else {
                [predictionButton setBackgroundImage:suggestion_side_bg forState:UIControlStateNormal];
            }
            [predictionButton setBackgroundImage:suggestion_pressed_bg forState:UIControlStateHighlighted];
            [_fixedButtonArray addObject:predictionButton];
            [self addSubview:predictionButton];
        }
    }
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDimensions];
        [self initFixedButtonArray];
        _buttonArray = [NSMutableArray array];
        self.multipleTouchEnabled = NO;
        NSUserDefaults * userInfo = [NSUserDefaults standardUserDefaults];
        self.allowAutoCorrect = [userInfo boolForKey:@"AutoCorrectSetting"];
        _buttonPressedPhase = NO;
    }
    return self;
}

- (void)LayoutButtons
{
    CGFloat startX = 0;
    CGFloat viewW = self.frame.size.width;
    CGFloat predictButtonW = viewW / PAGE_WORD_COUNT;
    CGFloat predictButtonH = self.frame.size.height - 1;
    
    for (int i = 0; i < PAGE_WORD_COUNT; i++)
    {
        UIButton * button = _fixedButtonArray[i];
        button.frame = CGRectMake(startX, 0, predictButtonW, predictButtonH);
        [button.titleLabel textRectForBounds:CGRectMake(startX + suggestion_padding, 0, predictButtonW - suggestion_padding * 2, predictButtonH) limitedToNumberOfLines:1];
        startX += viewW / PAGE_WORD_COUNT;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (_currentWidth != self.frame.size.width) {
        _currentWidth = self.frame.size.width;
        [self LayoutButtons];
    }
}

- (void)setPredictionArray:(NSArray *)predictionArray originPredictResultArray:(NSArray *)originPredictResultArray hasPrefer:(BOOL)hasPrefer
{
    _buttonPressedPhase = NO;
    [_buttonArray removeAllObjects];
    NSUInteger wordCount = predictionArray.count;
    NSUInteger emojiCount = _emojiPredictArray.count;
    NSUInteger totalCount = wordCount + emojiCount;
    NSUInteger realCount = totalCount < PAGE_WORD_COUNT ? totalCount : PAGE_WORD_COUNT;

    BOOL isWord = YES;
    BOOL needHighlight = NO;
    NSUInteger word_array_idx = 0;
    NSUInteger emoji_array_idx = 0;
    
    for (int i = 0; i < PAGE_WORD_COUNT; i++) {
        CKPredictButton * predictionButton = _fixedButtonArray[i];
        if (i >= realCount) {
            [predictionButton setTitle:nil forState:UIControlStateNormal];
            predictionButton.enabled = NO;
            continue;
        }
        predictionButton.enabled = YES;
        [predictionButton setTitleColor:self.font_normal_color forState:UIControlStateNormal];
        needHighlight = NO;
        if (i == 1) {
            if (self.allowAutoCorrect && !_sameWord  && hasPrefer && _isCharacter) {
                needHighlight = YES;
            }
            else if (emojiCount > 0) {
                isWord = NO;
            }
        }
        if (isWord) {
            predictionButton.tag = i;
            NSString * sourceWord = predictionArray[word_array_idx]; // 原始的单词,包含了空格,大小写等信息
            NSString *  buttonWord = sourceWord;
            if (i == 0 && !_sameWord && _isCharacter) {// 如果是第一个按钮,并且没有去除单词,并且不是有之前的单词做出的预测,需要有引号
                buttonWord = [NSString stringWithFormat:@"\"%@\"",buttonWord];
            }
            if ([buttonWord hasSuffix:@" "]) {
                buttonWord = [buttonWord stringByReplacingCharactersInRange:NSMakeRange(buttonWord.length - 1, 1) withString:@""];
            }
            if (needHighlight) {
                [predictionButton setTitleColor:self.font_highlight_color forState:UIControlStateNormal];
            }
            [predictionButton setTitle:buttonWord forState:UIControlStateNormal];
            predictionButton.sourceWord = sourceWord; // 保存原来的单词
            predictionButton.originWord = originPredictResultArray[word_array_idx];

            if (word_array_idx < wordCount - 1) {
                word_array_idx++;
            }
            else {
                isWord = NO;
            }
        }
        else {
            predictionButton.tag = EMOJI_BUTTON_TAG_START + i;
            [predictionButton setTitle:_emojiPredictArray[emoji_array_idx] forState:UIControlStateNormal];
            emoji_array_idx++;
            if (word_array_idx < wordCount) {
                isWord = YES;
            }
        }

        [_buttonArray addObject:predictionButton];
    }

    [self setNeedsLayout];
}

#pragma mark - 通知代理
- (void)predictButtonClicked:(CKPredictButton *)button
{
    if (_buttonPressedPhase) {
        return;
    }
    BOOL firstButton = NO;
    if (button.tag == 0) {
        firstButton = YES;
    }
    if (button.tag < EMOJI_BUTTON_TAG_START) {
        if ([self.predictionDelegate respondsToSelector:@selector(predictionViewWordButtonClicked:sameWords:firstButton:)]) {
            [self.predictionDelegate predictionViewWordButtonClicked:button sameWords:_sameWord firstButton:firstButton];
            // 当有按钮点击时,说明此时已经输入了,所以需要将Same置为0
            _sameWord = NO;
        }
        [CKSaveTools saveClickWithEventType:KB_Predition_Clicked key:@"normal"];
    }
    else {
        if ([self.predictionDelegate respondsToSelector:@selector(predictionViewEmojiButtonClicked:)]) {
            [self.predictionDelegate predictionViewEmojiButtonClicked:button];
        }
    }
    _buttonPressedPhase = YES;
}

- (void)predictButtonClickedFromSpace:(CKPredictButton *)button
{
    if (_buttonPressedPhase) {
        return;
    }
    BOOL firstButton = NO;
    if (button.tag == 0) {
        firstButton = YES;
    }
    if ([self.predictionDelegate respondsToSelector:@selector(predictionViewWordButtonClickedFromSpace:sameWords:firstButton:)]) {
        [self.predictionDelegate predictionViewWordButtonClickedFromSpace:button sameWords:_sameWord firstButton:firstButton];
        // 当有按钮点击时,说明此时已经输入了,所以需要将Same置为0
        _sameWord = NO;
    }
    [CKSaveTools saveClickWithEventType:KB_Predition_Clicked key:@"space"];
    _buttonPressedPhase = YES;
}

- (void)predictionArrayHasSameWord:(BOOL)same predictWithCharacter:(BOOL)isCharacter
{
    _sameWord = same;
    _isCharacter = isCharacter;
}

-(void)ClearButtonArray
{
    [self.buttonArray removeAllObjects];
}

@end
