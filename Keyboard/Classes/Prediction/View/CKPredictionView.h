//
//  CKPredictionView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/26.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

// preview单词的大写状态
typedef NS_ENUM(int, CKPreviewCapsType) {
    CKPreviewCapsTypeAllLower = 0, // 全部小写
    CKPreviewCapsTypeCaps,  // 首字母大写
    CKPreviewCapsTypeAllUpper,  // 全部大写
    CKPreviewCapsTypeLongPressedLower,
    CKPreviewCapsTypeLongPressedUpper
};

@class CKPredictButton;

@protocol CKPredictionViewDelegate<NSObject>

/*
 sameWord来传递是否在数组中有与输入字符完全相同的字符
 firstButton来传递是否是第一个按钮,如果是第一个按钮的话,有可能是带引号的
 */
- (void)predictionViewWordButtonClicked:(CKPredictButton *)button sameWords:(BOOL)same firstButton:(BOOL)firsrt;
- (void)predictionViewWordButtonClickedFromSpace:(CKPredictButton *)button sameWords:(BOOL)same firstButton:(BOOL)firsrt;
- (void)predictionViewEmojiButtonClicked:(UIButton *)emojiButton;

@end


@interface CKPredictionView : UIImageView

@property (nonatomic,weak) id<CKPredictionViewDelegate> predictionDelegate;
@property (nonatomic,strong) NSMutableArray * emojiPredictArray;

@property (nonatomic,strong) UIColor * font_normal_color;
@property (nonatomic,strong) UIColor * font_highlight_color;
//@property (nonatomic,strong) NSString * fontName;

@property (nonatomic,strong) NSMutableArray * buttonArray;
@property (nonatomic,assign) BOOL allowAutoCorrect;

// 是否处理了相同的单词,预测的时候用的是单个字母查询还是整个单词
- (void)predictionArrayHasSameWord:(BOOL)same predictWithCharacter:(BOOL)isCharacter;
- (void)predictButtonClicked:(CKPredictButton *)button;
- (void)predictButtonClickedFromSpace:(CKPredictButton *)button;
-(void)ClearButtonArray;
- (void)setPredictionArray:(NSArray *)predictionArray originPredictResultArray:(NSArray *)originPredictResultArray hasPrefer:(BOOL)hasPrefer;

@end
