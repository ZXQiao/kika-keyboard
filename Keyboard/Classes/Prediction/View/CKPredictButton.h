//
//  CKPredictButton.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/4/1.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

// 创建这个button的原因是,去词库查询的单词是大小写敏感的,比如WHO是"世界卫生组织",who是"谁"
// 所以在按钮显示的时候,和传给词库的单词是不一样的,因此要多一个属性来保存原来的词语
@interface CKPredictButton : UIButton

@property (nonatomic,copy) NSString * sourceWord;
@property (nonatomic,copy) NSString * originWord;

// 定义一个浮点数来记录按钮当前的位置信息,
// 3.1 3.2 3.3 分别表示一行三个按钮的第1.2.3个按钮
// 2.1 2.2
// 1.1

@property (nonatomic,assign) CGFloat locationTag;
//- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents sourceStr:(NSString *)sourceStr;

@end
