#ifndef _ASSOCIATE_
#define _ASSOCIATE_

#include <vector>
#include <list>
#include <string>

using namespace std;

template <class TChar>
class TrackChar
{
public:
    TrackChar () : freq(0.0), charNum(0), charLeft(0), charRight(0) {};
    TrackChar (TChar ch, float f) : freq(f), charNum(ch) {};

    void set(TChar ch, float f) { charNum = ch, charLeft = ch, charRight = ch, freq = f; };
    void set(TChar ch, TChar left, TChar right, float f) { charNum = ch, charLeft = left, charRight = right, freq = f; };
    void setChar(TChar num) { charNum = num; };
    void setLeftChar(TChar num) { charLeft = num; };
    void setRightChar(TChar num) { charRight = num; };

    TChar getChar() { return charNum; };
    float getFreq() { return freq; };
    TChar getLeftChar() { return charLeft; };
    TChar getRightChar() { return charRight; };

private:
    float freq;
    TChar charNum;
    TChar charLeft;
    TChar charRight;
};

template <class T>
class CharNode
{
public:
    CharNode()
    {
        chr = 0;
        freq = 0.0;
    }
    CharNode(T inChr, float inFreq)
    {
        chr = inChr;
        freq = inFreq;
    }

    inline bool operator< (const CharNode<T>& node) const
    {
        return freq < node.freq;
    }

    T getChar() { return chr; };
    float getFreq() { return freq; };

private:
    T chr;
    float freq;
};

template <class T>
class AssociateNode
{
public:
    T keyChar;
    list<CharNode<T> > charList;

    AssociateNode()
    {
        keyChar = 0;
        charList.clear();
    }
    AssociateNode(T inKeyChar)
    {
        keyChar = inKeyChar;
        charList.clear();
    }

    void addCharNode(T inChr, float inFreq)
    {
        CharNode<T> charNode(inChr, inFreq);
        charList.push_back(charNode);
//         charList.sort();
//         charList.reverse();
    }

    void deleteCharNode(T inChr)
    {
        typename list<CharNode<T> >::iterator itor;
        for (itor = charList.begin(); itor != charList.end(); itor++)
        {
            if (itor->getChar() == inChr)
            {
                charList.erase(itor);
                return;
            }
        }
    }
};

template <class T>
class Associate
{
private:
    vector<AssociateNode<T> > associateList;

    bool containKey(T keyChar)
    {
        for (int i = 0; i < associateList.size(); i++)
        {
            if (associateList.at(i).keyChar == keyChar)
                return true;
        }
        return false;
    }

    AssociateNode<T>* findAssociateNode(T keyChar)
    {
        for (int i = 0; i < associateList.size(); i++)
        {
            if (associateList.at(i).keyChar == keyChar)
                return &associateList.at(i);
        }
        return NULL;
    }

public:
    vector<AssociateNode<T> >& getAssociateList() { return associateList; }
    unsigned int getSize() { return associateList.size(); }

    T getKeyChar(unsigned int index)
    {
        if(index<0||index>=associateList.size())
            return 0;
        else
            return associateList.at(index).keyChar;
    }

    list<CharNode<T> > getFreqChar(unsigned int index)
    {
        list<CharNode<T> > charList;
        if (index < 0 || index >= associateList.size())
            return charList;
        else
        {
            AssociateNode<T> associateNode=associateList.at(index);
            charList=associateNode.charList;
        }
        return charList;
    }

    void clear() {associateList.clear();}

    void del()
    {
        if(!associateList.empty())
            associateList.pop_back();
    }

    void del(unsigned int index)
    {
        if(index>=0&&index<associateList.size())
        {
            typename vector<AssociateNode<T> >::iterator itor;
            unsigned int i;
            for(i=0,itor=associateList.begin();i<associateList.size()&&itor!=associateList.end();i++,itor++)
            {
                if(i==index)
                {
                    associateList.erase(itor);
                    return;
                }
            }
        }
    }

    void insert(T keyChar, T chr, float freq, int index, bool addNewKey = false)
    {
        if (!addNewKey)
        {
            if (index == -1)
                index = associateList.size() - 1;
            if (getKeyChar(index) == keyChar)
            {
                AssociateNode<T>* associateNodeAddr = &associateList.at(index);
                for (typename list<CharNode<T> >::iterator itor = (*associateNodeAddr).charList.begin(); itor != (*associateNodeAddr).charList.end(); itor++)
                {
                    if (itor->getChar() == chr)
                    {
                        associateNodeAddr->charList.erase(itor);
                        break;
                    }
                }
                (*associateNodeAddr).addCharNode(chr, freq);
            }
        }
        else
        {
            if(index == -1)
                index = associateList.size();
            if(index >= 0 && index <= associateList.size())
            {
                AssociateNode<T> associateNode(keyChar);
                associateNode.addCharNode(chr, freq);
                associateList.insert(associateList.begin() + index, associateNode);
            }
        }
    }

    //void print()
    //{
    //    for (int i = 0; i < associateList.size(); i++)
    //    {
    //        AssociateNode<T> node = associateList.at(i);
    //        printf("id:%u  %c",i,node.keyChar);
    //        list<CharNode<T> >::iterator itor;
    //        for (itor = node.charList.begin(); itor != node.charList.end(); itor++)
    //        {
    //            printf("  %c:%.2lf", itor->getChar(), itor->getFreq());
    //        }
    //        printf("\n");
    //    }
    //}
};

#endif
