#ifdef WIN32
#include <tchar.h>
#elif defined ANDROID
typedef unsigned char _TCHAR;
#include <stdio.h>
#endif

#include "levenshtein.h"

unsigned char levenshteinDistance::getDistance(string& str1, string& str2)
{
    unsigned char i = 0, j = 0;
    unsigned char len1 = str1.size();
    unsigned char len2 = str2.size();
    unsigned char* distArray1 = NULL;
    unsigned char* distArray2 = NULL;

    if (len1 == 0 && len2 == 0)
    {
        return 0;
    }

    if (len1 == 0 && len2 != 0)
    {
        return len2;
    }

    if (len1 != 0 && len2 == 0)
    {
        return len1;
    }

    if (len1 != 0 && len2 != 0)
    {
        distArray1 = new unsigned char[len1 + 1];
        distArray2 = new unsigned char[len1 + 1];

        for (i = 0; i <= len1; i++)
        {
            distArray2[i] = i;
        }

        for (j = 1; j <= len2; j++)
        {
            memcpy(distArray1, distArray2, sizeof(unsigned char) * (len1 + 1));
            distArray2[0] = j;

            for (i = 1; i <= len1; i++)
            {
                distArray2[i] = distArray1[i - 1];
                distArray2[i] += preciseCompChar((unsigned char)str1.at(i - 1), (unsigned char)str2.at(j - 1)) ? 1 : 0;

                if (distArray2[i - 1] + 1 < distArray2[i])
                {
                    distArray2[i] = distArray2[i - 1] + 1;
                }

                if (distArray1[i] + 1 < distArray2[i])
                {
                    distArray2[i] = distArray1[i] + 1;
                }
            }
        }

        len2 = distArray2[len1];
        delete[] distArray1;
        delete[] distArray2;

        return len2;
    }

    return 0;
}

unsigned char levenshteinDistance::getDistance(wstring& str1, wstring& str2)
{
    unsigned char i = 0, j = 0;
    unsigned char len1 = str1.size();
    unsigned char len2 = str2.size();
    unsigned char* distArray1 = NULL;
    unsigned char* distArray2 = NULL;

    if (len1 == 0 && len2 == 0)
    {
        return 0;
    }

    if (len1 == 0 && len2 != 0)
    {
        return len2;
    }

    if (len1 != 0 && len2 == 0)
    {
        return len1;
    }

    if (len1 != 0 && len2 != 0)
    {
        distArray1 = new unsigned char[len1 + 1];
        distArray2 = new unsigned char[len1 + 1];

        for (i = 0; i <= len1; i++)
        {
            distArray2[i] = i;
        }

        for (j = 1; j <= len2; j++)
        {
            memcpy(distArray1, distArray2, sizeof(unsigned char) * (len1 + 1));
            distArray2[0] = j;

            for (i = 1; i <= len1; i++)
            {
                distArray2[i] = distArray1[i - 1];
                distArray2[i] += preciseCompChar(str1.at(i - 1), str2.at(j - 1)) ? 1 : 0;

                if (distArray2[i - 1] + 1 < distArray2[i])
                {
                    distArray2[i] = distArray2[i - 1] + 1;
                }

                if (distArray1[i] + 1 < distArray2[i])
                {
                    distArray2[i] = distArray1[i] + 1;
                }
            }
        }

        len2 = distArray2[len1];
        delete[] distArray1;
        delete[] distArray2;

        return len2;
    }

    return 0;
}

unsigned char levenshteinDistance::getDistance(string& baseStr, string& subStr, vector<unsigned char>& result)
{
    unsigned char i = 0, j = 0, c = 0;
    unsigned char len1 = baseStr.size();
    unsigned char len2 = subStr.size();
    vector<unsigned char> tempResult;
    if (len1 == 0 && len2 == 0)
    {
        return 0;
    }

    if (len1 == 0 && len2 != 0)
    {
        return len2;
    }

    if (len1 != 0 && len2 == 0)
    {
        return len1;
    }

    if (len1 != 0 && len2 != 0)
    {
        i = 0;
        while (i < len2)
        {
            tempResult.clear();
            c = subStr.at(i);
            if (isOmitChar(c))
            {
                tempResult.push_back(result[0]);
                for (j = 1; j <= len1; j++)
                {
                    if (compChar(baseStr.at(j - 1), c))
                    {
                        result[j - 1] += 1;
                    }

                    tempResult.push_back(result[j - 1]);
                    if (tempResult[j] > result[j])
                    {
                        tempResult[j] = result[j];
                    }

                    if (tempResult[j] > tempResult[j - 1] + 1)
                    {
                        tempResult[j] = tempResult[j - 1] + 1;
                    }
                }
            }
            else
            {
                tempResult.push_back(result[0] + 1);
                for (j = 1; j <= len1; j++)
                {
                    if (compChar(baseStr.at(j - 1), c))
                    {
                        result[j - 1] += 1;
                    }

                    tempResult.push_back(result[j - 1]);
                    if (tempResult[j] > result[j] + 1)
                    {
                        tempResult[j] = result[j] + 1;
                    }

                    if (tempResult[j] > tempResult[j - 1] + 1)
                    {
                        tempResult[j] = tempResult[j - 1] + 1;
                    }
                }
            }

            for(j = 0; j <= len1; j++)
            {
                result[j] = tempResult[j];
            }
            i++;
        }
        return result[j - 1];
    }

    return 0;
}

unsigned char levenshteinDistance::getDistance(wstring& baseStr, wstring& subStr, vector<unsigned char>& result)
{
    wchar_t c = 0;
    unsigned char i = 0, j = 0;
    unsigned char len1 = baseStr.size();
    unsigned char len2 = subStr.size();
    vector<unsigned char> tempResult;

    if (len1 == 0 && len2 == 0)
    {
        return 0;
    }

    if (len1 == 0 && len2 != 0)
    {
        return len2;
    }

    if (len1 != 0 && len2 == 0)
    {
        return len1;
    }

    if (len1 != 0 && len2 != 0)
    {
        i = 0;
        while (i < len2)
        {
            tempResult.clear();
            c = subStr.at(i);
            if (isOmitChar(c))
            {
                tempResult.push_back(result[0]);
                for (j = 1; j <= len1; j++)
                {
                    if (compChar(baseStr.at(j - 1), c))
                    {
                        result[j - 1] += 1;
                    }

                    tempResult.push_back(result[j - 1]);
                    if (tempResult[j] > result[j])
                    {
                        tempResult[j] = result[j];
                    }

                    if (tempResult[j] > tempResult[j - 1] + 1)
                    {
                        tempResult[j] = tempResult[j - 1] + 1;
                    }
                }
            }
            else
            {
                tempResult.push_back(result[0] + 1);
                for (j = 1; j <= len1; j++)
                {
                    if (compChar(baseStr.at(j - 1), c))
                    {
                        result[j - 1] += 1;
                    }

                    tempResult.push_back(result[j - 1]);
                    if (tempResult[j] > result[j] + 1)
                    {
                        tempResult[j] = result[j] + 1;
                    }

                    if (tempResult[j] > tempResult[j - 1] + 1)
                    {
                        tempResult[j] = tempResult[j - 1] + 1;
                    }
                }
            }

            for (j = 0; j <= len1; j++)
            {
                result[j] = tempResult[j];
            }
            i++;
        }
        return result[j - 1];
    }

    return 0;
}
