#ifndef _LEVEN_SHTEIN_
#define _LEVEN_SHTEIN_

#include <string>
#include <vector>

using namespace std;

typedef class levenshteinDistance
{
public:
    static unsigned char getDistance(string& str1, string& str2);
    static unsigned char getDistance(wstring& str1, wstring& str2);
    static unsigned char getDistance(string& baseStr, string& subStr,vector<unsigned char>& result);
    static unsigned char getDistance(wstring& baseStr, wstring& subStr,vector<unsigned char>& result);

    inline static bool isOmitChar(unsigned char ch)
    {
        return (ch == 0x20 || ch == 0x27 || ch == 0x2D);
    };

    inline static bool isOmitChar(wchar_t ch)
    {
        return (ch == 0x20 || ch == 0x27 || ch == 0x2D);
    };

    static int compChar(unsigned char keyChar, unsigned char childChar)
    {
        int iRet = keyChar - childChar;

        if (keyChar == 0xff)
        {
			if (isOmitChar(childChar))
			{
				return 1;
			}
            return 0;
        }
        // will replaced by all language capital match, or store whole word.
        if (keyChar >= 'A' && keyChar <= 'Z' && iRet == -32)
        {
            return 0;
        }

        if (keyChar >= 'a' && keyChar <= 'z' && iRet == 32)
        {
            return 0;
        }

        if ((keyChar == 0x20 && iRet == -7) || (keyChar == 0x27 && iRet == 7))
        {
            return 0;
        }

        switch (keyChar)
        {
            case 'a':
            case 'A':
                if ((childChar >= 0xC0 && childChar <= 0xC6) ||
                    (childChar >= 0xE0 && childChar <= 0xE6))
                {
                    iRet = 0;
                }
                break;

            case 'e':
            case 'E':
                if ((childChar >= 0xC8 && childChar <= 0xCB) ||
                    (childChar >= 0xE8 && childChar <= 0xEB))
                {
                    iRet = 0;
                }
                break;

            case 'i':
            case 'I':
                if ((childChar >= 0xCC && childChar <= 0xCF) ||
                    (childChar >= 0xEC && childChar <= 0xEF))
                {
                    iRet = 0;
                }
                break;

            case 'o':
            case 'O':
                if ((childChar >= 0xD2 && childChar <= 0xD6) ||
                    (childChar >= 0xF2 && childChar <= 0xF6))
                {
                    iRet = 0;
                }
                break;

            case 'u':
            case 'U':
                if ((childChar >= 0xD9 && childChar <= 0xDC) ||
                    (childChar >= 0xF9 && childChar <= 0xFC))
                {
                    iRet = 0;
                }
                break;

            case 'c':
            case 'C':
                if (childChar == 0xC7 && childChar == 0xE7)
                {
                    iRet = 0;
                }
                break;

            case 'n':
            case 'N':
                if (childChar == 0xD1 && childChar == 0xF1)
                {
                    iRet = 0;
                }
                break;

            case 'y':
            case 'Y':
                if (childChar == 0xDD && childChar == 0xFD)
                {
                    iRet = 0;
                }
                break;
        }

        return iRet;
    };

    static int compChar(wchar_t keyChar, wchar_t childChar)
    {
        int iRet = keyChar - childChar;
		if (keyChar == 0x00ff)
		{
			if (isOmitChar(childChar))
			{
				return 1;
			}
			return 0;
		}
		else if (keyChar>=0x0430 && keyChar<=0x044f)
		{
			if (iRet == 32)
			{
				iRet = 0;
			}
		}
		else if (keyChar>=0x0410 && keyChar<=0x042f)
		{
			if (iRet == -32)
			{
				iRet = 0;
			}
		}
		else if (keyChar == 0x0451)
		{
			if (iRet == 80)
			{
				iRet = 0;
			}
		}
		else if (keyChar == 0x0401)
		{
			if (iRet == -80)
			{
				iRet = 0;
			}
		}
        else if ((keyChar == 0x20 && iRet == -7) || (keyChar == 0x27 && iRet == 7))
        {
            return 0;
        }

        return iRet;
    };

//  static void string2lower(string str)
//  {
//      for (int i=0;i<str.size();i++)
//      {
//          if (str.at(i) >= 0x41 && str.at(i) <= 0x5A)
//          {
//              str.replace(i,1,1,str.at(i)+0x20);
//          }
//      }
//  };

    inline static int preciseCompChar(unsigned char keyChar, unsigned char childChar)
    {
        return keyChar - childChar;
    };
	 
    inline static int preciseCompChar(wchar_t keyChar, wchar_t childChar)
    {
        return keyChar - childChar;
    };
} LSDistance;


//template <class T>
//class DistanceItem
//{
//public:
//    DistanceItem()
//    {
//        str.clear();
//        distance = 0;
//    };
//
//    DistanceItem(T inStr, unsigned char inDistance)
//    {
//        str = inStr;
//        distance = inDistance;
//    };
//
//    inline unsigned char getDistance() { return distance; };
//    inline T& getStr() { return str; };
//
//    bool operator< (DistanceItem& comp)
//    {
//        return distance < comp.getDistance();
//    };
//private:
//    T str;
//    unsigned char distance;
//};
//
//template <class T>
//class DistanceListNode
//{
//public:
//  T key;
//  list<DistanceItem<T>> distanceList;
//  vector<DistanceListNode> children;
//
//  DistanceListNode()
//  {
//      key.clear();
//      distanceList.clear();
//      children.clear();
//  }
//
//  DistanceListNode(T inKey)
//  {
//      key=inKey;
//      distanceList.clear();
//      children.clear();
//  }
//};

//class StoreDistance : PatriciaTrie
//{
//public:
//  DistanceListNode<string> root;
//
//  //在词库中查找与str的距离是0或者1的词语
//  void storeKeyDistance(unsigned char* pBuf, unsigned int currentIdx, string& str, unsigned char maxDistance, list<DistanceItem<string>>& distanceList, unsigned char prevDistance=255,bool index=false)
//  {
//      PatriciaTrieNode node;
//      string nodeStr;
//      unsigned int childIdx;
//      unsigned char cnt,distance;
//      unsigned char childNum=getSerialChildLen(pBuf + currentIdx);
//      unsigned char* pChildBuf=getSerialChildBuf(pBuf + currentIdx) + 1;
//
//      for (cnt = 0; cnt < childNum; cnt++)
//      {
//          childIdx = readU24(pChildBuf);
//
//          nodeStr.clear();
//          node.getSerialNodeStr(pBuf,childIdx,nodeStr);
//          distance=LSDistance::getDistance(str, nodeStr);
//
//          if(distance>=prevDistance&&index)
//          {
//              pChildBuf += 3;
//              continue;
//          }
//          if(distance<=prevDistance)
//          {
//              if (getSerialIsTerminal(pBuf + childIdx)&&distance<=maxDistance)
//              {
//                  DistanceItem<string> distanceItem(nodeStr,distance);
//                  distanceList.push_back(distanceItem);
//              }
//              if(distance==prevDistance)
//                  storeKeyDistance(pBuf, childIdx, str, maxDistance, distanceList,distance,true);
//              else
//                  storeKeyDistance(pBuf, childIdx, str, maxDistance, distanceList,distance);
//          }
//          pChildBuf += 3;
//      }
//  };
//
//  //在词库中查找与str的距离是0或者1的词语,如果其中包含距离是0的词语，那么将dis0设为true
//  void storeKeyDistance1(unsigned char* pBuf, unsigned int currentIdx, string& str, unsigned char maxDistance, list<DistanceItem<string>>& distanceList, bool& dis0, unsigned char prevDistance=255,bool index=false)
//  {
//      PatriciaTrieNode node;
//      string nodeStr;
//      unsigned int childIdx;
//      unsigned char cnt,distance;
//      unsigned char childNum=getSerialChildLen(pBuf + currentIdx);
//      unsigned char* pChildBuf=getSerialChildBuf(pBuf + currentIdx) + 1;
//
//      for (cnt = 0; cnt < childNum; cnt++)
//      {
//          childIdx = readU24(pChildBuf);
//
//          nodeStr.clear();
//          node.getSerialNodeStr(pBuf,childIdx,nodeStr);
//          distance=LSDistance::getDistance(str, nodeStr);
//
//          if(distance>=prevDistance&&index)
//          {
//              pChildBuf += 3;
//              continue;
//          }
//          if(distance<=prevDistance)
//          {
//              if (getSerialIsTerminal(pBuf + childIdx)&&distance<=maxDistance)
//              {
//                  DistanceItem<string> distanceItem(nodeStr,distance);
//                  distanceList.push_back(distanceItem);
//                  if(distance==0)
//                      dis0=true;
//              }
//              if(distance==prevDistance)
//                  storeKeyDistance1(pBuf, childIdx, str, maxDistance, distanceList,dis0,distance,true);
//              else
//                  storeKeyDistance1(pBuf, childIdx, str, maxDistance, distanceList,dis0,distance);
//          }
//          pChildBuf += 3;
//      }
//  };
//
//  //将str按照两个字符分割的原则，进行层级分割，存储所有分割后的subStr在词库中与之距离是0或1的词语
//  void storeDistanceTree(unsigned char* pBuf, DistanceListNode<string>& currNode, string& str, unsigned char maxDistance)
//  {
//      int i = (str.size() < 2) ? str.size() : 2;
//      for(; i <= str.length(); i++)
//      {
//          DistanceListNode<string> node;
//          node.key=str.substr(0, i);
//          storeKeyDistance(pBuf, 0, node.key, maxDistance, node.distanceList);
//
//          if(node.distanceList.size() > 0)
//          {
//              //storeDistanceTree(pBuf,node,str.substr(i),maxDistance);
//              currNode.children.push_back(node);
//          }
//          else
//              return;
//      }
//  };
//
//  //将str按照两个字符分割的原则，进行层级分割（只进行到aimLevel层），存储所有分割后的subStr在词库中与之距离是0或1的词语
//  void storeDistanceTree1(unsigned char* pBuf, DistanceListNode<string>& currNode, string& str, unsigned char maxDistance,unsigned char aimLevel,unsigned char level=1)
//  {
//      for(int i=str.length()%2==0?2:1;i<=str.length();i+=2)
//      {
//          DistanceListNode<string> node;
//          node.key=str.substr(0,i);
//          storeKeyDistance(pBuf,0,node.key,maxDistance,node.distanceList);
//          if(node.distanceList.size()>0)
//          {
//              if(level<aimLevel)
//                  storeDistanceTree1(pBuf,node,str.substr(i),maxDistance,aimLevel,level+1);
//              if(level>1||(level==1&&node.children.size()>0))
//                  currNode.children.push_back(node);
//          }
//          else
//              return;
//      }
//  };
//
//  //将str按照两个字符分割的原则，进行层级分割（每层只留倒数第backwardIndex的词语），存储所有分割后的subStr在词库中与之距离是0或1的词语
//  void storeDistanceTree2(unsigned char* pBuf, DistanceListNode<string>& currNode, string& str, unsigned char maxDistance,unsigned char backwardIndex=1)
//  {
//      for(int i=str.length()%2==0?2:1;i<=str.length();i+=2)
//      {
//          DistanceListNode<string> node;
//          node.key=str.substr(0,i);
//          storeKeyDistance(pBuf,0,node.key,maxDistance,node.distanceList);
//          if(node.distanceList.size()>0)
//          {
//              storeDistanceTree2(pBuf,node,str.substr(i),maxDistance,backwardIndex);
//              currNode.children.push_back(node);
//          }
//          else
//          {
//              break;
//          }
//      }
//      if(!currNode.children.empty())
//      {
//          unsigned char id=currNode.children.size()>=backwardIndex?currNode.children.size()-backwardIndex:0;
//          DistanceListNode<string> childNode=currNode.children.at(id);
//          currNode.children.clear();
//          currNode.children.push_back(childNode);
//      }
//  };
//
//  //将str按照两个字符分割的原则，进行层级分割（每层只留倒数第1个词语，倒数第1个存在与之距离为0的词语），存储所有分割后的subStr在词库中与之距离是0或1的词语
//  void storeDistanceTree3(unsigned char* pBuf, DistanceListNode<string>& currNode, string& str,vector<int>& pos)
//  {
//      int id0=-1;
//      bool dis0=false,posId=false;
//      for(int i=str.length()%2==0?2:1;i<=str.length();i+=2)
//      {
//          posId=false;
//          for(int j=0;j<pos.size();j++)
//          {
//              if(pos.at(j)==(str.length()<<8|i))
//              {
//                  posId=true;
//                  break;
//              }
//          }
//          if(posId)
//              continue;
//          else
//              pos.push_back(str.length()<<8|i);
//          DistanceListNode<string> node;
//          node.key=str.substr(0,i);
//          dis0=false;
//          storeKeyDistance1(pBuf,0,node.key,1,node.distanceList,dis0);
//          if(node.distanceList.size()>0)
//          {
//              currNode.children.push_back(node);
//              if(dis0)
//                  id0=currNode.children.size()-1;
//          }
//          else
//          {
//              break;
//          }
//      }
//      if(!currNode.children.empty())
//      {
//          DistanceListNode<string> childNode0;
//          int id1=currNode.children.size()-1;
//          if(id0!=-1&&id0!=id1)
//              childNode0=currNode.children.at(id0);
//          DistanceListNode<string> childNode1=currNode.children.at(id1);
//          currNode.children.clear();
//          if(id0!=-1&&id0!=id1)
//              currNode.children.push_back(childNode0);
//          currNode.children.push_back(childNode1);
//          for(int i=0;i<currNode.children.size();i++)
//          {
//              storeDistanceTree3(pBuf,currNode.children.at(i),str.substr(currNode.children.at(i).key.length()),pos);
//          }
//      }
//  };
//
//  void storeDistanceTree4(unsigned char* pBuf, DistanceListNode<string>& currNode, string& str)
//  {
//      int id0=-1;
//      bool dis0=false;
//      for(int i=str.length()%2==0?2:1;i<=str.length();i+=2)
//      {
//          DistanceListNode<string> node;
//          node.key=str.substr(0,i);
//          dis0=false;
//          storeKeyDistance1(pBuf,0,node.key,1,node.distanceList,dis0);
//          if(node.distanceList.size()>0)
//          {
//              currNode.children.push_back(node);
//              if(dis0)
//                  id0=currNode.children.size()-1;
//          }
//          else
//          {
//              break;
//          }
//      }
//      if(!currNode.children.empty())
//      {
//          DistanceListNode<string> childNode0;
//          int id1=currNode.children.size()-1;
//          if(id0!=-1&&id0!=id1)
//              childNode0=currNode.children.at(id0);
//          DistanceListNode<string> childNode1=currNode.children.at(id1);
//          currNode.children.clear();
//          if(id0!=-1&&id0!=id1)
//              currNode.children.push_back(childNode0);
//          else
//              currNode.children.push_back(childNode1);
//          storeDistanceTree4(pBuf,currNode.children.at(0),str.substr(currNode.children.at(0).key.length()));
//      }
//  };
//
//
//  void printTree(DistanceListNode<string>& currNode)
//  {
//      int i,j;
//      for(i=0;i<currNode.children.size();i++)
//      {
//          DistanceListNode<string> node=currNode.children.at(i);
//          list<DistanceItem<string>>::iterator itor;
//          printf("key:%s, count:%d\n",node.key.c_str(),node.distanceList.size());
//          for(j=0,itor=node.distanceList.begin();j<node.distanceList.size()&&itor!=node.distanceList.end();j++,itor++)
//          {
//              printf("%s, %d    ",(*itor).getStr().c_str(),(*itor).getDistance());
//              //if((j+1)%3==0)
//              //  printf("\n");
//          }
//          printf("\n");
//          printTree(node);
//      }
//  }
//};

#endif