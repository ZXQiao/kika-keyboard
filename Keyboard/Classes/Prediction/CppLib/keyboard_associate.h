#ifndef KEYBORD_ASSOCIATE
#define KEYBORD_ASSOCIATE

#include "associate.h"
#include <map>

#ifndef WIN32
#include <math.h>
using namespace std;

struct POINT{
	float x,y;
};
#endif

template <class TChar>
class TrackPoint
{
public:
    TrackPoint () : charNum(0)
    {
        pt.x = 0;
        pt.y = 0;
    };

    TrackPoint (int x, int y) : charNum(0)
    {
        pt.x = x;
        pt.y = y;
    };

    void setChar(TChar num) { charNum = num; };
    void setX(float x) { pt.x = x; };
    void setY(float y) { pt.y = y; };
    TChar getChar() { return charNum; };
    POINT& getPoint() { return pt; };

private:
    POINT pt;
    TChar charNum;
};

template <class CoValueType>
class Coordinate
{
public:
    Coordinate () : iX(0), iY(0), uRow(0), uCol(0) {};
    Coordinate (CoValueType x, CoValueType y) : iX(x), iY(y), uRow(0), uCol(0) {};
    Coordinate (CoValueType x, CoValueType y, unsigned char row, unsigned char col) : iX(x), iY(y), uRow(row), uCol(col) {};
    inline CoValueType getX() { return iX; };
    inline CoValueType getY() { return iY; };
    inline void setX(CoValueType x) { iX = x; };
    inline void setY(CoValueType y) { iY = y; };
    inline unsigned char getRow() { return uRow; };
    inline unsigned char getCol() { return uCol; };
private:
    CoValueType iX;
    CoValueType iY;
    unsigned char uRow;
    unsigned char uCol;
};

template <class T>
class keyMap
{
public:
    keyMap() : uCol(0), uRow(0) { keyMatrix = NULL; };
    keyMap(unsigned char rowValue, unsigned char colValue) : uCol(colValue), uRow(rowValue)
    {
        keyMatrix = new T*[uRow];
        for (int i = 0; i < uRow; i++)
        {
            keyMatrix[i] = new T[uCol];
        }
    };
    ~keyMap()
    {
        for (int i = 0; i < uRow; i++)
        {
            delete[] keyMatrix[i];
        }
        delete[] keyMatrix;
    };

    void initKeyMap(unsigned char rowValue, unsigned char colValue, float offset[])
    {
        int i;

        for (i = 0; i < uRow; i++)
        {
            delete[] keyMatrix[i];
        }
        delete[] keyMatrix;
        fOffsetVec.clear();

        uRow = rowValue;
        uCol = colValue;
        keyMatrix = new T*[uRow];
        for (i = 0; i < uRow; i++)
        {
            keyMatrix[i] = new T[uCol];
            fOffsetVec.push_back(offset[i]);
        }
    };

    void setOffset(float offset[])
    {
        for (int i = 0; i < uRow; i++)
        {
            fOffsetVec.at(i) = offset[i];
        }
    };

    float getOffset(unsigned char row)
    {
        if (row <= uRow && row > 0)
        {
            return fOffsetVec.at(row - 1);
        }

        return 0.0;
    };

    void setKey(T keyChar, unsigned char row, unsigned char col)
    {
        if (row == 0 || row > uRow || col == 0 || col > uCol)
        {
            return;
        }

        keyMatrix[row - 1][col - 1] = keyChar;
    };

    T getKey(unsigned char row, unsigned char col)
    {
        if (row == 0 || row > uRow || col == 0 || col > uCol)
        {
            return 0;
        }

        return keyMatrix[row - 1][col - 1];
    };

    inline unsigned char getKeyMatRow() { return uRow; };

private:
    unsigned char uCol;
    unsigned char uRow;
    T** keyMatrix;

    vector<float> fOffsetVec;
};

template <class T, class CoValueType>
class keyboard_associate
{
public:
    keyboard_associate() : distance(0)
    {
        centerKeyPosition.clear();
    };

    vector<TrackChar<T> >& getTrackCharVec() { return allChar; };
    void resetTrackCharVec() { allChar.clear(); };
    vector<TrackChar<T> >& getTrustCharVec() { return trustChar; };
    void resetTrustCharVec() { trustChar.clear(); };

    Associate<T>& getAssList() { return assList; };
    void resetAssList() { assList.clear(); };
    void delOneCharInAss() { assList.del(); };
    void delOneCharInAss(unsigned int idx) { assList.del(idx); };

    void generateKeyPosition(T key, CoValueType x, CoValueType y, unsigned int uRow = 0, unsigned int uCol = 0)
    {
#ifdef WIN32
        centerKeyPosition.insert(map<T, Coordinate<CoValueType>>::value_type (key, Coordinate<CoValueType>(x, y, uRow, uCol)));
#elif defined ANDROID
		typename std::map<T, Coordinate<CoValueType> >::value_type val(key, Coordinate<CoValueType>(x, y, uRow, uCol));
		centerKeyPosition.insert(val);
#else
        centerKeyPosition.insert(pair<T, Coordinate<CoValueType>> (key, Coordinate<CoValueType>(x, y, uRow, uCol)));
#endif
        if (uRow > 0 && uCol > 0)
        {
            keyMatrix.setKey(key, uRow, uCol);
        }
    };

    unsigned char clickHandle(T key, CoValueType keyWidth, CoValueType keyHeight, CoValueType x, CoValueType y)
    {
        unsigned char uRet = 0;
        float fastSqrtNum = 0.0;
        CoValueType centerX, centerY;
        typename map<T, Coordinate<CoValueType> >::iterator itor = centerKeyPosition.find(key);

        clickPoint.setX(x);
        clickPoint.setY(y);

        if (itor != centerKeyPosition.end())
        {
            centerY = itor->second.getY();
            centerX = itor->second.getX();

            keyWidth /= 4;
            keyHeight /= 4;
            x -= centerX;
            y -= centerY;

            fastSqrtNum = x * x + y * y;
            distance = 1 / InvSqrt(fastSqrtNum);

            if (x > 0 && x > keyWidth)
            {
                uRet |= 0x01;
            }
            else if (x < 0 && x < -1 * keyWidth)
            {
                uRet |= 0x02;
            }

            if (y > 0 && y > keyHeight)
            {
                uRet |= 0x04;
            }
            else if (y < 0 && y < -1 * keyHeight)
            {
                uRet |= 0x08;
            }
        }
        else
        {
            distance = (keyHeight > keyWidth) ? keyHeight : keyWidth;
        }

        addDefaultKeyToAssociateList(key);

        return uRet;
    };

    void clickHandle(T key)
    {
        unsigned char keyRow, keyCol;

        addDefaultKeyToAssociateList(key);
        typename map<T, Coordinate<CoValueType> >::iterator itor = centerKeyPosition.find(key);

        if (itor != centerKeyPosition.end())
        {
            keyRow = itor->second.getRow();
            keyCol = itor->second.getCol();
            processAssociate(key, keyRow, keyCol);
        }
    };

    void initKeyMap(unsigned char rowValue, unsigned char colValue, float offset[])
    {
        keyMatrix.initKeyMap(rowValue, colValue, offset);
    };

    void setOffset(float offset[])
    {
        keyMatrix.setOffset(offset);
    };

	virtual bool processTrackPoint()
	{
		generateTrustPointByIOChange(pointList);
		return true;
	}

	virtual void addTrackPointToList(float x, float y, T ch)
	{
		TrackPoint<T> trackP(x, y);
		trackP.setChar(ch);
		pointList.push_back(trackP);
	}

	virtual void resetTrackPointList()
	{
		pointList.clear();
	}

private:
    CoValueType distance;
    Coordinate<CoValueType> clickPoint;
    Associate<T> assList;
    map<T, Coordinate<CoValueType> > centerKeyPosition;
    keyMap<T> keyMatrix;
	vector<TrackChar<T> > trustChar;
	vector<TrackChar<T> > allChar;
	list<TrackPoint<T> > pointList;

    inline bool isVowel(T key)
    {
        if (key == 0x41 || key == 0x61)
        {
            return true;
        }
        if (key == 0x45 || key == 0x65)
        {
            return true;
        }
        if (key == 0x49 || key == 0x69)
        {
            return true;
        }
        if (key == 0x4F || key == 0x6F)
        {
            return true;
        }
        if (key == 0x55 || key == 0x75)
        {
            return true;
        }

        return false;
    };

    void processAssociate(T key, unsigned char keyRow, unsigned char keyCol)
    {
        float lineOff;
        T assKeyL, assKeyR;
        assKeyL = keyMatrix.getKey(keyRow, keyCol - 1);
        assKeyR = keyMatrix.getKey(keyRow, keyCol + 1);

        if (assKeyL > 0 && assKeyR > 0)
        {
            //bool isVowelL = isVowel(assKeyL);
            //bool isVowelR = isVowel(assKeyR);

            //if ((isVowelL && isVowelR) || !(isVowelL || isVowelR))
            //{
            //    addKeyToAssociateList(key, assKeyL);
            //    addKeyToAssociateList(key, assKeyR);
            //}
            //else if (isVowelL)
            //{
            //    addKeyToAssociateList(key, assKeyL);
            //}
            //else if (isVowelR)
            //{
            //    addKeyToAssociateList(key, assKeyR);
            //}
            addKeyToAssociateList(key, assKeyL);
            addKeyToAssociateList(key, assKeyR);
        }
        else if (assKeyL > 0)
        {
            addKeyToAssociateList(key, assKeyL);
        }
        else if (assKeyR > 0)
        {
            addKeyToAssociateList(key, assKeyR);
        }

        if (keyRow - 1 > 0)
        {
            lineOff = keyMatrix.getOffset(keyRow) - keyMatrix.getOffset(keyRow - 1);

            assKeyL = keyMatrix.getKey(keyRow - 1, floor(keyCol + lineOff));
            if (assKeyL > 0)
            {
                addKeyToAssociateList(key, assKeyL);
            }

            assKeyR = keyMatrix.getKey(keyRow - 1, ceil(keyCol + lineOff));
            if (assKeyR > 0 && assKeyR != assKeyL)
            {
                addKeyToAssociateList(key, assKeyR);
            }
        }

        if (keyRow + 1 <= keyMatrix.getKeyMatRow())
        {
            lineOff = keyMatrix.getOffset(keyRow) - keyMatrix.getOffset(keyRow + 1);

            assKeyL = keyMatrix.getKey(keyRow + 1, floor(keyCol + lineOff));
            if (assKeyL > 0)
            {
                addKeyToAssociateList(key, assKeyL);
            }

            assKeyR = keyMatrix.getKey(keyRow + 1, ceil(keyCol + lineOff));
            if (assKeyR > 0 && assKeyR != assKeyL)
            {
                addKeyToAssociateList(key, assKeyR);
            }
        }

//        if (isVowel(key))
//        {
//            addVowelKey(key);
//        }
    };

    void addKeyToAssociateList(T centerKey, T key, CoValueType x = 0, CoValueType y = 0)
    {
        //float fastSqrtNum = 0.0;
        //CoValueType clickX = (x == 0) ? clickPoint.getX() : x;
        //CoValueType clickY = (y == 0) ? clickPoint.getY() : y;

        typename map<T, Coordinate<CoValueType> >::iterator itor = centerKeyPosition.find(key);

        //if (itor != centerKeyPosition.end())
        //{
        //    clickY -= itor->second.getY();
        //    clickX -= itor->second.getX();

        //    fastSqrtNum = clickX * clickX + clickY * clickY;
        //    assList.insert(centerKey, key, distance * InvSqrt(fastSqrtNum), -1, false);
        //}
        if (itor != centerKeyPosition.end())
        {
            assList.insert(centerKey, key, 0.85, -1, false);
        }
    };

    void addVowelKey(T key)
    {
        if (key != 0x41 && key != 0x61)
        {
            addKeyToAssociateList(key, 0x61);
        }
        if (key != 0x45 && key != 0x65)
        {
            addKeyToAssociateList(key, 0x65);
        }
        if (key != 0x49 && key != 0x69)
        {
            addKeyToAssociateList(key, 0x69);
        }
        if (key != 0x4F && key != 0x6F)
        {
            addKeyToAssociateList(key, 0x6F);
        }
        if (key != 0x55 && key != 0x75)
        {
            addKeyToAssociateList(key, 0x75);
        }
    }

    void addDefaultKeyToAssociateList(T key)
    {
        assList.insert(key, key, 1.0, -1, true);
    };

    void generateTrustPointByIOChange(list<TrackPoint<T> >& trackPointList)
    {
        vector<TrackPoint<T> > pointBak;
        TrackPoint<T> endChar = trackPointList.front();
        TrackPoint<T> centerChar;
        TrackPoint<T> startChar = trackPointList.front();
        TrackChar<T> trackChar;
        typename map<T, Coordinate<CoValueType> >::iterator keyPosItor;
        unsigned int centerCnt = 0;
        float iPrevDistY = 0, iPrevDistX = 0, iDistY = 0, iDistX = 0;
        float cValue = 0.0;
        const float lambda = 0.7;
        const float untrustLambda = 0.15;
        const int validPointCount = 2;    // android point capture rate is too low. set to 2.

        trustChar.clear();
        allChar.clear();

        trackPointList.pop_front();
        trackChar.set(startChar.getChar(), 1.0);
        keyPosItor = centerKeyPosition.find(startChar.getChar());
        if (keyPosItor != centerKeyPosition.end())
        {
            unsigned char row = keyPosItor->second.getRow();
            unsigned char col = keyPosItor->second.getCol();
            T assKeyL, assKeyR;

            assKeyL = keyMatrix.getKey(row, col - 1);
            assKeyR = keyMatrix.getKey(row, col + 1);

            if (assKeyL > 0)
            {
                trackChar.setLeftChar(assKeyL);
            }
            if (assKeyR > 0)
            {
                trackChar.setRightChar(assKeyR);
            }
        }

        trustChar.push_back(trackChar);
        allChar.push_back(trackChar);

        while (trackPointList.size() > 0)
        {
            if (trackPointList.front().getChar() != startChar.getChar())
            {
                if (centerCnt < validPointCount)
                {
                    trackChar.set(startChar.getChar(), untrustLambda);
                    allChar.push_back(trackChar);
                }
                else
                {
                    centerChar = pointBak.at(((unsigned int)pointBak.size()) >> 1);
                    iPrevDistY = startChar.getPoint().y - centerChar.getPoint().y;
                    iPrevDistX = startChar.getPoint().x - centerChar.getPoint().x;
                    iDistY = centerChar.getPoint().y - endChar.getPoint().y;
                    iDistX = centerChar.getPoint().x - endChar.getPoint().x;

                    cValue = (iPrevDistX * iDistX + iPrevDistY * iDistY) *
                            InvSqrt(iPrevDistX * iDistX * iPrevDistX * iDistX +
                            iPrevDistY * iDistY * iPrevDistY * iDistY +
                            iPrevDistX * iDistY * iPrevDistX * iDistY +
                            iPrevDistY * iDistX * iPrevDistY * iDistX);
                    //TRACE("Track char: %c, Cos: %f.\n", startChar.getChar(), cValue);

                    cValue = (cValue > 1) ? 1 : cValue;
                    cValue = (cValue < -1) ? -1 : cValue;
                    cValue = ::acos(cValue);
                    //TRACE("Track char: %c,  %f.\n", startChar.getChar(), cValue);

                    trackChar.set(startChar.getChar(), cValue);
                    if (cValue > lambda)
                    {
                        trustChar.push_back(trackChar);
                    }
                    allChar.push_back(trackChar);
                }

                centerCnt = 0;
                endChar = trackPointList.front();
                pointBak.clear();
                startChar = trackPointList.front();
            }
            else
            {
                endChar = trackPointList.front();
                pointBak.push_back(trackPointList.front());
                centerCnt++;
            }

            trackPointList.pop_front();
        }

        trackChar.set(startChar.getChar(), 1.0);
        keyPosItor = centerKeyPosition.find(startChar.getChar());
        if (keyPosItor != centerKeyPosition.end())
        {
            unsigned char row = keyPosItor->second.getRow();
            unsigned char col = keyPosItor->second.getCol();
            T assKeyL, assKeyR;

            assKeyL = keyMatrix.getKey(row, col - 1);
            assKeyR = keyMatrix.getKey(row, col + 1);

            if (assKeyL > 0)
            {
                trackChar.setLeftChar(assKeyL);
            }
            if (assKeyR > 0)
            {
                trackChar.setRightChar(assKeyR);
            }
        }

        trustChar.push_back(trackChar);
        allChar.push_back(trackChar);
    };

    void generateTrustPointByIOChangeWithoutCenter(list<TrackPoint<T> >& trackPointList)
    {
        TrackPoint<T> tmpChar = trackPointList.front();
        trackPointList.pop_front();
        int iPrevDistY = 0, iPrevDistX = 0, iDistY = 0, iDistX = 0;
        int lambda = 10;

        while (trackPointList.size() > 0)
        {
            if (trackPointList.front().getChar() != tmpChar.getChar())
            {
                iDistY = trackPointList.front().getPoint().y - tmpChar.getPoint().y;
                iDistX = trackPointList.front().getPoint().x - tmpChar.getPoint().x;

                if ((iPrevDistX == 0 && iPrevDistY == 0) ||
                    (iDistY * iPrevDistX - iPrevDistY * iDistX > lambda * iDistX * iPrevDistX ||
                    iPrevDistY * iDistX - iDistY * iPrevDistX > lambda * iDistX * iPrevDistX))
                {
                    trustChar.push_back(tmpChar);
                }

                iPrevDistY = iDistY;
                iPrevDistX = iDistX;
            }

            tmpChar = trackPointList.front();
            trackPointList.pop_front();
        }

        trustChar.push_back(tmpChar);
    };

    float InvSqrt(float x)
    {
        float xhalf = 0.5 * x;
        int i = *(int*)&x;              // get bits for floating VALUE
        i = 0x5f375a86 - (i >> 1);      // gives initial guess y0
        x = *(float*)&i;                // convert bits BACK to float
        x = x * (1.5 - xhalf * x * x);  // Newton
        x = x * (1.5 - xhalf * x * x);  // Newton

        return x;
    }
};

#endif