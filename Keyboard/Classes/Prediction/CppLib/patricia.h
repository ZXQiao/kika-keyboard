#ifndef _PATRICIA_
#define _PATRICIA_

#if defined(__GNUC__ ) || defined(ANDROID)
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif
#ifdef ANDROID
#include <android/log.h>
#include <jni.h>
#define  LOG_TAG    "xinmei"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#endif

#include <stdlib.h>
#include <stdio.h>

#include <vector>
#include <list>
#include <algorithm>
#include "associate.h"
#include <queue>
#include <stack>

#ifdef WIN32
#include <afx.h>
#else
#include <sys/mman.h>
#endif

//#include "pthread.h"

#define MAX_USER_WORD_NUM 500
#define MINI_WEIGHT 0.02
#define MAX_P 512
#define MAX_BI_DATA_PREDICT 100
#define LEVEL_WEIGHT 0.1
#define TYPE_ONE 1
#define TYPE_TWO 2
#define DEFAULT_WORD_WEIGHT 100
#define DICT_VERSION "1006"
#define MAX_USER_DICT_TOKEN 60000
#define MAX_FUZZY_SEARCH_COUNT  2
#define OUTPUT_PHRASE_COUNT     2

//user dict define
#define USER_DICT_SIZE 500
#define TOKEN_SIZE 2
#define DICT_VERSION_SIZE 4
#define USER_WORD_LEN 1
#define DATA_SIZE 1
#define TIME_SIZE 2
#define USER_WORD 32
#define SYS_WORD_ID 4
#define NONE 1
#define WORD_MESSAGE_LEN  USER_WORD_LEN+DATA_SIZE+TIME_SIZE

//cells dict define
#define NATIVE_TITLE_MESSAGE_SIZE       13
#define NEW_TITLE_MESSAGE_SIZE          12
#define CELLS_WORD_LENGTH               32
#define CELLS_WORD_MESSAGE_LEN          2

//option define
#define CLOSE_FUZZY_SEARCH      0x00000001
#define CLOSE_HALF_MATCH_SEARCH 0x00000002



using namespace std;


class UdbStr
{
public:
    UdbStr() : pStr(NULL), uSize(0), bIsUnicode(false) {};
    void* pStr;
    unsigned int uSize;
    bool bIsUnicode;
};



template <class TString>
class CandItem
{
public:
    CandItem()
    {
        candStr.clear();
        fWeight = 0.0;
    };

    CandItem(TString str, float weight)
    {
        candStr = str;
        fWeight = weight;
    };

    inline float getWeight() { return fWeight; };
    inline void setWeight(float weight) { fWeight = weight; };
    inline TString& getCand() { return candStr; };
    inline void set(TString& str, float weight)
    {
        candStr = str;
        fWeight = weight;
    };

    inline bool less (const CandItem& comp) const
    {
        return fWeight < comp.fWeight;
    }
    inline bool operator< (const CandItem& comp) const
    {
        return fWeight < comp.fWeight;
    }

private:
    TString candStr;
    float fWeight;
};

template <class TString>
class bigramDataNode
{
public:
    bigramDataNode()
    {
        word.clear();
        freq = 0;
    }

    TString word;
    unsigned char freq;

    void reset()
    {
        word.clear();
        freq = 0;
    }
};

template <class TString, class TChar>
class PatriciaTrieNode {
public:
    vector<bigramDataNode<TString> > biData;
    vector<PatriciaTrieNode<TString, TChar> > children;

    PatriciaTrieNode() : data(0), currentIndex(0)
    {
        key.clear();
        flag = 0;
        children.clear();
        biData.clear();
        times = 0;
    }

    PatriciaTrieNode(TString inKeyStr) : data(0), currentIndex(0)
    {
        key = inKeyStr;
        flag = 0;
        children.clear();
        biData.clear();
        times = 0;
    }

    void reset()
    {
        data = 0;
        currentIndex = 0;
        key.clear();
        flag = 0;
        children.clear();
        biData.clear();
        times = 0;
    }

    int serializeNode(unsigned char* pBuf, unsigned int* pCurrentIdx, unsigned int* parentOffset, queue< pair<PatriciaTrieNode<TString, TChar> * , unsigned int> >& queue, bool isUserDict = false);

    bool unSerializeNode(unsigned char* pBuf, unsigned int* pCurrentIdx, unsigned int userDictToken, bool isUserDict = false);
    inline bool isUnicode(void) { return flag & 0x02; };
    inline bool isTerminal(void) { return flag & 0x01; };
    inline void setTerminal(bool bValue)
    {
        flag = (bValue) ? (flag | 0x01) : (flag & 0xFE);
    };
    inline bool isTypeOne(void) { return flag & 0x04; };
    inline void setTypeOne(bool bValue)
    {
        flag = (bValue) ? (flag | 0x04) : (flag & 0xFB);
    };
    inline bool isTypeTwo(void) { return flag & 0x08; };
    inline void setTypeTwo(bool bValue)
    {
        flag = (bValue) ? (flag | 0x08) : (flag & 0xF7);
    };
    inline bool isMatchType(unsigned int type)
    {
        if (!type || ((type == TYPE_ONE) && isTypeOne()) || ((type == TYPE_TWO) && isTypeTwo()))
            return true;
        return false;
    };
    //inline bool isPrefix(void) { return flag & 0x04; };
    //inline void setPrefix(bool bValue)
    //{
    //    flag = (bValue) ? (flag | 0x04) : (flag & 0xFB);
    //};
    inline void setByteData(unsigned char* pBuf, unsigned int* pIndex, unsigned char data)
    {
        *(pBuf + *pIndex) = data;
        *pIndex += 1;
    };
    inline unsigned char getFlag() { return flag; };
    inline unsigned int getData(void) { return data; };
    inline void setData(unsigned int value) { data = value; };
    inline void setToken(unsigned short value) { data = value; };
    inline unsigned int getCurIdx(void) { return currentIndex; };
    inline void setKeyStr(TString inStr) { key = inStr; };
    inline unsigned char getKeyStrLen(void) { return key.size(); };
    inline unsigned int getKeyStrBufLen(void) { return getKeyStrLen() * (isUnicode() ? 2 : 1); };
    inline TChar getKeychar(unsigned int idx) { return key.at(idx); };
    inline TString& getKeyStr(void) { return key; };
    inline TString getKeySubStr(unsigned int iStart, unsigned int iEnd) { return key.substr(iStart, iEnd); };
    inline TString getKeySubStr(unsigned int idx) { return key.substr(idx); };
    inline void appendStr(TString& suffixStr) { key.append(suffixStr); };
    inline unsigned int getTimes(void) { return times; };
    inline void setTimes(unsigned int value) { times = value; };
    inline void setIsUnicode(bool isUnicodeType)
    {
        if (isUnicodeType)
        {
            flag = flag | 0x02;
        }
        else
        {
            flag = flag & 0xfd;
        }
    };

private:
    unsigned char flag;
    unsigned int data;
    unsigned int times;

    unsigned int currentIndex; // not serialized.
    TString key;

    inline unsigned int readU32(unsigned char* pBuf)
    {
        return (((((*pBuf << 8) | *(pBuf + 1)) << 8) | *(pBuf + 2)) << 8) | *(pBuf + 3);
    }

    inline unsigned int readU24(unsigned char* pBuf)
    {
        return (((*pBuf << 8) | *(pBuf + 1)) << 8) | *(pBuf + 2);
    }

    inline unsigned char getSerialKeyLen(unsigned char* pBuf) { return (*pBuf >> 4) & 0x0F; };
};

template <class TString, class TChar>
class CandNode
{
public:
    CandNode() : node(NULL), childIndex(0) {};
    CandNode(PatriciaTrieNode<TString, TChar>* pNode, unsigned int uIdx) : node(pNode), childIndex(uIdx) {};

    PatriciaTrieNode<TString, TChar>* node;
    unsigned int childIndex;
};

template <class TString, class TChar>
class MultiPhrase
{
public:
    list<MultiPhrase<TString, TChar> > subList;

    MultiPhrase() : pNode(0), uConsume(0), fWeight(1.00)
    {
        subList.clear();
    };

    inline void setData(PatriciaTrieNode<TString, TChar>* pCandNode, unsigned int uConsumeNum)
    {
        pNode = pCandNode;
        uConsume = uConsumeNum;
    };

    MultiPhrase& operator= (MultiPhrase& phraseCand)
    {
        pNode = phraseCand.pNode;
        uConsume = phraseCand.uConsume;
        return *this;
    };

    void reset()
    {
        subList.clear();
        pNode = 0;
        uConsume = 0;
        fWeight = 1.00;
    };

    inline PatriciaTrieNode<TString, TChar>* getNode() { return pNode; };
    inline unsigned char getConsumeNum() { return uConsume; };
    inline float getWeight() { return fWeight; };
    inline void setWeight(float weight) { fWeight = weight; };
    inline void setMiniWeight() { fWeight = MINI_WEIGHT; };
    inline void setNegaWeight() { fWeight = -1; };

private:
    PatriciaTrieNode<TString, TChar>* pNode;
    unsigned char uConsume;
    float fWeight;
};

template <class TString>
class QuotePhrase
{
public:
    list<QuotePhrase<TString> > subList;

    QuotePhrase() : mP(0)
    {
        mKey.clear();
        subList.clear();
    };

    inline void setData(TString str, int p)
    {
        mKey = str;
        mP = p;
    };

    QuotePhrase& operator= (QuotePhrase& phraseCand)
    {
        mKey = phraseCand.mKey;
        mP = phraseCand.mP;
        return *this;
    };

    void reset()
    {
        subList.clear();
        mKey = "";
        mP = 0;
    };

    inline int getP() { return mP; };
    inline TString& getKey() { return mKey; };
    inline void setKey(TString& str){ mKey = str; }
    inline void setP(int p){ mP = p; }

private:
    TString mKey;
    int mP;
};

template <class TString>
class DistSerialPhrase
{
public:
    DistSerialPhrase<TString>() : nodeId(0), uConsume(0)
    {
        reString.clear();
        dist.clear();
    };

    inline void setData(unsigned int nodeCandId, unsigned int uConsumeNum)
    {
        nodeId = nodeCandId;
        uConsume = uConsumeNum;
    };

    DistSerialPhrase<TString>& operator= (DistSerialPhrase<TString>& phraseCand)
    {
        nodeId = phraseCand.nodeId;
        uConsume = phraseCand.uConsume;
        reString = phraseCand.getReString();
        setDist(phraseCand.getDist());
        return *this;
    };

    void reset()
    {
        nodeId = 0;
        uConsume = 0;
        reString.clear();
        dist.clear();
    };

    inline void setReString(TString reStr) { reString = reStr; };
    inline void insertReString(TString reStr) { reString.insert(0, reStr); };
    inline TString getReString() { return reString; };
    inline vector<unsigned char>& getDist() { return dist; };
    inline void setDist(vector<unsigned char>& refDist) { dist = refDist; };
    inline void pushDistValue(unsigned char uValue) { dist.push_back(uValue); };

    inline unsigned int  getNodeId() { return nodeId; };
    inline unsigned char getConsumeNum() { return uConsume; };

private:
    unsigned int nodeId;
    unsigned char uConsume;
    TString reString;
    vector<unsigned char> dist;
};

template <class TString>
class MultiSerialPhrase
{
public:
    list<MultiSerialPhrase<TString> > subListSerial;

    MultiSerialPhrase<TString>() : nodeId(0), fuzzyCnt(0), uConsume(0), fWeight(1.00), nodeFlag(0), bRewarded(false)
    {
        subListSerial.clear();
    };

    inline void setData(unsigned int nodeCandId, unsigned int uConsumeNum)
    {
        nodeId = nodeCandId;
        uConsume = uConsumeNum;
    };

    MultiSerialPhrase<TString>& operator= (MultiSerialPhrase<TString>& phraseCand)
    {
        nodeId = phraseCand.nodeId;
        fuzzyCnt = phraseCand.fuzzyCnt;
        uConsume = phraseCand.uConsume;
        fWeight = phraseCand.getWeight();
        nodeFlag = phraseCand.getNodeFlag();
        reString = phraseCand.getReString();
        bRewarded = phraseCand.getRewardState();
        return *this;
    };

    void reset()
    {
        subListSerial.clear();
        nodeId = 0;
        fuzzyCnt = 0;
        uConsume = 0;
        fWeight = 1.00;
        nodeFlag = 0;
        reString.clear();
        bRewarded = false;
    };

    void biReward(float fValue)
    {
        if (!bRewarded)
        {
            bRewarded = true;
            fWeight *= fValue;
        }
    }

    inline unsigned int  getNodeId() { return nodeId; };
    inline void setNodeId(unsigned int inNodeId) { nodeId = inNodeId; };
    inline unsigned int  getFuzzyCnt() { return fuzzyCnt; };
    inline void setFuzzyCnt(unsigned int inFuzzyCnt) { fuzzyCnt = inFuzzyCnt; };
    inline unsigned char getConsumeNum() { return uConsume; };
    inline float getWeight() { return fWeight; };
    inline float getUseCount() { return fWeight; }; // use weight to hold udb used count num.
    inline void setWeight(float weight) { fWeight = weight; };
    inline unsigned char getNodeFlag() { return nodeFlag; };
    inline void setNodeFlag(unsigned char inNodeFlag) { nodeFlag = inNodeFlag; };
    inline void setMiniWeight() { fWeight = MINI_WEIGHT; };
    inline bool isMiniWeight() { return fWeight == (float)MINI_WEIGHT; };
    inline void setNegaWeight() { fWeight = -1; };
    inline bool getRewardStatus() { return bRewarded; };

    inline void setReString(TString reStr) { reString = reStr; };
    inline void insertReString(TString reStr) { reString = reStr.append(reString); };
    inline TString getReString() { return reString; };

private:
    bool bRewarded;
    unsigned int nodeId;
    unsigned int fuzzyCnt;
    unsigned char uConsume;
    TString reString;
    float fWeight;
    unsigned char nodeFlag;
};

template <class TString>
class pairTermNode
{
public:
    pairTermNode()
    {
        reStr.clear();
        mIdx = 0;
    };

    pairTermNode(unsigned int idx ,TString strt)
    {
        reStr = strt;
        mIdx = idx;
    };

    inline unsigned int getIdx() { return mIdx; };
    inline unsigned int getNodeIdx() { return (mIdx & 0xFFFFFF); };
    inline void setIdx(unsigned int idx) { mIdx = idx; };
    inline TString getReStr() { return reStr; };
    inline void setReStr(TString str) { reStr = str; };

    bool operator< (const pairTermNode& comp) const
    {
        return mIdx < comp.mIdx;
    };

private:
    TString reStr;
    unsigned int mIdx;
};

template <class TString>
class DistSession
{
public:
    DistSession()
    {
        distList.clear();
        distHistory.clear();
    };

    inline void reset()
    {
        distList.clear();
        distHistory.clear();
    };

    inline void pushDistPhrase(DistSerialPhrase<TString>& distPhrase) { distList.push_back(distPhrase); };
    inline void resetDistList() { distList.clear(); };
    inline bool isStart() { return !distHistory.size(); }
    inline void pushDistHistory()
    {
        if (distList.size() == 0 && !distHistory.empty())
        {
            distList = distHistory.back();
        }
        distHistory.push_back(distList);
    };

    list<DistSerialPhrase<TString> >& popDistHistory()
    {
        distList = distHistory.back();
        distHistory.pop_back();
        return distList;
    };

    list<DistSerialPhrase<TString> >& distHistoryBack()
    {
        return distHistory.back();
    };

private:
    list<DistSerialPhrase<TString> > distList;
    list<list<DistSerialPhrase<TString> > > distHistory;
};

template <class TString, class TChar>
class PatriciaTrie
{
protected:
    PatriciaTrieNode<TString, TChar> root;
    list<MultiPhrase<TString, TChar> > retList;
    list<MultiSerialPhrase<TString> > retSerialList;
    list<QuotePhrase<TString> > retQuoteList;
    MultiPhrase<TString, TChar> cand;
    MultiSerialPhrase<TString> candSerial;
    MultiSerialPhrase<TString> fullMatchCandSerial;
    MultiSerialPhrase<TString> halfMatchCandSerial;
    MultiSerialPhrase<TString> fuzzyFullMatchCandSerial;
    MultiSerialPhrase<TString> fuzzyHalfMatchCandSerial;
    MultiSerialPhrase<TString> phraseFullMatchCandSerial;
    MultiSerialPhrase<TString> phraseHalfMatchCandSerial;
    MultiSerialPhrase<TString> phraseFuzzyFullMatchCandSerial;
    MultiSerialPhrase<TString> phraseFuzzyHalfMatchCandSerial;
    DistSession<TString> distSess;

    PatriciaTrieNode<TString, TChar>* processFind(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned int type = 0);
    unsigned int processFindInSerial(unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned int type = 0);
    PatriciaTrieNode<TString, TChar>* processFindAsQuoteFilter(PatriciaTrieNode<TString, TChar>& currNode, TString key, TString reStr, unsigned int type = 0);
    unsigned int processFindInSerialAsQuoteFilter(unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned int type = 0);
    PatriciaTrieNode<TString, TChar>* processFindAsHead(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned char* pConsume);
    unsigned int processFindInSerialAsHead(unsigned char* pBuf, unsigned int uNodeIdx, TString key);
    bool processFindAsMulti(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned char* pConsume, unsigned int type = 0);
    bool processFindInSerialAsMulti(unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned char* pConsume, TString reStr, TString& lastKey, unsigned int type = 0);
    bool processFindInSerialAsMulti(Associate<TChar>& assList,unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned char* pConsume, TString reStr, int dCurFreq, unsigned char uKeyLen,unsigned int type = 0);
    bool processFindInSerialAsMulti_Ext(Associate<TChar>& assList,unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned char uKeyLen, TString reStr, TString& lastKey, unsigned int type = 0);
    bool processFindInSerialAsDist(unsigned char* pBuf, unsigned int uNodeIdx, TString key,
        vector<unsigned char> prevDist, TString& prevStr, unsigned char consume, unsigned int uPrevEqualCnt, unsigned int type = 0);
    char processFindInSerialAsDistEx(unsigned char* pBuf, unsigned int uNodeIdx, TString key,
        vector<unsigned char> prevDist, TString& prevStr, unsigned char consume, unsigned int uPrevEqualCnt, unsigned int type = 0);
    bool processPutCandToSerialList(MultiSerialPhrase<TString>& phraseCand);
    bool processFindInSerialAsTrack(unsigned char* pBuf, unsigned int uNodeIdx, TString& prevStr, vector<TrackChar<TChar> >& allTrackCharVec,
        vector<TrackChar<TChar> >& allTrustCharVec, list<CandItem<TString> >& cand);
    bool processFindInSerialAsTrackCorrection(unsigned char* pBuf, unsigned int uNodeIdx, TString& prevStr, vector<TrackChar<TChar> >& allTrackCharVec,
        vector<TrackChar<TChar> >& allTrustCharVec, list<CandItem<TString> >& cand);

    bool processDelete(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned int type = 0);
    bool processDelete(PatriciaTrieNode<TString, TChar>* pParent, int idx);
    void processInsert(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned int value, list<bigramDataNode<TString> >& lstBigramData, unsigned int type = 0);
    bool constructBiAfterSerialize(unsigned char* pBuf, unsigned int pCurIdx, PatriciaTrieNode<TString, TChar>& node);
    void serializeChild(unsigned char* pBuf, unsigned int* pCurOffset, queue<pair<PatriciaTrieNode<TString, TChar> *, unsigned int> >& parentQueue, bool isUserDict = false);

    void getAllTermNodeIdx(unsigned char* pBuf, unsigned int parentIdx, list<pairTermNode<TString> >& lst, unsigned int count, unsigned int type = 0);

    inline unsigned char getSerialKeyLen(unsigned char* pNodeBuf)
    {
        return ((*pNodeBuf) >> 4) & 0x0F;
    };
    inline unsigned char getSerialBiDataLen(unsigned char* pNodeBuf, bool isUserDict = false)
    {
        return *(getSerialBiDataBuf(pNodeBuf, isUserDict) - 1);
    };
    inline unsigned char* getSerialBiDataBuf(unsigned char* pNodeBuf, bool isUserDict = false)
    {
        return pNodeBuf + ((*pNodeBuf & 0x1) ? 3 : 2) + getSerialKeyLen(pNodeBuf) * (getSerialIsUnicode(pNodeBuf) ? 2 : 1);
    };
    inline unsigned char getSerialChildLen(unsigned char* pNodeBuf, bool isUserDict = false)
    {
        return *(getSerialBiDataBuf(pNodeBuf, isUserDict) + getSerialBiDataLen(pNodeBuf, isUserDict) * 4);
    };
    inline unsigned char* getSerialChildBuf(unsigned char* pNodeBuf)
    {
        return getSerialBiDataBuf(pNodeBuf) + getSerialBiDataLen(pNodeBuf) * 4;
    };
    inline unsigned char* getSerialNextNode(unsigned char* pNodeBuf)
    {
        return pNodeBuf + 1/* flag */
            + ((*pNodeBuf & 0x1) ? 1 : 0) /* data */
            + getSerialKeyLen(pNodeBuf) * (getSerialIsUnicode(pNodeBuf) ? 2 : 1) /* key */
            + 1 /* bigram size */
            + getSerialBiDataLen(pNodeBuf) * 4 /* bigram */
            + 1 /* child size*/
            + getSerialOffSetSize(pNodeBuf)/* child offset */;
    };

    inline unsigned char* getSerialOffSet(unsigned char* pNodeBuf)
    {
        return pNodeBuf + 1/* flag */
            + ((*pNodeBuf & 0x1) ? 1 : 0) /* data */
            + getSerialKeyLen(pNodeBuf) * (getSerialIsUnicode(pNodeBuf) ? 2 : 1) /* key */
            + 1 /* bigram size */
            + getSerialBiDataLen(pNodeBuf) * 4 /* bigram */
            + 1 /* child size*/;
            //+ getSerialChildLen(pNodeBuf) > 0 ? 3 : 0 /* child offset */;
    };
    inline unsigned int getSerialOffSetSize(unsigned char* pNodeBuf, bool isUserDict =false)
    {
        return (getSerialChildLen(pNodeBuf, isUserDict) > 0 ? 3 : 0);
    };

    inline unsigned int getSerialFirstChildIdx(unsigned char* pNodeBuf)
    {
        return readU24(getSerialOffSet(pNodeBuf));
    };

    TChar getSerialKeyChar(unsigned char* pNodeBuf, int iIndex)
    {
        int iOff = (*pNodeBuf & 0x1) ? 2 : 1;

        if (getSerialIsUnicode(pNodeBuf))
        {
            return (*(pNodeBuf + iOff + iIndex * 2) << 8) | *(pNodeBuf +iOff+1 + iIndex * 2);
        }
        else
        {
            return *(pNodeBuf + iOff + iIndex);
        }
    }

    inline TString getSerialKey(unsigned char* pNodeBuf, int iIndex)
    {
        TString str;
//        str.clear();

        for (int i = 0; i < getSerialKeyLen(pNodeBuf + iIndex); i++)
        {
            str += (TChar)getSerialKeyChar(pNodeBuf + iIndex , i);
        }

        return str;
    }

    inline bool isSerialMatchType(unsigned char nodeFlag, unsigned int type)
    {
        if (!type || ((type == TYPE_ONE) && (nodeFlag & 0x04)) || ((type == TYPE_TWO) && (nodeFlag & 0x08)))
            return true;
        return false;
    };

    inline bool isBaseCharInTrack(TChar baseChar, TrackChar<TChar>& trackChar)
    {
        return levenshteinDistance::compChar(baseChar, trackChar.getChar());
    };
    inline bool isBaseCharNearTrack(TChar baseChar, TrackChar<TChar>& trackChar)
    {
        return levenshteinDistance::compChar(baseChar, trackChar.getChar()) &&
            levenshteinDistance::compChar(baseChar, trackChar.getLeftChar()) &&
            levenshteinDistance::compChar(baseChar, trackChar.getRightChar());
    };

    inline float calculateUserPhraseWeight(float preCandWeight, int usedCount) { return preCandWeight + 0.3 * usedCount; };
    inline float getFuzzyDiscount(MultiSerialPhrase<TString>& candPhrase)
    {
        int cnt = candPhrase.getFuzzyCnt();
        //cnt -= (cnt - 1 < 0) ? 0 : 1;
        cnt = (cnt < 0) ? 0 : cnt;
        return (float)((10 - cnt >= 0) ? (10 - cnt) : 0) / 10.0;
    };

    inline unsigned char getSerialNodeFlag(unsigned char* pNodeBuf) { return (*(pNodeBuf) & 0x0F); };
    inline unsigned char getSerialNodeFreq(unsigned char* pNodeBuf) { return getSerialIsTerminal(pNodeBuf) ? *(pNodeBuf + 1) : 0; };
    inline bool getSerialIsUnicode(unsigned char* pNodeBuf) { return *(pNodeBuf) & 0x02; }
    inline bool getSerialIsTerminal(unsigned char* pNodeBuf) { return *(pNodeBuf) & 0x01; };
    inline unsigned char* getSerialStrBuf(unsigned char* pNodeBuf) { return pNodeBuf + (getSerialIsTerminal(pNodeBuf) ? 2 : 1); };

public:
    typedef enum _predict_type_
    {
        PRECISE_MATCH,
        QUOTE_FILTER,
        AS_HEAD,
        AS_MULTI,
    }PREDICT_TYPE;
    unsigned int processFindInSerialExact(unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned int type = 0);

    unsigned short do_crc_table(unsigned char* ptr, int len)
    {
        unsigned int crc_ta[256] =
        {
            0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
            0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
            0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
            0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
            0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
            0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
            0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
            0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
            0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
            0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
            0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
            0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
            0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
            0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
            0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
            0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
            0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
            0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
            0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
            0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
            0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
            0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
            0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
            0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
            0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
            0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
            0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
            0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
            0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
            0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
            0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
            0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
        };
        unsigned short crc;
        unsigned char da;
        crc = 0;
        while (len-- != 0)
        {
            da = (unsigned short)(crc >> 8);
            crc <<= 8;
            crc ^= crc_ta[da ^ *ptr];
            ptr++;
        }
        return crc;
    };

    inline unsigned int getSerialNextNodeIdx(unsigned char* pNodeBuf, unsigned int idx, bool isUserDict = false)
    {
        return
            idx
            + 1 /* flag */
            + ((*(pNodeBuf + idx) & 0x1) ? 1 : 0) /* data */
            //+ (((*(pNodeBuf + idx) & 0x1) && isUserDict) ? 2 : 0)
            + getSerialKeyLen(pNodeBuf + idx) * (getSerialIsUnicode(pNodeBuf + idx) ? 2 : 1) /* key */
            + 1 /* bigram size */
            + getSerialBiDataLen(pNodeBuf + idx, isUserDict) * 4 /* bigram */
            + 1 /* child size*/
            + getSerialOffSetSize(pNodeBuf + idx, isUserDict)/* child offset */;
    };

    inline unsigned int readU32(unsigned char* pBuf)
    {
        return (((((*pBuf << 8) | *(pBuf + 1)) << 8) | *(pBuf + 2)) << 8) | *(pBuf + 3);
    }

    inline unsigned int readU24(unsigned char* pBuf)
    {
        return (((*pBuf << 8) | *(pBuf + 1)) << 8) | *(pBuf + 2);
    }

    inline unsigned short readU16(unsigned char* pBuf)
    {
        return ((*pBuf << 8) | *(pBuf + 1));
    }

    inline void writeU32(unsigned char* pBuf,unsigned int v)
    {
        unsigned char tmp = 0;
        tmp = (unsigned char)((v >> 24)&0x000000ff);
        *pBuf = tmp;
        tmp = (unsigned char)((v >> 16)&0x000000ff);
        *(pBuf+1) = tmp;
        tmp = (unsigned char)((v >> 8)&0x000000ff);
        *(pBuf+2) = tmp;
        tmp = (unsigned char)(v &0x000000ff);
        *(pBuf+3) = tmp;
    }

    inline void writeU16(unsigned char* pBuf,unsigned short v)
    {
        unsigned char tmp = 0;
        tmp = (unsigned char)((v >> 8)&0x00ff);
        *pBuf = tmp;
        tmp = (unsigned char)(v &0x00ff);
        *(pBuf+1) = tmp;
    }

    inline void del(TString key)
    {
        if (key.length() == 0)
        {
            return;
        }
        processDelete(root, key);
    }

    inline void del(PatriciaTrieNode<TString, TChar>* pParent, int idx)
    {
        if (!pParent)
        {
            return;
        }
        processDelete(pParent, idx);
    }

    inline void resetDistSession() { distSess.reset(); };

    MultiSerialPhrase<TString>& getFullMatchCandSerial()
    {
        return fullMatchCandSerial;
    };
    MultiSerialPhrase<TString>& getHalfMatchCandSerial()
    {
        return halfMatchCandSerial;
    };
    MultiSerialPhrase<TString>& getFuzzyFullMatchCandSerial()
    {
        return fuzzyFullMatchCandSerial;
    };
    MultiSerialPhrase<TString>& getFuzzyHalfMatchCandSerial()
    {
        return fuzzyHalfMatchCandSerial;
    };
    MultiSerialPhrase<TString>& getPhraseFullMatchCandSerial()
    {
        return phraseFullMatchCandSerial;
    };
    MultiSerialPhrase<TString>& getPhraseFuzzyFullMatchCandSerial()
    {
        return phraseFuzzyFullMatchCandSerial;
    };

    void clearAllMatchCandSerial()
    {
        fullMatchCandSerial.reset();
        halfMatchCandSerial.reset();
        fuzzyFullMatchCandSerial.reset();
        fuzzyHalfMatchCandSerial.reset();
        phraseFullMatchCandSerial.reset();
        phraseHalfMatchCandSerial.reset();
        phraseFuzzyFullMatchCandSerial.reset();
    };
    int fuzzyCompChar(AssociateNode<TChar>& assNode,TChar childChar)
    {
        typename list<CharNode<TChar> >::iterator itor;
        int i = 0;

        for (itor = assNode.charList.begin(); itor != assNode.charList.end(); itor++)
        {
            if (levenshteinDistance::compChar(itor->getChar(),childChar)==0)
            {
                return i;
            }
            i++;
        }
        return -1;
    }

    bool predictSearchWordInCandSerial(int nodeId, MultiSerialPhrase<TString>& parent)
    {
        if (parent.subListSerial.size() <= 0)
        {
            return true;
        }

        typename list<MultiSerialPhrase<TString> >::iterator itor;
        for (itor = parent.subListSerial.begin(); itor != parent.subListSerial.end(); itor++)
        {
            if (nodeId == itor->getNodeId())
            {
                return false;
            }
        }

        return true;
    }

    void predictInStr(TString key, int iLevel, unsigned int type = 0);
    void predictList(TString key, MultiPhrase<TString, TChar>& parent, unsigned int type = 0);

    bool predictTrackInSerial(unsigned char* pBuf, vector<TrackChar<TChar> >& allTrackCharVec,
        vector<TrackChar<TChar> >& allTrustCharVec, vector<TString>& cand);
    bool predictListInSerial(unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent,
        MultiSerialPhrase<TString>& prevCand, TString& lastKey, unsigned char uKeyLen = 0);
    void predictListInSerial_Ext(Associate<TChar>& assList,unsigned char* pBuf, TString key,int dCurFreq,
        unsigned char* adjustFreqBuf, MultiSerialPhrase<TString>& prevCand, TString& lastKey, unsigned char uKeyLen = 0);
    void predictListInSerial_Ext(unsigned char* pBuf, TString key, int dCurFreq,
        unsigned char* adjustFreqBuf, TString& lastKey, unsigned char uKeyLen=0);
    bool predictAssociationSearch(unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent,
        MultiSerialPhrase<TString>& prevCand, TString& lastKey, unsigned char uKeyLen=0);
    bool predictAssociationSearch(Associate<TChar>& assList,unsigned char* pBuf, TString key,
        MultiSerialPhrase<TString>& parent, MultiSerialPhrase<TString>& prevCand, TString& lastKey, unsigned char uKeyLen=0);
    int predictAssociationCompare(Associate<TChar>& assList, TString key, TString element, unsigned char uKeyLen);
    int predictAssociationCompare(TString key, TString element);
    int predictUDBCompare(Associate<TChar>& assList, TString& key, UdbStr& element, unsigned char uKeyLen = 0);
    int predictUDBCompare(TString& key, UdbStr& element);
    void predictSearchUDB(Associate<TChar>& assList,TString key,  MultiSerialPhrase<TString>& parent,list<MultiSerialPhrase<TString> >& userCandList);
    void predictSearchUDB(Associate<TChar>& assList,TString key,  MultiSerialPhrase<TString>& parent,unsigned char* userDataBuf);
    int predictSearchWordInUDB(int nodeId,unsigned char* sysWordFreq);
    void predictSearchCells(Associate<TChar>& assList,TString key,  MultiSerialPhrase<TString>& parent,unsigned char* serialCellsBuf);

    void getSerialNodeStr(unsigned char* pBuf, unsigned int uTargetIdx, TString& nodeStr);
    void getSerialNodeStr(unsigned char* pBuf, unsigned int uCurrentIdx, unsigned int uTargetIdx, TString& nodeStr);
    void storePredictSerialWeightListEx(unsigned char* pBuf , MultiSerialPhrase<TString>& phrase, int iLevel,
        TString prevStr, float prevWeight, float dPhraseWeight, list<CandItem<TString> >& lst, unsigned int type = 0);
    void storePredictSerialWeightList(unsigned char* pBuf , MultiSerialPhrase<TString>& phrase, int iLevel,
        TString prevStr, float prevWeight, float dPhraseWeight, list<CandItem<TString> >& lst, unsigned int type = 0)
    {
        TString candStr;
        float fWeight;
        PatriciaTrieNode<TString, TChar> node;
        list<pairTermNode<TString> > termNodeList;
        pairTermNode<TString> termNodePair;
        typename list<CandItem<TString> >::iterator itor;

        if (phrase.subListSerial.size() == 0)
        {
            if (iLevel == 0)
            {
                return;
            }

            for (itor = lst.begin(); itor != lst.end(); itor++)
            {
                if (!prevStr.compare(itor->getCand()))
                {
                    if (dPhraseWeight * prevWeight / (3 + (iLevel * LEVEL_WEIGHT)) > itor->getWeight())
                    {
                        itor->setWeight(dPhraseWeight * prevWeight / (3 + (iLevel * LEVEL_WEIGHT)));
                    }
                    return;
                }
            }

            CandItem<TString> cand(prevStr, dPhraseWeight * prevWeight / (3 + (iLevel * LEVEL_WEIGHT)));
            lst.push_back(cand);
            return;
        }

        while (phrase.subListSerial.size() > 0)
        {
            MultiSerialPhrase<TString>& candPhrase = phrase.subListSerial.front();

            if (candPhrase.getWeight() < 0)
            {
                phrase.subListSerial.pop_front();
                break;
            }

            // handle all node.
            if (candPhrase.isMiniWeight())
            {
                getAllTermNodeIdx(pBuf, candPhrase.getNodeId(), termNodeList, 10, type);

                if (termNodeList.size() > 0)
                {
                    termNodeList.sort();
                    //termNodeList.reverse();
                }

                while (termNodeList.size() > 0)
                {
                    termNodePair = termNodeList.front();
                    unsigned int nodeData = termNodePair.getIdx();
                    termNodeList.pop_front();

                    fWeight = prevWeight;
                    candStr = prevStr;
                    //getSerialNodeStr(pBuf, nodeData & 0xFFFFFF, candStr);
                    candStr = candPhrase.getReString() + termNodePair.getReStr();// +getSerialKey(pBuf, nodeData & 0xFFFFFF);
                    candStr.push_back((TChar)0x20);

                    fWeight += ((nodeData & 0xFF000000) >> 24) * candPhrase.getWeight();

                    CandItem<TString> cand(candStr, dPhraseWeight * fWeight / (4 + (iLevel * LEVEL_WEIGHT)));
                    lst.push_back(cand);
                }

                phrase.subListSerial.pop_front();
                continue;
            }
            else
            {
                candStr = prevStr;
                //getSerialNodeStr(pBuf, candPhrase.getNodeId(), candStr);
                candStr += candPhrase.getReString() ;
                candStr.push_back((TChar)0x20);

                if (candPhrase.subListSerial.size() == 0 && candPhrase.getWeight() >= 1.0)
                {
                    if (candPhrase.getNodeId() == 0)
                    {
                        CandItem<TString> cand(candStr, dPhraseWeight * candPhrase.getWeight() * getFuzzyDiscount(candPhrase) / (iLevel + 1));
                        lst.push_back(cand);
                    }
                    else
                    {
                        CandItem<TString> cand(candStr, dPhraseWeight * getSerialNodeFreq(pBuf + candPhrase.getNodeId()) *
                            candPhrase.getWeight() * getFuzzyDiscount(candPhrase) / (iLevel + 1));
                        lst.push_back(cand);
                    }

                    phrase.subListSerial.pop_front();
                    continue;
                }
                else
                {
                    fWeight = prevWeight;
                    fWeight += getSerialNodeFreq(pBuf + candPhrase.getNodeId()) * candPhrase.getWeight();
                }
            }

            storePredictSerialWeightList(pBuf , candPhrase, iLevel + 1, candStr, fWeight, dPhraseWeight, lst, type);
            phrase.subListSerial.pop_front();
        }
    }

    void predictDistInSerial(unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent, unsigned int uPrevIdx, unsigned int type = 0);
    void predictDistInSerialEx(unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent, unsigned int uPrevIdx, unsigned int type = 0);

    void PredictInSerial(unsigned char* pBuf, TString key, list<CandItem<TString> >& candList, float dCurFreq)
    {
        TString nulStr, lastKey;
        MultiSerialPhrase<TString> multiPhrase;

        nulStr.clear();

        fullMatchCandSerial.reset();

        predictListInSerial(pBuf, key, fullMatchCandSerial, multiPhrase, lastKey);
        storePredictSerialWeightList(pBuf, fullMatchCandSerial, 0, nulStr, 0.0, dCurFreq, candList);

        //if (dCurFreq >= 1.0)
        //{
        //    //clock_t tStart = clock();
        //    //predictDistInSerial(pBuf, key, candSerial, 0);
        //    predictDistInSerialEx(pBuf, key, candSerial, 0);
        //  distSess.pushDistHistory();
        //    //clock_t tEnd = clock();
        //    //printf("Using time: %d\n", tEnd - tStart);
        //  storePredictSerialWeightListEx(pBuf, candSerial, 0, nulStr, 0.0, dCurFreq, candList);
        //}
    }

    void PredictInSerial_Ext(Associate<TChar>& assList,unsigned char* pBuf, unsigned char* userBuf, unsigned char* serialCellsBuf,  unsigned char* adjustFreqBuf,TString key, list<CandItem<TString> >& candList,unsigned int searchOption)
    {
        TString nulStr, lastKey;
        MultiSerialPhrase<TString> multiPhrase;
        int i = 0;

        nulStr.clear();

        predictSearchUDB(assList,key,multiPhrase,userBuf+TOKEN_SIZE);

        if (serialCellsBuf)
        {
            predictSearchCells(assList,key,multiPhrase,serialCellsBuf);
        }
        if (key.size() > 1)
        {
            if (searchOption & CLOSE_FUZZY_SEARCH)
            {
                predictListInSerial_Ext(pBuf, key, 0, adjustFreqBuf, lastKey);
                return;
            }

            predictListInSerial_Ext(assList, pBuf, key, 0, adjustFreqBuf, multiPhrase, lastKey);
            if (key.size() > 2)
            {
                //for (i = 0; i < key.size() - 1; i++)
                //{
                //  TString curStr = key;
                //  curStr.replace(i, 1, 1, 0xff);
                //  predictListInSerial_Ext(pBuf, curStr, 1, adjustFreqBuf, lastKey);
                //}

                for (i = 0; i < key.size() - 1; i++)
                {
                    TString curStr = key;
                    TChar tempChar = curStr.at(i);
                    if (curStr.at(i) == curStr.at(i + 1))
                    {
                        continue;
                    }
                    curStr.replace(i, 1, 1, curStr.at(i + 1));
                    curStr.replace(i + 1, 1, 1, tempChar);
                    predictListInSerial_Ext(pBuf, curStr, 1, adjustFreqBuf, lastKey);
                }

                for (i = 0; i < key.size() - 1; i++)
                {
                    TString curStr = key;
                    if (curStr.at(i) == curStr.at(i + 1))
                    {
                        continue;
                    }

                    curStr.erase(i, 1);

                    predictListInSerial_Ext(pBuf, curStr, 1, adjustFreqBuf, lastKey);
                }
                for (i = 0; i < key.size(); i++)
                {
                    TString curStr = key;

                    curStr.insert(i, 1, 0xff);

                    predictListInSerial_Ext(pBuf, curStr, 1, adjustFreqBuf, lastKey);
                }
            }

        }
        else
        {
            predictListInSerial_Ext(pBuf, key,0 , adjustFreqBuf,lastKey);
        }

//      storePredictSerialWeightList(pBuf, fullMatchCandSerial, 0, nulStr, 0.0, dCurFreq, candList);
    }

    void predictAssociateInSerial(unsigned char* pBuf, TString& preStr, Associate<TChar>& assList, float dCurFreq)
    {
        int iCount = 0;
        TString curStr;
        list<CandItem<TString> > candList;
        AssociateNode<TChar> assNode = assList.getAssociateList().at(0);

        // default delete here.
        assList.getAssociateList().erase(assList.getAssociateList().begin());

        while (assNode.charList.size() > 0)
        {
            curStr = preStr;
            curStr.push_back(assNode.charList.front().getChar());
            assNode.charList.pop_front();

            if (assList.getAssociateList().size() > 0)
            {
                predictAssociateInSerial(pBuf, curStr, assList, dCurFreq);
            }
            else
            {
                candList.clear();
                candSerial.reset();
                predictListInSerial(pBuf, curStr, candSerial, 0);
                storePredictSerialWeightList(pBuf, candSerial, 0, "", 0.0, dCurFreq, candList);

                candList.sort();
                candList.reverse();

                while (candList.size() > 0)
                {
                    CandItem<TString> cand = candList.front();
                    candList.pop_front();
                    iCount++;

                    if (iCount >= 20)
                    {
                        break;
                    }
                }
            }
        }
    }

    void findBiDataExact(unsigned char* pBuf, TString key, vector<TString>& candVec)
    {
        PatriciaTrieNode<TString, TChar> node;
        bigramDataNode<TString> biDataNode;
        vector<bigramDataNode<TString> > biData;
        unsigned int nodeIdx = processFindInSerialExact(pBuf, 0, key);
        unsigned char* biDataBuf = getSerialBiDataBuf(pBuf + nodeIdx);
        unsigned int i, j, size = getSerialBiDataLen(pBuf + nodeIdx);

        if (size <= 0)
        {
            return;
        }
        else if (size > MAX_BI_DATA_PREDICT)
        {
            size = MAX_BI_DATA_PREDICT;
        }

        for (i = 0; i < size; i++)
        {
            biDataNode.reset();
            nodeIdx = readU24(biDataBuf);
            biDataBuf += 3;
            getSerialNodeStr(pBuf, nodeIdx, biDataNode.word);
            biDataNode.freq = *(biDataBuf);
            biDataBuf += 1;

            j = 0;
            while (j < biData.size())
            {
                if (biData.at(j).freq < biDataNode.freq)
                {
                    break;
                }
                j++;
            }
            biData.insert(biData.begin() + j, biDataNode);
            candVec.insert(candVec.begin() + j, biDataNode.word);
        }
    }

    void findBiDataExact_Ext(unsigned char* pBuf, TString key,MultiSerialPhrase<TString>& parent)
    {
        PatriciaTrieNode<TString, TChar> node;
        bigramDataNode<TString> biDataNode;
        vector<bigramDataNode<TString> > biData;
        unsigned int nodeIdx = processFindInSerialExact(pBuf, 0, key);
        unsigned char* biDataBuf = getSerialBiDataBuf(pBuf + nodeIdx);
        unsigned int i, j, size = getSerialBiDataLen(pBuf + nodeIdx);

        if (size <= 0)
        {
            return;
        }
        else if (size > MAX_BI_DATA_PREDICT)
        {
            size = MAX_BI_DATA_PREDICT;
        }

        for (i = 0; i < size; i++)
        {
            MultiSerialPhrase<TString>  candPhrase;
            biDataNode.reset();
            nodeIdx = readU24(biDataBuf);
            biDataBuf += 3;
            getSerialNodeStr(pBuf, nodeIdx, biDataNode.word);
            biDataNode.freq = *(biDataBuf);
            biDataBuf += 1;
            j = 0;
            while (j < biData.size())
            {
                if (biData.at(j).freq < biDataNode.freq)
                {
                    break;
                }
                j++;
            }
            biData.insert(biData.begin() + j, biDataNode);
            candPhrase.setData(nodeIdx,(biDataNode.word).size());
            candPhrase.setReString(biDataNode.word);
            candPhrase.setNodeFlag(getSerialNodeFlag(pBuf + nodeIdx));
            parent.subListSerial.push_back(candPhrase);
        }
    }

    void predictInStrInSerial(unsigned char* pBuf, TString key, int iLevel, unsigned int type = 0)
    {
        unsigned char uConsume = 0;
        bool bFindCand = false;
        TString nulStr;
        MultiSerialPhrase<TString> candPhrase;

        retSerialList.clear();
        nulStr.clear();

        bFindCand = processFindInSerialAsMulti(pBuf, 0, key, &uConsume, nulStr, type);
        list<MultiSerialPhrase<TString> > candList = retSerialList;

        while (candList.size() > 0)
        {
            candPhrase = candList.front();
            candList.pop_front();

            if (getSerialIsTerminal(pBuf + candPhrase.getNodeId()))
            {
                predictInStrInSerial(pBuf, key.substr(candPhrase.getConsumeNum()), iLevel + 1, type);
            }
        }
    }

    void predictListInSerial(unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent, unsigned int type = 0)
    {
        unsigned char uConsume = 0;
        bool bFindCand = false;
        TString nulStr;

        nulStr.clear();
        retSerialList.clear();
        bFindCand = processFindInSerialAsMulti(pBuf, 0, key, &uConsume, nulStr, type);
        if (retSerialList.size() <= 0)
        {
            return;
        }

        list<MultiSerialPhrase<TString> > candList = retSerialList;

        while (candList.size() > 0)
        {
            MultiSerialPhrase<TString>& candPhrase = candList.front();

            if (getSerialIsTerminal(pBuf + candPhrase.getNodeId()))
            {
                //predictInStr(key.substr(candPhrase.getConsumeNum()), iLevel + 1);
                predictListInSerial(pBuf, key.substr(candPhrase.getConsumeNum()), candPhrase, type);
                parent.subListSerial.push_back(candPhrase);
            }

            candList.pop_front();
        }
    }

    unsigned int serializeTrie(unsigned char* pBuf, bool isUserDict = false);
    unsigned int serializeAllNode(unsigned char* pBuf, bool isUserDict = false);
    bool unSerializeTrie(unsigned char* pBuf, unsigned int userDictToken, bool isUserDict = false);
    void insert(TString key, unsigned int value, list<bigramDataNode<TString> >& lstBigramData, unsigned int type = 0);
    PatriciaTrieNode<TString, TChar>* find(TString key, PREDICT_TYPE predictType, bool bAsLower, unsigned char* pConsume = 0, unsigned int type = 0);
};

template <class TString, class TChar>
class UserPatriciaTrie : public PatriciaTrie<TString, TChar>
{
private:
    unsigned short uNodeNum;
    unsigned short uCurToken;

    void processInsertAsUserWord(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned int value, unsigned short uToken);

    void findMiniDataNodeList(PatriciaTrieNode<TString, TChar>* parent, unsigned int index, vector<CandNode<TString, TChar> >& nodeList)
    {
        unsigned int i;
        PatriciaTrieNode<TString, TChar>& node = parent->children.at(index);

        if (node.isTerminal())
        {
            if (nodeList.size() > 0)
            {
                CandNode<TString, TChar>& firstNode = nodeList.at(0);
                if (node.getTimes() < firstNode.node->children.at(firstNode.childIndex).getTimes())
                {
                    nodeList.clear();
                    CandNode<TString, TChar> child(parent, index);
                    nodeList.push_back(child);
                }
                else if (node.getTimes() == firstNode.node->children.at(firstNode.childIndex).getTimes())
                {
                    if (nodeList.size() == 10)
                    {
                        srand((unsigned long)&node);
                        nodeList.erase(nodeList.begin() + (rand() % 10));
                    }

                    CandNode<TString, TChar> child(parent, index);
                    nodeList.push_back(child);
                }
            }
            else
            {
                CandNode<TString, TChar> child(parent, index);
                nodeList.push_back(child);
            }
        }

        for (i = 0; i < node.children.size(); i++)
        {
            findMiniDataNodeList(&node, i, nodeList);
        }
    }

public:

    list<MultiSerialPhrase<TString> > userWordList;

    UserPatriciaTrie() : uNodeNum(0), uCurToken(0) { userWordList.clear(); };

    void rootToList(unsigned char* pBuf)
    {
        TString key;
        key.clear();
        userWordList.clear();
        rootToList(pBuf, PatriciaTrie<TString, TChar>::root, key);
    }

    void rootToList(unsigned char* pBuf, PatriciaTrieNode<TString, TChar>& node, TString key)
    {
        for (int i = 0; i < node.children.size(); i++)
        {
            PatriciaTrieNode<TString, TChar>& child = node.children.at(i);
            if (child.isTerminal())
            {
                MultiSerialPhrase<TString> userWord;
                userWord.setReString(key + child.getKeyStr());
                unsigned int nodeIdx = PatriciaTrie<TString, TChar>::processFindInSerialExact(pBuf, 0, key + child.getKeyStr());
                if (nodeIdx > 0)
                {
                    userWord.setNodeId(nodeIdx);
                    userWord.setNodeFlag(PatriciaTrie<TString, TChar>::getSerialNodeFlag(pBuf + nodeIdx));
                }
                userWord.setWeight(child.getData());
                userWordList.push_back(userWord);
            }
            rootToList(pBuf, child, key + child.getKeyStr());
        }
    }

    void insertAsUserWord(TString key, unsigned int value, bool bAsMultiWord, bool bAsNewWord);
    //void deletePhoneBookName(TString key);

    bool unSerializeTrie(unsigned char* pBuf);
    unsigned int serializeTrie(unsigned char* pBuf);

    inline unsigned short readU16(unsigned char* pBuf)
    {
        return ((*pBuf << 8) | *(pBuf + 1));
    }
    inline void writeU16(unsigned char* pBuf, unsigned short uValue)
    {
        *(pBuf + 1) = uValue & 0xFF;
        *pBuf = (uValue >> 8) & 0xFF;
    }

    void findQuote(TString key, list<CandItem<TString> >& candList)
    {
        QuotePhrase<TString> candPhrase;

        PatriciaTrie<TString, TChar>::retQuoteList.clear();
//        PatriciaTrieNode<TString, TChar>* pNode = PatriciaTrie<TString, TChar>::find(key, PatriciaTrie<TString, TChar>::QUOTE_FILTER, true);

        if (PatriciaTrie<TString, TChar>::retQuoteList.size() <= 0)
        {
            return;
        }

        list<QuotePhrase<TString> > list = PatriciaTrie<TString, TChar>::retQuoteList;

        while (list.size() > 0)
        {
            candPhrase = list.front();
            list.pop_front();

            CandItem<TString> cand(candPhrase.getKey().c_str(), MAX_P);
            candList.push_back(cand);
        }
    }

    CandNode<TString, TChar> findRandomMiniDataNode()
    {
        vector<CandNode<TString, TChar> > nodeList;
        for(int i = 0; i < PatriciaTrie<TString, TChar>::root.children.size(); i++)
        {
            findMiniDataNodeList(&(PatriciaTrie<TString, TChar>::root), i, nodeList);
        }
#ifdef ANDROID
        int cnt = nodeList.size();

        srand((unsigned long)&nodeList);
        int rand = ::rand();
        if (rand < 0)
        {
            rand = -rand;
        }

        return nodeList.at(rand % cnt);
#else
        srand((unsigned long)&nodeList);
        return nodeList.at(rand()%nodeList.size());
#endif
    }
};

template <class TString, class TChar>
class KikaEngine
{
public:
    enum EngineStatus
    {
        S_START,
        S_EDIT,
        S_BACKSPACE,
    };

    KikaEngine () : serialBuf(NULL), serialUserBuf(NULL), serialUserBufForSys(NULL), serialCellsBuf(NULL), bUdbChanged(false), uSerialSize(0), closeFuzzySearch(false), closeHalfMatchSearch(false){};
    virtual ~KikaEngine ()
    {
        reset();
    };

    void reset()
    {
#ifdef WIN32
        delete[] serialBuf;
        delete[] serialCellsBuf;
#else
        if(serialBuf)
        {
            munmap(serialBuf, uSerialSize);
        }
        if(serialCellsBuf)
        {
            munmap(serialCellsBuf, cellsFileLen);
        }
#endif
        delete[] serialUserBuf;
        delete[] serialUserBufForSys;
    };

    bool predict(Associate<TChar>& assList, vector<TString>& cand, EngineStatus status);
    bool predict_Cells(Associate<TChar>& assList, vector<TString>& cand, EngineStatus status);
    bool predict(TString preword,  Associate<TChar>& assList, vector<TString>& cand, EngineStatus status);
    bool predictTrack(vector<TrackChar<TChar> >& allTrackCharVec,
        vector<TrackChar<TChar> >& allTrustCharVec, vector<TString>& cand);
    bool backspacePredict(Associate<TChar>& assList, vector<TString>& cand);
    bool backspacePredict(TString preword, Associate<TChar>& assList, vector<TString>& cand);
    bool backspacePredict_Cells(Associate<TChar>& assList, vector<TString>& cand);
    bool select(TString& inStr, vector<TString>& cand, bool bMulti, bool bNewWord);
    bool deletePhoneBook(TString& inStr);
    bool isUdbChanged() { return bUdbChanged; };
    void resetUdbChanged() { bUdbChanged = false; };
    void prewordAssociate(TString preword);
    bool updateCellDict(char* cellsPath,unsigned char* networkBuf);
    unsigned int saveCells2Dict(unsigned char* cellsBuf,unsigned char* networkBuf);
    bool removeRepetition( vector<TString>& cand, TString elementStr);

    void setImeOptionForFuzzySearch(bool closeFuzzy)
    {
        closeFuzzySearch = closeFuzzy;
    };
    void setImeOptionForHalfSearch(bool closeHalfMatch)
    {
        closeHalfMatchSearch = closeHalfMatch;
    };
    bool storeUDB(char* udbPath);
    bool loadDB(char* udbPath, char* sysPath);
    bool loadDB(char* udbPath, char* sysPath, char* cellsPath);
    void hardMaskUserData(unsigned char* userData,unsigned int dataSize);
    bool insertAsUserWord(unsigned char* inBuf, TString& newWord);
    bool deleteUserWord(TString& userWord);
    bool convertOldUserData2NewVersion(unsigned char* oldUserBuf, int DBVersion,bool doubleByte);
    void getDictVersion(TString& dictversion)
    {
#ifdef ANDROID
        dictversion[0]=(char)dictionaryVersion[0];
        dictversion[1]=(char)dictionaryVersion[1];
        dictversion[2]=(char)dictionaryVersion[2];
        dictversion[3]=(char)dictionaryVersion[3];
#else
        dictversion.insert(0,1,dictionaryVersion[0]);
        dictversion.insert(1,1,dictionaryVersion[1]);
        dictversion.insert(2,1,dictionaryVersion[2]);
        dictversion.insert(3,1,dictionaryVersion[3]);
#endif
    }

private:
    void predictAssociate(Associate<TChar>& assList,
        list<CandItem<TString> >& candList, TString& inStr, int idx,
        unsigned int uCorrectNum, EngineStatus status);
    void predictAssociate(Associate<TChar>& assList,list<CandItem<TString> >& candList);
    void predictAssociate_Ext(Associate<TChar>& assList, list<CandItem<TString> >& candList,
        TString& inStr,double freq,unsigned int searchOption);
    void predictCells(Associate<TChar>& assList,list<CandItem<TString> >& candList);

    void insertCandidate(vector<TString>& cand, int idx, TString str)
    {
        if (idx >= cand.size())
        {
            cand.push_back(str);
        }
        else
        {
            cand.insert(cand.begin() + idx, str);
        }
    };

    PatriciaTrieNode<TString, TChar> ptrieNode;
    PatriciaTrie<TString, TChar> ptrieOut;
    UserPatriciaTrie<TString, TChar> ptrieUserLocal;
    bool bUdbChanged;
    unsigned long uSerialSize;
    unsigned long cellsFileLen;
    bool closeFuzzySearch;
    bool closeHalfMatchSearch;

    unsigned char dictionaryVersion[5];
    unsigned char* serialBuf;
    unsigned char* serialUserBuf;
    unsigned char* serialUserBufForSys;
    unsigned char* serialCellsBuf;
};

class PredictEngineManager
{
public:
    PredictEngineManager() {};
    virtual ~PredictEngineManager() {};

    virtual bool LoadLocale(string udb, string sysdb)
    {
        bool bRet = true;

        if (singleCharEngineCore.isUdbChanged())
        {
            bRet = singleCharEngineCore.storeUDB((char*)udbPath.c_str());
        }
        if (doubleCharEngineCore.isUdbChanged())
        {
            bRet = doubleCharEngineCore.storeUDB((char*)udbPath.c_str());
        }

        udbPath = udb;
        sysPath = sysdb;

        singleCharEngineCore.reset();
        doubleCharEngineCore.reset();

#ifdef ANDROID
        int pos = sysPath.find(".");
        char cha = sysPath.at(pos -1);
        if ( cha == '1')
#else
        if (sysPath.at(sysPath.find(".")-1) == '1')
#endif
        {
            if (!singleCharEngineCore.loadDB((char*)udbPath.c_str(), (char*)sysPath.c_str()))
            {
                return false;
            }
        }
        else
        {
            if (!doubleCharEngineCore.loadDB((char*)udbPath.c_str(), (char*)sysPath.c_str()))
            {
                return false;
            }
        }


        return bRet;
    };

    virtual bool LoadLocale(string udb, string sysdb, string cellsdb)
    {
        bool bRet = true;

        if (singleCharEngineCore.isUdbChanged())
        {
            bRet = singleCharEngineCore.storeUDB((char*)udbPath.c_str());
        }
        if (doubleCharEngineCore.isUdbChanged())
        {
            bRet = doubleCharEngineCore.storeUDB((char*)udbPath.c_str());
        }

        udbPath = udb;
        sysPath = sysdb;
        cellsPath = cellsdb;

        singleCharEngineCore.reset();
        doubleCharEngineCore.reset();

#ifdef ANDROID
        int pos = sysPath.find(".");
        char cha = sysPath.at(pos -1);
        if ( cha == '1')
#else
        if (sysPath.at(sysPath.find(".")-1) == '1')
#endif
        {
            if (!singleCharEngineCore.loadDB((char*)udbPath.c_str(), (char*)sysPath.c_str(), (char*)cellsPath.c_str()))
            {
                return false;
            }
        }
        else
        {
            if (!doubleCharEngineCore.loadDB((char*)udbPath.c_str(), (char*)sysPath.c_str(), (char*)cellsPath.c_str()))
            {
                return false;
            }
        }

        return bRet;
    };

    virtual bool StoreUDB(string udb)
    {
        bool bRet = false;

        if (udb.size() > 0 && singleCharEngineCore.isUdbChanged())
        {
            bRet = singleCharEngineCore.storeUDB((char*)udb.c_str());
        }
        if (udb.size() > 0 && doubleCharEngineCore.isUdbChanged())
        {
            bRet = doubleCharEngineCore.storeUDB((char*)udb.c_str());
        }

        return bRet;
    }

    virtual bool predictTrack(vector<TrackChar<unsigned char> >& allTrackCharVec,
        vector<TrackChar<unsigned char> >& allTrustCharVec, vector<string>& cand)
    {
        return singleCharEngineCore.predictTrack(allTrackCharVec, allTrustCharVec, cand);
    };

    virtual bool predictTrack(vector<TrackChar<wchar_t> >& allTrackCharVec,
        vector<TrackChar<wchar_t> >& allTrustCharVec, vector<wstring>& cand)
    {
//      pthread_t pid;
//      pthread_attr_t attr;
//      pthread_attr_init(&attr);
//      pthread_attr_setscope(&attr, PTHREAD_SCOPE_PROCESS);
//      pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
//      pthread_create(&pid, &attr, NULL, NULL);
//      pthread_exit(NULL);

        //return doubleCharEngineCore.predictTrack(allTrackCharVec, allTrustCharVec, cand);
        return true;
    };

    virtual bool predict(Associate<unsigned char>& assList, vector<string>& cand, bool isStart = false)
    {
        return singleCharEngineCore.predict(assList, cand, isStart ? singleCharEngineCore.S_START : singleCharEngineCore.S_EDIT);
    };
    virtual bool predict(Associate<wchar_t>& assList, vector<wstring>& cand, bool isStart = false)
    {
        return doubleCharEngineCore.predict(assList, cand, isStart ? doubleCharEngineCore.S_START : doubleCharEngineCore.S_EDIT);
    };

    virtual bool predict(string preword, Associate<unsigned char>& assList, vector<string>& cand, bool isStart = false)
    {
        return singleCharEngineCore.predict(preword, assList, cand, isStart ? singleCharEngineCore.S_START : singleCharEngineCore.S_EDIT);
    };
    virtual bool predict(wstring preword, Associate<wchar_t>& assList, vector<wstring>& cand, bool isStart = false)
    {
        return doubleCharEngineCore.predict(preword, assList, cand, isStart ? doubleCharEngineCore.S_START : doubleCharEngineCore.S_EDIT);
    };

    virtual bool backspacePredict(Associate<unsigned char>& assList, vector<string>& cand)
    {
        return singleCharEngineCore.backspacePredict(assList, cand);
    };
    virtual bool backspacePredict(Associate<wchar_t>& assList, vector<wstring>& cand)
    {
        return doubleCharEngineCore.backspacePredict(assList, cand);
    };

    virtual bool backspacePredict(string preword, Associate<unsigned char>& assList, vector<string>& cand)
    {
        return singleCharEngineCore.backspacePredict(preword, assList, cand);
    };
    virtual bool backspacePredict(wstring preword, Associate<wchar_t>& assList, vector<wstring>& cand)
    {
        return doubleCharEngineCore.backspacePredict(preword, assList, cand);
    };


    virtual bool predict_Cells(Associate<unsigned char>& assList, vector<string>& cand, bool isStart = false)
    {
        return singleCharEngineCore.predict_Cells(assList, cand, isStart ? singleCharEngineCore.S_START : singleCharEngineCore.S_EDIT);
    };
    virtual bool predict_Cells(Associate<wchar_t>& assList, vector<wstring>& cand, bool isStart = false)
    {
        return doubleCharEngineCore.predict_Cells(assList, cand, isStart ? doubleCharEngineCore.S_START : doubleCharEngineCore.S_EDIT);
    };

    virtual bool backspacePredict_Cells(Associate<unsigned char>& assList, vector<string>& cand)
    {
        return singleCharEngineCore.backspacePredict_Cells(assList, cand);
    };
    virtual bool backspacePredict_Cells(Associate<wchar_t>& assList, vector<wstring>& cand)
    {
        return doubleCharEngineCore.backspacePredict_Cells(assList, cand);
    };

    virtual bool select(string& inStr, vector<string>& cand, bool bMulti, bool bNewWord)
    {
        return singleCharEngineCore.select(inStr, cand, bMulti, bNewWord);
    };
    virtual bool select(wstring& inStr, vector<wstring>& cand, bool bMulti, bool bNewWord)
    {
        return doubleCharEngineCore.select(inStr, cand, bMulti, bNewWord);
    };

    //virtual bool deletePhoneBook(string& inStr)
    //{
    //  return singleCharEngineCore.deletePhoneBook(inStr);
    //};
    //virtual bool deletePhoneBook(wstring& inStr)
    //{
    //  return doubleCharEngineCore.deletePhoneBook(inStr);
    //};

    virtual bool deleteUserWord(string& inStr)
    {
        return singleCharEngineCore.deleteUserWord(inStr);
    };
    virtual bool deleteUserWord(wstring& inStr)
    {
        return doubleCharEngineCore.deleteUserWord(inStr);
    };

    virtual void getDictVersion(string& inStr)
    {
        singleCharEngineCore.getDictVersion(inStr);
    };
    virtual void getDictVersion(wstring& inStr)
    {
        doubleCharEngineCore.getDictVersion(inStr);
    };

    virtual bool getDictVersion(string inStr,string& outStr)
    {
#ifdef WIN32
        unsigned char dataVersion[5]={0};
        FILE* fp = fopen(inStr.c_str(), "rb");
        if (!(fp))
        {
            printf("open DB failed!");
            return false;
        }
        fread(dataVersion, 1, 4, fp);
        fclose(fp);
        outStr.push_back(dataVersion[0]);
        outStr.push_back(dataVersion[1]);
        outStr.push_back(dataVersion[2]);
        outStr.push_back(dataVersion[3]);

#else
        unsigned char * dataVersion;
        int serial = open(inStr.c_str(), O_RDONLY, S_IRUSR);
        dataVersion = (unsigned char*)mmap(NULL, 4, PROT_READ, MAP_SHARED, serial, 0);
        outStr.push_back(*(dataVersion+0));
        outStr.push_back(*(dataVersion+1));
        outStr.push_back(*(dataVersion+2));
        outStr.push_back(*(dataVersion+3));

        close(serial);
#endif
        return true;
    };
    virtual bool getDictVersion(wstring& inStr,wstring& outStr)
    {
        return true;
    };
    virtual bool XM_create_new_dict(char* oldFileName,char* diffFileName,char* outNewFileName)
    {
        //XM_create_new_file(oldFileName,diffFileName,outNewFileName);
        return true;
    };
    virtual void setImeOptionForFuzzySearch(bool fuzzySearch)
    {
        doubleCharEngineCore.setImeOptionForFuzzySearch(fuzzySearch);
        singleCharEngineCore.setImeOptionForFuzzySearch(fuzzySearch);
    };
    virtual void setImeOptionForHalfSearch(bool halfSearch)
    {
        doubleCharEngineCore.setImeOptionForHalfSearch(halfSearch);
        singleCharEngineCore.setImeOptionForHalfSearch(halfSearch);
    };
    virtual bool XM_update_cell_dict(char* cellsPath,unsigned char* networkBuf)
    {
        if (networkBuf[0] == 2)
        {
            return doubleCharEngineCore.updateCellDict(cellsPath,networkBuf);
        }
        else
        {
            return singleCharEngineCore.updateCellDict(cellsPath,networkBuf);
        }
    };

    virtual bool getCellsDictID(char* cellsPath,unsigned char* cellsID)
    {
        unsigned char cellMessage[NATIVE_TITLE_MESSAGE_SIZE*(255+1)] = {0};
        unsigned char cellsCount = 0;
#ifdef WIN32
        FILE* fp = fopen((const char*)cellsPath, "rb");
        if (!(fp))
        {
            return false;
        }

        fread(cellMessage,1,NATIVE_TITLE_MESSAGE_SIZE*255,fp);
        fclose(fp);
#else
        int serial = open((const char*)cellsPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
        if(0>=serial){
            return false;
        }
        read(serial, cellMessage, NATIVE_TITLE_MESSAGE_SIZE*255);
        close(serial);

#endif
        cellsCount = cellMessage[0];

        for (int i=0;i<cellsCount;i++)
        {
            memcpy(cellsID+i*5,cellMessage+1+NATIVE_TITLE_MESSAGE_SIZE*i,5);
        }
        return true;
    };

    virtual bool setCellsForSearch(unsigned char* cellsPath,unsigned short cellsID,unsigned char flag)
    {
        unsigned int cellsSize = 0;
        unsigned char* cellsBuf;
        unsigned char hardMark=0,cellsCount=0;
        unsigned char cellIDHigtByte=0;
        unsigned char cellIDLowByte=0;
        int cellsIndex = 0;

        cellIDLowByte =(unsigned char) (cellsID & 0x00ff);
        cellIDHigtByte =(unsigned char) ((cellsID>>8) & 0x00ff);
#ifdef WIN32
        FILE* fpCells = fopen((const char*)cellsPath, "rb");
        if (fpCells <= 0)
        {
            return false;
        }
        fseek(fpCells, 0, SEEK_END);
        cellsSize = ftell(fpCells);
        fseek(fpCells, 0, SEEK_SET);
        cellsBuf = new unsigned char [cellsSize + 4];
        if (cellsBuf <= 0)
        {
            fclose(fpCells);
            return false;
        }
        memset(cellsBuf, 0x00, cellsSize + 4);
        if (cellsSize > 0)
        {
            cellsSize = fread(cellsBuf, 1, cellsSize, fpCells);
            hardMark = 1;
            for (int i=0;i<NATIVE_TITLE_MESSAGE_SIZE*255;i++)
            {
                if (i>=cellsSize)
                {
                    break;
                }
                cellsBuf[i] = cellsBuf[i]^hardMark;
                hardMark ++;
            }

        }

        fclose(fpCells);
        cellsCount = cellsBuf[0];
        cellsIndex = 0;
        for (cellsIndex=0;cellsIndex<cellsCount;cellsIndex++)
        {
            if (cellsBuf[NATIVE_TITLE_MESSAGE_SIZE*cellsIndex+1]==cellIDHigtByte && cellsBuf[NATIVE_TITLE_MESSAGE_SIZE*cellsIndex+2]==cellIDLowByte)
            {
                cellsBuf[NATIVE_TITLE_MESSAGE_SIZE*cellsIndex+3] = flag;
                break;
            }
        }

        if (cellsIndex == cellsCount)
        {
            fclose(fpCells);
            return false;
        }

        hardMark = 1;
        for (int i=0;i<NATIVE_TITLE_MESSAGE_SIZE*255;i++)
        {
            if (i>=cellsSize)
            {
                break;
            }
            cellsBuf[i] = cellsBuf[i]^hardMark;
            hardMark ++;
        }
        fpCells = fopen((const char*)cellsPath, "wb");
        fwrite(cellsBuf, sizeof(char), cellsSize, fpCells);
        fclose(fpCells);

#else
        int cells = open(udbPath.c_str(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
        if (cells <= 0)
        {
            return false;
        }
        cellsSize = (unsigned long)lseek(cells, 1, SEEK_END);
        lseek(cells, 0, SEEK_SET);
        cellsBuf = (unsigned char*)mmap(NULL, cellsSize, PROT_READ, MAP_SHARED, cells, 0);
        if (cellsBuf <= 0)
        {
            close(cells);
            return false;
        }
        if (cellsSize > 0)
        {
            read(cells, cellsBuf, cellsSize);
            hardMark = 1;
            for (int i=0;i<NATIVE_TITLE_MESSAGE_SIZE*255;i++)
            {
                if (i>=cellsSize)
                {
                    break;
                }
                cellsBuf[i] = cellsBuf[i]^hardMark;
                hardMark ++;
            }
        }

        cellsCount = cellsBuf[0];
        for (cellsIndex=0;cellsIndex<cellsCount;cellsIndex++)
        {
            if (cellsBuf[NATIVE_TITLE_MESSAGE_SIZE*cellsIndex+2]==cellIDHigtByte && cellsBuf[NATIVE_TITLE_MESSAGE_SIZE*cellsIndex+3]==cellIDLowByte)
            {
                cellsBuf[NATIVE_TITLE_MESSAGE_SIZE*cellsIndex+4] = flag;
                break;
            }
        }

        if (cellsIndex == cellsCount)
        {
            close(cells);
            return false;
        }

        hardMark = 1;
        for (int i=0;i<NATIVE_TITLE_MESSAGE_SIZE*255;i++)
        {
            if (i>=cellsSize)
            {
                break;
            }
            cellsBuf[i] = cellsBuf[i]^hardMark;
            hardMark ++;
        }

        write(cells, cellsBuf, cellsSize);
        close(cells);

#endif
        return true;
    };

private:
    string udbPath;
    string sysPath;
    string cellsPath;

    KikaEngine<string, unsigned char> singleCharEngineCore;
    KikaEngine<wstring, wchar_t> doubleCharEngineCore;
};

#endif
