//#include "stdafx.h"

#ifdef WIN32
#include <tchar.h>
#elif defined ANDROID
typedef unsigned char _TCHAR;
#include <stdio.h>
#define TRACE printf
#endif

#include "levenshtein.h"
#include "patricia.h"
#ifndef WIN32

#ifdef ANDROID
#define S_IRUSR 0
#define S_IWUSR 0
#include <fcntl.h>
#else
#include <sys/fcntl.h>
#endif


#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>


#define MULTI_PUNISH 0.3
#define BI_REWARD 1.3
#define MORPH_PUNISH 0.9
#define LAST_NOT_FINISH_PUNISH 0.8
#define NOT_ENDED_PUNISH 0.5
#define FUZZY_MATCH_PROPORTION 5
#define MAX_FUZZY_COUNT        2

template <class TString, class TChar>
int PatriciaTrieNode<TString, TChar>::serializeNode(unsigned char* pBuf, unsigned int* pCurrentIdx, unsigned int* parentOffset, queue< pair<PatriciaTrieNode<TString, TChar> *, unsigned int> >& queue, bool isUserDict)
{
    unsigned int  i = 0;
    currentIndex = *pCurrentIdx;
    if (key.size() > 0 && sizeof(key.at(0)) > 1)
    {
        setIsUnicode(true);
    }
    else
    {
        setIsUnicode(false);
    }

    //write parent child offset
    if (*parentOffset != 0)
    {
        setByteData(pBuf, parentOffset, (currentIndex >> 16) & 0xFF);
        setByteData(pBuf, parentOffset, (currentIndex >> 8) & 0xFF);
        setByteData(pBuf, parentOffset, (currentIndex ) & 0xFF);
        *parentOffset = 0;
    }
    // flag
    setByteData(pBuf, pCurrentIdx, ((getKeyStrLen() & 0x0F) << 4) | (flag & 0x0F));

    // data
    if (flag & 0x1)
    {
        //setByteData(pBuf, pCurrentIdx, (data >> 24) & 0xFF);
        //setByteData(pBuf, pCurrentIdx, (data >> 16) & 0xFF);
        //setByteData(pBuf, pCurrentIdx, (data >> 8) & 0xFF);
        setByteData(pBuf, pCurrentIdx, data & 0xFF);

        //serializeNode times for userDict
        if (isUserDict)
        {
            setByteData(pBuf, pCurrentIdx, (getTimes() >> 8) & 0xFF);
            setByteData(pBuf, pCurrentIdx, getTimes() & 0xFF);
        }
    }

    // key
    //setByteData(pBuf, pCurrentIdx, getKeyStrLen());
    for (i = 0; i < getKeyStrLen(); i++)
    {
        if (isUnicode())
        {
            setByteData(pBuf, pCurrentIdx, (key.at(i) >> 8) & 0xFF);
            setByteData(pBuf, pCurrentIdx, key.at(i) & 0xFF);
        }
        else
        {
            setByteData(pBuf, pCurrentIdx, key.at(i));
        }
    }

    // bigram
    setByteData(pBuf, pCurrentIdx, biData.size());
    for (i = 0; i < biData.size(); i++)
    {
        *pCurrentIdx += 3; // jump bi data address
        setByteData(pBuf, pCurrentIdx, biData.at(i).freq);
    }

    // children size
    setByteData(pBuf, pCurrentIdx, children.size());

    unsigned int offsetIdx = (children.size() == 0) ? 0 : *pCurrentIdx;

    if (children.size() > 0)
        *pCurrentIdx += 3;

    pair<PatriciaTrieNode<TString, TChar> *, unsigned int> pairCurrentNode(this, offsetIdx);

    queue.push(pairCurrentNode);

    return 0;
}

template <class TString, class TChar>
bool PatriciaTrieNode<TString, TChar>::unSerializeNode(unsigned char* pBuf, unsigned int* pCurrentIdx, unsigned int userDictToken, bool isUserDict)
{
    unsigned int i, loopCnt, childNodeIdx, biWordIdx;
    wchar_t unicodeChar = 0;
    TString nodeStr;
    bigramDataNode<TString> biDataNode;
    PatriciaTrieNode<TString, TChar> childNode;
    PatriciaTrie<TString, TChar> trie;

    flag = *(pBuf + *pCurrentIdx);
    *pCurrentIdx += 1;

    if (flag & 0x1)
    {
        data = *(pBuf + *pCurrentIdx);
        *pCurrentIdx += 1;

        //unSerializeNode times for userDict
        if (isUserDict)
        {
            times = (*(pBuf + *pCurrentIdx) << 8) | *(pBuf + *pCurrentIdx + 1);
            *pCurrentIdx += 2;
            if (userDictToken > MAX_USER_DICT_TOKEN)
            {
                times /= 2;
            }
        }
    }

    loopCnt = flag >> 4;
    for (i = 0; i < loopCnt; i++)
    {
        if (isUnicode())
        {
            unicodeChar = (*(pBuf + *pCurrentIdx) << 8) | *(pBuf + *pCurrentIdx + 1);
            *pCurrentIdx += 2;
            key.push_back(unicodeChar);
        }
        else
        {
            key.push_back(*(pBuf + *pCurrentIdx));
            *pCurrentIdx += 1;
        }
    }

    //*pCurrentIdx += 3;

    loopCnt = *(pBuf + *pCurrentIdx);
    *pCurrentIdx += 1;
    for (i = 0; i < loopCnt; i++)
    {
        biDataNode.reset();
        biWordIdx = readU24(pBuf + *pCurrentIdx);
        *pCurrentIdx += 3;

        nodeStr.clear();
        trie.getSerialNodeStr(pBuf, biWordIdx, biDataNode.word);
        biDataNode.freq = *(pBuf + *pCurrentIdx);
        *pCurrentIdx += 1;

        biData.push_back(biDataNode);
    }

    loopCnt = *(pBuf + *pCurrentIdx);
    *pCurrentIdx += 1;

    childNodeIdx = readU24(pBuf + *pCurrentIdx);
    for(i = 0; i < loopCnt; i++, childNodeIdx = trie.getSerialNextNodeIdx(pBuf, childNodeIdx, isUserDict))
    {
        childNode.reset();
        unsigned int brotherNodeIdx = childNodeIdx;
        childNode.unSerializeNode(pBuf, &brotherNodeIdx, userDictToken, isUserDict);
        children.push_back(childNode);
    }

    return true;
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::getSerialNodeStr(unsigned char* pBuf, unsigned int uTargetIdx, TString& nodeStr)
{
    getSerialNodeStr(pBuf, 0, uTargetIdx, nodeStr);
    return;
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::getSerialNodeStr(unsigned char* pBuf, unsigned int uCurrentIdx, unsigned int uTargetIdx, TString& nodeStr)
{
    unsigned char* pChildren = NULL;
    unsigned int uChildrenIdx = 0;
    unsigned int uPrevIdx = 0;
    unsigned int uNextIdx = 0, nextChildrenIdx = 0;
    int childrenLen, nextChildrenLen;

    pChildren = getSerialChildBuf(pBuf + uCurrentIdx);
    childrenLen = *pChildren;

    if (childrenLen == 0)
    {
        return;
    }

    uChildrenIdx = getSerialFirstChildIdx(pBuf + uCurrentIdx);
    uPrevIdx = uChildrenIdx;

    for (int j = 0; j < childrenLen; j++, uPrevIdx = getSerialNextNodeIdx(pBuf, uPrevIdx))
    {
        if(uPrevIdx == uTargetIdx)
        {
            nodeStr += getSerialKey(pBuf, uPrevIdx);
            return;
        }
    }

    uPrevIdx = uChildrenIdx;

    for (int i = 0; i < childrenLen-1; i++, uChildrenIdx = getSerialNextNodeIdx(pBuf, uChildrenIdx))
    {
        uNextIdx = getSerialNextNodeIdx(pBuf, uChildrenIdx);
        nextChildrenLen = *getSerialChildBuf(pBuf + uNextIdx);

        if(nextChildrenLen == 0)
        {
            continue;
        }

        nextChildrenIdx = getSerialFirstChildIdx(pBuf + uNextIdx);

        if(nextChildrenIdx <= uTargetIdx)
        {
            uPrevIdx = uNextIdx;
        }
        else
        {
            break;
        }
    }

    nodeStr += getSerialKey(pBuf, uPrevIdx);
    getSerialNodeStr(pBuf, uPrevIdx, uTargetIdx, nodeStr);

    return;
}

template <class TString, class TChar>
PatriciaTrieNode<TString, TChar>* PatriciaTrie<TString, TChar>::find(TString key, PREDICT_TYPE predictType, bool bAsLower, unsigned char* consume, unsigned int type)
{
    TString nulStr;

    if (key.length() == 0)
    {
        return 0;
    }

    if (bAsLower)
    {
        transform(key.begin(), key.end(), key.begin(), ::towlower);
    }
    // transform other codec to utf8

    switch (predictType)
    {
    case PRECISE_MATCH:
        return processFind(root, key, type);

    case QUOTE_FILTER:
        return processFindAsQuoteFilter(root, key, nulStr, type);

        //case AS_HEAD:
        //  return processFindAsHead(root, key, consume);

    case AS_MULTI:
        processFindAsMulti(root, key, consume, type);
        break;

    default:
        return 0;
    }

    return 0;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processFindAsMulti(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned char* pConsume, unsigned int type)
{
    unsigned int i, j, k, len;
    MultiPhrase<TString, TChar> phraseCand;
    unsigned char consumeNum = 0;
    bool bRet = false;
    bool bHasResult = false;
    float dMorphPunish = 1.0;

    for (i = 0; i < currNode.children.size(); i++)
    {
        PatriciaTrieNode<TString, TChar>& child = currNode.children.at(i);

        consumeNum = 0;
        len = child.getKeyStrLen();

        for (j = 0, k = 0; j < key.size() && k < len; j++, k++)
        {
            if (levenshteinDistance::compChar(key.at(j), child.getKeychar(k)))
            {
                if (!levenshteinDistance::isOmitChar(child.getKeychar(k)))
                {
                    break;
                }

                while (levenshteinDistance::isOmitChar(child.getKeychar(k)))
                {
                    k++;
                    if (k >= len)
                    {
                        break;
                    }
                }

                if (k >= len || levenshteinDistance::compChar(key.at(j), child.getKeychar(k)))
                {
                    break;
                }
            }
            else if (levenshteinDistance::preciseCompChar(key.at(j), child.getKeychar(k)))
            {
                dMorphPunish = MORPH_PUNISH;
            }
        }

        if (j == 0)
        {
            if (k >= len)
            {
                consumeNum += *pConsume;
                bRet = processFindAsMulti(child, key.substr(j), &consumeNum, type);
            }

            continue;
        }
        else
        {
            consumeNum += (j + *pConsume);
            if (j < key.size() && k >= len)
            {
                bRet = processFindAsMulti(child, key.substr(j), &consumeNum, type);

                phraseCand.setData(&child, consumeNum);
                bHasResult = true;

                if (child.isTerminal() && child.isMatchType(type))
                {
                    phraseCand.setWeight(dMorphPunish);
                    retList.push_front(phraseCand);
                }
                else if (!bRet)
                {
                    phraseCand.setWeight(dMorphPunish * NOT_ENDED_PUNISH);
                    retList.push_back(phraseCand);
                }
            }

            if (j == key.size() && k == len)
            {
                phraseCand.setData(&child, consumeNum);
                bHasResult = true;

                if (child.isTerminal() && child.isMatchType(type))
                {
                    phraseCand.setWeight(dMorphPunish);
                    retList.push_front(phraseCand);
                }
                else
                {
                    phraseCand.setWeight(dMorphPunish * LAST_NOT_FINISH_PUNISH);
                    retList.push_back(phraseCand);
                }
            }

            continue;
        }
    }

    return bHasResult;
}

template <class TString, class TChar>
PatriciaTrieNode<TString, TChar>* PatriciaTrie<TString, TChar>::processFind(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned int type)
{
    unsigned int i, j, len;
    PatriciaTrieNode<TString, TChar>* pChild = NULL;

    for (i = 0; i < currNode.children.size(); i++)
    {
        PatriciaTrieNode<TString, TChar>& child = currNode.children.at(i);

        len = (unsigned int)((unsigned int)child.getKeyStrLen() < key.length() ? (unsigned int)child.getKeyStrLen() : key.length());
        for (j = 0; j < len; j++)
        {
            if (levenshteinDistance::preciseCompChar(key.at(j), child.getKeychar(j)))
            {
                break;
            }
        }

        if (j == 0)
        {
            if (levenshteinDistance::preciseCompChar(key.at(0), child.getKeychar(0)) < 0)
            {
                return 0;
            }
            else
            {
                continue;
            }
        }
        else
        {
            if (j == len)
            {
                if (key.length() == child.getKeyStrLen() && child.isTerminal() && child.isMatchType(type))
                {
                    return &child;
                }
                else if (key.length() > child.getKeyStrLen())
                {
                    pChild = processFind(child, key.substr(j), type);
                    if (pChild)
                    {
                        return pChild;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            continue; // break; break to continue since capital has another branch.
        }
    }

    return 0;
}

template <class TString, class TChar>
PatriciaTrieNode<TString, TChar>* PatriciaTrie<TString, TChar>::processFindAsQuoteFilter(PatriciaTrieNode<TString, TChar>& currNode, TString key, TString reStr, unsigned int type)
{
    unsigned int i, j, k, len;
    PatriciaTrieNode<TString, TChar>* pChild = NULL;
    QuotePhrase<TString> phraseCand;

    for (i = 0; i < currNode.children.size(); i++)
    {
        PatriciaTrieNode<TString, TChar>& child = currNode.children.at(i);
        len = child.getKeyStrLen();

        for (j = 0, k = 0; j < key.size() && k < len; j++, k++)
        {
            if (levenshteinDistance::compChar(key.at(j), child.getKeychar(k)))
            {
                if (!levenshteinDistance::isOmitChar(child.getKeychar(k)))
                {
                    break;
                }

                while (levenshteinDistance::isOmitChar(child.getKeychar(k)))
                {
                    k++;
                    if (k >= len)
                    {
                        break;
                    }
                }

                if (k >= len || levenshteinDistance::compChar(key.at(j), child.getKeychar(k)))
                {
                    break;
                }
            }
        }

        if (j == 0)
        {
            if (k >= len)
            {
                pChild = processFindAsQuoteFilter(child, key, reStr, type);
                continue;
            }
            else if (levenshteinDistance::compChar(key.at(0), child.getKeychar(0)) < 0)
            {
                return 0;
            }
            else
            {
                continue;
            }
        }
        else
        {
            if (j < key.size() && k >= len)
            {
                pChild = processFindAsQuoteFilter(child, key.substr(j), reStr.c_str() + child.getKeyStr(), type);
                continue;
            }

            if (j == key.size() && k == len)
            {
                phraseCand.setData(reStr + child.getKeyStr(), child.getData());

                if (child.isTerminal() && child.isMatchType(type))
                {
                    retQuoteList.push_front(phraseCand);
                }
                continue;
            }
            break;
        }
    }

    return 0;
}

template <class TString, class TChar>
unsigned int PatriciaTrie<TString, TChar>::processFindInSerialExact(unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned int type)
{
    unsigned int i, j, len;
    unsigned char childrenLen, nodeFlag;
    unsigned char* pChildren = NULL;
    unsigned int uChildrenIdx;

    pChildren = getSerialChildBuf(pBuf + uNodeIdx);

    childrenLen = *pChildren;

    uChildrenIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

    for (i = 0; i < childrenLen; i++, uChildrenIdx = getSerialNextNodeIdx(pBuf ,uChildrenIdx))
    {
        len = getSerialKeyLen(pBuf + uChildrenIdx);
        len = len < key.length() ? len : key.length();
        nodeFlag = getSerialNodeFlag(pBuf + uChildrenIdx);

        for (j = 0; j < len; j++)
        {
            if (levenshteinDistance::preciseCompChar(key.at(j),getSerialKeyChar(pBuf + uChildrenIdx, j)))
            {
                break;
            }
        }

        if (j == 0)
        {
            if (levenshteinDistance::preciseCompChar(key.at(j),getSerialKeyChar(pBuf + uChildrenIdx, j))<0)
            {
                return 0;
            }
            else
            {
                continue;
            }
        }
        else
        {
            if (j == len)
            {
                if (key.length() == getSerialKeyLen(pBuf + uChildrenIdx) && (nodeFlag & 0x01) && isSerialMatchType(nodeFlag, type))
                {
                    return uChildrenIdx;
                }
                else if (key.length() > getSerialKeyLen(pBuf + uChildrenIdx))
                {
                    uChildrenIdx = processFindInSerialExact(pBuf, uChildrenIdx, key.substr(j), type);
                    if (uChildrenIdx)
                    {
                        return uChildrenIdx;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            continue; // break; break to continue since capital has another branch.
        }
    }

    return 0;
}

template <class TString, class TChar>
unsigned int PatriciaTrie<TString, TChar>::processFindInSerial(unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned int type)
{
    unsigned int i, j, len;
    unsigned char childrenLen, nodeFlag;
    unsigned char* pChildren = NULL;
    unsigned int uChildrenIdx;

    pChildren = getSerialChildBuf(pBuf + uNodeIdx);

    childrenLen = *pChildren;
    uChildrenIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

    for (i = 0; i < childrenLen; i++, uChildrenIdx = getSerialNextNodeIdx(pBuf , uChildrenIdx))
    {
        len = getSerialKeyLen(pBuf + uChildrenIdx);
        len = len < key.length() ? len : key.length();
        nodeFlag = getSerialNodeFlag(pBuf + uChildrenIdx);

        for (j = 0; j < len; j++)
        {
            if (compChar(key.at(j), getSerialKeyChar(pBuf + uChildrenIdx, j)))
            {
                break;
            }
        }

        if (j == 0)
        {
            if (compChar(key.at(0), getSerialKeyChar(pBuf + uChildrenIdx, 0)) < 0)
            {
                return 0;
            }
            else
            {
                continue;
            }
        }
        else
        {
            if (j == len)
            {
                if (key.length() == getSerialKeyLen(pBuf + uChildrenIdx) && (nodeFlag & 0x01) && isSerialMatchType(nodeFlag, type))
                {
                    return uChildrenIdx;
                }
                else if (key.length() > getSerialKeyLen(pBuf + uChildrenIdx))
                {
                    uChildrenIdx = processFindInSerial(pBuf, uChildrenIdx, key.substr(j), type);
                    if (uChildrenIdx)
                    {
                        return uChildrenIdx;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            continue; // break; break to continue since capital has another branch.
        }
    }

    return 0;
}

template <class TString, class TChar>
unsigned int PatriciaTrie<TString, TChar>::processFindInSerialAsQuoteFilter(unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned int type)
{
    unsigned int i, j, k, len;
    unsigned char childrenLen, nodeFlag;
    unsigned char* pChildren = NULL;
    unsigned int uChildrenIdx;

    pChildren = getSerialChildBuf(pBuf + uNodeIdx);

    childrenLen = *pChildren;
    uChildrenIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

    for (i = 0; i < childrenLen; i++, uChildrenIdx = getSerialNextNodeIdx(pBuf , uChildrenIdx))
    {
        len = getSerialKeyLen(pBuf + uChildrenIdx);
        nodeFlag = getSerialNodeFlag(pBuf + uChildrenIdx);

        for (j = 0, k = 0; j < key.size() && k < len; j++, k++)
        {
            if (compChar(key.at(j), getSerialKeyChar(pBuf + uChildrenIdx, k)))
            {
                if (!isOmitChar(getSerialKeyChar(pBuf + uChildrenIdx, k)))
                {
                    break;
                }

                while (isOmitChar(getSerialKeyChar(pBuf + uChildrenIdx, k)))
                {
                    k++;
                    if (k >= len)
                    {
                        break;
                    }
                }

                if (k >= len || compChar(key.at(j), getSerialKeyChar(pBuf + uChildrenIdx, k)))
                {
                    break;
                }
            }
        }

        if (j == 0)
        {
            if (k >= len)
            {
                uChildrenIdx = processFindInSerialAsQuoteFilter(pBuf, uNodeIdx, key, type);
                if (uChildrenIdx)
                {
                    return uChildrenIdx;
                }
                else
                {
                    continue;
                }
            }
            else if (compChar(key.at(0), getSerialKeyChar(pBuf + uChildrenIdx, 0)) < 0)
            {
                return 0;
            }
            else
            {
                continue;
            }
        }
        else
        {
            if (j < key.size() && k >= len)
            {
                uChildrenIdx = processFindInSerialAsQuoteFilter(pBuf, uNodeIdx, key, type);
                if (uChildrenIdx)
                {
                    return uChildrenIdx;
                }
                else
                {
                    continue;
                }
            }

            if (j == key.size() && k == len && isSerialMatchType(nodeFlag, type))
            {
                return uChildrenIdx;
            }
            break;
        }
    }

    return 0;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processFindInSerialAsMulti_Ext(Associate<TChar>& assList,unsigned char* pBuf, unsigned int uNodeIdx,TString key, unsigned char  uKeyLen, TString reStr, TString& lastKey, unsigned int type)
{
    TString curStr;
    TString curSearchStr;
    bool bFindCand = false;
    unsigned char uConsume = 0;
    typename list<CharNode<TChar> > ::iterator itor;
    typename list<CharNode<TChar> > ::iterator itor_2;

    bFindCand = processFindInSerialAsMulti(pBuf, uNodeIdx, key, &uConsume, reStr,lastKey);
    if (key.size() < 3)
    {
        return bFindCand;
    }
    for(int idx=uKeyLen;idx < assList.getAssociateList().size();idx++)
    {
        AssociateNode<TChar>& assNode = assList.getAssociateList().at(idx);
        for (itor = assNode.charList.begin(),itor++; itor != assNode.charList.end(); itor++)
        {
            curStr = key;
            curStr.replace(idx-uKeyLen,1,1,itor->getChar());
            if (key.size() > 3)
            {
                for(int i=idx+1;i < assList.getAssociateList().size();i++)
                {
                    AssociateNode<TChar> assNode2 = assList.getAssociateList().at(i);
                    for (itor_2 = assNode.charList.begin(); itor_2 != assNode.charList.end(); itor_2++)
                    {
                        curSearchStr = curStr;
                        curSearchStr.replace(i-uKeyLen,1,1,itor_2->getChar());
                        bFindCand = processFindInSerialAsMulti(pBuf, uNodeIdx, curStr, &uConsume, reStr,lastKey);
                    }

                }
            }
            else
            {
                bFindCand = processFindInSerialAsMulti(pBuf, uNodeIdx, curStr, &uConsume, reStr,lastKey);
            }
//          if (retSerialList.size() > 30)
//          {
//              return bFindCand;
//          }
        }
//      if ((idx-uKeyLen) > 6)
//      {
//          break;
//      }
    }
    return bFindCand;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processPutCandToSerialList(MultiSerialPhrase<TString>& phraseCand)
{
    typename list<MultiSerialPhrase<TString> > ::iterator itor;
    bool isExist = false;

    for(itor = retSerialList.begin();itor != retSerialList.end();itor++)
    {
        if (itor->getNodeId() == phraseCand.getNodeId())
        {
            isExist = true;
            break;
        }
    }

    if (isExist)
    {
        return false;
    }
    else
    {
        retSerialList.push_front(phraseCand);
        return true;
    }
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processFindInSerialAsMulti(Associate<TChar>& assList,unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned char* pConsume, TString reStr, int dCurFreq, unsigned char uKeyLen,unsigned int type)
{
    unsigned int i, j, k, len;
    unsigned char childrenLen, nodeFlag;
    unsigned char* pChildren = NULL;
    unsigned int uChildrenIdx;
    unsigned char consumeNum = 0;
    bool bRet = false;
    bool bHasResult = false;
    const unsigned int uMaxWideSearchNum = 50;
    const unsigned int uMaxCnt = 10;
    unsigned int uCnt;
    MultiSerialPhrase<TString> phraseCand;
    std::list<pairTermNode<TString> >  allSubNodeList;
    pairTermNode<TString> termNodePair;

    if (key.size() == 0)
    {
        return false;
    }

    pChildren = getSerialChildBuf(pBuf + uNodeIdx);

    childrenLen = *pChildren;
    uChildrenIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

    for (i = 0; i < childrenLen; i++, uChildrenIdx = getSerialNextNodeIdx(pBuf, uChildrenIdx))
    {
        consumeNum = 0;

        len = getSerialKeyLen(pBuf + uChildrenIdx);
        nodeFlag = getSerialNodeFlag(pBuf + uChildrenIdx);
        unsigned int thisFreq = dCurFreq;

        for (j = 0, k = 0; j < key.size() && k < len; j++, k++)
        {
            int compRet = fuzzyCompChar(assList.getAssociateList().at(uKeyLen+j), getSerialKeyChar(pBuf + uChildrenIdx, k));
            if (compRet < 0)
            {
                if (!levenshteinDistance::isOmitChar(getSerialKeyChar(pBuf + uChildrenIdx, k)))
                {
                    break;
                }

                while (levenshteinDistance::isOmitChar(getSerialKeyChar(pBuf + uChildrenIdx, k)))
                {
                    k++;
                    if (k >= len)
                    {
                        break;
                    }
                }

                if (k >= len || fuzzyCompChar(assList.getAssociateList().at(uKeyLen+j), getSerialKeyChar(pBuf + uChildrenIdx, k)) < 0)
                {
                    break;
                }
                else if (fuzzyCompChar(assList.getAssociateList().at(uKeyLen + j), getSerialKeyChar(pBuf + uChildrenIdx, k)) > 0)
                {
                    thisFreq = dCurFreq + 1;
                }
            }
            else if (compRet > 0)
            {
                thisFreq = dCurFreq + 1;
            }
        }

        if (thisFreq > MAX_FUZZY_COUNT)
        {
            continue;
        }

        if (j == 0)
        {
            if (len == 0 || len > 10)
            {
                continue;
            }

            if (k >= len)
            {
                consumeNum += *pConsume;
                bRet = processFindInSerialAsMulti(assList, pBuf, uChildrenIdx, key.substr(j), &consumeNum,
                    reStr + getSerialKey(pBuf, uChildrenIdx).substr(0, k), thisFreq,uKeyLen+j, type);
            }

            continue;
        }
        else
        {
            consumeNum += (j + *pConsume);
            phraseCand.reset();

            if (j < key.size() && k >= len)
            {
                bRet = processFindInSerialAsMulti(assList,pBuf, uChildrenIdx, key.substr(j), &consumeNum,
                    reStr + getSerialKey(pBuf, uChildrenIdx).substr(0, k), thisFreq, uKeyLen + j,  type);
                phraseCand.setData(uChildrenIdx, consumeNum);
                phraseCand.setReString(reStr + getSerialKey(pBuf, uChildrenIdx).substr(0, k));

                if (getSerialIsTerminal(pBuf + uChildrenIdx) && isSerialMatchType(nodeFlag, type))
                {
                    phraseCand.setNodeFlag(nodeFlag);
                    phraseCand.setFuzzyCnt(thisFreq);
                    if (processPutCandToSerialList(phraseCand))
                    {
                        bHasResult = true;
                    }
                }
            }

            if (j == key.size())
            {
                bHasResult = true;

                if (getSerialIsTerminal(pBuf + uChildrenIdx) && isSerialMatchType(nodeFlag, type))
                {
                    phraseCand.setData(uChildrenIdx, consumeNum);
                    phraseCand.setReString(reStr + getSerialKey(pBuf, uChildrenIdx));
                    phraseCand.setNodeFlag(nodeFlag);
                    phraseCand.setFuzzyCnt(thisFreq);
                    processPutCandToSerialList(phraseCand);
                }

                allSubNodeList.clear();
                getAllTermNodeIdx(pBuf, uChildrenIdx, allSubNodeList, uMaxWideSearchNum, type);

                if (allSubNodeList.size() > 0)
                {
                    allSubNodeList.sort();
                    allSubNodeList.reverse();
                }

                uCnt = 0;
                while (allSubNodeList.size() > 0)
                {
                    termNodePair = allSubNodeList.front();
                    allSubNodeList.pop_front();

                    phraseCand.setData(termNodePair.getNodeIdx(), consumeNum);
                    phraseCand.setReString(reStr + getSerialKey(pBuf, uChildrenIdx) + termNodePair.getReStr());
                    phraseCand.setNodeFlag(getSerialNodeFlag(pBuf + termNodePair.getNodeIdx()));
                    phraseCand.setFuzzyCnt(thisFreq);
                    processPutCandToSerialList(phraseCand);

                    uCnt++;

                    if (uCnt >= uMaxCnt)
                    {
                        break;
                    }
                }
            }

            continue;
        }
    }

    return bHasResult;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processFindInSerialAsMulti(unsigned char* pBuf, unsigned int uNodeIdx, TString key, unsigned char* pConsume, TString reStr, TString& lastKey, unsigned int type)
{
    unsigned int i, j, k, len;
    unsigned char childrenLen, nodeFlag;
    unsigned char* pChildren = NULL;
    unsigned int uChildrenIdx;
    unsigned char consumeNum = 0;
    bool bRet = false;
    bool bHasResult = false;
    const unsigned int uMaxWideSearchNum = 50;
    const unsigned int uMaxCnt = 10;
    unsigned int uCnt;
    MultiSerialPhrase<TString> phraseCand;
    list<pairTermNode<TString> > allSubNodeList;
    pairTermNode<TString> termNodePair;

    if (key.size() == 0)
    {
        return false;
    }

    pChildren = getSerialChildBuf(pBuf + uNodeIdx);

    childrenLen = *pChildren;
    uChildrenIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

    for (i = 0; i < childrenLen; i++, uChildrenIdx = getSerialNextNodeIdx(pBuf , uChildrenIdx))
    {
        consumeNum = 0;

        len = getSerialKeyLen(pBuf + uChildrenIdx);
        nodeFlag = getSerialNodeFlag(pBuf + uChildrenIdx);

        for (j = 0, k = 0; j < key.size() && k < len; j++, k++)
        {
            if (levenshteinDistance::compChar(key.at(j), getSerialKeyChar(pBuf + uChildrenIdx, k)))
            {
                if (!levenshteinDistance::isOmitChar(getSerialKeyChar(pBuf + uChildrenIdx, k)))
                {
                    break;
                }

                while (levenshteinDistance::isOmitChar(getSerialKeyChar(pBuf + uChildrenIdx, k)))
                {
                    k++;
                    if (k >= len)
                    {
                        break;
                    }
                }

                if (k >= len || levenshteinDistance::compChar(key.at(j), getSerialKeyChar(pBuf + uChildrenIdx, k)))
                {
                    break;
                }
            }
        }

        if (j == 0)
        {
            if (len == 0 || len > 10)
            {
                continue;
            }
            if (k >= len)
            {
                consumeNum += *pConsume;
                bRet = processFindInSerialAsMulti(pBuf, uChildrenIdx, key.substr(j), &consumeNum, reStr + getSerialKey(pBuf, uChildrenIdx).substr(0, k), lastKey, type);
            }

            continue;
        }
        else
        {
            consumeNum += (j + *pConsume);
            phraseCand.reset();

            if (j < key.size() && k >= len)
            {
                phraseCand.setReString(reStr + getSerialKey(pBuf, uChildrenIdx).substr(0, k));
                bRet = processFindInSerialAsMulti(pBuf, uChildrenIdx, key.substr(j), &consumeNum, phraseCand.getReString(), lastKey, type);
                phraseCand.setData(uChildrenIdx, consumeNum);

                if (getSerialIsTerminal(pBuf + uChildrenIdx) && isSerialMatchType(nodeFlag, type))
                {
                    phraseCand.setNodeFlag(nodeFlag);
                    if (processPutCandToSerialList(phraseCand))
                    {
                        bHasResult = true;
                    }
                }
            }

            if (j == key.size())
            {
                bHasResult = true;

                if (getSerialIsTerminal(pBuf + uChildrenIdx) && isSerialMatchType(nodeFlag, type))
                {
                    phraseCand.setData(uChildrenIdx, consumeNum);
                    phraseCand.setReString(reStr + getSerialKey(pBuf, uChildrenIdx));
                    phraseCand.setNodeFlag(nodeFlag);
                    processPutCandToSerialList(phraseCand);
                }

                if (key.compare(lastKey.c_str()))
                {
                    allSubNodeList.clear();
                    getAllTermNodeIdx(pBuf, uChildrenIdx, allSubNodeList, uMaxWideSearchNum, type);
                    if (lastKey.size() > 0 && (lastKey.at(lastKey.size() - 1) == 0x20))
                    {
                        lastKey.clear();
                        lastKey.insert(0, key);
                    }
                    else
                    {
                        lastKey.clear();
                        lastKey.insert(0, key);
                        lastKey.insert(key.size(), 1, 0x20);
                    }
                }
                else
                {
                    continue;
                }

                if (allSubNodeList.size() > 0)
                {
                    allSubNodeList.sort();
                    allSubNodeList.reverse();
                }

                uCnt = 0;
                while (allSubNodeList.size() > 0)
                {
                    termNodePair = allSubNodeList.front();
                    allSubNodeList.pop_front();

                    phraseCand.setData(termNodePair.getNodeIdx(), consumeNum);
                    phraseCand.setReString(reStr + getSerialKey(pBuf, uChildrenIdx) + termNodePair.getReStr());
                    phraseCand.setNodeFlag(getSerialNodeFlag(pBuf + termNodePair.getNodeIdx()));
                    processPutCandToSerialList(phraseCand);

                    uCnt++;

                    if (uCnt >= uMaxCnt)
                    {
                        break;
                    }
                }
            }

            continue;
        }
    }

    return bHasResult;
}

template <class TString, class TChar>
char PatriciaTrie<TString, TChar>::processFindInSerialAsDistEx(unsigned char* pBuf, unsigned int uNodeIdx,
    TString key, vector<unsigned char> prevDist, TString& prevStr, unsigned char consume, unsigned int uPrevEqualCnt, unsigned int type)
{
    unsigned int i, j, len;
    const unsigned int ucMaxEqualCnt = 3;
    unsigned char childrenLen, nodeFlag;
    unsigned char* pChildren = NULL;
    unsigned char* pStrBuf = NULL;
    unsigned int uChildrenIdx;
    unsigned char consumeNum, uDist = 0;
    unsigned char prevDistValue = prevDist.at(prevDist.size() - 1);
    TString curNodeStr;
    char ret = 0;
    char hasResult = 0;
    MultiSerialPhrase<TString> phraseCand;
    typename list<MultiSerialPhrase<TString> > ::iterator phraseCandItor;
    vector<unsigned char> curDist;
    DistSerialPhrase<TString> phraseDist;

    pChildren = getSerialChildBuf(pBuf + uNodeIdx);

    childrenLen = *pChildren;
    uChildrenIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

    for (i = 0; i < childrenLen; i++, uChildrenIdx = getSerialNextNodeIdx(pBuf , uChildrenIdx))
    {
        curDist = prevDist;
        consumeNum = 0;
        curNodeStr.clear();
        phraseCand.reset();
        phraseDist.reset();

        len = getSerialKeyLen(pBuf + uChildrenIdx);
        nodeFlag = getSerialNodeFlag(pBuf + uChildrenIdx);
        pStrBuf = getSerialStrBuf(pBuf + uChildrenIdx);

        for (j = 0; j < len; j++)
        {
            if (getSerialIsUnicode(pBuf + uChildrenIdx))
            {
                curNodeStr.push_back((*pStrBuf << 8) | *(pStrBuf + 1));
                pStrBuf += 2;
            }
            else
            {
                curNodeStr.push_back(*pStrBuf);
                pStrBuf += 1;
            }
        }
        uDist = levenshteinDistance::getDistance(key, curNodeStr, curDist);
        phraseDist.setDist(curDist);
        curNodeStr.insert(0, prevStr);

        if (uDist < prevDistValue || (uDist == prevDistValue && uPrevEqualCnt < ucMaxEqualCnt))
        {
            consumeNum += (len + consume);
            ret = processFindInSerialAsDistEx(pBuf, uChildrenIdx, key, curDist,
                curNodeStr, consumeNum, uPrevEqualCnt + ((uDist == prevDistValue) ? 1 : 0), type);

            //if (ret < 0 && uDist <= len)
            if (ret < 0 && uDist <= 1 && isSerialMatchType(nodeFlag, type))
            {
                hasResult |= 0x80;
                phraseDist.setData(uChildrenIdx, consumeNum);
                phraseDist.insertReString(curNodeStr);
                distSess.pushDistPhrase(phraseDist);
            }

            if (!(ret & 0x01) && getSerialIsTerminal(pBuf + uChildrenIdx) && uDist <= 1 && isSerialMatchType(nodeFlag, type))
            {
                hasResult |= 0x01;
                phraseCand.setData(uChildrenIdx, consumeNum);
                phraseCand.insertReString(curNodeStr);
                retSerialList.push_front(phraseCand);
            }
        }
        else
        {
            hasResult |= 0x80;
            //phraseDist.setData(uChildrenIdx, consumeNum);
            //phraseDist.insertReString(curNodeStr);
            //distSess.pushDistPhrase(phraseDist);

            if (getSerialIsTerminal(pBuf + uChildrenIdx) && uDist <= 1 && isSerialMatchType(nodeFlag, type))
            {
                hasResult |= 0x01;
                consumeNum += (len + consume);
                phraseCand.setData(uChildrenIdx, consumeNum);
                phraseCand.insertReString(curNodeStr);
                retSerialList.push_front(phraseCand);
            }
        }

        continue;
    }

    return hasResult;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processFindInSerialAsTrack(unsigned char* pBuf, unsigned int uNodeIdx,
    TString& prevStr, vector<TrackChar<TChar> > & allTrackCharVec, vector<TrackChar<TChar> > & allTrustCharVec,
    list<CandItem<TString> > & cand)
{
    unsigned int prevIdx, i, j, k, len, matchCnt;
    unsigned char childrenLen;
    unsigned char* pChildren = NULL;
    unsigned char* pStrBuf = NULL;
    unsigned int uChildrenIdx;
    TString curNodeStr;
    CandItem<TString> candResult;
    bool bHasResult = false;
    float fWordFreq = 0.0;
    float fTmpFreq = 0.0;
    const float lambda = 0.7;

    pChildren = getSerialChildBuf(pBuf + uNodeIdx);

    childrenLen = *pChildren;
    uChildrenIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

    for (k = 0; k < childrenLen; k++, uChildrenIdx = getSerialNextNodeIdx(pBuf , uChildrenIdx))
    {
        len = getSerialKeyLen(pBuf + uChildrenIdx);
        pStrBuf = getSerialStrBuf(pBuf + uChildrenIdx);

        if (len == 0 || (prevStr.size() == 0 && isBaseCharInTrack(getSerialKeyChar(pBuf + uChildrenIdx, 0), allTrackCharVec.front())))
        {
            continue;
        }

        curNodeStr = prevStr;
        for (j = 0; j < len; j++)
        {
            curNodeStr.push_back((TChar)*(pStrBuf + j));
        }
        if (curNodeStr.size() <= allTrackCharVec.size() && curNodeStr.size() < allTrustCharVec.size()+3)
        {
            processFindInSerialAsTrack(pBuf, uChildrenIdx, curNodeStr, allTrackCharVec, allTrustCharVec, cand);
        }

        if (getSerialIsTerminal(pBuf + uChildrenIdx))
        {
            if (curNodeStr.size()+1 < allTrustCharVec.size() || curNodeStr.size() > allTrustCharVec.size()+2)
            {
                continue;
            }
            if (isBaseCharNearTrack(getSerialKeyChar(pBuf + uChildrenIdx, len - 1), allTrackCharVec.back()))
            {
                fWordFreq = 0.0;
            }
            else
            {
                fWordFreq = 2.0;
            }

            matchCnt = 0;
            i = 0, j = 0;
            prevIdx = 0;

            while (j < curNodeStr.size())
            {
                for (i = prevIdx; i < allTrackCharVec.size(); i++)
                {
                    if (!isBaseCharInTrack(curNodeStr.at(j), allTrackCharVec.at(i)))
                    {
                        fTmpFreq = allTrackCharVec.at(i).getFreq();
                        matchCnt += (fTmpFreq > lambda) ? 1 : 0;
                        prevIdx = i + 1;
                        fWordFreq += fTmpFreq;
                        break;
                    }
                }

                if (i == allTrackCharVec.size())
                {
                    fWordFreq -= 0.8;
                }

                j++;
            }

            if (matchCnt + 2 > allTrustCharVec.size())
            {
                candResult.set(curNodeStr, fWordFreq);
                cand.push_back(candResult);
                bHasResult = true;
            }
        }
    }

    return bHasResult;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processFindInSerialAsTrackCorrection(unsigned char* pBuf, unsigned int uNodeIdx,
    TString& prevStr, vector<TrackChar<TChar> > & allTrackCharVec, vector<TrackChar<TChar> > & allTrustCharVec,
    list<CandItem<TString> > & cand)
{
    unsigned int prevIdx, i, j, k, len, matchCnt;
    unsigned char childrenLen;
    unsigned char* pChildren = NULL;
    unsigned char* pStrBuf = NULL;
    unsigned int uChildrenIdx;
    TString curNodeStr;
    CandItem<TString> candResult;
    bool bHasResult = false;
    float fWordFreq = 0.0;
    float fTmpFreq = 0.0;
    const float lambda = 0.7;

    pChildren = getSerialChildBuf(pBuf + uNodeIdx);

    childrenLen = *pChildren;
    uChildrenIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

    for (k = 0; k < childrenLen; k++, uChildrenIdx = getSerialNextNodeIdx(pBuf , uChildrenIdx))
    {
        len = getSerialKeyLen(pBuf + uChildrenIdx);
        pStrBuf = getSerialStrBuf(pBuf + uChildrenIdx);

        if (len == 0 ||(prevStr.size() == 0 && isBaseCharNearTrack(getSerialKeyChar(pBuf + uChildrenIdx, 0), allTrackCharVec.front())))
        {
            continue;
        }

        curNodeStr = prevStr;
        for (j = 0; j < len; j++)
        {
            curNodeStr.push_back((TChar)*(pStrBuf + j));
        }

        if (curNodeStr.size() <= allTrackCharVec.size())
        {
            processFindInSerialAsTrackCorrection(pBuf, uChildrenIdx, curNodeStr, allTrackCharVec, allTrustCharVec, cand);
        }

        if (getSerialIsTerminal(pBuf + uChildrenIdx))
        {
            if (isBaseCharNearTrack(getSerialKeyChar(pBuf + uChildrenIdx, len - 1),
                allTrackCharVec.back()))
            {
                continue;
            }

            matchCnt = 0;
            prevIdx = 0;
            i = 0, j = 0;
            fWordFreq = 0.0;

            while (j < curNodeStr.size())
            {
                for (i = prevIdx; i < allTrackCharVec.size(); i++)
                {
                    if (i == 0 && !isBaseCharNearTrack(curNodeStr.at(j), allTrackCharVec.at(i)))
                    {
                        fTmpFreq = allTrackCharVec.at(i).getFreq();
                        matchCnt += (fTmpFreq > lambda) ? 1 : 0;
                        prevIdx = i + 1;
                        fWordFreq += fTmpFreq;
                        break;
                    }
                    else if (!isBaseCharInTrack(curNodeStr.at(j), allTrackCharVec.at(i)))
                    {
                        fTmpFreq = allTrackCharVec.at(i).getFreq();
                        matchCnt += (fTmpFreq > lambda) ? 1 : 0;
                        prevIdx = i + 1;
                        fWordFreq += fTmpFreq;
                        break;
                    }
                }

                if (i == allTrackCharVec.size())
                {
                    fWordFreq -= 0.8;
                }

                j++;
            }

            if (matchCnt + 2 > allTrustCharVec.size())
            {
                candResult.set(curNodeStr, fWordFreq);
                cand.push_back(candResult);
                bHasResult = true;
            }
        }
    }

    return bHasResult;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processFindInSerialAsDist(unsigned char* pBuf, unsigned int uNodeIdx,
    TString key, vector<unsigned char> prevDist, TString& prevStr, unsigned char consume, unsigned int uPrevEqualCnt, unsigned int type)
{
    unsigned int i, j, len;
    const unsigned int ucMaxEqualCnt = 3;
    unsigned char childrenLen, nodeFlag;
    unsigned char* pChildren = NULL;
    unsigned char* pStrBuf = NULL;
    unsigned int uChildrenIdx;
    unsigned char consumeNum, uDist = 0;
    unsigned char prevDistValue = prevDist.at(prevDist.size() - 1);
    TString curNodeStr;
    bool bRet = false;
    bool bHasResult = false;
    MultiSerialPhrase<TString> phraseCand;
    typename list<MultiSerialPhrase<TString> > ::iterator phraseCandItor;
    vector<unsigned char> curDist;

    pChildren = getSerialChildBuf(pBuf + uNodeIdx);

    childrenLen = *pChildren;
    uChildrenIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

    for (i = 0; i < childrenLen; i++, uChildrenIdx = getSerialNextNodeIdx(pBuf , uChildrenIdx))
    {
        curDist = prevDist;
        consumeNum = 0;
        curNodeStr.clear();
        phraseCand.reset();

        len = getSerialKeyLen(pBuf + uChildrenIdx);
        nodeFlag = getSerialNodeFlag(pBuf + uChildrenIdx);
        pStrBuf = getSerialStrBuf(pBuf + uChildrenIdx);

        for (j = 0; j < len; j++)
        {
            if (getSerialIsUnicode(pBuf + uChildrenIdx))
            {
                curNodeStr.push_back((*pStrBuf << 8) | *(pStrBuf + 1));
                pStrBuf += 2;
            }
            else
            {
                curNodeStr.push_back(*pStrBuf);
                pStrBuf += 1;
            }
        }
        uDist = levenshteinDistance::getDistance(key, curNodeStr, curDist);

        if (uDist < prevDistValue || (uDist == prevDistValue && uPrevEqualCnt < ucMaxEqualCnt))
        {
            consumeNum += (len + consume);
            curNodeStr.insert(0, prevStr);
            bRet = processFindInSerialAsDist(pBuf, uChildrenIdx, key, curDist,
                curNodeStr, consumeNum, uPrevEqualCnt + ((uDist == prevDistValue) ? 1 : 0), type);

            if (!bRet && getSerialIsTerminal(pBuf + uChildrenIdx) && uDist <= 1 && isSerialMatchType(nodeFlag, type))
            {
                bHasResult = true;
                phraseCand.setData(uChildrenIdx, consumeNum);
                phraseCand.insertReString(curNodeStr);
                retSerialList.push_front(phraseCand);
            }
        }
        else if (getSerialIsTerminal(pBuf + uChildrenIdx) && uDist <= 1 && isSerialMatchType(nodeFlag, type))
        {
            bHasResult = true;
            consumeNum += (len + consume);
            curNodeStr.insert(0, prevStr);
            phraseCand.setData(uChildrenIdx, consumeNum);
            phraseCand.insertReString(curNodeStr);
            retSerialList.push_front(phraseCand);
        }

        continue;
    }

    return bHasResult;
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::storePredictSerialWeightListEx(unsigned char* pBuf , MultiSerialPhrase<TString>& phrase,
    int iLevel, TString prevStr, float prevWeight, float dPhraseWeight, list<CandItem<TString> > & lst, unsigned int type)
{
    TString candStr;
    float fWeight;
    PatriciaTrieNode<TString, TChar> node;
    list<pairTermNode<TString> >  termNodeList;
    pairTermNode<TString> termNodePair;
    typename list<CandItem<TString> > ::iterator itor;
    typename list<DistSerialPhrase<TString> > ::iterator distItor;

    if (phrase.subListSerial.size() == 0)
    {
        if (iLevel == 0)
        {
            list<DistSerialPhrase<TString> > & lastDist = distSess.distHistoryBack();
            for (distItor = lastDist.begin(); distItor != lastDist.end(); distItor++)
            {
                CandItem<TString> cand(distItor->getReString(), dPhraseWeight * getSerialNodeFreq(pBuf + distItor->getNodeId()));
                lst.push_back(cand);
            }
            return;
        }

        for (itor = lst.begin(); itor != lst.end(); itor++)
        {
            if (!prevStr.compare(itor->getCand()))
            {
                if (dPhraseWeight * prevWeight / iLevel > itor->getWeight())
                {
                    itor->setWeight(dPhraseWeight * prevWeight / iLevel);
                }
                return;
            }
        }

        CandItem<TString> cand(prevStr, dPhraseWeight * prevWeight / iLevel);
        lst.push_back(cand);
        return;
    }

    while (phrase.subListSerial.size() > 0)
    {
        MultiSerialPhrase<TString>& candPhrase = phrase.subListSerial.front();

        if (candPhrase.getWeight() < 0)
        {
            phrase.subListSerial.pop_front();
            break;
        }

        // handle all node.
        if (candPhrase.isMiniWeight())
        {
            getAllTermNodeIdx(pBuf, candPhrase.getNodeId(), termNodeList, 10, type);

            if (termNodeList.size() > 0)
            {
                termNodeList.sort();
            }

            while (termNodeList.size() > 0)
            {
                termNodePair = termNodeList.front();
                unsigned int nodeData = (unsigned int)termNodePair.getIdx();
                termNodeList.pop_front();

                fWeight = prevWeight;
                candStr = prevStr;
                candStr = candPhrase.getReString() + termNodePair.getReStr();
                candStr.push_back((TChar)0x20);

                fWeight += ((nodeData & 0xFF000000) >> 24) * candPhrase.getWeight();

                CandItem<TString> cand(candStr, dPhraseWeight * fWeight / (iLevel + 1));
                lst.push_back(cand);
            }

            phrase.subListSerial.pop_front();
            continue;
        }
        else
        {
            candStr = prevStr;
            candStr += candPhrase.getReString() ;
            candStr.push_back((TChar)0x20);

            if (candPhrase.subListSerial.size() == 0 && candPhrase.getWeight() == 1.0)
            {
                CandItem<TString> cand(candStr, dPhraseWeight * getSerialNodeFreq(pBuf + candPhrase.getNodeId()) * candPhrase.getWeight() / (iLevel + 1));
                lst.push_back(cand);

                phrase.subListSerial.pop_front();
                continue;
            }
            else
            {
                fWeight = prevWeight;
                fWeight += getSerialNodeFreq(pBuf + candPhrase.getNodeId()) * candPhrase.getWeight();
            }
        }

        storePredictSerialWeightList(pBuf , candPhrase, iLevel + 1, candStr, fWeight, dPhraseWeight, lst, type);
        phrase.subListSerial.pop_front();
    }
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processDelete(PatriciaTrieNode<TString, TChar>* pParent, int idx)
{
    int iChildNum = 0;

    if (!pParent)
    {
        return false;
    }

    iChildNum = (int)pParent->children.size();

    if (idx >= iChildNum)
    {
        return false;
    }

    if (pParent->children.at(idx).children.size() == 0)
    {
        pParent->children.erase(pParent->children.begin() + idx);
    }
    else
    {
        pParent->children.at(idx).setTerminal(false);
    }

    // merge node for currNode
    if (!pParent->isTerminal() && pParent->children.size() == 1)
    {
        PatriciaTrieNode<TString, TChar>& singleChild = pParent->children.at(0);
        pParent->appendStr(singleChild.getKeyStr());
        pParent->setData(singleChild.getData());
        pParent->setTerminal(singleChild.isTerminal());
        pParent->children = singleChild.children;
    }

    return true;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::processDelete(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned int type)
{
    bool done = false;
    unsigned int i, j, len;

    for (i = 0; i < currNode.children.size(); i++)
    {
        PatriciaTrieNode<TString, TChar>& child = currNode.children.at(i);

        len = (unsigned int)(child.getKeyStrLen() < key.length() ? child.getKeyStrLen() : key.length());
        for (j = 0; j < len; j++)
        {
            if (levenshteinDistance::preciseCompChar(key.at(j), child.getKeychar(j)))
            {
                break;
            }
        }

        if (j == 0)
        {
            if (levenshteinDistance::preciseCompChar(key.at(0), child.getKeychar(0)) < 0)
            {
                return false;
            }
            else
            {
                continue;
            }
        }
        else
        {
            if (key.length() == child.getKeyStrLen() && child.isTerminal() && child.isMatchType(type))
            {
                // found key, del it
                if (child.children.size() == 0)
                {
                    currNode.children.erase(currNode.children.begin() + i);
                }
                else
                {
                    child.setTerminal(false);
                    child.setTypeOne(false);
                    child.setTypeTwo(false);
                }

                // merge node for currNode
                if (!currNode.isTerminal() && currNode.children.size() == 1)
                {
                    PatriciaTrieNode<TString, TChar> & singleChild = currNode.children.at(0);
                    currNode.appendStr(singleChild.getKeyStr());
                    currNode.setData(singleChild.getData());
                    currNode.setTerminal(singleChild.isTerminal());
                    currNode.setTypeOne(singleChild.isTypeOne());
                    currNode.setTypeTwo(singleChild.isTypeTwo());
                    currNode.children = singleChild.children;
                }

                done = true;
            }
            else if (key.length() > child.getKeyStrLen())
            {
                done = processDelete(child, key.substr(j), type);
            }

            break;
        }
    }

    if (!done)
    {
        printf("Not found. \n");
    }

    return done;
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::insert(TString key, unsigned int value, list<bigramDataNode<TString> > & lstBigramData, unsigned int type)
{
    if (key.length() == 0)
    {
        return;
    }

    processInsert(root, key, value, lstBigramData, type);
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::processInsert(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned int value, list<bigramDataNode<TString> > & lstBigramData, unsigned int type)
{
    bool done = false;
    unsigned int i, j, len;
    typename list<bigramDataNode<TString> > ::iterator itor;

    for (i = 0; i < currNode.children.size(); i++)
    {
        PatriciaTrieNode<TString, TChar>& child = currNode.children.at(i);

        len = (unsigned int)(child.getKeyStrLen() < key.length() ? child.getKeyStrLen() : key.length());
        for (j = 0; j < len; j++)
        {
            if (levenshteinDistance::preciseCompChar(key.at(j), child.getKeychar(j)))
            {
                break;
            }
        }

        if (j == 0)
        {
            if (levenshteinDistance::preciseCompChar(key.at(0), child.getKeychar(0)) < 0)
            {
                PatriciaTrieNode<TString, TChar> node(key);
                node.setTerminal(true);
                node.setData(value);
                if (type == TYPE_ONE)
                {
                    node.setTypeOne(true);
                }
                else if (type == TYPE_TWO)
                {
                    node.setTypeTwo(true);
                }

                for (itor = lstBigramData.begin(); itor != lstBigramData.end(); itor++)
                {
                    node.biData.push_back(*itor);
                }

                if (key.length() > 15)
                {
                    int subIdx = key.length() / 2;
                    PatriciaTrieNode<TString, TChar> subKeyNode(node.getKeySubStr(subIdx));
                    subKeyNode.setTerminal(node.isTerminal());
                    subKeyNode.setData(node.getData());
                    subKeyNode.setTypeOne(node.isTypeOne());
                    subKeyNode.setTypeTwo(node.isTypeTwo());
                    subKeyNode.biData = node.biData;

                    node.setKeyStr(key.substr(0, subIdx));
                    node.setTerminal(false);
                    node.setData(0);
                    node.setTypeOne(false);
                    node.setTypeTwo(false);
                    node.biData.clear();
                    node.children.clear();
                    node.children.insert(node.children.begin(), subKeyNode);
                }

                currNode.children.insert(currNode.children.begin() + i, node);
                done = true;
                break;
            }
            else
            {
                continue;
            }
        }
        else
        {
            if (j == len)
            {
                if (key.length() == child.getKeyStrLen())
                {
                    child.setTerminal(true);
                    child.setData(value);
                    if (type == TYPE_ONE)
                    {
                        child.setTypeOne(true);
                    }
                    else if (type == TYPE_TWO)
                    {
                        child.setTypeTwo(true);
                    }

                    for (itor = lstBigramData.begin(); itor != lstBigramData.end(); itor++)
                    {
                        child.biData.push_back(*itor);
                    }
                }
                else if (key.length() > child.getKeyStrLen())
                {
                    processInsert(child, key.substr(j), value, lstBigramData, type);
                }
                else
                {
                    PatriciaTrieNode<TString, TChar> subChildNode(child.getKeySubStr(j));
                    subChildNode.setTerminal(child.isTerminal());
                    subChildNode.setData(child.getData());
                    subChildNode.setTypeOne(child.isTypeOne());
                    subChildNode.setTypeTwo(child.isTypeTwo());

                    subChildNode.children = child.children;
                    subChildNode.biData = child.biData;

                    child.setKeyStr(key);
                    child.setTerminal(true);
                    child.setData(value);
                    child.setTypeOne(false);
                    child.setTypeTwo(false);
                    if (type == TYPE_ONE)
                    {
                        child.setTypeOne(true);
                    }
                    else if (type == TYPE_TWO)
                    {
                        child.setTypeTwo(true);
                    }

                    child.biData.clear();
                    for (itor = lstBigramData.begin(); itor != lstBigramData.end(); itor++)
                    {
                        child.biData.push_back(*itor);
                    }

                    child.children.clear();
                    child.children.insert(child.children.begin(), subChildNode);
                }
            }
            else
            {
                TString& childSubkey = child.getKeySubStr(j);
                TString& subkey = key.substr(j);

                PatriciaTrieNode<TString, TChar> subChildNode(childSubkey);
                subChildNode.setTerminal(child.isTerminal());
                subChildNode.setData(child.getData());
                subChildNode.setTypeOne(child.isTypeOne());
                subChildNode.setTypeTwo(child.isTypeTwo());

                subChildNode.children = child.children;
                subChildNode.biData = child.biData;

                // update child's key
                child.setKeyStr(child.getKeySubStr(0, j));
                child.setTerminal(false);
                child.setData(0);
                child.setTypeOne(false);
                child.setTypeTwo(false);
                child.biData.clear();
                child.children.clear();

                PatriciaTrieNode<TString, TChar> node(subkey);
                node.setTerminal(true);
                node.setData(value);
                if (type == TYPE_ONE)
                {
                    node.setTypeOne(true);
                }
                else if (type == TYPE_TWO)
                {
                    node.setTypeTwo(true);
                }

                for (itor = lstBigramData.begin(); itor != lstBigramData.end(); itor++)
                {
                    node.biData.push_back(*itor);
                }

                if (subkey.length() > 15)
                {
                    int subIdx = subkey.length() / 2;
                    PatriciaTrieNode<TString, TChar> subKeyNode(node.getKeySubStr(subIdx));
                    subKeyNode.setTerminal(node.isTerminal());
                    subKeyNode.setData(node.getData());
                    subKeyNode.setTypeOne(node.isTypeOne());
                    subKeyNode.setTypeTwo(node.isTypeTwo());
                    subKeyNode.biData = node.biData;

                    node.setKeyStr(node.getKeySubStr(0, subIdx));
                    node.setTerminal(false);
                    node.setData(0);
                    node.setTypeOne(false);
                    node.setTypeTwo(false);
                    node.biData.clear();
                    node.children.clear();
                    node.children.insert(node.children.begin(), subKeyNode);
                }

                if(levenshteinDistance::preciseCompChar(node.getKeychar(0), childSubkey.at(0)) < 0)
                {
                    child.children.insert(child.children.begin(), subChildNode);
                    child.children.insert(child.children.begin(), node);
                }
                else
                {
                    child.children.insert(child.children.begin(), node);
                    child.children.insert(child.children.begin(), subChildNode);
                }
            }

            done = true;
            break;
        }
    }

    if (!done)
    {
        PatriciaTrieNode<TString, TChar> node(key);
        node.setTerminal(true);
        node.setData(value);
        if (type == TYPE_ONE)
        {
            node.setTypeOne(true);
        }
        else if (type == TYPE_TWO)
        {
            node.setTypeTwo(true);
        }

        for (itor = lstBigramData.begin(); itor != lstBigramData.end(); itor++)
        {
            node.biData.push_back(*itor);
        }

        if (key.length() > 15)
        {
            int subIdx = key.length() / 2;
            PatriciaTrieNode<TString, TChar> subKeyNode(node.getKeySubStr(subIdx));
            subKeyNode.setTerminal(node.isTerminal());
            subKeyNode.setData(node.getData());
            subKeyNode.setTypeOne(node.isTypeOne());
            subKeyNode.setTypeTwo(node.isTypeTwo());
            subKeyNode.biData = node.biData;

            node.setKeyStr(node.getKeySubStr(0, subIdx));
            node.setTerminal(false);
            node.setData(0);
            node.setTypeOne(false);
            node.setTypeTwo(false);
            node.biData.clear();
            node.children.clear();
            node.children.insert(node.children.begin(), subKeyNode);
        }



        currNode.children.insert(currNode.children.end(), node);
    }
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::predictInStr(TString key, int iLevel, unsigned int type)
{
    unsigned char uConsume = 0;
    PatriciaTrieNode<TString, TChar>* pNode;
    MultiPhrase<TString, TChar> candPhrase;
    int i;

    retList.clear();
    pNode = find(key, AS_MULTI, true, &uConsume, type);

    if (retList.size() <= 0)
    {
        return;
    }

    list<MultiPhrase<TString, TChar> >  candList = retList;

    while (candList.size() > 0)
    {
        candPhrase = candList.front();
        candList.pop_front();

        pNode = candPhrase.getNode();

        for (i = 0; i < iLevel; i++)
        {
            printf("\t");
        }

        if (pNode->isTerminal())
        {
            printf("Found key %s .\n", key.substr(0, candPhrase.getConsumeNum()).c_str());
            predictInStr(key.substr(candPhrase.getConsumeNum()), iLevel + 1, type);
        }
        else
        {
            printf("All sub node %s at: %s\n", key.substr(0, candPhrase.getConsumeNum()).c_str(), pNode->getKeyStr().c_str());
        }
    }
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::predictList(TString key, MultiPhrase<TString, TChar>& parent, unsigned int type)
{
    unsigned char uConsume = 0;
    PatriciaTrieNode<TString, TChar>* pNode;

    retList.clear();
    pNode = find(key, AS_MULTI, true, &uConsume, type);

    if (retList.size() <= 0)
    {
        return;
    }

    list<MultiPhrase<TString, TChar> >  candList = retList;

    while (candList.size() > 0)
    {
        MultiPhrase<TString, TChar>& candPhrase = candList.front();
        pNode = candPhrase.getNode();

        if (pNode->isTerminal())
        {
            // no bi check since no bi compare support on trie.
            predictList(key.substr(candList.front().getConsumeNum()), candPhrase, type);
            parent.subList.push_back(candPhrase);
        }
        else if (candPhrase.getConsumeNum() == key.size())
        {
            candPhrase.setMiniWeight();
            parent.subList.push_back(candPhrase);
        }
        candList.pop_front();
    }
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::predictDistInSerialEx(unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent, unsigned int uPrevIdx, unsigned int type)
{
    unsigned char uConsume = 0;
    unsigned int i, uCnt, uNodeIdx = 0;
    bool bFindCand = false;
    vector<unsigned char> prevDist;
    TString nulStr;
    DistSerialPhrase<TString> phraseDist;

    nulStr.clear();
    for (i = 0; i <= key.size(); i++)
    {
        prevDist.push_back(i);
    }

    retSerialList.clear();

    distSess.resetDistList();
    // start
    if (distSess.isStart())
    {
        bFindCand = processFindInSerialAsDistEx(pBuf, 0, key, prevDist, nulStr, uConsume, 0, type);
    }
    // continue
    else
    {
        typename list<DistSerialPhrase<TString> > ::iterator distPhraseItor = distSess.distHistoryBack().begin();
        uCnt = distSess.distHistoryBack().size();

        for (i = 0; i < uCnt; i++, distPhraseItor++)
        {
            phraseDist.reset();
            distPhraseItor->pushDistValue((unsigned char)key.size());
            prevDist = distPhraseItor->getDist();
            if (levenshteinDistance::getDistance(key, distPhraseItor->getReString(), prevDist) <= 1)
            {
                phraseDist.setData(distPhraseItor->getNodeId(), distPhraseItor->getConsumeNum());
                phraseDist.setReString(distPhraseItor->getReString());
                phraseDist.setDist(prevDist);
                distSess.pushDistPhrase(phraseDist);
            }

            bFindCand = processFindInSerialAsDistEx(pBuf, distPhraseItor->getNodeId(), key, distPhraseItor->getDist(), distPhraseItor->getReString(), uConsume, 0, type);
        }
    }

    while (retSerialList.size() > 0)
    {
        MultiSerialPhrase<TString>& candPhrase = retSerialList.front();
        uNodeIdx = candPhrase.getNodeId();

        if (getSerialIsTerminal(pBuf + uNodeIdx))
        {
            parent.subListSerial.push_back(candPhrase);
        }
        retSerialList.pop_front();
    }
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::predictTrackInSerial(unsigned char* pBuf,
    vector<TrackChar<TChar> > & allTrackCharVec, vector<TrackChar<TChar> > & allTrustCharVec,
    vector<TString>& cand)
{
    TString nulStr;
    list<CandItem<TString> > candList;
    typename list<CandItem<TString> > ::iterator itor;
    bool bRet = false;

    nulStr.clear();
    bRet = processFindInSerialAsTrack(pBuf, 0, nulStr, allTrackCharVec, allTrustCharVec, candList);

    if (candList.size() == 0)
    {
 //       bRet = processFindInSerialAsTrackCorrection(pBuf, 0, nulStr, allTrackCharVec, allTrustCharVec, candList);
    }

    candList.sort();
    candList.reverse();

    for (itor = candList.begin(); itor != candList.end(); itor++)
    {
        cand.push_back(itor->getCand());
    }

    return bRet;
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::predictDistInSerial(unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent, unsigned int uPrevIdx, unsigned int type)
{
    unsigned char uConsume = 0;
    unsigned int  uNodeIdx = 0;
    bool bFindCand = false;
    vector<unsigned char> prevDist;
    TString nulStr;

    nulStr.clear();
    for (int i = 0; i <= key.size(); i++)
    {
        prevDist.push_back(i);
    }

    retSerialList.clear();
    bFindCand = processFindInSerialAsDist(pBuf, 0, key, prevDist, nulStr, uConsume, 0, type);

    while (retSerialList.size() > 0)
    {
        MultiSerialPhrase<TString>& candPhrase = retSerialList.front();
        uNodeIdx = candPhrase.getNodeId();

        if (getSerialIsTerminal(pBuf + uNodeIdx))
        {
            parent.subListSerial.push_back(candPhrase);
        }
        retSerialList.pop_front();
    }
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::predictListInSerial(unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent, MultiSerialPhrase<TString>& prevCand, TString& lastKey, unsigned char uKeyLen)
{
    unsigned char uConsume = 0;
    unsigned char biIdx = 0;
    unsigned char* pBiData = NULL;
    unsigned int uCnt, childIdx, uNodeIdx = 0;
    unsigned int uPrevIdx = prevCand.getNodeId();
    float preCandWeight = 0;
    bool bFindCand = false, ret=true;
    TString multiStr;
    unsigned char keyLen = (uKeyLen == 0) ? key.size() : uKeyLen;

    if (key.size() == 0)
    {
        return true;
    }
    retSerialList.clear();
    bFindCand = processFindInSerialAsMulti(pBuf, 0, key, &uConsume, multiStr, lastKey);

    if (retSerialList.size() <= 0)
    {
        return false;
    }

    preCandWeight = prevCand.getWeight();
    list<MultiSerialPhrase<TString> > candList = retSerialList;

    while (candList.size() > 0)
    {
        MultiSerialPhrase<TString>& candPhrase = candList.front();
        uNodeIdx = candPhrase.getNodeId();

        if (candPhrase.getConsumeNum() < key.size())
        {
            candPhrase.setWeight(0.5 * preCandWeight);
        }
        else
        {
            candPhrase.setWeight(preCandWeight);
        }

        if (getSerialIsTerminal(pBuf + uNodeIdx))
        {
            if (getSerialNodeFreq(pBuf + uNodeIdx) < 50)
            {
                candList.pop_front();
                continue;
            }

            ret = predictListInSerial(pBuf, key.substr(candPhrase.getConsumeNum()), candPhrase, candPhrase, lastKey, keyLen);

            if (ret == false)
            {
                candList.pop_front();
                continue;
            }
            // check Bi.
            if (uPrevIdx > 0)
            {
                pBiData = getSerialBiDataBuf(pBuf + uPrevIdx);
                for (biIdx = 0; biIdx < getSerialBiDataLen(pBuf + uPrevIdx); biIdx++)
                {
                    if (uNodeIdx == readU24(pBiData + biIdx * 4))
                    {
                        candPhrase.biReward(BI_REWARD);
                        prevCand.biReward(BI_REWARD);
                        break;
                    }
                }

                // multi punishment.
                if (candPhrase.getReString().size() != key.size())
                {
                    candPhrase.setWeight(candPhrase.getWeight() * MULTI_PUNISH);
                }
                else
                {
                    candPhrase.setWeight(candPhrase.getWeight() * 2);
                }
            }

            if ((parent.getNodeFlag() & 0x0C) == 0 || (parent.getNodeFlag() & candPhrase.getNodeFlag() & 0x0C))
            {
                if ((parent.getNodeFlag() & 0x0C) == 0x08)
                {
                    candPhrase.setNodeFlag(0x08);
                }
                else if ((parent.getNodeFlag() & 0x0C) == 0x04)
                {
                    candPhrase.setNodeFlag(0x04);
                }
                parent.subListSerial.push_back(candPhrase);
            }
        }
        else // missing in end.
        {
            list<unsigned int> list;
            bool bFound = false;
            list.push_back(candPhrase.getNodeId());

            while (!list.empty())
            {
                uNodeIdx = list.front();
                list.pop_front();

                childIdx = getSerialFirstChildIdx(pBuf + uNodeIdx);

                for (uCnt = 0; uCnt < getSerialChildLen(pBuf + uNodeIdx); uCnt++, childIdx = getSerialNextNodeIdx(pBuf, childIdx))
                {
                    list.push_back(childIdx);

                    if (getSerialIsTerminal(pBuf + childIdx))
                    {
                        candPhrase.setWeight(0.01);
                        candPhrase.setData(childIdx, candPhrase.getConsumeNum());
                        if ((parent.getNodeFlag() & 0x0C) == 0 || (parent.getNodeFlag() & candPhrase.getNodeFlag() & 0x0C))
                        {
                            if ((parent.getNodeFlag() & 0x0C) == 0x08)
                            {
                                candPhrase.setNodeFlag(0x08);
                            }
                            else if ((parent.getNodeFlag() & 0x0C) == 0x04)
                            {
                                candPhrase.setNodeFlag(0x04);
                            }
                            parent.subListSerial.push_back(candPhrase);
                        }
                        bFound = true;
                        break;
                    }
                }

                if (bFound)
                {
                    break;
                }
            }
        }

        candList.pop_front();
    }
    if ( parent.subListSerial.size() == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

template <class TString, class TChar>
int PatriciaTrie<TString, TChar>::predictAssociationCompare(Associate<TChar>& assList, TString key, TString element, unsigned char uKeyLen)
{
    int usedKeyLen=0,j=0;
    int error_count = 0,fuzzy_count=0;

    if (key.size() < element.size())
    {
        return -1;
    }
    while (usedKeyLen < element.size())
    {
        if (levenshteinDistance::compChar((TChar)key.at(usedKeyLen), (TChar)element.at(j)))
        {
            bool errorKey = true;
            AssociateNode<TChar> & assNode = assList.getAssociateList().at(uKeyLen+usedKeyLen);
            typename list<CharNode<TChar> >::iterator itor;
            for (itor = assNode.charList.begin(); itor != assNode.charList.end(); itor++)
            {
                if (levenshteinDistance::compChar((TChar)element.at(j),(TChar)itor->getChar()) == 0)
                {
                    fuzzy_count++;
                    errorKey = false;
                    break;
                }
            }
            if (fuzzy_count > 1)
            {
                errorKey = true;
            }

            if (errorKey)
            {
                error_count ++;
                return -1;
            }
        }
        j++;
        usedKeyLen++;
    }

    return fuzzy_count;
}

template <class TString, class TChar>
int PatriciaTrie<TString, TChar>::predictAssociationCompare(TString key, TString element)
{
    int usedKeyLen=0;

    while (usedKeyLen < element.size() && usedKeyLen < key.size())
    {
        if (levenshteinDistance::compChar((TChar)key.at(usedKeyLen), (TChar)element.at(usedKeyLen)))
        {
            return -1;
        }

        usedKeyLen++;
    }

    if (element.size() == key.size())
    {
        return 0;
    }
    else if (element.size() > key.size())
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::predictAssociationSearch(Associate<TChar>& assList,unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent, MultiSerialPhrase<TString>& prevCand, TString& lastKey, unsigned char uKeyLen)
{
    vector<TString> cand;
    int i = 0, fuzzy_element_count = 0;
    int fuzzy_count = 0, compare_count = 0, usedKeyLen = 0;
    float preCandWeight = 0;
    TString multiStr;
    unsigned char uConsume = 0;
    bool phraseExist = false, ret = true;

    findBiDataExact(pBuf, prevCand.getReString(), cand);
    if (cand.size() > 0)
    {
        preCandWeight = prevCand.getWeight() / 2;
    }

    while (i < cand.size())
    {
        fuzzy_count = predictAssociationCompare(assList,key,cand[i],uKeyLen);
        if (fuzzy_count> 0 && fuzzy_element_count >= MAX_FUZZY_SEARCH_COUNT)
        {
            i++;
            continue;
        }
        if (fuzzy_count>=0)
        {
            MultiSerialPhrase<TString> candPhrase;
            unsigned int nodeIdx = processFindInSerialExact(pBuf, 0, cand[i]);
            usedKeyLen = cand[i].size();
            candPhrase.setReString(cand[i]);
            candPhrase.setWeight(preCandWeight*(1.0-0.2*fuzzy_count));
            candPhrase.setData(nodeIdx,usedKeyLen);
            candPhrase.setNodeFlag(getSerialNodeFlag(pBuf + nodeIdx));
            candPhrase.setFuzzyCnt(fuzzy_count);
            if (usedKeyLen < key.size())
            {
                ret = predictAssociationSearch(assList,pBuf, key.substr(usedKeyLen), candPhrase, candPhrase, lastKey, uKeyLen+usedKeyLen);
            }
            else
            {
                retSerialList.clear();
                processFindInSerialAsMulti(pBuf, 0, cand[i], &uConsume, multiStr,lastKey);
                list<MultiSerialPhrase<TString> > candList = retSerialList;
                while(candList.size()>0)
                {
                    MultiSerialPhrase<TString>& candPhrase_tmp = candList.front();
                    if (candPhrase_tmp.getReString().size() > cand[i].size())
                    {
                        candPhrase_tmp.setWeight(candPhrase.getWeight()*NOT_ENDED_PUNISH);
                        if ((parent.getNodeFlag() & 0x0C) == 0 || (parent.getNodeFlag() & candPhrase_tmp.getNodeFlag() & 0x0C))
                        {
                            if ((parent.getNodeFlag() & 0x0C) == 0x08)
                            {
                                candPhrase_tmp.setNodeFlag(0x08);
                            }
                            else if ((parent.getNodeFlag() & 0x0C) == 0x04)
                            {
                                candPhrase_tmp.setNodeFlag(0x04);
                            }
                            parent.subListSerial.push_back(candPhrase_tmp);
                            phraseExist = true;
                        }
                    }
                    candList.pop_front();
                }

            }
            if (ret &&((parent.getNodeFlag() & 0x0C) == 0 || (parent.getNodeFlag() & candPhrase.getNodeFlag() & 0x0C)))
            {
                if ((parent.getNodeFlag() & 0x0C) == 0x08)
                {
                    candPhrase.setNodeFlag(0x08);
                }
                else if ((parent.getNodeFlag() & 0x0C) == 0x04)
                {
                    candPhrase.setNodeFlag(0x04);
                }
                if (fuzzy_count > 0)
                {
                    fuzzy_element_count++;
                }
                parent.subListSerial.push_back(candPhrase);
                phraseExist = true;

            }

            compare_count++;
        }
        i++;
    }

    return  phraseExist;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::predictAssociationSearch(unsigned char* pBuf, TString key, MultiSerialPhrase<TString>& parent, MultiSerialPhrase<TString>& prevCand, TString& lastKey, unsigned char uKeyLen)
{
    MultiSerialPhrase<TString> associateCandList;
    int isComper=0;
    int i = 0;
    float preCandWeight = 0;
    bool phraseExist=false;

    preCandWeight = prevCand.getWeight();
    findBiDataExact_Ext(pBuf, prevCand.getReString(), associateCandList);
    list<MultiSerialPhrase<TString> > & associateCand =associateCandList.subListSerial;

    while (associateCand.size() > 0)
    {
        bool ret = true;
        MultiSerialPhrase<TString>& candPhrase = associateCand.front();
        if (i>=MAX_FUZZY_SEARCH_COUNT)
        {
            break;
        }
        isComper = predictAssociationCompare(key,candPhrase.getReString());
        if (isComper == 2)
        {
            ret = predictAssociationSearch(pBuf, key.substr(candPhrase.getConsumeNum()), candPhrase, candPhrase, lastKey);
            if (ret)
            {
                parent.subListSerial.push_front(candPhrase);
                phraseExist = true;
            }
        }
        else if(isComper >=0)
        {
            parent.subListSerial.push_front(candPhrase);
            phraseExist = true;
        }

        associateCand.pop_front();
        i++;
    }

    return phraseExist;
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::predictListInSerial_Ext(unsigned char* pBuf, TString key, int dCurFreq, unsigned char* adjustFreqBuf, TString& lastKey, unsigned char uKeyLen)
{
    unsigned char uConsume = 0;
    float preCandWeight = 0;
    bool bFindCand = false,ret = true;
    TString multiStr;

    if (key.size() == 0 && dCurFreq > 1)
    {
        return;
    }

    retSerialList.clear();

    bFindCand = processFindInSerialAsMulti(pBuf, 0, key, &uConsume, multiStr,lastKey);

    if (retSerialList.size() <= 0)
    {
        return;
    }
    preCandWeight = 1.0;
    list<MultiSerialPhrase<TString> > candList = retSerialList;

    while (candList.size() > 0)
    {
        MultiSerialPhrase<TString>& candPhrase = candList.front();

        candPhrase.setFuzzyCnt(dCurFreq);
        int odds = candPhrase.getReString().size() > key.size() ? (candPhrase.getReString().size() - key.size()) : (key.size() - candPhrase.getReString().size());
        candPhrase.setFuzzyCnt(candPhrase.getFuzzyCnt() + odds);

        //typename list<MultiSerialPhrase<TString> >::iterator element;
        //for (element = fullMatchCandSerial.subListSerial.begin(); element != fullMatchCandSerial.subListSerial.end(); element++)
        //{
        //    if (element->getNodeId() == candPhrase.getNodeId())
        //    {
        //        repeated = true;
        //        break;
        //    }
        //}

        //for (element = halfMatchCandSerial.subListSerial.begin(); element != halfMatchCandSerial.subListSerial.end(); element++)
        //{
        //    if (element->getNodeId() == candPhrase.getNodeId())
        //    {
        //        repeated = true;
        //        break;
        //    }
        //}

        //if (repeated)
        //{
        //    candList.pop_front();
        //    continue;
        //}

        if (key.size() > candPhrase.getReString().size())
        {
            candList.pop_front();
            continue;
            //if (phraseFuzzyFullMatchCandSerial.subListSerial.size() > 0 || phraseFullMatchCandSerial.subListSerial.size() > 0)
            //{
            //    candList.pop_front();
            //    continue;
            //}
            //if (dCurFreq)
            //{
            //    if (getSerialNodeFreq(pBuf + candPhrase.getNodeId()) < 200)
            //    {
            //        candList.pop_front();
            //        continue;
            //    }
            //}

            //candPhrase.setWeight(preCandWeight);
            //ret = predictAssociationSearch(pBuf, key.substr(candPhrase.getConsumeNum()), candPhrase, candPhrase, lastKey, uKeyLen+candPhrase.getConsumeNum());
            //if (ret == false)
            //{
            //    candList.pop_front();
            //    continue;
            //}
            //if (dCurFreq)
            //{
            //    phraseFuzzyFullMatchCandSerial.subListSerial.push_front(candPhrase);
            //}
            //else
            //{
            //    phraseFullMatchCandSerial.subListSerial.push_front(candPhrase);
            //}
        }
        else if(key.size() == candPhrase.getReString().size())
        {
            bool nopoint = true;
            int usedCount = predictSearchWordInUDB(candPhrase.getNodeId(),adjustFreqBuf);
            if (usedCount > 0)
            {
                candPhrase.setWeight(calculateUserPhraseWeight(preCandWeight, usedCount));
            }
            else
            {
                candPhrase.setWeight(preCandWeight);
            }
            for (int i = 0; i < candPhrase.getReString().size(); i++)
            {
                if (levenshteinDistance::isOmitChar((TChar)candPhrase.getReString().at(i)))
                {
                    if (dCurFreq)
                    {
                        if (predictSearchWordInCandSerial(candPhrase.getNodeId(),fuzzyFullMatchCandSerial))
                        {
                            fuzzyHalfMatchCandSerial.subListSerial.push_front(candPhrase);
                        }
                    }
                    else
                    {
                        if (predictSearchWordInCandSerial(candPhrase.getNodeId(),fullMatchCandSerial))
                        {
                            halfMatchCandSerial.subListSerial.push_front(candPhrase);
                        }
                    }
                    nopoint = false;
                    break;
                }
            }
            if (nopoint)
            {
                if (dCurFreq)
                {
                    if (predictSearchWordInCandSerial(candPhrase.getNodeId(),fuzzyFullMatchCandSerial))
                    {
                        fuzzyFullMatchCandSerial.subListSerial.push_front(candPhrase);
                    }
                }
                else
                {
                    if (predictSearchWordInCandSerial(candPhrase.getNodeId(),fullMatchCandSerial))
                    {
                        fullMatchCandSerial.subListSerial.push_front(candPhrase);
                    }
                }
            }
        }
        else if((key.size()) + 1 == candPhrase.getReString().size())
        {
            bool nopoint = true;
            int usedCount = predictSearchWordInUDB(candPhrase.getNodeId(),adjustFreqBuf);
            if (usedCount > 0)
            {
                candPhrase.setWeight(calculateUserPhraseWeight(preCandWeight, usedCount));
            }
            else
            {
                candPhrase.setWeight(preCandWeight);
            }
            for (int i=0;i<candPhrase.getReString().size();i++)
            {
                if (levenshteinDistance::isOmitChar((TChar)candPhrase.getReString().at(i)))
                {
                    if (dCurFreq)
                    {
                        if (predictSearchWordInCandSerial(candPhrase.getNodeId(),fuzzyFullMatchCandSerial))
                        {
                            fuzzyFullMatchCandSerial.subListSerial.push_front(candPhrase);
                        }
                    }
                    else
                    {
                        if (predictSearchWordInCandSerial(candPhrase.getNodeId(),fullMatchCandSerial))
                        {
                            fullMatchCandSerial.subListSerial.push_front(candPhrase);
                        }
                    }
                    nopoint = false;
                    break;
                }
            }
            if (nopoint)
            {
                if (dCurFreq)
                {
                    if (predictSearchWordInCandSerial(candPhrase.getNodeId(),fuzzyHalfMatchCandSerial))
                    {
                        fuzzyHalfMatchCandSerial.subListSerial.push_front(candPhrase);
                    }
                }
                else
                {
                    if (predictSearchWordInCandSerial(candPhrase.getNodeId(),halfMatchCandSerial))
                    {
                        halfMatchCandSerial.subListSerial.push_front(candPhrase);
                    }
                }
            }
        }
        else
        {
            int usedCount = predictSearchWordInUDB(candPhrase.getNodeId(),adjustFreqBuf);
            if (usedCount > 0)
            {
                candPhrase.setWeight(calculateUserPhraseWeight(preCandWeight, usedCount));
            }
            else
            {
                candPhrase.setWeight(preCandWeight);
            }
            if (dCurFreq)
            {
                if (predictSearchWordInCandSerial(candPhrase.getNodeId(),fuzzyHalfMatchCandSerial))
                {
                    fuzzyHalfMatchCandSerial.subListSerial.push_front(candPhrase);
                }
            }
            else
            {
                if (predictSearchWordInCandSerial(candPhrase.getNodeId(),halfMatchCandSerial))
                {
                    halfMatchCandSerial.subListSerial.push_front(candPhrase);
                }
            }

        }

        ret = true;
        candList.pop_front();
    }
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::predictListInSerial_Ext(Associate<TChar>& assList,unsigned char* pBuf, TString key, int dCurFreq,
    unsigned char* adjustFreqBuf, MultiSerialPhrase<TString>& prevCand, TString& lastKey, unsigned char uKeyLen)
{
    unsigned char uConsume = 0;
    float preCandWeight = 0;
    bool bFindCand = false, ret = true;
    TString multiStr;
    int i = 0;

    if (key.size() == 0 && dCurFreq > 1)
    {
        return;
    }

    retSerialList.clear();

    bFindCand = processFindInSerialAsMulti(assList, pBuf, 0, key, &uConsume, multiStr, 0, 0);

    preCandWeight = 1.0;
    list<MultiSerialPhrase<TString> > candList = retSerialList;

    while (candList.size() > 0)
    {
        MultiSerialPhrase<TString>& candPhrase = candList.front();

        int odds = candPhrase.getReString().size() > key.size() ? (candPhrase.getReString().size() - key.size()) : (key.size() - candPhrase.getReString().size());
        candPhrase.setFuzzyCnt(candPhrase.getFuzzyCnt() + odds);

        if (key.size() > candPhrase.getReString().size())
        {
            if (phraseFuzzyFullMatchCandSerial.subListSerial.size() > 0 &&
                phraseFullMatchCandSerial.subListSerial.size() > 0)
            {
                candList.pop_front();
                continue;
            }

            if (candPhrase.getFuzzyCnt() > (1 + odds))
            {
                if (getSerialNodeFreq(pBuf + candPhrase.getNodeId()) < 150)
                {
                    candList.pop_front();
                    continue;
                }
            }
            else if (candPhrase.getFuzzyCnt() > odds)
            {
                if (getSerialNodeFreq(pBuf + candPhrase.getNodeId()) < 100)
                {
                    candList.pop_front();
                    continue;
                }
            }

            ret = predictAssociationSearch(assList, pBuf, key.substr(candPhrase.getConsumeNum()),
                candPhrase, candPhrase, lastKey, candPhrase.getConsumeNum());
            if (ret == false)
            {
                candList.pop_front();
                continue;
            }

            if (candPhrase.getFuzzyCnt() > odds)
            {
                phraseFuzzyFullMatchCandSerial.subListSerial.push_front(candPhrase);
            }
            else
            {
                phraseFullMatchCandSerial.subListSerial.push_front(candPhrase);
            }
        }
        else if (key.size() == candPhrase.getReString().size())
        {
            bool nopoint = true;
            int usedCount = predictSearchWordInUDB(candPhrase.getNodeId(), adjustFreqBuf);

            if (usedCount > 0)
            {
                candPhrase.setWeight(calculateUserPhraseWeight(preCandWeight, usedCount));
            }
            else
            {
                candPhrase.setWeight(preCandWeight);
            }

            for (i = 0; i < candPhrase.getReString().size(); i++)
            {
                if (levenshteinDistance::isOmitChar((TChar)candPhrase.getReString().at(i)))
                {
                    if (candPhrase.getFuzzyCnt() > odds)
                    {
                        if (predictSearchWordInCandSerial(candPhrase.getNodeId(), fuzzyFullMatchCandSerial))
                        {
                            fuzzyHalfMatchCandSerial.subListSerial.push_front(candPhrase);
                        }
                    }
                    else
                    {
                        if (predictSearchWordInCandSerial(candPhrase.getNodeId(),fullMatchCandSerial))
                        {
                            halfMatchCandSerial.subListSerial.push_front(candPhrase);
                        }
                    }
                    nopoint = false;
                    break;
                }
            }

            if (nopoint)
            {
                if (candPhrase.getFuzzyCnt() > odds)
                {
                    fuzzyFullMatchCandSerial.subListSerial.push_front(candPhrase);
                }
                else
                {
                    fullMatchCandSerial.subListSerial.push_front(candPhrase);
                }
            }
        }
        else if (key.size() + 1 == candPhrase.getReString().size())
        {
            bool nopoint = true;
            int usedCount = predictSearchWordInUDB(candPhrase.getNodeId(), adjustFreqBuf);

            if (usedCount > 0)
            {
                candPhrase.setWeight(calculateUserPhraseWeight(preCandWeight, usedCount));
            }
            else
            {
                candPhrase.setWeight(preCandWeight);
            }

            for (i = 0; i < candPhrase.getReString().size(); i++)
            {
                if (levenshteinDistance::isOmitChar((TChar)candPhrase.getReString().at(i)))
                {
                    if (candPhrase.getFuzzyCnt() > odds)
                    {
                        fuzzyFullMatchCandSerial.subListSerial.push_front(candPhrase);
                    }
                    else
                    {
                        fullMatchCandSerial.subListSerial.push_front(candPhrase);
                    }
                    nopoint = false;
                    break;
                }
            }

            if (nopoint)
            {
                if (candPhrase.getFuzzyCnt() > odds)
                {
                    fuzzyHalfMatchCandSerial.subListSerial.push_front(candPhrase);
                }
                else
                {
                    halfMatchCandSerial.subListSerial.push_front(candPhrase);
                }
            }
        }
        else
        {
            int usedCount = predictSearchWordInUDB(candPhrase.getNodeId(),adjustFreqBuf);
            if (usedCount > 0)
            {
                candPhrase.setWeight(calculateUserPhraseWeight(preCandWeight, usedCount));
            }
            else
            {
                candPhrase.setWeight(preCandWeight);
            }

            if (candPhrase.getFuzzyCnt() > odds)
            {
                fuzzyHalfMatchCandSerial.subListSerial.push_front(candPhrase);
            }
            else
            {
                halfMatchCandSerial.subListSerial.push_front(candPhrase);
            }

        }

        ret = true;
        candList.pop_front();
    }

    list<MultiSerialPhrase<TString> > candUDBList = prevCand.subListSerial;

    while(candUDBList.size() > 0)
    {
        MultiSerialPhrase<TString>& candPhrase = candUDBList.front();

        int odds = candPhrase.getReString().size() > key.size() ? (candPhrase.getReString().size() - key.size()) : (key.size() - candPhrase.getReString().size());
        candPhrase.setFuzzyCnt(candPhrase.getFuzzyCnt() + odds);

        if (candPhrase.getNodeId() != -1)
        {
            //if (candPhrase.getUseCount() > 2)
            //{
            //    candPhrase.setWeight(calculateUserPhraseWeight(preCandWeight, 3));
            //}
            //else
            //{
            //    candPhrase.setWeight(calculateUserPhraseWeight(preCandWeight, (int)candPhrase.getWeight()));
            //}

            if (candPhrase.getReString().size() == key.size())
            {
                if (candPhrase.getFuzzyCnt() > odds)
                {
                    fuzzyFullMatchCandSerial.subListSerial.push_front(candPhrase);
                }
                else
                {
                    fullMatchCandSerial.subListSerial.push_front(candPhrase);
                }
            }
            else
            {
                if (candPhrase.getFuzzyCnt() > odds)
                {
                    fuzzyHalfMatchCandSerial.subListSerial.push_front(candPhrase);
                }
                else
                {
                    halfMatchCandSerial.subListSerial.push_front(candPhrase);
                }
            }
        }

        candUDBList.pop_front();
    }
}

template <class TString, class TChar>
int PatriciaTrie<TString, TChar>::predictUDBCompare(TString& key, UdbStr& element)
{
    int usedKeyLen = 0;
    unsigned int uKeySize = key.size();
    TChar* pKeyChar = key.c_str();
    wchar_t* pUnicodeStr = (wchar_t*)element.pStr;
    unsigned char* pSingleStr = (unsigned char*)element.pStr;

    if (uKeySize > element.uSize)
    {
        return -1;
    }

    while (usedKeyLen < uKeySize)
    {
        if (element.bIsUnicode)
        {
            if (levenshteinDistance::compChar(*(pKeyChar + usedKeyLen), *(pUnicodeStr + usedKeyLen)))
            {
                return -1;
            }
        }
        else
        {
            if (levenshteinDistance::compChar(*(pKeyChar + usedKeyLen), *(pSingleStr + usedKeyLen)))
            {
                return -1;
            }
        }

        usedKeyLen++;
    }

    return 0;
}

template <class TString, class TChar>
int PatriciaTrie<TString, TChar>::predictUDBCompare(Associate<TChar>& assList, TString& key, UdbStr& element, unsigned char uKeyLen)
{
    int usedKeyLen = 0, j = 0;
    int error_count = 0, fuzzy_count = 0;
    bool errorKey = true;
    unsigned int uKeySize = key.size();
    TChar* pKeyChar = (TChar*)key.c_str();
    typename list<CharNode<TChar> >::iterator itor;

    wchar_t* pUnicodeStr = (wchar_t*)element.pStr;
    unsigned char* pSingleStr = (unsigned char*)element.pStr;

    if (uKeySize > element.uSize)
    {
        return -1;
    }

    while (usedKeyLen < uKeySize && j < element.uSize)
    {
        if (element.bIsUnicode)
        {
            if (levenshteinDistance::isOmitChar(*(pUnicodeStr + j)))
            {
                j++;
                continue;
            }

            if (levenshteinDistance::compChar(*((wchar_t*)pKeyChar + usedKeyLen), *(pUnicodeStr + j)))
            {
                errorKey = true;
                AssociateNode<TChar>& assNode = assList.getAssociateList().at(uKeyLen + usedKeyLen);
                itor = assNode.charList.begin();
                itor++;
                for (; itor != assNode.charList.end(); itor++)
                {
                    if (!levenshteinDistance::compChar(*(pUnicodeStr + j), (wchar_t)itor->getChar()))
                    {
                        fuzzy_count++;
                        errorKey = false;
                        break;
                    }
                }

                // (keysize + 2) / 3
                if (fuzzy_count * 3 >= uKeySize + 6)
                {
                    errorKey = true;
                }

                if (errorKey)
                {
                    error_count++;
                    return -1;
                }
            }
        }
        else
        {
            if (levenshteinDistance::isOmitChar(*(pSingleStr + j)))
            {
                j++;
                continue;
            }

            if (levenshteinDistance::compChar(*((unsigned char*)pKeyChar + usedKeyLen), *(pSingleStr + j)))
            {
                errorKey = true;
                AssociateNode<TChar>& assNode = assList.getAssociateList().at(uKeyLen + usedKeyLen);
                itor = assNode.charList.begin();
                itor++;
                for (; itor != assNode.charList.end(); itor++)
                {
                    if (!levenshteinDistance::compChar(*(pSingleStr + j), (unsigned char)itor->getChar()))
                    {
                        fuzzy_count++;
                        errorKey = false;
                        break;
                    }
                }

                // (keysize + 2) / 3
                if (fuzzy_count * 3 >= uKeySize + 6)
                {
                    errorKey = true;
                }

                if (errorKey)
                {
                    error_count++;
                    return -1;
                }
            }
        }
        j++;
        usedKeyLen++;
    }

    return fuzzy_count;
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::predictSearchUDB(Associate<TChar>& assList,TString key, MultiSerialPhrase<TString>& parent,list<MultiSerialPhrase<TString> >& userCandList)
{
    unsigned int len;
    MultiPhrase<TString, TChar> phraseCand;
    unsigned char consumeNum = 0;
    int fuzzy = 0;

    typename list<MultiSerialPhrase<TString> >::iterator itor;

    for (itor=userCandList.begin();itor != userCandList.end();itor++)
    {
        consumeNum = 0;
        len = itor->getReString().size();

        if (len < key.size())
        {
            continue;
        }

        fuzzy = predictUDBCompare(assList,key,itor->getReString());

        if (fuzzy >= 0)
        {
            if (fuzzy>0 && key.size()+1 < len )
            {
                continue;
            }

            MultiSerialPhrase<TString>  candPhrase;
            candPhrase.setReString(itor->getReString());
            candPhrase.setNodeFlag(itor->getNodeFlag());
            candPhrase.setWeight(itor->getWeight());
            candPhrase.setData(itor->getNodeId(),key.size());
            candPhrase.setFuzzyCnt(fuzzy);
            parent.subListSerial.push_back(candPhrase);
        }
    }
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::predictSearchUDB(Associate<TChar>& assList,TString key, MultiSerialPhrase<TString>& parent,unsigned char* userDataBuf)
{
    unsigned char len = 0;
    unsigned int i = 0;
    unsigned char letterByteCount = 0;
    int fuzzy = 0;
    unsigned char singleWordBuf[40]={0};
    unsigned short doubleWordBuf[40]={0};

    letterByteCount = sizeof(key.at(0));
    if (letterByteCount == 0)
    {
        return;
    }
    for (i=0;i<MAX_USER_WORD_NUM;i++)
    {
        TString oneWord;
        UdbStr oneStr;
        int freq;
        if (letterByteCount == 1)
        {
            memset(singleWordBuf,0,sizeof(singleWordBuf));
            memcpy(singleWordBuf,userDataBuf+(USER_WORD+WORD_MESSAGE_LEN)*i,USER_WORD+WORD_MESSAGE_LEN);
            len = singleWordBuf[0];
            freq = singleWordBuf[1];
            oneStr.pStr = singleWordBuf+WORD_MESSAGE_LEN;
            oneStr.uSize = len;
        }
        else
        {
            len = userDataBuf[(USER_WORD*2+WORD_MESSAGE_LEN)*i];
            freq = singleWordBuf[(USER_WORD * 2 + WORD_MESSAGE_LEN)*i + 1];
            oneStr.pStr = userDataBuf+(USER_WORD*2+WORD_MESSAGE_LEN)*i+WORD_MESSAGE_LEN;
            oneStr.uSize = len;
            oneStr.bIsUnicode = true;
        }

        if (len < key.size())
        {
            continue;
        }

        fuzzy = predictUDBCompare(assList,key,oneStr);

        if (fuzzy >= 0)
        {
            if (fuzzy>0 && key.size()+1 < len )
            {
                continue;
            }
            if (letterByteCount == 1)
            {
                for (int j=0;j<len;j++)
                {
                    oneWord.push_back(singleWordBuf[WORD_MESSAGE_LEN+j]);
                }

            }
            else
            {
                for (int j=0;j<len;j++)
                {
                    oneWord.insert(j,1,readU16(userDataBuf+(USER_WORD*2+WORD_MESSAGE_LEN)*i+WORD_MESSAGE_LEN+j*2));
                }

            }
            MultiSerialPhrase<TString>  candPhrase;
            candPhrase.setReString(oneWord);
            candPhrase.setWeight(freq * DEFAULT_WORD_WEIGHT);
            candPhrase.setData(0,key.size());
            candPhrase.setFuzzyCnt(fuzzy);
            parent.subListSerial.push_back(candPhrase);

        }
    }
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::predictSearchCells(Associate<TChar>& assList,TString key, MultiSerialPhrase<TString>& parent,unsigned char* serialCellsBuf)
{
    unsigned char len = 0,cellsCount = 0;
    unsigned int i = 0,keyLen = 0;
    unsigned char letterByteCount = 0;
    int fuzzy = 0;
    unsigned char singleWordBuf[40]={0};
    unsigned short doubleWordBuf[40]={0};

    cellsCount = *serialCellsBuf;
    letterByteCount = sizeof(key.at(0));
    if (letterByteCount == 0)
    {
        return;
    }

    keyLen = key.size();
    for(int k=0;k<cellsCount;k++)
    {
        unsigned int cellsAddress;
        unsigned int cellsSize;
        unsigned int cellsDateIndex = 0;
        if ((*(serialCellsBuf+1+NATIVE_TITLE_MESSAGE_SIZE*k+2))== 0)
        {
            continue;
        }
        cellsAddress = readU32(serialCellsBuf+1+NATIVE_TITLE_MESSAGE_SIZE*k+5);
        cellsSize = readU32(serialCellsBuf+1+NATIVE_TITLE_MESSAGE_SIZE*k+9);
        i = 0;
        while (cellsDateIndex < cellsSize)
        {
            TString oneWord;
            unsigned char cellFreq = 0;
            UdbStr oneStr;
            if (letterByteCount == 1)
            {
                memset(singleWordBuf,0,sizeof(singleWordBuf));
                memcpy(singleWordBuf,serialCellsBuf+cellsAddress+cellsDateIndex,serialCellsBuf[cellsAddress+cellsDateIndex]+CELLS_WORD_MESSAGE_LEN);
                len = singleWordBuf[0];
                cellsDateIndex = cellsDateIndex + len + 2;
                if (len < keyLen || len >(keyLen+3))
                {
                    i++;
                    continue;
                }
                cellFreq = singleWordBuf[1];
                oneStr.pStr = singleWordBuf+CELLS_WORD_MESSAGE_LEN;
                oneStr.uSize = len;
            }
            else
            {
                len = serialCellsBuf[cellsAddress+cellsDateIndex];
                if (len < keyLen || len >(keyLen+3))
                {
                    cellsDateIndex = cellsDateIndex + len*2+CELLS_WORD_MESSAGE_LEN;
                    continue;
                }
                cellFreq = serialCellsBuf[cellsAddress+cellsDateIndex+1];
                oneStr.pStr = serialCellsBuf+cellsAddress+cellsDateIndex+CELLS_WORD_MESSAGE_LEN;
                oneStr.uSize = len;
                oneStr.bIsUnicode = true;
            }

            fuzzy = predictUDBCompare(assList,key,oneStr);

            if (fuzzy >= 0)
            {
                if (fuzzy>0 && keyLen+1 < len )
                {
                    continue;
                }
                if (letterByteCount == 1)
                {
                    for (int j=0;j<len;j++)
                    {
                        oneWord.push_back(singleWordBuf[CELLS_WORD_MESSAGE_LEN+j]);
                    }
                }
                else
                {
                    for (int j=0;j<len;j++)
                    {
                        oneWord.insert(j,1,readU16(serialCellsBuf+cellsAddress+cellsDateIndex+CELLS_WORD_MESSAGE_LEN+j*2));
                    }
                    cellsDateIndex = cellsDateIndex + len*2+CELLS_WORD_MESSAGE_LEN;
                }
                MultiSerialPhrase<TString>  candPhrase;
                candPhrase.setReString(oneWord);
                candPhrase.setWeight(1.0 * cellFreq);
                candPhrase.setData(0,keyLen);
                candPhrase.setFuzzyCnt(fuzzy);
                parent.subListSerial.push_back(candPhrase);

            }
            i++;
        }
    }
}

template <class TString, class TChar>
int PatriciaTrie<TString, TChar>::predictSearchWordInUDB(int nodeId, unsigned char* sysWordFreq)
{
    int i;
    for (i = 0; i < MAX_USER_WORD_NUM; i++)
    {
        if (((*(sysWordFreq + i * (SYS_WORD_ID + WORD_MESSAGE_LEN)) << 24) & readU24(sysWordFreq + i * 8 + 1)) == nodeId)
        {
            return sysWordFreq[i * (SYS_WORD_ID + WORD_MESSAGE_LEN) + 4];
        }
    }
    return -1;
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::getAllTermNodeIdx(unsigned char* pBuf, unsigned int parentIdx, list<pairTermNode<TString> >& lst, unsigned int count, unsigned int type)
{
    unsigned char cnt = 0;
    unsigned char childNum;
    unsigned int childIdx,currentIdx;
    unsigned int uListSize = lst.size();
    TString childStr;
    list<pairTermNode<TString> > list;
    pairTermNode<TString> currentPair(parentIdx, childStr);
    list.push_back(currentPair);

    while (!list.empty())
    {
        currentPair = list.front();
        currentIdx = currentPair.getIdx();
        childStr = currentPair.getReStr();
        list.pop_front();
        childNum = getSerialChildLen(pBuf + currentIdx);
        childIdx = getSerialFirstChildIdx(pBuf + currentIdx);

        for (cnt = 0; cnt < childNum; cnt++, childIdx = getSerialNextNodeIdx(pBuf, childIdx))
        {
            currentPair.setIdx(childIdx);
            currentPair.setReStr(childStr + getSerialKey(pBuf, childIdx));
            list.push_back(currentPair);
            if (getSerialIsTerminal(pBuf + childIdx) && isSerialMatchType(getSerialNodeFlag(pBuf + childIdx), type))
            {
                if (uListSize < count)
                {
                    currentPair.setIdx((getSerialNodeFreq(pBuf + childIdx) << 24) | childIdx);
                    lst.push_back(currentPair);
                    uListSize++;
                    if (uListSize == count)
                    {
                        return;
                    }
                }
            }
        }
    }

    return;
}

template <class TString, class TChar>
unsigned int PatriciaTrie<TString, TChar>::serializeTrie(unsigned char* pBuf, bool isUserDict)
{
    unsigned int uCur = 0;
    unsigned int uSize;

    uSize = serializeAllNode(pBuf, isUserDict);

    uCur = 0;
    if (!isUserDict)
    {
        constructBiAfterSerialize(pBuf, uCur, root);
    }

    return uSize;
}

template <class TString, class TChar>
unsigned int PatriciaTrie<TString, TChar>::serializeAllNode(unsigned char* pBuf, bool isUserDict)
{
    unsigned int uCur = 0;
    unsigned int parentoffsetIdx = 0;
    queue<pair<PatriciaTrieNode<TString, TChar> *, unsigned int>> parentQueue;

    root.serializeNode(pBuf, &uCur, &parentoffsetIdx, parentQueue, isUserDict);
    serializeChild(pBuf, &uCur, parentQueue, isUserDict);

    return uCur;
}

template <class TString, class TChar>
void PatriciaTrie<TString, TChar>::serializeChild(unsigned char* pBuf, unsigned int* pCurOffset,
    queue<pair<PatriciaTrieNode<TString, TChar> *, unsigned int>>& parentQueue, bool isUserDict)
{
    unsigned int i;
    unsigned int parentoffsetIdx = 0;
    PatriciaTrieNode<TString, TChar> * pNode;
    vector<queue<pair<PatriciaTrieNode<TString, TChar> *, unsigned int>>> currentQueueVec;

    while (parentQueue.size() > 0)
    {
        currentQueueVec.clear();
        pNode = parentQueue.front().first;
        parentoffsetIdx = parentQueue.front().second;

        for (i = 0; i < pNode->children.size(); i++)
        {
            queue<pair<PatriciaTrieNode<TString, TChar> *, unsigned int>> currentQue;
            pNode->children.at(i).serializeNode(pBuf, pCurOffset, &parentoffsetIdx, currentQue, isUserDict);
            currentQueueVec.push_back(currentQue);
        }

        for (i = 0; i < pNode->children.size(); i++)
        {
            serializeChild(pBuf, pCurOffset, currentQueueVec.at(i), isUserDict);
        }
        parentQueue.pop();
    }

    return;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::constructBiAfterSerialize(unsigned char* pBuf, unsigned int pCurIdx, PatriciaTrieNode<TString, TChar>& node)
{
    PatriciaTrieNode<TString, TChar>* pBiNode;
    unsigned int i, targetIndex ;
    unsigned int currentIndex;

    currentIndex = pCurIdx;
    // data
    pCurIdx += (6 - 3 + node.getKeyStrBufLen());

    // bigram
    for (i = 0; i < node.biData.size(); i++)
    {
        pBiNode = find(node.biData.at(i).word, PRECISE_MATCH, false, 0);
        targetIndex = (pBiNode) ? pBiNode->getCurIdx() : 0;

        node.setByteData(pBuf, &pCurIdx, (targetIndex >> 16) & 0xFF);
        node.setByteData(pBuf, &pCurIdx, (targetIndex >> 8) & 0xFF);
        node.setByteData(pBuf, &pCurIdx, targetIndex & 0xFF);

        //  freq here.
        pCurIdx += 1;
    }

    // children
    pCurIdx = getSerialFirstChildIdx(pBuf + currentIndex);
    for (i = 0; i < node.children.size(); i++)
    {
        constructBiAfterSerialize(pBuf, pCurIdx, node.children.at(i));
        pCurIdx = getSerialNextNodeIdx(pBuf, pCurIdx);
    }

    return true;
}

template <class TString, class TChar>
bool PatriciaTrie<TString, TChar>::unSerializeTrie(unsigned char* pBuf, unsigned int userDictToken, bool isUserDict)
{
    unsigned int uIdx = 0;
    root.reset();
    return root.unSerializeNode(pBuf, &uIdx, userDictToken, isUserDict);
}

template <class TString, class TChar>
void UserPatriciaTrie<TString, TChar>::processInsertAsUserWord(PatriciaTrieNode<TString, TChar>& currNode, TString key, unsigned int value, unsigned short uToken)
{
    bool done = false;
    unsigned int i, j, len;

    for (i = 0; i < currNode.children.size(); i++)
    {
        PatriciaTrieNode<TString, TChar>& child = currNode.children.at(i);

        len = (unsigned int)(child.getKeyStrLen() < key.length() ? child.getKeyStrLen() : key.length());
        for (j = 0; j < len; j++)
        {
            if (levenshteinDistance::preciseCompChar(key.at(j), child.getKeychar(j)))
            {
                break;
            }
        }

        if (j == 0)
        {
            if (levenshteinDistance::preciseCompChar(key.at(0), child.getKeychar(0)) < 0)
            {
                PatriciaTrieNode<TString, TChar> node(key);
                node.setTerminal(true);
                node.setData(value);
                node.setTimes(uToken);

                currNode.children.insert(currNode.children.begin() + i, node);
                done = true;

                break;
            }
            else
            {
                continue;
            }
        }
        else
        {
            if (j == len)
            {
                if (key.length() == child.getKeyStrLen())
                {
                    if (child.isTerminal())
                    {
                        child.setData(child.getData() + 1);
                    }
                    else
                    {
                        child.setTerminal(true);
                        child.setData(value);
                    }
                    child.setTimes(uToken);
                }
                else if (key.length() > child.getKeyStrLen())
                {
                    processInsertAsUserWord(child, key.substr(j), value, uToken);
                }
                else
                {
                    PatriciaTrieNode<TString, TChar> subChildNode(child.getKeySubStr(j));
                    subChildNode.setTerminal(child.isTerminal());
                    subChildNode.setData(child.getData());
                    subChildNode.setTimes(child.getTimes());
                    subChildNode.children = child.children;

                    child.setKeyStr(key);
                    child.setTerminal(true);
                    child.setData(value);
                    child.setTimes(uToken);

                    child.children.clear();
                    child.children.insert(child.children.begin(), subChildNode);
                }
            }
            else
            {
                TString childSubkey = child.getKeySubStr(j);
                TString subkey = key.substr(j);

                PatriciaTrieNode<TString, TChar> subChildNode(childSubkey);
                subChildNode.setTerminal(child.isTerminal());
                subChildNode.setData(child.getData());
                subChildNode.setTimes(child.getTimes());
                subChildNode.children = child.children;

                // update child's key
                child.setKeyStr(child.getKeySubStr(0, j));
                child.setTerminal(false);
                child.setData(0);
                child.setTimes(0);
                child.children.clear();

                PatriciaTrieNode<TString, TChar> node(subkey);
                node.setTerminal(true);
                node.setData(value);
                node.setTimes(uToken);

                if (subkey.at(0) < childSubkey.at(0))
                {
                    child.children.insert(child.children.begin(), subChildNode);
                    child.children.insert(child.children.begin(), node);
                }
                else
                {
                    child.children.insert(child.children.begin(), node);
                    child.children.insert(child.children.begin(), subChildNode);
                }
            }

            done = true;
            break;
        }
    }

    if (!done)
    {
        PatriciaTrieNode<TString, TChar> node(key);
        node.setTerminal(true);
        node.setData(value);
        node.setTimes(uToken);

        currNode.children.insert(currNode.children.end(), node);
    }
}

template <class TString, class TChar>
void UserPatriciaTrie<TString, TChar>::insertAsUserWord(TString key, unsigned int values, bool bAsMultiWord, bool bAsNewWord)
{
    if (key.size() == 0 || bAsMultiWord || !bAsNewWord)
    {
        return;
    }

    PatriciaTrieNode<TString, TChar>* pNode = PatriciaTrie<TString, TChar>::find(key, PatriciaTrie<TString, TChar>::PRECISE_MATCH, true);

    uCurToken++;
    if (pNode)
    {
        if (pNode->getData() < 3)
        {
            pNode->setData(pNode->getData() + 1);
        }
        pNode->setTimes(uCurToken);
    }
    else
    {
        if (uNodeNum < MAX_USER_WORD_NUM)
        {
            uNodeNum++;
        }
        else
        {
            // find a random minimum data here, then use the below del.
                    CandNode<TString, TChar>& node = this->findRandomMiniDataNode();
                    PatriciaTrie<TString, TChar>::del(node.node, node.childIndex);
        }
        processInsertAsUserWord(PatriciaTrie<TString, TChar>::root, key, values, uCurToken);
    }
}

//template <class TString, class TChar>
//void UserPatriciaTrie<TString, TChar>::deletePhoneBookName(TString bookName)
//{
//    if (uNodeNum > 0)
//    {
//        uNodeNum--;
//        PatriciaTrie<TString, TChar>::del(bookName);
//    }
//}

template <class TString, class TChar>
bool UserPatriciaTrie<TString, TChar>::unSerializeTrie(unsigned char* pBuf)
{
    uNodeNum = readU16(pBuf);
    pBuf += 2;

    uCurToken = readU16(pBuf);
    pBuf += 2;

    unsigned int tmpToken = uCurToken;

    if (uCurToken > MAX_USER_DICT_TOKEN)
    {
        uCurToken /= 2;
    }

    return PatriciaTrie<TString, TChar>::unSerializeTrie(pBuf, tmpToken, true);
}

template <class TString, class TChar>
unsigned int UserPatriciaTrie<TString, TChar>::serializeTrie(unsigned char* pBuf)
{
    unsigned int buf_size=0;
    writeU16(pBuf, uNodeNum);
    pBuf += 2;

    writeU16(pBuf, uCurToken);
    pBuf += 2;

    buf_size = PatriciaTrie<TString, TChar>::serializeTrie(pBuf, true);
    buf_size +=4;
    return buf_size;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::storeUDB(char* udbPath)
{
    unsigned int uSerialSize = TOKEN_SIZE + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE * 2 + TIME_SIZE * 2 + USER_WORD * 2 + SYS_WORD_ID + NONE);
    unsigned char versionChar[5] = DICT_VERSION;
    unsigned int id = 0;
    unsigned char* maskBuf = NULL;

    for (int i = 0; i < 4; i++)
    {
        ptrieNode.setByteData(serialUserBuf, &id, versionChar[i] & 0xFF);
    }
    maskBuf = new unsigned char[uSerialSize];
    memcpy(maskBuf, serialUserBuf, uSerialSize);
    hardMaskUserData(maskBuf,uSerialSize);

#ifdef WIN32
    FILE* fpUser = fopen(udbPath, "wb");

    if (!fpUser)
    {
        delete[] serialUserBuf;
        printf("open DB failed!");
        return false;
    }

    fclose(fpUser);
#else
    int user = open(udbPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    write(user, maskBuf, uSerialSize);
    close(user);
#endif

    delete[] maskBuf;
    resetUdbChanged();

    return true;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::convertOldUserData2NewVersion(unsigned char* oldUserBuf, int DBVersion,bool doubleByte)
{
    unsigned char* userBuffer;
    userBuffer = oldUserBuf+DICT_VERSION_SIZE+TOKEN_SIZE;
    if (DBVersion == 0X31303031)
    {
        if (doubleByte)
        {
            for (int i=0;i<MAX_USER_WORD_NUM;i++)
            {
                int wordlen = 0;
                TString oneWord;
                wordlen = userBuffer[i*(WORD_MESSAGE_LEN+USER_WORD*2)];
                if (wordlen == 0)
                {
                    continue;
                }
                for (int j=0;j<wordlen;j++)
                {
                    oneWord.insert(j,1,ptrieOut.readU16(userBuffer+(USER_WORD*2+WORD_MESSAGE_LEN)*i+WORD_MESSAGE_LEN+j*2));
                }
                insertAsUserWord(serialUserBuf,oneWord);
            }
        }
        else
        {
            for (int i=0;i<MAX_USER_WORD_NUM;i++)
            {
                int wordlen = 0;
                TString oneWord;
                unsigned char singleWordBuf[USER_WORD+1]={0};
                wordlen = userBuffer[i*(WORD_MESSAGE_LEN+USER_WORD)];
                if (wordlen == 0)
                {
                    continue;
                }
                memcpy(singleWordBuf,userBuffer+(USER_WORD+WORD_MESSAGE_LEN)*i+WORD_MESSAGE_LEN,USER_WORD);
                for (int j=0;j<wordlen;j++)
                {
                    oneWord.push_back(singleWordBuf[j]);
                }
                insertAsUserWord(serialUserBuf,oneWord);
            }
        }
    }

    return true;
}
template <class TString, class TChar>
void KikaEngine<TString, TChar>::hardMaskUserData(unsigned char* userData,unsigned int dataSize)
{
    unsigned char hardMaskChar=1;

    for (int i=0;i<dataSize;i++)
    {
        userData[i] = userData[i]^hardMaskChar;
        hardMaskChar++;
    }
}

template <class TString, class TChar>
unsigned int KikaEngine<TString, TChar>::saveCells2Dict(unsigned char* cellsBuf,unsigned char* networkBuf)
{
    unsigned char updateCellsCount = 0,nativeCellsCount = 0;
    unsigned short newCellsID = 0,nativeCellsID = 0;
    unsigned int newCellsSize=0,lastCellsSize=0;
    int i = 0,j = 0;

    updateCellsCount = networkBuf[0];
    nativeCellsCount = cellsBuf[0];
    for (i=0; i<updateCellsCount;i++)
    {
        newCellsID = ptrieOut.readU16(networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i);

        for (j = 0; j < nativeCellsCount; j++)
        {
            nativeCellsID = ptrieOut.readU16(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * j);
            if (newCellsID == nativeCellsID)
            {
                unsigned int nativeCellsAddress = ptrieOut.readU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * j + 5);
                unsigned int newCellsAddress = ptrieOut.readU32(networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i + 4);
                unsigned int nativeCellsSize = ptrieOut.readU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * j + 9);

                ptrieOut.writeU16(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * j + 3, ptrieOut.readU16(networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i + 2));

                newCellsSize = ptrieOut.readU32(networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i + 8);
                memmove(cellsBuf + nativeCellsAddress + newCellsSize, cellsBuf + nativeCellsAddress, newCellsSize);
                memcpy(cellsBuf + nativeCellsAddress, networkBuf + newCellsAddress, newCellsSize);

                nativeCellsSize += newCellsSize;
                ptrieOut.writeU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * j + 9, nativeCellsSize);
                break;
            }
        }
        if (j >= nativeCellsCount)
        {
            unsigned char lastCount = *cellsBuf;
            unsigned int newCellsAddress = ptrieOut.readU32(networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i + 4);
            newCellsSize = ptrieOut.readU32(networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i + 8);

            *cellsBuf = lastCount + 1;
            if (lastCount == 0)
            {
                memcpy(cellsBuf + 1, networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i, 2);
                *(cellsBuf + 3) = 1;
                *(cellsBuf + 4) = 0;
                *(cellsBuf + 5) = 1;
                ptrieOut.writeU32(cellsBuf + 6, NATIVE_TITLE_MESSAGE_SIZE + 1);
                memcpy(cellsBuf + 10, networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i + 8, 4);
                memcpy(cellsBuf + NATIVE_TITLE_MESSAGE_SIZE + 1, networkBuf + newCellsAddress, newCellsSize);
            }
            else
            {
                unsigned int nativeCellsAddress = ptrieOut.readU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * (lastCount-1) + 5);
                unsigned int nativeCellsSize = ptrieOut.readU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * (lastCount-1) + 9);
                memcpy(cellsBuf + nativeCellsAddress + nativeCellsSize, networkBuf + newCellsAddress, newCellsSize);
                memmove(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * (lastCount+1), cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * lastCount, nativeCellsAddress + nativeCellsSize + newCellsSize);
                memcpy(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * lastCount, networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i, 2);
                *(cellsBuf + 3 + NATIVE_TITLE_MESSAGE_SIZE * lastCount) = 1;
                *(cellsBuf + 4 + NATIVE_TITLE_MESSAGE_SIZE * lastCount) = 0;
                *(cellsBuf + 5 + NATIVE_TITLE_MESSAGE_SIZE * lastCount) = 1;
                ptrieOut.writeU32(cellsBuf + 6 + NATIVE_TITLE_MESSAGE_SIZE * lastCount, nativeCellsAddress + nativeCellsSize);
                memcpy(cellsBuf + 10 + NATIVE_TITLE_MESSAGE_SIZE * lastCount, networkBuf + 1 + NEW_TITLE_MESSAGE_SIZE * i + 8, 4);
                for(j=0;j<lastCount+1;j++)
                {
                    unsigned int nativeCellsAddress = ptrieOut.readU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * j + 5);
                    nativeCellsAddress += NATIVE_TITLE_MESSAGE_SIZE;
                    ptrieOut.writeU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * j + 5, nativeCellsAddress);
                }
            }
        }
        else
        {
            for(j++; j < nativeCellsCount; j++)
            {
                unsigned int nativeCellsAddress = ptrieOut.readU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * j + 5);
                nativeCellsAddress += newCellsSize;
                ptrieOut.writeU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * j + 5,nativeCellsAddress);
            }
        }
    }

    lastCellsSize = ptrieOut.readU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * ((*cellsBuf) - 1) + 5) + ptrieOut.readU32(cellsBuf + 1 + NATIVE_TITLE_MESSAGE_SIZE * ((*cellsBuf) - 1) + 9);

    return lastCellsSize;
}


template <class TString, class TChar>
bool KikaEngine<TString, TChar>::updateCellDict(char* cellsPath,unsigned char* networkBuf)
{
    unsigned int newCellSize = 0,oldCellSize = 0;
    unsigned char* cellsBuf;
    if (!(networkBuf))
    {
        return false;
    }
    newCellSize = ptrieOut.readU24(networkBuf + 1);

    if (newCellSize <= 0)
    {
        return false;
    }

#ifdef WIN32
    FILE* fpCells = fopen(cellsPath, "rb");
    if (fpCells > 0)
    {
        fseek(fpCells, 0, SEEK_END);
        oldCellSize = ftell(fpCells);
        fseek(fpCells, 0, SEEK_SET);

    }
    cellsBuf = new unsigned char [oldCellSize + newCellSize + 4];
    if (cellsBuf <= 0)
    {
        if (fpCells)
        {
            fclose(fpCells);
        }
        return false;
    }
    memset(cellsBuf, 0x00, oldCellSize + newCellSize + 4);
    if (oldCellSize > 0)
    {
        oldCellSize = fread(cellsBuf, 1, oldCellSize, fpCells);
        //hardMaskUserData(cellsBuf,oldCellSize);
    }
    if (fpCells)
    {
        fclose(fpCells);
    }
    newCellSize = saveCells2Dict(cellsBuf,networkBuf+4);

    //hardMaskUserData(cellsBuf,newCellSize);
    fpCells = fopen(cellsPath, "wb");
    fwrite(cellsBuf, sizeof(char), newCellSize, fpCells);
    fclose(fpCells);

#else
    int cells = open(cellsPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    if (cells <= 0)
    {
        return false;
    }
    oldCellSize = (unsigned long)lseek(cells, 1, SEEK_END);
    lseek(cells, 0, SEEK_SET);
    cellsBuf = (unsigned char*)mmap(NULL, oldCellSize+newCellSize, PROT_READ, MAP_SHARED, cells, 0);
    if (cellsBuf <= 0)
    {
        return false;
    }
    if (oldCellSize > 0)
    {
        read(cells, cellsBuf, oldCellSize);
        //hardMaskUserData(cellsBuf,oldCellSize);
    }
    newCellSize = saveCells2Dict(cellsBuf,networkBuf+4);

    //hardMaskUserData(cellsBuf,newCellSize);
    write(cells, cellsBuf, newCellSize);
    close(cells);

#endif

    return true;
}



template <class TString, class TChar>
bool KikaEngine<TString, TChar>::loadDB(char* udbPath, char* sysPath)
{
#ifdef WIN32
    FILE* fp = fopen(sysPath, "rb");
    FILE* fpUser = fopen(udbPath, "rb+");
    unsigned int uUserDBSize = DICT_VERSION_SIZE + TOKEN_SIZE + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE * 2 + TIME_SIZE * 2 + USER_WORD * 2 + SYS_WORD_ID + NONE);

    if (!(fp))
    {
        printf("open DB failed!");
        return false;
    }
    if (!(fpUser) )
    {
        fpUser = fopen(udbPath, "ab+");
    }

    //fseek(fpUser, 0, SEEK_END);
    //uUserDBSize = ftell(fpUser);

    fseek(fpUser, 0, SEEK_SET);
    serialUserBuf = new unsigned char [uUserDBSize + 4];
    memset(serialUserBuf, 0x00, uUserDBSize + 4);

    if (uUserDBSize > 0)
    {
        fread(serialUserBuf, 1, uUserDBSize, fpUser);
    }

    fseek(fp, 0, SEEK_END);
    uSerialSize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    serialBuf = new unsigned char [uSerialSize + 4];
    memset(serialBuf, 0x00, uSerialSize + 4);


    if (uSerialSize > 0)
    {
        uSerialSize = fread(serialBuf, 1, uSerialSize, fp);
        memset(dictionaryVersion,0,5);
        memcpy(dictionaryVersion,serialBuf,4);
    }
    fclose(fpUser);
    fclose(fp);
#elif defined ANDROID
    int serial = open(sysPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    if(0>=serial){
        return false;
    }
    uSerialSize = (unsigned long)lseek(serial, 1, SEEK_END);

    lseek(serial, 0, SEEK_SET);
    serialBuf = (unsigned char*)mmap(NULL, uSerialSize, PROT_READ, MAP_SHARED, serial, 0);
    if(0>=serialBuf){
        return false;
    }

    memset(dictionaryVersion,0,5);
    memcpy(dictionaryVersion,serialBuf,4);
    close(serial);

    int user = open(udbPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    unsigned long uUserDBSize = DICT_VERSION_SIZE + TOKEN_SIZE + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE * 2 + TIME_SIZE * 2 + USER_WORD * 2 + SYS_WORD_ID + NONE);
    lseek(user, 0, SEEK_SET);
    serialUserBuf = new unsigned char[uUserDBSize + 4];
    memset(serialUserBuf, 0, uUserDBSize + 4);
    read(user, serialUserBuf, uUserDBSize);
    close(user);
#else
    int serial = open(sysPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    uSerialSize = (unsigned long)lseek(serial, 1, SEEK_END);
    lseek(serial, 0, SEEK_SET);
    serialBuf = (unsigned char*)mmap(NULL, uSerialSize, PROT_READ, MAP_SHARED, serial, 0);
    memset(dictionaryVersion,0,5);
    memcpy(dictionaryVersion,serialBuf,4);
    close(serial);

    int user = open(udbPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    unsigned long uUserDBSize = DICT_VERSION_SIZE + TOKEN_SIZE + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE * 2 + TIME_SIZE * 2 + USER_WORD * 2 + SYS_WORD_ID + NONE);
    lseek(user, 0, SEEK_SET);
    serialUserBuf = new unsigned char[uUserDBSize + 4];
    memset(serialUserBuf, 0, uUserDBSize + 4);
    read(user, serialUserBuf, uUserDBSize);
    close(user);
#endif

    hardMaskUserData(serialUserBuf,uUserDBSize);
    string udbStr = udbPath;
    unsigned char versionChar[5] = DICT_VERSION;
    unsigned int sysVersion = ptrieOut.readU32(serialBuf);
    unsigned int realVersion = ptrieOut.readU32(serialUserBuf);
    unsigned int codeVersion = ptrieOut.readU32(versionChar);

    //unsigned char* crc_buf = serialBuf + uSerialSize - 2;
    //unsigned short crc_code = ptrieOut.readU16(crc_buf);
    //unsigned char* gen_crc_buf = serialBuf + 4;
    //unsigned short gen_crc_code = ptrieOut.do_crc_table(gen_crc_buf, uSerialSize - 6);

    //if (codeVersion != sysVersion || gen_crc_code != crc_code)
    //{
    //    //do some thing for update user dict.
    //    delete[] serialUserBuf;
    //    delete[] serialBuf;
    //    return false;
    //}

    if (codeVersion != sysVersion)
    {
        //do some thing for update user dict.
        delete[] serialUserBuf;
        delete[] serialBuf;
        return false;
    }

    if (codeVersion != realVersion)
    {
        unsigned char* oldUersBuf = serialUserBuf;
        serialUserBuf = new unsigned char [uUserDBSize + 4];
        memset(serialUserBuf, 0x00, uUserDBSize + 4);
        if (udbStr.at(udbStr.find(".") - 1) == '2')
        {
            convertOldUserData2NewVersion(oldUersBuf,realVersion,true);
        }
        else
        {
            convertOldUserData2NewVersion(oldUersBuf,realVersion,false);
        }
        delete[] oldUersBuf;
    }
    if (udbStr.at(udbStr.find(".") - 1) == '1')
    {
        serialUserBufForSys = serialUserBuf + DICT_VERSION_SIZE + TOKEN_SIZE + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + USER_WORD);
    }
    else
    {
        serialUserBufForSys = serialUserBuf + DICT_VERSION_SIZE + TOKEN_SIZE + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + USER_WORD * 2);
    }

    return true;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::loadDB(char* udbPath, char* sysPath, char* cellsPath)
{
#ifdef WIN32
    FILE* fp = fopen(sysPath, "rb");
    FILE* fpUser = fopen(udbPath, "rb+");
    FILE* fpCells = fopen(cellsPath, "rb+");
    unsigned int uUserDBSize = DICT_VERSION_SIZE + TOKEN_SIZE + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE * 2 + TIME_SIZE * 2 + USER_WORD * 2 + SYS_WORD_ID + NONE);

    if (!(fp))
    {
        printf("open DB failed!");
        return false;
    }

    if (!(fpUser) )
    {
        fpUser = fopen(udbPath, "ab+");
    }

    //fseek(fpUser, 0, SEEK_END);
    //uUserDBSize = ftell(fpUser);

    fseek(fpUser, 0, SEEK_SET);
    serialUserBuf = new unsigned char [uUserDBSize + 4];
    memset(serialUserBuf, 0x00, uUserDBSize + 4);

    if (uUserDBSize > 0)
    {
        fread(serialUserBuf, 1, uUserDBSize, fpUser);
    }

    fseek(fp, 0, SEEK_END);
    uSerialSize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    serialBuf = new unsigned char [uSerialSize + 4];
    memset(serialBuf, 0x00, uSerialSize + 4);

    if (uSerialSize > 0)
    {
        uSerialSize = fread(serialBuf, 1, uSerialSize, fp);
        memset(dictionaryVersion,0,5);
        memcpy(dictionaryVersion,serialBuf,4);
    }
    fclose(fpUser);
    fclose(fp);


    if (fpCells)
    {
        fseek(fpCells, 0, SEEK_END);
        cellsFileLen = ftell(fpCells);
        fseek(fpCells, 0, SEEK_SET);
        serialCellsBuf = new unsigned char [cellsFileLen + 4];
        memset(serialCellsBuf, 0x00, cellsFileLen + 4);

        if (cellsFileLen > 0)
        {
            cellsFileLen = fread(serialCellsBuf, 1, cellsFileLen, fpCells);
        }

        fclose(fpCells);
    }
#else
    int serial = open(sysPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    if (0 >= serial)
    {
        return false;
    }
    uSerialSize = (unsigned long)lseek(serial, 1, SEEK_END);

    lseek(serial, 0, SEEK_SET);
    serialBuf = (unsigned char*)mmap(NULL, uSerialSize, PROT_READ, MAP_SHARED, serial, 0);
    if (0 >= serialBuf)
    {
        return false;
    }

    memset(dictionaryVersion, 0, 5);
    memcpy(dictionaryVersion, serialBuf, 4);
    close(serial);

    int user = open(udbPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    unsigned long uUserDBSize = DICT_VERSION_SIZE + TOKEN_SIZE + USER_DICT_SIZE *
        (USER_WORD_LEN + DATA_SIZE * 2 + TIME_SIZE * 2 + USER_WORD * 2 + SYS_WORD_ID + NONE);
    lseek(user, 0, SEEK_SET);
    if (serialUserBuf != NULL)
    {
        delete[] serialUserBuf;
    }

    serialUserBuf = new unsigned char[uUserDBSize + 4];
    memset(serialUserBuf, 0, uUserDBSize + 4);
    read(user, serialUserBuf, uUserDBSize);
    close(user);

    int cells = open(cellsPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    cellsFileLen = (unsigned long)lseek(cells, 1, SEEK_END);
    lseek(cells, 0, SEEK_SET);
    serialCellsBuf = (unsigned char*)mmap(NULL, cellsFileLen, PROT_READ, MAP_SHARED, cells, 0);
    //hardMaskUserData(serialCellsBuf,cellsFileLen);
    close(cells);
#endif

    hardMaskUserData(serialUserBuf,uUserDBSize);
    string udbStr = udbPath;
    unsigned char versionChar[5] = DICT_VERSION;
    unsigned int sysVersion = ptrieOut.readU32(serialBuf);
    unsigned int realVersion = ptrieOut.readU32(serialUserBuf);
    unsigned int codeVersion = ptrieOut.readU32(versionChar);

    //unsigned char* crc_buf = serialBuf + uSerialSize - 2;
    //unsigned short crc_code = ptrieOut.readU16(crc_buf);
    //unsigned char* gen_crc_buf = serialBuf + 4;
    //unsigned short gen_crc_code = ptrieOut.do_crc_table(gen_crc_buf, uSerialSize - 6);

    //if (codeVersion != sysVersion || gen_crc_code != crc_code)
    //{
    //    //do some thing for update user dict.
    //    delete[] serialUserBuf;
    //    delete[] serialBuf;
    //    return false;
    //}

    if (codeVersion != sysVersion)
    {
        //do some thing for update user dict.
        delete[] serialUserBuf;
        delete[] serialBuf;
        return false;
    }

    if (codeVersion != realVersion)
    {
        unsigned char* oldUersBuf = serialUserBuf;
        serialUserBuf = new unsigned char [uUserDBSize + 4];
        memset(serialUserBuf, 0x00, uUserDBSize + 4);
        if (udbStr.at(udbStr.find(".") - 1) == '2')
        {
            convertOldUserData2NewVersion(oldUersBuf,realVersion,true);
        }
        else
        {
            convertOldUserData2NewVersion(oldUersBuf,realVersion,false);
        }
        delete[] oldUersBuf;
    }
    if (udbStr.at(udbStr.find(".") - 1) == '1')
    {
        serialUserBufForSys = serialUserBuf + DICT_VERSION_SIZE + TOKEN_SIZE + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + USER_WORD);
    }
    else
    {
        serialUserBufForSys = serialUserBuf + DICT_VERSION_SIZE + TOKEN_SIZE + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + USER_WORD * 2);
    }

    return true;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::insertAsUserWord(unsigned char* inBuf, TString& newWord)
{
    unsigned char* pBuf = inBuf + DICT_VERSION_SIZE;
    bool inserted = false;
    if (newWord.length() == 0 || newWord.length() >= 32)
    {
        return inserted;
    }

    unsigned short token = ptrieOut.readU16(pBuf) + 1;
    unsigned int i, j, len, nodeIdx = 0;
    char asciiChar;
    wchar_t unicodeChar;
    unsigned int pCurrentIdx;
    unsigned int minTimesId = 0, minTimes = INT_MAX;
    struct { unsigned char len, data; unsigned short times; TString key; } userNode;
    struct { unsigned int id; unsigned char data; unsigned short times; } sysNode;
    bool isUnicode = (sizeof(newWord.at(0)) == 2) ? true : false;

    nodeIdx = ptrieOut.processFindInSerialExact(serialBuf+DICT_VERSION_SIZE, 0, newWord);

    if (nodeIdx > 0)
    {
        for (i = 0; i < USER_DICT_SIZE; i++)
        {
            pCurrentIdx = i * (SYS_WORD_ID + DATA_SIZE + TIME_SIZE + NONE) + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;

            sysNode.id = ptrieOut.readU32(pBuf + pCurrentIdx);
            pCurrentIdx += SYS_WORD_ID;
            sysNode.data = *(pBuf + pCurrentIdx);
            pCurrentIdx += DATA_SIZE;
            sysNode.times = ptrieOut.readU16(pBuf + pCurrentIdx);

            if (sysNode.id == nodeIdx)
            {
                pCurrentIdx = i * (SYS_WORD_ID + DATA_SIZE + TIME_SIZE + NONE) + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;
                pCurrentIdx += SYS_WORD_ID;
                ptrieNode.setByteData(pBuf, &pCurrentIdx, (sysNode.data > 2 ? 3 : (sysNode.data + 1)) & 0xFF);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, (token >> 8) & 0xFF);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, token & 0xFF);
                inserted = true;
                break;
            }
            else
            {
                if (sysNode.times < minTimes)
                {
                    minTimes = sysNode.times;
                    minTimesId = i * (SYS_WORD_ID + DATA_SIZE + TIME_SIZE + NONE) + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;
                }
            }
        }
        if (!inserted)
        {
            pCurrentIdx = minTimesId;
            ptrieNode.setByteData(pBuf, &pCurrentIdx, nodeIdx >> 24 & 0xFF);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, nodeIdx >> 16 & 0xFF);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, nodeIdx >> 8 & 0xFF);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, nodeIdx & 0xFF);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x01);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, (token >> 8) & 0xFF);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, token & 0xFF);
            inserted = true;
        }
    }
    else
    {
        for (i = 0; i < USER_DICT_SIZE; i++)
        {
            pCurrentIdx = i * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;

            userNode.len = *(pBuf + pCurrentIdx);
            pCurrentIdx += USER_WORD_LEN;
            userNode.data = *(pBuf + pCurrentIdx);
            pCurrentIdx += DATA_SIZE;
            userNode.times = ptrieOut.readU16(pBuf + pCurrentIdx);
            pCurrentIdx += TIME_SIZE;
            userNode.key.clear();
            if (isUnicode)
            {
                for (j = 0; j < userNode.len; j++)
                {
                    unicodeChar = ptrieOut.readU16(pBuf + pCurrentIdx);
                    pCurrentIdx += 2;
                    userNode.key.push_back(unicodeChar);
                }
            }
            else
            {
                for (j = 0; j < userNode.len; j++)
                {
                    asciiChar = *(pBuf + pCurrentIdx);
                    pCurrentIdx += 1;
                    userNode.key.push_back(asciiChar);
                }
            }

            len = (unsigned int)(userNode.len < newWord.length() ? userNode.len : newWord.length());
            for (j = 0; j < len; j++)
            {
                if (userNode.key.at(j) != newWord.at(j))
                {
                    break;
                }
            }
            if (j == len && userNode.len == newWord.length())
            {
                pCurrentIdx = i * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;
                pCurrentIdx += 1;
                ptrieNode.setByteData(pBuf, &pCurrentIdx, (userNode.data > 2 ? 3 : (userNode.data + 1)) & 0xFF);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, (token >> 8) & 0xFF);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, token & 0xFF);
                inserted = true;
                break;
            }
            else
            {
                if (userNode.times < minTimes)
                {
                    minTimes = userNode.times;
                    minTimesId = i * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;
                }
            }
        }
        if (!inserted)
        {
            pCurrentIdx = minTimesId;
            ptrieNode.setByteData(pBuf, &pCurrentIdx, newWord.length() & 0xFF);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x01);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, (token >> 8) & 0xFF);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, token & 0xFF);
            for (j = 0; j < USER_WORD; j++)
            {
                if (j < newWord.length())
                {
                    if (isUnicode)
                    {
                        ptrieNode.setByteData(pBuf, &pCurrentIdx, (newWord.at(j) >> 8) & 0xFF);
                        ptrieNode.setByteData(pBuf, &pCurrentIdx, newWord.at(j) & 0xFF);
                    }
                    else
                    {
                        ptrieNode.setByteData(pBuf, &pCurrentIdx, newWord.at(j) & 0xFF);
                    }
                }
                else
                {
                    if (isUnicode)
                    {
                        ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                        ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                    }
                    else
                    {
                        ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                    }

                }

            }
            inserted = true;
        }
    }

    pCurrentIdx = 0;
    ptrieNode.setByteData(pBuf, &pCurrentIdx, (token >> 8) & 0xFF);
    ptrieNode.setByteData(pBuf, &pCurrentIdx, token & 0xFF);

    if (token > MAX_USER_DICT_TOKEN)
    {
        for (i = 0; i < USER_DICT_SIZE; i++)
        {
            pCurrentIdx = i * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;
            pCurrentIdx += USER_WORD_LEN + DATA_SIZE;
            token = ptrieOut.readU16(pBuf + pCurrentIdx) / 2;
            ptrieNode.setByteData(pBuf, &pCurrentIdx, (token >> 8) & 0xFF);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, token & 0xFF);
        }
        for (i = 0; i < USER_DICT_SIZE; i++)
        {
            pCurrentIdx = i * (SYS_WORD_ID + DATA_SIZE + TIME_SIZE + NONE) + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;
            pCurrentIdx += SYS_WORD_ID + DATA_SIZE;
            token = ptrieOut.readU16(pBuf + pCurrentIdx) / 2;
            ptrieNode.setByteData(pBuf, &pCurrentIdx, (token >> 8) & 0xFF);
            ptrieNode.setByteData(pBuf, &pCurrentIdx, token & 0xFF);
        }

        pCurrentIdx = 0;
        token = ptrieOut.readU16(pBuf) / 2;
        ptrieNode.setByteData(pBuf, &pCurrentIdx, (token >> 8) & 0xFF);
        ptrieNode.setByteData(pBuf, &pCurrentIdx, token & 0xFF);
    }

    return inserted;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::deleteUserWord(TString& userWord)
{
    unsigned char* pBuf = serialUserBuf + DICT_VERSION_SIZE;
    bool deleted = false;
    if (userWord.length() == 0 || userWord.length() >= 32)
    {
        return deleted;
    }

    unsigned int i, j, k, len, nodeIdx = 0;
    char asciiChar;
    wchar_t unicodeChar;
    unsigned int pCurrentIdx;
    struct { unsigned char len, data; unsigned short times; TString key; } userNode;
    unsigned int sysId;
    bool isUnicode = (sizeof(userWord.at(0)) == 2) ? true : false;

    nodeIdx = ptrieOut.processFindInSerialExact(serialBuf + DICT_VERSION_SIZE, 0, userWord);

    if (nodeIdx > 0)
    {
        for (i = 0; i < USER_DICT_SIZE; i++)
        {
            pCurrentIdx = i * (SYS_WORD_ID + DATA_SIZE + TIME_SIZE + NONE) + USER_DICT_SIZE * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;

            sysId = ptrieOut.readU32(pBuf + pCurrentIdx);

            if (sysId == nodeIdx)
            {
                ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                deleted = true;
                break;
            }
        }
    }
    else
    {
        for (i = 0; i < USER_DICT_SIZE; i++)
        {
            pCurrentIdx = i * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;

            userNode.len = *(pBuf + pCurrentIdx);
            pCurrentIdx += USER_WORD_LEN;
            userNode.data = *(pBuf + pCurrentIdx);
            pCurrentIdx += DATA_SIZE;
            userNode.times = ptrieOut.readU16(pBuf + pCurrentIdx);
            pCurrentIdx += TIME_SIZE;
            userNode.key.clear();
            if (isUnicode)
            {
                for (j = 0; j < userNode.len; j++)
                {
                    unicodeChar = ptrieOut.readU16(pBuf + pCurrentIdx);
                    pCurrentIdx += 2;
                    userNode.key.push_back(unicodeChar);
                }
            }
            else
            {
                for (j = 0; j < userNode.len; j++)
                {
                    asciiChar = *(pBuf + pCurrentIdx);
                    pCurrentIdx += 1;
                    userNode.key.push_back(asciiChar);
                }
            }

            len = (unsigned int)(userNode.len < userWord.length() ? userNode.len : userWord.length());
            for (j = 0; j < len; j++)
            {
                if (userNode.key.at(j) != userWord.at(j))
                {
                    break;
                }
            }
            if (j == len && userNode.len == userWord.length())
            {
                pCurrentIdx = i * (USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD)) + TOKEN_SIZE;
                for (k = 0; k < USER_WORD_LEN + DATA_SIZE + TIME_SIZE + (isUnicode ? 2 * USER_WORD : USER_WORD); k++)
                    ptrieNode.setByteData(pBuf, &pCurrentIdx, 0x00);
                deleted = true;
                break;
            }
        }
    }

    return deleted;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::backspacePredict(Associate<TChar>& assList, vector<TString>& cand)
{
    bool hasfullMatch = false;
    TString inStr,nulStr;
    int iCount = 0,phraseCnt = 0, index = 0;
    unsigned int searchOption = 0;
    list<CandItem<TString> > fullMatchCandList;
    list<CandItem<TString> > halfMatchCandList;
    list<CandItem<TString> > fuzzyFullMatchCandList;
    list<CandItem<TString> > fuzzyHalfMatchCandList;
    list<CandItem<TString> > phraseFullMatchCandList;
    list<CandItem<TString> > phraseFuzzyFullMatchList;
    list<CandItem<TString> > candList;

    typename list<CandItem<TString> >::iterator itor;

    ptrieOut.clearAllMatchCandSerial();

    if (closeFuzzySearch)
    {
        searchOption = searchOption | CLOSE_FUZZY_SEARCH;
    }
    if (closeHalfMatchSearch)
    {
        searchOption = searchOption | CLOSE_HALF_MATCH_SEARCH;
    }

    predictAssociate_Ext(assList, candList, inStr, 0.0, searchOption);

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFullMatchCandSerial(),0,nulStr,1.0,1.0,fullMatchCandList);
    fullMatchCandList.sort();
    fullMatchCandList.reverse();

    if (closeHalfMatchSearch == false)
    {
        nulStr.clear();
        ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getHalfMatchCandSerial(),0,nulStr,1.0,1.0,halfMatchCandList);
        halfMatchCandList.sort();
        halfMatchCandList.reverse();

        nulStr.clear();
        ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFuzzyHalfMatchCandSerial(),0,nulStr,1.0,1.0,fuzzyHalfMatchCandList);
        fuzzyHalfMatchCandList.sort();
        fuzzyHalfMatchCandList.reverse();
    }

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFuzzyFullMatchCandSerial(),0,nulStr,1.0,1.0,fuzzyFullMatchCandList);
    fuzzyFullMatchCandList.sort();
    fuzzyFullMatchCandList.reverse();

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getPhraseFuzzyFullMatchCandSerial(),0,nulStr,1.0,1.0,phraseFuzzyFullMatchList);
    phraseFuzzyFullMatchList.sort();
    phraseFuzzyFullMatchList.reverse();

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getPhraseFullMatchCandSerial(),0,nulStr,1.0,1.0,phraseFullMatchCandList);
    phraseFullMatchCandList.sort();
    phraseFullMatchCandList.reverse();

    cand.clear();

    for (itor = fullMatchCandList.begin(), iCount = 0;
        itor != fullMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    index = iCount;

    for (itor = fuzzyFullMatchCandList.begin();
        itor != fuzzyFullMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (assList.getSize() == 2
            && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(0)), (TChar)tolower(itor->getCand().at(0)))
            && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(1)), (TChar)tolower(itor->getCand().at(1))))
        {
            continue;
        }

        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    if (fuzzyFullMatchCandList.size() > 0 && halfMatchCandList.size() > 0 && iCount > index)
    {
        int i;
        bool hasOmit = false;
        if (fullMatchCandList.size() > 0)
        {
            for (i = 0; i < fullMatchCandList.begin()->getCand().size(); i++)
            {
                if (levenshteinDistance::isOmitChar((TChar)(fullMatchCandList.begin()->getCand().at(i))))
                {
                    hasOmit = true;
                    break;
                }
            }
        }

        if (fuzzyFullMatchCandList.begin()->getWeight() < halfMatchCandList.begin()->getWeight() && removeRepetition(cand, halfMatchCandList.begin()->getCand()))
        {

            if (index > 1)
            {
                i = index - 1;
                CandItem<TString> fullMatchCand = fullMatchCandList.back();
                while (i > 0 && fullMatchCand.getCand().size() == assList.getSize() && fullMatchCand.getWeight() < halfMatchCandList.begin()->getWeight())
                {
                    fullMatchCandList.pop_back();
                    fullMatchCand = fullMatchCandList.back();
                    i--;
                }
                insertCandidate(cand, i + 1, halfMatchCandList.begin()->getCand());
            }
            else if (index == 1 && hasOmit)
            {
                i = 1;
                if (fullMatchCandList.begin()->getWeight() < halfMatchCandList.begin()->getWeight())
                {
                    i = 0;
                }
                insertCandidate(cand, i, halfMatchCandList.begin()->getCand());
            }
            else
            {
                insertCandidate(cand, index, halfMatchCandList.begin()->getCand());
            }
            iCount++;
            halfMatchCandList.pop_front();
        }
        else if (fuzzyFullMatchCandList.begin()->getWeight() >= halfMatchCandList.begin()->getWeight() && removeRepetition(cand, halfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index + 1, halfMatchCandList.begin()->getCand());
            if (index > 1)
            {
                i = index - 1;
                CandItem<TString> fullMatchCand = fullMatchCandList.back();
                while (i > 0 && fullMatchCand.getCand().size() == assList.getSize() && fullMatchCand.getWeight() < fuzzyFullMatchCandList.begin()->getWeight())
                {
                    fullMatchCandList.pop_back();
                    fullMatchCand = fullMatchCandList.back();
                    i--;
                }
                if (i < index - 1)
                {
                    cand.erase(cand.begin() + index);
                    insertCandidate(cand, i + 1, fuzzyFullMatchCandList.begin()->getCand());
                }
            }
            else if (index == 1 && hasOmit)
            {
                if (fullMatchCandList.begin()->getWeight() < fuzzyFullMatchCandList.begin()->getWeight())
                {
                    cand.erase(cand.begin() + index);
                    insertCandidate(cand, 0, fuzzyFullMatchCandList.begin()->getCand());
                }
            }
            iCount++;
            halfMatchCandList.pop_front();
        }
        if (iCount > 20)
        {
            cand.pop_back();
            iCount--;
        }
    }

    for (itor = halfMatchCandList.begin();
        itor != halfMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    if (assList.getSize() == 2)
    {
        for (itor = fuzzyFullMatchCandList.begin();
            itor != fuzzyFullMatchCandList.end() && iCount < 20;
            itor++)
        {
            if (levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(0)), (TChar)tolower(itor->getCand().at(0)))
                && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(1)), (TChar)tolower(itor->getCand().at(1))))
            {
                if (removeRepetition(cand, itor->getCand()))
                {
                    cand.push_back(itor->getCand());
                    iCount++;
                }
            }
        }
    }

    if (fuzzyFullMatchCandList.size() == 0 && halfMatchCandList.size() > 0 && fuzzyHalfMatchCandList.size() > 0)
    {
        if (halfMatchCandList.begin()->getWeight() < fuzzyHalfMatchCandList.begin()->getWeight() && removeRepetition(cand, fuzzyHalfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index, fuzzyHalfMatchCandList.begin()->getCand());
            iCount++;
            fuzzyHalfMatchCandList.pop_front();
        }
        else if (halfMatchCandList.begin()->getWeight() >= fuzzyHalfMatchCandList.begin()->getWeight() && removeRepetition(cand, fuzzyHalfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index + 1, fuzzyHalfMatchCandList.begin()->getCand());
            iCount++;
            fuzzyHalfMatchCandList.pop_front();
        }
        if (iCount > 20)
        {
            cand.pop_back();
            iCount--;
        }
    }

    for (itor = fuzzyHalfMatchCandList.begin();
        itor != fuzzyHalfMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    int i = 0, j = 0, phraseFuzzyCount = 0;
    TString phraseWord;
    phraseWord.clear();
    if (phraseFullMatchCandList.size() > 0)
    {
        phraseWord = phraseFullMatchCandList.begin()->getCand();
        phraseFullMatchCandList.pop_front();
    }
    else if (phraseFuzzyFullMatchList.size() > 0)
    {
        phraseWord = phraseFuzzyFullMatchList.begin()->getCand();
        phraseFuzzyFullMatchList.pop_front();
    }

    if (phraseWord.size() > 0)
    {
        while (i < assList.getSize() && (j + 1) < phraseWord.size())
        {
            if (levenshteinDistance::isOmitChar(assList.getKeyChar(i)))
            {
                i++;
                continue;
            }
            else if (levenshteinDistance::isOmitChar((TChar)phraseWord.at(j)))
            {
                j++;
                continue;
            }
            else
            {
                if (levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(i)), (TChar)tolower(phraseWord.at(j))))
                {
                    phraseFuzzyCount++;
                }
                i++;
                j++;
            }
        }
        if (i < assList.getSize())
        {
            phraseFuzzyCount += (assList.getSize() - i);
        }
        else if ((j + 1) < phraseWord.size())
        {
            phraseFuzzyCount += (phraseWord.size() - j - 1);
        }
    }

    for (itor = phraseFullMatchCandList.begin();
        itor != phraseFullMatchCandList.end() && iCount < 20 && phraseCnt < OUTPUT_PHRASE_COUNT;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
            phraseCnt++;
        }
    }

    for (itor = phraseFuzzyFullMatchList.begin();
        itor != phraseFuzzyFullMatchList.end() && iCount < 20 && phraseCnt < OUTPUT_PHRASE_COUNT;
        itor++)
    {
        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
            phraseCnt++;
        }
    }

    if (phraseFuzzyCount < 3 && phraseWord.size() > 0 && (index + phraseFuzzyCount) < iCount)
    {
        insertCandidate(cand, ((index + phraseFuzzyCount) < iCount ? (index + phraseFuzzyCount) : iCount), phraseWord);
    }
    if (cand.size() > 20)
    {
        cand.pop_back();
        iCount--;
    }

    if (cand.size() == 0)
    {
        predictAssociate(assList, candList);
        candList.sort();
        candList.reverse();
        for (itor = candList.begin(), iCount = 0;
            itor != candList.end() && iCount < 3;
            itor++)
        {
            if (removeRepetition(cand, itor->getCand()))
            {
                cand.push_back(itor->getCand());
                iCount++;
            }
        }
    }

    return hasfullMatch;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::backspacePredict(TString preword, Associate<TChar>& assList, vector<TString>& cand)
{
    bool hasfullMatch = false;
    TString inStr,nulStr;
    int iCount = 0,phraseCnt = 0, index = 0;
    unsigned int searchOption = 0;
    list<CandItem<TString> > fullMatchCandList;
    list<CandItem<TString> > halfMatchCandList;
    list<CandItem<TString> > fuzzyFullMatchCandList;
    list<CandItem<TString> > fuzzyHalfMatchCandList;
    list<CandItem<TString> > phraseFullMatchCandList;
    list<CandItem<TString> > phraseFuzzyFullMatchList;
    list<CandItem<TString> > candList;

    typename list<CandItem<TString> >::iterator itor;

    ptrieOut.clearAllMatchCandSerial();
    if (closeFuzzySearch)
    {
        searchOption = searchOption | CLOSE_FUZZY_SEARCH;
    }
    if (closeHalfMatchSearch)
    {
        searchOption = searchOption | CLOSE_HALF_MATCH_SEARCH;
    }

    predictAssociate_Ext(assList, candList, inStr,0.0, searchOption);
    prewordAssociate(preword);

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFullMatchCandSerial(),0,nulStr,1.0,1.0,fullMatchCandList);
    fullMatchCandList.sort();
    fullMatchCandList.reverse();

    if (closeHalfMatchSearch == false)
    {
        nulStr.clear();
        ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getHalfMatchCandSerial(),0,nulStr,1.0,1.0,halfMatchCandList);
        halfMatchCandList.sort();
        halfMatchCandList.reverse();

        nulStr.clear();
        ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFuzzyHalfMatchCandSerial(),0,nulStr,1.0,1.0,fuzzyHalfMatchCandList);
        fuzzyHalfMatchCandList.sort();
        fuzzyHalfMatchCandList.reverse();
    }

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFuzzyFullMatchCandSerial(),0,nulStr,1.0,1.0,fuzzyFullMatchCandList);
    fuzzyFullMatchCandList.sort();
    fuzzyFullMatchCandList.reverse();

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getPhraseFuzzyFullMatchCandSerial(),0,nulStr,1.0,1.0,phraseFuzzyFullMatchList);
    phraseFuzzyFullMatchList.sort();
    phraseFuzzyFullMatchList.reverse();

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getPhraseFullMatchCandSerial(),0,nulStr,1.0,1.0,phraseFullMatchCandList);
    phraseFullMatchCandList.sort();
    phraseFullMatchCandList.reverse();

    cand.clear();

    if (fullMatchCandList.size() != 0 || fuzzyFullMatchCandList.size() != 0)
    {
        hasfullMatch = true;
    }

    for (itor = fullMatchCandList.begin(), iCount = 0;
        itor != fullMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    index = iCount;

    for (itor = fuzzyFullMatchCandList.begin();
        itor != fuzzyFullMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (assList.getSize() == 2
            && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(0)), (TChar)tolower(itor->getCand().at(0)))
            && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(1)), (TChar)tolower(itor->getCand().at(1))))
        {
            continue;
        }

        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    if (fuzzyFullMatchCandList.size() > 0 && halfMatchCandList.size() > 0 && iCount > index)
    {
        int i;
        bool hasOmit = false;
        if (fullMatchCandList.size() > 0)
        {
            for (i = 0; i < fullMatchCandList.begin()->getCand().size(); i++)
            {
                if (levenshteinDistance::isOmitChar((TChar)(fullMatchCandList.begin()->getCand().at(i))))
                {
                    hasOmit = true;
                    break;
                }
            }
        }

        if (fuzzyFullMatchCandList.begin()->getWeight() < halfMatchCandList.begin()->getWeight() && removeRepetition(cand, halfMatchCandList.begin()->getCand()))
        {

            if (index > 1)
            {
                i = index - 1;
                CandItem<TString> fullMatchCand = fullMatchCandList.back();
                while (i > 0 && fullMatchCand.getCand().size() == assList.getSize() && fullMatchCand.getWeight() < halfMatchCandList.begin()->getWeight())
                {
                    fullMatchCandList.pop_back();
                    fullMatchCand = fullMatchCandList.back();
                    i--;
                }
                insertCandidate(cand, i + 1, halfMatchCandList.begin()->getCand());
            }
            else if (index == 1 && hasOmit)
            {
                i = 1;
                if (fullMatchCandList.begin()->getWeight() < halfMatchCandList.begin()->getWeight())
                {
                    i = 0;
                }
                insertCandidate(cand, i, halfMatchCandList.begin()->getCand());
            }
            else
            {
                insertCandidate(cand, index, halfMatchCandList.begin()->getCand());
            }
            iCount++;
            halfMatchCandList.pop_front();
        }
        else if (fuzzyFullMatchCandList.begin()->getWeight() >= halfMatchCandList.begin()->getWeight() && removeRepetition(cand, halfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index + 1, halfMatchCandList.begin()->getCand());
            if (index > 1)
            {
                i = index - 1;
                CandItem<TString> fullMatchCand = fullMatchCandList.back();
                while (i > 0 && fullMatchCand.getCand().size() == assList.getSize() && fullMatchCand.getWeight() < fuzzyFullMatchCandList.begin()->getWeight())
                {
                    fullMatchCandList.pop_back();
                    fullMatchCand = fullMatchCandList.back();
                    i--;
                }
                if (i < index - 1)
                {
                    cand.erase(cand.begin() + index);
                    insertCandidate(cand, i + 1, fuzzyFullMatchCandList.begin()->getCand());
                }
            }
            else if (index == 1 && hasOmit)
            {
                if (fullMatchCandList.begin()->getWeight() < fuzzyFullMatchCandList.begin()->getWeight())
                {
                    cand.erase(cand.begin() + index);
                    insertCandidate(cand, 0, fuzzyFullMatchCandList.begin()->getCand());
                }
            }
            iCount++;
            halfMatchCandList.pop_front();
        }
        if (iCount > 20)
        {
            cand.pop_back();
            iCount--;
        }
    }

    for (itor = halfMatchCandList.begin();
        itor != halfMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    if (assList.getSize() == 2)
    {
        for (itor = fuzzyFullMatchCandList.begin();
            itor != fuzzyFullMatchCandList.end() && iCount < 20;
            itor++)
        {
            if (levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(0)), (TChar)tolower(itor->getCand().at(0)))
                && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(1)), (TChar)tolower(itor->getCand().at(1))))
            {
                if (removeRepetition(cand, itor->getCand()))
                {
                    cand.push_back(itor->getCand());
                    iCount++;
                }
            }
        }
    }

    if (fuzzyFullMatchCandList.size() == 0 && halfMatchCandList.size() > 0 && fuzzyHalfMatchCandList.size() > 0)
    {
        if (halfMatchCandList.begin()->getWeight() < fuzzyHalfMatchCandList.begin()->getWeight() && removeRepetition(cand, fuzzyHalfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index, fuzzyHalfMatchCandList.begin()->getCand());
            iCount++;
            fuzzyHalfMatchCandList.pop_front();
        }
        else if (halfMatchCandList.begin()->getWeight() >= fuzzyHalfMatchCandList.begin()->getWeight() && removeRepetition(cand, fuzzyHalfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index + 1, fuzzyHalfMatchCandList.begin()->getCand());
            iCount++;
            fuzzyHalfMatchCandList.pop_front();
        }
        if (iCount > 20)
        {
            cand.pop_back();
            iCount--;
        }
    }

    for (itor = fuzzyHalfMatchCandList.begin();
        itor != fuzzyHalfMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    int i = 0, j = 0, phraseFuzzyCount = 0;
    TString phraseWord;
    phraseWord.clear();
    if (phraseFullMatchCandList.size() > 0)
    {
        phraseWord = phraseFullMatchCandList.begin()->getCand();
        phraseFullMatchCandList.pop_front();
    }
    else if (phraseFuzzyFullMatchList.size() > 0)
    {
        phraseWord = phraseFuzzyFullMatchList.begin()->getCand();
        phraseFuzzyFullMatchList.pop_front();
    }

    if (phraseWord.size() > 0)
    {
        while (i < assList.getSize() && (j + 1) < phraseWord.size())
        {
            if (levenshteinDistance::isOmitChar(assList.getKeyChar(i)))
            {
                i++;
                continue;
            }
            else if (levenshteinDistance::isOmitChar((TChar)phraseWord.at(j)))
            {
                j++;
                continue;
            }
            else
            {
                if (levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(i)), (TChar)tolower(phraseWord.at(j))))
                {
                    phraseFuzzyCount++;
                }
                i++;
                j++;
            }
        }
        if (i < assList.getSize())
        {
            phraseFuzzyCount += (assList.getSize() - i);
        }
        else if ((j + 1) < phraseWord.size())
        {
            phraseFuzzyCount += (phraseWord.size() - j - 1);
        }
    }

    for (itor = phraseFullMatchCandList.begin();
        itor != phraseFullMatchCandList.end() && iCount < 20 && phraseCnt < OUTPUT_PHRASE_COUNT;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
            phraseCnt++;
        }
    }

    for (itor = phraseFuzzyFullMatchList.begin();
        itor != phraseFuzzyFullMatchList.end() && iCount < 20 && phraseCnt < OUTPUT_PHRASE_COUNT;
        itor++)
    {
        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
            phraseCnt++;
        }
    }

    if (phraseFuzzyCount < 3 && phraseWord.size() > 0 && (index + phraseFuzzyCount) < iCount)
    {
        insertCandidate(cand, ((index + phraseFuzzyCount) < iCount ? (index + phraseFuzzyCount) : iCount), phraseWord);
    }
    if (cand.size() > 20)
    {
        cand.pop_back();
        iCount--;
    }

    if (cand.size() == 0)
    {
        predictAssociate(assList, candList);
        candList.sort();
        candList.reverse();
        for (itor = candList.begin(), iCount = 0;
            itor != candList.end() && iCount < 3;
            itor++)
        {
            if (removeRepetition(cand,itor->getCand()))
            {
                cand.push_back(itor->getCand());
                iCount++;
            }
        }
    }

    return hasfullMatch;
}


template <class TString, class TChar>
bool KikaEngine<TString, TChar>::backspacePredict_Cells(Associate<TChar>& assList, vector<TString>& cand)
{
    TString inStr;
    int iCount = 0;
    list<CandItem<TString> > candList;
    typename list<CandItem<TString> >::iterator itor;
    predictCells(assList, candList);

    candList.sort();
    candList.reverse();

    cand.clear();

    for (itor = candList.begin(), iCount = 0;
        itor != candList.end() && iCount < 20;
        itor++, iCount++)
    {
        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
        }
    }

    return true;
}
template <class TString, class TChar>
bool KikaEngine<TString, TChar>::removeRepetition( vector<TString>& cand, TString elementStr)
{
    for(int i=0;i<cand.size();i++)
    {
        if ((cand[i].compare(elementStr))==0)
        {
            return false;
        }
    }
    return true;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::predict(Associate<TChar>& assList, vector<TString>& cand, EngineStatus status)
{
    bool hasfullMatch = false;
    TString inStr,nulStr;
    int iCount = 0,phraseCnt=0, index = 0;
    unsigned int searchOption = 0;
    list<CandItem<TString> > candList;
    list<CandItem<TString> > fullMatchCandList;
    list<CandItem<TString> > halfMatchCandList;
    list<CandItem<TString> > fuzzyFullMatchCandList;
    list<CandItem<TString> > fuzzyHalfMatchCandList;
    list<CandItem<TString> > phraseFullMatchCandList;
    list<CandItem<TString> > phraseFuzzyFullMatchList;

    typename list<CandItem<TString> >::iterator itor;

    if (status == S_START)
    {
        ptrieOut.resetDistSession();
    }

    ptrieOut.clearAllMatchCandSerial();
    if (closeFuzzySearch)
    {
        searchOption = searchOption | CLOSE_FUZZY_SEARCH;
    }
    if (closeHalfMatchSearch)
    {
        searchOption = searchOption | CLOSE_HALF_MATCH_SEARCH;
    }

    predictAssociate_Ext(assList, candList, inStr, 0.0, searchOption);

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf + DICT_VERSION_SIZE, ptrieOut.getFullMatchCandSerial(), 0,
        nulStr, 1.0, 1.0, fullMatchCandList);
    fullMatchCandList.sort();
    fullMatchCandList.reverse();

    if (closeHalfMatchSearch == false)
    {
        nulStr.clear();
        ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getHalfMatchCandSerial(),0,nulStr,1.0,1.0,halfMatchCandList);
        halfMatchCandList.sort();
        halfMatchCandList.reverse();

        nulStr.clear();
        ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFuzzyHalfMatchCandSerial(),0,nulStr,1.0,1.0,fuzzyHalfMatchCandList);
        fuzzyHalfMatchCandList.sort();
        fuzzyHalfMatchCandList.reverse();
    }

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFuzzyFullMatchCandSerial(),0,nulStr,1.0,1.0,fuzzyFullMatchCandList);
    fuzzyFullMatchCandList.sort();
    fuzzyFullMatchCandList.reverse();

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getPhraseFuzzyFullMatchCandSerial(),0,nulStr,1.0,1.0,phraseFuzzyFullMatchList);
    phraseFuzzyFullMatchList.sort();
    phraseFuzzyFullMatchList.reverse();

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getPhraseFullMatchCandSerial(),0,nulStr,1.0,1.0,phraseFullMatchCandList);
    phraseFullMatchCandList.sort();
    phraseFullMatchCandList.reverse();

    if (fullMatchCandList.size() != 0 || fuzzyFullMatchCandList.size() != 0)
    {
        hasfullMatch = true;
    }

    cand.clear();

    for (itor = fullMatchCandList.begin(), iCount = 0;
         itor != fullMatchCandList.end() && iCount < 20;
         itor++)
    {
        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    index = iCount;

    for (itor = fuzzyFullMatchCandList.begin();
        itor != fuzzyFullMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (assList.getSize() == 2
            && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(0)), (TChar)tolower(itor->getCand().at(0)))
            && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(1)), (TChar)tolower(itor->getCand().at(1))))
        {
            continue;
        }
        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    if (fuzzyFullMatchCandList.size() > 0 && halfMatchCandList.size() > 0 && iCount > index)
    {
        int i;
        bool hasOmit = false;
        if (fullMatchCandList.size() > 0)
        {
            for (i = 0; i < fullMatchCandList.begin()->getCand().size(); i++)
            {
                if (levenshteinDistance::isOmitChar((TChar)(fullMatchCandList.begin()->getCand().at(i))))
                {
                    hasOmit = true;
                    break;
                }
            }
        }

        if (fuzzyFullMatchCandList.begin()->getWeight() < halfMatchCandList.begin()->getWeight() && removeRepetition(cand, halfMatchCandList.begin()->getCand()))
        {
            if (index > 1)
            {
                i = index - 1;
                CandItem<TString> fullMatchCand = fullMatchCandList.back();
                while (i > 0 && fullMatchCand.getCand().size() == assList.getSize() && fullMatchCand.getWeight() < halfMatchCandList.begin()->getWeight())
                {
                    fullMatchCandList.pop_back();
                    fullMatchCand = fullMatchCandList.back();
                    i--;
                }
                insertCandidate(cand, i + 1, halfMatchCandList.begin()->getCand());
            }
            else if (index == 1 && hasOmit)
            {
                i = 1;
                if (fullMatchCandList.begin()->getWeight() < halfMatchCandList.begin()->getWeight())
                {
                    i = 0;
                }
                insertCandidate(cand, i, halfMatchCandList.begin()->getCand());
            }
            else
            {
                insertCandidate(cand, index, halfMatchCandList.begin()->getCand());
            }
            iCount++;
            halfMatchCandList.pop_front();
        }
        else if (fuzzyFullMatchCandList.begin()->getWeight() >= halfMatchCandList.begin()->getWeight() && removeRepetition(cand, halfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index + 1, halfMatchCandList.begin()->getCand());
            if (index > 1)
            {
                i = index - 1;
                CandItem<TString> fullMatchCand = fullMatchCandList.back();
                while (i > 0 && fullMatchCand.getCand().size() == assList.getSize() && fullMatchCand.getWeight() < fuzzyFullMatchCandList.begin()->getWeight())
                {
                    fullMatchCandList.pop_back();
                    fullMatchCand = fullMatchCandList.back();
                    i--;
                }
                if (i < index - 1)
                {
                    cand.erase(cand.begin() + index);
                    insertCandidate(cand, i + 1, fuzzyFullMatchCandList.begin()->getCand());
                }
            }
            else if (index == 1 && hasOmit)
            {
                if (fullMatchCandList.begin()->getWeight() < fuzzyFullMatchCandList.begin()->getWeight())
                {
                    cand.erase(cand.begin() + index);
                    insertCandidate(cand, 0, fuzzyFullMatchCandList.begin()->getCand());
                }
            }
            iCount++;
            halfMatchCandList.pop_front();
        }
        if (iCount > 20)
        {
            cand.pop_back();
            iCount--;
        }
    }

    for (itor = halfMatchCandList.begin();
        itor != halfMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    if (assList.getSize() == 2)
    {
        for (itor = fuzzyFullMatchCandList.begin();
            itor != fuzzyFullMatchCandList.end() && iCount < 20;
            itor++)
        {
            if (levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(0)), (TChar)tolower(itor->getCand().at(0)))
                && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(1)), (TChar)tolower(itor->getCand().at(1))))
            {
                if (removeRepetition(cand, itor->getCand()))
                {
                    cand.push_back(itor->getCand());
                    iCount++;
                }
            }
        }
    }

    if (fuzzyFullMatchCandList.size() == 0 && halfMatchCandList.size() > 0 && fuzzyHalfMatchCandList.size() > 0)
    {
        if (halfMatchCandList.begin()->getWeight() < fuzzyHalfMatchCandList.begin()->getWeight() && removeRepetition(cand, fuzzyHalfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index, fuzzyHalfMatchCandList.begin()->getCand());
            iCount++;
            fuzzyHalfMatchCandList.pop_front();
        }
        else if (halfMatchCandList.begin()->getWeight() >= fuzzyHalfMatchCandList.begin()->getWeight() && removeRepetition(cand, fuzzyHalfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index + 1, fuzzyHalfMatchCandList.begin()->getCand());
            iCount++;
            fuzzyHalfMatchCandList.pop_front();
        }
        if (iCount > 20)
        {
            cand.pop_back();
            iCount--;
        }
    }

    for (itor = fuzzyHalfMatchCandList.begin();
        itor != fuzzyHalfMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    int i = 0, j = 0, phraseFuzzyCount = 0;
    TString phraseWord;
    phraseWord.clear();
    if (phraseFullMatchCandList.size() > 0)
    {
        phraseWord = phraseFullMatchCandList.begin()->getCand();
        phraseFullMatchCandList.pop_front();
    }
    else if (phraseFuzzyFullMatchList.size() > 0)
    {
        phraseWord = phraseFuzzyFullMatchList.begin()->getCand();
        phraseFuzzyFullMatchList.pop_front();
    }

    if (phraseWord.size() > 0)
    {
        while (i < assList.getSize() && (j + 1) < phraseWord.size())
        {
            if (levenshteinDistance::isOmitChar(assList.getKeyChar(i)))
            {
                i++;
                continue;
            }
            else if (levenshteinDistance::isOmitChar((TChar)phraseWord.at(j)))
            {
                j++;
                continue;
            }
            else
            {
                if (levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(i)), (TChar)tolower(phraseWord.at(j))))
                {
                    phraseFuzzyCount++;
                }
                i++;
                j++;
            }
        }
        if (i < assList.getSize())
        {
            phraseFuzzyCount += (assList.getSize() - i);
        }
        else if ((j + 1) < phraseWord.size())
        {
            phraseFuzzyCount += (phraseWord.size() - j - 1);
        }
    }

    for (itor = phraseFullMatchCandList.begin();
        itor != phraseFullMatchCandList.end() && iCount < 20 && phraseCnt < OUTPUT_PHRASE_COUNT;
        itor++)
    {
        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
            phraseCnt++;
        }
    }

    for (itor = phraseFuzzyFullMatchList.begin();
        itor != phraseFuzzyFullMatchList.end() && iCount < 20 && phraseCnt < OUTPUT_PHRASE_COUNT;
        itor++)
    {
        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
            phraseCnt++;
        }
    }

    if (phraseFuzzyCount < 3 && phraseWord.size() > 0 && (index + phraseFuzzyCount) < iCount)
    {
        insertCandidate(cand, ((index + phraseFuzzyCount) < iCount ? (index + phraseFuzzyCount) : iCount), phraseWord);
    }
    if (cand.size() > 20)
    {
        cand.pop_back();
        iCount--;
    }

    if (cand.size() == 0)
    {
        predictAssociate(assList, candList);
        candList.sort();
        candList.reverse();
        for (itor = candList.begin(), iCount = 0;
            itor != candList.end() && iCount < 3;
            itor++)
        {
            if (removeRepetition(cand,itor->getCand()))
            {
                cand.push_back(itor->getCand());
                iCount++;
            }
        }
    }

    return hasfullMatch;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::predict(TString preword, Associate<TChar>& assList, vector<TString>& cand, EngineStatus status)
{
    bool hasfullMatch = false;
    TString inStr,nulStr;
    TString curPreword = preword;
    int iCount = 0,phraseCnt=0, index = 0;
    unsigned int searchOption = 0;
    list<CandItem<TString> > candList;
    list<CandItem<TString> > fullMatchCandList;
    list<CandItem<TString> > halfMatchCandList;
    list<CandItem<TString> > fuzzyFullMatchCandList;
    list<CandItem<TString> > fuzzyHalfMatchCandList;
    list<CandItem<TString> > phraseFullMatchCandList;
    list<CandItem<TString> > phraseFuzzyFullMatchList;

    typename list<CandItem<TString> >::iterator itor;

    if (status == S_START)
    {
        ptrieOut.resetDistSession();
    }

    ptrieOut.clearAllMatchCandSerial();
    if (closeFuzzySearch)
    {
        searchOption = searchOption | CLOSE_FUZZY_SEARCH;
    }
    if (closeHalfMatchSearch)
    {
        searchOption = searchOption | CLOSE_HALF_MATCH_SEARCH;
    }

    predictAssociate_Ext(assList, candList, inStr,0.0, searchOption);
    prewordAssociate(preword);

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFullMatchCandSerial(),0,nulStr,1.0,1.0,fullMatchCandList);

    fullMatchCandList.sort();
    fullMatchCandList.reverse();

    if (fullMatchCandList.size() != 0 || fuzzyFullMatchCandList.size() != 0)
    {
        hasfullMatch = true;
    }

    if (closeHalfMatchSearch == false)
    {
        nulStr.clear();
        ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getHalfMatchCandSerial(),0,nulStr,1.0,1.0,halfMatchCandList);
        halfMatchCandList.sort();
        halfMatchCandList.reverse();

        nulStr.clear();
        ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFuzzyHalfMatchCandSerial(),0,nulStr,1.0,1.0,fuzzyHalfMatchCandList);
        fuzzyHalfMatchCandList.sort();
        fuzzyHalfMatchCandList.reverse();
    }

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getFuzzyFullMatchCandSerial(),0,nulStr,1.0,1.0,fuzzyFullMatchCandList);
    fuzzyFullMatchCandList.sort();
    fuzzyFullMatchCandList.reverse();

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getPhraseFullMatchCandSerial(),0,nulStr,1.0,1.0,phraseFullMatchCandList);
    phraseFullMatchCandList.sort();
    phraseFullMatchCandList.reverse();

    nulStr.clear();
    ptrieOut.storePredictSerialWeightList(serialBuf+DICT_VERSION_SIZE,ptrieOut.getPhraseFuzzyFullMatchCandSerial(),0,nulStr,1.0,1.0,phraseFuzzyFullMatchList);
    phraseFuzzyFullMatchList.sort();
    phraseFuzzyFullMatchList.reverse();

    cand.clear();

    for (itor = fullMatchCandList.begin(), iCount = 0;
        itor != fullMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    index = iCount;

    for (itor = fuzzyFullMatchCandList.begin();
        itor != fuzzyFullMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (assList.getSize() == 2
            && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(0)), (TChar)tolower(itor->getCand().at(0)))
            && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(1)), (TChar)tolower(itor->getCand().at(1))))
        {
            continue;
        }

        if (removeRepetition(cand,itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    if (fuzzyFullMatchCandList.size() > 0 && halfMatchCandList.size() > 0 && iCount > index)
    {
        int i;
        bool hasOmit = false;
        if (fullMatchCandList.size() > 0)
        {
            for (i = 0; i < fullMatchCandList.begin()->getCand().size(); i++)
            {
                if (levenshteinDistance::isOmitChar((TChar)(fullMatchCandList.begin()->getCand().at(i))))
                {
                    hasOmit = true;
                    break;
                }
            }
        }

        if (fuzzyFullMatchCandList.begin()->getWeight() < halfMatchCandList.begin()->getWeight() && removeRepetition(cand, halfMatchCandList.begin()->getCand()))
        {

            if (index > 1)
            {
                i = index - 1;
                CandItem<TString> fullMatchCand = fullMatchCandList.back();
                while (i > 0 && fullMatchCand.getCand().size() == assList.getSize() && fullMatchCand.getWeight() < halfMatchCandList.begin()->getWeight())
                {
                    fullMatchCandList.pop_back();
                    fullMatchCand = fullMatchCandList.back();
                    i--;
                }
                insertCandidate(cand, i + 1, halfMatchCandList.begin()->getCand());
            }
            else if (index == 1 && hasOmit)
            {
                i = 1;
                if (fullMatchCandList.begin()->getWeight() < halfMatchCandList.begin()->getWeight())
                {
                    i = 0;
                }
                insertCandidate(cand, i, halfMatchCandList.begin()->getCand());
            }
            else
            {
                insertCandidate(cand, index, halfMatchCandList.begin()->getCand());
            }
            iCount++;
            halfMatchCandList.pop_front();
        }
        else if (fuzzyFullMatchCandList.begin()->getWeight() >= halfMatchCandList.begin()->getWeight() && removeRepetition(cand, halfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index + 1, halfMatchCandList.begin()->getCand());
            if (index > 1)
            {
                i = index - 1;
                CandItem<TString> fullMatchCand = fullMatchCandList.back();
                while (i > 0 && fullMatchCand.getCand().size() == assList.getSize() && fullMatchCand.getWeight() < fuzzyFullMatchCandList.begin()->getWeight())
                {
                    fullMatchCandList.pop_back();
                    fullMatchCand = fullMatchCandList.back();
                    i--;
                }
                if (i < index - 1)
                {
                    cand.erase(cand.begin() + index);
                    insertCandidate(cand, i + 1, fuzzyFullMatchCandList.begin()->getCand());
                }
            }
            else if (index == 1 && hasOmit)
            {
                if (fullMatchCandList.begin()->getWeight() < fuzzyFullMatchCandList.begin()->getWeight())
                {
                    cand.erase(cand.begin() + index);
                    insertCandidate(cand, 0, fuzzyFullMatchCandList.begin()->getCand());
                }
            }
            iCount++;
            halfMatchCandList.pop_front();
        }
        if (iCount > 20)
        {
            cand.pop_back();
            iCount--;
        }
    }

    for (itor = halfMatchCandList.begin();
        itor != halfMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    if (assList.getSize() == 2)
    {
        for (itor = fuzzyFullMatchCandList.begin();
            itor != fuzzyFullMatchCandList.end() && iCount < 20;
            itor++)
        {
            if (levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(0)), (TChar)tolower(itor->getCand().at(0)))
                && levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(1)), (TChar)tolower(itor->getCand().at(1))))
            {
                if (removeRepetition(cand, itor->getCand()))
                {
                    cand.push_back(itor->getCand());
                    iCount++;
                }
            }
        }
    }

    if (fuzzyFullMatchCandList.size() == 0 && halfMatchCandList.size() > 0 && fuzzyHalfMatchCandList.size() > 0)
    {
        if (halfMatchCandList.begin()->getWeight() < fuzzyHalfMatchCandList.begin()->getWeight() && removeRepetition(cand, fuzzyHalfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index, fuzzyHalfMatchCandList.begin()->getCand());
            iCount++;
            fuzzyHalfMatchCandList.pop_front();
        }
        else if (halfMatchCandList.begin()->getWeight() >= fuzzyHalfMatchCandList.begin()->getWeight() && removeRepetition(cand, fuzzyHalfMatchCandList.begin()->getCand()))
        {
            insertCandidate(cand, index + 1, fuzzyHalfMatchCandList.begin()->getCand());
            iCount++;
            fuzzyHalfMatchCandList.pop_front();
        }
        if (iCount > 20)
        {
            cand.pop_back();
            iCount--;
        }
    }

    for (itor = fuzzyHalfMatchCandList.begin();
        itor != fuzzyHalfMatchCandList.end() && iCount < 20;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
        }
    }

    int i = 0, j = 0, phraseFuzzyCount = 0;
    TString phraseWord;
    phraseWord.clear();
    if (phraseFullMatchCandList.size() > 0)
    {
        phraseWord = phraseFullMatchCandList.begin()->getCand();
        phraseFullMatchCandList.pop_front();
    }
    else if (phraseFuzzyFullMatchList.size() > 0)
    {
        phraseWord = phraseFuzzyFullMatchList.begin()->getCand();
        phraseFuzzyFullMatchList.pop_front();
    }

    if (phraseWord.size() > 0)
    {
        while (i < assList.getSize() && (j + 1) < phraseWord.size())
        {
            if (levenshteinDistance::isOmitChar(assList.getKeyChar(i)))
            {
                i++;
                continue;
            }
            else if (levenshteinDistance::isOmitChar((TChar)phraseWord.at(j)))
            {
                j++;
                continue;
            }
            else
            {
                if (levenshteinDistance::preciseCompChar((TChar)tolower(assList.getKeyChar(i)), (TChar)tolower(phraseWord.at(j))))
                {
                    phraseFuzzyCount++;
                }
                i++;
                j++;
            }
        }
        if (i < assList.getSize())
        {
            phraseFuzzyCount += (assList.getSize() - i);
        }
        else if ((j + 1) < phraseWord.size())
        {
            phraseFuzzyCount += (phraseWord.size() - j - 1);
        }
    }

    for (itor = phraseFullMatchCandList.begin();
        itor != phraseFullMatchCandList.end() && iCount < 20 && phraseCnt < OUTPUT_PHRASE_COUNT;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
            phraseCnt++;
        }
    }

    for (itor = phraseFuzzyFullMatchList.begin();
        itor != phraseFuzzyFullMatchList.end() && iCount < 20 && phraseCnt < OUTPUT_PHRASE_COUNT;
        itor++)
    {
        if (removeRepetition(cand, itor->getCand()))
        {
            cand.push_back(itor->getCand());
            iCount++;
            phraseCnt++;
        }
    }

    if (phraseFuzzyCount < 3 && phraseWord.size() > 0 && (index + phraseFuzzyCount) < iCount)
    {
        insertCandidate(cand, ((index + phraseFuzzyCount) < iCount ? (index + phraseFuzzyCount) : iCount), phraseWord);
    }
    if (cand.size() > 20)
    {
        cand.pop_back();
        iCount--;
    }

    if (cand.size() == 0)
    {
        predictAssociate(assList, candList);
        candList.sort();
        candList.reverse();
        for (itor = candList.begin(), iCount = 0;
            itor != candList.end() && iCount < 3;
            itor++)
        {
            if (removeRepetition(cand,itor->getCand()))
            {
                cand.push_back(itor->getCand());
                iCount++;
            }
        }
    }

    return hasfullMatch;
}


template <class TString, class TChar>
bool KikaEngine<TString, TChar>::select(TString& inStr, vector<TString>& cand, bool bMulti, bool bNewWord)
{
    TString outStr, currentStr = inStr;
    TString space;
    int i = 0, j = 0;
    int spaceNum = 0;
    space.push_back((TChar)0x20);

    while (1)
    {
        i = currentStr.find(space);

        if (i != -1)
            spaceNum = 1;

        while (1){
            j = currentStr.find(space, i + 1);
            if (j == i + 1){
                i = j;
                spaceNum++;
            }
            else{
                break;
            }
        }

        if (i == -1){
            outStr = currentStr;
            break;
        }
        else if (i == currentStr.size() - 1)
        {
            outStr = currentStr.substr(0, currentStr.size() - spaceNum);
            break;
        }

        currentStr = currentStr.substr(i + 1);
    }
    inStr = outStr;

    //ptrieUserLocal.insertAsUserWord(inStr, 1, false, true);
    bUdbChanged = insertAsUserWord(serialUserBuf, inStr);
    //ptrieUserLocal.rootToList(serialBuf);
    //bUdbChanged = true;
    ptrieOut.findBiDataExact(serialBuf+DICT_VERSION_SIZE, inStr, cand);
    return true;
}

//template <class TString, class TChar>
//bool KikaEngine<TString, TChar>::deletePhoneBook(TString& inStr)
//{
//    ptrieUserLocal.deletePhoneBookName(inStr);
//    ptrieUserLocal.rootToList(serialBuf+DICT_VERSION_SIZE);
//    bUdbChanged = true;
//    return true;
//}

template <class TString, class TChar>
void KikaEngine<TString, TChar>::predictAssociate(Associate<TChar>& assList,
    list<CandItem<TString> >& candList, TString& inStr, int idx,
    unsigned int uCorrectNum, EngineStatus status)
{
    unsigned int corNum = uCorrectNum;
    TString curStr;
    typename list<CharNode<TChar> >::iterator itor;

    if (idx < assList.getAssociateList().size())
    {
        AssociateNode<TChar> & assNode = assList.getAssociateList().at(idx);
        for (itor = assNode.charList.begin(); itor != assNode.charList.end(); itor++)
        {
            curStr = inStr;
            corNum = uCorrectNum;
            curStr.push_back(itor->getChar());

            if (itor->getFreq() < 1.0)
            {
                continue;
//                 corNum++;
//                 if (curStr.size()>3)
//                 {
//                     if (corNum > 2)
//                     {
//                         continue;
//                     }
//                 }
//                 else
//                 {
//                     if (corNum > 1)
//                     {
//                         continue;
//                     }
//                 }
            }

            predictAssociate(assList, candList, curStr, idx + 1, corNum, status);

            if (idx > 0 && corNum < 1 && itor == assNode.charList.begin())
            {
                curStr = inStr;
                TChar lastChar = curStr.back();

                if (lastChar == itor->getChar())
                {
                    continue;
                }
                curStr.pop_back();
                curStr.push_back(itor->getChar());
                curStr.push_back(lastChar);

                // do this or not will see.
                predictAssociate(assList, candList, curStr, idx + 1, corNum + 2, status);
            }
        }
    }
    else
    {
        ptrieUserLocal.findQuote(inStr, candList);
        //ptrieOut.PredictInSerial(serialBuf, inStr, candList, dCurFreq / idx);
        ptrieOut.PredictInSerial(serialBuf+DICT_VERSION_SIZE, inStr, candList, corNum);
//      ptrieOut.PredictInSerial_Ext(assList,serialBuf,ptrieUserLocal.userWordList, inStr, candList, corNum);
    }
}


template <class TString, class TChar>
void KikaEngine<TString, TChar>::predictAssociate(Associate<TChar>& assList,list<CandItem<TString> >& candList)
{
    TString curStr;
    typename list<CharNode<TChar> >::iterator itor;

    for(int i=0;i<assList.getAssociateList().size();i++)
    {
        AssociateNode<TChar> & assNode = assList.getAssociateList().at(i);
        itor = assNode.charList.begin();
        curStr.push_back(itor->getChar());
    }

//  ptrieUserLocal.findQuote(curStr, candList);
    ptrieOut.PredictInSerial(serialBuf+DICT_VERSION_SIZE, curStr, candList, 1);
}


template <class TString, class TChar>
bool KikaEngine<TString, TChar>::predict_Cells(Associate<TChar>& assList, vector<TString>& cand, EngineStatus status)
{
    TString inStr;
    int iCount = 0;
    list<CandItem<TString> > candList;
    typename list<CandItem<TString> >::iterator itor;

    if (status == S_START)
    {
        ptrieOut.resetDistSession();
    }
    predictCells(assList, candList);

    candList.sort();
    candList.reverse();

    cand.clear();

    for (itor = candList.begin(), iCount = 0;
        itor != candList.end() && iCount < 20;
        itor++, iCount++)
    {
        cand.push_back(itor->getCand());
    }

    return true;
}

template <class TString, class TChar>
bool KikaEngine<TString, TChar>::predictTrack(vector<TrackChar<TChar> >& allTrackCharVec,
    vector<TrackChar<TChar> >& allTrustCharVec, vector<TString>& cand)
{
    return ptrieOut.predictTrackInSerial(serialBuf+DICT_VERSION_SIZE, allTrackCharVec, allTrustCharVec, cand);
}

template <class TString, class TChar>
void KikaEngine<TString, TChar>::predictAssociate_Ext(Associate<TChar>& assList,
    list<CandItem<TString> >& candList, TString& inStr, double freq, unsigned int searchOption)
{
    TString curStr;
    typename list<CharNode<TChar> >::iterator itor;

    for(int idx=0;idx < assList.getAssociateList().size();idx++)
    {
        AssociateNode<TChar> & assNode = assList.getAssociateList().at(idx);
        itor = assNode.charList.begin();
        curStr.push_back(itor->getChar());
    }
    if (curStr.size() == 0)
    {
        return;
    }

    while(assList.getAssociateList().at(0).charList.size() > 3)
    {
        assList.getAssociateList().at(0).charList.pop_back();
    }

//    ptrieUserLocal.findQuote(curStr, candList);
    ptrieOut.PredictInSerial_Ext(assList,serialBuf+DICT_VERSION_SIZE,serialUserBuf+DICT_VERSION_SIZE,serialCellsBuf,serialUserBufForSys, curStr, candList, searchOption);
}


template <class TString, class TChar>
void KikaEngine<TString, TChar>::predictCells(Associate<TChar>& assList,list<CandItem<TString> >& candList)
{
    TString curStr;
    typename list<CharNode<TChar> >::iterator itor;

    for(int idx=0;idx < assList.getAssociateList().size();idx++)
    {
        AssociateNode<TChar>& assNode = assList.getAssociateList().at(idx);
        itor = assNode.charList.begin();
        curStr.push_back(itor->getChar());

    }

    ptrieUserLocal.findQuote(curStr, candList);
    ptrieOut.PredictInSerial(serialCellsBuf, curStr, candList, 1.0);
}

template <class TString, class TChar>
void KikaEngine<TString, TChar>::prewordAssociate(TString preword)
{
    TString inStr = preword;
    vector<unsigned int> candIds;
    vector<TString> precand;
    //ptrieUserLocal.rootToList(serialBuf);
    ptrieOut.findBiDataExact(serialBuf+DICT_VERSION_SIZE, inStr, precand);
    for (int i = 0; i < precand.size(); i++)
    {
            unsigned int prenodeIdx = ptrieOut.processFindInSerialExact(serialBuf+DICT_VERSION_SIZE, 0, precand[i]);
            candIds.push_back(prenodeIdx);
    }
    for (int i = 0; i < candIds.size(); i++)
    {
        typename list<MultiSerialPhrase<TString> >::iterator itor;
        if (ptrieOut.getFullMatchCandSerial().subListSerial.size() > 0)
        {
            list<MultiSerialPhrase<TString> > & subListFM = ptrieOut.getFullMatchCandSerial().subListSerial;
            for (itor = subListFM.begin(); itor != subListFM.end(); itor++)
            {
                if (candIds[i] == itor->getNodeId())
                {
                    itor->setWeight(itor->getWeight() * 2);
                }
            }
        }
        if (ptrieOut.getHalfMatchCandSerial().subListSerial.size() > 0)
        {
            list<MultiSerialPhrase<TString> > & subListHM = ptrieOut.getHalfMatchCandSerial().subListSerial;
            for (itor = subListHM.begin(); itor != subListHM.end(); itor++)
            {
                if (candIds[i] == itor->getNodeId())
                {
                    itor->setWeight(itor->getWeight() * 2);
                }
            }
        }
        if (ptrieOut.getFuzzyFullMatchCandSerial().subListSerial.size() > 0)
        {
            list<MultiSerialPhrase<TString> > & subListFFM = ptrieOut.getFuzzyFullMatchCandSerial().subListSerial;
            for (itor = subListFFM.begin(); itor != subListFFM.end(); itor++)
            {
                if (candIds[i] == itor->getNodeId())
                {
                    itor->setWeight(itor->getWeight() * 2);
                }
            }
        }
        if (ptrieOut.getFuzzyHalfMatchCandSerial().subListSerial.size() > 0)
        {
            list<MultiSerialPhrase<TString> > & subListFHM = ptrieOut.getFuzzyHalfMatchCandSerial().subListSerial;
            for (itor = subListFHM.begin(); itor != subListFHM.end(); itor++)
            {
                if (candIds[i] == itor->getNodeId())
                {
                    itor->setWeight(itor->getWeight() * 2);
                }
            }
        }

        /*PatriciaTrie.fullMatchCandSerial
        PatriciaTrie.halfMatchCandSerial
        PatriciaTrie.fuzzyFullMatchCandSerial
        PatriciaTrie.fuzzyHalfMatchCandSerial*/
    }
}