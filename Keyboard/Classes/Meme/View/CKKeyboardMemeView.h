//
//  CKKeyboardMemeView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/11.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CKKeyboardMemeViewDelegate <NSObject>

- (void)memeBottomBarButtonClicked:(UIButton *)button;

@end

@interface CKKeyboardMemeView : UIView
@property (nonatomic, weak) id<CKKeyboardMemeViewDelegate> delegate;
@property (nonatomic, strong) UIButton * deleteButton;
@end
