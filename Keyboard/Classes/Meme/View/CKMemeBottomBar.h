//
//  CKMemeBottomBar.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/11.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CKMemeBottomBarDelegate <NSObject>

- (void)memeBottomBarButtonClicked:(UIButton *)button;

@end

@interface CKMemeBottomBar : UIImageView
@property (nonatomic, weak) id<CKMemeBottomBarDelegate> delegate;
@property (nonatomic, strong) UIButton * recentButton;
@property (nonatomic, strong) UIButton * localButton;
@property (nonatomic, strong) UIButton * deleteButton;
@end
