//
//  CKKeyboardMemeView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/11.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKKeyboardMemeView.h"
#import "CKMemeKeyboardCollectionCell.h"
#import "CKMemeDataManager.h"
#import "CKMemeBottomBar.h"
#import "UIImageView+WebCache.h"
#import "CKKeyboardUImanger.h"
#import "CKLocalMemeModel.h"
@interface CKKeyboardMemeView()<UICollectionViewDataSource,UICollectionViewDelegate,CKMemeBottomBarDelegate>

{
    NSMutableArray * _recentArray;
    NSMutableArray * _memeArray;
    CGFloat collectionCellSize;
    CGFloat collectionCellPadding;
    CGFloat collectionLinePadding;
    CGFloat collectionViewLeftPadding;
    CGFloat collectionViewRightPadding;
    CGFloat collectionViewTopPadding;
    CGFloat collectionViewBottomPadding;
    CGFloat bottomViewHeight;
}

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, assign) BOOL nowRecent;
@property (nonatomic, strong) NSMutableArray *defaultMemeModelArray;
@property (nonatomic, strong) NSMutableArray *downloadedOnlineMemeModelArray;

@end

static NSString * const reuseIdentifier = @"CKMemeKeyboardCollectionCell";

@implementation CKKeyboardMemeView

- (NSMutableArray *)defaultMemeModelArray
{
    if (_defaultMemeModelArray == nil) {
        _defaultMemeModelArray = [NSMutableArray array];
        for (int i = 1; i < 21; i ++) {
            CKLocalMemeModel * memeModel = [[CKLocalMemeModel alloc] init];
            memeModel.selected = NO;
            memeModel.online = NO;
            memeModel.localImageName = [NSString stringWithFormat:@"defaultP_%02d.jpg",i];
            [_defaultMemeModelArray addObject:memeModel];
        }
    }
    return _defaultMemeModelArray;
}

- (NSMutableArray *)downloadedOnlineMemeModelArray
{
    if (_downloadedOnlineMemeModelArray == nil) {
        NSArray * temp = [NSArray arrayWithContentsOfURL:MEME_DOWLOADED_PLIST_URL];
        _downloadedOnlineMemeModelArray = [NSMutableArray array];
        for (NSString * imageName in temp) {
            CKLocalMemeModel * memeModel = [[CKLocalMemeModel alloc] init];
            memeModel.selected = NO;
            memeModel.online = YES;
            memeModel.localImageName = imageName;
            [_downloadedOnlineMemeModelArray addObject:memeModel];
        }
    }
    return _downloadedOnlineMemeModelArray;
}

- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"meme_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * realDimensions = [[CKKeyboardUImanger shareUImanger] getRealDimensionsByModelAndDirection:dimensions];

    collectionCellSize = [realDimensions[@"collectionCellSize"] floatValue];
    collectionLinePadding = [realDimensions[@"collectionLinePadding"] floatValue];
    collectionCellPadding = [realDimensions[@"collectionCellPadding"] floatValue];
    collectionViewLeftPadding = [realDimensions[@"collectionViewLeftPadding"] floatValue];
    collectionViewRightPadding = [realDimensions[@"collectionViewRightPadding"] floatValue];
    collectionViewTopPadding = [realDimensions[@"collectionViewTopPadding"] floatValue];
    collectionViewBottomPadding = [realDimensions[@"collectionViewBottomPadding"] floatValue];
    bottomViewHeight = [realDimensions[@"bottomViewHeight"] floatValue];
}

- (NSString *)getFullImagePath:(NSString *)imageName
{
    if ([imageName containsString:@"/"]) { // 这句代码是考虑到之前的时候我们RecentArray里存储的是路径,路径中包含了@"/"
        // 取出这种情况下的名字
        imageName = [imageName componentsSeparatedByString:@"/"].lastObject;
    }
    NSString * imagePath;
    if ([imageName hasPrefix:@"defaultP"]) { // 有这个前缀的话证明是本地图片
        imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:nil];
    }
    else{
        imagePath = [MEME_SAVE_URL URLByAppendingPathComponent:imageName].path;
    }
    return imagePath;
}

- (void)initArrays
{
    _recentArray = [NSMutableArray array];
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray * tempArray = (NSMutableArray *)[userDefaults objectForKey:@"Meme_recentArray"];
    if (tempArray != nil) {
        [_recentArray addObjectsFromArray:tempArray];
        NSFileManager * myFileManger = [NSFileManager defaultManager];
        for (NSInteger i = _recentArray.count - 1; i >= 0; i --) {
            if (![myFileManger fileExistsAtPath:[self getFullImagePath:_recentArray[i]]]) {
                [_recentArray removeObjectAtIndex:i];
            }
        }
    }
//    _memeArray = [NSMutableArray arrayWithContentsOfFile:MEME_SHARED_PLIST_URL.path];
}

- (void)saveRecentArray
{
    [CKSaveTools saveArray:_recentArray forKey:@"Meme_recentArray" inRegion:CKSaveRegionExtensionKeyboard];
}

- (void)updateRecentArray:(NSString *)currentItem
{
    if ([_recentArray containsObject:currentItem]) {
        int currentIndex = (int)[_recentArray indexOfObject:currentItem];
        if (currentIndex != 0) {
            [_recentArray removeObjectAtIndex:currentIndex];
            [_recentArray insertObject:currentItem atIndex:0];
        }
    }
    else {
        [_recentArray insertObject:currentItem atIndex:0];
        if (_recentArray.count > 15) {
            [_recentArray removeLastObject];
        }
    }
    [self saveRecentArray];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [CKBackend setSwitchKeyboardType:CKSwitchKeyboardTypeMeme];
        [self initArrays];
        [self initDimensions];

        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        UICollectionView * collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - bottomViewHeight) collectionViewLayout:flowLayout];
        [self addSubview:collectionView];
        self.collectionView = collectionView;
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;

        UINib *nib = [UINib nibWithNibName:reuseIdentifier bundle:nil];
        [self.collectionView registerNib:nib forCellWithReuseIdentifier:reuseIdentifier];
        self.backgroundColor  = [UIColor clearColor];
        self.collectionView.backgroundColor = [UIColor clearColor];
        collectionView.allowsSelection = YES;

        CKMemeBottomBar * bottomBar = [[CKMemeBottomBar alloc] initWithFrame:CGRectMake(0, self.frame.size.height - bottomViewHeight, self.frame.size.width, bottomViewHeight)];
        [self addSubview:bottomBar];
        bottomBar.delegate = self;
        if (_recentArray.count) { // 如果本地有数据
            self.nowRecent = YES;
        }
        [self.collectionView reloadData];
    }
    return self;
}

- (void)memeBottomBarButtonClicked:(UIButton *)button
{
    switch (button.tag) {
        case 0: // ABC
        {
            [self.delegate memeBottomBarButtonClicked:button];
        }
            break;
        case 1: // 切换到最近使用
        {
            self.nowRecent = YES;
            [self.collectionView reloadData];
        }
            break;
        case 2: // 切换到本地
        {
            self.nowRecent = NO;
            [self.collectionView reloadData];
        }
            break;
 
        case 3: // 跳转键盘
        {
            [CKSaveTools saveClickWithEventType:KB_Jump_To_MainApp_Click key:@"Meme"];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                               action:KB_Jump_To_MainApp_Click
                                                label:@"Meme"];
            NSString *msg = [NSString stringWithFormat:@"Open\n%@ App\nfor more!",APP_NAME];
            [CKCommonTools showInfoByFittingParentView:self msg:msg];
//            UIResponder * responder = self;
//            while ((responder = [responder nextResponder]) != nil) {
//                if ([responder respondsToSelector:@selector(openURL:)] == YES) {
//                    [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@.meme", APP_ROOT_URL]]];
//                }
//            }
        }
            
            break;
        case 4: // backspace
        {
            [self.delegate memeBottomBarButtonClicked:button];
        }
            break;

            
        default:
            break;
    }
}

#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.nowRecent) {
        return _recentArray.count;
    }
    else
    {
        return self.defaultMemeModelArray.count + self.downloadedOnlineMemeModelArray.count;
    }
}
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个UICollectionView展示的内容
-(CKMemeKeyboardCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CKMemeKeyboardCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[CKMemeKeyboardCollectionCell alloc] init];
    }
    NSString * memeImagePath;
    if (self.nowRecent) {
        NSString * memeImgName = _recentArray[indexPath.item];
        if ([memeImgName containsString:@"/"]) { // 这句代码是考虑到之前的时候我们RecentArray里存储的是路径,路径中包含了@"/"
            // 取出这种情况下的名字
            memeImgName = [memeImgName componentsSeparatedByString:@"/"].lastObject;
        }
        if ([memeImgName hasPrefix:@"defaultP"]) { // 有这个前缀的话证明是本地图片
            memeImagePath = [[NSBundle mainBundle] pathForResource:memeImgName ofType:nil];
        }
        else{
            memeImagePath = [MEME_SAVE_URL URLByAppendingPathComponent:memeImgName].path;
        }
    }
    else
    {
        if (indexPath.item < self.defaultMemeModelArray.count) {
            NSString *  localImageName = [NSString stringWithFormat:@"defaultP_%02d.jpg",(int)indexPath.item + 1];
            memeImagePath = [[NSBundle mainBundle] pathForResource:localImageName ofType:nil];
        }
        else
        {
            NSString * memeImageName = [self.downloadedOnlineMemeModelArray[indexPath.item - self.defaultMemeModelArray.count] localImageName];
            memeImagePath = [MEME_SAVE_URL URLByAppendingPathComponent:memeImageName].path;
        }
    }
    [cell.memeImageView sd_setImageWithURL:[NSURL fileURLWithPath:memeImagePath] placeholderImage:[UIImage imageWithContentsOfFile:memeImagePath]];
    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionCellSize, collectionCellSize);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return collectionLinePadding; // 行间距
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return collectionCellPadding; // cell间距
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(collectionViewTopPadding, collectionViewLeftPadding,collectionViewBottomPadding, collectionViewRightPadding); // 上下左右的偏移量
}

#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [CKSaveTools saveInteger:SelectedMemeInput forKey:SELECT_INPUT inRegion:CKSaveRegionExtensionKeyboard];
    [[NSNotificationCenter defaultCenter] postNotificationName:UserHaveDoneSomethingNotification object:nil];
    if (![CKCommonTools isOpenAccessGranted]) {
        float h = self.frame.size.height - 40;
        if (iPhone4Land || iPhone5Land || iPhone6Land){
            h = h * 1.3;
        }
        float w = h / 2 * 3;
        [CKCommonTools showFullAccessViewWithView:self frame:CGRectMake(0, 0, w, h)];
        return;
    }
    
    NSString * memeImagePath;
    if (self.nowRecent) {
        NSString * memeImgName = _recentArray[indexPath.item];
        if ([memeImgName hasPrefix:@"defaultP"]) { // 有这个前缀的话证明是本地图片
            memeImagePath = [[NSBundle mainBundle] pathForResource:memeImgName ofType:nil];
        }
        else{
            memeImagePath = [MEME_SAVE_URL URLByAppendingPathComponent:memeImgName].path;
        }
        [self updateRecentArray:memeImgName];
    }
    else
    {
        if (indexPath.item < self.defaultMemeModelArray.count) {
            NSString *  localImageName = [NSString stringWithFormat:@"defaultP_%02d.jpg",(int)indexPath.item + 1];
            memeImagePath = [[NSBundle mainBundle] pathForResource:localImageName ofType:nil];
            [self updateRecentArray:localImageName];
            [CKSaveTools saveClickWithEventType:KB_Meme_Click key:localImageName];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                               action:KB_Meme_Click
                                                label:localImageName];
        }
        else
        {
            NSString * memeImageName = [self.downloadedOnlineMemeModelArray[indexPath.item - self.defaultMemeModelArray.count] localImageName];
            memeImagePath = [MEME_SAVE_URL URLByAppendingPathComponent:memeImageName].path;
            [CKSaveTools saveClickWithEventType:KB_Meme_Click key:memeImageName];
            [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                               action:KB_Meme_Click
                                                label:memeImageName];
            [self updateRecentArray:memeImageName];
        }
    }
    
    UIImage * memeImage = [UIImage imageWithContentsOfFile:memeImagePath];
    if (memeImage != nil) {
        [CKBackend setMemeUsedInApp:YES];
        UIPasteboard *pasteBoard=[UIPasteboard generalPasteboard];
        pasteBoard.image = [UIImage imageWithContentsOfFile:memeImagePath];
        [CKCommonTools showInfoByFittingParentView:self msg:@"Now paste it \nin a message!"];
    }
    if (self.nowRecent) {
        [self.collectionView reloadData];
    }
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

@end
