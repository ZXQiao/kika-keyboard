//
//  CKMemeCollectionCell.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/11.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CKMemeKeyboardCollectionCell : UICollectionViewCell
@property (nonatomic,strong) UIImageView * memeImageView;
@end
