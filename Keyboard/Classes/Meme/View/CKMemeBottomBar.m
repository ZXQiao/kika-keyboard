//
//  CKMemeBottomBar.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/11.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMemeBottomBar.h"
#import "CKKeyboardUImanger.h"

@interface CKMemeBottomBar()

{
    CGFloat bottomViewHeight;
    CGFloat bottomViewButtonHeight;
    CGFloat bottomViewResFuncPadding;
    CGFloat bottomViewFuncButtonWidth;
    UIButton * _selectedBackButton;
}

@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * moreButton;
@property (nonatomic, strong) UIButton * selectedButton;
@property (nonatomic, strong) NSMutableArray * recentArray;
@end

@implementation CKMemeBottomBar

- (void)refreshSelectedBackgroundButton
{
    if (_selectedBackButton != nil) {
        [_selectedBackButton removeFromSuperview];
        _selectedBackButton = nil;
    }
    _selectedBackButton = [[UIButton alloc] init];
    _selectedBackButton.enabled = NO;
    UIImage * selectBackImage;
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    if (nowUseTheme) {
        if (nowUseTheme.isOnline) {
            selectBackImage = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emoji_select" resize:NO];
        }
        else{
            selectBackImage = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emoji_select" resize:NO];
        }
    }
    if (!nowUseTheme) {
        selectBackImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_emoji_select@2x.png",Default_Theme_Name] ofType:nil]]];
    }
    [_selectedBackButton setImage:selectBackImage forState:UIControlStateNormal];
    [_selectedBackButton setImage:selectBackImage forState:UIControlStateHighlighted];
    _selectedBackButton.adjustsImageWhenDisabled = NO;
    [self addSubview:_selectedBackButton];
}

- (void)initDimensions
{
    NSString * resourceTypePath = [[NSBundle mainBundle] pathForResource:@"meme_dimensions.plist" ofType:nil];
    NSDictionary * dimensions = [NSDictionary dictionaryWithContentsOfFile:resourceTypePath];
    NSDictionary * realDimensions = [[CKKeyboardUImanger shareUImanger] getRealDimensionsByModelAndDirection:dimensions];
    bottomViewHeight = [realDimensions[@"bottomViewHeight"] floatValue];
    bottomViewButtonHeight = [realDimensions[@"bottomViewButtonHeight"] floatValue];
    bottomViewResFuncPadding = [realDimensions[@"bottomViewResFuncPadding"] floatValue];
    bottomViewFuncButtonWidth = [realDimensions[@"bottomViewFuncButtonWidth"] floatValue];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        [self initDimensions];
        [self refreshSelectedBackgroundButton];
        UIButton * backButton = [[UIButton alloc] init];
        self.backButton = backButton;
        backButton.adjustsImageWhenHighlighted = NO;
        [self addSubview:backButton];
        backButton.tag = 0;
        [backButton setTitle:@"ABC" forState:UIControlStateNormal];
        NSString * fontName = nil;
        NSString * emoji_norlmal_color = nil;
        NSString * emoji__highlighted = nil;
        CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
        if (nowUseTheme) {
            fontName = nowUseTheme.fontName;
            emoji_norlmal_color = nowUseTheme.emoji_icon_normal;
            emoji__highlighted = nowUseTheme.emoji_icon_highlight;
        }
        else {
            fontName = Default_FontName;
            emoji_norlmal_color = Default_Emoji_Normal_Color;
            emoji__highlighted = Default_Emoji_Highed_Color;
        }
        CGFloat defaultFontSize = [[CKKeyboardUImanger shareUImanger] getFunctionFontSize:fontName];
        UIFont * font = [UIFont fontWithName:buttonDefaultFontName size:defaultFontSize];
        if ([fontName isEqualToString:@"LithosPro-Black"]) {
            font = [UIFont fontWithName:fontName size:defaultFontSize];
        }else if([fontName isEqualToString:@"Chalkboard SE"])
        {
            font = [UIFont fontWithName:fontName size:defaultFontSize];
        }
        backButton.titleLabel.font = font;
        [backButton setTitleColor:[UIColor colorFromHexString:emoji_norlmal_color] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        UIButton *recentButton = [[UIButton alloc] init];
        self.recentButton = recentButton;
        [self addSubview:recentButton];
        [recentButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        recentButton.tag = 1;
        UIImage * recentButtonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"recent@2x.png" ofType:nil]];
        UIImage * recentButtonImageSelected = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"recent_selected@2x.png" ofType:nil]];
        [recentButton setImage:[recentButtonImage tintImageWithColor:[UIColor colorFromHexString:emoji_norlmal_color]] forState:UIControlStateNormal];
        [recentButton setImage:[recentButtonImageSelected tintImageWithColor:[UIColor colorFromHexString:emoji__highlighted]] forState:UIControlStateSelected];
        recentButton.backgroundColor = [UIColor clearColor];
        
        UIButton * localButton = [[UIButton alloc] init];
        self.localButton = localButton;
        [self addSubview:localButton];
        localButton.tag = 2;
        [localButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        UIImage * localButtonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"local@2x.png" ofType:nil]];
        UIImage * localButtonImageSelected = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"local_selected@2x.png" ofType:nil]];
        [localButton setImage:[localButtonImage tintImageWithColor:[UIColor colorFromHexString:emoji_norlmal_color]] forState:UIControlStateNormal];
        [localButton setImage:[localButtonImageSelected tintImageWithColor:[UIColor colorFromHexString:emoji__highlighted]] forState:UIControlStateSelected];
        localButton.backgroundColor = [UIColor clearColor];
        
        UIButton * moreButton = [[UIButton alloc] init];
        self.moreButton = moreButton;
        [self addSubview:moreButton];
        moreButton.tag = 3;
        moreButton.backgroundColor = [UIColor clearColor];
        [moreButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        UIImage * moreButtonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"more@2x.png" ofType:nil]];
        UIImage * moreButtonImageSelected = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"more_selected@2x.png" ofType:nil]];
        [moreButton setImage:[moreButtonImage tintImageWithColor:[UIColor colorFromHexString:emoji_norlmal_color]]forState:UIControlStateNormal];
        [moreButton setImage:[moreButtonImageSelected tintImageWithColor:[UIColor colorFromHexString:emoji__highlighted]] forState:UIControlStateSelected];
        
        UIButton * deleteButton = [[UIButton alloc] init];
        self.deleteButton = deleteButton;
        [self addSubview:deleteButton];
        deleteButton.adjustsImageWhenHighlighted = NO;
        deleteButton.tag = 4;
        [deleteButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];

        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        NSMutableArray * tempArray = (NSMutableArray *)[userDefaults objectForKey:@"Meme_recentArray"];
        self.recentArray = tempArray;
        if (self.recentArray.count) {
            self.selectedButton = recentButton;
        }
        else {
            self.selectedButton = localButton;
        }
        self.selectedButton.selected = YES;
        
        self.userInteractionEnabled = YES;
        
        [self setUpImage];
    }
    return self;
}

- (void)setUpImage
{
    // recent跟emjoji上面分类的选中态一致
    // more跟return字体和背景一致
    UIColor * normalColor;
//    UIColor * highLightColor;
//    UIColor * fontColor;
    UIImage * bottomBarBackImage;
    UIImage * deleteImage;

//    NSURL *containerURL = [CKCommonTools GetAPP_GROUPS_Library_ThemesURL];
    CKMainThemeModel * nowUseTheme = [CKMainThemeModel sholdUseThisModel];
    if (nowUseTheme) {
//        highLightColor = [UIColor colorFromHexString:nowUseTheme.emoji_icon_highlight];
//        fontColor = [UIColor colorFromHexString:nowUseTheme.emoji_icon_normal];
    normalColor = [UIColor colorFromHexString:nowUseTheme.emoji_icon_normal];
        if (nowUseTheme.isOnline) {
            bottomBarBackImage = [UIImage imageWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emojibar" resize:YES];
        }
        else{
            bottomBarBackImage = [UIImage imageFromMainBundleWithThemeName:nowUseTheme.themeName ImageNameExtension:@"_emojibar" resize:YES];
        }
    }
    if (!nowUseTheme || bottomBarBackImage == nil) {
        bottomBarBackImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_emojibar@2x.png",Default_Theme_Name] ofType:nil]]];
//        highLightColor = [UIColor colorFromHexString:Default_Meme_Bottom_Highted_Color];
//        fontColor = [UIColor colorFromHexString:Default_Font_Color];
        normalColor = [UIColor colorFromHexString:Default_Emoji_Normal_Color];
    }

    deleteImage = [UIImage imageNamed:@"universal_delete_in"];
    self.image = bottomBarBackImage;

    UIImage * functionButtonBgImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"function_button_bg@2x.png" ofType:nil]]];
    UIImage * functionButtonPressedImage = [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"function_button_pressed@2x.png" ofType:nil]]];

    [self.backButton setBackgroundImage:functionButtonPressedImage forState:UIControlStateHighlighted];
    [self.backButton setBackgroundImage:functionButtonBgImage forState:UIControlStateNormal];
    [self.moreButton setBackgroundImage:functionButtonPressedImage forState:UIControlStateHighlighted];
    [self.moreButton setBackgroundImage:functionButtonBgImage forState:UIControlStateNormal];
    [self.deleteButton setBackgroundImage:functionButtonPressedImage forState:UIControlStateHighlighted];
    [self.deleteButton setBackgroundImage:functionButtonBgImage forState:UIControlStateNormal];
    [self.deleteButton setImage:[deleteImage tintImageWithColor:normalColor] forState:UIControlStateNormal];
}


- (void)layoutSubviews
{
    [super layoutSubviews];

    CGFloat topPadding = (bottomViewHeight - bottomViewButtonHeight) / 2;

    self.backButton.frame = CGRectMake(0, topPadding, bottomViewFuncButtonWidth, bottomViewButtonHeight);

    CGFloat deleteX = self.frame.size.width - bottomViewFuncButtonWidth;
    self.deleteButton.frame = CGRectMake(deleteX, topPadding, bottomViewFuncButtonWidth, bottomViewButtonHeight);

    CGFloat moreX = self.frame.size.width - bottomViewFuncButtonWidth * 2;
    self.moreButton.frame = CGRectMake(moreX, topPadding, bottomViewFuncButtonWidth, bottomViewButtonHeight);
    
    CGFloat resButtonW = (moreX - bottomViewFuncButtonWidth - bottomViewResFuncPadding * 2) / 2;
    self.recentButton.frame = CGRectMake(bottomViewFuncButtonWidth + bottomViewResFuncPadding, topPadding, resButtonW, bottomViewButtonHeight);
    self.localButton.frame = CGRectMake(bottomViewFuncButtonWidth + bottomViewResFuncPadding +  resButtonW, topPadding, resButtonW, bottomViewButtonHeight);
    _selectedBackButton.frame = CGRectMake(0, 0, self.selectedButton.frame.size.width, self.selectedButton.frame.size.height);
    _selectedBackButton.center = self.selectedButton.center;
}

- (void)buttonClicked:(UIButton *)button
{
    if (button.tag == 1 || button.tag == 2) {
        self.selectedButton.selected = NO;
        button.selected = YES;
        self.selectedButton = button;
        
    _selectedBackButton.frame = CGRectMake(0, 0, button.frame.size.width, button.frame.size.height);
    _selectedBackButton.center = button.center;

    }
    if(button.tag == 0){
        [[NSNotificationCenter defaultCenter] postNotificationName:UserClickBottomBackButtonNotification object:@"meme"];
    }
    if ([self.delegate respondsToSelector:@selector(memeBottomBarButtonClicked:)]) {
        [self.delegate memeBottomBarButtonClicked:button];
    }
    
}
@end
