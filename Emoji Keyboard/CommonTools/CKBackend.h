//
//  CKCommonTools.h
//  Emoji Keyboard
//
//

#import <Foundation/Foundation.h>

#define Full_Access_Type (@"Full_Access_Type")

typedef NS_ENUM(int, CKFullAccessType){
    CKFullAccessTypeDefault = 0,
    CKFullAccessTypeTutorial,
    CKFullAccessTypeVideo,
    CKFullAccessTypeHelp
};

typedef NS_ENUM(int, CKSwitchKeyboardType){
    CKSwitchKeyboardTypeDefault = 0,
    CKSwitchKeyboardTypeEmoji,
    CKSwitchKeyboardTypeMeme
};

@interface CKBackend : NSObject

+(NSMutableArray *)getEventArray;
+(NSMutableDictionary *)getEventDicts;
+(void)sendEventDataToBackend;
+(void)sendCrashDataToBackend:(NSArray *)stacktrace;
+(void)sendMetaDataToBackend;
+(void)setSwitchKeyboardType:(CKSwitchKeyboardType)type;
+(CKSwitchKeyboardType)getSwitchKeyboardType;
+(void)setEmojiUsedInApp:(BOOL)used;
+(BOOL)getEmojiUsedInApp;
+(void)setStickerUsedInApp:(BOOL)used;
+(BOOL)getStickerUsedInApp;
+(void)setGifUsedInApp:(BOOL)used;
+(BOOL)getGifUsedInApp;
+(void)setMemeUsedInApp:(BOOL)used;
+(BOOL)getMemeUsedInApp;

@end
