//
//  CKSaveTools.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/17.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKSaveTools.h"
#import <time.h>


@implementation CKSaveTools

+ (BOOL)saveMainTheme:(CKMainThemeModel *)mainTheme toRegion:(CKSaveRegion)region
{
    NSString *filePath = [CKSaveTools pathWithRegion:region];
    return [NSKeyedArchiver archiveRootObject:mainTheme toFile:filePath];
}


+(CKMainThemeModel *)takeOutMainThemefromRegion:(CKSaveRegion)region
{
    CKMainThemeModel *mainTheme = [NSKeyedUnarchiver unarchiveObjectWithFile:[CKSaveTools pathWithRegion:region]];
    return mainTheme;
}

+(NSString *)pathWithRegion:(CKSaveRegion)region
{
    NSString *filePath;
    switch (region) {
        case CKSaveRegionContainingApp:
            // 沙盒路径,这里不用区分是主App还是Extension的沙盒,谁调用这个方法,沙盒就是谁的
            filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"mainTheme.data"];
            break;
        case CKSaveRegionExtensionKeyboard:
            filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"mainTheme.data"];
            break;
        case  CKSaveRegionAppGroup:
            // 创建文件夹,App Group中的文件夹
        {
            NSURL * groupUrl = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS];
            filePath = [groupUrl URLByAppendingPathComponent:@"mainTheme.data"].path;
        }
            break;
        default:
            break;
    }
    return filePath;
}

+(void)saveString:(NSString *)valueString forKey:(NSString *)key inRegion:(CKSaveRegion)region
{
    switch (region) {
        case CKSaveRegionAppGroup: // AppGroup
        {
            NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
            [sharedDefaults setObject:valueString forKey:key];
            [sharedDefaults synchronize];
        }
            break;
        case CKSaveRegionExtensionKeyboard : // NSUserDefaults
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:valueString forKey:key];
            [userDefaults synchronize];
        }
        case CKSaveRegionContainingApp : // NSUserDefaults
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:valueString forKey:key];
            [userDefaults synchronize];

        }
  
            break;
        default:
            break;
    }
}

// 保存Integer值
+(void)saveInteger:(NSInteger )valueinteger forKey:(NSString *)key inRegion:(CKSaveRegion)region
{
    switch (region) {
        case CKSaveRegionAppGroup: // AppGroup
        {
            NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
            [sharedDefaults setInteger:valueinteger forKey:key];
            [sharedDefaults synchronize];
        }
            break;
        case CKSaveRegionExtensionKeyboard: // NSUserDefaults
            [self saveToDefaultsWithKey:key intValue:valueinteger];
            break;
        case CKSaveRegionContainingApp:
            [self saveToDefaultsWithKey:key intValue:valueinteger];
            break;
        default:
            break;
    }
}

+ (void)saveToDefaultsWithKey:(NSString *)key intValue:(NSUInteger)value
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:value forKey:key];
    [userDefaults synchronize];
    
}

// 保存数组
+(void)saveArray:(NSArray *)valueArray forKey:(NSString *)key inRegion:(CKSaveRegion)region
{
    switch (region) {
        case CKSaveRegionAppGroup: // AppGroup
        {
            NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
            [sharedDefaults setObject:valueArray forKey:key];
            [sharedDefaults synchronize];
        }
            break;
        case CKSaveRegionExtensionKeyboard: // NSUserDefaults
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:valueArray forKey:key];
            [userDefaults synchronize];
        }
            break;
        case CKSaveRegionContainingApp:
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:valueArray forKey:key];
            [userDefaults synchronize];
        }
            break;
        default:
            break;
    }
}

/**
 *  数据统计
 */
+(void)saveClickWithEventType:(NSString *)eventType key:(NSString *)key
{
    NSMutableDictionary * dicts = [CKBackend getEventDicts];
    NSMutableDictionary * dict = [dicts objectForKey:eventType];
    if (dict == nil) {
        dict = [NSMutableDictionary dictionary];
    }
    int count = [[dict objectForKey:key] intValue];
    count++;
    dict[key] = @(count);
    dict[[key stringByAppendingString:@"_time"]] = @(time(NULL));
    dicts[eventType] = dict;

    NSMutableArray * eventArray = [CKBackend getEventArray];
    if (![eventArray containsObject:eventType]) {
        [eventArray addObject:eventType];
    }
}

@end
