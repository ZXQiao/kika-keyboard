//
//  CKCommonTools.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/21.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CKAllowFullAccessView.h"

@interface CKCommonTools : NSObject

+(BOOL)screenDirectionIsLand;
+(NSURL*)GetAPP_GROUPS_Library_ThemesURL;
+(void)showInfoByFittingParentView:(UIView *)parentView msg:(NSString *)msg;
+(float)systemVersion;
/**
 *  用户是否允许完全访问
 *
 */
+ (BOOL)isOpenAccessGranted;
+ (void)showFullAccessViewWithView:(UIView *)view frame:(CGRect)frame;
+ (void)asyncDoSthWithBlock:(void(^)(void))block;

@end
