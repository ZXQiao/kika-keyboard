//
//  CKBackend.m
//  Emoji Keyboard
//
//

#import "CKBackend.h"
#import <zlib.h>
#import "AFNetworking.h"
#import <CoreFoundation/CFString.h>
#import <CommonCrypto/CommonDigest.h>
#import <time.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

#define TEST_SERVER_URL @"http://192.168.100.13/api.php?type=%@&duid=123"
#define BACKEND_DEBUG_SERVER_URL @"http://dc.kika-backend.com/api.php?type=debug&duid=123"
#define BACKEND_EVENT_SERVER_URL @"http://dc.kika-backend.com/api.php?type=%@&duid=123"

#define PUBLIC_RSA_KEY "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDknDV5/aLBz82aTuN2oTEBQMGZJ9aZ2zlHRmA0kPaqzUiBR3/SP/jj180NOrebvG8OgFRbFdcz2RMpo14auIqTN5BsP9KQ3TpuprIlYd+ESjwuDBgcri9ulK/bzS0YtOtutOP7ThObxWF5cHANlZLdVlS1vp8cGyrq5aoRO3E/RQIDAQAB";

#define SERVER_URL BACKEND_EVENT_SERVER_URL

#define SDK_VERSION "1.1.0.0"

#define META_INTERVAL 43200 //(12*3600)

@implementation CKBackend

static NSMutableArray * global_eventArray = nil;
static NSMutableDictionary * global_eventDicts = nil;

+(NSMutableArray *)getEventArray
{
    if (global_eventArray == nil) {
        NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
        global_eventArray = [NSMutableArray arrayWithArray:[sharedDefaults arrayForKey:KB_Event_Array]];
        if (global_eventArray == nil) {
            global_eventArray = [NSMutableArray array];
        }
        [sharedDefaults removeObjectForKey:KB_Event_Array];
        [sharedDefaults synchronize];
    }
    return global_eventArray;
}

+(NSMutableDictionary *)getEventDicts
{
    if (global_eventDicts == nil) {
        global_eventDicts = [NSMutableDictionary dictionary];
        NSMutableArray * eventArray = [self getEventArray];
        NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
        for (NSString * eventType in eventArray) {
            NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:[sharedDefaults objectForKey:eventType]];
            global_eventDicts[eventType] = dict;
            [sharedDefaults removeObjectForKey:eventType];
        }
        [sharedDefaults synchronize];
    }
    return global_eventDicts;
}
/*
+(NSString *)getNetworkType
{
    NSArray *subviews = [[[[UIApplication sharedApplication] valueForKey:@"statusBar"] valueForKey:@"foregroundView"]subviews];
    NSNumber *dataNetworkItemView = nil;

    for (id subview in subviews) {
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
            dataNetworkItemView = subview;
            break;
        }
    }

    NSString * networkType = nil;
    switch ([[dataNetworkItemView valueForKey:@"dataNetworkType"]integerValue]) {
        case 0:
            networkType = @"No Service";
            break;
        case 1:
            networkType = @"2G";
            break;
        case 2:
            networkType = @"3G";
            break;
        case 3:
            networkType = @"4G";
            break;
        case 4:
            networkType = @"LTE";
            break;
        case 5:
            networkType = @"Wifi";
            break;
        default:
            break;
    }
    return networkType;
}
*/

+(NSString *)getCarrierName
{
    CTTelephonyNetworkInfo *telephonyInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [telephonyInfo subscriberCellularProvider];
    NSString *currentCountry=[carrier carrierName];
    return currentCountry;
}

+(NSString *) getUUID {
    NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    NSString * uuid_saved = [sharedDefaults stringForKey:@"uuid"];
    if (uuid_saved == nil) {
        CFUUIDRef puuid = CFUUIDCreate( nil );
        CFStringRef uuidString = CFUUIDCreateString( nil, puuid );
        CFRelease(puuid);
        NSString * str = (__bridge NSString *)uuidString;
        [CKSaveTools saveString:str forKey:@"uuid" inRegion:CKSaveRegionAppGroup];
        CFRelease(uuidString);
        return str;
    }
    else {
        return uuid_saved;
    }
}

+(NSString *)getMD5String:(NSString *)inStr
{
    const char *original_str = [inStr UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, inStr.length, result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++) {
        [hash appendFormat:@"%02X", result[i]];
    }
    return [hash lowercaseString];
}

+(NSMutableData *)getCommonBody
{
    char* sdk_version = SDK_VERSION;
    short sdk_version_len = strlen(sdk_version);
    char* agent_key = BACKEND_AGENT_KEY;
    short agent_key_len = strlen(agent_key);
    char* lang = "en";
    short lang_len = strlen(lang);
    const char* duid = [[self getUUID] UTF8String];
    short duid_len = strlen(duid);
    
    NSMutableData * commonBody = [[NSMutableData alloc] init];
    char padding = 0;
    [commonBody appendBytes:&padding length:1];
    [commonBody appendBytes:&sdk_version_len length:1];
    [commonBody appendBytes:sdk_version length:sdk_version_len];
    
    [commonBody appendBytes:&padding length:1];
    [commonBody appendBytes:&agent_key_len length:1];
    [commonBody appendBytes:agent_key length:agent_key_len];
    
    [commonBody appendBytes:&padding length:1];
    [commonBody appendBytes:&lang_len length:1];
    [commonBody appendBytes:lang length:lang_len];
    
    [commonBody appendBytes:&padding length:1];
    [commonBody appendBytes:&duid_len length:1];
    [commonBody appendBytes:duid length:duid_len];

    return commonBody;
}

+(NSData *)getRealBody:(NSDictionary *)dictionary
{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@";" withString:@"\n\t"];
    jsonString = [jsonString stringByAppendingString:@"\n"];
    jsonData = [jsonString dataUsingEncoding: NSUTF8StringEncoding];
    return jsonData;
}

+(NSData *)getCompressedData:(NSData *)body
{
    NSMutableData * data = [[NSMutableData alloc] init];
    NSInteger bodyLength = body.length;

    char* compressedBytes;
    compressedBytes = (char*)malloc(bodyLength);
    NSInteger compressedLength = bodyLength;
    compress((Bytef *)compressedBytes, (uLongf*)&compressedLength, (const Bytef*)body.bytes, bodyLength);

    [data appendBytes:compressedBytes length:compressedLength];
    free(compressedBytes);
    return data;
}

+(NSData *)getEncryptedData:(NSData *)body encryptType:(char)encryptType
{
    NSMutableData * data = [[NSMutableData alloc] init];
    NSInteger bodyLength = body.length;

    char* encryptedBytes = NULL;
    NSInteger encryptedLength = 0;

    if (encryptType == 2) {
        char length_mod = bodyLength % 128;
        encryptedBytes = (char*)malloc(bodyLength);
        for (int i = 0; i < bodyLength; i++) {
            encryptedBytes[i] = ((const Bytef*)body.bytes)[i] ^ length_mod;
        }
        encryptedLength = bodyLength;
    }
    [data appendBytes:encryptedBytes length:encryptedLength];
    free(encryptedBytes);
    return data;
}

+(void)sendDataToBackend:(NSData *)data type:(NSString *)type removeKey:(NSString *)removeKey
{
    NSString *url = [NSString stringWithFormat:SERVER_URL,type];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setTimeoutInterval:15];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    request.HTTPBody = data;
    NSMutableDictionary * dicts = [CKBackend getEventDicts];
    NSMutableArray * eventArray = [CKBackend getEventArray];
    NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];

    AFHTTPRequestOperation* operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([type isEqualToString:@"operate"]) {
            [dicts removeObjectForKey:removeKey];
            [eventArray removeObject:removeKey];
        }
        else if ([type isEqualToString:@"meta"]) {
            NSInteger tm_now = (NSInteger)time(NULL);
            [CKSaveTools saveInteger:tm_now forKey:@"meta_time" inRegion:CKSaveRegionAppGroup];
        }
        NSLog(@"success");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([type isEqualToString:@"operate"]) {
            NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:[sharedDefaults objectForKey:removeKey]];
            if (dict == nil || dict.count == 0) {
                [sharedDefaults setObject:[dicts objectForKey:removeKey] forKey:removeKey];
                [sharedDefaults synchronize];
            }
            NSMutableArray * eventArray = [NSMutableArray arrayWithArray:[sharedDefaults arrayForKey:KB_Event_Array]];
            if (eventArray == nil) {
                eventArray = [NSMutableArray array];
            }
            if (![eventArray containsObject:removeKey]) {
                [eventArray addObject:removeKey];
                [sharedDefaults setObject:eventArray forKey:KB_Event_Array];
                [sharedDefaults synchronize];
            }
        }
        NSLog(@"error");
    }];

    [operation start];
}

+(void)sendEventDataToBackend
{
    bool isCompressed = true;
    char encryptType = 0;

    NSMutableData * commonBody = [self getCommonBody];

    NSArray * eventArray = [CKBackend getEventArray];
    NSMutableDictionary * dicts = [CKBackend getEventDicts];

    for (NSString * eventType in eventArray) {
        NSDictionary * dict = [dicts objectForKey:eventType];
        if (dict != nil) {
            for (NSString * key in dict.allKeys) {
                if ([key containsString:@"_time"]) {
                    continue;
                }
                int count = [dict[key] intValue];
                NSString * count_str = [NSString stringWithFormat:@"%d",count];
                NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
                [dictionary setValue:eventType forKey:@"l"];
                int time = [dict[[key stringByAppendingString:@"_time"]] intValue];
                [dictionary setObject:@(time) forKey:@"ts"];
                [dictionary setValue:@"click" forKey:@"otp"];

                NSMutableDictionary *extra_dictionary = [[NSMutableDictionary alloc] init];
                [extra_dictionary setValue:key forKey:@"n"];
                [extra_dictionary setValue:count_str forKey:@"c"];
                [dictionary setValue:extra_dictionary forKey:@"extra"];
                [dictionary setValue:@"category" forKey:@"iid"];

                NSString * oid = [[[NSString stringWithUTF8String:BACKEND_AGENT_KEY] stringByAppendingString:eventType] stringByAppendingString:@"category"];

                [dictionary setValue:[self getMD5String:oid]forKey:@"oid"];
                [dictionary setValue:@"event" forKey:@"tp"];

                NSMutableData * body = [[NSMutableData alloc] init];

                [body appendData:commonBody];
                [body appendData:[self getRealBody:dictionary]];

                NSMutableData * data = [[NSMutableData alloc] init];
                [data appendBytes:&isCompressed length:1];
                [data appendBytes:&encryptType length:1];
                [data appendData:[self getCompressedData:body]];

                [self sendDataToBackend:data type:@"operate" removeKey:eventType];
            }
            //[sharedDefaults removeObjectForKey:eventType];
        }
    }
    //[sharedDefaults removeObjectForKey:KB_Event_Array];
    //[sharedDefaults synchronize];
}

+(void)sendCrashDataToBackend:(NSArray *)stacktrace
{
    bool isCompressed = true;
    char encryptType = 0;

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:@"ios" forKey:@"os"];

    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appCurVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    [dictionary setValue:appCurVersion forKey:@"app"];

    NSString* phoneModel = [[UIDevice currentDevice] model];
    [dictionary setValue:phoneModel forKey:@"model"];

    NSLocale *locale = [NSLocale currentLocale];
    NSString *country = [locale localeIdentifier];
    [dictionary setValue:country forKey:@"na"];

    NSInteger tm = (NSInteger)time(NULL);
    [dictionary setObject:@(tm) forKey:@"ts"];

    NSString * oid = [[[NSString stringWithUTF8String:BACKEND_AGENT_KEY] stringByAppendingString:@"error"] stringByAppendingString:@"category"];
    
    [dictionary setValue:[self getMD5String:oid]forKey:@"oid"];
    [dictionary setValue:@"error" forKey:@"tp"];

    [dictionary setValue:[stacktrace componentsJoinedByString:@";"] forKey:@"st"];

    [dictionary setValue:@(SDK_VERSION) forKey:@"agent"];

    NSMutableData * body = [[NSMutableData alloc] init];

    [body appendData:[self getCommonBody]];
    [body appendData:[self getRealBody:dictionary]];

    NSMutableData * data = [[NSMutableData alloc] init];
    [data appendBytes:&isCompressed length:1];
    [data appendBytes:&encryptType length:1];
    [data appendData:[self getCompressedData:body]];

    [self sendDataToBackend:data type:@"error" removeKey:nil];
}

+(void)sendMetaDataToBackend
{
    NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    NSInteger tm_saved = [sharedDefaults integerForKey:@"meta_time"];
    NSInteger tm_now = (NSInteger)time(NULL);

    if (tm_saved != 0 && difftime(tm_now, tm_saved) < META_INTERVAL) {
        return;
    }

    bool isCompressed = false;
    char encryptType = 2;

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];

    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appCurVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    [dictionary setValue:appCurVersion forKey:@"app"];

    CGRect rect = [[UIScreen mainScreen] bounds];
    CGSize size = rect.size;
    CGFloat width = size.width;
    CGFloat height = size.height;
    CGFloat scale_screen = [UIScreen mainScreen].scale;
    int res_width = width * scale_screen;
    int res_height = height * scale_screen;
    NSString * resolution = [NSString stringWithFormat:@"%dx%d",res_width, res_height];
    [dictionary setValue:resolution forKey:@"res"];

    NSString* phoneVersion = [[UIDevice currentDevice] systemVersion];
    [dictionary setValue:phoneVersion forKey:@"os"];

    NSMutableDictionary *extra_dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:extra_dictionary forKey:@"tacc"];

    NSString* phoneModel = [[UIDevice currentDevice] model];
    if ([phoneModel isEqualToString:@"iPhone"]) {
        if (height == 480) {
            phoneModel = @"iPhone4";
        }
        else if (height == 568) {
            phoneModel = @"iPhone5";
        }
        else if (height == 667) {
            phoneModel = @"iPhone6";
        }
        else if (height == 736) {
            phoneModel = @"iPhone6s";
        }
    }
    [dictionary setValue:phoneModel forKey:@"model"];

    [dictionary setObject:@(tm_now) forKey:@"ts"];

    NSString * currentCountry = [self getCarrierName];
    if (currentCountry != nil) {
        [dictionary setObject:[self getCarrierName] forKey:@"op"];
    }
    else {
        [dictionary setObject:@"" forKey:@"op"];
    }
    [dictionary setObject:@"" forKey:@"net"];
    //[dictionary setObject:[self getNetworkType] forKey:@"net"];
    [dictionary setValue:@(SDK_VERSION) forKey:@"agent"];

    NSArray *languageArray = [NSLocale preferredLanguages];
    NSString *language = [languageArray objectAtIndex:0];    [dictionary setValue:language forKey:@"lang"];

    [dictionary setValue:@"ios" forKey:@"pf"];

    NSLocale *locale = [NSLocale currentLocale];
    NSString *country = [locale localeIdentifier];
    [dictionary setValue:country forKey:@"na"];

    [dictionary setValue:[NSString stringWithUTF8String:[[self getUUID] UTF8String]] forKey:@"duid"];

    [dictionary setValue:@"" forKey:@"pn"];
    [dictionary setValue:@"apple_store" forKey:@"ch"];
    [dictionary setValue:@"apple" forKey:@"mf"];

    NSMutableArray * in_full_array = [[NSMutableArray alloc] init];
    [dictionary setValue:in_full_array forKey:@"in_full"];
    [dictionary setValue:in_full_array forKey:@"in_builtin"];

    NSMutableData * data = [[NSMutableData alloc] init];
    [data appendBytes:&isCompressed length:1];
    [data appendBytes:&encryptType length:1];

    [data appendData:[self getCommonBody]];
    [data appendData:[self getEncryptedData:[self getRealBody:dictionary] encryptType:encryptType]];

    [self sendDataToBackend:data type:@"meta" removeKey:nil];
}

static CKSwitchKeyboardType global_switch_keyboard_type = CKSwitchKeyboardTypeDefault;

+(void)setSwitchKeyboardType:(CKSwitchKeyboardType)type
{
    global_switch_keyboard_type = type;
}

+(CKSwitchKeyboardType)getSwitchKeyboardType
{
    return global_switch_keyboard_type;
}

static BOOL global_emoji_used_in_app = NO;

+(void)setEmojiUsedInApp:(BOOL)used
{
    global_emoji_used_in_app = used;
}

+(BOOL)getEmojiUsedInApp
{
    return global_emoji_used_in_app;
}

static BOOL global_sticker_used_in_app = NO;

+(void)setStickerUsedInApp:(BOOL)used
{
    global_sticker_used_in_app = used;
}

+(BOOL)getStickerUsedInApp
{
    return global_sticker_used_in_app;
}

static BOOL global_gif_used_in_app = NO;

+(void)setGifUsedInApp:(BOOL)used
{
    global_gif_used_in_app = used;
}

+(BOOL)getGifUsedInApp
{
    return global_gif_used_in_app;
}

static BOOL global_meme_used_in_app = NO;

+(void)setMemeUsedInApp:(BOOL)used
{
    global_meme_used_in_app = used;
}

+(BOOL)getMemeUsedInApp
{
    return global_meme_used_in_app;
}

@end
