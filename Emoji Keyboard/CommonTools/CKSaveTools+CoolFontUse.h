//
//  CKSaveTools+CoolFontUse.h
//  Emoji Keyboard
//
//  Created by xinmeihutong on 15/11/3.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKSaveTools.h"
typedef  NS_ENUM(NSInteger,EvaluatStatus){//评价状态
    AlreadyEvaluated=1,                   //用户已经评价
    NoEvaluate
};

typedef NS_ENUM(NSInteger,LockStatus) {//锁状态
    LockStatusOpen=1,                  //锁打开
    LockStatusLock
};

typedef NS_ENUM(NSInteger,SelectedInputStatus) {//统计点击选择未输入
    SelectedEmojiInput=1,
    SelectedMemeInput,
    SelectedEmoji,
    SelectedMeme
};

@interface CKSaveTools (CoolFontUse)

//判断是否为新用户 YES 新用户
+(BOOL)determineNowUserType;
//以免用没有打开full access 保存appGroup中的用户类型到extension
+(void)saveAppGroupUserTypeToExtension;
//判断用户是否已经评价过 YES 评价过
+(BOOL)determineUserHaveEvaluated;
//锁是否打开..YES 锁打开
+(BOOL)lockStatus;
//由对应的视图打开应用
+(BOOL)switchToURL:(NSString *)url withResponder:(UIView *)showView;
//保存appGroup中的评价状态
+(void)saveAppGroupEvalteStatusToExtension;
//累加到 num 次数触发action事件
+(void)saveClickCountToNum:(NSInteger)num Key:(NSString *)key triggerAction:(SEL)action withHandle:(id)handle;
//统计Meme Emoji 选择未输入事件
+(void)saveEventForEmojiMeme;
@end
