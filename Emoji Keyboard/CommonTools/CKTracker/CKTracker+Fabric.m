//
//  CKTracker+Fabric.m
//  Emoji Keyboard
//
//  Created by croath on 12/29/15.
//  Copyright © 2015 Beijing Xinmei Hutong Technology Co.,Ltd.. All rights reserved.
//

#import "CKTracker+Fabric.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@implementation CKTracker (Fabric)

- (void)fabric_initialize{
  [Fabric with:@[[Crashlytics class]]];
}

@end
