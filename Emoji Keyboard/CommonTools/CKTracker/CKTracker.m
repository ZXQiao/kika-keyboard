//
//  CKTracker.m
//  Emoji Keyboard
//
//  Created by croath on 12/2/15.
//  Copyright © 2015 Beijing Xinmei Hutong Technology Co.,Ltd.. All rights reserved.
//

#import "CKTracker.h"
#import "CKTracker+GA.h"
#import "CKTracker+Fabric.h"

@implementation CKTracker

static CKTracker *__tracker;

+ (instancetype)sharedTraker{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __tracker = [[CKTracker alloc] init];
    });
    return __tracker;
}

#pragma mark - initialize

- (id)init{
    self = [super init];
    if (self) {
        [self initializeTrackers];
    }
    return self;
}

- (void)initializeTrackers{
    [self ga_initialize];
    [self fabric_initialize];
}

#pragma mark - View

- (void)enterView:(NSString*)viewName{
    [self ga_enterView:viewName];
}

- (void)leaveView:(NSString*)viewName{
    [self ga_leaveView:viewName];
}

#pragma mark - Action

- (void)logWithCategory:(NSString *)category action:(NSString *)action {
    [self logWithCategory:category action:action label:nil];
}

- (void)logWithCategory:(NSString*)category
                 action:(NSString*)action
                  label:(NSString*)label{
    [self ga_logWithCategory:category action:action label:label];
}

#pragma mark - Timings

- (void)trackTimeWithCategory:(NSString*)category
                         name:(NSString*)name
                        label:(NSString*)label
                         time:(NSTimeInterval)loadTime{
    [self ga_trackTimeWithCategory:category name:name label:label time:loadTime];
}

#pragma mark - Excption

- (void)catchException:(NSException*)exception{
    [self ga_catchException:exception];
}

#pragma mark - User

- (void)signInWithUserId:(NSString*)userId{
    [self ga_signInWithUserId:userId];
}

- (void)signInWithAnonymousUser{
    [self ga_signInWithAnonymousUser];
}

- (void)signOut{
    [self ga_signOut];
}

@end
