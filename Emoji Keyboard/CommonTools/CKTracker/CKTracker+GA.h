//
//  CKTracker+GA.h
//  Emoji Keyboard
//
//  Created by croath on 12/2/15.
//  Copyright © 2015 Beijing Xinmei Hutong Technology Co.,Ltd.. All rights reserved.
//

#import "CKTracker.h"

@interface CKTracker (GA)

- (void)ga_initialize;

#pragma mark - View

- (void)ga_enterView:(NSString*)viewName;
- (void)ga_leaveView:(NSString*)viewName;

#pragma mark - Action

- (void)ga_logWithCategory:(NSString*)category
                    action:(NSString*)action
                     label:(NSString*)label;

#pragma mark - Timings

- (void)ga_trackTimeWithCategory:(NSString*)category
                            name:(NSString*)name
                           label:(NSString*)label
                            time:(NSTimeInterval)loadTime;

#pragma mark - Excption

- (void)ga_catchException:(NSException*)exception;

#pragma mark - User

- (void)ga_signInWithUserId:(NSString*)userId;
- (void)ga_signInWithAnonymousUser;
- (void)ga_signOut;

@end
