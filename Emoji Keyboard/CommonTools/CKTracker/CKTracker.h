//
//  CKTracker.h
//  Emoji Keyboard
//
//  Created by croath on 12/2/15.
//  Copyright © 2015 Beijing Xinmei Hutong Technology Co.,Ltd.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Google/Analytics.h>

@interface CKTracker : NSObject

@property (nonatomic, strong) id<GAITracker> googleTracker;

- (void)enterView:(NSString*)viewName;
- (void)leaveView:(NSString*)viewName;

- (void)logWithCategory:(NSString*)category
                 action:(NSString*)action;

- (void)logWithCategory:(NSString*)category
                 action:(NSString*)action
                  label:(NSString*)label;

- (void)trackTimeWithCategory:(NSString*)category
                         name:(NSString*)name
                        label:(NSString*)label
                         time:(NSTimeInterval)loadTime;

- (void)catchException:(NSException*)exception;

- (void)signInWithUserId:(NSString*)userId;
- (void)signInWithAnonymousUser;
- (void)signOut;

+ (instancetype)sharedTraker;

@end
