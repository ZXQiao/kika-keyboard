//
//  CKTracker+Fabric.h
//  Emoji Keyboard
//
//  Created by croath on 12/29/15.
//  Copyright © 2015 Beijing Xinmei Hutong Technology Co.,Ltd.. All rights reserved.
//

#import "CKTracker.h"

@interface CKTracker (Fabric)

- (void)fabric_initialize;

@end
