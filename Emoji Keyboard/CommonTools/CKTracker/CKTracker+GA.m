//
//  CKTracker+GA.m
//  Emoji Keyboard
//
//  Created by croath on 12/2/15.
//  Copyright © 2015 Beijing Xinmei Hutong Technology Co.,Ltd.. All rights reserved.
//

#import "CKTracker+GA.h"
#import <Google/Analytics.h>

static NSString *const kGaPropertyId = @"UA-70917853-5";

static NSString *const KGaUidKey = @"&uid";
static NSString *const KGaUidAnonymousValue = @"Anonymous";

static int const kGaDispatchPeriod = 30;

@implementation CKTracker (GA)

- (void)ga_initialize{
    [GAI sharedInstance].trackUncaughtExceptions = NO;
    [GAI sharedInstance].dispatchInterval = kGaDispatchPeriod;
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelError];
    [[GAI sharedInstance] trackerWithTrackingId:kGaPropertyId];
    [[GAI sharedInstance] setOptOut:NO];
    self.googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kGaPropertyId];
}

#pragma mark - View

- (void)ga_enterView:(NSString*)viewName{
    [self.googleTracker set:kGAIScreenName value:viewName];
    [self.googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)ga_leaveView:(NSString*)viewName{
    [self.googleTracker set:kGAIScreenName value:nil];
}

#pragma mark - Action

- (void)ga_logWithCategory:(NSString*)category
                    action:(NSString*)action
                     label:(NSString*)label{
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:category
                                                                           action:action
                                                                            label:label
                                                                            value:nil];
//    if ([CKCommonTools isAppExtension]) {
//        [builder set:[CKCommonTools getHostAppName] forKey:[GAIFields customDimensionForIndex:1]];
//    }
    [self.googleTracker send:[builder build]];
}

#pragma mark - Timings

- (void)ga_trackTimeWithCategory:(NSString*)category
                            name:(NSString*)name
                           label:(NSString*)label
                            time:(NSTimeInterval)loadTime{
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createTimingWithCategory:category
                                                                          interval:@((NSUInteger)(loadTime*1000))
                                                                              name:name
                                                                             label:label];
//    if ([CKCommonTools isAppExtension]) {
//        [builder set:[CKCommonTools getHostAppName] forKey:[GAIFields customDimensionForIndex:1]];
//    }
    [self.googleTracker send:[builder build]];
}

#pragma mark - Excption

- (void)ga_catchException:(NSException*)exception{
    NSString *desc = [NSString stringWithFormat:@"Exception: %@, %@",
                      [exception name], [exception reason]];
    [self.googleTracker send:[[GAIDictionaryBuilder
                               createExceptionWithDescription:desc
                               withFatal:@NO] build]];
}

#pragma mark - User

- (void)ga_signInWithUserId:(NSString*)userId{
    [self.googleTracker set:KGaUidKey value:userId];
}

- (void)ga_signInWithAnonymousUser{
    [self ga_signInWithUserId:KGaUidAnonymousValue];
}

- (void)ga_signOut{
    [self ga_signInWithAnonymousUser];
}


@end
