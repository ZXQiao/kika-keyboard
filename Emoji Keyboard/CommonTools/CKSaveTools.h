//
//  CKSaveTools.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/17.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CKMainThemeModel.h"
/**
 *  用来存储数据,存储文件到路径,存储数据到AppGroup,存储到NSUserDefaults等
 */

/**
 *  存储数据的区域,分为三块:
 *  1.主App的沙盒
 *  2.Extension的沙盒
 *  3.AppGroup
 *  
 *  对于1和3,在代码上是没有什么差别的,因为谁调用这个SaveTools就会存到谁的沙盒,但是为了代码的可读性
 *  在这里将SaveRegion设置为三个枚举
 */
typedef NS_ENUM(int, CKSaveRegion){
    CKSaveRegionContainingApp = 0,
    CKSaveRegionExtensionKeyboard,
    CKSaveRegionAppGroup,
};
@interface CKSaveTools : NSObject

// 保存字符串
+(void)saveString:(NSString *)valueString forKey:(NSString *)key inRegion:(CKSaveRegion)region;
// 保存Integer值
+(void)saveInteger:(NSInteger )valueinteger forKey:(NSString *)key inRegion:(CKSaveRegion)region;
// 保存数组
+(void)saveArray:(NSArray *)valueArray forKey:(NSString *)key inRegion:(CKSaveRegion)region;

+(void)saveClickWithEventType:(NSString *)eventType key:(NSString *)key;

+ (BOOL)saveMainTheme:(CKMainThemeModel *)mainTheme toRegion:(CKSaveRegion)region;
+(CKMainThemeModel *)takeOutMainThemefromRegion:(CKSaveRegion)region;

@end
