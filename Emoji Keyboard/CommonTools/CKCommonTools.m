//
//  CKCommonTools.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/21.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKCommonTools.h"

@implementation CKCommonTools

static float global_systemVersion = 0;

+ (BOOL)isOpenAccessGranted
{
    return ([UIPasteboard generalPasteboard] != nil);
}

+(BOOL)screenDirectionIsLand
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    return (screenSize.width > screenSize.height);
}

+(void)showInfoByFittingParentView:(UIView *)parentView msg:(NSString *)msg
{
    UIView * coverView = [[UIView alloc] initWithFrame:parentView.bounds];
    [parentView addSubview:coverView];
    coverView.backgroundColor = [UIColor lightGrayColor];
    coverView.alpha = .2;
    
    UIImageView * view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, parentView.superview.frame.size.width, parentView.superview.frame.size.height)];
    view.image = [UIImage imageWithBundlePath:@"toast@2x" imageType:nil];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = .9;
    [parentView addSubview:view];
    
    UILabel * label = [[UILabel alloc] initWithFrame:view.bounds];
    label.text = msg;//@"Now paste it \nin a message!";
    label.center = view.center;
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"helvetica" size:30];
    label.textColor = [UIColor whiteColor];
    label.numberOfLines = 0;
    
    [parentView.superview addSubview:label];
    [parentView.superview bringSubviewToFront:label];
    
    [CKCommonTools asyncDoSthWithBlock:^{
        [UIView animateWithDuration:.3 animations:^{
        } completion:^(BOOL finished) {
            [view removeFromSuperview];
            [label removeFromSuperview];
            [coverView removeFromSuperview];
        }];
    }];
}

+ (void)asyncDoSthWithBlock:(void (^)(void))block
{
    dispatch_queue_t q = dispatch_queue_create("myQueue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(q, ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1* NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            block();
        });
    });
}

+(NSURL*)GetAPP_GROUPS_Library_ThemesURL
{
    NSURL *containerURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS];
    containerURL = [containerURL URLByAppendingPathComponent:@"Library/Themes"];
    return containerURL;
}

+(float)systemVersion
{
    if (global_systemVersion < FLOAT_EPSINON) {
        global_systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    }
    return global_systemVersion;
}

+ (void)showFullAccessViewWithView:(UIView *)view frame:(CGRect)frame
{
    CKAllowFullAccessView * allowFullAccessView = [[CKAllowFullAccessView alloc] initWithFrame:frame];
    CGFloat y = view.frame.origin.y;
    CGFloat h = view.superview.frame.size.height - y ;
    UIImageView * backImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, y, view.frame.size.width, h)];
    backImageView.image = [UIImage imageWithBundlePath:@"toast@2x" imageType:nil];
    [view.superview addSubview:backImageView];
    backImageView.backgroundColor = [UIColor lightGrayColor];
    backImageView.alpha = .9;
    allowFullAccessView.backView = backImageView;
    [view.superview addSubview:allowFullAccessView];
    allowFullAccessView.center = CGPointMake(backImageView.center.x, backImageView.center.y - 3);
    view.userInteractionEnabled = NO;
    allowFullAccessView.frameView = view;
    backImageView.userInteractionEnabled = NO;
    allowFullAccessView.tag = 520;
    for (UIView * subview in view.superview.subviews) {
        if (subview.tag != 520) {
            subview.userInteractionEnabled = NO;
        }
    }
}
@end
