//
//  CKSaveTools+CoolFontUse.m
//  Emoji Keyboard
//
//  Created by xinmeihutong on 15/11/3.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKSaveTools+CoolFontUse.h"

@implementation CKSaveTools (CoolFontUse)
+(BOOL)determineNowUserType{
    if(![self useEngelish]){//非英语，没有coolfont
        return NO;
    }
    BOOL userType = NO;
    NSString *extensionType=[self getExtensionValueForKey:LOCAL_USER_TYPE];
    NSString *appgroupType = [self getAppGroupValueForKey:USER_TYPE];
    if(appgroupType != nil ){//新安装主APP启动过
        if( extensionType != nil){//extension本地状态
            if([extensionType isEqualToString:NEW_USER ]){
                userType=YES;
            }else if ([extensionType isEqualToString:OLD_USER ]){
                userType=NO;
            }
        }
    }else{//新安装主app 未启动过
        NSString *result = [self getAppGroupValueForKey:@"clearFlag"];
        if( extensionType != nil){
            if([extensionType isEqualToString:NEW_USER ]){
                userType=YES;
            }else if ([extensionType isEqualToString:OLD_USER ]){
                userType=NO;
            }
        }else{
            if(!result){
                userType=YES;
            }else{
                userType=NO;
            }
        }
    }
    return userType;
}

+(void)saveAppGroupUserTypeToExtension{
    NSString * extensionUserType = [self getExtensionValueForKey:LOCAL_USER_TYPE];
    NSString * appGroupUserType = [self getAppGroupValueForKey:USER_TYPE];
    if( !extensionUserType ){
        if([appGroupUserType isEqualToString:NEW_USER]){
            [self saveString:NEW_USER forKey:LOCAL_USER_TYPE inRegion:CKSaveRegionExtensionKeyboard];
        }else if ([appGroupUserType isEqualToString:OLD_USER]){
            [self saveString:OLD_USER forKey:LOCAL_USER_TYPE inRegion:CKSaveRegionExtensionKeyboard];
        }
    }
}
+(void)saveAppGroupEvalteStatusToExtension{
    NSString *extensionEvalte=[self getExtensionValueForKey:EVALUATE_STATUS];
    NSString *appGroupStatus=[self getAppGroupValueForKey:EVALUATE_STATUS_GROUP];
    if( !extensionEvalte && appGroupStatus){
        [CKSaveTools saveInteger:AlreadyEvaluated forKey:EVALUATE_STATUS inRegion:CKSaveRegionExtensionKeyboard];
    }
}
+(BOOL)determineUserHaveEvaluated{
    return [[NSUserDefaults standardUserDefaults] integerForKey:EVALUATE_STATUS]==AlreadyEvaluated ? YES : NO;
}

+(BOOL)lockStatus{
    return [[NSUserDefaults standardUserDefaults]integerForKey:LOCK_STATUS] == LockStatusOpen ? YES : NO;
}
+(void)saveClickCountToNum:(NSInteger)num Key:(NSString *)key triggerAction:(SEL)action withHandle:(id)handle
{
    NSUserDefaults *shareDefault=[NSUserDefaults standardUserDefaults];
    NSInteger count=[shareDefault integerForKey:key];
    if(count < num){
        count ++;
        [shareDefault setInteger:count forKey:key];
    }else if(count == num){
        if([handle respondsToSelector:action]){
            [handle performSelector:action];
        }
        count ++;
        [shareDefault setInteger:count forKey:key];
    }

}

+(void)saveEventForEmojiMeme{
 
    NSUserDefaults *shareDefault = [NSUserDefaults standardUserDefaults];
    NSInteger inputStatus = [shareDefault integerForKey:SELECT_INPUT];
    if(inputStatus == SelectedEmoji){
        //选择了Emoji 没有输入
        [CKSaveTools saveClickWithEventType:KB_Selected_Emoji_Not_Input key:@"KB_Selected_Emoji_Not_Input"];
        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                           action:KB_Selected_Emoji_Not_Input];
        
    }else if(inputStatus == SelectedMeme){
        //选择了Meme 没有输入
        [CKSaveTools saveClickWithEventType:KB_Selected_Meme_Not_Input key:@"KB_Selected_Meme_Not_Input"];
        [[CKTracker sharedTraker] logWithCategory:CK_KEYBOARD_CATEGORY
                                           action:KB_Selected_Meme_Not_Input];
    }
    
    [shareDefault removeObjectForKey:SELECT_INPUT];
}
+(BOOL)switchToURL:(NSString *)url withResponder:(UIView *)showView{
    UIResponder * responder = showView;
    while ((responder = [responder nextResponder]) != nil) {
        if ([responder respondsToSelector:@selector(openURL:)] == YES) {
            [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:url]];
        }
    }
    return TRUE;
}
+(BOOL)useEngelish{
    NSString * language = [self getAppGroupValueForKey:@"Language"];
    if(language.length == 0 || language == nil ||[language  isEqualToString:@"English"]){
        return YES;
    }
    return NO;
}
+(NSString *)getExtensionValueForKey:(NSString *)key{
    NSUserDefaults * extensionSharedDefaults = [NSUserDefaults standardUserDefaults];
    return [extensionSharedDefaults objectForKey:key];
}
+(NSString *)getAppGroupValueForKey:(NSString *)key{
    NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    return  [sharedDefaults objectForKey:key];
}
@end
