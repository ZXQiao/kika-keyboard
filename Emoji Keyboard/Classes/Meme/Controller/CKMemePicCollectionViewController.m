//
//  CKMemeLocalViewController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/3.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMemePicCollectionViewController.h"
#import "CKMemeCollectionCell.h"
#import "Colours.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"
#import "CKCustomMemeViewController.h"
#import "DBCameraGridView.h"
#import "CKMemeCameraView.h"

@interface CKMemePicCollectionViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,CKMemeCollectionCellDelegate,DBCameraViewControllerDelegate,CKMemeCameraViewDelegate, DBCameraViewControllerDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton * cameraButton;
@property (nonatomic, strong) CKMemeCameraView * cameraView;
@property (nonatomic, strong) UIButton * cameraCoverButton;

@end


static NSString * const reuseIdentifier = @"CKMemePicCollectionViewController";
#define PNG_COUNT 20
#define JPEG_COUNT 10

@implementation CKMemePicCollectionViewController
- (CKMemeCameraView *)cameraView
{
    if (_cameraView == nil) {
        _cameraView = [[CKMemeCameraView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height , self.view.frame.size.width, self.view.frame.size.height * .2)];
        [self.view addSubview:_cameraView];
        _cameraView.delegate = self;
    }
    return _cameraView;
}

- (void)setUpCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    CGSize size = CGSizeMake(0, 0);
    [flowLayout setItemSize:size];//设置cell的尺
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);//设置其边界
    CGRect frame = self.view.frame;
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[CKMemeCollectionCell class] forCellWithReuseIdentifier:reuseIdentifier];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTitleView];
    [self setUpCollectionView];
}

- (void)setUpTitleView
{
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Library";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;
    
    
    // 给导航条右边添加一个按钮
    UIButton * cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cameraButton.frame = CGRectMake(0, 0, 44, 44);
    cameraButton.layer.masksToBounds = YES;
    cameraButton.layer.cornerRadius = 15;
    UIImage * rightImage = [UIImage imageNamed:@"meme_camera"];
    cameraButton.adjustsImageWhenHighlighted = NO;
    cameraButton.contentMode = UIViewContentModeRight;
    [cameraButton setImage:rightImage forState:UIControlStateNormal];
    UIBarButtonItem *itemRight = [[UIBarButtonItem alloc] initWithCustomView:cameraButton];
    [cameraButton addTarget:self action:@selector(cameraButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = itemRight;
    self.cameraButton = cameraButton;
    cameraButton.tag = 0;
}

- (void)cameraViewButtonClicked:(UIButton *)button
{
    switch (button.tag) {
        case 0:
        {
            [self openCamera];
            [self dismissCameraActionSheet];
        }
            break;
        case 1:
            [self openAlbum];
            break;
        case 2:
            [self dismissCameraActionSheet];
            break;
        default:
            break;
    }
}

- (void)dismissCameraActionSheet
{
    [self.cameraCoverButton removeFromSuperview];
    [UIView animateWithDuration:.3 animations:^{
        self.cameraView.frame = CGRectMake(0,self.view.frame.size.height , self.view.frame.size.width, self.cameraView.frame.size.height);
    }];

}

- (void)showCameraActionSheet
{
    UIButton * cameraCoverButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - self.cameraView.frame.size.height)];
    [[[UIApplication sharedApplication] keyWindow] addSubview:cameraCoverButton];
    cameraCoverButton.backgroundColor = [UIColor clearColor];
    [cameraCoverButton addTarget:self action:@selector(dismissCameraActionSheet) forControlEvents:UIControlEventTouchUpInside];
    self.cameraCoverButton = cameraCoverButton;
    [UIView animateWithDuration:.3 animations:^{
        self.cameraView.frame = CGRectMake(0, self.view.frame.size.height - self.cameraView.frame.size.height, self.view.frame.size.width, self.cameraView.frame.size.height);
    }];
}
- (void)cameraButtonClicked:(UIButton *)button
{
    [self showCameraActionSheet];
}



- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    CKCustomMemeViewController *detail = [[CKCustomMemeViewController alloc] init];
    [detail setDetailImage:image];
    [self.navigationController pushViewController:detail animated:NO];
    [cameraViewController restoreFullScreenMode];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (void)openAlbum
{
    
        DBCameraLibraryViewController *vc = [[DBCameraLibraryViewController alloc] init];
        [vc setDelegate:self];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [nav setNavigationBarHidden:YES];
        [self presentViewController:nav animated:YES completion:nil];
}

- (void) openCamera
{
    DBCameraContainerViewController *cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [cameraContainer setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraContainer];
    [nav setNavigationBarHidden:YES];
    self.title = @"Library";
    [self presentViewController:nav animated:YES completion:^{
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

#pragma mark - DBCameraViewControllerDelegate

- (void) dismissCamera:(id)cameraViewController{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];

}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return PNG_COUNT + JPEG_COUNT;
}


- (CKMemeCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CKMemeCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CKMemeCollectionCell alloc]init];
    }
    cell.cellType = CKCellTypeLocal;
    NSString * imageName ;
    UIImage * image;
    if (indexPath.item < PNG_COUNT) {
        imageName   = [NSString stringWithFormat:@"meme_%02d",(int)indexPath.item + 1];
        image = [UIImage imageNamed:imageName];
    }
    else
    { // negative_01
        imageName = [NSString stringWithFormat:@"negative_%02d.jpg",(int)indexPath.item + 1 - PNG_COUNT];
        image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:nil]];
    }
    cell.themeView.image = image;
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat w = (self.view.frame.size.width - 2) / 2;
    return CGSizeMake(w, w);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,2,0); // 上下左右的偏移量
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * imageName ;
    UIImage * image;
    NSString * indexString = [NSString stringWithFormat:@"%d",(int)indexPath.item + 1];
//    NSDictionary *dict = @{Main_CustomMemeNegativeClicked : indexString};

    [CKSaveTools saveClickWithEventType:Main_CustomMemeNegativeClicked key:indexString];
    
    if (indexPath.item < PNG_COUNT) {
        imageName   = [NSString stringWithFormat:@"meme_%02d",(int)indexPath.item + 1];
        image = [UIImage imageNamed:imageName];
    }
    else
    { // negative_01
        imageName = [NSString stringWithFormat:@"negative_%02d.jpg",(int)indexPath.item + 1 - PNG_COUNT];
        image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:nil]];
    }
    CKCustomMemeViewController *detail = [[CKCustomMemeViewController alloc] init];
    self.title = @"Library";
    [detail setDetailImage:image];
    
    [self.navigationController pushViewController:detail animated:YES];
}
@end
