//
//  CKMemeLocalViewController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/3.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMemeLocalViewController.h"
#import "CKMemeCollectionCell.h"
#import "Colours.h"
#import "CKMemeViewController.h"
#import "CKMemePicCollectionViewController.h"
#import "CKMemePreviewView.h"
#import "MWPhotoBrowser.h"
#import "CKLocalMemeModel.h"
#import "CKMemeDataManager.h"
#import "CKShareTools.h"
#import "MBProgressHUD.h"

#import "UIImageView+WebCache.h"


@interface CKMemeLocalViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,CKMemeCollectionCellDelegate,MWPhotoBrowserDelegate, CKMemePreviewViewDelegate>
{
    NSMutableArray *_selections;
}
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton * addButton;
@property (nonatomic, strong) CKMemeCollectionCell * currentCell;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, assign) BOOL deleteShouldShow;
@property (nonatomic, strong) UIButton * deleteButton;
@property (nonatomic, assign) int selectedMemeCount;
@property (nonatomic, strong) UIButton * bottomDeleteButton;

@property (nonatomic, strong) NSMutableArray * defaultMemeModelArray;
@property (nonatomic, strong) NSMutableArray * downloadedOnlineMemeModelArray;

@end


static NSString * const reuseIdentifier = @"CKMemeLocalCollectionCell";



@implementation CKMemeLocalViewController

- (NSMutableArray *)defaultMemeModelArray
{
    if (_defaultMemeModelArray == nil) {
        _defaultMemeModelArray = [NSMutableArray array];
        for (int i = 1; i < 21; i ++) {
            CKLocalMemeModel * memeModel = [[CKLocalMemeModel alloc] init];
            memeModel.selected = NO;
            memeModel.online = NO;
            memeModel.localImageName = [NSString stringWithFormat:@"defaultP_%02d.jpg",i];
            [_defaultMemeModelArray addObject:memeModel];
        }
    }
    return _defaultMemeModelArray;
}

- (NSMutableArray *)downloadedOnlineMemeModelArray
{
    if (_downloadedOnlineMemeModelArray == nil) {
        NSArray * temp = [NSArray arrayWithContentsOfURL:MEME_DOWLOADED_PLIST_URL];
        _downloadedOnlineMemeModelArray = [NSMutableArray array];
        for (NSString * imageName in temp) {
            CKLocalMemeModel * memeModel = [[CKLocalMemeModel alloc] init];
            memeModel.selected = NO;
            memeModel.online = YES;
            memeModel.localImageName = imageName;
            [_downloadedOnlineMemeModelArray addObject:memeModel];
        }
    }
    return _downloadedOnlineMemeModelArray;
}
- (UIButton *)bottomDeleteButton
{
    if (_bottomDeleteButton == nil) {
        _bottomDeleteButton = [[UIButton alloc] init];
        UITabBar * tabBar = self.tabBarController.tabBar;
        _bottomDeleteButton.frame = tabBar.frame;
        _bottomDeleteButton.backgroundColor = [UIColor whiteColor];
        [_bottomDeleteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_bottomDeleteButton addTarget:self action:@selector(deleteLocalMeme:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bottomDeleteButton;
}

- (void)updateLocalMemeArray:(NSNotification *)notification
{
    if (!notification.object) return;
    NSString * memeImageName = (NSString *)notification.object;
    CKLocalMemeModel * memeModel = [[CKLocalMemeModel alloc] init];
    memeModel.localImageName = memeImageName;
    memeModel.online = YES;
    [self.downloadedOnlineMemeModelArray insertObject:memeModel atIndex:0];
    [self.collectionView reloadData];
    
    // 将名字写入Plist,并保存,并通知刷新
    NSMutableArray * memePlistArray = [NSMutableArray arrayWithContentsOfURL:MEME_DOWLOADED_PLIST_URL];
    memePlistArray = memePlistArray ? memePlistArray :[NSMutableArray array];
    [memePlistArray insertObject:memeImageName atIndex:0];
    [memePlistArray writeToURL:MEME_DOWLOADED_PLIST_URL atomically:YES];
}



- (void)setUpCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    CGSize size = CGSizeMake(0, 0);
    [flowLayout setItemSize:size];//设置cell的尺
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);//设置其边界
    CGRect frame = self.view.frame;
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[CKMemeCollectionCell class] forCellWithReuseIdentifier:reuseIdentifier];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
}

- (void)deleteCustomMeme:(UIButton *)button
{
    switch (button.tag) {
        case 0 :
        {
            button.tag = 1;
            [self.collectionView reloadData];
        }
            break;
        case 1 :
        {
            button.tag = 0;
        }
            break;
        default:
            break;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self setUpCollectionView];
    [self setUpAddButton];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteShouldShowOnCell:) name:@"CKLocalMemeShouldDelete" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocalMemeArray:) name:@"CKTabBarControllerShouldHiddenSelfTabBar" object:nil];

    
    [self.view addSubview:self.bottomDeleteButton];
}

- (void)deleteShouldShowOnCell:(NSNotification *)notification{
    UIButton * button = notification.object;
    self.deleteShouldShow = !button.tag;
    self.addButton.hidden = !button.tag;
    self.deleteButton = button;
    if (button.tag == 1) { // 说明用户取消
        self.selectedMemeCount = 0;
        [self updateBottomDeleteButton:YES];
        for (CKLocalMemeModel * meme in self.downloadedOnlineMemeModelArray) {
            if (meme.selected) {
                meme.selected = NO;
            }
        }
    }
    
    [self.collectionView reloadData];
}

- (void)deleteLocalMeme:(UIButton *)button
{
    NSMutableArray * deleteIndexArray  = [NSMutableArray array];
    NSFileManager * fileMgr = [NSFileManager defaultManager];
    NSError * error;
    NSMutableArray * modelArray = [NSMutableArray array];
    NSMutableArray * imageNameArray = [NSMutableArray array];
    
    for (int i = 0; i < self.downloadedOnlineMemeModelArray.count; i++) {
        CKLocalMemeModel * meme = self.downloadedOnlineMemeModelArray[i];
        NSString * imageName = meme.localImageName;
        [deleteIndexArray addObject:@(i)];
        if (meme.selected) { // 如果选中的话就删除图片
            NSString * imagePath = [MEME_SAVE_URL.path stringByAppendingPathComponent:meme.localImageName];
            if ([fileMgr fileExistsAtPath:imagePath]) {
                // 如果图片存在则删除
                [fileMgr removeItemAtPath:imagePath error:&error];
            }
        }
        else{ // 没有选中的时候
            [modelArray addObject:meme];
            [imageNameArray addObject:imageName];
        }
    }
    
        self.deleteShouldShow = NO;
        [self.deleteButton setTitle:@"Select" forState:UIControlStateNormal];
    [self.downloadedOnlineMemeModelArray removeAllObjects];
    [self.downloadedOnlineMemeModelArray addObjectsFromArray:modelArray];
    [self updateBottomDeleteButton:YES];
    [self.collectionView reloadData];
    BOOL writeSuccess = [imageNameArray writeToURL:MEME_DOWLOADED_PLIST_URL atomically:YES];
    // reload在线页面
    if (writeSuccess) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CKOnlineMemeShouldReloadDataNotification" object:imageNameArray];
    }else CKLog(@"Write DownloadMemePlist Failed-----I'm in CKMemeLocalViewController");
}
#define ADD_BUTTONW 55
- (void)setUpAddButton
{
    UIButton * addButton = [[UIButton alloc] init];
    [self.view addSubview:addButton];
    addButton.frame = CGRectMake(0, self.view.frame.size.height - self.tabBarController.tabBar.frame.size.height - 20 - ADD_BUTTONW , ADD_BUTTONW, ADD_BUTTONW);
    self.addButton = addButton;
    [addButton setBackgroundImage:[UIImage imageWithBundlePath:@"meme_custom@2x" imageType:nil] forState:UIControlStateNormal];
    addButton.center = CGPointMake(self.view.center.x, addButton.center.y);
    [addButton addTarget:self action:@selector(addNewMeme) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark ---- 创建Appgroup中Meme的文件夹
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // 创建AppGroup下的Meme路径
        NSFileManager * myFileMgr = [NSFileManager defaultManager];
        if (![myFileMgr fileExistsAtPath:MEME_SAVE_URL.path]) { // 如果不存在的话创建这个文件夹
            [myFileMgr createDirectoryAtURL:MEME_SAVE_URL withIntermediateDirectories:NO attributes:nil error:NULL];
        }
    }
    return self;
}


- (void)addNewMeme
{
    [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:Main_CustomMemeAddButtonClicked];// 自定义Meme加号按钮的点击次数
    [CKSaveTools saveClickWithEventType:Main_CustomMemeAddButtonClicked key:Main_CustomMemeAddButtonClicked];
    CKMemePicCollectionViewController * pictureVC = [[CKMemePicCollectionViewController alloc] init];
    [self.navigationController pushViewController:pictureVC animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.addButton.hidden = NO;
    UIApplication * app = [UIApplication sharedApplication];
    if (app.statusBarHidden) {
        app.statusBarHidden = NO;
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.deleteShouldShow) {
        self.deleteShouldShow = NO;
        [self.deleteButton setTitle:@"Select" forState:UIControlStateNormal];
        [self.collectionView reloadData];
    }
    [self updateBottomDeleteButton:YES];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    self.addButton.hidden = YES;
    [CKMemeDataManager saveMemeToShare];
    
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.defaultMemeModelArray.count + self.downloadedOnlineMemeModelArray.count;
}


- (CKMemeCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CKMemeCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CKMemeCollectionCell alloc]init];
    }
    CKLocalMemeModel * localMemeModel;
    
    if (indexPath.item < self.defaultMemeModelArray.count) {
        cell.deleteButton.hidden = YES;
        localMemeModel = self.defaultMemeModelArray[indexPath.item];
        localMemeModel.localIndex = (int)indexPath.item;
    }
    else{
        
        cell.deleteButton.hidden = !self.deleteShouldShow;
        localMemeModel = self.downloadedOnlineMemeModelArray[indexPath.item  - self.defaultMemeModelArray.count];
        localMemeModel.localIndex = (int)indexPath.item;
    }
    cell.localMemeModel = localMemeModel;
    cell.delegate = self;
    return cell;
}

- (void)localDeleteButtonClicked:(CKLocalMemeModel *)localMemeModel
{
    int localIndex = localMemeModel.localIndex;
    CKLocalMemeModel * meme = self.downloadedOnlineMemeModelArray[localIndex - LOCAL_MEME_COUNT];
    if (meme.selected == YES) { // 如果meme是选中态,说明用户取消了
        self.selectedMemeCount --;
    }
    else{
        self.selectedMemeCount ++;
        
        if (self.selectedMemeCount == 1) {
            [self updateBottomDeleteButton:NO];
        }
    }
    
    if (!self.selectedMemeCount) { // 如果为0的话,让bottomDelete隐藏
        [self updateBottomDeleteButton:YES];
    }
    else
    {
        [self.bottomDeleteButton setTitle:[NSString stringWithFormat:@"Delete(%d)",self.selectedMemeCount] forState:UIControlStateNormal];
    }
 
    meme.selected = !meme.selected;
    [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:localIndex inSection:0]]];
}

- (void)updateBottomDeleteButton:(BOOL)hidden
{
    if (hidden) {
        [self.bottomDeleteButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [UIView animateWithDuration:.3 animations:^{
            UITabBar * tabBar = self.tabBarController.tabBar;
            self.bottomDeleteButton.frame = tabBar.frame;
        }];
        self.selectedMemeCount =0;
    }
    else
    {
        [UIView animateWithDuration:.3 animations:^{
            UITabBar * tabBar = self.tabBarController.tabBar;
            CGFloat height = tabBar.frame.size.height;
            [UIView animateWithDuration:.3 animations:^{
                self.bottomDeleteButton.frame = CGRectMake(0, tabBar.frame.origin.y - height, tabBar.frame.size.width, height);
            }];
        }];
        

    }
}
#pragma mark <CKMemeCollectionCellDelegate>
- (void)showInfo:(NSString *)status deley:(float)time
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = status;
    hud.margin = 10.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:time];
}



#pragma mark <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:@"All MemeClicked"];
    [CKSaveTools saveClickWithEventType:@"Main_All_MemeClicked" key:@"All MemeClicked"];
    if (indexPath.item < self.defaultMemeModelArray.count) {
        NSString * indexString = [NSString stringWithFormat:@"%d",(int)indexPath.item + 1];
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:Main_LocalMemeClicked label:indexString];
        [CKSaveTools saveClickWithEventType:Main_LocalMemeClicked key:indexString];
    }
    else
    {
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:Main_OnlineMemeClicked];
        [CKSaveTools saveClickWithEventType:Main_OnlineMemeClicked key:Main_OnlineMemeClicked];
    }
    
    if (self.deleteShouldShow) {
        if (indexPath.item >= self.defaultMemeModelArray.count) {
            
            CKLocalMemeModel * localMemeModel = self.downloadedOnlineMemeModelArray[indexPath.item - self.defaultMemeModelArray.count];
            [self localDeleteButtonClicked:localMemeModel];
        }
        else{// Default memes can't be deleted
            [self showInfo:@"Default memes can't be deleted" deley:1.5];
        }
        return;
    }
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < self.defaultMemeModelArray.count; i ++) {
        NSString * imageName = [NSString stringWithFormat:@"defaultP_%02d.jpg",i + 1];
        UIImage * localImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:imageName ofType:nil]];
        [photos addObject:[MWPhoto photoWithImage:localImage]];
    }
    for (CKLocalMemeModel * memeModel in self.downloadedOnlineMemeModelArray) {
        UIImage * memeImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",MEME_SAVE_URL.path,memeModel.localImageName]];
        [photos addObject:[MWPhoto photoWithImage:memeImage]];
    }
    self.photos = photos;

    
    // Create browser
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = NO;
//    browser.controllerType = CKControllerTypeLocal;
    browser.displayNavArrows = NO;
    
    browser.displaySelectionButtons = NO;
    browser.alwaysShowControls = NO;
    browser.zoomPhotosToFill = YES;
    
    browser.enableGrid = NO; // 表格
    browser.startOnGrid = NO; // 开始就是表格显示
    browser.enableSwipeToDismiss = YES;
    [browser setCurrentPhotoIndex:indexPath.item];
    
    _selections = [NSMutableArray new];
    for (int i = 0; i < photos.count; i++) {
        [_selections addObject:[NSNumber numberWithBool:NO]];
    }
    [self.navigationController pushViewController:browser animated:YES];
    self.navigationController.navigationBar.hidden = YES;

}

- (void)shareMemeWithItem:(int)item
{
    MWPhoto * currentPhoto = self.photos[item];
    UIImage * image = currentPhoto.underlyingImage;
    
    //创建弹出菜单容器
//    id container = [ShareSDK container];
//    if (iPad) {
//        [container setIPadContainerWithView:self.view arrowDirect:UIPopoverArrowDirectionUp];
//    }
//    else{
//        container = nil;
//    }
//    [CKShareTools shareImage:[ShareSDK pngImageWithImage:image] text:nil container:(id<ISSContainer>)container];
    
    
    // 这里之所以先把图片转成Data再存起来,再传一个Path去分享
    // 而不使用[ShareSDK pngImageWithImage:(UIImage*)image]这个方法是因为在分享到短信的时候,只有传一个path,短信中才会有这个图片
    // 如果传图片过去,短信中显示不出来
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *  imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"shareTempImage@2x.png"];
    [imageData writeToFile:imagePath atomically:YES];
    [self shareMemeImageWithPath:imagePath];
}

- (void)shareMemeImageWithPath:(NSString * )imagePath
{
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:[NSString stringWithFormat:@"I'm using %@:%@",APP_NAME,APP_ITUNES_CONNECTION]
                                       defaultContent:APP_NAME
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:APP_NAME
                                                  url:APP_ITUNES_CONNECTION
                                          description:nil
                                            mediaType:SSPublishContentMediaTypeNews];
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:nil arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"分享成功");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(@"分享失败,错误码:%ld,错误描述:%@", (long)[error errorCode], [error errorDescription]);
                                }
                            }];
    
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
    return [_photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    
}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return [[_selections objectAtIndex:index] boolValue];
}


- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
    [_selections replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:selected]];
    
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)memeCollectionViewShouldScrollToItem:(int)item
{
    self.collectionView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    NSIndexPath * indexPath = [NSIndexPath indexPathForItem:item inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat w = (self.view.frame.size.width - 2) / 2;
    return CGSizeMake(w, w);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,2,0); // 上下左右的偏移量
}



@end
