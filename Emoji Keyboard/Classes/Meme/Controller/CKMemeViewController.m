//
//  CKMemeViewController.m
//  MJExtensionDemo
//
//  Created by 张赛 on 15/5/27.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import "UIImageView+WebCache.h"
#import "MJExtension.h"
#import "AFNetworking.h"
#import "SDImageCache.h"
#import "MWCommon.h"
#import "MWPhoto.h"
#import "MWPhotoBrowser.h"
#import "SVPullToRefresh.h"
#import "MBProgressHUD.h"
#import "CKGuideView.h"

#import "CKMemeViewController.h"
#import "CKMemeModel.h"
#import "CKMemeCollectionCell.h"
#import "CKMemeDownloadTool.h"
#import "CKMemeDataManager.h"
#import "CKShareTools.h"
#import "SDWebImageManager.h"



#define POPULAR @"http://version1.api.memegenerator.net/Instances_Select_ByPopular"

@interface CKMemeViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,MWPhotoBrowserDelegate,CKMemeCollectionCellDelegate,MBProgressHUDDelegate,CKMemeDownloadDelegate,CKGuideViewDelegate>
{
        NSMutableArray *_selections;
        MBProgressHUD *HUD;
        UIView * _backView;
}
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray * memeArray;
@property (nonatomic, strong) NSString * pageIndexString;
@property (nonatomic, strong) UIView * footerView;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray * hasDownloadArray;
//@property (nonatomic, strong) NSMutableArray * downloadToLocalArray;
@property (nonatomic, assign) BOOL firstIn;
@property (nonatomic, strong) NSOperationQueue *queue; // 下载队列
@property (nonatomic, strong) NSMutableArray * currentDowndingArray;

@end

@implementation CKMemeViewController

- (NSMutableArray *)currentDowndingArray
{
    if (_currentDowndingArray == nil) {
        _currentDowndingArray = [NSMutableArray array];
    }
    return _currentDowndingArray;
}

static NSString * const reuseIdentifier = @"CKMemeCollectionCell";

- (NSOperationQueue *)queue
{
    if (!_queue)
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 1;
    return _queue;
}


//- (NSMutableArray *)downloadToLocalArray
//{
//    if (_downloadToLocalArray == nil) {
//        _downloadToLocalArray = [NSMutableArray array];
//        NSMutableArray * tempArray = [NSMutableArray arrayWithContentsOfFile:MEME_DOWLOADED_PLIST_URL.path];
//        if (tempArray) {
//            [_downloadToLocalArray addObjectsFromArray:tempArray];
//        }
//        
//    }
//    return _downloadToLocalArray;
//}

- (NSMutableArray *)hasDownloadArray
{
    if (_hasDownloadArray == nil) {
        _hasDownloadArray = [NSMutableArray array];
      NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:MEME_SAVE_URL.path error:NULL];
        _hasDownloadArray = (NSMutableArray *)files;
    }
    return _hasDownloadArray;
}
- (NSMutableArray *)memeArray{
    if (_memeArray == nil) {
        _memeArray = [NSMutableArray array];
    }
    return _memeArray;
}
- (void)setUpCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    CGSize size = CGSizeMake(0, 0);
    [flowLayout setItemSize:size];//设置cell的尺
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);//设置其边界
    CGRect frame = self.view.frame;
    if (self.firstIn == 1) {
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:flowLayout];
            [self setEdgesForExtendedLayout:UIRectEdgeNone];
        self.firstIn = NO;
    }
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[CKMemeCollectionCell class] forCellWithReuseIdentifier:reuseIdentifier];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self.hasDownloadArray removeAllObjects];
//    [self.downloadToLocalArray removeAllObjects];
//    self.hasDownloadArray = nil;
//    self.downloadToLocalArray = nil;

    AFNetworkReachabilityManager * status = [AFNetworkReachabilityManager sharedManager];
//    CKLog(@"%d===状态",(int)status.networkReachabilityStatus);
    if (status.networkReachabilityStatus == 0) {
        
    }
    
    if (!self.memeArray.count) { // 如果页面为空的话,要重新加载数据
//    [self.collectionView triggerPullToRefresh];
    }
}

- (void)updateData:(NSNotification *)notification
{
    NSArray * array = notification.object;
    [self.hasDownloadArray removeAllObjects];
    [self.hasDownloadArray addObjectsFromArray:array];
    CKLog(@"%@====has",self.hasDownloadArray);
    [self.collectionView reloadData];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    UIApplication * app = [UIApplication sharedApplication];
    if (app.statusBarHidden) {
        app.statusBarHidden = NO;
    }
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpnotification];
    [self loadData];
}

- (void)loadData
{
    self.firstIn = YES;
    [self firstMemeInfo];
//    AFNetworkReachabilityManager * ReachabilityManager = [AFNetworkReachabilityManager sharedManager];
//    if (ReachabilityManager.reachable) { // 如果有网络
//    }
//    else
//    {
//        [self showNetworkStatus:@"Network is not stable, please reconnect." deley:2];
//    }
}

- (void)firstMemeInfo
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = @"Loading";
    [HUD show:YES];
//    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
//    NSString * pageString = @"0";
    self.pageIndexString = @"0";
    [self loadNewMemeWithUrlStr:POPULAR pageString:self.pageIndexString];
    [self setUpCollectionView];
    // 集成上拉刷新控件
    [self setupUpRefresh];
}
- (void)showNetworkStatus:(NSString *)status deley:(float)time
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = status;
    hud.margin = 10.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:time];
}

- (void)setUpnotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currenNetWorkStatus:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:@"CKOnlineMemeShouldReloadDataNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CKMemeTitleViewHasBeenClicked:) name:@"CKMemeTitleViewHasBeenClicked" object:nil];
}

- (void)currenNetWorkStatus:(NSNotification *)note
{
    NSDictionary * dict = note.userInfo;
    NSString * netStatus = dict[AFNetworkingReachabilityNotificationStatusItem];
//    CKLog(@"%@====note",note);
    switch (netStatus.intValue) {
        case 0: // 没网了
        {
            // Network is not stable, please reconnect.
           [self showNetworkStatus:@"Network is not stable, please reconnect." deley:2];
        }
            break;
            
        case 1:
        {
         
            [self loadMemeIfNoMemeNow];
        }
            break;
        case 2:
        {
            [self loadMemeIfNoMemeNow];
        }
            break;
        default:
            break;
    }
    
}

- (void)loadMemeIfNoMemeNow // 如果现在还没loadmeme,才执行
{
    if (!self.memeArray.count) {
        [self firstMemeInfo];
    }
    else // 如果有数据的话只提示网络重新连接
    {
        [self showNetworkStatus:@"Network connect" deley:1];
    }
}

// 保存meme.plist
- (void)CKMemeTitleViewHasBeenClicked:(NSNotification *)notification
{
    NSNumber * number = notification.object;
    int num = number.intValue;
    if (num == 0) { // 这儿是需要保存memeplist
//        if (self.downloadToLocalArray.count != 0) {
//            NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
//            NSString * folderPath = [cachePath stringByAppendingPathComponent:@"Meme"];
//            NSFileManager * fileMgr = [NSFileManager defaultManager];
//            NSError * error;
//            if (![fileMgr fileExistsAtPath:folderPath]) {
//                [fileMgr createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error];
//            }
//            NSString * imagePath = [folderPath stringByAppendingPathComponent:@"MemeResource"];
//            if (![fileMgr fileExistsAtPath:imagePath]) {
//                [fileMgr createDirectoryAtPath:imagePath withIntermediateDirectories:YES attributes:nil error:&error];
//            }
//            [self.downloadToLocalArray writeToFile:DOWNLOAD_MEME_PLIST_PATH atomically:YES];
//        }
    }
}


- (void)setupUpRefresh
{
    __weak CKMemeViewController *weakSelf = self;    
    [self.collectionView addInfiniteScrollingWithActionHandler:^{
        [weakSelf loadNewMemeWithUrlStr:POPULAR pageString:weakSelf.pageIndexString];
    }];
}




//- (void)setupDownRefresh {
//    __weak CKMemeViewController *weakSelf = self;
//    [self.collectionView addPullToRefreshWithActionHandler:^{
//       [weakSelf loadNewMemeWithUrlStr:POPULAR pageString:weakSelf.pageIndexString];
//    }];
//}

- (void)loadNewMemeWithUrlStr:(NSString *)urlString pageString:(NSString *)page
{
    // 1.请求管理者
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    // pageSize
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"pageSize"] = @"24";
    params[@"pageIndex"] = page;
    // 2.发送请求
    [mgr GET:POPULAR parameters:params success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        
        NSArray * tempArray = [CKMemeModel objectArrayWithKeyValuesArray:responseObject[@"result"]];
        [self.memeArray addObjectsFromArray:tempArray];
        if (responseObject) {
            self.pageIndexString = [NSString stringWithFormat:@"%d",self.pageIndexString.intValue + 1];// 请求成功的时候累加

            [CKSaveTools saveString:self.pageIndexString forKey:@"CKMemePageIndexPopular" inRegion:CKSaveRegionContainingApp];
            [self.collectionView reloadData];
            __weak CKMemeViewController *weakSelf = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.collectionView.infiniteScrollingView stopAnimating];
            });
            
            [HUD hide:YES];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        CKLog(@"请求失败");
        [HUD hide:YES];
        [self showNetworkStatus:@"Network is not stable, please reconnect." deley:2];
        __weak CKMemeViewController *weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf.collectionView.infiniteScrollingView stopAnimating];
        });

    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.memeArray.count;
}


- (CKMemeCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CKMemeCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CKMemeCollectionCell alloc]init];
    }

    CKMemeModel * meme = self.memeArray[indexPath.item];
    NSString * instaceUrl = meme.instanceImageUrl;
    NSArray * array = [instaceUrl componentsSeparatedByString:@"/"];
    NSString * instaceID = array.lastObject;
    CKLog(@"%@===instaceId",instaceID);
    if ([self.hasDownloadArray containsObject:instaceID]) {
        meme.progress = 1.0;
        meme.downloaded = YES;
    }
    else{
        meme.downloaded = NO;
    }
    meme.imageName = instaceID;
    meme.index = (int)indexPath.item;
    cell.memeModel = meme;
    cell.delegate = self;
    
    return cell;
}


#pragma mark <CKMemeCollectionCellDelegate>
- (void)downloadButtonOnCellClicked:(CKMemeCollectionCell *)cell
{
    [self saveClickCount];
//    OnlineMemeDownload
    [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:Main_OnlineMemeDownload];// 在线Meme的下载次数
    [CKSaveTools saveClickWithEventType:Main_OnlineMemeDownload key:Main_OnlineMemeDownload];
     CKMemeModel * memeModel = cell.memeModel;
    NSString * memeName = memeModel.imageName;
    
    if ([self.hasDownloadArray containsObject:memeName] || [self.currentDowndingArray containsObject:memeName] ) { // 如果已经存在
        // 正在下载
        return;
    }
    else
    {
        
        [self.currentDowndingArray addObject:memeName];

        NSURL * imageInstanceURL = [NSURL URLWithString:memeModel.instanceImageUrl];
        CKMemeDownloadTool * download = [[CKMemeDownloadTool alloc] init];
        download.delegate = self;
//        NSString * imageUniqueName = [[imageInstanceURL.absoluteString componentsSeparatedByString:@"/"].lastObject stringByReplacingOccurrencesOfString:@".jpg" withString:@""];
//        [SDWebImageManager sharedManager].delegate = self;
//    [[SDWebImageManager sharedManager] downloadImageWithURL:imageInstanceURL options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//        CKLog(@"%ld====receivedSize,%ld===expectedSize",(long)receivedSize,(long)expectedSize);
//        memeModel.progress = receivedSize * 1.0 / expectedSize;
//        [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:memeModel.index inSection:0]]];
//    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//        if (finished) {
//            NSData * data = UIImagePNGRepresentation(image);
//            [data writeToURL:[MEME_SAVE_URL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.png",imageUniqueName]] atomically:YES];
//            // 保存数据
//            [self.downloadToLocalArray insertObject:memeModel.imageName atIndex:0];
//            [self.currentDowndingArray removeObject:memeName];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"CKTabBarControllerShouldHiddenSelfTabBar" object:nil];
//        }
//    }];
        
        //    NSBlockOperation * downloadMeme = [NSBlockOperation blockOperationWithBlock:^{
        
        
        [download downloadWithURL:imageInstanceURL progress:^(float percent) {
            memeModel.progress = percent;
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:memeModel.index inSection:0]]];
            if (percent == 1.0) { // 下载完成
                // 保存数据
                if (![self.hasDownloadArray containsObject:memeModel.imageName]) {
                    [self.hasDownloadArray insertObject:memeModel.imageName atIndex:0];
                }
                [self.currentDowndingArray removeObject:memeName];

            }
        }];
    }
}

#pragma mark -- 添加弹出评价引导框
-(void)saveClickCount{
    
    NSUserDefaults *shareDefaults=[NSUserDefaults standardUserDefaults];
     NSUserDefaults *groupDefaults = [[NSUserDefaults alloc]initWithSuiteName:APP_GROUPS];
    if(![shareDefaults objectForKey:EVALUATE_STATUS_APP] &&
       ![groupDefaults objectForKey:EVALUATE_STATUS_GROUP]){
        //没有评价过
        [CKSaveTools saveClickCountToNum:2 Key:@"DownloadMemeCount" triggerAction:@selector(showRateView) withHandle:self];
    }
}
-(void)showRateView{
        [CKSaveTools saveString:@"HaveEvalate" forKey:EVALUATE_STATUS_APP inRegion:CKSaveRegionContainingApp];
        _backView =[[UIView alloc]initWithFrame:self.view.bounds];
        _backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        [[UIApplication sharedApplication].keyWindow addSubview:_backView];
        
        CGFloat w =self.view.frame.size.width;
        CGFloat h = 0;
        w = w*0.75;
        h = w*1.04;
        CKGuideView * rateView = [[CKGuideView alloc] initWithAppType:CKAppTypeContainingApp guideType:CKGuideTypeRate fram:CGRectMake(0, 0, w, h)];
        rateView.center =_backView.center;
        rateView.delegate = self;
        [[UIApplication sharedApplication].keyWindow addSubview:rateView];
    
}

#pragma mark -- 评分代理
-(void)CKGuideView:(CKGuideView*)guideView clickBtnType:(CKClickType)clickType{
    [guideView removeFromSuperview];
    [_backView removeFromSuperview];
    if(clickType == CKClickTypeRate){
        //评分
        
        [CKSaveTools saveString:@"EVALUATE_STATUS_GROUP" forKey:EVALUATE_STATUS_GROUP inRegion:CKSaveRegionAppGroup];
        [CKSaveTools saveClickWithEventType:KB_APP_Download_Meme_Clicked key:@"KB_APP_Download_Meme_Clicked"];
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:KB_APP_Download_Meme_Clicked];
        [CKSaveTools switchToURL:[NSString stringWithFormat:@"%@%@",AppStore_URL,APPID] withResponder:self.view];
    }else if(clickType == CKClickTypeCancel){
        [CKSaveTools saveClickWithEventType:KB_APP_Download_Meme_Cancel_Clicked key:@"KB_APP_Download_Meme_Cancel_Clicked"];
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:KB_APP_Download_Meme_Cancel_Clicked];
    }
}


- (void)dowloadDoneWithFinalIamgePath:(NSString *)imagePath
{
    NSString * imageUniqueName = [imagePath componentsSeparatedByString:@"/"].lastObject;
    NSFileManager * myFileMgr = [NSFileManager defaultManager];
    if ([myFileMgr fileExistsAtPath:imagePath]) {
        [myFileMgr moveItemAtPath:imagePath toPath:[MEME_SAVE_URL URLByAppendingPathComponent:imageUniqueName].path error:NULL];
        if ([myFileMgr fileExistsAtPath:[MEME_SAVE_URL URLByAppendingPathComponent:imageUniqueName].path]) {

            [[NSNotificationCenter defaultCenter] postNotificationName:@"CKTabBarControllerShouldHiddenSelfTabBar" object:imageUniqueName];
            [self.hasDownloadArray insertObject:imageUniqueName atIndex:0];
            [self.collectionView reloadData];
        }
    }
}


#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat w = (self.view.frame.size.width - 2) / 2;
    return CGSizeMake(w, w);
}



- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
return UIEdgeInsetsMake(0,0,2,0); // 上下左右的偏移量
    
}



//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    // Browser
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    for (CKMemeModel * memeModel in self.memeArray) {
        [photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:memeModel.instanceImageUrl]]];
    }
    
    self.photos = photos;

    
    // Create browser
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//    browser.controllerType = CKControllerTypeOnline;
    browser.displayActionButton = NO;
    
    browser.displayNavArrows = NO;
    
    browser.displaySelectionButtons = NO;
    browser.alwaysShowControls = NO;
    browser.zoomPhotosToFill = YES;
    
    browser.enableGrid = NO; // 表格
    browser.startOnGrid = NO; // 开始就是表格显示
    browser.enableSwipeToDismiss = YES;
    [browser setCurrentPhotoIndex:indexPath.item];
    
    _selections = [NSMutableArray new];
        for (int i = 0; i < photos.count; i++) {
            [_selections addObject:[NSNumber numberWithBool:NO]];
        }
    [self.navigationController pushViewController:browser animated:YES];
    self.navigationController.navigationBar.hidden = YES;

}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {

}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return [[_selections objectAtIndex:index] boolValue];
}


- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
    [_selections replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:selected]];

}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)memeCollectionViewShouldScrollToItem:(int)item
{
    self.collectionView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    NSIndexPath * indexPath = [NSIndexPath indexPathForItem:item inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
}

- (void)shareMemeWithItem:(int)item
{
    MWPhoto * currentPhoto = self.photos[item];
    UIImage * image = currentPhoto.underlyingImage;
    
    //创建弹出菜单容器
    id container = [ShareSDK container];
    if (iPad) {
        [container setIPadContainerWithView:self.view arrowDirect:UIPopoverArrowDirectionUp];
    }
    else{
        container = nil;
    }
    // 这里之所以先把图片转成Data再存起来,再传一个Path去分享
    // 而不使用[ShareSDK pngImageWithImage:(UIImage*)image]这个方法是因为在分享到短信的时候,只有传一个path,短信中才会有这个图片
    // 如果传图片过去,短信中显示不出来
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *  imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"shareTempImage@2x.png"];
    [imageData writeToFile:imagePath atomically:YES];
    [self shareMemeImageWithPath:imagePath];
}

- (void)shareMemeImageWithPath:(NSString * )imagePath{
    //构造分享内容
       id<ISSContent> publishContent = [ShareSDK content:[NSString stringWithFormat:@"I'm using %@:%@",APP_NAME,APP_ITUNES_CONNECTION]
                                       defaultContent:APP_NAME
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:APP_NAME
                                                  url:APP_ITUNES_CONNECTION
                                          description:nil
                                            mediaType:SSPublishContentMediaTypeNews];
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:nil arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"分享成功");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(@"分享失败,错误码:%ld,错误描述:%@", (long)[error errorCode], [error errorDescription]);
                                }
                            }];
    
}

- (void)downloadMemeWithItem:(int)item
{
    MWPhoto * currentPhoto = self.photos[item];
    UIImage * image = currentPhoto.underlyingImage;
    CKMemeModel * memeModel = self.memeArray[item];
    if ([self.hasDownloadArray containsObject:memeModel.imageName]) { // 如果已经存在
        return;
    }
    NSURL * imageInstanceURL = [NSURL URLWithString:memeModel.instanceImageUrl];
    if (image == nil) {
        CKMemeDownloadTool * download = [[CKMemeDownloadTool alloc] init];
        [download downloadWithURL:imageInstanceURL progress:^(float percent) {
            memeModel.progress = percent;
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:memeModel.index inSection:0]]];
            if (percent == 1.0) { // 下载完成
                [self.hasDownloadArray addObject:memeModel.imageName];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CKTabBarControllerShouldHiddenSelfTabBar" object:memeModel.imageName];
                [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:item inSection:0]]];
}
        }];
    }
    else
    {
      BOOL saveSuccessful = [UIImageJPEGRepresentation(image, 1) writeToFile:[NSString stringWithFormat:@"%@/%@",MEME_SAVE_URL.path,memeModel.imageName] atomically:YES];
        if (saveSuccessful) {
            [self.hasDownloadArray addObject:memeModel.imageName];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"CKTabBarControllerShouldHiddenSelfTabBar" object:memeModel.imageName];
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:item inSection:0]]];

        }
    }
    [self showNetworkStatus:@"Download Success" deley:2];

}




@end
