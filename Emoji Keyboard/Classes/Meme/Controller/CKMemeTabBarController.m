//
//  CKMemeTabBarController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/3.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMemeTabBarController.h"
#import "CKMemeTitleView.h"


@interface CKMemeTabBarController ()<CKMemeTitleViewDelegate>
@property (nonatomic, strong) CKMemeTitleView * memeTitleView;
@property (nonatomic, strong) UIButton * deleteMemeButton;
@end

static NSString * const reuseIdentifier = @"CKMemeLocalCollectionCell";
@implementation CKMemeTabBarController


- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    CKMemeTitleView * memeTitleView = [[CKMemeTitleView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 2, 44)];
    self.memeTitleView = memeTitleView;
    self.navigationItem.titleView = memeTitleView;
    memeTitleView.selectedIndex = 0;
    memeTitleView.delegate = self;

    self.tabBar.hidden = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    UIButton * deleteMemeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteMemeButton.frame = CGRectMake(0, 0, 60, 44);
    deleteMemeButton.layer.masksToBounds = YES;
    deleteMemeButton.layer.cornerRadius = 15;
    [deleteMemeButton setTitle:@"Select" forState:UIControlStateNormal];
    deleteMemeButton.titleLabel.font = [UIFont systemFontOfSize:15];
    UIBarButtonItem *itemRight = [[UIBarButtonItem alloc] initWithCustomView:deleteMemeButton];
    [deleteMemeButton addTarget:self action:@selector(deleteCustomMeme:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = itemRight;
    self.deleteMemeButton = deleteMemeButton;
    deleteMemeButton.tag = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)deleteCustomMeme:(UIButton *)button
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CKLocalMemeShouldDelete" object:button];
    button.tag = !button.tag; // 改变tag值
    NSString * title = !button.tag?@"Select":@"Cancel";
    [self.deleteMemeButton setTitle:title forState:UIControlStateNormal];
}


- (void)viewWillAppear:(BOOL)animated
{   [super viewWillAppear:animated];
    self.tabBar.hidden = YES;
}
- (void)memeTitleButtonClickedToChangeLocalOrOnline:(UIButton *)button
{
    int buttonTag = (int)button.tag;
    self.memeTitleView.selectedIndex = buttonTag;
    self.deleteMemeButton.hidden = buttonTag;
    [UIView animateWithDuration:.5 animations:^{
        self.selectedIndex = buttonTag;
    }];
}
@end
