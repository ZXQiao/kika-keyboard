//
//  CKSaveShareController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/9.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKSaveShareController.h"
#import "CKTabBarController.h"
#import "CKShareTools.h"


@interface CKSaveShareController()
@property (nonatomic, strong) UIButton * backToLocalButton;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, strong) UIImage * finalImage;
@end

@implementation CKSaveShareController
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpTitleView];
//    self.navigationItem.hidesBackButton = YES;
}

- (void)backToLocalButtonClick:(UIButton *)button
{
    switch (button.tag) {
        case 0:
        {
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CKTabBarControllerShouldHiddenSelfTabBar" object:nil];
        }
            break;
            case 1:
        {
            //创建弹出菜单容器
            id container = [ShareSDK container];
            if (iPad) {
                [container setIPadContainerWithView:self.shareButton arrowDirect:UIPopoverArrowDirectionDown];
            }
            else{
                container = nil;
            }
            //            [self shareMemeImage:self.finalImage];
            
            // 这里之所以先把图片转成Data再存起来,再传一个Path去分享
            // 而不使用[ShareSDK pngImageWithImage:(UIImage*)image]这个方法是因为在分享到短信的时候,只有传一个path,短信中才会有这个图片
            // 如果传图片过去,短信中显示不出来
            NSData *imageData = UIImagePNGRepresentation(self.finalImage);
            NSString *  imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"shareTempImage@2x.png"];
            [imageData writeToFile:imagePath atomically:YES];
            [self shareMemeImageWithPath:imagePath];
        }
            break;
        default:
            break;
    }
}


- (void)shareMemeImageWithPath:(NSString * )imagePath
{
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:[NSString stringWithFormat:@"I'm using %@:%@",APP_NAME,APP_ITUNES_CONNECTION]
                                       defaultContent:APP_NAME
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:APP_NAME
                                                  url:APP_ITUNES_CONNECTION
                                          description:nil
                                            mediaType:SSPublishContentMediaTypeNews];
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:nil arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"分享成功");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(@"分享失败,错误码:%ld,错误描述:%@", (long)[error errorCode], [error errorDescription]);
                                }
                            }];
    
}
- (void)setUpTitleView
{
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Share";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;
    
    
    
   
}

- (void)setImage:(UIImage *)image withframe:(CGRect)frame
{
    
    self.finalImage = image;
    UIImageView * imageView = [[UIImageView alloc] init];
    [self.view addSubview:imageView];
    imageView.frame = frame;
    imageView.image = image;
    imageView.center = CGPointMake(self.view.center.x, imageView.center.y + 20);
    
 
    
    
    UIView * success = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 2, 42)];
    success.center = CGPointMake(self.view.center.x, CGRectGetMaxY(imageView.frame) + 40 + 21);
    [self.view addSubview:success];
    
    UIImageView * successImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 42, 42)];
    successImage.image = [UIImage imageWithBundlePath:@"meme_success@2x" imageType:nil];
    [success addSubview:successImage];
    
    UILabel * successLabel = [[UILabel alloc] initWithFrame:CGRectMake(42 + 10, 0, success.frame.size.width - 42 - 10, success.frame.size.height)];
    successLabel.text = @"Success";
    successLabel.textColor = [UIColor colorFromHexString:@"#26d99c"];
    successLabel.font = [UIFont systemFontOfSize:25];
    [success addSubview:successLabel];
    successLabel.textAlignment = NSTextAlignmentCenter;
    


    CGFloat labelY = self.view.frame.size.height - 69 - 30 - 64;
    UILabel * backLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, labelY, self.view.frame.size.width / 2, 30)];
    backLabel.font = [UIFont systemFontOfSize:20];
    backLabel.textColor = [UIColor colorFromHexString:@"#65befc"];
    backLabel.text = @"Back to local";
    [self.view addSubview:backLabel];
    backLabel.textAlignment = NSTextAlignmentCenter;

    
    UIButton * backToLocalButton = [[UIButton alloc] init];
    [self.view addSubview:backToLocalButton];
    backToLocalButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
     backToLocalButton.tag = 0;
    backToLocalButton.frame = CGRectMake(0, CGRectGetMinY(backLabel.frame) - 20 - 50 , self.view.frame.size.width * .5, 50);
    self.backToLocalButton = backToLocalButton;
    
    [backToLocalButton setImage:[UIImage resizableImageNamed:@"meme_backtolocal"] forState:UIControlStateNormal];
    [backToLocalButton addTarget:self action:@selector(backToLocalButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [backToLocalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    UILabel * shareLable = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2, labelY, self.view.frame.size.width / 2, 30)];
    shareLable.font = [UIFont systemFontOfSize:20];
    shareLable.textColor = [UIColor colorFromHexString:@"#2eda9f"];
    shareLable.text = @"Share";
    shareLable.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:shareLable];
    
    UIButton * shareButton = [[UIButton alloc] init];
    [self.view addSubview:shareButton];
    shareButton.frame = CGRectMake(backToLocalButton.frame.size.width, CGRectGetMinY(backLabel.frame) - 20 - 50  , self.view.frame.size.width * .5, 50);
    self.shareButton = shareButton;
    [shareButton setImage:[UIImage resizableImageNamed:@"meme_share_custom"]forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(backToLocalButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    shareButton.tag = 1;
        shareButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [shareButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    
    
}

@end
