//
//  LanguageTableViewController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 14/11/11.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "CKMemeTextViewController.h"
#import "CKMemeTextTableViewCell.h"
#import "CKShadowLable.h"

@interface CKMemeTextViewController ()


@property (nonatomic, strong) NSMutableArray * textArray;

@end
static NSString * const reuseIdentifier = @"CKMemeTextTableViewControllerCell";

@implementation CKMemeTextViewController

- (NSMutableArray *)textArray
{
    NSError * error ;
    NSString * totalString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"memeText.h" ofType:nil] encoding:NSUTF8StringEncoding error:&error];
    return (NSMutableArray *)[totalString componentsSeparatedByString:@"+"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Text";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * tableViewContentOffset = [userDefaults objectForKey:@"CKMemeTextViewControllertableViewContentOffset"];
    CGPoint offset = CGPointFromString(tableViewContentOffset);
    self.tableView.contentOffset = offset;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSString * tableViewContentOffset = NSStringFromCGPoint(self.tableView.contentOffset);
    [CKSaveTools saveString:tableViewContentOffset forKey:@"CKMemeTextViewControllertableViewContentOffset" inRegion:CKSaveRegionExtensionKeyboard];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return self.textArray.count;
}

- (CKMemeTextTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CKMemeTextTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (cell == nil) {
        cell = [[CKMemeTextTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString * totalString = self.textArray[indexPath.row];
    NSArray * array = [totalString componentsSeparatedByString:@"#"];
    cell.topLabel.text = array[0];
    cell.bottomLabel.text = array[1];
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CKUserSelectedMemeText" object:self.textArray[indexPath.row]];
    
}


@end
