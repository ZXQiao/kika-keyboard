//
//  CKCustomMemeViewController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/5.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKCustomMemeViewController.h"
#import "CKSaveShareController.h"
#import "CKMemeTextViewController.h"
#import <CoreText/CoreText.h>
#import "CKMemeDataManager.h"
#define KEY_BOARD_HEIGHT 258 // 键盘 + 预测栏的高度
#define NAV_BAR_HEIGHT 64
#define MAX_FONTSIZE 30
#define MIN_FONTSIZE 16
#define DEFAULT_FONTSIZE MAX_FONTSIZE
#define DEFAULT_FONTNAME @"Impact"
#define DEFAULT_TEST_STR @"I"

@interface CKCustomMemeViewController ()<UITextViewDelegate>
{
    CGSize defaultSuggestSize;
    int currentTopTextViewFontSize;
    int currentBottomTextViewFontSize;
}

@property (nonatomic, strong) UIImageView * imageView;
@property (nonatomic, strong) UIButton * textButton;
@property (nonatomic, strong) UIButton * randomButton;
@property (nonatomic, strong) UIButton * saveButton;

@property (nonatomic, strong) UITextView * topTextView;
@property (nonatomic, strong) UITextView * bottomTextView;
@property (nonatomic, assign) int topFontSize;
@property (nonatomic, assign) int bottomFontSize;
@property (nonatomic, assign) int custom_meme_count;
@property (nonatomic, strong) UILabel * topLabel;
@property (nonatomic, strong) UILabel * bottomLabel;
//@property (nonatomic, strong) UIView * topView;
//@property (nonatomic, strong) UIView * bottomView;

@property (nonatomic, strong) NSMutableArray * localMemeArray;
@property (nonatomic, strong) NSMutableArray * textArray;


@end

@implementation CKCustomMemeViewController

- (NSMutableArray *)textArray
{
    NSError * error ;
    NSString * totalString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"memeText.h" ofType:nil] encoding:NSUTF8StringEncoding error:&error];
    return (NSMutableArray *)[totalString componentsSeparatedByString:@"+"];
}

- (int)custom_meme_count
{
    if (_custom_meme_count == 0) {
        _custom_meme_count = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"custom_meme_count" ];
    }
    return _custom_meme_count;
}
//- (NSMutableArray *)localMemeArray
//{
//    if (_localMemeArray == nil) {
//        _localMemeArray = [NSMutableArray array];
//        NSMutableArray * temp = [NSMutableArray arrayWithContentsOfFile:MEME_DOWLOADED_PLIST_URL.path];
//        if (temp) {
//            [_localMemeArray addObjectsFromArray:temp];
//        }
//    }
//    return _localMemeArray;
//}


//- (UILabel *)topLabel
//{
//    if (_topLabel == nil) {
//        
//    }
//    return _topLabel;
//}
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChange:) name:UITextViewTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CKUserSelectedMemeText:) name:@"CKUserSelectedMemeText" object:nil];
    
    UIButton * textButton = [[UIButton alloc] init];
    self.textButton = textButton;
    [self.view addSubview:textButton];
    [textButton setTitle:@"Text" forState:UIControlStateNormal];
    [textButton setBackgroundImage:[UIImage imageWithBundlePath:@"random_button@2x" imageType:nil] forState:UIControlStateNormal];
    [textButton setBackgroundImage:[UIImage imageWithBundlePath:@"random_button_pressed@2x" imageType:nil] forState:UIControlStateHighlighted];
    textButton.tag = 0;
    CGFloat padding = 40;
    CGFloat buttonW = (self.view.frame.size.width - padding * 3) / 2;
    CGFloat buttonH = 30;
    CGFloat buttonY = [UIScreen mainScreen].bounds.size.height - KEY_BOARD_HEIGHT - 20 - buttonH - NAV_BAR_HEIGHT;
    textButton.frame = CGRectMake(padding, buttonY, buttonW, buttonH);
    [textButton setTitleColor:[UIColor skyBlueColor] forState:UIControlStateNormal];
    [textButton addTarget:self action:@selector(textRandomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * randomButton = [[UIButton alloc] init];
    self.randomButton = randomButton;
    [self.view addSubview:randomButton];
    [randomButton setBackgroundImage:[UIImage imageWithBundlePath:@"random_button@2x" imageType:nil] forState:UIControlStateNormal];
    [randomButton setBackgroundImage:[UIImage imageWithBundlePath:@"random_button_pressed@2x" imageType:nil] forState:UIControlStateHighlighted];
    randomButton.tag = 1;
    [randomButton addTarget:self action:@selector(textRandomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [randomButton setTitle:@"Random" forState:UIControlStateNormal];
    randomButton.frame = CGRectMake(padding * 2 + buttonW, buttonY, buttonW, buttonH);
    [randomButton setTitleColor:[UIColor skyBlueColor] forState:UIControlStateNormal];
    randomButton.backgroundColor = [UIColor clearColor];
    

    
    CGFloat imagePadding = 10;
    CGFloat imageWH = CGRectGetMinY(self.textButton.frame) - imagePadding *2;
    CGFloat leftPadding = (self.view.frame.size.width - imageWH ) / 2;
    UIImageView * imageView = [[UIImageView alloc]init];
    [self.view addSubview:imageView];
    self.imageView = imageView;
    imageView.frame = CGRectMake(leftPadding, imagePadding, imageWH, imageWH);
    imageView.userInteractionEnabled = YES;
    
    CGSize imageViewSize = self.imageView.frame.size;
    
    UITextView * topTextView = [[UITextView alloc] init];
    topTextView.frame = CGRectMake(0, 0, imageViewSize.width , imageViewSize.height / 2);
    [self.imageView addSubview:topTextView];
    self.topTextView = topTextView;
    topTextView.backgroundColor = [UIColor clearColor];
    topTextView.font = [UIFont fontWithName:DEFAULT_FONTNAME size:DEFAULT_FONTSIZE];
    topTextView.textColor = [UIColor whiteColor];
    topTextView.contentOffset = CGPointMake(0, imageViewSize.height / 4);
    topTextView.scrollEnabled = NO;
    topTextView.delegate = self;
    self.topTextView.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    topTextView.textAlignment = NSTextAlignmentCenter;
    self.topFontSize = DEFAULT_FONTSIZE;
    self.bottomFontSize = DEFAULT_FONTSIZE;


    UILabel * topLabel = [[UILabel alloc] init];
    topLabel.frame = CGRectMake(0, 0, self.topTextView.frame.size.width, self.topTextView.frame.size.height * 1/3);
    [self.imageView insertSubview:topLabel belowSubview:topTextView];
    self.topLabel = topLabel;
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.font = [UIFont fontWithName:@"Impact" size:40];
    topLabel.textColor = [UIColor whiteColor];
    topLabel.numberOfLines = 0;
    topLabel.adjustsFontSizeToFitWidth = YES;
    topLabel.text = @"TOP TEXT";
    topLabel.textAlignment = NSTextAlignmentCenter;

    currentTopTextViewFontSize = DEFAULT_FONTSIZE;
    currentBottomTextViewFontSize = DEFAULT_FONTSIZE;
    defaultSuggestSize = [self getSuggestFrameSizeForStringInTextView:topTextView string:DEFAULT_TEST_STR fontSize:DEFAULT_FONTSIZE];

    UITextView * bottomTextView = [[UITextView alloc] init];
    CGFloat textPadding = topTextView.textContainerInset.top + topTextView.textContainerInset.bottom;
    CGFloat topY = imageViewSize.height - defaultSuggestSize.height - textPadding;
    bottomTextView.frame = CGRectMake(0, topY, imageViewSize.width, defaultSuggestSize.height + textPadding);
    [self.imageView addSubview:bottomTextView];
    self.bottomTextView = bottomTextView;
    bottomTextView.font = [UIFont fontWithName:DEFAULT_FONTNAME size:DEFAULT_FONTSIZE];
    bottomTextView.textColor = [UIColor whiteColor];
    bottomTextView.scrollEnabled = NO;
    bottomTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    bottomTextView.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [topTextView becomeFirstResponder];
    bottomTextView.backgroundColor = [UIColor clearColor];
    bottomTextView.textAlignment = NSTextAlignmentCenter;
    bottomTextView.delegate = self;
    
    UILabel * bottomLabel = [[UILabel alloc] init];
    bottomLabel.frame = CGRectMake(0, self.imageView.frame.size.height - self.imageView.frame.size.height / 6, self.bottomTextView.frame.size.width, self.imageView.frame.size.height / 6);

    [self.imageView insertSubview:bottomLabel belowSubview:bottomTextView];
    self.bottomLabel = bottomLabel;
    bottomLabel.backgroundColor = [UIColor clearColor];
    bottomLabel.font = [UIFont fontWithName:@"Impact" size:40];
    bottomLabel.textColor = [UIColor whiteColor];
    bottomLabel.numberOfLines = 0;
    bottomLabel.adjustsFontSizeToFitWidth = YES;
    bottomLabel.text = @"BOTTOM TEXT";
    bottomLabel.textAlignment = NSTextAlignmentCenter;
    [self setUpTitleView];
}

- (void)CKUserSelectedMemeText:(NSNotification *)notification
{
    NSString * totalString = notification.object;
    [self settingMemeTextWithTotalString:totalString];
}

- (void)settingMemeTextWithTotalString:(NSString *)totalString
{
//    self.topView.hidden = YES;
    self.topLabel.hidden = YES;
//    self.bottomView.hidden = YES;
    self.bottomLabel.hidden = YES;
    NSArray * array = [totalString componentsSeparatedByString:@"#"];
    NSString * topString = array[0];
    NSString * bottomString ;
    if (array.count == 2) {
        bottomString = array[1];
    }
    if (topString == nil) {
        topString = @" ";
    }
    if (bottomString == nil) {
        bottomString = @" ";
    }
    self.topTextView.text = topString.uppercaseString;
    self.bottomTextView.text = bottomString.uppercaseString;
    [self.topTextView endEditing:YES];
    [self.bottomTextView endEditing:YES];
    [self.topTextView resignFirstResponder];
    [self.bottomTextView resignFirstResponder];
    [self.view endEditing:YES];
    NSNotification * topNotification = [NSNotification notificationWithName:@"top" object:(id)self.topTextView];
    [self textChange:topNotification];
    NSNotification * bottomNotification = [NSNotification notificationWithName:@"bottom" object:(id)self.bottomTextView];
    [self textChange:bottomNotification];
}

- (void)textRandomButtonClicked:(UIButton *)button
{
    switch (button.tag) {
        case 0:
        {
            self.title = @"Custom";
            CKMemeTextViewController * textViewController = [[CKMemeTextViewController alloc] init];
            [self.navigationController pushViewController:textViewController animated:YES];
 
        }
            break;
        case 1:
            
        {
            int randomNum = arc4random() % 50;
            NSString * totalString = self.textArray[randomNum];
            [self settingMemeTextWithTotalString:totalString];
        }
            break;
        default:
            break;
    }

}

- (void)setUpTitleView
{
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Custom";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;
    
    // 给导航条右边添加一个按钮
    UIButton * cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cameraButton.frame = CGRectMake(0, 0,100, 44);
    cameraButton.layer.masksToBounds = YES;
    cameraButton.layer.cornerRadius = 15;
    cameraButton.contentMode = UIViewContentModeRight;
//    cameraButton.tit
    [cameraButton setTitle:@"Save&Share" forState:UIControlStateNormal];
    cameraButton.adjustsImageWhenHighlighted = NO;
    UIBarButtonItem *itemRight = [[UIBarButtonItem alloc] initWithCustomView:cameraButton];
    [cameraButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    cameraButton.backgroundColor = [UIColor clearColor];
    self.navigationItem.rightBarButtonItem = itemRight;
    self.saveButton = cameraButton;
    cameraButton.tag = 0;
}

- (void)saveButtonClicked:(UIButton *)buttton
{

    [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:Main_CustomMemeSaveShare];
// 自定义meme保存分享的次数
    [CKSaveTools saveClickWithEventType:Main_CustomMemeSaveShare key:Main_CustomMemeSaveShare];
    
    [self.topTextView resignFirstResponder];
    [self.bottomTextView resignFirstResponder];
    if (self.topLabel.hidden == NO) {
        self.topLabel.hidden = YES;
        self.bottomLabel.hidden = YES;
    }

    
    NSData * data = [self cutImage];
    NSString * imageName = [NSString stringWithFormat:@"custom_meme_image_%d.png",self.custom_meme_count];
    NSString * finalPath = [NSString stringWithFormat:@"%@/%@",MEME_SAVE_URL.path,imageName];
    [data writeToFile:finalPath atomically:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CKTabBarControllerShouldHiddenSelfTabBar" object:imageName];
    [CKSaveTools saveInteger:self.custom_meme_count + 1  forKey:@"custom_meme_count" inRegion:CKSaveRegionContainingApp];
    CKSaveShareController * saveVC = [[CKSaveShareController alloc] init];
    [self.navigationController pushViewController:saveVC animated:YES];
    [saveVC setImage:[UIImage imageWithData:data] withframe:self.imageView.bounds];

}

- (CGSize)getSuggestFrameSizeForStringInTextView:(UITextView *)textView string:(NSString *)text fontSize:(int)fontSize
{
    //  creating and formatting CFMutableAttributedString
    CFMutableAttributedStringRef attrStr = CFAttributedStringCreateMutable(kCFAllocatorDefault, 0);
    CFAttributedStringReplaceString (attrStr, CFRangeMake(0, 0), (CFStringRef) text);
    CTFontRef font = CTFontCreateWithName((CFStringRef)DEFAULT_FONTNAME, fontSize, NULL);
    CTTextAlignment alignment = kCTJustifiedTextAlignment;
    CTParagraphStyleSetting _settings[] = { {kCTParagraphStyleSpecifierAlignment, sizeof(alignment), &alignment} };
    CTParagraphStyleRef paragraphStyle = CTParagraphStyleCreate(_settings, sizeof(_settings) / sizeof(_settings[0]));
    CFAttributedStringSetAttribute(attrStr, CFRangeMake(0, CFAttributedStringGetLength(attrStr)), kCTParagraphStyleAttributeName, paragraphStyle);
    CFAttributedStringSetAttribute(attrStr, CFRangeMake(0, CFAttributedStringGetLength(attrStr)), kCTFontAttributeName, font);
    CFRelease(paragraphStyle);
    CFRelease(font);

    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attrStr);
    CFRelease(attrStr);

    CFRange range;
    CGFloat maxWidth  = textView.frame.size.width - textView.textContainer.lineFragmentPadding * 2;
    CGFloat maxHeight = 1000;//self.imageView.frame.size.height / 2;//textView.frame.size.height;
    CGSize constraint = CGSizeMake(maxWidth, maxHeight);
    CGSize fianlSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, text.length), nil, constraint, &range);
    // C-type的变量要手动释放
    CFRelease(framesetter);
    return fianlSize;
}

- (void)textChange:(NSNotification *)notification
{
    UITextView * textView = notification.object;
    CGFloat textPadding = textView.textContainerInset.top + textView.textContainerInset.bottom;
    CGSize imageViewSize = self.imageView.frame.size;
    CGFloat topY;

    CGSize textFrameSize;
    CGSize singleFrameSize;
    CGFloat realTextPadding;
    int currentFontSize;
    if (textView == self.topTextView) {
        currentFontSize = currentTopTextViewFontSize;
    }
    else {
        currentFontSize = currentBottomTextViewFontSize;
    }

    if (textView.text.length) {// 输入不为空
        textFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:textView.text fontSize:currentFontSize];
        singleFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:DEFAULT_TEST_STR fontSize:currentFontSize];
        realTextPadding = textPadding + (textFrameSize.height / singleFrameSize.height);
        if (textFrameSize.height + realTextPadding <= imageViewSize.height / 2) {
            for (int i = currentFontSize + 1; i <= MAX_FONTSIZE; i++) {
                textFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:textView.text fontSize:i];
                singleFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:DEFAULT_TEST_STR fontSize:i];
                realTextPadding = textPadding + (textFrameSize.height / singleFrameSize.height);
                if (textFrameSize.height + realTextPadding > imageViewSize.height / 2) {
                    currentFontSize = i - 1;
                    break;
                }
            }
        }
        else {
            for (int i = currentFontSize - 1; i >= MIN_FONTSIZE; i--) {
                textFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:textView.text fontSize:i];
                singleFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:DEFAULT_TEST_STR fontSize:i];
                realTextPadding = textPadding + (textFrameSize.height / singleFrameSize.height);
                if (textFrameSize.height + realTextPadding <= imageViewSize.height / 2) {
                    currentFontSize = i;
                    break;
                }
            }
        }
        textFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:textView.text fontSize:currentFontSize];
        singleFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:DEFAULT_TEST_STR fontSize:currentFontSize];
        realTextPadding = textPadding + (textFrameSize.height / singleFrameSize.height);
        CGFloat currentHeight;

        if (textFrameSize.height + realTextPadding <= imageViewSize.height / 2) {
            currentHeight = textFrameSize.height + realTextPadding;
        }
        else {
            currentHeight = imageViewSize.height / 2;
        }

        if (textView == self.topTextView) {
            self.topLabel.hidden = YES;
            topY = 0;
        }
        else {
            self.bottomLabel.hidden = YES;
            topY = imageViewSize.height - currentHeight;
        }

        textView.frame = CGRectMake(0, topY, imageViewSize.width, currentHeight);
    }
    else
    {
        currentFontSize = DEFAULT_FONTSIZE;
        if (textView == self.topTextView) {
            self.topLabel.hidden = NO;
            topY = 0;
        }
        else {
            self.bottomLabel.hidden = NO;
            topY = imageViewSize.height - defaultSuggestSize.height - textPadding;
        }
        textView.frame = CGRectMake(0, topY, imageViewSize.width, defaultSuggestSize.height + textPadding);
    }

    if (textView == self.topTextView) {
        currentTopTextViewFontSize = currentFontSize;
    }
    else {
        currentBottomTextViewFontSize = currentFontSize;
    }
    textView.font = [UIFont fontWithName:DEFAULT_FONTNAME size:currentFontSize];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@""]) {
        return YES;
    }
    int currentFontSize;
    if (textView == self.topTextView) {
        currentFontSize = currentTopTextViewFontSize;
    }
    else {
        currentFontSize = currentBottomTextViewFontSize;
    }
    if (currentFontSize == MIN_FONTSIZE) {
        CGSize textFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:[textView.text stringByAppendingString:text] fontSize:currentFontSize];
        CGSize singleFrameSize = [self getSuggestFrameSizeForStringInTextView:textView string:DEFAULT_TEST_STR fontSize:currentFontSize];
        CGFloat textPadding = textView.textContainerInset.top + textView.textContainerInset.bottom;
        CGFloat realTextPadding = textPadding + (textFrameSize.height / singleFrameSize.height);
        CGSize imageViewSize = self.imageView.frame.size;
    
        if (textFrameSize.height + realTextPadding > imageViewSize.height / 2) {
            return NO;
        }
        else {
            return YES;
        }
    }
    else {
        return YES;
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_imageView setImage:_detailImage];
    
}



- (NSData *)cutImage
{
    if(&UIGraphicsBeginImageContextWithOptions){
        UIGraphicsBeginImageContextWithOptions(self.imageView.frame.size, NO, 0.0);
    }
    //获取图像
    [self.imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData * data;
    if (UIImagePNGRepresentation(image) == nil) {
        data = UIImageJPEGRepresentation(image, 1);
    } else {
        data = UIImagePNGRepresentation(image);
    }
    return data;
}
@end
