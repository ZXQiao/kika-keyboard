//
//  CKSaveShareController.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/9.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKBaseViewController.h"

@interface CKSaveShareController : CKBaseViewController

- (void)setImage:(UIImage *)image withframe:(CGRect)frame;

@end
