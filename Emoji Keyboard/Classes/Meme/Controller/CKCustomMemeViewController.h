//
//  CKCustomMemeViewController.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/5.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKBaseViewController.h"

@interface CKCustomMemeViewController : CKBaseViewController

@property (nonatomic, strong) UIImage *detailImage;
@end
