//
//  CKMemeTextTableViewCell.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/12.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MEME_FONT_NAME @"Impact"
@class CKShadowLable;
@interface CKMemeTextTableViewCell : UITableViewCell


@property (nonatomic, strong) CKShadowLable * topLabel;
@property (nonatomic, strong) CKShadowLable * bottomLabel;
@end
