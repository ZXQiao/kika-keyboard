//
//  CKThemeCollectionCell.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/14.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMemeCollectionCell.h"
#import "UIImageView+WebCache.h"
#import "DACircularProgressView.h"
#import "Colours.h"
#import "CKLocalMemeModel.h"


@interface CKMemeCollectionCell()
{
    	DACircularProgressView *_loadingIndicator;
}

@property (nonatomic, strong) UIImageView * backImageView;

@property (nonatomic, strong) UIButton * downloadButton;

@property (nonatomic, strong) UIImageView * meme_coverView;




@end

#define DOWNLOAD_MEME_IMG_PATH [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"Meme/OnlineDownloadImage"]
@implementation CKMemeCollectionCell


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    _loadingIndicator = [[DACircularProgressView alloc] initWithFrame:CGRectMake(140.0f, 30.0f, 40.0f, 40.0f)];
    _loadingIndicator.userInteractionEnabled = NO;
        _loadingIndicator.thicknessRatio = 0.1;
        _loadingIndicator.roundedCorners = NO;
    _loadingIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin |
    UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.contentView addSubview:_loadingIndicator];
    [self.contentView bringSubviewToFront:_loadingIndicator];
    _loadingIndicator.hidden = NO;
    _loadingIndicator.progress = 0;
    
    UIImageView * meme_coverView = [[UIImageView alloc] init];
    self.meme_coverView = meme_coverView;
    [self.contentView insertSubview:meme_coverView belowSubview:self.downloadButton];
    meme_coverView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"meme_cover@2x.png" ofType:nil] ];
    
    UIButton * deleteButton = [[UIButton alloc] init];
    self.deleteButton = deleteButton;
    [self.themeView addSubview:deleteButton];
    self.deleteButton.hidden = YES;
    deleteButton.backgroundColor = [UIColor clearColor];
    [deleteButton setImage:[UIImage imageWithBundlePath:@"meme_delete@2x" imageType:nil] forState:UIControlStateNormal];
    [deleteButton setImage:[UIImage imageWithBundlePath:@"meme_delete_selected@2x" imageType:nil] forState:UIControlStateSelected];
    [deleteButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    return self;
}
- (void)deleteButtonClicked:(UIButton *)button{
    CKLocalMemeModel * localMemeModel = self.localMemeModel;
    if ([self.delegate respondsToSelector:@selector(localDeleteButtonClicked:)]) {
        [self.delegate localDeleteButtonClicked:localMemeModel];
    }
}
- (UIButton *)downloadButton{
    if (_downloadButton == nil) {
        _downloadButton = [[UIButton alloc] init];
        [self.contentView addSubview:_downloadButton];
        _downloadButton.backgroundColor = [UIColor clearColor];
        [_downloadButton addTarget:self action:@selector(downloadButtonClicked) forControlEvents:UIControlEventTouchUpInside];
       UIImage * normalImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"meme_downloading@2x.png" ofType:nil] ];
        UIImage * selectedImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"meme_downloaded@2x.png" ofType:nil] ];
        [_downloadButton setImage:normalImage forState:UIControlStateNormal];
        [_downloadButton setImage:selectedImage forState:UIControlStateSelected];
        
    }
    return _downloadButton;
}
- (void)downloadButtonClicked{
    if (self.downloadButton.selected) { // 说明已经下载完成,直接返回
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(downloadButtonOnCellClicked:)]) {
        [self.delegate downloadButtonOnCellClicked:self];
    }
}
- (UIImageView *)backImageView{
    if (_backImageView == nil) {
        _backImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_backImageView];
        _backImageView.backgroundColor = [UIColor whiteColor];
        
    
    }
    _backImageView.userInteractionEnabled = YES;

    return _backImageView;
}
- (UIImageView *)themeView{
    if (_themeView == nil) {
        _themeView = [[UIImageView alloc] init];
        [self.contentView insertSubview:_themeView aboveSubview:self.backImageView];
        _themeView.userInteractionEnabled = YES;
        _themeView.backgroundColor = [UIColor clearColor];
    }
    return _themeView;

}

- (void)setMemeModel:(CKMemeModel *)memeModel
{
    _memeModel = memeModel;

    [self.themeView sd_setImageWithURL:[NSURL URLWithString:memeModel.instanceImageUrl] placeholderImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"meme_loading@2x.png" ofType:nil ]]];
    self.downloadButton.selected = NO;
    if (!memeModel.progress) { // 如果没有下载
        [self.downloadButton setTitle:@"" forState:UIControlStateNormal];
        _loadingIndicator.hidden = YES;

        self.downloadButton.selected = NO;
    }
    else
    {
        self.downloadButton.selected = YES;
        _loadingIndicator.hidden = NO;
        _loadingIndicator.progress = memeModel.progress;
        if (memeModel.progress == 1.0) {
            
            if (memeModel.isDownloaded) {
                _loadingIndicator.hidden = YES;
                memeModel.downloaded = YES;

            }
            else
            {
                memeModel.downloaded = YES;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    _loadingIndicator.hidden = YES;

                });
            }
 
        }
}
    
}
#define MEME_SAVE_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS] URLByAppendingPathComponent:@"Meme"]

- (void)setLocalMemeModel:(CKLocalMemeModel *)localMemeModel
{
    _localMemeModel = localMemeModel;
    self.cellType = CKCellTypeLocal;

    if (!localMemeModel.isOnline) { // 本地图片
        NSURL * fileURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:localMemeModel.localImageName ofType:nil]];
        [self.themeView sd_setImageWithURL:fileURL];
        self.deleteButton.selected = NO;
    }
    else
    {
        NSString * imagePath = [NSString stringWithFormat:@"%@/%@",MEME_SAVE_URL.path,localMemeModel.localImageName];
        NSURL * fileURL = [NSURL fileURLWithPath:imagePath];
        [self.themeView sd_setImageWithURL:fileURL];
        self.deleteButton.selected = localMemeModel.selected;
    }
}
- (void)setCellType:(CKCellType)cellType
{
    _cellType = cellType;
    if (cellType == CKCellTypeLocal) {
        self.meme_coverView.hidden = YES;
        self.downloadButton.hidden = YES;
        _loadingIndicator.hidden = YES;
    }
}
- (void)dealloc
{

    self.downloadButton.selected = NO;
    _loadingIndicator.hidden = YES;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.backImageView.frame = self.contentView.frame;
    self.themeView.frame = self.contentView.bounds;
    
    self.downloadButton.frame = CGRectMake(self.contentView.frame.size.width - 40 , self.contentView.frame.size.height - 40 ,40, 40);
    self.meme_coverView.frame = CGRectMake(0, self.contentView.frame.size.height - 40, self.frame.size.width, 40);
    
    self.deleteButton.frame = CGRectMake(self.frame.size.width - 30, 0, 30, 30);
    

        _loadingIndicator.frame = CGRectMake(floorf((self.contentView.bounds.size.width - _loadingIndicator.frame.size.width) / 2.),
                                             floorf((self.contentView.bounds.size.height - _loadingIndicator.frame.size.height) / 2),
                                             _loadingIndicator.frame.size.width,
                                             _loadingIndicator.frame.size.height);
    [self.contentView bringSubviewToFront:self.meme_coverView];
    [self.contentView bringSubviewToFront:self.downloadButton];
    [self.contentView bringSubviewToFront:_loadingIndicator];

}


@end

