//
//  CKMemeButton.m
//  MJExtensionDemo
//
//  Created by 张赛 on 15/6/2.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import "CKMemeButton.h"
@implementation CKMemeButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // 1.设置文字的颜色
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        // 3.设置图片不拉伸
        self.imageView.contentMode = UIViewContentModeCenter;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        // 让系统高亮状态不调整图片的样式
        self.adjustsImageWhenHighlighted = NO;
    }
    return self;
}

// 标题的位置
- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleY = self.frame.size.height * .7;
    CGFloat titleX = 0;
    // 整个按钮的宽度 - 图片的宽度
    CGFloat titleW = self.frame.size.width;
    CGFloat titleH = self.frame.size.height * .3;
    return CGRectMake(titleX, titleY, titleW, titleH);
    
}
// 图标的位置
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageY = 0;
    CGFloat imageW = self.frame.size.width;
    CGFloat imageX = 0;
    CGFloat imageH = self.frame.size.height * .7;
    return CGRectMake(imageX, imageY, imageW, imageH);
}

@end
