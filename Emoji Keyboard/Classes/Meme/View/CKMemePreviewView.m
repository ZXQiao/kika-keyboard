//
//  CKMemePreviewView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/4.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMemePreviewView.h"
#import "Colours.h"
@interface CKMemePreviewView()

@property (nonatomic, strong) UIImageView * memeImageView;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, strong) UIButton * shareWordButton;

@end

#define DOWNLOAD_MEME_IMG_PATH [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"Meme/OnlineDownloadImage"]

@implementation CKMemePreviewView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        UIImageView * memeImageView = [[UIImageView alloc] init];
        [self addSubview:memeImageView];
        self.memeImageView = memeImageView;
        
        UIButton * shareButton = [[UIButton alloc] init];
        [self addSubview:shareButton];
        self.shareButton = shareButton;
        
        [shareButton addTarget:self action:@selector(shareMeme) forControlEvents:UIControlEventTouchUpInside];
        UIImage * shareImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"meme_share_big@2x.png" ofType:nil] ];
        [shareButton setImage:shareImage forState:UIControlStateNormal];
        UIButton * shareLabel = [[UIButton alloc] init];
        [shareLabel setTitleColor:[UIColor colorFromHexString:@"#38e4a9"] forState:UIControlStateNormal];
        [shareLabel setTitle:@"Share" forState:UIControlStateNormal];
        shareLabel.titleLabel.font = [UIFont systemFontOfSize:18];
        [self addSubview:shareLabel];
        self.shareWordButton = shareLabel;
        [self.shareWordButton addTarget:self action:@selector(shareMeme) forControlEvents:UIControlEventTouchUpInside];

        
    }
    return self;
}

- (void)shareMeme
{
//    CKLog(@"什么情况");
}
- (void)setImageName:(NSString *)imageName
{
    _imageName = imageName;
    UIImage * memeImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",DOWNLOAD_MEME_IMG_PATH,imageName]];
    self.memeImageView.image = memeImage;
    CKLog(@"%@",memeImage);
    CGSize size = memeImage.size;
    CGFloat w = [UIScreen mainScreen].bounds.size.width;
    CGFloat ratio = size.height / size.width;
    CGFloat h = w * ratio;
    self.memeImageView.frame = CGRectMake(0, 0, w, h);
    self.memeImageView.center = self.center;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat iPhone6W = 53;
    CGFloat iPhone6H = iPhone6W / 0.6;
    CGFloat bottomiPhone6Padding = 69;
    CGFloat ratio = [UIScreen mainScreen].bounds.size.width / 375 ;
    
    CGFloat buttonW = ratio * iPhone6W;
    CGFloat buttonH = ratio * iPhone6H;
    CGFloat bottomPadding = ratio * bottomiPhone6Padding;
    self.shareButton.frame = CGRectMake(0, self.frame.size.height - bottomPadding - 20 - buttonH, buttonW, buttonH);
    self.shareButton.center = CGPointMake(self.center.x, self.shareButton.center.y);
    self.shareWordButton.frame = CGRectMake(0, CGRectGetMaxY(self.shareButton.frame), 100, 20);
    self.shareWordButton.center = CGPointMake(self.shareButton.center.x, self.shareWordButton.center.y);
    self.memeImageView.center = CGPointMake(self.center.x, self.center.y - buttonH - 20);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }
    if ([self.delegate respondsToSelector:@selector(memePreviewShouldRemoveFromSuperView:)]) {
        [self.delegate memePreviewShouldRemoveFromSuperView:self];
    }
}

@end
