//
//  CKMemeTitleView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/3.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMemeTitleView.h"
#import "Colours.h"
@interface CKMemeTitleView()
@property (nonatomic, strong) UIButton * localButton;
@property (nonatomic, strong) UIButton * onlineButton;
@property (nonatomic, strong) UILabel * bottomLine;
@property (nonatomic, strong) UIButton * selectedButton;


@end
@implementation CKMemeTitleView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        UIButton * localButton = [[UIButton alloc] init];
        [self addSubview:localButton];
        self.localButton = localButton;
        localButton.tag = 0;
        [localButton setTitle:@"Local" forState:UIControlStateNormal];
        [localButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [localButton setTitleColor:[UIColor colorFromHexString:@"#006ab4"] forState:UIControlStateNormal];
        [localButton addTarget:self action:@selector(memeTitleViewButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        localButton.titleLabel.font = [UIFont systemFontOfSize:20];
        localButton.adjustsImageWhenHighlighted = NO;
        
        UIButton * onlineButton = [[UIButton alloc] init];
        [self addSubview:onlineButton];
        self.onlineButton = onlineButton;
        [onlineButton setTitle:@"Online" forState:UIControlStateNormal];
        onlineButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [onlineButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [onlineButton setTitleColor:[UIColor colorFromHexString:@"#006ab4"] forState:UIControlStateNormal];
        [onlineButton addTarget:self action:@selector(memeTitleViewButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        onlineButton.tag = 1;
        onlineButton.adjustsImageWhenHighlighted = NO;
        
        UILabel * bottomLine = [[UILabel alloc] init];
        [self addSubview:bottomLine];
        self.bottomLine = bottomLine;
        bottomLine.backgroundColor = [UIColor whiteColor];
        self.bottomLine.frame = CGRectMake(0, self.frame.size.height - 2, self.frame.size.width / 2, 2);
        
//        UILabel * labelColor = [[UILabel alloc] init];
//        [bottomLine addSubview:labelColor];
//        self.lableColor = labelColor;
//        bottomLine.backgroundColor = [UIColor whiteColor];
//        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.localButton.frame = CGRectMake(0, 0, self.frame.size.width / 2, self.frame.size.height);
    self.onlineButton.frame = CGRectMake(self.frame.size.width / 2, 0, self.frame.size.width / 2, self.frame.size.height);
    self.bottomLine.frame = CGRectMake(0, self.frame.size.height - 2, self.frame.size.width / 2, 2);
    
}
- (void)memeTitleViewButtonClicked:(UIButton *)button
{
    if ([self.delegate respondsToSelector:@selector(memeTitleButtonClickedToChangeLocalOrOnline:)]) {
        [self.delegate memeTitleButtonClickedToChangeLocalOrOnline:button];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CKMemeTitleViewHasBeenClicked" object:@(button.tag)];
    }
    if ([self.delegate respondsToSelector:@selector(themeButtonClickedToChangeLocalOrOnline:)]) {
        [self.delegate themeButtonClickedToChangeLocalOrOnline:button];
    }
}

- (void)setSelectedIndex:(int)selectedIndex
{
    self.selectedButton.selected  = NO;
    
    switch (selectedIndex) {
        case 0:
        {
            [UIView animateWithDuration:.3 animations:^{
                self.bottomLine.frame = CGRectMake(0, self.frame.size.height - 2, self.frame.size.width / 2, 2);
                self.localButton.selected = YES;
                self.selectedButton = self.localButton;
            }];
        }
            break;
            
        case 1:
        {
                        [UIView animateWithDuration:.3 animations:^{
                self.bottomLine.frame = CGRectMake(self.frame.size.width / 2, self.frame.size.height - 2, self.frame.size.width / 2, 2);
                            self.onlineButton.selected = YES;
                            self.selectedButton = self.onlineButton;

            }];
        }
            break;
        default:
            break;
    }
}

@end
