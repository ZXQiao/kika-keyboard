//
//  CKThemeCollectionCell.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/14.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKMemeModel.h"

// cell类型
typedef NS_ENUM(int, CKCellType) {
    CKCellTypeLocal = 0, // 本地
    CKCellTypeOnline, // online
};
@class CKMemeCollectionCell,CKLocalMemeModel;


@protocol CKMemeCollectionCellDelegate <NSObject>
@optional
- (void)downloadButtonOnCellClicked:(CKMemeCollectionCell *)cell;

- (void)localDeleteButtonClicked:(CKLocalMemeModel *)localMemeModel;

@end

@interface CKMemeCollectionCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView * themeView;
@property (nonatomic, strong) CKMemeModel * memeModel;
@property (nonatomic, assign) CKCellType  cellType;
@property (nonatomic, weak) id <CKMemeCollectionCellDelegate> delegate;
@property (nonatomic, strong) UIButton * deleteButton;
@property (nonatomic, strong) CKLocalMemeModel * localMemeModel;



@end
