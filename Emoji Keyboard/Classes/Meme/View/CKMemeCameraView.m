//
//  CKMemeCameraView.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/5.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMemeCameraView.h"
@interface CKMemeCameraView()
@property (nonatomic, strong) UIImageView * topImageView;
@property (nonatomic, strong) UIButton * cameraButton; // 相机
@property (nonatomic, strong) UIButton * albumButton; // 相册
@property (nonatomic, strong) UIButton * cancleButton;
@end
@implementation CKMemeCameraView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        UIImageView * topImageView = [[UIImageView alloc] init];
        [self addSubview:topImageView];
        self.topImageView = topImageView;
        topImageView.userInteractionEnabled = YES;
        topImageView.backgroundColor = [UIColor whiteColor];
        
        UIButton * cameraButton = [[UIButton alloc] init];
        [self addSubview:cameraButton];
        self.cameraButton = cameraButton;
        cameraButton.tag = 0;
        [cameraButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cameraButton setImage:[UIImage imageWithBundlePath:@"meme_toChoseCamera@2x" imageType:nil] forState:UIControlStateNormal];
        [cameraButton setImage:[UIImage imageWithBundlePath:@"meme_toChoseCamera_pressed@2x" imageType:nil] forState:UIControlStateHighlighted];
        cameraButton.backgroundColor = [UIColor clearColor];
        
        UIButton * albumButton = [[UIButton alloc] init];
        [self addSubview:albumButton];
        self.albumButton = albumButton;
        [albumButton setImage:[UIImage imageWithBundlePath:@"meme_photo@2x" imageType:nil] forState:UIControlStateNormal];
        [albumButton setImage:[UIImage imageWithBundlePath:@"meme_photo_pressed@2x" imageType:nil] forState:UIControlStateHighlighted];
        [albumButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        albumButton.tag = 1;
        albumButton.backgroundColor = [UIColor clearColor];
        
        UIButton * cancelButton = [[UIButton alloc] init];
        [self addSubview:cancelButton];
        self.cancleButton = cancelButton;
        [cancelButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cancelButton.tag = 2;
        cancelButton.backgroundColor = [UIColor whiteColor];
        [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor colorFromHexString:@"#7cc8fd"] forState:UIControlStateNormal];
        
        
    }
    return self;
}

- (void)buttonClicked:(UIButton *)button
{
    if ([self.delegate respondsToSelector:@selector(cameraViewButtonClicked:)]) {
        [self.delegate cameraViewButtonClicked:button];
    }
}

- (void)layoutSubviews{
    CGFloat paddingiPhone6 = 98;
    CGFloat padding = paddingiPhone6 / 375 * self.frame.size.width;
    CGFloat buttonWH = 40;
    self.topImageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 54);
    self.cameraButton.frame = CGRectMake(padding, 0, buttonWH, buttonWH);
    self.albumButton.frame = CGRectMake(self.frame.size.width - padding - buttonWH, 0, buttonWH, buttonWH);
    self.cameraButton.center = CGPointMake(self.cameraButton.center.x,self.topImageView.center.y);
    self.albumButton.center = CGPointMake(self.albumButton.center.x,self.topImageView.center.y);
    self.cancleButton.frame = CGRectMake(0, self.frame.size.height - 44, self.frame.size.width, 44);
    
}

@end
