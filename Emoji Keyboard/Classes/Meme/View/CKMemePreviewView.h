//
//  CKMemePreviewView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/4.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CKMemePreviewView;
@protocol CKMemePreviewViewDelegate<NSObject>
@optional
- (void)memePreviewShouldRemoveFromSuperView:(CKMemePreviewView *)preview;
@end

@interface CKMemePreviewView : UIView
@property (nonatomic, strong) NSString * imageName;
@property (nonatomic, weak) id<CKMemePreviewViewDelegate> delegate;
@end
