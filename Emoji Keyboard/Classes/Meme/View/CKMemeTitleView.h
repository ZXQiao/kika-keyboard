//
//  CKMemeTitleView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/3.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CKMemeTitleViewDelegate <NSObject>

@optional
- (void)memeTitleButtonClickedToChangeLocalOrOnline:(UIButton *)button;
- (void)themeButtonClickedToChangeLocalOrOnline:(UIButton *)button;


@end

@interface CKMemeTitleView : UIView

@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, weak) id <CKMemeTitleViewDelegate> delegate;

@end
