//
//  CKMemeCameraView.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/5.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CKMemeCameraViewDelegate <NSObject>

- (void)cameraViewButtonClicked:(UIButton *)button;

@end

@interface CKMemeCameraView : UIView
@property (nonatomic, weak) id<CKMemeCameraViewDelegate> delegate;

@end
