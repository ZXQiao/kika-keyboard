//
//  CKMemeTextTableViewCell.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/12.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKMemeTextTableViewCell.h"
#import "CKShadowLable.h"

@interface CKMemeTextTableViewCell()
@property (nonatomic, strong) UIImageView * backImageView;



@end

@implementation CKMemeTextTableViewCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        UIImageView * backImageView = [[UIImageView alloc] init];
        self.backImageView = backImageView;
        backImageView.image = [UIImage resizableImageNamed:@"meme_cell_back"];
        [self.contentView addSubview:backImageView];
        
        CKShadowLable * topLabel = [[CKShadowLable alloc] init];
        self.topLabel = topLabel;
        topLabel.text = @"";
        
        [backImageView addSubview:topLabel];
        topLabel.font = [UIFont fontWithName:MEME_FONT_NAME size:20];
        topLabel.adjustsFontSizeToFitWidth = YES;
        topLabel.textColor = [UIColor whiteColor];
//        topLabel.backgroundColor = [UIColor purpleColor];
        
        
        CKShadowLable * bottomLabel = [[CKShadowLable alloc] init];
        self.bottomLabel = bottomLabel;
        [backImageView addSubview:bottomLabel];
        bottomLabel.font = [UIFont fontWithName:MEME_FONT_NAME size:20];
        bottomLabel.adjustsFontSizeToFitWidth = YES;
        bottomLabel.textColor = [UIColor whiteColor];
//        bottomLabel.
//        bottomLabel.backgroundColor = [UIColor pinkColor];
        
        
        
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.backImageView.frame = CGRectMake(10, 10, self.contentView.frame.size.width - 10 * 2, self.contentView.frame.size.height - 10);

    self.topLabel.frame = CGRectMake(10, 0, self.backImageView.frame.size.width - 20 , self.backImageView.frame.size.height / 2);
    self.bottomLabel.frame = CGRectMake(10, self.backImageView.frame.size.height / 2 - 5, self.backImageView.frame.size.width - 10 * 2, self.backImageView.frame.size.height / 2);
}

@end
