//
//  CKMemeDownloadTool.m
//  07-URLConnection下载
//
//  Created by 张赛 on 14-7-1.
//  Copyright (c) 2014年 zhangsai. All rights reserved.
//

#import "CKMemeDownloadTool.h"

typedef void(^ProgressBlock)(float percent);

@interface CKMemeDownloadTool() <NSURLConnectionDataDelegate>

@property (nonatomic, strong) NSMutableData *dataM;

// 保存在沙盒中的文件路径
@property (nonatomic, strong) NSString *cachePath;
// 文件总长度
@property (nonatomic, assign) long long fileLength;
// 当前下载的文件长度
@property (nonatomic, assign) long long currentLength;

// 回调块代码
@property (nonatomic, copy) ProgressBlock progress;

@end

@implementation CKMemeDownloadTool

- (NSMutableData *)dataM
{
    if (!_dataM) {
        _dataM = [NSMutableData data];
    }
    return _dataM;
}

- (void)downloadWithURL:(NSURL *)url progress:(void (^)(float))progress
{
    // 0. 记录块代码
    self.progress = progress;
    
    // 1. request GET
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // 2. connection
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    // 让connection支持多线程，指定代理的工作队列即可
    // NSURLConnection在运行时，运行循环不负责监听代理的具体执行
    [connection setDelegateQueue:[[NSOperationQueue alloc] init]];
    
    // 3. 启动连接
    [connection start];
}

#pragma mark - 代理方法
// 1. 接收到服务器的响应，服务器执行完请求，向客户端回传数据
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
//    CKLog(@"%@ %lld", response.suggestedFilename, response.expectedContentLength);
    // 1. 保存的缓存路径
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString * folderPath = [cachePath stringByAppendingPathComponent:@"Meme"];
    NSFileManager * fileMgr = [NSFileManager defaultManager];
    NSError * error;
    if (![fileMgr fileExistsAtPath:folderPath]) {
        [fileMgr createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    NSString * imagePath = [folderPath stringByAppendingPathComponent:@"OnlineDownloadImage"];
    if (![fileMgr fileExistsAtPath:imagePath]) {
        [fileMgr createDirectoryAtPath:imagePath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    self.cachePath = [imagePath stringByAppendingPathComponent:response.suggestedFilename];
    CKLog(@"%@",self.cachePath);
    // 2. 文件总长度
    self.fileLength = response.expectedContentLength;
    // 3. 当前下载的文件长度
    self.currentLength = 0;
    
    // 清空数据
    [self.dataM setData:nil];
}

// 2. 接收数据，从服务器接收到数据
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // 拼接数据
    [self.dataM appendData:data];
    
    // 根据data的长度增加当前下载的文件长度
    self.currentLength += data.length;
    
    float progress = (float)self.currentLength / self.fileLength;
    
    // 判断是否定义了块代码
    if (self.progress) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            // 强制运行循环执行一次更新
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate date]];
            
            self.progress(progress);
            

        }];
    }
}

// 3. 完成接收
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
//    CKLog(@"%s %@", __func__, [NSThread currentThread]);
    // 将dataM写入沙盒的缓存目录
    // 写入数据，NSURLConnection底层实现是用磁盘做的缓存
    [self.dataM writeToFile:self.cachePath atomically:YES];
    if ([self.delegate respondsToSelector:@selector(dowloadDoneWithFinalIamgePath:)]) {
        [self.delegate dowloadDoneWithFinalIamgePath:self.cachePath];
    }
//    CKLog(@"%@=====cachePath",self.cachePath);
}

// 4. 出现错误
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    CKLog(@"%@", error.localizedDescription);
}

@end
