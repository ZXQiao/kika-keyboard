//
//  CKMemeDataManager.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/11.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define MEME_SHARED_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS] URLByAppendingPathComponent:@"Library/Meme"]
//#define MEME_SHARED_PLIST_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS] URLByAppendingPathComponent:@"Library/Meme/memeshred.plist"]
//
//#define DOWNLOAD_MEME_IMG_PATH [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"Meme/OnlineDownloadImage"]
//
//#define DOWNLOAD_MEME_PLIST_PATH [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject]stringByAppendingPathComponent:@"Meme/MemeResource/onlineDownloadMeme.plist"]

#define MEME_SAVE_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS] URLByAppendingPathComponent:@"Meme"]
#define MEME_DOWLOADED_PLIST_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS] URLByAppendingPathComponent:@"onlineDownloadMeme.plist"]


@interface CKMemeDataManager : NSObject

+(void)saveMemeToShare;

@end
