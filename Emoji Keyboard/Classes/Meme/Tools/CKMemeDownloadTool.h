//
//  CZDownload.h
//  07-URLConnection下载
//
//  Created by LNJ on 14-7-1.
//  Copyright (c) 2014年 zhangsai. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol CKMemeDownloadDelegate<NSObject>

- (void)dowloadDoneWithFinalIamgePath:(NSString *)imagePath;
@end

@interface CKMemeDownloadTool : NSObject

- (void)downloadWithURL:(NSURL *)url progress:(void (^)(float percent))progress;
@property (nonatomic,weak) id<CKMemeDownloadDelegate> delegate;
@end
