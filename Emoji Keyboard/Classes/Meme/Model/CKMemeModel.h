//
//  CKMemeModel.h
//  MJExtensionDemo
//
//  Created by 张赛 on 15/5/27.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKMemeModel : NSObject

@property (nonatomic, copy) NSString *generatorID;
@property (nonatomic, copy) NSString *displayName;
@property (nonatomic, copy) NSString *urlName;
@property (nonatomic, copy) NSString *totalVotesScore;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *instanceID;
@property (nonatomic, copy) NSString *text0;
@property (nonatomic, copy) NSString *text1;
@property (nonatomic, copy) NSString *instanceImageUrl;
@property (nonatomic, copy) NSString *instanceUrl;

@property (nonatomic,assign,getter=isDownloaded) BOOL downloaded;
@property (nonatomic, assign) int index;
@property (nonatomic, assign) float progress;
@property (nonatomic, strong) NSString * imageName;



@end
