//
//  CKLocalMemeModel.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/10.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKLocalMemeModel : NSObject
@property (nonatomic, strong) NSString * localImageName;
@property (nonatomic, assign) BOOL selected;
@property (nonatomic, assign) int localIndex;

@property (nonatomic,assign,getter=isOnline) BOOL online;

@end
