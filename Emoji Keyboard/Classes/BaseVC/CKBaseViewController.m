//
//  CKBaseViewControlle.m
//  Flash Keyboard
//
//  Created by 张赛 on 16/2/16.
//  Copyright © 2016年 ANXIANGZI. All rights reserved.
//

#import "CKBaseViewController.h"
#import "CKTracker.h"
#import "CKTracker+GA.h"

@interface CKBaseViewController ()

@end

@implementation CKBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[CKTracker sharedTraker] enterView:[self currentVCName]];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[CKTracker sharedTraker] leaveView:[self currentVCName]];
}


- (NSString*)currentVCName{
    CKLog(@"CurrentVCName----%@",NSStringFromClass(self.class));
    
    return NSStringFromClass(self.class);
}

@end
