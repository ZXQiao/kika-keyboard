//
//  LanguageTableViewController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 14/11/11.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "LanguageTableViewController.h"
#import "LanguageCell.h"
#import "Consetting.h"

@interface LanguageTableViewController ()

@property (nonatomic, strong) NSMutableArray * languageArrayM;


@property (nonatomic, assign) int selectedNum;
@end

@implementation LanguageTableViewController

- (NSMutableArray *)languageArrayM
{
    if (_languageArrayM == nil) {
        _languageArrayM = [NSMutableArray array];
        NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"languageSetting.txt" ofType:nil];
        NSError * error;
        NSString * languageSetting = [NSString stringWithContentsOfFile:plistPath encoding:NSUTF8StringEncoding error:&error];
        NSArray * tempArray = [languageSetting componentsSeparatedByString:@","];
        _languageArrayM = (NSMutableArray *)tempArray;
        
    }
    return _languageArrayM;
}



- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    self.view.backgroundColor = [UIColor colorWithRed:250.0 / 255 green:250.0 / 255 blue:250.0 / 255 alpha:1];
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Language";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.contentSize = CGSizeMake(0, 1000);
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int LanguageIndex = (int)[defaults integerForKey:@"LanguageIndex"];
    self.selectedNum = LanguageIndex;
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:LanguageIndex inSection:0];
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    return self.languageArrayM.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    LanguageCell * cell = [LanguageCell cellWithTabelView:tableView];
    
    cell.languageName = self.languageArrayM[indexPath.row];
    
    
    
    if (self.selectedNum == indexPath.row) {
        cell.selectedView.image = [UIImage imageNamed:@"cellSelected"];
    }
    else
    {
        cell.selectedView.image = [UIImage imageNamed:@"languageBack"];
    }
    return cell;

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedNum = (int)indexPath.row;
    NSString * languageName = self.languageArrayM[indexPath.row];
    [CKSaveTools saveString:languageName forKey:@"Language" inRegion:CKSaveRegionAppGroup];
    [CKSaveTools saveInteger:indexPath.row  forKey:@"LanguageIndex" inRegion:CKSaveRegionContainingApp];
    [self.tableView reloadData];
}

@end
