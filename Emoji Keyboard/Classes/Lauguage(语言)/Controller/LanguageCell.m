//
//  LanguageCell.m
//  Emoji Keyboard
//
//  Created by 张赛 on 14/11/24.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "LanguageCell.h"


@interface LanguageCell ()
// 国旗图标
@property (nonatomic, strong) UIImageView * countryImageView;

// 语言字符串
@property (nonatomic, strong) UILabel * languageLabel;







@end

@implementation LanguageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+ (instancetype)cellWithTabelView:(UITableView *)tableView
{
    // 1.定义一个标识
    static NSString *ID = @"Language";
    
    // 2.去缓存池中取出可循环利用的cell
    LanguageCell * cell = (LanguageCell *)[tableView dequeueReusableCellWithIdentifier:ID];
    if (cell != nil) {
        cell.languageChose = NO;
    }
    else
    {
        cell = [[LanguageCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 0.清空cell的背景颜色
        self.backgroundColor = [UIColor clearColor];
        
        self.languageChose = NO;
        [self setupView];

    }
    return self;
}

- (void)setupView
{
    UIImageView * countryImageView = [[UIImageView alloc] init];
    self.countryImageView = countryImageView;
    [self.contentView addSubview:countryImageView];
    
    UILabel * languageLabel = [[UILabel alloc] init];
    self.languageLabel = languageLabel;
    [self.contentView addSubview:languageLabel];
    
    UIImageView * selectedView = [[UIImageView alloc] init];
    self.selectedView = selectedView;
    [self.contentView addSubview:selectedView];

    self.selectedView.image = [UIImage imageNamed:@"languageBack"];

    
    
    
}

- (void)layoutSubviews
{
    CGFloat boarderMargin = 25;
    CGFloat midMargin = 15;
    CGFloat topMargin = 10;
    CGFloat countryImageViewX = boarderMargin;
    CGFloat countryImageViewY = topMargin;
    CGFloat countryImageViewH = self.contentView.frame.size.height - topMargin;
    CGFloat countryImageViewW = countryImageViewH  / 104 * 90;
    self.countryImageView.frame = CGRectMake(countryImageViewX, countryImageViewY, countryImageViewW, countryImageViewH);
    
    CGFloat selectedButtonW = 20;
    
    CGFloat languageLabelX = CGRectGetMaxX(self.countryImageView.frame) + midMargin;
    CGFloat languageLabelW = self.frame.size.width - self.countryImageView.frame.size.width - selectedButtonW - 2 * midMargin - 2 * boarderMargin;
    CGFloat languageLabelH = selectedButtonW;
    self.languageLabel.frame = CGRectMake(languageLabelX, 0, languageLabelW, languageLabelH);
    self.languageLabel.center = CGPointMake(self.languageLabel.center.x, self.contentView.center.y);
    
    CGFloat selectedViewX = CGRectGetMaxX(self.languageLabel.frame) + midMargin ;
    CGFloat selectedViewY = self.languageLabel.frame.origin.y;
    CGFloat selectedViewW = selectedButtonW;
    CGFloat selectedViewH = selectedButtonW;
    self.selectedView.frame = CGRectMake(selectedViewX, selectedViewY, selectedViewW, selectedViewH);
}
    
- (void)setLanguageName:(NSString *)languageName
{
    _languageName = languageName;
    UIImage * image = [UIImage imageNamed:languageName.lowercaseString];
    self.countryImageView.image = image;
    self.languageLabel.text = languageName;
}

@end
