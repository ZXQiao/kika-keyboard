//
//  LanguageCell.h
//  Emoji Keyboard
//
//  Created by 张赛 on 14/11/24.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageModel.h"
@interface LanguageCell : UITableViewCell

+ (instancetype)cellWithTabelView:(UITableView *)tableView;

@property (nonatomic,strong) NSString * languageName;

@property (nonatomic, strong) LanguageModel * languageModel;

// 选中图标
@property (nonatomic, strong) UIImageView * selectedView;

// 是否选中
@property (nonatomic, assign,getter = isLanguageChose) BOOL languageChose;

@end
