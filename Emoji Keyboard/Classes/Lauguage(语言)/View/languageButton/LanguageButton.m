

#import "LanguageButton.h"



@implementation LanguageButton

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // 1.设置图片居中
//        self.imageView.contentMode = UIViewContentModeCenter;
        // 2.设置文字居中
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        // 3.设置文字大小
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        
        
        
    }
    return self;
}

// 控制按钮上图片显示的位置
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageX = 0;
    CGFloat imageY = 0;
    CGFloat imageW = self.frame.size.width;
    
    // 让图片的高度是整个按钮高度的60%
    CGFloat imageH = self.frame.size.height / 151 * 104;
//    CGFloat imageH = self.frame.size.height * .6;
    return CGRectMake(imageX, imageY, imageW, imageH);
}

// 控制按钮上标题显示的位置
- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleX = 0;
    CGFloat titleY = self.frame.size.height / 151 * (104 + 21);
//        CGFloat titleY = self.frame.size.height * .6;
    CGFloat titleW = self.frame.size.width;
    CGFloat titleH = self.frame.size.height - titleY;
    
    return CGRectMake(titleX, titleY, titleW, titleH);
    
}


@end
