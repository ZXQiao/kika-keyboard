//
//  CKGuideView.h
//  Emoji Keyboard
//
//  Created by xinmeihutong on 15/11/4.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CKGuideView;
typedef NS_ENUM(NSInteger,CKGuideType){//定义弹框的类型
    CKGuideTypeRate=1,//评分弹框
    CkGuideTypeShare,//分享弹框
    CKGuideTypeRateFromKeyBoard//从键盘弹出的评分引导
};

typedef NS_ENUM(NSInteger,CKClickType) {//定义点击的按钮类型
    CKClickTypeCancel=1,//退出
    CKClickTypeShare,//分享
    CKClickTypeRate//评分
};

typedef NS_ENUM(NSInteger,CKAppType) {//调用弹框的App
    CKAppTypeContainingApp=1,//主app
    CKAppTypeExtensionApp//扩展
};

//回调点击的类型
@protocol CKGuideViewDelegate <NSObject>

-(void)CKGuideView:(CKGuideView*)guideView clickBtnType:(CKClickType)clickType;

@end

@interface CKGuideView : UIView

@property (nonatomic,assign)id<CKGuideViewDelegate>delegate;

-(instancetype)initWithAppType:(CKAppType)appType guideType:(CKGuideType)guide fram:(CGRect)fram;

@end
