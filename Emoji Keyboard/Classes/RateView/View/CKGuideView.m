//
//  CKGuideView.m
//  Emoji Keyboard
//
//  Created by xinmeihutong on 15/11/4.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKGuideView.h"

@interface CKGuideView ()
{
    CKGuideType _guideType;
    CKAppType _appType;
    CGRect viewFram;
}
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImg;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftBtnBottomConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftLead;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rigtLead;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightTrail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *XBtnTop;

@end


@implementation CKGuideView


-(instancetype)initWithAppType:(CKAppType)appType guideType:(CKGuideType)guide fram:(CGRect)fram
{
    self = [self initWithFrame:fram];
    _guideType = guide;
    _appType = appType;
    [self.leftButton setTitleColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1] forState:UIControlStateNormal];
    if(appType==CKAppTypeExtensionApp){//键盘调用
        if(guide == CKGuideTypeRate){//coolFont评分引导设置背景
            [self setBackGroundImage:@"coolfont_unlock.png" andLeftBtnTitle:@"Later" RightBtnTitle:@"Review Now"];
        }else if(guide == CKGuideTypeRateFromKeyBoard){//键盘弹出评分引导
            [self setBackGroundImage:@"coolfont_bg_more.png" andLeftBtnTitle:@"Later" RightBtnTitle:@"Review Now"];
        }else if(guide == CkGuideTypeShare){//分享引导
            [self setBackGroundImage:@"coolfont_bg_share.png" andLeftBtnTitle:@"Later" RightBtnTitle:@"Share"];
        }
    }else if(appType == CKAppTypeContainingApp){//主app调用设置背景及按钮
        [self setBackGroundImage:@"coolfont_bg_app.png"andLeftBtnTitle:@"Later"
                   RightBtnTitle:@"Review Now"];
        [self.leftButton setBackgroundImage:
         [UIImage resizableImageNamed:@"later_app.png"]
                                   forState:UIControlStateNormal];
        [self.rightButton setBackgroundImage:[UIImage resizableImageNamed:@"rate_app.png"]
                                    forState:UIControlStateNormal];
    }
    return self;
}

-(void)setBackGroundImage:(NSString *)imageName andLeftBtnTitle:(NSString *)leftTitel RightBtnTitle:(NSString *)rightTitle{
    self.backGroundImg.image=[UIImage imageNamed:imageName];
    [self.leftButton setTitle:leftTitel forState:UIControlStateNormal];
    [self.rightButton setTitle:rightTitle forState:UIControlStateNormal];
}
-(void)updateConstraints{
    [super updateConstraints];
    if(_guideType == CKGuideTypeRateFromKeyBoard){
        self.leftBtnBottomConstraints.constant=15;
    }
    if(_appType == CKAppTypeContainingApp){
        //app约束进行刷新
        self.leftLead.constant = self.frame.size.width*0.087;
        self.rigtLead.constant = self.frame.size.width*0.051;
        self.rightTrail.constant = self.frame.size.width*0.087;
        self.leftBtnHeight.constant = self.frame.size.height*0.1373;
        self.XBtnTop.constant = self.frame.size.height*0.140;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self =[super initWithFrame:frame];
    if(self){
        self = [[[NSBundle mainBundle] loadNibNamed:@"CKGuideView" owner:nil options:nil]lastObject];
        self.frame=frame;
        [self setNeedsUpdateConstraints];
    }
    return self;
}



- (IBAction)clickLeftBtn:(id)sender {
    if([self.delegate respondsToSelector:@selector(CKGuideView:clickBtnType:)]){
        [self.delegate CKGuideView:self clickBtnType:CKClickTypeCancel];
    }
}
- (IBAction)rightBtnClick:(id)sender {
    if([self.delegate respondsToSelector:@selector(CKGuideView:clickBtnType:)]){
        if(_guideType == CkGuideTypeShare){
            //分享
            [self.delegate CKGuideView:self clickBtnType:CKClickTypeShare];
        }else if(_guideType == CKGuideTypeRate || _guideType == CKGuideTypeRateFromKeyBoard){
            //评分
            [self.delegate CKGuideView:self clickBtnType:CKClickTypeRate];
        }
    }
}

@end
