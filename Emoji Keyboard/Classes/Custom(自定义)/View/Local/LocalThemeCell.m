//
//  SHTitleCell.m
//  SweetHeart
//
//  Created by 张赛 on 14-7-28.
//  Copyright (c) 2014年 张赛. All rights reserved.
//

#import "LocalThemeCell.h"

@interface LocalThemeCell()

@end
#define beishu 4
@implementation LocalThemeCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        // 背景图
         UIView * backView = [[UIView alloc] init];
          self.backView = backView;
         [self.contentView addSubview:backView];

        // 大图
        UIImageView * themeImageView = [[UIImageView alloc] init];
        self.themeImageView =  themeImageView;
        [self.backView addSubview:themeImageView];
        themeImageView.layer.cornerRadius = 5;
        themeImageView.layer.masksToBounds = YES;
        
        // 名称
        UILabel * themeNameLabel = [[UILabel alloc] init];
        themeNameLabel.text = @"ThemeName";
        self.themeNameLabel = themeNameLabel;
        self.themeNameLabel.textAlignment = NSTextAlignmentCenter;
        self.themeNameLabel.font = [UIFont systemFontOfSize:12];
        [self.themeImageView addSubview:themeNameLabel];
        themeNameLabel.textColor = [UIColor whiteColor];
        self.themeNameLabel.numberOfLines = 0;
        
        // 选中按钮
        UIButton * selectButton = [[UIButton alloc] init];
        self.selectButton = selectButton;
        [self.themeImageView addSubview:selectButton];
        self.backgroundColor = COMMON_VIEW_BACK_COLOR;
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];

    
    CGFloat topPadding = 10;
    CGFloat leftPadding = self.frame.size.width * 1/ 20;

    CGFloat backViewW = self.frame.size.width - 2 * leftPadding;
    CGFloat backViewH = backViewW / 690 * 182;
    CGFloat backViewX = leftPadding;
    CGFloat backViewY = topPadding;
        //self.backView.backgroundColor = [UIColor purpleColor];
    self.backView.frame = CGRectMake(backViewX, backViewY, backViewW, backViewH);
        // CKLog(@"self.frame.size.height = %f",self.backView.frame.size.height);
    // 388 : 280
    CGFloat padding = .5;
    CGFloat themeImageViewH = backViewH - 2 * padding;
    CGFloat themeImageViewW = themeImageViewH / 182 * 690;
    CGFloat themeImageViewX = padding;
    CGFloat themeImageViewY = padding;
    self.themeImageView.frame = CGRectMake(themeImageViewX, themeImageViewY, themeImageViewW, themeImageViewH);
    
    CGFloat themeNameLabelX = self.frame.size.width * .6;
    CGFloat themeNameLabelY = themeImageViewH / 20;
    CGFloat themeNameLabelW = CGRectGetMaxX(self.themeImageView.frame)- themeNameLabelX - padding;
    CGFloat themeNameLabelH = themeImageViewH / 5;
    self.themeNameLabel.frame = CGRectMake(themeNameLabelX, themeNameLabelY, themeNameLabelW, themeNameLabelH);
    
    CGFloat themeIntroduceLabelX = themeNameLabelX - 15;
    CGFloat themeIntroduceLabelY = themeNameLabelY + themeNameLabelH;
    CGFloat themeIntroduceLabelW = themeNameLabelW + 15;
    CGFloat themeIntroduceLabelH = backViewH - CGRectGetMaxY(self.themeNameLabel.frame);
    self.themeIntroduceLabel.frame = CGRectMake(themeIntroduceLabelX, themeIntroduceLabelY, themeIntroduceLabelW, themeIntroduceLabelH);

    CGFloat selectButtonY = padding + themeImageViewH / 3 * 2;
    CGFloat selectButtonH = themeImageViewH / 3;
    CGFloat selectButtonW = selectButtonH;
    CGFloat selectButtonX = CGRectGetMaxX(self.themeImageView.frame) - selectButtonW;

    self.selectButton.frame = CGRectMake(selectButtonX, selectButtonY, selectButtonW, selectButtonH);



}

- (void)awakeFromNib
{

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];


}


@end
