//
//  SHTitleCell.h
//  SweetHeart
//
//  Created by 张赛 on 14-7-28.
//  Copyright (c) 2014年 张赛. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomThemeCell : UITableViewCell


@property (nonatomic,strong) UIView * backView;

@property (nonatomic, strong) UIImageView * themeImageView;

//@property (nonatomic, strong) UILabel * themeNameLabel;
//
//@property (nonatomic, strong) UILabel * themeIntroduceLabel;

@property (nonatomic, strong) UIButton * selectButton;

@property (nonatomic, strong) UIButton * deleteButton;




@end
