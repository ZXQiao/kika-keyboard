//
//  CollectionCell.h
//  Emoji Keyboard
//
//  Created by wangfeng on 15-1-7.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionCell : UICollectionViewCell


@property (nonatomic,strong) UIView * backView;

@property (nonatomic, strong) UIImageView * themeImageView;


@property (nonatomic, strong) UIButton * selectButton;

@property (nonatomic, strong) UIButton * deleteButton;
@end
