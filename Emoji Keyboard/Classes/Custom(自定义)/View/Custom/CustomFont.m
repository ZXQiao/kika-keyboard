//
//  CustomBackGround.m
//  Emoji Keyboard
//
//  Created by 张赛 on 14/12/27.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "CustomFont.h"
#import "Consetting.h"
@interface CustomFont()
@property (nonatomic, strong) NSMutableArray * fontArrayM;
@property (nonatomic, strong) NSMutableArray * fontButtonArrayM;
@property (nonatomic, strong) NSMutableArray * fontNamesArray;

@end

@implementation CustomFont
-(NSMutableArray *)fontNamesArray
{
    if (_fontNamesArray == nil) {
    NSArray* fontArray = @[
                               @"Helvetica Light",
                               @"Helvetica",
                               @"ArchitectsDaughter",
                               @"Carousel",
                               @"Apple Chancery",
                               @"Palatino Linotype",
                               @"Herculanum",
                               @"PlantagenetCherokee",
                               @"Al Nile",
                               @"Apple SD Gothic Neo",
                               @"Arial Hebrew",
                               @"Avenir",
                               @"Avenir Next",
                               @"Bangla Sangam MN"
                               ];

        _fontNamesArray = [NSMutableArray arrayWithArray:fontArray];
    }
    return _fontNamesArray;
}

- (UIScrollView *)fontScrollView
{
    if (_fontScrollView == nil) {
        _fontScrollView = [[UIScrollView alloc] init];
        
        _fontScrollView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
        [self addSubview:_fontScrollView];
        self.fontScrollView.showsHorizontalScrollIndicator = NO;
    }
    return _fontScrollView;
}
// 按钮数组
- (NSMutableArray *)fontButtonArrayM
{
    if (_fontButtonArrayM == nil) {
        
        _fontButtonArrayM = [NSMutableArray array];
        for (int i = 0; i < self.fontNamesArray.count; i ++) {
            UIButton * button = [[UIButton alloc] init];
            button.tag = i ;
            [_fontButtonArrayM addObject:button];
            [self.fontScrollView addSubview:button];
            [button setTitle:@"Aa" forState:UIControlStateNormal];
            [button setTitleColor:[UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor whiteColor];

            NSString * fontName = self.fontNamesArray[i];
            int fontSize = 17;
            if (iPhone6) {
                fontSize = 20;
            }
            if (iPhone6s) {
                fontSize = 20;
            }
            /**
             *  设置选中第一个
             */
            if (0 == i) {
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [button setBackgroundColor:[UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1]];
            }
            button.titleLabel.font = [UIFont fontWithName:fontName size:fontSize];
            button.layer.masksToBounds = YES;
            [button addTarget:self action:@selector(fontButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    return _fontButtonArrayM;
}


- (void)fontButtonClicked:(UIButton *)button
{
    if ([self.customFontDelegate respondsToSelector:@selector(customFontButtonClicked:fontName:)]) {
        [self.customFontDelegate customFontButtonClicked:button fontName:self.fontNamesArray[button.tag]];
    }
    for (UIView* view in self.fontScrollView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton* btn = (UIButton*)view;
            [btn setTitleColor:[UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1] forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor whiteColor]];
        }
    }

    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1]];

}

// 设置frame
- (void)layoutSubviews
{
    CGFloat leftPadding = 10;
    CGFloat btn_btn = 5;
    int buttonCount = (int)self.fontButtonArrayM.count;
    CGFloat buttonW = ([UIScreen mainScreen].bounds.size.width - 2 * leftPadding) / 6;
    CGFloat buttonH = buttonW;
    CGFloat buttonX = 0;
    CGFloat buttonY = 5;
    for (int i = 0; i < self.fontButtonArrayM.count; i ++) {
        buttonX = leftPadding + (buttonW + btn_btn) * i;
        UIButton * button = self.fontButtonArrayM[i];
        button.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
         button.layer.cornerRadius = buttonW  / 2;
    }
    CGFloat fontScrollViewH = buttonH;
    CGFloat fontScrollViewW = [UIScreen mainScreen].bounds.size.width ;
    CGFloat contentSizeW = buttonW * buttonCount + (buttonCount - 1) * btn_btn + leftPadding * 2;
    self.fontScrollView.frame = CGRectMake(0, 0, fontScrollViewW, fontScrollViewH + 10);
    self.fontScrollView.contentSize = CGSizeMake(contentSizeW, 0);

    
}
@end
