//
//  CustomBackGround.h
//  Emoji Keyboard
//
//  Created by 张赛 on 14/12/27.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomBackGroundDelegate <NSObject>

- (void)customBackgroundButtonClicked:(UIButton *)button;

- (void)customBackgroundThemeButtonClicked:(UIButton *)button;


@end
@interface CustomBackGround : UIView

@property (nonatomic, weak) id<CustomBackGroundDelegate> customBackgroundDelegate;


@end
