//
//  CustomBackGround.m
//  Emoji Keyboard
//
//  Created by 张赛 on 14/12/27.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "CustomBackGround.h"

@interface CustomBackGround()

@property (nonatomic, strong) NSMutableArray * customBackArrayM;

@property (nonatomic, strong) NSMutableArray * buttonArrayM;

@property (nonatomic, strong) UIButton * cameraButton;

@property (nonatomic, strong) UIButton * photoButton;

@property (nonatomic, strong) UIView * bottomView;

// 当前选中的按钮
@property (nonatomic, strong) UIButton * currentSelectedButton;


@end

@implementation CustomBackGround

// 按钮数组
- (NSMutableArray *)buttonArrayM
{
    if (_buttonArrayM == nil) {
        
        _buttonArrayM = [NSMutableArray array];
        for (int i = 0; i < 10; i ++) {
            UIButton * button = [[UIButton alloc] init];
            button.tag = i + 1 ;
            [_buttonArrayM addObject:button];
            NSString * imageName = [NSString stringWithFormat:@"bg_%02d",(int)i + 1];
            [button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
            // 设置选中状态的图片
            [button setImage:[UIImage imageNamed:@"bg_select"] forState:UIControlStateSelected];
            [button addTarget:self action:@selector(themeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            if (button.tag == 1) {
                button.selected = YES;
                self.currentSelectedButton = button;
            }
            [self addSubview:button];
        }
        
    }
    return _buttonArrayM;
}
- (void)themeButtonClicked:(UIButton *)button
{
    
    self.currentSelectedButton.selected = NO;
    button.selected = YES;
    self.currentSelectedButton = button;
    
    if ([self.customBackgroundDelegate respondsToSelector:@selector(customBackgroundThemeButtonClicked:)]) {
        [self.customBackgroundDelegate customBackgroundThemeButtonClicked:button];
        
    }
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView * bottomView = [[UIView alloc] init];
        self.bottomView = bottomView;
        bottomView.backgroundColor = [UIColor whiteColor];
        [self addSubview:bottomView];

        UIButton * cameraButton = [[UIButton alloc] init];
        [self addSubview:cameraButton];
        self.cameraButton = cameraButton;
        [self.cameraButton setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
        [self.cameraButton setImage:[UIImage imageNamed:@"camera_pressed"] forState:UIControlStateHighlighted];
        self.cameraButton.tag = 0;
        
        UIButton * photoButton = [[UIButton alloc] init];
        [self addSubview:photoButton];
        self.photoButton = photoButton;
        [self.photoButton setImage:[UIImage imageNamed:@"picture"] forState:UIControlStateNormal];
        [self.photoButton setImage:[UIImage imageNamed:@"picture_pressed"] forState:UIControlStateHighlighted];
        self.photoButton.tag = 1;
        
        [self.cameraButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.photoButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        

    }
    return self;
}


- (void)buttonClicked:(UIButton *)button
{
    if ([self.customBackgroundDelegate respondsToSelector:@selector(customBackgroundButtonClicked:)]) {
        [self.customBackgroundDelegate customBackgroundButtonClicked:button];
    }
}

// 设置frame
- (void)layoutSubviews

{
    CGFloat leftPadding = 37;
    CGFloat btn_btn = 10;
    // 每一行的按钮个数
    int buttonCount = 5;
    CGFloat buttonW = ([UIScreen mainScreen].bounds.size.width - 2 * leftPadding - (buttonCount - 1) * btn_btn) / buttonCount;
    CGFloat buttonH = buttonW;
    CGFloat buttonX = 0;
    CGFloat buttonY = 0;
    CGFloat row = 0;
    CGFloat line = 0;
    for (int i = 0; i < self.buttonArrayM.count; i ++) {
        // 获取行号
        row = i / 5;
        line = i % 5;
        buttonX = leftPadding + (buttonW + btn_btn) * line;
        buttonY = (buttonH + 13) * row;
        UIButton * button = self.buttonArrayM[i];
        button.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
    }
    CGFloat leftPadding2 = ([UIScreen mainScreen].bounds.size.width - buttonH * 2 )/ 4;
    CGFloat bottomY = self.frame.size.height - 6 - 5 - buttonH;
    self.bottomView.frame = CGRectMake(0, bottomY, self.frame.size.width, 6 + 5 + buttonH);
    self.cameraButton.frame = CGRectMake(leftPadding2 + buttonW, bottomY + 5, buttonW, buttonH);
    self.photoButton.frame = CGRectMake(self.frame.size.width - buttonW * 2 - leftPadding2, bottomY + 5, buttonW, buttonH);
 
    
}
@end
