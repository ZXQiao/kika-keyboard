//
//  KeyBoardPreview.h
//  Emoji Keyboard
//
//  Created by wangfeng on 14-12-24.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyBoardPreview : UIView

    // 英文字母
@property (strong, nonatomic)  NSMutableArray *characterKeys;


    // 大小写切换
@property (strong, nonatomic) UIButton *shiftButton;

@property (strong, nonatomic)  UIButton *altButton;
    // 回车键
@property (strong, nonatomic)  UIButton *returnButton;
    // 删除
@property (strong, nonatomic)  UIButton *deleteButton;

    // global
@property (strong, nonatomic) UIButton *globalButton;

    // 空格
@property (strong, nonatomic)  UIButton *spaceButton;

    // 新加的theme按钮
@property (strong, nonatomic)  UIButton *themeButton;



@property (strong, nonatomic)UIImageView *keyboardBackground;
@end
