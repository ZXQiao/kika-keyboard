//
//  SHTitleCell.m
//  SweetHeart
//
//  Created by 张赛 on 14-7-28.
//  Copyright (c) 2014年 张赛. All rights reserved.
//

#import "CustomThemeCell.h"

@interface CustomThemeCell()







@end
#define beishu 4
@implementation CustomThemeCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        // 背景图
        UIView * backView = [[UIView alloc] init];
        self.backView = backView;
        [self.contentView addSubview:backView];
        self.backView.backgroundColor = [UIColor whiteColor];
        backView.clipsToBounds = YES;
        self.backView.layer.masksToBounds = YES;
        self.backView.layer.cornerRadius = 5;
        
        // 大图
        UIImageView * themeImageView = [[UIImageView alloc] init];
        themeImageView.backgroundColor = [UIColor greenColor];
        self.themeImageView =  themeImageView;
        [self.backView addSubview:themeImageView];
        
        // 选中按钮
        UIButton * selectButton = [[UIButton alloc] init];
        self.selectButton = selectButton;
        [self.backView addSubview:selectButton];
        // custom_select
        [selectButton setImage:[UIImage imageNamed:@"custom_select"] forState:UIControlStateNormal];
        [selectButton setImage:[UIImage imageNamed:@"custom_select"] forState:UIControlStateHighlighted];
        selectButton.contentEdgeInsets = UIEdgeInsetsMake(24, 23, 0,0);
        
        UIButton * deleteButton = [[UIButton alloc] init];
        self.deleteButton = deleteButton;
        [self.backView addSubview:deleteButton];
        [deleteButton setImage:[UIImage imageNamed:@"background_delete"] forState:UIControlStateNormal];
        [deleteButton setImage:[UIImage imageNamed:@"background_delete_pressed"] forState:UIControlStateHighlighted];
        deleteButton.contentEdgeInsets = UIEdgeInsetsMake(0, 22, 23, 0);

        self.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
        self.selectButton.hidden  = YES;
        self.deleteButton.hidden  = YES;

    }
    return self;
}



-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat topPadding = 10;
    CGFloat leftPadding = self.frame.size.width * 1/ 20;

    CGFloat backViewW = self.frame.size.width - 2 * leftPadding;
    // 图片对应比例的高度根据backView的宽度确定
    CGFloat imageH = (backViewW  - 60)/ 604 * 284 ;
    // cell的高度与localThemeCell的高度相同
    CGFloat backViewH = backViewW / 690 * 182 * 1.2;
    CGFloat backViewX = leftPadding;
    CGFloat backViewY = topPadding;
    self.backView.frame = CGRectMake(backViewX, backViewY, backViewW, backViewH);


        //self.backView.backgroundColor = [UIColor purpleColor];
        //CKLog(@"height  %f",self.backView.frame.size.height);

//    CGFloat keyboardH = backViewH;
//    CGFloat keyboardW = backViewH / 216 * backViewW;
    CGFloat padding = .5;
//    CGFloat themeImageViewH = backViewH - 2 * padding;
    CGFloat themeImageViewH = backViewW / (self.frame.size.width - 74 )* imageH;;
    CGFloat themeImageViewX = padding;
    CGFloat themeImageViewY = backViewH - themeImageViewH;
    self.themeImageView.frame = CGRectMake(themeImageViewX, themeImageViewY, backViewW - 60, themeImageViewH);

    CGFloat selectButtonY = backViewH - 50;
    CGFloat selectButtonH = 50;
    CGFloat selectButtonW = selectButtonH;
    CGFloat selectButtonX = backViewW - selectButtonW;
    self.selectButton.frame = CGRectMake(selectButtonX, selectButtonY, selectButtonW, selectButtonH);

    
    CGFloat deleteButtonW = selectButtonW;
    CGFloat deleteButtonH = selectButtonH;
    CGFloat deleteButtonX = backViewW - deleteButtonW;
    CGFloat deleteButotnY = 0;
    self.deleteButton.frame = CGRectMake(deleteButtonX, deleteButotnY, deleteButtonW, deleteButtonH);

    

}

- (void)awakeFromNib
{

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];


}


@end
