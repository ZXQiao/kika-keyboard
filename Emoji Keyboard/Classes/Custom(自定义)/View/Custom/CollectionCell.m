//
//  CollectionCell.m
//  Emoji Keyboard
//
//  Created by wangfeng on 15-1-7.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CollectionCell.h"

@implementation CollectionCell


-(UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:_backView];

    }
    return _backView;
}

-(UIImageView *)themeImageView
{
    if (!_themeImageView) {
        _themeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.backView.frame.size.width, self.backView.frame.size.height)];

        [self.backView addSubview:_themeImageView];
    }
    return _themeImageView;
}

-(UIButton *)deleteButton
{
    if (!_deleteButton) {
        _deleteButton = [[UIButton alloc]initWithFrame:CGRectMake(self.backView.frame.size.width - 50, 0, 50, 50)];


        [_deleteButton setImage:[UIImage imageNamed:@"background_delete"] forState:UIControlStateNormal];
        [_deleteButton setImage:[UIImage imageNamed:@"background_delete_pressed"] forState:UIControlStateHighlighted];
        [self.backView addSubview:_deleteButton];

        _deleteButton.contentEdgeInsets = UIEdgeInsetsMake(0, 22, 23, 0);
        _deleteButton.hidden = YES;

    }
    return _deleteButton;
}
-(UIButton *)selectButton
{
    if (!_selectButton) {
        _selectButton = [[UIButton alloc]initWithFrame:CGRectMake(self.backView.frame.size.width-50, self.backView.frame.size.height-50, 50, 50)];
        [_selectButton setImage:[UIImage imageNamed:@"custom_select"] forState:UIControlStateNormal];
        [_selectButton setImage:[UIImage imageNamed:@"custom_select"] forState:UIControlStateHighlighted];
        _selectButton.contentEdgeInsets = UIEdgeInsetsMake(24, 23, 0,0);
        _selectButton.hidden  = YES;

        [self.backView addSubview:_selectButton];
    }
    return _selectButton;
}
@end
