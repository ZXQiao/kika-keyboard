//
//  CustomThemeView.h
//  Emoji Keyboard
//
//  Created by wangfeng on 14-12-23.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomThemeView : UIView

@property (nonatomic, strong) NSMutableArray * deleteButtonArray;

@property (nonatomic, strong) NSMutableArray * customThumbnailPath;

@property (nonatomic, strong) NSMutableArray * customImageArray;

@property (nonatomic, strong) UITableView * customTableView;

@property (nonatomic, strong) NSMutableArray * selectedArray;

- (void)test;
@end
