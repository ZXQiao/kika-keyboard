//
//  CustomBackGround.h
//  Emoji Keyboard
//
//  Created by 张赛 on 14/12/27.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomFontDelegate <NSObject>

- (void)customFontButtonClicked:(UIButton *)button fontName:(NSString *)fontName;

@end
@interface CustomFont : UIView

@property (nonatomic, weak) id<CustomFontDelegate> customFontDelegate;

// 暴露出来是为了给sliderBar设置frame使用,不把slider放在这个视图里是因为slider如果用代理的话会比较烦人
@property (nonatomic, strong) UIScrollView * fontScrollView;

@end
