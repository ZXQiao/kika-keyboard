//
//  KeyBoardPreview.m
//  Emoji Keyboard
//
//  Created by wangfeng on 14-12-24.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "KeyBoardPreview.h"

enum {
    PKNumberPadViewImageLeft = 0,
    PKNumberPadViewImageInner,
    PKNumberPadViewImageRight,
    PKNumberPadViewImageMax
};

@interface KeyBoardPreview()
    // 背景



@property (nonatomic, assign, getter=isShifted) BOOL shifted;







#pragma mark - 三行数组
@property (nonatomic, strong)  NSMutableArray * lineOneButtons;

@property (nonatomic, strong)  NSMutableArray * lineTwoButtons;

@property (nonatomic, strong)  NSMutableArray * lineThreeButtons;



@property (nonatomic, assign) CGFloat renderR;

@property (nonatomic, assign) CGFloat renderG;

@property (nonatomic, assign) CGFloat renderB;

@property (nonatomic, strong) NSMutableArray * themeArray;

@property (nonatomic, strong) NSString * themeName;




@property (nonatomic, strong) NSMutableDictionary * languageDict;

@property (nonatomic, strong) NSMutableArray * languageArray;

@property (nonatomic, strong) NSMutableArray * characterStrArray;

@property (nonatomic, strong) NSMutableArray * characterStrUperArray;

@property (nonatomic, assign) CGRect beginFrame;

@property (nonatomic, assign) CGFloat currentViewW;


@property (nonatomic, assign) CGFloat offsetAlreadySet;






    // 当前色值
@property (nonatomic, assign) CGFloat currentR;

@property (nonatomic, assign) CGFloat currentG;

@property (nonatomic, assign) CGFloat currentB;


    // 四行的y值
@property (nonatomic, assign) CGFloat lineOneY;
@property (nonatomic, assign) CGFloat lineTwoY;
@property (nonatomic, assign) CGFloat lineThreeY;
@property (nonatomic, assign) CGFloat lineFourY;

@property (nonatomic, assign) int lineONEbtnCount;
@property (nonatomic, assign) int lineTWObtnCount;
@property (nonatomic, assign) int lineTHREEbtnCount;

@property (nonatomic, assign) CGRect leftFrame;
@property (nonatomic, assign) CGRect rightFrame;
@property (nonatomic, strong) UIButton * leftButton;
@property (nonatomic, strong) UIButton * righButton;


#pragma mark - 为了让点按更明显,在每个button的后面添加一堆view
@property (nonatomic, strong) NSMutableArray * buttonBackViewArrayM;




    // shift按键添加双击手势处理
@property (nonatomic, strong) UITapGestureRecognizer * tapTwoGestureRecognizer;

@end


@implementation KeyBoardPreview

- (void)PreViewKeyboardButtonChangeColor:(NSNotification *)note
{
    
    CGFloat shiftRatio = 0;
    CGFloat deleteRatio = 0;
    CGFloat themeRatio = 0;
    CGFloat globalRatio = 0;

//    CKLog(@"%@",note.object);
    
    shiftRatio = .2;
    deleteRatio = .2;
    themeRatio = .2;
    globalRatio = .2;
    
    UIColor * color = note.object;
    
    
    [self buttonMatchImage:self.shiftButton imageName:@"custom_shift_small" color:color ratio:shiftRatio];
    [self buttonMatchImage:self.deleteButton imageName:@"custom_delete" color:color ratio:deleteRatio];
    [self buttonMatchImage:self.themeButton imageName:@"custom_skin" color:color ratio:themeRatio];
    [self buttonMatchImage:self.globalButton imageName:@"custom_global" color:color ratio:globalRatio];
    [self.altButton setTitleColor:color forState:UIControlStateNormal];
    [self.spaceButton setTitleColor:color forState:UIControlStateNormal];
    [self.returnButton setTitleColor:color forState:UIControlStateNormal];
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.lineONEbtnCount = (int)[self.languageArray[0] count];
        self.lineTWObtnCount = (int)[self.languageArray[1] count];
        self.lineTHREEbtnCount = (int)[self.languageArray[2] count];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PreViewKeyboardButtonChangeColor:) name:@"PreViewKeyboardButtonChangeColor" object:nil];


    self.themeName = @"custom";
    [self loadCharactersWithArray:self.characterStrUperArray];



    [self.shiftButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special",self.themeName]] forState:UIControlStateNormal];
    [self.shiftButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special_pressed",self.themeName]] forState:UIControlStateHighlighted];

    [self.deleteButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special",self.themeName]] forState:UIControlStateNormal];
    [self.deleteButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special_pressed",self.themeName]] forState:UIControlStateHighlighted];

    [self.spaceButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_space",self.themeName]] forState:UIControlStateNormal];
    [self.spaceButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_space_pressed",self.themeName]] forState:UIControlStateHighlighted];
    [self.spaceButton setTitle:@"Space" forState:UIControlStateNormal];
    [self.spaceButton setTitle:@"Space" forState:UIControlStateHighlighted];
    self.spaceButton.titleLabel.font = [UIFont systemFontOfSize:13];

    [self.returnButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_search",self.themeName]] forState:UIControlStateNormal];
    [self.returnButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_search_pressed",self.themeName]] forState:UIControlStateHighlighted];
    [self.returnButton setTitle:@"Return" forState:UIControlStateNormal];
    [self.returnButton setTitle:@"Return" forState:UIControlStateHighlighted];
    self.returnButton.titleLabel.font = [UIFont systemFontOfSize:12];


    [self.altButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special",self.themeName]]forState:UIControlStateNormal];
    [self.altButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special_pressed",self.themeName]] forState:UIControlStateHighlighted];


    [self.themeButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special",self.themeName]]  forState:UIControlStateNormal];
    [self.themeButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special_pressed",self.themeName]] forState:UIControlStateHighlighted];


    [self.globalButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special",self.themeName]] forState:UIControlStateNormal];
    [self.globalButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_special_pressed",self.themeName]]  forState:UIControlStateHighlighted];


        // 按钮到边界的距离
    CGFloat lineOne_btn_border = 0;
        // 这个间距要根据第一行的数据来计算
    CGFloat lineTwo_btn_border = 0;
        // 按钮宽度
    CGFloat btnW = 0;
        // 按钮高度
    CGFloat btnH = 0;
        // 按钮到顶部的高度
    CGFloat lineOne_btn_top = 0;
        // 按钮到底部的高度
    CGFloat lineFour_btn_bottom = 0;
        // 按钮到按钮的距离
    CGFloat btn_btn = 0;
        // 行间距
    CGFloat line_line = 0;
        // 第三行的特殊按钮间距
    CGFloat lineThree_btn_padding = 0;
        // 第三行首尾按钮的宽度
    CGFloat lineThree_btn_W = 0;
        // space按钮宽度
    CGFloat spaceButtonW = 0;
        // return按钮宽度
    CGFloat returnButtonW = 0;

    CGFloat shiftRatio = 0;
    CGFloat deleteRatio = 0;
    CGFloat themeRatio = 0;
    CGFloat globalRatio = 0;

        // 竖屏
    if (self.frame.size.width < 420) {

        shiftRatio = .2;
        deleteRatio = .2;
        themeRatio = .2;
        globalRatio = .2;

        lineOne_btn_top = 10;
        lineOne_btn_border = 3;
        lineFour_btn_bottom = 3;
        btn_btn = 5;
        line_line = 11;
        lineThree_btn_padding = 5;



    }

//    NSString * shitImageName = nil;
//
//    int shiftBtnTAG = (int)self.shiftButton.tag;
//    switch (shiftBtnTAG) {
//        case 1:
//            shitImageName = @"custom_shift_small";
//            break;
//        case 2:
//            shitImageName = @"shiftOn";
//            break;
//        case 3:
//            shitImageName = @"shift_total";
//            break;
//
//        default:
//            break;
//    }



        // 数目
    int lineONEnum =  (int)[self.languageArray[0] count];
    int lineTWOnum =  (int)[self.languageArray[1] count];
    int lineTHreenum =  (int)[self.languageArray[2] count];

        // 索引号
    int lineONEindex = (int) [self.languageArray[0] count];
    int lineTWOindex = (int) [self.languageArray[1] count] + lineONEindex;
    int lineTHREEindex = (int) [self.languageArray[2] count] + lineTWOindex;

    btnW = (self.frame.size.width - lineOne_btn_border * 2 - (lineONEnum - 1)  * btn_btn ) / lineONEnum;
    btnH = (self.frame.size.height - lineOne_btn_top - lineFour_btn_bottom - 3 * line_line) / 4;

    CGFloat viewW = self.frame.size.width / lineONEnum;
    CGFloat viewH = self.frame.size.height / 4 + line_line / 5;
    CGFloat __block viewX = 0;
    CGFloat __block viewY = 0;

        // 为了让键盘保持统一和美观,让英语界面的button按钮为统一标准
    CGFloat englishBtnW = (self.frame.size.width - lineOne_btn_border * 2 - (10 - 1)  * btn_btn ) / 10;
    returnButtonW = 3 * englishBtnW;
    spaceButtonW = 4 * englishBtnW;
        // 第二行首尾间距
    lineTwo_btn_border = (self.frame.size.width - (lineTWOnum - 1) * btn_btn - lineTWOnum * btnW ) / 2;
        // 第三行首尾按钮的宽度
    lineThree_btn_W = (self.frame.size.width - (lineTHreenum - 1) * btn_btn - lineTHreenum * btnW - 2 * lineThree_btn_padding - lineOne_btn_border * 2) / 2;



    CGFloat __block btnX = 0;
    CGFloat __block btnY = 0;


        // 遍历数组,
    [self.characterKeys enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL *stop) {



        if (idx < lineONEindex) {

            btnY = lineOne_btn_top;
            btnX = lineOne_btn_border + (btnW + btn_btn) * idx;
            self.lineOneY = btnY;


            obj.frame = CGRectMake(btnX, btnY, btnW, btnH);
            UIView * view = self.buttonBackViewArrayM[idx];
            viewX = idx * viewW;
            viewY = 0;
            view.frame = CGRectMake(viewX, viewY, viewW, viewH);
        }

        if (idx >= lineONEindex && idx < lineTWOindex) {

            btnY = lineOne_btn_top + btnH + line_line ;
            btnX = lineTwo_btn_border + (btnW + btn_btn) * (idx - lineONEindex);

            self.leftFrame = CGRectMake(0, btnY, lineTwo_btn_border, btnH);
            CGFloat rightX = self.frame.size.width - lineTwo_btn_border;
            self.rightFrame = CGRectMake(rightX, btnY, btnW, btnH);


            if (idx == lineONEindex) {
                self.leftButton = obj;
            }
            if (idx == lineTWOindex - 1) {
                self.righButton = obj;
            }



            obj.frame = CGRectMake(btnX, btnY, btnW, btnH);
            UIView * view = self.buttonBackViewArrayM[idx];
            viewX = (idx - lineONEindex) * viewW + lineTwo_btn_border - lineOne_btn_border;
            viewY = viewH;
            view.frame = CGRectMake(viewX, viewY, viewW, viewH);


        }


        if (idx >= lineTWOindex && idx < lineTHREEindex) {
            btnY = lineOne_btn_top + btnH * 2 + line_line * 2;
            btnX =lineOne_btn_border + lineThree_btn_W + lineThree_btn_padding + (btnW + btn_btn) * (idx - lineTWOindex);
            self.lineThreeY = btnY;

            obj.frame = CGRectMake(btnX, btnY, btnW, btnH);
            UIView * view = self.buttonBackViewArrayM[idx];
            viewX = (idx - lineTWOindex) * viewW + lineThree_btn_W + lineOne_btn_border + lineOne_btn_border;
            viewY = viewH * 2;
            view.frame = CGRectMake(viewX, viewY, viewW, viewH) ;


        }




    }];

    CGFloat shifX = lineOne_btn_border;
    CGFloat shifY = self.lineThreeY;
    CGFloat shifW = lineThree_btn_W;
    CGFloat shifH = btnH;
    self.shiftButton.frame = CGRectMake(shifX, shifY, shifW, shifH);

    CGFloat deleteW = lineThree_btn_W;
    CGFloat deleteX = self.frame.size.width - lineOne_btn_border - deleteW;
    CGFloat deleteY = self.lineThreeY;
    CGFloat deleteH = btnH;
    self.deleteButton.frame = CGRectMake(deleteX, deleteY, deleteW, deleteH);

    self.lineFourY = CGRectGetMaxY(self.deleteButton.frame) + line_line;
    CGFloat altX = lineOne_btn_border;
    CGFloat altY = self.lineFourY;
    CGFloat altW = (self.frame.size.width - returnButtonW - spaceButtonW - 4 * btn_btn - 2 * lineOne_btn_border) / 3;
    CGFloat altH = btnH;
    self.altButton.frame = CGRectMake(altX, altY, altW, altH);

    CGFloat skinX = altX + altW + btn_btn;
    CGFloat skinY = self.lineFourY;
    CGFloat skinW = altW;
    CGFloat skinH = btnH;
    self.themeButton.frame = CGRectMake(skinX, skinY, skinW, skinH);

    CGFloat spaceX = skinX + skinW + btn_btn;
    CGFloat spaceY = self.lineFourY;
    CGFloat spaceH = btnH;
    self.spaceButton.frame = CGRectMake(spaceX, spaceY, spaceButtonW, spaceH);

    CGFloat globalX = CGRectGetMaxX(self.spaceButton.frame) + btn_btn;
    CGFloat globalY = self.lineFourY;
    CGFloat globalW = altW;
    CGFloat globalH = btnH;
    self.globalButton.frame = CGRectMake(globalX, globalY, globalW, globalH);

    CGFloat returnX = CGRectGetMaxX(self.globalButton.frame) + btn_btn;
    CGFloat returnY = self.lineFourY;
    CGFloat returnH = btnH;
    self.returnButton.frame = CGRectMake(returnX, returnY, returnButtonW, returnH);


    [self buttonMatchImage:self.shiftButton imageName:@"custom_shift_small" color:nil ratio:shiftRatio];
    [self buttonMatchImage:self.deleteButton imageName:@"custom_delete" color:nil ratio:deleteRatio];
    [self buttonMatchImage:self.themeButton imageName:@"custom_skin" color:nil ratio:themeRatio];
    [self buttonMatchImage:self.globalButton imageName:@"custom_global" color:nil ratio:globalRatio];




    self.altButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Light" size:13];
    self.shiftButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Light" size:13];
    self.spaceButton.titleLabel.font =[UIFont fontWithName:@"Helvetica Light" size:15];
    self.returnButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Light" size:12];
    
    
   



    return self;
}



- (NSMutableArray *)buttonBackViewArrayM
{
    if (_buttonBackViewArrayM == nil) {
        _buttonBackViewArrayM = [NSMutableArray array];

        for (int i = 0 ; i < self.characterKeys.count; i ++) {
            UIView * view = [[UIView alloc] init];

            [_buttonBackViewArrayM addObject:view];




        }
    }
    return _buttonBackViewArrayM;
}

    // 三行按钮
- (NSMutableArray *)lineOneButtons
{
    if (!_lineOneButtons) {

        _lineOneButtons = [NSMutableArray array];

        for (int i = 0; i < [self.languageArray[0] count]; i ++) {
            UIButton * button = [[UIButton alloc] init];

            [_lineOneButtons addObject:button];
            button.enabled = NO;
            button.adjustsImageWhenDisabled = NO;

        }
    }
    return _lineOneButtons;

}


- (NSMutableArray *)lineTwoButtons
{
    if (!_lineTwoButtons) {

        _lineTwoButtons = [NSMutableArray array];

        for (int i = 0; i < [self.languageArray[1] count]; i ++) {
            UIButton * button = [[UIButton alloc] init];

            [_lineTwoButtons addObject:button];
            button.enabled = NO;
            button.adjustsImageWhenDisabled = NO;

        }
    }
    return _lineTwoButtons;

}

- (NSMutableArray *)lineThreeButtons
{
    if (!_lineThreeButtons) {

        _lineThreeButtons = [NSMutableArray array];

        for (int i = 0; i < [self.languageArray[2] count]; i ++) {
            UIButton * button = [[UIButton alloc] init];

            [_lineThreeButtons addObject:button];
            button.enabled = NO;
            button.adjustsImageWhenDisabled = NO;

        }
    }
    return _lineThreeButtons;

}



- (NSMutableArray *)characterKeys
{
    if(!_characterKeys)
        {
        _characterKeys = [NSMutableArray array];
        for (UIButton * button in self.lineOneButtons) {
            [_characterKeys addObject:button];

            [self addSubview:button];
        }
        for (UIButton * button in self.lineTwoButtons) {
            [_characterKeys addObject:button];

            [self addSubview:button];
        }
        for (UIButton * button in self.lineThreeButtons) {
            [_characterKeys addObject:button];

            [self addSubview:button];
        }

        }
    return _characterKeys;
}


- (UIButton *)shiftButton
{
    if (_shiftButton == nil) {
        _shiftButton = [[UIButton alloc] init];

        [self addSubview:_shiftButton];

    }
    return _shiftButton;
}

- (UIButton *)deleteButton
{
    if (_deleteButton == nil) {
        _deleteButton = [[UIButton alloc] init];

        [self addSubview:_deleteButton];
    }
    return _deleteButton;
}

- (UIButton *)altButton
{
    if (_altButton == nil) {

        _altButton = [[UIButton alloc] init];

        [self addSubview:_altButton];
        [_altButton setTitle:@"123" forState:UIControlStateNormal];
        [_altButton setTitle:@"123" forState:UIControlStateNormal];

    }
    return _altButton;
}
- (UIButton *)themeButton
{
    if (_themeButton == nil) {
        _themeButton = [[UIButton alloc] init];

        [self addSubview:_themeButton];

    }
    return _themeButton;
}

- (UIButton *)spaceButton
{
    if (_spaceButton == nil) {
        _spaceButton = [[UIButton alloc] init];

        [self addSubview:_spaceButton];
    }
    return _spaceButton;
}

- (UIButton *)globalButton
{
    if (_globalButton == nil) {
        _globalButton = [[UIButton alloc] init];
        [self addSubview:_globalButton];
           }
    return _globalButton;
}

- (UIButton *)returnButton
{
    if (_returnButton == nil) {
        _returnButton = [[UIButton alloc] init];

        [self addSubview:_returnButton];

    }
    return _returnButton;
}

#pragma mark - 键盘文字大写数组
- (NSMutableArray *)characterStrUperArray
{
    if (!_characterStrUperArray) {

        _characterStrUperArray = [NSMutableArray array];
        for (NSString * str  in self.characterStrArray) {


            NSString * tempStr  =  [str uppercaseString];
            [_characterStrUperArray addObject:tempStr];

        }
    }
    return _characterStrUperArray;
}
#pragma mark - 键盘文字数组
- (NSMutableArray *)characterStrArray
{
    if (!_characterStrArray) {

        _characterStrArray = [NSMutableArray array];

        for (NSString *str  in self.languageArray[0]) {

            [_characterStrArray addObject:str];
        }

        for (NSString *str  in self.languageArray[1]) {

            [_characterStrArray addObject:str];
        }

        for (NSString *str  in self.languageArray[2]) {

            [_characterStrArray addObject:str];

        }


    }
    return _characterStrArray;
}





#pragma mark - 语言数组
-(NSMutableArray *)languageArray
{
    if (!_languageArray) {

        NSUserDefaults * userLanguageInfo = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];

        NSString * language = [userLanguageInfo objectForKey:@"Language"];

        if (language.length == 0 || language == nil) {
            language = @"English";
        }
        _languageArray = self.languageDict[language];
        [userLanguageInfo synchronize];
    }
    return _languageArray;
}



- (NSMutableDictionary *)languageDict
{
    if (!_languageDict) {

        _languageDict = [NSMutableDictionary dictionary];

        NSDictionary * tempDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Language.plist" ofType:nil]];

        _languageDict = (NSMutableDictionary *)tempDict;

    }

    return _languageDict;
}



- (NSMutableArray *)themeArray
{
    if (_themeArray == nil) {
        _themeArray = [NSMutableArray array];

        NSString * fullPath = [[NSBundle mainBundle] pathForResource:@"ThemeMatch.plist" ofType:nil];
        NSArray * arr = [NSArray arrayWithContentsOfFile:fullPath];
        _themeArray = (NSMutableArray *)arr;

    }
    return _themeArray;
}



    // 生成一个自动匹配的背景图片
- (UIImage *)imageMatch:(NSString *)imageName;
{
    UIImage * image = [UIImage imageNamed:imageName];

    NSInteger leftCapWidth = image.size.width * 0.5f;
        // 顶端盖高度
    NSInteger topCapHeight = image.size.height * 0.5f;
        // 重新赋值
    image = [image stretchableImageWithLeftCapWidth:leftCapWidth topCapHeight:topCapHeight];


    return image;
}


    // 传进来一个按钮，和一个图片的名称，返回设置好的按钮；
- (void)buttonMatchImage:(UIButton *)button imageName:(NSString *)imageName color:(UIColor *)color ratio:(CGFloat )ratio
{
    UIImage * image = [UIImage imageNamed:imageName];
    CGFloat buttonW = button.frame.size.width;
    CGFloat buttonH = button.frame.size.height;

        // 获得较短边
    CGFloat lessLength = 0;
    CGFloat topPadding = 0;
    CGFloat leftPadding = 0;
        // 宽度小
    if (buttonW <= buttonH) {
        leftPadding = ratio * buttonW;
        lessLength = buttonW - leftPadding * 2;
        topPadding = (buttonH - lessLength) / 2;

    }
    if (buttonW > buttonH) {
        topPadding = ratio * buttonH;
        lessLength = buttonH - topPadding * 2;
        leftPadding = (buttonW - lessLength) / 2;
    }

    button.contentEdgeInsets = UIEdgeInsetsMake(topPadding, leftPadding, topPadding, leftPadding);

    [button setImage:[image imageWithTintColor:color] forState:UIControlStateNormal];
    [button setImage:[image imageWithTintColor:color]forState:UIControlStateHighlighted];
    if (color == nil) {
        [button setImage:image forState:UIControlStateNormal];
        [button setImage:image forState:UIControlStateHighlighted];
    }
}

-(void)loadCharactersWithArray:(NSArray *)a {

    self.backgroundColor = [UIColor clearColor];
    int i = 0;
    for (UIButton *btn in self.characterKeys) {
#pragma mark - 普通button的背景图片
        NSString * buttonImageName = [NSString stringWithFormat:@"%@_button",self.themeName];
        UIImage * buttonImage  = [self imageMatch:buttonImageName];
        [btn setBackgroundImage:buttonImage forState:UIControlStateNormal];

        btn.showsTouchWhenHighlighted = YES;

        [btn setTitle:[a objectAtIndex:i] forState:UIControlStateNormal];
        [btn setTitle:[a objectAtIndex:i] forState:UIControlStateHighlighted];
            //        btn.titleLabel.font = [UIFont systemFontOfSize:26];
            //         btn.titleLabel.font = [UIFont fontWithName:@"Helvetica Light" size:20];

        /**
         *  设置预览键盘默认的字体
         */
        btn.titleLabel.font = [UIFont fontWithName:@"Helvetica Light" size:20];
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        i++;
    }



}

    // 观察者的注销，优化内存
-(void)dealloc{[[NSNotificationCenter defaultCenter] removeObserver:self];}


@end
