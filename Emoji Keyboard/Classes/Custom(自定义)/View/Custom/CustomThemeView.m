//
//  CustomThemeView.m
//  Emoji Keyboard
//
//  Created by wangfeng on 14-12-23.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "CustomThemeView.h"
#import "KeyBoardPreview.h"
#import "CustomThemeCell.h"
#import "Consetting.h"
#import "UIImage+Tools.h"
#import "Colours.h"


@interface CustomThemeView()<UITableViewDataSource,UITableViewDelegate>



@property (nonatomic, strong) UIButton * addButton;
@property (nonatomic, strong) NSString * selectedRowStr;
@property (nonatomic, strong) UIButton * selectedButton;
@property (nonatomic, strong) NSArray * keyboardElementsArray;

@end

@implementation CustomThemeView

- (NSArray *)keyboardElementsArray
{
    if (_keyboardElementsArray == nil) {
        NSString * arrayPath = [[NSBundle mainBundle] pathForResource:@"keyboardElementsArray.plist" ofType:nil];
        _keyboardElementsArray = [NSArray arrayWithContentsOfFile:arrayPath];
    }
    return _keyboardElementsArray;
}


- (NSMutableArray *)selectedArray
{
    if (_selectedArray == nil) {
        _selectedArray = [NSMutableArray array];
        NSUserDefaults * myUserInfo = [NSUserDefaults standardUserDefaults];
        NSString * themeStyle = [myUserInfo objectForKey:@"ThemeStyle"];
        if ([themeStyle isEqualToString:@"CUSTOM"]) {
            NSString * themeIndexStr = [myUserInfo objectForKey:@"themeIndexStr"];
            for (int i = 0; i < self.customImageArray.count ; i ++) {
                NSString * selectedStr;
                if (themeIndexStr.intValue != i) {
                     selectedStr = @"NO";
                }
                else{
                    selectedStr = @"YES";
                }
                [_selectedArray addObject:selectedStr];
            }
        }
        else
        {
            for (int i = 0; i < self.customImageArray.count ; i ++) {
                NSString * selectedStr = @"NO";
                [_selectedArray addObject:selectedStr];
            }
        }
    }
    return  _selectedArray;
}

- (NSMutableArray *)deleteButtonArray
{
    if (_deleteButtonArray == nil) {
        _deleteButtonArray = [NSMutableArray array];
    }
    return _deleteButtonArray;
}
    
- (NSMutableArray *)customThumbnailPath
{
    if (_customThumbnailPath == nil || _customThumbnailPath.count == 0) {
        _customThumbnailPath = [NSMutableArray array];
        NSString *docDir = [[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Themes"] stringByAppendingPathComponent:@"Custom"] ;
        NSString * plistPath = [docDir stringByAppendingPathComponent:@"customThemeArray.plist"];
        NSArray * tempArray = [NSArray arrayWithContentsOfFile:plistPath];
        for (NSString * fileName in tempArray) {
            NSString * imagePath = [[docDir stringByAppendingPathComponent:fileName] stringByAppendingPathComponent:@"thumbnail.png"];
            [_customThumbnailPath addObject:imagePath];
        }
    }
    return _customThumbnailPath;
}

- (NSMutableArray *)customImageArray
{
    if (_customImageArray == nil || _customImageArray.count == 0) {
        _customImageArray = [NSMutableArray array];
        if (self.customThumbnailPath != nil && self.customThumbnailPath.count != 0) {
            for (NSString * imagePath in self.customThumbnailPath) {
                UIImage * image = [UIImage imageWithContentsOfFile:imagePath];
                [_customImageArray addObject:image];
            }
        }
    }
    return _customImageArray;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        UITableView * customTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - 50)];
        self.customTableView = customTableView;
        customTableView.delegate = self;
        customTableView.dataSource = self;
        [self addSubview:customTableView];
        customTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.customTableView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
        // 收到通知后,selecetdButton隐藏,且cell不可点击
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CellSelectButtonShouldHide:) name:@"CellSelectButtonShouldHide" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CustomThemeViewShouleSelectedLineONE:) name:@"CustomThemeViewShouleSelectedLineONE" object:nil];
    }return self;
}
- (void)CustomThemeViewShouleSelectedLineONE:(NSNotification *)note
{
    [self saveSelecetedTheme:[NSIndexPath indexPathForRow:0 inSection:0]];
}

- (void)CellSelectButtonShouldHide:(NSNotification *)note
{
    [self test];
    
    UIButton * button = (UIButton *)note.object;
    int buttonTAG = (int)button.tag;
    if (buttonTAG == 0) {
        self.customTableView.allowsSelection = NO; // 这个按钮不能被隐藏
        return;
    }
    if (buttonTAG ) {
        self.customTableView.allowsSelection = YES;
        return;
    }
}

- (void)test
{
//    CKLog(@"ImageArray===%@数量==%d,customThumbnailPath===%@数量==%d",self.customImageArray,(int)self.customImageArray.count, self.customThumbnailPath,(int)self.customThumbnailPath.count);
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.customThumbnailPath.count;
}

-(CustomThemeCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.customThumbnailPath.count > 0) {
        
        static NSString * ID = @"CustomThemeCell";
        CustomThemeCell * cell = [tableView dequeueReusableCellWithIdentifier:ID];
        if (cell == nil) {
            cell = [[CustomThemeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.themeImageView.image = self.customImageArray[indexPath.row];
        cell.deleteButton.tag = indexPath.row;
        [self.deleteButtonArray addObject:cell.deleteButton];
        [cell.deleteButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectButton.hidden = YES;
        
        NSUserDefaults * myUserInfo = [NSUserDefaults standardUserDefaults];
        NSString * themeStyle = [myUserInfo objectForKey:@"ThemeStyle"];
        if ([themeStyle isEqualToString:@"CUSTOM"]) {
            if ([self.selectedArray[indexPath.row] isEqualToString:@"YES"]) {
                cell.selectButton.hidden = NO;
                self.selectedButton = cell.selectButton;
            }
        }
            return cell;
    }
    else{return nil;}
}

- (void)deleteButtonClicked:(UIButton *)deleteButton
{
    int buttonTAG = (int)deleteButton.tag;
    // 1. 如果customImageArray数量大于0
    if (self.customImageArray.count > 0) {
        NSString *docDir = [[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Themes"] stringByAppendingPathComponent:@"Custom"] ;
        NSString * plistPath = [docDir stringByAppendingPathComponent:@"customThemeArray.plist"];
        NSMutableArray * tempArray = [NSMutableArray arrayWithContentsOfFile:plistPath];
                NSString * filePath = [docDir stringByAppendingPathComponent:tempArray[buttonTAG]];
//                CKLog(@"%@",filePath);
                NSFileManager * myFileManager = [NSFileManager defaultManager];
                NSError * error;
                if ([myFileManager fileExistsAtPath:filePath]) {
        
                }
                [myFileManager removeItemAtPath:filePath error:&error];
        
        [tempArray removeObjectAtIndex:buttonTAG];
//        CKLog(@"tempArray++==%@",tempArray);
//        CKLog(@"删除后的图片数组===%@",self.customImageArray);
        [tempArray writeToFile:plistPath atomically:YES];
        [self.customThumbnailPath removeObjectAtIndex:buttonTAG];
        [self.selectedArray removeObjectAtIndex:buttonTAG];
        [self.customImageArray removeObjectAtIndex:buttonTAG];
//        CKLog(@"customImageArray===%@",self.customImageArray);
        [self.customTableView reloadData];

        if (self.customThumbnailPath.count == 0) {// 如果图片数组为0,通知themeViewController选中第一行
            NSUserDefaults * myUserInfo = [NSUserDefaults standardUserDefaults];
            [myUserInfo setObject:@"LOCAL" forKey:@"ThemeStyle"];
            // 2.存储对应的选中信息
            [myUserInfo setObject:Default_Theme_Name forKey:@"themeName"];
            [myUserInfo setObject:@"0" forKey:@"themeIndexStr"];
        }

    }
  
}

#pragma 删除数据元素,把传入的序号处的元素删掉
- (NSMutableArray *)deleteArray:(NSMutableArray *)arrayM ElementAtIndex:(int)index
{
    NSMutableArray * tempArrayM = [NSMutableArray array];
    for (int i = 0; i < arrayM.count; i ++) {
        NSData * tempData = arrayM[i];
        if (i == index) {
            continue;
        }
        if (i != index) {
            [tempArrayM addObject:tempData];
        }
    }
    return  tempArrayM;

}


// cell行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row ==  self.customImageArray.count - 1) {

        if (iPhone4 || iPhone5) {
            return 111.2;
        }
        else if (iPhone6) {
                //return 126.8 ;
            return 140;
        }
        else if (iPhone6s) {
            return 137.9;
        }

        else{
            
            return 0;
        }

    }


    if (iPhone4 || iPhone5) {
        return 111.2 - 10;
    }
    else if (iPhone6) {
        return 126.8 - 10;
    }
    else if (iPhone6s) {
        return 137.9- 10;
    }

    else{
        
        return 0;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.selectedArray setObject:@"YES" atIndexedSubscript:(int)indexPath.row];
    CustomThemeCell * selectedCell = (CustomThemeCell *)[tableView cellForRowAtIndexPath:indexPath];
//  动画效果
    CGRect frame = selectedCell.backView.frame;
    [UIView animateWithDuration:1 animations:^{
        selectedCell.backView.alpha = 1;
        [UIView animateWithDuration:.5 animations:^{
            selectedCell.backView.transform = CGAffineTransformMakeScale(1.2, 1.2);
            selectedCell.backView.alpha = .4;
            
        }];
        
        [UIView animateWithDuration:.6 animations:^{
            selectedCell.backView.transform = CGAffineTransformMakeScale(.8, .8);
            selectedCell.backView.alpha = .8;
        }];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.5 animations:^{
            selectedCell.backView.transform = CGAffineTransformMakeScale(1, 1);
            selectedCell.backView.alpha = 1;
            selectedCell.backView.frame = frame;
            
        }];
    }];
    for (int i = 0; i < self.selectedArray.count;  i ++) {
        if (indexPath.row != i) {
        [self.selectedArray setObject:@"NO" atIndexedSubscript:i];
        }
        if (indexPath.row == i) {
            [self.selectedArray setObject:@"YES" atIndexedSubscript:i];
        }
    }
    [self.customTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    [self.customTableView reloadData];
    [self saveSelecetedTheme:indexPath];
  
    
}

- (void)saveSelecetedTheme:(NSIndexPath *)indexPath
{
    // 点选之后,将图片存至AppGroup
    // 1.创建文件夹,App Group中的文件夹
    NSFileManager * myFileManger = [NSFileManager defaultManager];
    NSError * err;
    NSURL *containerURL = [CKCommonTools GetAPP_GROUPS_Library_ThemesURL];
    NSString * path = containerURL.absoluteString;
    if (![myFileManger fileExistsAtPath:path]) {
        [myFileManger removeItemAtURL:containerURL error:&err];
        [myFileManger createDirectoryAtURL:containerURL withIntermediateDirectories:YES attributes:nil error:nil];
        
    }
    // 3.背景图
    // customThemeArray.plist
    NSString *docDir1 = [[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Themes"]stringByAppendingPathComponent:@"Custom"];
    // 获得当前点按的文件夹名字
    NSArray * tempShowArray = [NSArray arrayWithContentsOfFile:[docDir1 stringByAppendingPathComponent:@"customThemeArray.plist"]];
    docDir1 = [docDir1 stringByAppendingPathComponent:tempShowArray[indexPath.row]];
    NSString * imageFilePath = [docDir1 stringByAppendingPathComponent:@"back.png"];
    NSString * plistFilePath = [docDir1 stringByAppendingPathComponent:@"customInfo.plist"];
    // 图片
    [myFileManger copyItemAtURL:[NSURL fileURLWithPath:imageFilePath] toURL:[containerURL URLByAppendingPathComponent:[NSString stringWithFormat:@"finalBackground.png"]] error:&err];
    // plist
    [myFileManger copyItemAtURL:[NSURL fileURLWithPath:plistFilePath] toURL:[containerURL URLByAppendingPathComponent:[NSString stringWithFormat:@"finalSettingInfo.plist"]] error:&err];
    
    NSDictionary * tempDict = [NSDictionary dictionaryWithContentsOfFile:plistFilePath];
    // 2.存储各种按钮
    // 在这里直接将图片保存为有颜色的,避免在加载键盘的时候还需要渲染颜色而导致卡顿
    NSString * fontColorStr = tempDict[@"FontColor"];
    UIColor * renderColor = [UIColor colorFromHexString:fontColorStr];
    for (NSString * str in self.keyboardElementsArray) {
        NSString * imageName = [NSString stringWithFormat:@"custom_%@.png",str];
        UIImage * image;
        NSString * imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:nil];
        BOOL sholdNotRender = ([str isEqualToString:@"button@2x"] ||[str isEqualToString:@"emojibar@2x"] || [str isEqualToString:@"search_pressed@2x"] || [str isEqualToString:@"search@2x"]|| [str isEqualToString:@"space_pressed@2x"]|| [str isEqualToString:@"space@2x"] || [str isEqualToString:@"special_pressed@2x"] || [str isEqualToString:@"special@2x"] );
        if (sholdNotRender) {
            image = [UIImage imageWithContentsOfFile:imagePath];
        }
        if (!sholdNotRender) {
            image = [[UIImage imageWithContentsOfFile:imagePath] imageWithTintColor:renderColor];
        }
        NSData * data = UIImagePNGRepresentation(image);
        NSURL * destionURL = [containerURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.png",str]];
        [data writeToURL:destionURL atomically:YES];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OnlineThemeViewShouldReloadData" object:nil];
    
    
    // 存储起来当前的index,记录选中行
    NSString * ThemeStyle;
    NSString * themeIndexStr;
    NSUserDefaults * myUserInfo = [NSUserDefaults standardUserDefaults];
    ThemeStyle = @"CUSTOM";
    themeIndexStr = [NSString stringWithFormat:@"%d",(int)indexPath.row];
    [myUserInfo setObject:ThemeStyle forKey:@"ThemeStyle"];
    [myUserInfo setObject:themeIndexStr forKey:@"themeIndexStr"];
}


@end
