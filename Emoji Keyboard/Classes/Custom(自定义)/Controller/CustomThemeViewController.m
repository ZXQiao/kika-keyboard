//
//  CustomThemeViewController.m
//  Emoji Keyboard
//
//  Created by wangfeng on 14-12-24.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "CustomThemeViewController.h"
#import "KeyBoardPreview.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "VPImageCropperViewController.h"
#import "CustomBackGround.h"
#import "CustomFont.h"
#import "Consetting.h"
#import "Colours.h"
#pragma mark - 进度显示
#import "MBProgressHUD.h"


@interface CustomThemeViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate, VPImageCropperDelegate,CustomBackGroundDelegate,CustomFontDelegate,UIAlertViewDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    
    long long expectedLength;
    long long currentLength;
}



@property(nonatomic,strong)UIImageView* imageView;


@property(strong,nonatomic)NSArray* colorArray ;

@property(strong,nonatomic)UISlider* colorSlider ;

@property(strong,nonatomic) KeyBoardPreview* keyboardPre ;

@property (nonatomic, strong) UIView * keyboardRootView;
// 背景页面
@property (nonatomic, strong) CustomBackGround * customBackGround;
// 字体页面
@property (nonatomic, strong) CustomFont * customFont;
// 当前字体名称
@property (nonatomic, strong) NSString * currentFontName;
// 调整字体Slider
@property (nonatomic, strong) UISlider *textSlider;
// 保存按钮
@property (nonatomic, strong) UIButton * saveButton;
// 保存AlertView
@property (nonatomic, strong) UIAlertView * saveAlertView;
// 删除AlertView
@property (nonatomic, strong) UIAlertView * deleteAlertView;

@property (nonatomic, strong) NSMutableArray * customArray;

@property (nonatomic, assign) CGFloat currentFontSize;

@property (nonatomic, assign) CGFloat fontColorR;
@property (nonatomic, assign) CGFloat fontColorG;
@property (nonatomic, assign) CGFloat fontColorB;
@property (nonatomic, assign) BOOL fontColorChanged;


//@property (nonatomic, strong)NSMutableArray *  customThemeArray;
@property (nonatomic, strong) NSString * fontColorStr;
@property (nonatomic, strong) NSMutableArray * transferThemeArray;



#define color [UIColor colorWithRed:0/255.0 green:175/255.0 blue:240/255.0 alpha:1]
#define scrollViewHeight 80

@property (nonatomic) CGFloat viewSize;
@end


#define ORIGINAL_MAX_WIDTH 640.0f
@implementation CustomThemeViewController

- (NSMutableArray *)transferThemeArray
{
    if (_transferThemeArray == nil) {
        NSString *docDir = [[[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Themes"] stringByAppendingPathComponent:@"Custom"] stringByAppendingPathComponent:@"customThemeArray.plist"];
        _transferThemeArray = [NSMutableArray array];
        _transferThemeArray = [NSMutableArray arrayWithContentsOfFile:docDir];
        if (_transferThemeArray == nil) {
            _transferThemeArray = [NSMutableArray array];
        }
    }
    return _transferThemeArray;
}

//- (NSMutableArray *)customThemeArray
//{
//    if (_customThemeArray == nil) {
//        NSString *docDir = [[[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Themes"] stringByAppendingPathComponent:@"Custom"] stringByAppendingPathComponent:@"customThemeArray.plist"];
//        _customThemeArray = [NSMutableArray array];
//        _customThemeArray = [NSMutableArray arrayWithContentsOfFile:docDir];
//        if (_customThemeArray == nil) {_customThemeArray = [NSMutableArray array];}
//    }
//    return _customThemeArray;
//}

NSMutableArray* yArray;

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    

//    CKLog(@"transferArray===%@",self.transferThemeArray);
//    UIColor * firstColor = [UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1];
//    [self.navigationController.navigationBar setBackgroundImage:[self buttonImageFromColor:firstColor] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setBackIndicatorTransitionMaskImage:[self buttonImageFromColor:firstColor]];
//    [self.navigationController.navigationBar setShadowImage:[self buttonImageFromColor:firstColor]];
}



- (UIImage *)buttonImageFromColor:(UIColor *)barColor
{
    CGRect rect = CGRectMake(0, 0,10, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [barColor CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}



- (void)viewDidLoad
{
    
//    CKLog(@"Load%@",self.transferThemeArray);
    [super viewDidLoad];
    self.view.backgroundColor =  [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    self.title = @"Custom";
    self.navigationController.navigationBar.hidden = NO;
    
    CGFloat segmentBackY = 110;
    if (iPhone4) {
        segmentBackY = 95;
    }
    UIView * segmentBack = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, segmentBackY)];
    [self.view addSubview:segmentBack];
    segmentBack.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    // 顶部的标签切换
    UISegmentedControl* segment = [[UISegmentedControl alloc]initWithItems:@[@"Background ",@"Font"]];

    CGFloat segmentCenterY = 90;
    if (iPhone4) {
        segmentCenterY = 75;
    }
    segment.center = CGPointMake(self.view.frame.size.width / 2, segmentCenterY);
    segment.center = CGPointMake(self.view.center.x, segment.center.y);

    segment.tintColor = [UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1];
    [segment sizeToFit];
    [segment addTarget:self action:@selector(localViewToCustomView:) forControlEvents:UIControlEventValueChanged];
    segment.selectedSegmentIndex = 0;
    [self.view addSubview:segment];
//    segment.tintColor = [UIColor whiteColor];
    
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Custom";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;

    CGFloat marginH = 40;

    CGFloat keyboardW = self.view.frame.size.width - marginH;

    CGFloat keyboardH = 216 * keyboardW / self.view.frame.size.width;

    CGFloat keyboardDeltaY = 20;
    CGRect keyBoardPreviewRect = CGRectMake(marginH / 2,CGRectGetMaxY(segment.frame) + keyboardDeltaY, keyboardW, keyboardH);
    // 在键盘下面添加一个rootView是为了保存截图
    UIView * keyboardRootView = [[UIView alloc] initWithFrame:keyBoardPreviewRect];
    keyboardRootView.tag = 5201314;
    self.keyboardRootView = keyboardRootView;
    self.keyboardRootView.backgroundColor = [UIColor redColor];
    [self.view addSubview:keyboardRootView];

    // 键盘
  
    KeyBoardPreview* pre = [[KeyBoardPreview alloc]initWithFrame:keyboardRootView.bounds];    
    pre.backgroundColor = [UIColor clearColor];
    [self.keyboardRootView addSubview:pre];
    self.keyboardPre = pre;
    self.imageView = [[UIImageView alloc]initWithFrame:pre.bounds];
    [self.keyboardRootView addSubview:self.imageView];
    self.imageView.backgroundColor = [UIColor clearColor];
    [self.keyboardRootView sendSubviewToBack:self.imageView];

  
    
    // 取出图片
    //NSString * imageFilePath;
    // NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    // imageFilePath = [NSString stringWithFormat:@"%@/custom.png",docDir];
    //UIImage * customImage = [UIImage imageWithContentsOfFile:imageFilePath];
    //if (customImage == nil)
    // {
       UIImage * customImage = [UIImage imageNamed:@"background_bg_01"];

        // 首次加载的时候需要有一张默认图片,不然用户可能直接保存导致找不到图片数据而崩溃
        NSData * data;
        NSString * imageFilePath;
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        if (UIImagePNGRepresentation(customImage) == nil)
        {
            data = UIImageJPEGRepresentation(customImage, 1);
            imageFilePath = [NSString stringWithFormat:@"%@/custom@2x.jpg",docDir];
        } else
        {
            data = UIImagePNGRepresentation(customImage);
            imageFilePath = [NSString stringWithFormat:@"%@/custom@2x.png",docDir];
        }
        [data writeToFile:imageFilePath atomically:YES];
        // }
    self.imageView.image = customImage;

        //默认出现的字体，颜色，大小,和初始化显示的是一致的
        // 2. 保存字体名称
    if (self.currentFontName.length == 0) {
        self.currentFontName = @"Helvetica Light";
    }
    
        // 3. 保存字体大小
    if (self.currentFontSize == 0) {
        self.currentFontSize = 20;
    }
        // 4. 保存字体颜色
    if (!self.fontColorChanged) {
        self.fontColorR = 1.0;
        self.fontColorG = 1.0;
        self.fontColorB = 1.0;
    }

    /**
     *  custombackground位置设置
     */
    CGFloat deltaY = 49;
    if (iPhone5) {
        deltaY = 39;
    }
    if (iPhone4) {
        deltaY = 14;
    }

    CGFloat customBackGroundY = CGRectGetMaxY(self.keyboardRootView.frame) + deltaY;
    CustomBackGround * customBackGround = [[CustomBackGround alloc] initWithFrame:CGRectMake(0, customBackGroundY, self.view.frame.size.width, self.view.frame.size.height - customBackGroundY)];
    customBackGround.backgroundColor =  [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    self.customBackGround = customBackGround;
    [self.view addSubview:self.customBackGround];
    self.customBackGround.customBackgroundDelegate = self;

    /**
     *  customFont位置设置
     */

    CustomFont * customFont = [[CustomFont alloc] initWithFrame:customBackGround.frame];
    self.customFont = customFont;
    customFont.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    [self.view addSubview:self.customFont];
    self.customFont.hidden = YES;
    self.customFont.customFontDelegate = self;
    
    
    CGFloat colorSliderDeltaY = 110;
    if (iPhone5) {
        colorSliderDeltaY = 90;
    }
    if (iPhone4) {
        colorSliderDeltaY = 70;
    }
    UISlider* colorSlider = [[UISlider alloc]initWithFrame:CGRectMake(0, colorSliderDeltaY, 300, 20)];
    colorSlider.center = CGPointMake(self.view.center.x, colorSlider.center.y);
    [colorSlider addTarget:self action:@selector(colorChange) forControlEvents:UIControlEventValueChanged];
    colorSlider.continuous = true;
    colorSlider.minimumValue = 0;
    colorSlider.maximumValue = self.colorArray.count -1 ;
    colorSlider.value = 4.5;
    colorSlider.backgroundColor = [UIColor clearColor];
    colorSlider.maximumTrackTintColor = [UIColor whiteColor];
    colorSlider.minimumTrackTintColor = [UIColor whiteColor];
    UIImage* myImage =  [UIImage imageNamed:@"color_bar"];
    self.colorSlider = colorSlider;
    self.colorSlider.hidden = YES;
    UIImage* myImage2 = [myImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, myImage.size.width, 0, 0)];
    [colorSlider setMinimumTrackImage:myImage2 forState:UIControlStateNormal];
    [colorSlider setMaximumTrackImage:myImage2 forState:UIControlStateNormal];

    [colorSlider setThumbImage:[UIImage imageNamed:@"font_color_slide"] forState:UIControlStateNormal];
    self.colorSlider = colorSlider;
    [self.customFont addSubview:self.colorSlider];


    CGFloat textSliderDeltaY = 60;
    if (iPhone5) {
        textSliderDeltaY = 50;
    }
    if (iPhone4) {
        textSliderDeltaY = 30;
    }
    UISlider *textSlider = [[UISlider alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.colorSlider.frame) + textSliderDeltaY, 250, 20)];
    self.textSlider = textSlider;
    [self.customFont addSubview:textSlider];
    textSlider.center = CGPointMake(self.view.center.x, textSlider.center.y);
    [textSlider addTarget:self action:@selector(textChange) forControlEvents:UIControlEventValueChanged];
    textSlider.continuous = true;
    textSlider.minimumValue = 15;
    textSlider.maximumValue = 30;
    textSlider.value = 20;
    textSlider.backgroundColor = [UIColor clearColor];
    textSlider.maximumTrackTintColor = [UIColor whiteColor];
    textSlider.minimumTrackTintColor = [UIColor whiteColor];

    UIImage *minImage = [[UIImage imageNamed:@"font_slidebutton"]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(0, 2, 0, 2)];

    UIImage *maxImage = [UIImage imageNamed:@"font_slidebar"];
        //resizableImageWithCapInsets:UIEdgeInsetsZero];
    [textSlider setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [textSlider setMinimumTrackImage:minImage forState:UIControlStateNormal];

    UILabel * smallLabel = [[UILabel alloc] init];
    smallLabel.text = @"T";
    smallLabel.textAlignment = NSTextAlignmentCenter;
    smallLabel.textColor = [UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1];
    [self.customFont addSubview:smallLabel];
    smallLabel.font = [UIFont systemFontOfSize:15];
    CGFloat smallLabelX = CGRectGetMinX(textSlider.frame) - 40;
    smallLabel.frame = CGRectMake(smallLabelX, textSlider.frame.origin.y, 40, 40);
    smallLabel.center = CGPointMake(smallLabel.center.x, textSlider.center.y);

    
    UILabel * bigLabel = [[UILabel alloc] init];
    bigLabel.text = @"T";
    bigLabel.textAlignment = NSTextAlignmentCenter;
    bigLabel.textColor = [UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1];
    [self.customFont addSubview:bigLabel];
    bigLabel.font = [UIFont systemFontOfSize:30];
    CGFloat bigLabelX = CGRectGetMaxX(textSlider.frame);
    bigLabel.frame = CGRectMake(bigLabelX, textSlider.frame.origin.y, 40, 40);
    bigLabel.center = CGPointMake(bigLabel.center.x, textSlider.center.y);
    
    
    // 导航栏右侧保存按钮
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(0, 0, 40, 40);
    self.saveButton = saveButton;
    self.saveButton = saveButton;
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *itemRight = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    self.navigationItem.rightBarButtonItem = itemRight;


    yArray = [NSMutableArray array];
    for (UIView* view in self.view.subviews)
    {
        //view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height);

    [yArray addObject:[NSNumber numberWithFloat:view.frame.origin.y]];

    }

    
}

#pragma mark - 保存当前主题
- (void)saveButtonClicked:(UIButton *)saveButton
{
    if (self.transferThemeArray.count >= 10) {
        // 弹出Alert,询问是否保存
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"You can save less than 10 \nPlease delete some😂" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        self.deleteAlertView  = alert;
        [alert show];
    }
    else{[self showWithLabel:nil];}
}

- (void)showWithLabel:(id)sender {
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = @"Saving";
    [HUD showWhileExecuting:@selector(saveTask) onTarget:self withObject:nil animated:YES];
}

- (void)saveTask {
    // Do something usefull in here instead of sleeping ...
    sleep(2);
    [self saveCurrentTheme];
    
}
#pragma save  AlertView代理方法
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.deleteAlertView) {
    [self.navigationController popViewControllerAnimated:YES];}
}


- (void)saveCurrentTheme
{
    // 1. 生成图片,保存图片
    [self saveCustomImage];
    [self.navigationController popViewControllerAnimated:YES];
    // 发送一个通知,让Theme界面调整到custom界面,传一个数组是为了更新selectArray的数据
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ThemeViewControllerShowCustom" object:self.transferThemeArray];
}


- (void)textChange
{   // 1.改当前的预览字体大小
    for (UIButton * button in self.keyboardPre.characterKeys) {
        button.titleLabel.font = [UIFont fontWithName:self.currentFontName size:self.textSlider.value];
    }
    self.currentFontSize = self.textSlider.value;
}

-(void)saveCustomImage
{
    // 1. 获取当前的custom数量,注意数量比序列号多1
    int customThemeCount = (int)self.transferThemeArray.count;
    // 2. 存储背景图片
    // 2.1 创建文件夹
    NSFileManager * myFileManager = [NSFileManager defaultManager];
    NSString *docDir = [[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Themes"] stringByAppendingPathComponent:@"Custom"] ;
    NSString * fileName = [NSString stringWithFormat:@"custom_%d",customThemeCount];
    NSString * thumbnailImagePath = [docDir stringByAppendingPathComponent:fileName];
    NSError * error;
    NSString * currentThemePath = [docDir stringByAppendingPathComponent:fileName];
    if (![myFileManager fileExistsAtPath:currentThemePath]) {
        [myFileManager createDirectoryAtPath:currentThemePath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    [myFileManager createDirectoryAtPath:thumbnailImagePath withIntermediateDirectories:YES attributes:nil error:&error];
    
    for (UIView * view in self.view.subviews) {
        if (view.tag == 5201314) {
            // deprecated in iOS 7, we should not use it
            // UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, 0.0);
            UIGraphicsBeginImageContext(view.frame.size);
            //获取图像
            [view.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            NSData * data;
            if (UIImagePNGRepresentation(image) == nil) {
                data = UIImageJPEGRepresentation(image, 1);
            } else {
                data = UIImagePNGRepresentation(image);
            }
            // 缩略图
            NSString * imagePath = [currentThemePath stringByAppendingPathComponent:@"thumbnail@2x.png"];
            [data writeToFile:imagePath atomically:YES];
        }
        
    }
    
    
    
    // 取出图片
    NSString * imageFilePath;
    NSString *docDir1 = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    imageFilePath = [NSString stringWithFormat:@"%@/custom@2x.png",docDir1];
    // 把背景图片copy到当前目录下
    NSString * backImagePath = @"back@2x.png";
    [myFileManager copyItemAtPath:imageFilePath toPath:[currentThemePath stringByAppendingPathComponent:backImagePath] error:&error];
    UIImage * customImage = [UIImage imageWithContentsOfFile:imageFilePath];
    self.imageView.image = customImage;
    // 1.文字名称
    NSMutableDictionary * dictM = [NSMutableDictionary dictionary];
    dictM[@"FontName"] = self.currentFontName;
    // 2.文字颜色
    if (self.fontColorStr == nil) {
        self.fontColorStr = @"#ffffff";
    }
    dictM[@"FontColor"] = self.fontColorStr;
    if (self.currentFontSize == 0) {
        self.currentFontSize = 20;
    }
    // 3.文字大小
    dictM[@"FontSize"] = [NSString stringWithFormat:@"%f",self.currentFontSize];
    // 4.popUp颜色
    dictM[@"PopColor"] = @"#ffffff";
    
    NSString * plistStr = [currentThemePath stringByAppendingPathComponent:@"customInfo.plist"];
    [dictM writeToFile:plistStr atomically:YES];
    // 更新transferThemeArray数组
//    CKLog(@"fileName===%@",fileName);
    [self.transferThemeArray insertObject:fileName atIndex:0];
    [self.transferThemeArray writeToFile:[docDir stringByAppendingPathComponent:@"customThemeArray.plist"] atomically:YES];

}
#pragma customBackgroundDelegate
- (void)customBackgroundButtonClicked:(UIButton *)button
{
    if (0 == button.tag) {
        [self cameraClick];
    }
    else if(1 == button.tag)
    {
        [self photoClick];
    }
}

- (void)customBackgroundThemeButtonClicked:(UIButton *)button
{
//    CKLog(@"我来了");
    int buttonTAG = (int)button.tag;
    NSString * backImageName = [NSString stringWithFormat:@"background_bg_%02d",buttonTAG];
    UIImage * backImage = [UIImage imageNamed:backImageName];
    self.imageView.image = backImage;
    
    // 将背景图保存到本地,当Save的时候再存入AppGroup,保存的路径应当与保存照片的路径一致
    NSData * data;
    NSString * imageFilePath;
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    if (UIImagePNGRepresentation(backImage) == nil) {
        data = UIImageJPEGRepresentation(backImage, 1);
        imageFilePath = [NSString stringWithFormat:@"%@/custom@2x.jpg",docDir];
    } else {
        data = UIImagePNGRepresentation(backImage);
        imageFilePath = [NSString stringWithFormat:@"%@/custom@2x.png",docDir];
    }
    [data writeToFile:imageFilePath atomically:YES];
}

#pragma CustomFontDelegate
- (void)customFontButtonClicked:(UIButton *)button fontName:(NSString *)fontName
{
    if (self.currentFontSize == 0) {
        self.currentFontSize = 20;
    }
    // 1.改当前的预览字体
    for (UIButton * button in self.keyboardPre.characterKeys) {
         button.titleLabel.font = [UIFont fontWithName:fontName size:self.currentFontSize];
    }

        //self.keyboardPre.shiftButton.titleLabel.font = [UIFont fontWithName:fontName size:13];
    self.keyboardPre.altButton.titleLabel.font = [UIFont fontWithName:fontName size:13];
    self.keyboardPre.spaceButton.titleLabel.font = [UIFont fontWithName:fontName size:15];
    self.keyboardPre.returnButton.titleLabel.font = [UIFont fontWithName:fontName size:12];

    // 2.存储字体名称
    self.currentFontName = fontName;
    // 这里没有直接存储起来是因为用户可能只是试试,没必要在这里存储,当用户点击Save的时候再存储
    self.textSlider.value = self.currentFontSize;
    
}


-(void)colorChange
{

    int colorIndex = (int)self.colorSlider.value;
    NSString * currentColorStr = self.colorArray[colorIndex];
    self.fontColorStr = currentColorStr;
    UIColor* bgColor = [UIColor colorFromHexString:currentColorStr];
    for (UIButton* btn in self.keyboardPre.characterKeys) {
        [btn setTitleColor:bgColor forState:UIControlStateNormal];
    }
    self.fontColorChanged = YES;
    
    // 发送通知,让themeView刷新数据
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PreViewKeyboardButtonChangeColor" object:bgColor];
    
    
    
}

    //segment事件处理
-(void)localViewToCustomView:(UISegmentedControl*)seg
{

    NSInteger index = seg.selectedSegmentIndex;
    if (0 == index)
        {

        self.customBackGround.hidden = NO;
        self.colorSlider.hidden = YES;
        self.customFont.hidden = YES;
        }
    else if(1 == index)
        {

        self.customBackGround.hidden = YES;
        self.colorSlider.hidden = NO;
            self.customFont.hidden = NO;
        }

}


    //拍照作为背景
-(void)cameraClick
{
        // 拍照
    if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos])
        {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];


        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
            //controller.allowsEditing = YES;
        [self presentViewController:controller
                           animated:YES
                         completion:^(void){
                         }];
        }

    else{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Camera not available" message:[NSString stringWithFormat:@"Please allow %@ to access your device's camera in \"Setting\"->\"Privacy\"->\"Camera\"",APP_NAME] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
    }

}
    //选择照片作为背景
- (void)photoClick
{
    if ([self isPhotoLibraryAvailable])
        {
        //设置sourcetype为照片簿
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            //设置显示为图片
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;


            //自定义的剪裁controller必须设置为NO
            //controller.allowsEditing = YES;
        [self presentViewController:controller
                           animated:YES
                         completion:^(void){

                         }];
        }
    else
        {

        }

}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    portraitImg = [self imageByScalingToMaxSize:portraitImg];

    CGFloat cropH = 0;
        // 裁剪
    if (iPhone4) {
        cropH = 257;
    }
    
    if (iPhone5) {
         cropH = 257;
    }
    
    if (iPhone6) {
        cropH = 265;
    }
    
    if (iPhone6s) {
         cropH = 260;
    }
    VPImageCropperViewController *imgEditorVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(0, +226/2, self.view.frame.size.width, cropH) limitScaleRatio:3.0];
    imgEditorVC.delegate = self;

    [picker dismissViewControllerAnimated:NO completion:^{
        [self presentViewController:imgEditorVC animated:YES completion:nil];
    }];
}


#pragma mark image scale utility
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage
{
    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH)
        return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    if (sourceImage.size.width > sourceImage.size.height) {
        btHeight = ORIGINAL_MAX_WIDTH;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
    } else {
        btWidth = ORIGINAL_MAX_WIDTH;
        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
        {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;

        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;

            // center the image
        if (widthFactor > heightFactor)
            {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
            }
        else
            if (widthFactor < heightFactor)
                {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
                }
        }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;

    [sourceImage drawInRect:thumbnailRect];

    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)

        //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    self.imageView.image = editedImage;
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
            // TO DO
    }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:NO completion:^{
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}






#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable
{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable
{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos
{
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable
{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}





-(NSArray*)colorArray
{
    if (!_colorArray) {
        _colorArray = @[
                        @"#ff000000",
                        @"#ff373838",
                        @"#ff797979",
                        @"#ffbbbcbc",
                        @"#fffff5f5",
                        @"fffffffff",
                        @"#ffff0012",
                        @"#ffff005d",
                        @"#ffff00b1",
                        @"#fff300f3",
                        @"#ffb300ff",
                        @"#ff5f00ff",
                        @"#ff1806ff",
                        @"#ff003dff",
                        @"#ff008fff",
                        @"#ff00e3ff",
                        @"#ff00ffd4",
                        @"#ff00ff7c",
                        @"#ff00ff25",
                        @"#ff28ff02",
                        @"#ff75ff00",
                        @"#ffc9ff00",
                        @"#fffce500",
                        @"#ffff9900",
                        @"#ffff5100",
                        @"#ffff0111"
                        ];

    }
    return _colorArray;
}

    //这种方法没下种好
//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//
//    CKLog(@"view willAppear  %d",isFirst);
//    if (!isFirst)
//    {
//
//    for (int i = 0;i < self.view.subviews.count;i++)
//        {
//
//        NSNumber* number = yArray[i];
//        UIView* view = self.view.subviews[i];
//
//            view.frame = CGRectMake(view.frame.origin.x,[number floatValue]-64 , view.frame.size.width, view.frame.size.height);
//
//        }
//
//
//
//    }
//        isFirst = NO;
//
//}


-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];

        // CKLog(@"22view WilllayoutSubview frame = %@,bound = %@",NSStringFromCGRect(self.view.frame),NSStringFromCGRect(self.view.bounds));


        //刚开始self.view的y=0，之后变为64.
    if (0 ==self.view.frame.origin.y)
    {
        //根据记录的之前的subView的y值，减去64
    for (int i = 0;i < self.view.subviews.count;i++)
        {

        NSNumber* number = yArray[i];
        UIView* view = self.view.subviews[i];

        view.frame = CGRectMake(view.frame.origin.x,[number floatValue] , view.frame.size.width, view.frame.size.height);

        }
    }
    else
    {
        //根据记录的之前的subView的y值，减去64
        for (int i = 0;i < self.view.subviews.count;i++)
        {
            
            NSNumber* number = yArray[i];
            UIView* view = self.view.subviews[i];
            
            view.frame = CGRectMake(view.frame.origin.x,[number floatValue]-64 , view.frame.size.width, view.frame.size.height);
            
        }

    }
}


@end
