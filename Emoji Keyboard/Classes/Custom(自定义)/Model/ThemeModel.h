//
//  ThemeModel.h
//  Emoji Keyboard
//
//  Created by wangfeng on 14-12-24.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ThemeModel : NSObject

/**
 *  按键字体相关
 */
@property(nonatomic,strong)NSString* fontName;
@property(nonatomic,assign)CGFloat fontSize;
@property(nonatomic,strong)UIColor* fontColor;

/**
 *  按键背景
 */
@property(nonatomic,strong)UIImage* keyBgImage;
/**
 *  键盘背景
 */
@property(nonatomic,strong)UIImage* bgImage;





@end
