

#import "CKTabBar.h"
#import "CKTabBarButton.h"

//#define RED_DOT_W 7
@interface CKTabBar()
{
    NSURL * _jumpURL;
}
/**
 *  定义数组保存所有的选项卡按钮
 */
@property (nonatomic, strong) NSMutableArray *buttons;
//@property (nonatomic, strong) UIView * memeRedDotView;

/**
 *  定义属性保存选中按钮
 */
@property (nonatomic, weak) CKTabBarButton  * selectedBtn;
@end

@implementation CKTabBar

//- (UIView *)memeRedDotView
//{
//    if (_memeRedDotView == nil) {
//        _memeRedDotView = [[UIView alloc] init];
//        _memeRedDotView.backgroundColor = [UIColor redColor];
//        _memeRedDotView.layer.masksToBounds = YES;
//        _memeRedDotView.layer.cornerRadius = RED_DOT_W / 2;
//        NSUserDefaults * userDefauts = [NSUserDefaults standardUserDefaults];
//        NSString * memeHasClicked = [userDefauts objectForKey:@"ContainingmemeHasClicked"];
//        if ([memeHasClicked isEqualToString:@"ContainingmemeHasClicked"]) { // 如果meme已经被点击了,就不再显示
//            _memeRedDotView.hidden =YES;
//        }
//        else
//        {
//            _memeRedDotView.hidden = NO;
//        }
//        
//        
//        
//
//        
//
//}
//    return _memeRedDotView;
//}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // 1.设置默认的背景颜色
        [self setupBg];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardLaunchContaningApp:) name:@"CKKeyboardLaunchContaningApp" object:nil];
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CKRepleyUsersFeedback:) name:@"CKRepleyUsersFeedback" object:nil];
    }
    return self;
}
/**
 *  设置默认的背景颜色
 */
- (void)setupBg
{
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tabbar_background"]];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // 2.设置选项卡按钮的frame
    [self setupTabBarButtonFrame];
}
/**
 *  设置选项卡按钮的frame
 */
- (void)setupTabBarButtonFrame
{
    // 便利选项卡按钮设置frame
    int count = (int)self.buttons.count;
    CGFloat height = self.frame.size.height;
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
    CGFloat width = self.frame.size.width / 3;
#else
    CGFloat width = self.frame.size.width / 2;
#endif
    for (int i = 0; i < count; i++) {
        // 1.取出对应位置的按钮
        UIButton *btn = self.buttons[i];
        // 2.设置frame
        CGFloat btnH = height;
        CGFloat btnW = width;
        CGFloat btnY = 0;
        CGFloat btnX = i * btnW;

    
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
//        if (i == 1) {
//            self.memeRedDotView.frame = CGRectMake(btnW - RED_DOT_W , 0, RED_DOT_W, RED_DOT_W);
//            self.memeRedDotView.center = CGPointMake(btnW / 2 + RED_DOT_W, self.memeRedDotView.center.y);
//        }
    }
}

/**
 *  添加选项卡按钮
 */
- (void)addTabBarButton:(UITabBarItem *)item
{
    // 1.创建对应自控制器的按钮
    CKTabBarButton *btn = [[CKTabBarButton alloc] init];
    // 2.设置按钮显示的内容
    btn.item = item;
    
    // 监听按钮点击事件
    [btn addTarget:self action:@selector(btnOnClick:) forControlEvents:UIControlEventTouchDown];
    
    // 设置tag
    btn.tag = self.buttons.count;
    
//    if (btn.tag == 1) { // 说明是meme的按钮
//        [btn addSubview:self.memeRedDotView];
//    }
    
    // 3.添加按钮到当前view
    [self addSubview:btn];
    
    // 4.将刚刚创建的选项卡按钮添加到数组中
    [self.buttons addObject:btn];
    
    // 5.设置默认选中
    if (self.buttons.count == 1) {
        btn.selected = YES;
        self.selectedBtn= btn;
        [self btnOnClick:btn];
      }
}

/**
 *  监听选项卡按钮点击
 */
- (void)btnOnClick:(CKTabBarButton *)btn
{
    // 0.通知代理
    if ([self.delegate respondsToSelector:@selector(tabBar:didSelectFrom:to:withUrl:)]) {
        // 传递上一次选中的按钮的tag, 和当前选中按钮的tag
        [self.delegate tabBar:self didSelectFrom:self.selectedBtn.tag to:btn.tag withUrl:_jumpURL];
        _jumpURL = nil;
    }
    
    // 1.取消上次选中
    self.selectedBtn.selected = NO;
    // 2.选中本次
    btn.selected = YES;
    // 3.记录当前选中
    self.selectedBtn = btn;
    
//    if (btn.tag == 1) { // 如果点击的是meme
//        [CKSaveTools saveString:@"ContainingmemeHasClicked" forKey:@"ContainingmemeHasClicked" inRegion:CKSaveRegionContainingApp];
//        self.memeRedDotView.hidden =YES;
//    }
}

- (void)keyboardLaunchContaningApp:(NSNotification *)notification
{
    NSURL * url = notification.object;
    _jumpURL = url;
    if ([url.absoluteString isEqualToString:[NSString stringWithFormat:@"%@.setting", APP_ROOT_URL]]) {

#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
        [self btnOnClick:self.buttons[2]];
#else
        [self btnOnClick:self.buttons[1]];
#endif
    }
    else if ([url.absoluteString isEqualToString:[NSString stringWithFormat:@"%@.themes", APP_ROOT_URL]]) {
        [self btnOnClick:self.buttons[0]];
    }
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
    else if ([url.absoluteString isEqualToString:[NSString stringWithFormat:@"%@.meme", APP_ROOT_URL]])
    {
        [self btnOnClick:self.buttons[1]];
    }
#endif
    else
    {
        return;
    }
}

- (void)CKRepleyUsersFeedback:(NSNotification *)notification
{
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
    [self btnOnClick:self.buttons[2]];
#else
    [self btnOnClick:self.buttons[1]];
#endif
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CKSettingShouldPushFeedback" object:nil];
}
#pragma mark - 懒加载
- (NSMutableArray *)buttons
{
    if (_buttons == nil) {
        _buttons = [NSMutableArray array];
    }
    
    return _buttons;
}



@end
