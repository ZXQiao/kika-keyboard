
#import <UIKit/UIKit.h>
@class CKTabBar;

@protocol IWTabBarDelegate <NSObject>

- (void)tabBar:(CKTabBar *)tabBar didSelectFrom:(NSInteger)from to:(NSInteger)to withUrl:(NSURL *)url;

@end

@interface CKTabBar : UIView

/**
 *  提供一个方法给外加创建自定义IWTabBar的按钮
 *
 *  @param item 包含了(图片/选中图片/标题)的模型
 */
- (void)addTabBarButton:(UITabBarItem *)item;

@property (nonatomic, weak) id<IWTabBarDelegate>  delegate;
@end
