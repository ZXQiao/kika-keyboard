

#import "CKTabBarButton.h"

#define IWTabBarButtonRatio 0.6

@implementation CKTabBarButton

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // 1.设置图片居中
        self.imageView.contentMode = UIViewContentModeCenter;
        // 2.设置文字居中
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        // 3.设置文字大小
        self.titleLabel.font = [UIFont systemFontOfSize:11];
        
        // 设置默认状态按钮的文字颜色
        [self setTitleColor:[UIColor colorWithRed:153 / 255.0 green:153/ 255.0 blue:153/ 255.0 alpha:1] forState:UIControlStateNormal];
        // 设置选中状态按钮的文字颜色
        [self setTitleColor:[UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1] forState:UIControlStateSelected];
        
//        self.backgroundColor = [UIColor redColor];
//        self.contentEdgeInsets = UIEdgeInsetsMake(15, 10, 15, 10);
        


    }
    return self;
}

// 控制按钮上图片显示的位置
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageX = 0;
    CGFloat imageY = 0;
    // 让图片的高度是整个按钮高度的60%
    CGFloat imageH = self.frame.size.height * IWTabBarButtonRatio;
    CGFloat imageW = self.frame.size.width;
    
    return CGRectMake(imageX, imageY, imageW, imageH);
}

// 控制按钮上标题显示的位置
- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleX = 0;
    CGFloat titleY = self.frame.size.height * IWTabBarButtonRatio;
    CGFloat titleW = self.frame.size.width;
    CGFloat titleH = self.frame.size.height - titleY;
    return CGRectMake(titleX, titleY, titleW, titleH);
    
}

- (void)setItem:(UITabBarItem *)item
{
    _item = item;
    
    [self setTitle:item.title forState:UIControlStateNormal];
    [self setImage:item.image forState:UIControlStateNormal];
    [self setImage:item.selectedImage forState:UIControlStateSelected];
    
}

@end
