

#import <UIKit/UIKit.h>

@interface CKTabBarButton : UIButton


@property (nonatomic, strong) UITabBarItem *item;
@end
