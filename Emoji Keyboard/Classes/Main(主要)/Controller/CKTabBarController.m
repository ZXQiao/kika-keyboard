
#import "CKTabBarController.h"
#import "CKTabBar.h"
#import "CKNavigationController.h"
#import "LanguageTableViewController.h"
#import "SettingTableViewController.h"
#import "CKMainThemeViewController.h"
#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
#import "CKMemeViewController.h"
#import "CKMemeTabBarController.h"
#import "CKMemeLocalViewController.h"
#endif
#import "FeedBackController.h"


@interface CKTabBarController ()<IWTabBarDelegate>
/**
 *  自定义TabBar
 */
@property (nonatomic, weak) CKTabBar *customTabBar;
@end

@implementation CKTabBarController


- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardLaunchContaningApp:) name:@"CKKeyboardLaunchContaningApp" object:nil];
    }
    return self;
}

- (void)keyboardLaunchContaningApp:(NSNotification *)notification{
    NSURL *url=notification.object;
    if ([url.absoluteString isEqualToString:[NSString stringWithFormat:@"%@.rate", APP_ROOT_URL]])
    {//低评分跳转到反馈页面
        self.selectedIndex = tabBarClickedSetting;
        for (UIButton * ckTabBtn in self.customTabBar.subviews) {
            if(ckTabBtn.tag == 2){
                ckTabBtn.selected=YES;
                break;
            }
            ckTabBtn.selected = NO;
        }
        UINavigationController *nav= self.childViewControllers[2];
        
        FeedBackController * feedBack=[[FeedBackController alloc]init];
        [nav pushViewController:feedBack animated:YES];
    }else if ([url.absoluteString isEqualToString:[NSString stringWithFormat:@"%@.share", APP_ROOT_URL]]){
        //跳转到分享页面
        self.selectedIndex = tabBarClickedSetting;
        for (UIButton * ckTabBtn in self.customTabBar.subviews) {
            if(ckTabBtn.tag == 2){
                ckTabBtn.selected=YES;
                break;
            }
            ckTabBtn.selected = NO;
        }
        UINavigationController *nav= self.childViewControllers[2];
        for (UIViewController *viewController in nav.viewControllers) {
            if([viewController isKindOfClass:[SettingTableViewController class]]){
                SettingTableViewController *settingView=(SettingTableViewController *)viewController;
                [settingView shareMyApp];
            }
        }
        
    }
    
}

// init方法内部默认会调用initWithNibName这个方法
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {

        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldHiddenSELFTabBar) name:@"CKTabBarControllerShouldHiddenSelfTabBar" object:nil];

        // 添加主题的controller
        CKMainThemeViewController * myTheme = [[CKMainThemeViewController alloc]init];
        [self setupChileViewController:myTheme title:@"Themes" imageName:@"themes" selectedImageName:@"themes_pressed"];

#if (APP_VARIANT_NAME == APP_VARIANT_MEMOJI)
            // meme
            CKMemeTabBarController * meme = [[CKMemeTabBarController alloc] init];
            CKMemeLocalViewController *local = [[CKMemeLocalViewController alloc] init];
            CKMemeViewController * online =[[CKMemeViewController alloc] init];
            [meme addChildViewController:local];
            [meme addChildViewController:online];
            [self setupChileViewController:meme title:@"Meme" imageName:@"meme" selectedImageName:@"meme_pressed"];
#endif
        
        // setting
        SettingTableViewController *setting = [[SettingTableViewController alloc] init];
        [self setupChileViewController:setting title:@"Settings" imageName:@"settings" selectedImageName:@"settings_pressed"];

        [CKSaveTools saveClickWithEventType:Main_Launch key:Main_Launch];
    }
    return self;
}
/**
 *  初始化子控制器
 *
 *  @param child             需要初始化的子控制器
 *  @param title             需要设置的标题
 *  @param imageName         需要设置的默认状态的图片
 *  @param selectedImageName 需要设置的选中状态的图片
 */
- (void)setupChileViewController:(UIViewController *)child title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{

    child.title = title;
    child.tabBarItem.image = [[UIImage imageNamed:imageName]imageWithTintColor:[UIColor colorWithRed:153 / 255.0 green:153/ 255.0 blue:153/ 255.0 alpha:1]];
    
    UIImage *selectedImage = [UIImage imageNamed:selectedImageName];
    child.tabBarItem.selectedImage = selectedImage;
    
    // 包装一个导航控制器
    CKNavigationController *nav = [[CKNavigationController alloc] initWithRootViewController:child];
    
    // 1.3添加自定义控制器到TabBarController
    [self addChildViewController:nav];
    
    // 2.根据对应的子控制器创建子控制器对应的按钮
    [self.customTabBar addTabBarButton:child.tabBarItem];
    
    
}

- (void)shouldHiddenSELFTabBar
{
    [self.tabBar bringSubviewToFront:self.customTabBar];
}

#pragma mark - IWTabBarDelegate
- (void)tabBar:(CKTabBar *)tabBar didSelectFrom:(NSInteger)from to:(NSInteger)to withUrl:(NSURL *)url
{
    [CKSaveTools saveClickWithEventType:Main_TableSelected key:[NSString stringWithFormat:@"%d", (int)to]];
    self.selectedIndex = to;
    
    if (url) {
        if ([url.absoluteString isEqualToString:[NSString stringWithFormat:@"%@.themes", APP_ROOT_URL]]) {
            [self.childViewControllers[0].childViewControllers[0] setJumpURL:url];
        }
    }
}


- (CKTabBar *)customTabBar
{
    if (_customTabBar == nil) {
        // 1.创建自定义TabBar
        CKTabBar *customTabBar = [[CKTabBar alloc] init];
        customTabBar.frame = self.tabBar.bounds;
        customTabBar.delegate = self;
        [self.tabBar addSubview:customTabBar];
        self.customTabBar = customTabBar;
        self.tabBar.backgroundColor = [UIColor clearColor];
    }
    return _customTabBar;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    for (UIView *view in self.tabBar.subviews) {
        if ([view isKindOfClass:[UIControl class]]) {
            // 删除系统自带TabBar上的按钮
            [view removeFromSuperview];
        }
    }
    
//     CKLog(@"到底是为什么");
    
}

@end
