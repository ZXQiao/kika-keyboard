
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,tabBarClicked) {
    tabBarClickedThemes,
    tabBarClickedMeme,
    tabBarClickedSetting
};

@interface CKTabBarController : UITabBarController

@end
