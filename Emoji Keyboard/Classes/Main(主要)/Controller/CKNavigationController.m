

#import "CKNavigationController.h"

@interface CKNavigationController ()

@end

@implementation CKNavigationController

+ (void)initialize
{
    // 1.设置导航条的主题
    [self setupNavTheme];
}

/**
 *  设置导航条的主题
 */
+ (void)setupNavTheme
{
    // 1.1.拿到appearance主题
    UINavigationBar *navBar = [UINavigationBar appearance];
    // 设置导航栏的颜色
    [navBar setBarTintColor:[UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1]];
    // 设置backView的颜色
    [navBar setTintColor:[UIColor whiteColor]];
        // 2.设置导航条标题的属性
        NSMutableDictionary *md = [NSMutableDictionary dictionary];
        // 文字颜色
        md[NSForegroundColorAttributeName] = [UIColor blackColor];
        // 文字偏移位
        NSShadow *shadow = [[NSShadow alloc] init];
        shadow.shadowColor = [UIColor colorWithWhite:.0f alpha:1.f];
        shadow.shadowOffset = CGSizeMake(0, 0);
        md[NSShadowAttributeName] = shadow;
        // 文字字体大小
        md[NSFontAttributeName] = [UIFont systemFontOfSize:20];
        [navBar setTitleTextAttributes:md];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarController.hidesBottomBarWhenPushed = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{

    if (self.viewControllers.count > 0) {
        // 1.只有栈中有控制器的情况才需要隐藏工具条
        viewController.hidesBottomBarWhenPushed = YES;
    }
    // 第一次(根控制器)不需要隐藏工具条
    [super pushViewController:viewController animated:animated];
}


-(UIViewController *)popViewControllerAnimated:(BOOL)animated
{
        // CKLog(@"popViewController");
   return  [ super popViewControllerAnimated:animated];

}


- (void)leftItemClickTheme:(UIViewController * )viewController
{
        [self popViewControllerAnimated:YES];
    
  
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [CKBackend sendEventDataToBackend];
    [CKBackend sendMetaDataToBackend];
}

@end
