//
//  AppDelegate.h
//  Emoji Keyboard
//
//  Created by 张赛 on 14-9-20.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IKeyboardHelpController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IKeyboardHelpController *viewController;

@end

