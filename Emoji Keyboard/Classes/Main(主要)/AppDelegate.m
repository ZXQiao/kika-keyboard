//
//  AppDelegate.m
//  iKeyboard_iOS8
//
//  Created by 张赛 on 14-9-7.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//
#import <ShareSDK/ShareSDK.h>
#import "AppDelegate.h"
#import "Consetting.h"
#import "CKTabBarController.h"
#import "UMFeedback.h"
#import "UMOpus.h"
#import "SDWebImageManager.h"
#import "MMWormhole.h"
#import "UMessage.h"
#import "CKGuideVideoView.h"
#import "CKUncaughtExceptionHandler.h"
#import "UIKeyboardInputMode.h"
#import <objc/runtime.h>
#define EvaluateTime 90
NSInteger secondCountDown = 0;
//#define KEYBOARD_LANDSCAPE_HEIGHT 162
@interface AppDelegate ()

@property (nonatomic, strong) UINavigationController * nav;
@property (nonatomic, strong) NSTimer *countDownTimer;
@property (nonatomic, strong) MMWormhole *wormhole;

#if (APP_VARIANT_NAME != APP_VARIANT_MEMOJI)
@property (nonatomic, strong) UIButton * helpbutton;
@property (nonatomic, strong) UIButton * closebutton;
@property (nonatomic, strong) UIImageView * helpImageView;
#endif

@end
@implementation AppDelegate




- (void)initWormhole{
    self.wormhole = [[MMWormhole alloc] initWithApplicationGroupIdentifier:APP_GROUPS optionalDirectory:@"wormhole"];
    // Become a listener for changes to the wormhole for the button message
    [self.wormhole listenForMessageWithIdentifier:MemojiKey_Launch_ContainApp listener:^(id messageObject) {
        // The number is identified with the buttonNumber key in the message object
        NSString * eventKey = [messageObject valueForKey:UM_Event_Key];
        NSString * eventValue = [messageObject valueForKey:UM_Event_Value];
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY
                                           action:eventKey
                                            label:eventValue];
    }];
    

}
- (void)initializePlat{
//                          84c1ff3e9278
    [ShareSDK registerApp:@"7cb7a458527c"];
    //连接短信分享
    [ShareSDK connectSMS];
    /**
     连接Facebook应用以使用相关功能，此应用需要引用FacebookConnection.framework
     https://developers.facebook.com上注册应用，并将相关信息填写到以下字段
     **/
    [ShareSDK connectFacebookWithAppKey:FACEBOOK_APP_ID
                              appSecret:FACEBOOK_SECRET_ID];
    
    /**
     连接Twitter应用以使用相关功能，此应用需要引用TwitterConnection.framework
     https://dev.twitter.com上注册应用，并将相关信息填写到以下字段
     **/

    [ShareSDK connectTwitterWithConsumerKey:Twitter_Consumer_Key
                             consumerSecret:Twitter_Consumer_Secret
                                redirectUri:@"http://www.twitter.com/"];
    //连接邮件
    [ShareSDK connectMail];
    [ShareSDK connectWhatsApp];
    [ShareSDK connectCopy];
}

- (void)setUpUMPushWithLaunchOptions:(NSDictionary *)launchOptions
{
    [UMessage startWithAppkey:UMAppKey launchOptions:launchOptions];
    //register remoteNotification types （iOS 8.0及其以上版本）
    UIMutableUserNotificationAction *action1 = [[UIMutableUserNotificationAction alloc] init];
    action1.identifier = @"action1_identifier";
    action1.title=@"Accept";
    action1.activationMode = UIUserNotificationActivationModeForeground;//当点击的时候启动程序
    
    UIMutableUserNotificationAction *action2 = [[UIMutableUserNotificationAction alloc] init];  //第二按钮
    action2.identifier = @"action2_identifier";
    action2.title=@"Reject";
    action2.activationMode = UIUserNotificationActivationModeBackground;//当点击的时候不启动程序，在后台处理
    action2.authenticationRequired = YES;//需要解锁才能处理，如果action.activationMode = UIUserNotificationActivationModeForeground;则这个属性被忽略；
    action2.destructive = YES;
    
    UIMutableUserNotificationCategory *categorys = [[UIMutableUserNotificationCategory alloc] init];
    categorys.identifier = @"category1";//这组动作的唯一标示
    [categorys setActions:@[action1,action2] forContext:(UIUserNotificationActionContextDefault)];
    
    UIUserNotificationSettings *userSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert categories:[NSSet setWithObject:categorys]];
    [UMessage registerRemoteNotificationAndUserNotificationSettings:userSettings];
    [UMessage setLogEnabled:YES];
    
    //关闭状态时点击反馈消息进入反馈页
    NSDictionary *notificationDict = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    [UMFeedback didReceiveRemoteNotification:notificationDict];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkFinished:)
                                                 name:UMFBCheckFinishedNotification
                                               object:nil];
}


- (void)checkFinished:(NSNotification *)notification {
    CKLog(@"class checkFinished = %@", notification.object);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [UMessage registerDeviceToken:deviceToken];
    CKLog(@"umeng message alias is: %@", [UMFeedback uuid]);
    [UMessage addAlias:[UMFeedback uuid] type:[UMFeedback messageType] response:^(id responseObject, NSError *error) {
        if (error != nil) {
            CKLog(@"2222%@", error);
            CKLog(@"11111%@", responseObject);
        }
    }];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    /**
     *  这里要判断notification的type,如果是feedback则跳转到feedback的界面
     */
    NSString * noteType = userInfo[UM_PUSH_NOTE_TYPE];
    if (noteType == nil) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CKRepleyUsersFeedback" object:nil];
    }
    else if ([noteType isEqualToString:UM_PUSH_THEME]){
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@.themes", APP_ROOT_URL]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CKKeyboardLaunchContaningApp" object:url];
        [UMessage didReceiveRemoteNotification:userInfo];
    }
    else if ([noteType isEqualToString:UM_PUSH_MEME]){
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@.meme", APP_ROOT_URL]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CKKeyboardLaunchContaningApp" object:url];
        [UMessage didReceiveRemoteNotification:userInfo];
    }
    else{
        return;
    }
}

- (void)installUncaughtExceptionHandler
{
    InstallUncaughtExceptionHandler();
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {  
    NSArray * arr = [UITextInputMode activeInputModes];
    BOOL keyboardInActive=NO;//设置键盘是否在列表中的标示
    Class clsUIKeyboardInputMode=objc_getClass("UIKeyboardInputMode");
    Ivar idIver = class_getInstanceVariable(clsUIKeyboardInputMode, "identifier");
    for (id inputMode in arr) {
        NSString *identifier = object_getIvar(inputMode, idIver);
        if (identifier != NULL && [identifier isEqualToString:APP_IDENTIFIER]){
            keyboardInActive = YES;
            break;
        }
    }
    [self installUncaughtExceptionHandler];
    [self initializePlat]; // 分享功能
    [CKTracker sharedTraker];
    [UMFeedback setAppkey:UMAppKey];
    [self setUpUMPushWithLaunchOptions:launchOptions];
    [UMOpus setAudioEnable:YES];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    [application setStatusBarStyle:UIStatusBarStyleLightContent];//     设置状态栏字体为白色
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    //.设置根控制器
    CKTabBarController *tabBarVc =  [[CKTabBarController alloc] init];
    self.window.rootViewController = tabBarVc;
#pragma mark - 展示新特性,不要删除
    
    // 1.获取沙盒中的版本号
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //引导评分
    NSString *Evakey = @"evaluation";
    NSString *SecondKey = @"second";
    NSInteger secondCount = [defaults integerForKey:SecondKey];
    //根据之前的启动次数判断是否为新用户
    if( ![[NSUserDefaults standardUserDefaults]objectForKey:FIRST_LAUNCHED] ){
        //第一次启动
        if(secondCount == 0){
            //启动次数为0新用户
            [CKSaveTools saveString:NEW_USER forKey:USER_TYPE inRegion:CKSaveRegionAppGroup];
        }else{
            //不为0是老用户
            [CKSaveTools saveString:OLD_USER forKey:USER_TYPE inRegion:CKSaveRegionAppGroup];
        }
        [CKSaveTools saveString:@"Launched" forKey:FIRST_LAUNCHED inRegion:CKSaveRegionContainingApp];
    }
    
    [CKSaveTools saveInteger:1 forKey:@"UserAllowFullAccess" inRegion:CKSaveRegionAppGroup];
    
//    [self initWormhole];
#if (APP_VARIANT_NAME != APP_VARIANT_MEMOJI)
    [NSThread sleepForTimeInterval:1.0];
#endif
    [self.window makeKeyAndVisible];

    NSString *key = (__bridge_transfer NSString *)kCFBundleVersionKey;
    NSString *sandBoxVersion = [defaults valueForKey:key];
    // 2.获取当前软件的版本号
    NSDictionary *md =[NSBundle mainBundle].infoDictionary;
    NSString *currentVersion = [md objectForKey:@"CFBundleShortVersionString"];
    if(sandBoxVersion == nil) {
        
        NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
        [sharedDefaults setInteger:CKFullAccessTypeVideo forKey:Full_Access_Type];
        [sharedDefaults synchronize];
        
        [defaults setObject:currentVersion forKey:key]; // 存储当前版本号
        [defaults synchronize];
       
        if( !keyboardInActive ){
            [self showHelp];
        }
        
    }
    if (sandBoxVersion && [currentVersion compare:sandBoxVersion] ==  NSOrderedDescending){ // 第一次使用这个版本或者升级了
        [defaults setObject:currentVersion forKey:key]; // 存储当前版本号
        [defaults synchronize];
        
        CKLog(@"showhelp");
        //        CKGuideVideoView * guideVideoView = [CKGuideVideoView]
        //        NSFileManager * myFileManager = [NSFileManager defaultManager];
        //        NSString * docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        //        NSString * resource = [docDir stringByAppendingPathComponent:@"Resource"];
        //        NSString * onlineTheme = [docDir stringByAppendingString:@"OnlineTheme"];
        //        NSError * error;
        //        [myFileManager removeItemAtPath:resource error:&error];
        //        [myFileManager removeItemAtPath:onlineTheme error:&error];
        //
        //        NSURL *containerURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS];
        //        containerURL = [containerURL URLByAppendingPathComponent:@"Library"];
        //
        //        NSURL * onlineURL = [containerURL URLByAppendingPathComponent:@"OnlineThemes"];
        //        NSURL * resourceURL = [containerURL URLByAppendingPathComponent:@"Resource"];
        //        [myFileManager removeItemAtURL:onlineURL error:&error];
        //        [myFileManager removeItemAtURL:resourceURL error:&error];
    }
    else{
        // 不是第一次使用当前版本 --> 不作操作
    }
    
    return YES;
}

#if (APP_VARIANT_NAME != APP_VARIANT_MEMOJI)
-(void)showHelp{
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    UIImageView *helpImageView = [[UIImageView alloc] initWithFrame:window.frame];
    helpImageView.userInteractionEnabled = YES;
    self.helpImageView = helpImageView;
    CGRect frame = window.frame;
    CGFloat x = frame.size.width / 5;
    CGFloat y = frame.size.height * 5 / 6;
    CGFloat w = frame.size.width - 2 * x;
    CGFloat h = (frame.size.height - y) - 30;
    UIButton *helpButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, w, h)];
    self.helpbutton = helpButton;
    UIButton *closeButton;
    UIImage *HelpImage;
    
    if (iPhone4) {
        HelpImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ios9_4.png" ofType:nil ]];
        closeButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width - 50, 0, 50, 50)];
        [closeButton setContentEdgeInsets:UIEdgeInsetsMake(6.5, 6.5, 6.5, 6.5)];
    }
    else if (iPhone5){
        HelpImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ios9_5.png" ofType:nil ]];
        closeButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width - 50, 0, 50, 50)];
        [closeButton setContentEdgeInsets:UIEdgeInsetsMake(6.5, 6.5, 6.5, 6.5)];
    }else if (iPhone6){
        HelpImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ios9_6.png" ofType:nil ]];
        closeButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width - 50, 0, 50, 50)];
        
        [closeButton setContentEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    }else if (iPhone6s){
        HelpImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ios9_6+.png" ofType:nil ]];
        closeButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width - 60, 0, 60, 60)];
        [closeButton setContentEdgeInsets:UIEdgeInsetsMake(9, 9, 9, 9)];
    }
    
    UIImage *closeImage = [[UIImage alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"help_guide_close@2x.png" ofType:nil]];
    [closeButton setImage:[closeImage imageWithTintColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
    
    self.closebutton = closeButton;
    //    [closeButton setBackgroundColor:[UIColor redColor]];
    [window addSubview:helpImageView];
    [self.helpImageView addSubview:helpButton];
    [self.helpImageView addSubview:closeButton];
    
    helpImageView.image = HelpImage ;
    [helpButton addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)buttonPress:(UIButton *)button
{
    [self.helpbutton removeFromSuperview];
    self.helpbutton = nil;
    [self.helpImageView removeFromSuperview];
    self.helpImageView = nil;
    [self.closebutton removeFromSuperview];
    self.closebutton = nil;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}
#else
-(void)showHelp{
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    CKGuideVideoView *helpVideo = [[CKGuideVideoView alloc]initWithFrame:window.bounds];
    [window addSubview:helpVideo];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}
#endif



-(void)timeFireMethod
{
    secondCountDown++;
    [CKSaveTools saveInteger:secondCountDown forKey:@"second" inRegion:CKSaveRegionExtensionKeyboard];
    if (secondCountDown == EvaluateTime) {
        [self.countDownTimer invalidate];
        self.countDownTimer = nil;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        int EvaCount;
        if (buttonIndex == 0)//取消
        {
            //更改为1
            EvaCount = 1;
        }
        else if (buttonIndex == 2)
        {
            EvaCount = 2;
        }
        else//评分
        {
            NSString* evaStr = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?mt=8&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software&id=%@",APPID];
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:evaStr]];
            //更改计数为1
            EvaCount = 1;
        }
        [CKSaveTools saveInteger:EvaCount forKey:@"evaluation" inRegion:CKSaveRegionContainingApp];
        [CKSaveTools saveInteger:secondCountDown forKey:@"second" inRegion:CKSaveRegionContainingApp];
}
    else
    {
        if (0 == buttonIndex) {return;}
        if (1 == buttonIndex){

            NSString *uRLStr = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/cool-keyboard-themes-personlize/id%@?mt=8",APPID];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:uRLStr]];
        }
    }
}

- (BOOL)application:(UIApplication *)application shouldAllowExtensionPointIdentifier:(NSString *)extensionPointIdentifier
{
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL ret = [ShareSDK handleOpenURL:url
                     sourceApplication:sourceApplication
                            annotation:annotation
                            wxDelegate:self];
    if(ret) {
        return [ShareSDK handleOpenURL:url
                     sourceApplication:sourceApplication
                            annotation:annotation
                            wxDelegate:self];
    }
    else
    {
        if ([url.absoluteString isEqualToString:[NSString stringWithFormat:@"%@.guide", APP_ROOT_URL]]) {
            NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
            [sharedDefaults setInteger:CKFullAccessTypeTutorial forKey:Full_Access_Type];
            [sharedDefaults synchronize];
            [self showHelp];
            return  YES;
        }
//        else if([url.absoluteString hasPrefix:@"memojikey://send."])
//        {
//            [self sendMessageToWXWithURL:url];
//            return YES;
//        }
        
        
        // 从键盘跳转过来要发送通知
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CKKeyboardLaunchContaningApp" object:url];
        
        return YES;
    }
}

//- (void)sendMessageToWXWithURL:(NSURL *)url
//{
//    [self sendGifContent];
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // 向操作系统申请后台运行的资格，能维持多久，是不确定的

    __block UIBackgroundTaskIdentifier task = [application beginBackgroundTaskWithExpirationHandler:^{
        // 当申请的后台运行时间已经结束（过期），就会调用这个block
        if (task) {
            [application endBackgroundTask:task];
        }
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    // 发送通知,让themeView刷新数据
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ThemeViewSholdRloadData" object:nil];
    [CKSaveTools saveInteger:1 forKey:@"UserAllowFullAccess" inRegion:CKSaveRegionAppGroup];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
// 观察者的注销
-(void)dealloc{[[NSNotificationCenter defaultCenter] removeObserver:self];}

// 内存警告,清楚缓存
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    SDWebImageManager *mgr = [SDWebImageManager sharedManager];
    // 1.取消下载
    [mgr cancelAll];
    
    // 2.清除内存中的所有图片
    [mgr.imageCache clearMemory];
}

- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
}



@end