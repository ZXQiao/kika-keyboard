//
//  ProvicyViewController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 14-10-1.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//

#import "ProvicyViewController.h"
#import "Colours.h"
@interface ProvicyViewController ()<UIWebViewDelegate>
@property (strong, nonatomic) UIWebView *webView;
@end

@implementation ProvicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Privacy";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;
    [self showWebView];
}


- (void)showWebView
{
    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.webView];
    self.webView.delegate = self;
    NSString * htmlPath = [[NSBundle mainBundle] pathForResource:@"Privacy.html" ofType:nil];
    NSError * error;
    NSString * html = [[NSString alloc] initWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:&error];
    NSString * cssPath = [[NSBundle mainBundle] pathForResource:@"guide.css" ofType:nil];
    html = [html stringByReplacingOccurrencesOfString:@"<!--CssPath#0-->" withString:cssPath];
    NSString * jsPath = [[NSBundle mainBundle] pathForResource:@"guide.js" ofType:nil];
    html = [html stringByReplacingOccurrencesOfString:@"<!--JsPath#0-->" withString:jsPath];
    html = [html stringByReplacingOccurrencesOfString:@"<!--AppName#0-->" withString:APP_NAME];
    [self.webView loadHTMLString:html baseURL:nil];
                       
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString isEqualToString:@"http://loveemojikeyboard.blogspot.jp/2014/10/emoji-keyboard-privacy-policy-1.html"]) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return  NO;
    }
    else
    {
    return YES;
    }
}


@end
