function howToEnableKeyboard() {
    window.open("pinssiblefancykeyboard://howtoenablekeyboard");
}

function viewWebsite() {
    window.open("http://loveemojikeyboard.blogspot.jp/2014/10/emoji-keyboard-privacy-policy-1.html");
}

function floatStepIndicator() {
    var panelWidgets = document.getElementsByClassName("panel");
    
    var bodyWidget = document.getElementsByTagName("body")[0];
    if (bodyWidget.offsetHeight <= window.innerHeight) {
        return;
    }
    
    var indicatorHTML = "<div class='step'><img class='stepImg' src='./step_3x.png'></div>";

    for (var i = panelWidgets.length - 1; i >= 1; i--) {
        indicatorHTML += "<div class='step'><img class='stepImg' src='./step_3x.png'></div>";
    };
    
    var stepIndicatorWidget = document.getElementsByClassName("step-indicator")[0];
    stepIndicatorWidget.innerHTML = indicatorHTML;
    stepIndicatorWidget.style.top = (window.innerHeight - panelWidgets.length * 12) / 2 + "px";
    
    var stepsWidgets = document.getElementsByClassName("step");
    var lastSkeletonStep = stepsWidgets[0];
    lastSkeletonStep.innerHTML = "<img class='stepImg' src='./activestep_3x.png'>";

    function isScrolledIntoView(elem) {
        var docViewTop = window.pageYOffset;
        var docViewBottom = docViewTop + window.innerHeight;

        var elemTop = elem.offsetTop;
        var elemBottom = elemTop + elem.offsetHeight;

        var largeThanViewPort = (elemTop <= docViewTop) && (elemBottom >= docViewBottom);
        var elemTopInViewPot = (elemTop <= docViewBottom) && (elemTop >= docViewTop);

        return largeThanViewPort || elemTopInViewPot;
    }

    function scrollListener() {
        var hasSkeleton = false;
        for (var i = 0; i < panelWidgets.length; i++) {
            if (isScrolledIntoView(panelWidgets[i]) && !hasSkeleton) {
                stepsWidgets[i].innerHTML = "<img class='stepImg' src='./activestep_3x.png'>";
                lastSkeletonStep = stepsWidgets[i];
                hasSkeleton = true;
            } else {
                stepsWidgets[i].innerHTML = "<img class='stepImg' src='./step_3x.png'>";
            };
        };

        if (!hasSkeleton) {
            lastSkeletonStep.innerHTML = "<img class='stepImg' src='./activestep_3x.png'>";
        };
    }
    
    // window.onscroll = scrollListener;
    window.addEventListener("scroll", scrollListener , 300);
}

// Firefox considers the whitespace between element nodes to be text nodes (whereas IE does not)
function nextSibling(elem) {
    do {
        elem = elem.nextSibling;
    } while (elem && elem.nodeType !== 1);
    return elem;
}

function toggleNextPanelBody(obj) {
    var el = obj;
    do {
        el = nextSibling(el);
        if (el && el.className === "panel-body" ) {
            if (el.style.display != 'none') {
                el.style.display = 'none';
                obj.className = "panel-heading panel-heading-without-body";
            } else {
                el.style.display = '';
                obj.className = "panel-heading";
            }
            
            //floatStepIndicator();
        }
        else
            break;
    } while (true);
}

