//
//  CKSoundsCell.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/1.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKSoundsCell.h"
#import "CKSoundsModel.h"
@interface CKSoundsCell()

@property (nonatomic, strong) UILabel * soundsNameLabel;
@property (nonatomic, strong) UIImageView * selectedImageView;
@property (nonatomic, strong) UILabel * divisionLabel;

@end

@implementation CKSoundsCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIColor * color = [UIColor colorFromHexString:@"#f0f4f6"];
        
        UILabel * soundsNameLabel = [[UILabel alloc] init];
        self.soundsNameLabel = soundsNameLabel;
        [self.contentView addSubview:soundsNameLabel];
        soundsNameLabel.textColor = [UIColor colorFromHexString:@"#757576"];
        soundsNameLabel.backgroundColor = color;
        soundsNameLabel.font = [UIFont systemFontOfSize:14];
        
        UIImageView * selectedImageView = [[UIImageView alloc] init];
        self.selectedImageView = selectedImageView;
        [self.contentView addSubview:selectedImageView];
        selectedImageView.image = [UIImage imageNamed:@"sound_on"];
        selectedImageView.hidden = YES;
        selectedImageView.backgroundColor = color;
        
        UILabel * divisionLabel = [[UILabel alloc] init];
        self.divisionLabel = divisionLabel;
        [self addSubview:divisionLabel];
        divisionLabel.backgroundColor = [UIColor colorFromHexString:@"#e5e5e5"];

        self.contentView.backgroundColor = color;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat leftPadding = 30.0;
    CGFloat x = leftPadding;
    CGFloat y = 0;
    CGFloat h = self.contentView.frame.size.height - 1;
    CGFloat w = 220;
    CGRect frame = CGRectMake(x, y, w, h);
    self.soundsNameLabel.frame = frame;
    
    CGFloat rightPadding = 36;
    CGFloat selectW = 25;
    CGFloat selectH = 25;
    CGFloat rightX = self.contentView.frame.size.width - selectW - rightPadding;
    
    self.selectedImageView.frame = CGRectMake(rightX, 0, selectW, selectH);
    self.selectedImageView.center = CGPointMake(self.selectedImageView.center.x, self.contentView.center.y);
    CGFloat divisionX = leftPadding;
    CGFloat divisionY = self.contentView.frame.size.height - 1;
    CGFloat divisionH = 1;
    CGFloat divisionW = self.contentView.frame.size.width - leftPadding - rightPadding;
    self.divisionLabel.frame = CGRectMake(divisionX, divisionY, divisionW, divisionH);
    
    
}
- (void)setCellModel:(CKSoundsModel *)cellModel
{
    _cellModel = cellModel;
    self.soundsNameLabel.text = cellModel.name;
        self.selectedImageView.hidden = !cellModel.selected;

    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
