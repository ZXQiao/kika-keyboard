//
//  CKSoundsCell.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/1.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CKSoundsModel;
@interface CKSoundsCell : UITableViewCell
@property (nonatomic, strong)CKSoundsModel * cellModel;

@end
