//
//  CKSoundsModel.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/1.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKSoundsModel.h"

@implementation CKSoundsModel

+(NSArray *)soundsModelArrayWithPlistName:(NSString *)plistName
{
    NSArray * array = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"]];
    NSMutableArray * modelArray = [NSMutableArray array];
    for (NSString * string in array) {
        CKSoundsModel * model = [[CKSoundsModel alloc] init];
        model.name = string;
        model.selected = NO;
        if ([plistName isEqualToString:@"Sounds"]) {
            model.soundsType = CKSoundSTypeVoice;
        }
        else
        {
            model.soundsType = CKSoundSTypeMusic;
        }
        
        [modelArray addObject:model];
    }
    return (NSArray *)modelArray;
}


@end
