//
//  CKSoundsModel.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/1.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKSoundsModel : NSObject
@property (nonatomic, strong) NSString * name;
@property (nonatomic, assign) CKSoundSType soundsType;
@property (nonatomic, assign) BOOL selected;

+(NSArray *)soundsModelArrayWithPlistName:(NSString *)plistName;
@end
