//
//  CKSoundsViewController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/5/1.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKSoundsViewController.h"
#import "CKSoundsModel.h"
#import "CKSoundsCell.h"
#import "SoundManager.h"

@interface CKSoundsViewController ()
@property (nonatomic, strong) NSArray * voiceModelArray;
@property (nonatomic, strong) NSArray * musicModelArray;
@property (nonatomic, strong) NSMutableArray * currentArray;
@property (nonatomic, assign) CKSoundSType currentSoundsType;
@property (nonatomic, assign) int currentSoundIndex;
@property (nonatomic, assign) BOOL notFirstComeIn;
@property (nonatomic, assign) int voiceIndex; // 从1开始,避免与默认值0冲突
@property (nonatomic, assign) int musicIndex;



@end

@implementation CKSoundsViewController

- (NSMutableArray *)currentArray{
    if (_currentArray == nil) {
        _currentArray = [NSMutableArray array];
    }
    return _currentArray;
}
- (NSArray *)voiceModelArray{
    if (_voiceModelArray == nil) {
        _voiceModelArray = [CKSoundsModel soundsModelArrayWithPlistName:@"Sounds"];
        if (!self.voiceIndex && !self.musicIndex) {
                self.voiceIndex = 1;
            CKSoundsModel * model = _voiceModelArray[self.voiceIndex -1 ];
            model.selected = YES;
        }
        else{
            if (self.voiceIndex) {
                CKSoundsModel * model = _voiceModelArray[self.voiceIndex -1 ];
                
                model.selected = YES;
            }
        }
        
    }
    return _voiceModelArray;
}
- (NSArray *)musicModelArray{
    if (_musicModelArray == nil) {
        _musicModelArray = [CKSoundsModel soundsModelArrayWithPlistName:@"Music"];

        if (self.musicIndex) {
            CKSoundsModel * model = _musicModelArray[self.musicIndex -1 ];
            model.selected = YES;
        }

    }
    return _musicModelArray;
}

- (void)loadView
{
    [super loadView];
    
    NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //add it for changing default keyboard sound to off
    //need to change back next version
    NSString * clearFlag = [sharedDefaults stringForKey:@"clearFlag"];
    if (clearFlag == nil) {
        [sharedDefaults setInteger:1 forKey:@"VoiceIndex"];
        [sharedDefaults setInteger:0 forKey:@"MusicIndex"];
        [sharedDefaults setObject:@"YES" forKey:@"clearFlag"];
        [sharedDefaults synchronize];
    }

    int musicIndex = (int)[sharedDefaults integerForKey:@"MusicIndex"];
    int voiceIndex = (int)[sharedDefaults integerForKey:@"VoiceIndex"];
    //fullAccess 打开
    if ((musicIndex || voiceIndex))
    {
        int type = (int)[sharedDefaults integerForKey:@"CurrentSoundsType"];
        self.currentSoundsType = type;
        
        int voiceIndex = (int)[sharedDefaults integerForKey:@"VoiceIndex"];
        self.voiceIndex = voiceIndex;
        
        int musicIndex = (int)[sharedDefaults integerForKey:@"MusicIndex"];
        self.musicIndex = musicIndex;
        
        NSString *SoundName = [sharedDefaults objectForKey:@"SoundsName"];
        if (voiceIndex != 0 && musicIndex == 0) {
            [defaults setObject:SoundName forKey:@"SoundsName"];
            [defaults setInteger:voiceIndex forKey:@"VoiceIndex"];
            [defaults setInteger:0 forKey:@"MusicIndex"];
            [defaults synchronize];
        }else if (musicIndex != 0 && voiceIndex == 0)
        {
            [defaults setObject:SoundName forKey:@"SoundsName"];
            [defaults setInteger:0 forKey:@"VoiceIndex"];
            [defaults setInteger:musicIndex forKey:@"MusicIndex"];
            [defaults synchronize];
        }
        self.currentSoundIndex = musicIndex ? musicIndex - 1 : voiceIndex - 1;
        self.currentArray = (NSMutableArray *)self.voiceModelArray;
    }
    else //fullacess没打开
    {
        if (!self.notFirstComeIn) {
            int type = (int)[defaults integerForKey:@"CurrentSoundsType"];
            self.currentSoundsType = type;
            
            int voiceIndex = (int)[defaults integerForKey:@"VoiceIndex"];
            self.voiceIndex = voiceIndex;
            
            int musicIndex = (int)[defaults integerForKey:@"MusicIndex"];
            self.musicIndex = musicIndex;
            
            if (!voiceIndex && !musicIndex) {
                voiceIndex = 1;
            }
            self.currentSoundIndex = musicIndex ? musicIndex - 1 : voiceIndex - 1;
            self.currentArray = (NSMutableArray *)self.voiceModelArray;
            
            CKSoundsModel * lastModel = _musicModelArray[voiceIndex - 1];
            NSString *SoundName = lastModel.name;
            
            [defaults setObject:SoundName forKey:@"SoundsName"];
            [defaults setInteger:voiceIndex forKey:@"VoiceIndex"];
            [defaults setInteger:0 forKey:@"MusicIndex"];
            [defaults synchronize];
        }
        else
        {
            // 取出存起来的信息
            int type = (int)[defaults integerForKey:@"CurrentSoundsType"];
            self.currentSoundsType = type;
            
            int voiceIndex = (int)[defaults integerForKey:@"VoiceIndex"];
            self.voiceIndex = voiceIndex;
            
            int musicIndex = (int)[defaults integerForKey:@"MusicIndex"];
            self.musicIndex = musicIndex;
            
            int ShareType = (int)[sharedDefaults integerForKey:@"CurrentSoundsType"];
            
            int ShareVoiceIndex = (int)[sharedDefaults integerForKey:@"VoiceIndex"];
            
            int ShareMusicIndex = (int)[sharedDefaults integerForKey:@"MusicIndex"];
            
            if ((ShareType != type || ShareVoiceIndex != voiceIndex || ShareMusicIndex != musicIndex)  && self.currentArray.count != 0) {
                CKSoundsModel * lastModel;
                if (type == CKSoundSTypeVoice) {
                    lastModel = self.voiceModelArray[voiceIndex - 1];
                }
                else{
                    lastModel = self.musicModelArray[musicIndex - 1];
                }
                //            lastModel.selected = NO;
                NSString * soundName = lastModel.name;
                [sharedDefaults setObject:soundName forKey:@"SoundsName"];
                [sharedDefaults setInteger:voiceIndex forKey:@"VoiceIndex"];
                [sharedDefaults setInteger:musicIndex forKey:@"MusicIndex"];
                [sharedDefaults synchronize];
                //            }
            }
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.currentSoundsType = CKSoundSTypeVoice;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =  [UIColor colorFromHexString:@"#f0f4f6"];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Sounds";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:self.currentSoundsType forKey:@"CurrentSoundsType"];
    [defaults synchronize];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.currentArray.count;
}


-(CKSoundsCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * ID = @"SoundCell";
    CKSoundsCell * cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[CKSoundsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    CKSoundsModel * model = self.currentArray[indexPath.row];
    cell.cellModel = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CKSoundsModel * currentModel = self.currentArray[indexPath.row];
    currentModel.selected = YES;
    NSString * soundName = currentModel.name;
    
    // 友盟统计主题选中
    NSString * key ;
    if (self.currentSoundsType == CKSoundSTypeVoice) {
        key = Main_SoundVoice;
    }
    else if (self.currentSoundsType == CKSoundSTypeMusic)
    {
        key = Main_SoundMusic;
    }
    if (key) {
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY
                                           action:key
                                            label:soundName];

        [CKSaveTools saveClickWithEventType:key key:soundName];
    }
    else{
        CKLog(@"友盟统计Sounds的key为空,出现异常-----I'm in SKSoundsViewController");
    }
    
    
    SoundManager * soundManager = [SoundManager sharedManager];
    soundManager.allowsBackgroundMusic = YES;
    soundManager.soundVolume = .1; // 设置声音大小

    if (self.currentSoundsType == CKSoundSTypeVoice) {
        NSString * bundlePath =[[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:@"Sounds.bundle"];
        NSString * soundPath = [NSString stringWithFormat:@"%@/%@.caf",bundlePath,soundName];
        [soundManager playSound:soundPath looping:NO];
    }
    else
    {
        NSError * error;
        NSString * musicString = [[NSString alloc] initWithContentsOfFile:
                                  [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@.txt",soundName] ofType:nil ]encoding:NSUTF8StringEncoding error:&error];

        NSArray * musicArray = [musicString componentsSeparatedByString:@","];
        NSString * firstMusical = musicArray[0];
        NSString * bundlePath =[[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:@"Piano.bundle"];
        NSString * musicPath = [NSString stringWithFormat:@"%@/%@.mp3",bundlePath,firstMusical];
        [soundManager playSound:musicPath looping:NO];
    }

    NSString * keyString;
    NSString * resetString;
    //选中的行号
    int selectedIndex = (int)indexPath.row;
    if (self.currentSoundsType == CKSoundSTypeVoice) {//当前页面
        keyString = @"VoiceIndex";
        resetString = @"MusicIndex";
        if (self.voiceIndex) { // 如果统一
            if (self.voiceIndex - 1 == selectedIndex) { // 选择的是同一个
                
            }
            else
            {
                CKSoundsModel * beforeModel = self.currentArray[self.currentSoundIndex];
                beforeModel.selected = NO;
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.currentSoundIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        }
        else // 如果不统一,说明之前是music
        {
            self.musicIndex = 0;
            CKSoundsModel * beforeModel = self.musicModelArray[self.currentSoundIndex];
            beforeModel.selected = NO;
        }
        self.voiceIndex = selectedIndex + 1;
        self.currentSoundIndex = selectedIndex;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    else
    {
        keyString = @"MusicIndex";
        resetString = @"VoiceIndex";
        if (self.musicIndex) { // 如果统一
            if (self.musicIndex - 1 == selectedIndex) { // 选择的是同一个
                
            }
            else
            {
                CKSoundsModel * beforeModel = self.currentArray[self.currentSoundIndex];
                beforeModel.selected = NO;
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.currentSoundIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
        else // 如果不统一,说明之前是voice
        {
            self.voiceIndex = 0;
            CKSoundsModel * beforeModel = self.voiceModelArray[self.currentSoundIndex];
            beforeModel.selected = NO;
            
        }
        self.musicIndex = selectedIndex + 1;
        self.currentSoundIndex = selectedIndex;

        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    if([CKCommonTools isOpenAccessGranted]){
        [sharedDefaults setObject:soundName forKey:@"SoundsName"];
        [sharedDefaults setInteger:selectedIndex + 1 forKey:keyString];
        [sharedDefaults setInteger:0 forKey:resetString];
        [sharedDefaults synchronize];
        
        [defaults setInteger:0 forKey:resetString];
        [defaults setInteger:selectedIndex + 1 forKey:keyString];
        [defaults synchronize];
    }
    else{
        [defaults setObject:soundName forKey:@"SoundsName"];
        [defaults setInteger:selectedIndex + 1 forKey:keyString];
        [defaults setInteger:0 forKey:resetString];
        [defaults synchronize];
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    UIColor * color = [UIColor colorFromHexString:@"#f0f4f6"];
    view.backgroundColor = color;
    
    UISegmentedControl* segment = [[UISegmentedControl alloc]initWithItems:@[@"Voice ",@"Music"]];
    [view addSubview:segment];
    [segment sizeToFit];
    segment.tintColor = [UIColor colorWithRed:60/255.0 green:155/255.0 blue:237/255.0 alpha:1];
    
    segment.center = view.center;
    [segment addTarget:self action:@selector(changeTableView:) forControlEvents:UIControlEventValueChanged];

    
    if (!self.notFirstComeIn) {
        segment.selectedSegmentIndex = 0;
    }
    else
    {
        segment.selectedSegmentIndex = self.currentSoundsType;
    }

    return view;

}



- (void)changeTableView:(UISegmentedControl*)seg
{
    int segIndex = (int)seg.selectedSegmentIndex;
    CKSoundSType type = CKSoundSTypeNone;
    self.notFirstComeIn = YES;
    switch (segIndex) {
        case 0:
        {
            type = CKSoundSTypeVoice;
            self.currentArray = (NSMutableArray *)self.voiceModelArray;
        }
            break;
        case 1:{
             type =CKSoundSTypeMusic;
            self.currentArray = (NSMutableArray *)self.musicModelArray;
        }
            break;
        default:
            break;
    }
    self.currentSoundsType = type;
    
    [self.tableView reloadData];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:self.currentSoundsType forKey:@"CurrentSoundsType"];
    [defaults synchronize];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50 ;
}
@end
