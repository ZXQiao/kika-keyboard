//
//  SettingTableViewController.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/1/4.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewController : UITableViewController
- (void)shareMyApp;
@end
