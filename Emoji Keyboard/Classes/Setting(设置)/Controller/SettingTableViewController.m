//
//  SettingTableViewController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/1/4.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "SettingTableViewController.h"
#import "SettingViewCell.h"
#import "LanguageTableViewController.h"
#import "ProvicyViewController.h"
#import "UMFeedback.h"
#import "FeedBackController.h"
#import "CKShareTools.h"
#import "CKGuideVideoView.h"
#import "CKSoundsViewController.h"
#import "AFNetworking.h"

@interface SettingTableViewController ()

@property (nonatomic, strong) UIView * backView;
@property (nonatomic, strong) UIImageView * helpImageView;
@property (nonatomic, strong) UIButton * cancelButton;
@property (nonatomic, assign) BOOL helpViewShow;
#if (APP_VARIANT_NAME != APP_VARIANT_MEMOJI)
@property (nonatomic, strong) UIButton * helpbutton;
@property (nonatomic, strong) UIButton * closebutton;
#endif
@property (nonatomic, strong) NSString * share_string;

@end

@implementation SettingTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.share_string = [NSString stringWithFormat:@"Hey, I'm using %@ to send awesome new emoji stickers! You can get it here - %@",APP_NAME, APP_ITUNES_CONNECTION];
   
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Settings";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;

     self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor yellowColor] forKey:NSForegroundColorAttributeName];

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CKSettingShouldPushFeedback:) name:@"CKSettingShouldPushFeedback" object:nil];

}

- (void)CKSettingShouldPushFeedback:(NSNotification *)notification
{
    CKLog(@"开发者回复");
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    
//    FeedBackController * feedBack = [[FeedBackController alloc] init];
//    [self.navigationController pushViewController:feedBack animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return 7;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * ID = @"SettingCell";
    SettingViewCell * cell = [tableView dequeueReusableCellWithIdentifier:ID];
    
    if (cell == nil) {
        cell = [[SettingViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    int index = (int)indexPath.row;
    
    NSString * leftImageName;
    
    
    switch (index) {
        case 0:
            leftImageName = @"Language";
            break;
        case 1:
            leftImageName = @"Sounds & Music";
            break;
        case 2:
            leftImageName = @"Feedback";
            break;
        case 3:
            leftImageName = @"Rate";
            break;
        case 4:
            leftImageName = @"Share";
            break;
        case 5:
            leftImageName = @"Help";
            break;
        case 6:
            leftImageName = @"Privacy";
            break;
        default:
            break;
    }
    
    [CKSaveTools saveClickWithEventType:Main_SelectedSetting key:[NSString stringWithFormat:@"%d",index]];
    
    [cell.leftButton setImage:[UIImage imageNamed:leftImageName.lowercaseString] forState:UIControlStateNormal];
    cell.functionLabel.text = leftImageName;
    cell.functionLabel.font = [UIFont systemFontOfSize:18];
    cell.functionLabel.textColor = [UIColor skyBlueColor];
    cell.functionLabel.textAlignment = NSTextAlignmentLeft;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (iPhone4 || iPhone5) {
        return 50;
    }
    if (iPhone6 || iPhone6s) {
        
        return 60;
    }
    else
    {
        return 80;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    int index = (int)indexPath.row;

    switch (index) {
        case 0:
        {
            LanguageTableViewController * language  = [[LanguageTableViewController alloc]init];
            [self.navigationController pushViewController:language animated:YES];
        }
            break;
        case 1:
        {
            
            CKSoundsViewController * sound = [[CKSoundsViewController alloc] init];
            [self.navigationController pushViewController:sound animated:YES];
        }
            break;
        case 2:
        {
            
            FeedBackController * feedBack = [[FeedBackController alloc] init];
            [self.navigationController pushViewController:feedBack animated:YES];
        }
            break;
        case 3:
        {
            NSString *URLStr = [NSString stringWithFormat:@"%@%@",AppStore_URL,APPID];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLStr]];
        }
            break;
        case 4:
        {
            //创建弹出菜单容器
//            id container = [ShareSDK container];
//            if (iPad) {
//                [container setIPadContainerWithView:self.tabBarController.tabBar arrowDirect:UIPopoverArrowDirectionDown];
//            }
//            else{
//                container = nil;
//            }
            [self shareMyApp];
        }
            break;
        case 5:
        {
            NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
            [sharedDefaults setInteger:CKFullAccessTypeHelp forKey:Full_Access_Type];
            [sharedDefaults synchronize];
            [self showHelp];
        }
            break;
        case 6:
        {
            
            ProvicyViewController * provicy = [[ProvicyViewController alloc] init];
            [self.navigationController pushViewController:provicy animated:YES];
            
        }
            break;
        default:
        break;
    }
}

- (void)shareMyApp
{
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"ShareBigPicture" ofType:@"png"];
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:self.share_string
                                       defaultContent:APP_NAME
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:APP_NAME
                                                  url:APP_ITUNES_CONNECTION
                                          description:nil
                                            mediaType:SSPublishContentMediaTypeNews];
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:nil arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"分享成功");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(@"分享失败,错误码:%ld,错误描述:%@", (long)[error errorCode], [error errorDescription]);
                                }
                            }];
    
}


#if (APP_VARIANT_NAME != APP_VARIANT_MEMOJI)
-(void)showHelp{
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];

    UIImageView *helpImageView = [[UIImageView alloc] initWithFrame:window.frame];
    helpImageView.userInteractionEnabled = YES;
    self.helpImageView = helpImageView;
    CGRect frame = window.frame;
    CGFloat x = frame.size.width / 5;
    CGFloat y = frame.size.height * 5 / 6;
    CGFloat w = frame.size.width - 2 * x;
    CGFloat h = (frame.size.height - y) - 30;
    UIButton *helpButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, w, h)];
    self.helpbutton = helpButton;
    UIButton *closeButton;
    UIImage *HelpImage;
    
    
    if (iPhone4) {
        HelpImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ios9_4.png" ofType:nil ]];
        closeButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width - 50, 0, 50, 50)];
        [closeButton setContentEdgeInsets:UIEdgeInsetsMake(6.5, 6.5, 6.5, 6.5)];
    }
    else if (iPhone5){
        HelpImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ios9_5.png" ofType:nil ]];
        closeButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width - 50, 0, 50, 50)];
        [closeButton setContentEdgeInsets:UIEdgeInsetsMake(6.5, 6.5, 6.5, 6.5)];
    }else if (iPhone6){
        HelpImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ios9_6.png" ofType:nil ]];
        closeButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width - 50, 0, 50, 50)];
        
        [closeButton setContentEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    }else if (iPhone6s){
        HelpImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ios9_6+.png" ofType:nil ]];
        closeButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width - 60, 0, 60, 60)];
        [closeButton setContentEdgeInsets:UIEdgeInsetsMake(9, 9, 9, 9)];
    }
    
    UIImage *closeImage = [[UIImage alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"help_guide_close@2x.png" ofType:nil]];
    [closeButton setImage:[closeImage imageWithTintColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
    
    self.closebutton = closeButton;
//    [closeButton setBackgroundColor:[UIColor redColor]];
    [window addSubview:helpImageView];
    [self.helpImageView addSubview:helpButton];
    [self.helpImageView addSubview:closeButton];

    helpImageView.image = HelpImage ;
    [helpButton addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)buttonPress:(UIButton *)button
{
    [self.helpbutton removeFromSuperview];
    self.helpbutton = nil;
    [self.helpImageView removeFromSuperview];
    self.helpImageView = nil;
    [self.closebutton removeFromSuperview];
    self.closebutton = nil;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}
#else
-(void)showHelp{
    self.helpViewShow = YES;
    [self prefersStatusBarHidden];
    [self setNeedsStatusBarAppearanceUpdate];
    
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    CKGuideVideoView *helpVideo = [[CKGuideVideoView alloc]initWithFrame:window.frame];
    [window addSubview:helpVideo];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}
#endif

- (void)cancelButtonClicked
{
    [self.backView removeFromSuperview];
#if (APP_VARIANT_NAME != APP_VARIANT_MEMOJI)
    [self.helpImageView removeFromSuperview];
#endif
    [self.cancelButton removeFromSuperview];
}



@end
