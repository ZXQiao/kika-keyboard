//
//  CKShareTools.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/17.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>
@protocol ISSCAttachment;
@interface CKShareTools : NSObject

+(void)shareImage:(id<ISSCAttachment>)image text:(NSString *)text container:(id<ISSContainer>)container;
@end
