//
//  CKShareTools.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/6/17.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "CKShareTools.h"
#import <ShareSDK/ShareSDK.h>

@interface CKShareTools()

@end

@implementation CKShareTools
- (void)testShare
{
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"ShareBigPicture" ofType:@"png"];
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:@"分享内容"
                                       defaultContent:@"测试一下"
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"ShareSDK"
                                                  url:@"http://www.mob.com"
                                          description:@"这是一条测试信息"
                                            mediaType:SSPublishContentMediaTypeNews];
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:nil arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"分享成功");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(@"分享失败,错误码:%ld,错误描述:%@", (long)[error errorCode], [error errorDescription]);
                                }
                            }];
    
}
//分享函数
+(void)shareImage:(id<ISSCAttachment>)image text:(NSString *)text container:(id<ISSContainer>)container
{
//    [ShareSDK cancelAuthWithType:ShareTypeFacebook];
        //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:text
                                       defaultContent:APP_NAME
                                                image:image
                                                title:APP_NAME
                                                  url:APP_ITUNES_CONNECTION
                                          description:nil
                                            mediaType:SSPublishContentMediaTypeNews];
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    CKLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    CKLog(NSLocalizedString(@"TEXT_ShARE_FAIL", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                }
                            }];
}



@end
