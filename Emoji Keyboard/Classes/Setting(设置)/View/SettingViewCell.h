//
//  SHTitleCell.h
//  SweetHeart
//
//  Created by 张赛 on 14-7-28.
//  Copyright (c) 2014年 张赛. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SettingViewCell : UITableViewCell

@property (nonatomic, strong) UIButton * leftButton;
@property (nonatomic, strong) UILabel * functionLabel;
@end
