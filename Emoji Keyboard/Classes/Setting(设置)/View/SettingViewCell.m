//
//  SHTitleCell.m
//  SweetHeart
//
//  Created by 张赛 on 14-7-28.
//  Copyright (c) 2014年 张赛. All rights reserved.
//

#import "SettingViewCell.h"

@interface SettingViewCell()

@property (nonatomic, strong) UIButton * rightutton;
@property (nonatomic, strong) UILabel * bottomLabel;
@end

@implementation SettingViewCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {


        
        // 左图
        UIButton* leftImageView = [[UIButton alloc] init];
//        leftImageView.backgroundColor = [UIColor greenColor];
        self.leftButton =  leftImageView;
        [self.contentView addSubview:leftImageView];
//        self.leftButton.contentEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);

        
        // 名称
        UILabel * functionLabel = [[UILabel alloc] init];
        functionLabel.text = @"ThemeName";
        self.functionLabel = functionLabel;
        self.functionLabel.textAlignment = NSTextAlignmentCenter;
        self.functionLabel.font = [UIFont systemFontOfSize:13];
        [self.contentView addSubview:functionLabel];
        functionLabel.textColor = [UIColor whiteColor];
        self.functionLabel.numberOfLines = 0;

        
        
        // 选中按钮
        UIButton * rightutton = [[UIButton alloc] init];
        self.rightutton = rightutton;
        [self.contentView addSubview:rightutton];
        // custom_select
        [rightutton setImage:[UIImage imageNamed:@"setting_arrow@2x"] forState:UIControlStateNormal];
        [rightutton setImage:[UIImage imageNamed:@"setting_arrow"] forState:UIControlStateHighlighted];

        
        UILabel * bottomLabel = [[UILabel alloc] init];
        self.bottomLabel = bottomLabel;
        [self.contentView addSubview:bottomLabel];
        bottomLabel.backgroundColor = [UIColor colorWithRed:204/ 255.0 green:204 / 255.0 blue:204/ 255.0 alpha:1];
        
        self.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    }
    return self;
}

-(void)layoutSubviews
{
    CGFloat W = 40;
    CGFloat H = 40;
    self.leftButton.frame = CGRectMake(0, 0, W, H);
    self.leftButton.center = CGPointMake(self.leftButton.center.x, self.contentView.center.y);
    
    CGFloat labelX = W;
    CGFloat labelH = 40;
    CGFloat labelW = self.frame.size.width - 2 * W;
    self.functionLabel.frame = CGRectMake(labelX, 0, labelW, labelH);
    self.functionLabel.center = CGPointMake(self.functionLabel.center.x, self.contentView.center.y);
    
    CGFloat rightButtonX = W + self.functionLabel.frame.size.width;
    self.rightutton.frame = CGRectMake(rightButtonX, self.functionLabel.center.y, W, H);
    self.rightutton.center = CGPointMake(self.rightutton.center.x, self.contentView.center.y);
    
    self.bottomLabel.frame = CGRectMake(15, self.contentView.frame.size.height - 1, self.contentView.frame.size.width - 2 * 15, 1);
}

- (void)awakeFromNib
{

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];
}


@end
