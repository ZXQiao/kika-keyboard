//
//  CKGuideVideoView.m
//  CKVideoDemo
//
//  Created by 张赛 on 15/8/5.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import "CKGuideVideoView.h"
#import <Masonry.h>
#import "CKTracker.h"


@interface CKGuideVideoView()
@property (weak, nonatomic) IBOutlet UIImageView *guideView;
@property (strong, nonatomic) UIButton *setButton;
@end

@implementation CKGuideVideoView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self = [[[NSBundle mainBundle] loadNibNamed:@"CKGuideVideoView" owner:self options:nil] lastObject];
        self.frame = frame;
        
        if (iPhone4 || iPhone5) {
            [_guideView setImage:[UIImage imageNamed:@"app_allow_5"]];
        } else if (iPhone6 || iPhone6s) {
            [_guideView setImage:[UIImage imageNamed:@"app_allow_6"]];
        }
        
        _setButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_setButton setTitle:@"Set Now" forState:UIControlStateNormal];
        [_setButton setBackgroundColor:[UIColor colorFromHexString:@"#00CA97"]];
        [_setButton addTarget:self action:@selector(setPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_setButton.layer setMasksToBounds:YES];
        [self addSubview:_setButton];
    }
    return self;
}

- (void)updateConstraints{
    CGFloat btnHeight = 0.0, btnWidthMargin = 0.0, btnBottomMargin = 0.0;
    
    if (iPhone4) {
        btnHeight = 35.0;
        btnWidthMargin = 44.0;
        btnBottomMargin = 20.0;
    } else if (iPhone5) {
        btnHeight = 40.0;
        btnWidthMargin = 30.0;
        btnBottomMargin = 30.0;
    } else if (iPhone6) {
        btnHeight = 44.0;
        btnWidthMargin = 35.0;
        btnBottomMargin = 35.0;
    } else if (iPhone6s) {
        btnHeight = 50.0;
        btnWidthMargin = 35.0;
        btnBottomMargin = 35.0;
    }
    
    __weak __typeof(self) weakSelf = self;
    
    [_setButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        make.left.equalTo(strongSelf).offset(btnWidthMargin);
        make.right.equalTo(strongSelf).offset(-btnWidthMargin);
        make.bottom.equalTo(strongSelf).offset(-btnBottomMargin);
        make.height.mas_equalTo(btnHeight);
    }];
    [_setButton.titleLabel setFont:[UIFont systemFontOfSize:btnHeight/2.0]];
    [_setButton.layer setCornerRadius:btnHeight/2.0];
    
    [super updateConstraints];
}

- (IBAction)CancelHelp:(id)sender {
    [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:@"APP_Guidance_Close_Click"];
    [self removeFromSuperview];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (void)setPressed:(id)sender {
    [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:@"APP_Guidance_Set_Click"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=General&path=Keyboard/KEYBOARDS"]];
}


@end
