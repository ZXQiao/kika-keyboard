

#import "MessageCell.h"
#import "CellFrameModel.h"
#import "MessageModel.h"
#import "UIImage+Tools.h"

@interface MessageCell()
{
    UILabel *_timeLabel;
    UIImageView *_iconView;
    UIButton *_textView;
}
@end

@implementation MessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.textColor = [UIColor blackColor];
        _timeLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_timeLabel];
        
        _iconView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconView];
        
        _textView = [UIButton buttonWithType:UIButtonTypeCustom];
        _textView.titleLabel.numberOfLines = 0;
        _textView.titleLabel.font = [UIFont systemFontOfSize:13];
        [_textView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _textView.contentEdgeInsets = UIEdgeInsetsMake(textPadding, 13, textPadding, textPadding);
        [self.contentView addSubview:_textView];

    }
    return self;
}

- (void)setCellFrame:(CellFrameModel *)cellFrame
{
    _cellFrame = cellFrame;
    MessageModel *message = cellFrame.message;
    
//    _timeLabel.frame = cellFrame.timeFrame;
//    _timeLabel.text = message.time;
    
    _iconView.frame = cellFrame.iconFrame;
    NSString *iconStr = message.messageType ? @"other" : @"me";
    _iconView.image = [UIImage imageNamed:iconStr];
    
    _textView.frame = cellFrame.textFrame;
    NSString *textBg = message.messageType ? @"bubbleSomeone" : @"bubbleMine";
    UIColor *textColor = message.messageType ? [UIColor blackColor] : [UIColor blackColor];
//    UIColor * textColor = [UIColor blackColor];
    [_textView setTitleColor:textColor forState:UIControlStateNormal];
    [_textView setBackgroundImage:[UIImage resizableImageNamed:textBg] forState:UIControlStateNormal];
    [_textView setTitle:message.content forState:UIControlStateNormal];
}

@end
