

#import <UIKit/UIKit.h>
@class CellFrameModel;

@interface MessageCell : UITableViewCell

@property (nonatomic, strong) CellFrameModel *cellFrame;

@end
