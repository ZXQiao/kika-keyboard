

#import "MessageModel.h"

@implementation MessageModel

+ (id)messageModelWithDict:(NSDictionary *)dict
{
    MessageModel *message = [[self alloc] initWithDict:dict];
//    message.text = dict[@"text"];
//    message.time = dict[@"time"];
//    message.messageType = [dict[@"type"] intValue];
    if ([message.type isEqualToString:@"dev_reply"]){
//        CKLog(@"开发者回复");
        message.messageType = kMessageModelTypeMe;
    }else{
//        CKLog(@"用户回复");
        message.messageType = kMessageModelTypeOther;
    }
    
    return message;
}

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
        
    }
    return self;
}

@end
