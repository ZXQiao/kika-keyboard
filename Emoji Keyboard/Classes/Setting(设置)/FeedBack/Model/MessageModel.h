

#import <Foundation/Foundation.h>

typedef enum {
    kMessageModelTypeOther,
    kMessageModelTypeMe
} MessageModelType;

@interface MessageModel : NSObject

@property (nonatomic, copy) NSString *reply_id;
@property (nonatomic, copy) NSString *audio;
@property (nonatomic, copy) NSString *audio_length;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) MessageModelType messageType;
@property (nonatomic, assign) BOOL is_failed;

+ (id)messageModelWithDict:(NSDictionary *)dict;

@end
