//
//  FeedBackController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/19.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "FeedBackController.h"
#import "UMFeedback.h"
#import "MessageModel.h"
#import "CellFrameModel.h"
#import "MessageCell.h"

#define kToolBarH 44
#define kTextFieldH 30

@interface FeedBackController ()<UMFeedbackDataDelegate,UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    NSMutableArray *_cellFrameDatas;
    UITableView *_chatView;
    UIImageView *_toolBar;
}

@property (strong, nonatomic) UMFeedback *feedback;
@property (nonatomic, strong) CellFrameModel * firstFrame;
@property (nonatomic, strong) NSTimer * reloadTimer;

@end


@implementation FeedBackController

- (CellFrameModel *)firstFrame
{
    if (_firstFrame == nil) {
        _firstFrame = [[CellFrameModel alloc] init];
        NSDictionary * dict = @{@"content":[NSString stringWithFormat:@"Hi there! I'm the product manager of %@. Please feel free to tell us your comments or suggestions. Your support is our strength to move forward. Thank you!", APP_NAME],@"type":@"dev_reply"};
        MessageModel *message = [MessageModel messageModelWithDict:dict];
        //    message.content = @"您好,我是产品经理,欢迎您给我们的产品提出宝贵的意见";
        //    message.type = @"dev_reply";
        _firstFrame.message = message;
    }
    return _firstFrame;
}
- (void)updaeteCellData
{
    [_cellFrameDatas removeAllObjects];
    _cellFrameDatas =[NSMutableArray array];
    NSArray *dataArray = self.feedback.topicAndReplies;
    for (NSDictionary *dict in dataArray) {
        MessageModel *message = [MessageModel messageModelWithDict:dict];
        CellFrameModel *cellFrame = [[CellFrameModel alloc] init];
        cellFrame.message = message;
        [_cellFrameDatas addObject:cellFrame];
    }
}
- (void)get:(void (^)(NSError *error))completionCallback
{
//    CKLog(@"收到用户反馈");
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.feedback = [UMFeedback sharedInstance];
    self.feedback.delegate = self;

    self.view.backgroundColor = [UIColor colorWithRed:250.0 / 255 green:250.0 / 255 blue:250.0 / 255 alpha:1];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Feedback";
    label.font = [UIFont systemFontOfSize:20];
    self.navigationItem.titleView = label;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];

    
    //0.加载数据
    [self updaeteCellData];
    
    //1.tableView
    [self addChatView];
    //5.自动滚到最后一行
    NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_cellFrameDatas.count inSection:0];
    [_chatView scrollToRowAtIndexPath:lastPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    //2.工具栏
    [self addToolBar];

}

- (void)viewWillDisappear:(BOOL)animated
{

    [super viewWillDisappear:animated];
    [self.reloadTimer setFireDate:[NSDate distantFuture]];
    [self.reloadTimer invalidate];
    self.reloadTimer = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updaeteCellData];
}

#pragma mark - tableView的数据源和代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cellFrameDatas.count + 1;
}
//
- (MessageCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if (indexPath.row == 0) {
        cell.cellFrame = self.firstFrame;
    }
    else
    {
        cell.cellFrame = _cellFrameDatas[indexPath.row - 1];
    }
    
    return cell;
}
//
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return self.firstFrame.cellHeght ;
    }
    else
    {
    CellFrameModel *cellFrame = _cellFrameDatas[indexPath.row - 1];
    return cellFrame.cellHeght;
    }

}



- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark - UITextField的代理方法
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    //1.获得时间
//    NSDate *senddate=[NSDate date];
//    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
//    [dateformatter setDateFormat:@"HH:mm"];
//    NSString *locationString=[dateformatter stringFromDate:senddate];
    
    //2.创建一个MessageModel类
    NSString * content = textField.text;
//    CKLog(@"content===%@",content);
    
    NSDictionary * dict = @{@"content":content,@"type":@"user_reply"};
    MessageModel *message = [MessageModel messageModelWithDict:dict];
    //3.创建一个CellFrameModel类
    CellFrameModel *cellFrame = [[CellFrameModel alloc] init];
    cellFrame.message = message;
    
    //4.添加进去，并且刷新数据
    [_cellFrameDatas addObject:cellFrame];
    [_chatView reloadData];
    
    //5.自动滚到最后一行
    NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_cellFrameDatas.count  inSection:0];
    [_chatView scrollToRowAtIndexPath:lastPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    textField.text = @"";
    
    // 向服务器发送数据
    NSDictionary * postDict = @{@"content":content};
    [self.feedback post:postDict];
    [self reloadTimerStart];
    
    
    return YES;
}


#pragma mark 定时器启动
- (void)reloadTimerStart
{
    NSTimer * reloadTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(chatViewShouldReloadData) userInfo:nil repeats:YES];
    self.reloadTimer = reloadTimer;
    [self.reloadTimer setFireDate:[NSDate distantPast]];
}

- (void)chatViewShouldReloadData
{


    [self.feedback get];
    [self updaeteCellData];
    [_chatView reloadData];
    
    //5.自动滚到最后一行
    NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_cellFrameDatas.count inSection:0];
    [_chatView scrollToRowAtIndexPath:lastPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    CKLog(@"%d",(int)_cellFrameDatas.count);
}
- (void)endEdit
{
    [self.view endEditing:YES];
}

/**
 *  添加TableView
 */
- (void)addChatView
{
    self.view.backgroundColor = [UIColor colorWithRed:235.0/255 green:235.0/255 blue:235.0/255 alpha:1.0];
    UITableView *chatView = [[UITableView alloc] init];
    chatView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - kToolBarH);
    chatView.backgroundColor = [UIColor colorWithRed:235.0/255 green:235.0/255 blue:235.0/255 alpha:1.0];
    chatView.delegate = self;
    chatView.dataSource = self;
    chatView.separatorStyle = UITableViewCellSeparatorStyleNone;
    chatView.allowsSelection = NO;
    [chatView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEdit)]];
    _chatView = chatView;
    
    [self.view addSubview:chatView];
}

/**
 *  添加工具栏
 */
- (void)addToolBar
{
    UIImageView *bgView = [[UIImageView alloc] init];
    bgView.frame = CGRectMake(0, self.view.frame.size.height - kToolBarH, self.view.frame.size.width, kToolBarH);
    bgView.image = [UIImage imageNamed:@"chat_bottom_bg"];
    bgView.userInteractionEnabled = YES;
    _toolBar = bgView;
    [self.view addSubview:bgView];

    UITextField *textField = [[UITextField alloc] init];
    textField.returnKeyType = UIReturnKeySend;
    textField.enablesReturnKeyAutomatically = YES;
    textField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 1)];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.frame = CGRectMake(kToolBarH * .5, (kToolBarH - kTextFieldH) * 0.5, self.view.frame.size.width - kToolBarH, kTextFieldH);
    textField.background = [UIImage imageNamed:@"chat_bottom_textfield"];
    textField.delegate = self;
    [bgView addSubview:textField];
}

/**
 *  键盘发生改变执行
 */
- (void)keyboardWillChange:(NSNotification *)note
{

    NSDictionary *userInfo = note.userInfo;
    CGFloat duration = [userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    
    CGRect keyFrame = [userInfo[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    CGFloat moveY = keyFrame.origin.y - self.view.frame.size.height;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, moveY);
    }];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.reloadTimer invalidate];
    self.reloadTimer = nil;

}


@end
