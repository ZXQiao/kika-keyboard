//
//  CKMainThemeViewController.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/15.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKMainThemeViewController.h"
#import "CKOnlineThemeView.h"// 在线主题
#import "AFNetworking.h"
#import "CKMainThemeCell.h"
#import "NSDictionary+Log.h"

#import "CKMainThemeModel.h"
#import "CKMemeTitleView.h"
#import <ShareSDK/ShareSDK.h>
#import "FeedBackController.h"
#import "CKGuideView.h"
#define CKTHEME_URL @"http://interface.1015game.com/ios/theme.ios.php"
/**
 *  在线的下载完成后生成一个onlineDownloadTheme数组,然后添加到本地
 */

@interface CKMainThemeViewController ()<UITableViewDelegate,UITableViewDataSource,CKMemeTitleViewDelegate,CKGuideViewDelegate>
{
    UITableView * _mainTableView;
    CKOnlineThemeView * _onlineThemeView;
    CKMainThemeModel * _nowUseThemeModel;
    UIView *_backView;
}
@property (nonatomic, strong) NSArray * defaultThemeArray;
@property (nonatomic, strong) NSMutableArray * onlineThemeArray;
@property (nonatomic, strong) CKMemeTitleView * memeTitleView;

@end

@implementation CKMainThemeViewController

- (NSArray *)defaultThemeArray
{
    if (_defaultThemeArray == nil) {
        _defaultThemeArray = [CKMainThemeModel defaultThemeModels];
    }
    return _defaultThemeArray;
}

- (NSMutableArray *)onlineThemeArray
{
    if (_onlineThemeArray == nil) {
        _onlineThemeArray = [NSMutableArray array];
    NSArray * array = [NSArray arrayWithContentsOfURL:[Online_Theme_URL URLByAppendingPathComponent:@"downloadOnlineThemeArray.plist"]];
        for (NSMutableDictionary * dict in array) {
            if ([dict[@"themeName"] isEqualToString:@"rainyday"]) {
                dict[@"themeName"] = @"lollipop";
                dict[@"setting_icon_highlight"] = @"#ececec";
            }
            CKMainThemeModel * mainTheme = [CKMainThemeModel mainThemeModelWithDict:dict];
            [_onlineThemeArray addObject:mainTheme];
        }
    }
    return _onlineThemeArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COMMON_VIEW_BACK_COLOR;
    _nowUseThemeModel = [CKMainThemeModel nowUseThemeModel];
    if (!_nowUseThemeModel) {
        _nowUseThemeModel = self.defaultThemeArray[Default_Theme_Index];
        [CKSaveTools saveMainTheme:_nowUseThemeModel toRegion:CKSaveRegionAppGroup];
    }
    [self addSomeNotification];
    // 加载本地的ThemeView
    [self addMainThemeTableView];
    [self addOnLineThemeView];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];    
    if (self.jumpURL) {
        if ([self.jumpURL.absoluteString isEqualToString:[NSString stringWithFormat:@"%@.themes", APP_ROOT_URL]]) {
            UIButton * button = [[UIButton alloc] init];
            button.tag = 1;
            [self themeButtonClickedToChangeLocalOrOnline:button];
            _jumpURL = nil;
        }
    }
}

- (void)addOnLineThemeView
{
    // 在线主题界面OnlineThemeView
    CKOnlineThemeView * onlineThemeView = [[CKOnlineThemeView alloc] initWithFrame:self.view.bounds];
    _onlineThemeView = onlineThemeView;
    [self.view addSubview:onlineThemeView];
    _onlineThemeView.hidden = YES;
}

- (void)addSomeNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardLaunchContaningApp:) name:@"CKKeyboardLaunchContaningApp" object:nil];
    // AppDelegate发送的通知,从后台回来的时候,通知主题界面选中新的主题
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ThemeViewSholdRloadData:) name:@"ThemeViewSholdRloadData" object:nil];
}


- (void)ThemeViewSholdRloadData:(NSNotification *)note
{
    _nowUseThemeModel = [CKMainThemeModel sholdUseThisModel];
    [_mainTableView reloadData];
}

- (void)keyboardLaunchContaningApp:(NSNotification *)notification
{
    NSURL * url = notification.object;
    if ([url.absoluteString isEqualToString:[NSString stringWithFormat:@"%@.themes", APP_ROOT_URL]]) {
//        self.segment.selectedSegmentIndex = 1;
//        [self buttonAction:self.segment];
        UIButton * button = [[UIButton alloc] init];
        button.tag = 1;
        [self themeButtonClickedToChangeLocalOrOnline:button];
    }
    else{
        return;
    }
}

- (void)themeButtonClickedToChangeLocalOrOnline:(UIButton *)button
{
    int buttonTag = (int)button.tag;
    self.memeTitleView.selectedIndex = buttonTag;
    
    if (0 == buttonTag)
    {
        _mainTableView.hidden = NO;
        [_mainTableView reloadData];
        _onlineThemeView.hidden = YES;
        _onlineThemeArray = nil;
        _nowUseThemeModel = [CKMainThemeModel nowUseThemeModel];
        [_mainTableView reloadData];
    }
    if(1 == buttonTag)
    {
        _mainTableView.hidden = YES;
        _onlineThemeView.hidden = NO;
        //通知onlineTheme加载数据
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadData" object:[NSString stringWithFormat:@"%@", CKTHEME_URL]];
        [_onlineThemeView.themeTableView reloadData];
    }

}
- (void)addMainThemeTableView
{
    CKMemeTitleView * memeTitleView = [[CKMemeTitleView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 2, 44)];
    self.memeTitleView = memeTitleView;
    self.navigationItem.titleView = memeTitleView;
    memeTitleView.selectedIndex = 0;
    memeTitleView.delegate = self;
    
    
    _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 20)];
    _mainTableView.delegate = self;
    _mainTableView.dataSource = self;
    _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_mainTableView];
    _mainTableView.hidden = NO;
    _mainTableView.backgroundColor = COMMON_VIEW_BACK_COLOR;
}

#pragma segment event methord
//-(void)buttonAction:(UISegmentedControl*)seg
//{
//    NSInteger index = seg.selectedSegmentIndex;
//    if (0 == index)
//    {
//        _mainTableView.hidden = NO;
//        [_mainTableView reloadData];
//        _onlineThemeView.hidden = YES;
//        _onlineThemeArray = nil;
//        _nowUseThemeModel = [CKMainThemeModel nowUseThemeModel];
//        [_mainTableView reloadData];
//    }
//    if(1 == index)
//    {
//        _mainTableView.hidden = YES;
//        _onlineThemeView.hidden = NO;
//        //通知onlineTheme加载数据
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadData" object:[NSString stringWithFormat:@"%@", CKTHEME_URL]];
//        [_onlineThemeView.themeTableView reloadData];
//    }
//}


#pragma mark ----- tableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.defaultThemeArray.count + self.onlineThemeArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * ID = @"MainThemeCell";
    CKMainThemeCell * mainThemeCell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (mainThemeCell == nil) {
        mainThemeCell = [[CKMainThemeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    mainThemeCell.selectionStyle = UITableViewCellSelectionStyleNone;
    _nowUseThemeModel = [CKMainThemeModel nowUseThemeModel];
    CKMainThemeModel * mainthemeModel;
    if (indexPath.row < _defaultThemeArray.count) {
        mainthemeModel = _defaultThemeArray[indexPath.row];
        mainthemeModel.onUse = NO;
        if (!mainthemeModel.isOnline) {
            if ([mainthemeModel.themeName isEqualToString:_nowUseThemeModel.themeName]) {
                mainthemeModel.onUse = YES;
            }
        }
    }
    else{
        mainthemeModel = _onlineThemeArray[indexPath.row - _defaultThemeArray.count];
        mainthemeModel.online = YES;
        mainthemeModel.onUse = NO;
        if (mainthemeModel.isOnline) {
            if ([mainthemeModel.themeName isEqualToString:_nowUseThemeModel.themeName]) {
                mainthemeModel.onUse = YES;
            }
        }
    }
    mainThemeCell.mainThemeModel = mainthemeModel;
    
    return mainThemeCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (iPhone6) {
        return 109;
    }
    else if (iPhone6s)
    {
        return 118;;
    }
    else if (iPad)
    {
        return 210;
    }
    else
        return 96;
}

#define Group_Online_Theme_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS]URLByAppendingPathComponent:@"OnlineThemes"]

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self saveClickCount];
    CKMainThemeCell * cell = (CKMainThemeCell *)[tableView cellForRowAtIndexPath:indexPath];

    CKMainThemeModel * themeModel;
    if (indexPath.row < _defaultThemeArray.count) {
        themeModel = _defaultThemeArray[indexPath.row];
    }
    else if(indexPath.row < _defaultThemeArray.count + _onlineThemeArray.count){
        themeModel = _onlineThemeArray[indexPath.row - _defaultThemeArray.count];
        NSString * imageName = [NSString stringWithFormat:@"%@/%@_background@2x.png",themeModel.themeName,themeModel.themeName];
        UIImage * backImage = [[UIImage alloc] initWithContentsOfFile: [Group_Online_Theme_URL URLByAppendingPathComponent:imageName].path];
        if (!backImage) {
            [_onlineThemeArray removeObjectAtIndex:indexPath.row - _defaultThemeArray.count];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            // 删除
    //        NSError * error;
    //        [[NSFileManager defaultManager] removeItemAtPath:[Online_Theme_URL URLByAppendingPathComponent:themeModel.themeName].path error:&error];
            NSURL * arrayURL = [Online_Theme_URL URLByAppendingPathComponent:@"downloadOnlineThemeArray.plist"];
            NSMutableArray * downloadArrayM = [NSMutableArray arrayWithContentsOfURL:arrayURL];
            downloadArrayM = (downloadArrayM == nil)?[NSMutableArray array]:downloadArrayM;
            if (indexPath.row - _defaultThemeArray.count < downloadArrayM.count) {
                [downloadArrayM removeObjectAtIndex:(indexPath.row - _defaultThemeArray.count)];
            }
            [downloadArrayM writeToURL:arrayURL atomically:YES];
            return;
        }
    }
    
    themeModel.onUse = YES;
    // 友盟统计
    if (themeModel.themeName) {
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY action:Main_SelectedTheme label:themeModel.themeName];
        [CKSaveTools saveClickWithEventType:Main_SelectedTheme key:themeModel.themeName];
    }
    else{
        CKLog(@"友盟统计主App主题点击名字为空--- I'm in CKMainThemeVIewController");
    }
    
    [_mainTableView reloadData];
    [cell animateCell:cell];
    [CKSaveTools saveMainTheme:themeModel toRegion:CKSaveRegionAppGroup];
    NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    [sharedDefaults setObject:[NSDate date] forKey:Last_Save_Theme_Date];
    [sharedDefaults synchronize];
 
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

//记录使用主题次数
-(void)saveClickCount{
    NSUserDefaults *shareDefaults=[NSUserDefaults standardUserDefaults];
    NSUserDefaults *groupDefaults = [[NSUserDefaults alloc]initWithSuiteName:APP_GROUPS];
    //只有用户打开full access 该项判断才有效
    if(![shareDefaults objectForKey:EVALUATE_STATUS_APP] &&
       ![groupDefaults objectForKey:EVALUATE_STATUS_GROUP]){
    //没有评价过 点击主题三次触发弹出框
        [CKSaveTools saveClickCountToNum:2 Key:@"useThemCount" triggerAction:@selector(showRateView) withHandle:self];
    }
}
-(void)showRateView{
    [CKSaveTools saveString:@"HaveEvalate" forKey:EVALUATE_STATUS_APP inRegion:CKSaveRegionContainingApp];
    _backView =[[UIView alloc]initWithFrame:self.view.bounds];
    _backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [[UIApplication sharedApplication].keyWindow addSubview:_backView];
    
    CGFloat w =self.view.frame.size.width;
    CGFloat h = 0;
    w = w*0.75;
    h = w*1.04;
    CKGuideView * rateView = [[CKGuideView alloc] initWithAppType:CKAppTypeContainingApp guideType:CKGuideTypeRate fram:CGRectMake(0, 0, w, h)];
    rateView.center =_backView.center;
    rateView.delegate = self;
    [[UIApplication sharedApplication].keyWindow addSubview:rateView];
}

#pragma mark -- 评分代理
-(void)CKGuideView:(CKGuideView*)guideView clickBtnType:(CKClickType)clickType{
    [guideView removeFromSuperview];
    [_backView removeFromSuperview];

    if(clickType == CKClickTypeRate){
        //评分按钮点击
        [CKSaveTools saveString:@"EVALUATE_STATUS_GROUP" forKey:EVALUATE_STATUS_GROUP inRegion:CKSaveRegionAppGroup];
        [CKSaveTools saveClickWithEventType:KB_APP_Use_Them_Clicked key:@"KB_APP_Use_Them_Clicked"];
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY
                                           action:KB_APP_Use_Them_Clicked];
        [CKSaveTools switchToURL:[NSString stringWithFormat:@"%@%@",AppStore_URL,APPID] withResponder:self.view];
    }else if (clickType == CKClickTypeCancel){
        //later按钮点击
        [CKSaveTools saveClickWithEventType:KB_APP_Use_Them_Cancel_Clicked key:@"KB_APP_Use_Them_Cancel_Clicked"];
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY
                                           action:KB_APP_Use_Them_Cancel_Clicked];
    }
}

#if DEBUG
#pragma mark -- DEBUG阶段可以删除主题,方便调试
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
       if (indexPath.row < _defaultThemeArray.count) {
           return;
       }
        else
        {
            // 判断当前被删除的主题是否正在使用
            CKMainThemeModel * mainTheme = _onlineThemeArray[indexPath.row - _defaultThemeArray.count];
            if ([_nowUseThemeModel.themeName isEqualToString:mainTheme.themeName]) {
                [CKSaveTools saveMainTheme:_defaultThemeArray[0] toRegion:CKSaveRegionAppGroup];
            }
            // 删除
            NSError * error;
            [[NSFileManager defaultManager] removeItemAtPath:[Online_Theme_URL URLByAppendingPathComponent:mainTheme.themeName].path error:&error];
            [_onlineThemeArray removeObjectAtIndex:(indexPath.row - _defaultThemeArray.count)];
            NSURL * arrayURL = [Online_Theme_URL URLByAppendingPathComponent:@"downloadOnlineThemeArray.plist"];
            NSMutableArray * downloadArrayM = [NSMutableArray arrayWithContentsOfURL:arrayURL];
            downloadArrayM = (downloadArrayM == nil)?[NSMutableArray array]:downloadArrayM;
            if (indexPath.row - _defaultThemeArray.count < downloadArrayM.count) {
                [downloadArrayM removeObjectAtIndex:(indexPath.row - _defaultThemeArray.count)];
            }
            [downloadArrayM writeToURL:arrayURL atomically:YES];
            [_mainTableView reloadData];
        }
    }
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{if (indexPath.row < _defaultThemeArray.count) {
    //初始化AlertView
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tip:"
                                                    message:@"message"
                                                   delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //设置标题与信息，通常在使用frame初始化AlertView时使用
    alert.title = @"Tip:";
    alert.message = @"Local theme can't be removed�";
    [alert show];
    return UITableViewCellEditingStyleNone;
}
    return UITableViewCellEditingStyleDelete;
}
#endif


// 观察者的注销
-(void)dealloc{[[NSNotificationCenter defaultCenter] removeObserver:self];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
