//
//  CKMainThemeViewController.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/15.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKBaseViewController.h"
@interface CKMainThemeViewController : CKBaseViewController

@property (nonatomic,strong) NSURL * jumpURL;
@end
