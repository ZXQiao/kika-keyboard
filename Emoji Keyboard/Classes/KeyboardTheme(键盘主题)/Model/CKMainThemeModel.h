//
//  CKMainThemeModel.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/15.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+MJKeyValue.h"

@interface CKMainThemeModel : NSObject<NSCoding>
@property (nonatomic, copy) NSString * themeName;
@property (nonatomic, copy) NSString * FontColor;
@property (nonatomic, copy) NSString * PopColor;
@property (nonatomic, copy) NSString * fontName;
@property (nonatomic, copy) NSString * font_normal_color;
@property (nonatomic, copy) NSString * font_highlight_color;
@property (nonatomic, copy) NSString * emoji_icon_normal;
@property (nonatomic, copy) NSString * emoji_icon_highlight;
@property (nonatomic, copy) NSString * setting_icon_normal;
@property (nonatomic, copy) NSString * setting_icon_highlight;
@property (nonatomic, copy) NSString * setting_fontcolor;
@property (nonatomic, copy) NSString * coolfont_color;
@property (nonatomic, copy) NSString * popupstyle_index;

@property (nonatomic, assign,getter=isOnline) BOOL online;
@property (nonatomic, assign,getter=isDownloaded) BOOL downloaded;
@property (nonatomic, assign,getter=isOnUse) BOOL onUse;

// 传入一个字典,返回一个model
+ (instancetype)mainThemeModelWithDict:(NSDictionary *)dict;
// 传入一个字典Json字符串,返回一个model
+ (instancetype)mainThemeModelWithJsonString:(NSString *)json;
// 包含所有默认主题model的model数组
+ (NSArray *)defaultThemeModels;
+ (instancetype)nowUseThemeModel;
+ (CKMainThemeModel *)sholdUseThisModel;
+ (void)sysThemeModelInKeyboardAndGroup;

@end
