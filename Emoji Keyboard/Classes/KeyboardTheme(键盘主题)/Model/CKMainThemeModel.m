//
//  CKMainThemeModel.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/15.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKMainThemeModel.h"
#import "CKSaveTools.h"
#import "MJExtension.h"


@implementation CKMainThemeModel

+ (void)sysThemeModelInKeyboardAndGroup
{
    
    NSDate * lastSaveInKeyboard = [[NSUserDefaults standardUserDefaults] objectForKey:Last_Save_Theme_Date];
    NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
    NSDate * lastSaveDateInGroup = [sharedDefaults objectForKey:Last_Save_Theme_Date];
    // 这里主要是为了让主App跟键盘的主题同步
    if (lastSaveInKeyboard == nil){ // 如果键盘没存东西,直接返回
        return;
    }
    else{ // 如果键盘有东西,要比较他们存储的日期,如果键盘存的日期更近一些,要让主App跟他同步
        double timeInterval = [lastSaveDateInGroup timeIntervalSinceDate:lastSaveInKeyboard];
        if (timeInterval < 0) { // 如果group里存得比较早的话,则替换
            [CKSaveTools saveMainTheme:[CKSaveTools takeOutMainThemefromRegion:CKSaveRegionExtensionKeyboard]toRegion:CKSaveRegionAppGroup];
        }
     else{
         return;
     }
    }
    
}

+ (CKMainThemeModel *)sholdUseThisModel
{
        NSDate * lastSaveInKeyboard = [[NSUserDefaults standardUserDefaults] objectForKey:Last_Save_Theme_Date];
        NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
        NSDate * lastSaveDateInGroup = [sharedDefaults objectForKey:Last_Save_Theme_Date];
    
        if (lastSaveInKeyboard == nil && lastSaveDateInGroup == nil) { // 证明用户下载后直接使用键盘了,什么主题都没选
            // 直接返回为空,键盘会选中默认主题
            return nil;
        }
        if (lastSaveInKeyboard == nil && lastSaveDateInGroup) { // 键盘没写,但是group里有,直接从group里取出
            return [CKMainThemeModel nowUseThemeModel] ? [CKMainThemeModel nowUseThemeModel]:[CKMainThemeModel defaultThemeModels][Default_Theme_Index];
        }
        if (lastSaveInKeyboard && lastSaveDateInGroup == nil) { // 键盘有但是group里没有,直接从沙盒里拿
            return [CKSaveTools takeOutMainThemefromRegion:CKSaveRegionExtensionKeyboard];
        }
        if (lastSaveDateInGroup && lastSaveInKeyboard) { // 两个都有,要比较时间
            double timeInterval = [lastSaveDateInGroup timeIntervalSinceDate:lastSaveInKeyboard];
            if (timeInterval < 0) {
                return [CKSaveTools takeOutMainThemefromRegion:CKSaveRegionExtensionKeyboard];
            }
            else
            {
                return [CKMainThemeModel nowUseThemeModel];
            }
        }
    return [CKMainThemeModel nowUseThemeModel] ? [CKMainThemeModel nowUseThemeModel]:[CKMainThemeModel defaultThemeModels][Default_Theme_Index] ;
}

+ (instancetype)nowUseThemeModel{
    return [CKSaveTools takeOutMainThemefromRegion:CKSaveRegionAppGroup];
}

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
     self = [CKMainThemeModel objectWithKeyValues:dict];
    }
    return self;
}

+ (instancetype)mainThemeModelWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

+ (instancetype)mainThemeModelWithJsonString:(NSString *)json
{
    NSDictionary * dict = [NSString dictionaryWithJsonString:json];
    return [[self alloc] initWithDict:dict];
}

+ (NSArray * )defaultThemeModels
{
    NSArray * mainThemeArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MainThemes.plist" ofType:nil]];
    NSMutableArray * modelArrayM = [NSMutableArray array];
    for (NSDictionary * dict in mainThemeArray) {
        CKMainThemeModel * themeModel = [[CKMainThemeModel alloc] initWithDict:dict];
        [modelArrayM addObject:themeModel];
    }
    return (NSArray *)modelArrayM;
}

/**
 *   归档一个对象到文件中的时候会调用
 *
 *   在此方法中告诉系统如何保存输入
 */
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.themeName forKey:@"themeName"];
    [encoder encodeObject:self.FontColor forKey:@"FontColor"];
    [encoder encodeObject:self.PopColor forKey:@"PopColor"];
    [encoder encodeObject:self.fontName forKey:@"fontName"];
    [encoder encodeObject:self.font_normal_color forKey:@"font_normal_color"];
    [encoder encodeObject:self.font_highlight_color forKey:@"font_highlight_color"];
    [encoder encodeObject:self.emoji_icon_normal forKey:@"emoji_icon_normal"];
    [encoder encodeObject:self.emoji_icon_highlight forKey:@"emoji_icon_highlight"];
    [encoder encodeObject:self.setting_icon_normal forKey:@"setting_icon_normal"];
    [encoder encodeObject:self.setting_icon_highlight forKey:@"setting_icon_highlight"];
    [encoder encodeObject:self.setting_fontcolor forKey:@"setting_fontcolor"];
    [encoder encodeObject:self.coolfont_color forKey:@"coolfont_color"];
    [encoder encodeObject:self.popupstyle_index forKey:@"popupstyle_index"];
    [encoder encodeBool:self.isOnline forKey:@"online"];
    [encoder encodeBool:self.isDownloaded forKey:@"downloaded"];
    [encoder encodeBool:self.isOnUse forKey:@"onUse"];
}

/**
 *  解归档一个数据的时候会调用
 *
 *  告诉系统如何解档数据
 */
- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.themeName = [decoder decodeObjectForKey:@"themeName"];
        self.FontColor = [decoder decodeObjectForKey:@"FontColor"];
        self.PopColor = [decoder decodeObjectForKey:@"PopColor"];
        self.fontName = [decoder decodeObjectForKey:@"fontName"];
        self.font_normal_color = [decoder decodeObjectForKey:@"font_normal_color"];
        self.font_highlight_color = [decoder decodeObjectForKey:@"font_highlight_color"];
        self.emoji_icon_normal = [decoder decodeObjectForKey:@"emoji_icon_normal"];
        self.emoji_icon_highlight = [decoder decodeObjectForKey:@"emoji_icon_highlight"];
        self.setting_icon_normal = [decoder decodeObjectForKey:@"setting_icon_normal"];
        self.setting_icon_highlight = [decoder decodeObjectForKey:@"setting_icon_highlight"];
        self.setting_fontcolor = [decoder decodeObjectForKey:@"setting_fontcolor"];
        self.coolfont_color = [decoder decodeObjectForKey:@"coolfont_color"];
        self.popupstyle_index = [decoder decodeObjectForKey:@"popupstyle_index"];
        self.online = [decoder decodeBoolForKey:@"online"];
        self.downloaded = [decoder decodeBoolForKey:@"downloaded"];
        self.onUse = [decoder decodeBoolForKey:@"onUse"];
    }
    return self;
}

@end
