//
//  CKMainThemeCell.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/15.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKMainThemeModel.h"

@interface CKMainThemeCell : UITableViewCell
@property (nonatomic, strong) CKMainThemeModel * mainThemeModel;
@property (nonatomic, strong) UIView * backView;
- (void)animateCell:(CKMainThemeCell *)cell;
@end
