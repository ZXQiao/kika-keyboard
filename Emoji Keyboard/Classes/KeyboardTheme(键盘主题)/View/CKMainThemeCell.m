//
//  CKMainThemeCell.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/15.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//


#import "CKMainThemeCell.h"

@interface CKMainThemeCell()


@property (nonatomic, strong) UIImageView * themeImageView;
@property (nonatomic, strong) UILabel * themeNameLabel;
@property (nonatomic, strong) UIButton * selectButton;

@end

@implementation CKMainThemeCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 背景图
        UIView * backView = [[UIView alloc] init];
        self.backView = backView;
        [self.contentView addSubview:backView];
        
        // 大图
        UIImageView * themeImageView = [[UIImageView alloc] init];
        self.themeImageView =  themeImageView;
        [self.backView addSubview:themeImageView];
        themeImageView.layer.cornerRadius = 5;
        themeImageView.layer.masksToBounds = YES;
        
        // 名称
        UILabel * themeNameLabel = [[UILabel alloc] init];
        themeNameLabel.text = @"ThemeName";
        self.themeNameLabel = themeNameLabel;
        self.themeNameLabel.textAlignment = NSTextAlignmentCenter;
        self.themeNameLabel.font = [UIFont systemFontOfSize:12];
        [self.themeImageView addSubview:themeNameLabel];
        themeNameLabel.textColor = [UIColor whiteColor];
        self.themeNameLabel.numberOfLines = 0;
        
        // 选中按钮
        UIButton * selectButton = [[UIButton alloc] init];
        self.selectButton = selectButton;
        [self.themeImageView addSubview:selectButton];
        self.backgroundColor = COMMON_VIEW_BACK_COLOR;
        [selectButton setImage:[UIImage imageNamed:@"click"] forState:UIControlStateSelected];
//        [selectButton setImage:nil forState:UIControlStateNormal];
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    
    CGFloat topPadding = 10;
    CGFloat leftPadding = self.frame.size.width * 1/ 20;
    
    CGFloat backViewW = self.frame.size.width - 2 * leftPadding;
    CGFloat backViewH = backViewW / 690 * 182;
    CGFloat backViewX = leftPadding;
    CGFloat backViewY = topPadding;
    self.backView.frame = CGRectMake(backViewX, backViewY, backViewW, backViewH);

    CGFloat padding = .5;
    CGFloat themeImageViewH = backViewH - 2 * padding;
    CGFloat themeImageViewW = themeImageViewH / 182 * 690;
    CGFloat themeImageViewX = padding;
    CGFloat themeImageViewY = padding;
    self.themeImageView.frame = CGRectMake(themeImageViewX, themeImageViewY, themeImageViewW, themeImageViewH);
    
    CGFloat themeNameLabelX = self.frame.size.width * .6;
    CGFloat themeNameLabelY = themeImageViewH / 20;
    CGFloat themeNameLabelW = CGRectGetMaxX(self.themeImageView.frame)- themeNameLabelX - padding;
    CGFloat themeNameLabelH = themeImageViewH / 5;
    self.themeNameLabel.frame = CGRectMake(themeNameLabelX, themeNameLabelY, themeNameLabelW, themeNameLabelH);
    
    CGFloat selectButtonY = padding + themeImageViewH / 3 * 2;
    CGFloat selectButtonH = themeImageViewH / 3;
    CGFloat selectButtonW = selectButtonH;
    CGFloat selectButtonX = CGRectGetMaxX(self.themeImageView.frame) - selectButtonW;
    self.selectButton.frame = CGRectMake(selectButtonX, selectButtonY, selectButtonW, selectButtonH);
}


- (void)setMainThemeModel:(CKMainThemeModel *)mainThemeModel
{
    _mainThemeModel = mainThemeModel;
    NSString * name = mainThemeModel.themeName;
    _themeNameLabel.text = name;
    _selectButton.selected = mainThemeModel.isOnUse;
    if (mainThemeModel.isOnline) {
        NSURL * url = [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS]URLByAppendingPathComponent:[NSString stringWithFormat:@"OnlineThemes/%@/%@@2x.png",mainThemeModel.themeName,mainThemeModel.themeName]];
        self.themeImageView.image = [UIImage imageWithContentsOfFile:url.path];
    }
    else{
        self.themeImageView.image = [UIImage imageNamed:mainThemeModel.themeName];
    }
    
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
//        [self animateCell];
    }
 
}

- (void)animateCell:(CKMainThemeCell *)cell
{
    // 动画效果
    [UIView animateWithDuration:1 animations:^{
        cell.contentView.alpha = 1;
        [UIView animateWithDuration:.5 animations:^{
            cell.contentView.transform = CGAffineTransformMakeScale(1.2, 1.2);
            cell.contentView.alpha = .4;
        }];
        [UIView animateWithDuration:.6 animations:^{
            cell.contentView.transform = CGAffineTransformMakeScale(.8, .8);
            cell.contentView.alpha = .8;
        }];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:.5 animations:^{
            cell.contentView.transform = CGAffineTransformMakeScale(1, 1);
            cell.contentView.alpha = 1;
            
        }];
    }];
    
}



@end
