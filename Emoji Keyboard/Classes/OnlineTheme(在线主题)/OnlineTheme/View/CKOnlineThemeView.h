//
//  OnlineThemeView.h
//  ThemeOnlineInterface
//
//  Created by 张赛 on 15/1/21.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import <UIKit/UIKit.h>

#define Online_Theme_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS]URLByAppendingPathComponent:@"OnlineThemes"]
@protocol OnlineThemeViewDelegate <NSObject>

@end
@interface CKOnlineThemeView : UIView
@property (nonatomic,strong) UITableView * themeTableView;
@property (nonatomic,weak) id<OnlineThemeViewDelegate> delegate;
@end
