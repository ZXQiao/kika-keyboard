//
//  OnlineThemeView.m
//  ThemeOnlineInterface
//
//  Created by 张赛 on 15/1/21.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "CKOnlineThemeView.h"
#import "ThemeDownload.h"
#import "CKOnlineThemeCell.h"
#import "UIImageView+WebCache.h"
#import "NSArray+Log.h"
#import "NSDictionary+Log.h"
#import "CKOnlineThemeModel.h"
#import "CKMainThemeModel.h"


#define CKTHEME_URL @"http://interface.1015game.com/ios/theme.ios.php"
@interface CKOnlineThemeView()<UITableViewDataSource,UITableViewDelegate,ThemeDownloadDelegate, MBProgressHUDDelegate,CKOnlineThemeCellDelegate>
{
    MBProgressHUD *HUD;
    NSMutableArray * _onlineThemeModelArray;
    CKMainThemeModel * _nowUseThemeModel;
}
@property (nonatomic,strong) ThemeDownload * download;
@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, strong) NSMutableArray * downloadTaskarray;
@property (nonatomic, strong) NSMutableArray * hasDownloadedNameArrayM;

@end



@implementation CKOnlineThemeView
- (NSMutableArray *)hasDownloadedNameArrayM
{
    if (_hasDownloadedNameArrayM == nil) {
        _hasDownloadedNameArrayM = [NSMutableArray array];
        NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:Online_Theme_URL.path error:NULL];
        [_hasDownloadedNameArrayM addObjectsFromArray:files];
    }
    return _hasDownloadedNameArrayM;
}

- (NSMutableArray *)downloadTaskarray
{
    if (_downloadTaskarray == nil) {
        _downloadTaskarray = [NSMutableArray array];
    }return _downloadTaskarray;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        
        _nowUseThemeModel = [CKMainThemeModel nowUseThemeModel];
        //检查网络
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currenNetWorkStatus:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData:) name:@"loadData" object:nil];
    }return self;
}

- (void)OnlineThemeViewShouldAddTableView
{
    // 减去导航栏以及工具条的尺寸
    CKLog(@"%@====onlineURL",Online_Theme_URL.path);
    CGRect adjustFrame = CGRectMake(0,64,self.frame.size.width , self.frame.size.height - 64 - 49);
    UITableView * themeTableView = [[UITableView alloc] initWithFrame:adjustFrame];
    [self addSubview:themeTableView];
    themeTableView.dataSource = self;
    themeTableView.delegate = self;
    self.themeTableView = themeTableView;
    themeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.themeTableView.backgroundColor = COMMON_VIEW_BACK_COLOR;
}

- (void)onlineThemeCellButtonClicked:(UIButton *)button{
    CKOnlineThemeModel * onlineThemeModel = _onlineThemeModelArray[button.tag];
    if(onlineThemeModel.downloadStatus == CKOnlineThemeStatusOnUse) return;
    
    if(onlineThemeModel.downloadStatus == CKOnlineThemeStatusNotDownload){ // 没有下载
        // 下载主题
        [self downloadWithOnlineThemeModel:onlineThemeModel downloadButton:button];
        onlineThemeModel.downloadStatus = CKOnlineThemeStatusIsDownloading;
        NSString * themeName = onlineThemeModel.themeName;
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY
                                           action:Main_DownLoadOnlineTheme
                                            label:themeName];
        [CKSaveTools saveClickWithEventType:Main_DownLoadOnlineTheme key:themeName];
        [self.themeTableView reloadData];
        return;
    }
    
    else if(onlineThemeModel.downloadStatus == CKOnlineThemeStatusIsDownloading){ // 正在下载
        // cancel下载并删除主题
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY
                                           action:Main_DownloadFailed];
        [CKSaveTools saveClickWithEventType:Main_DownloadFailed key:Main_DownloadFailed];
        [self deleteFoldersInDocuments:onlineThemeModel.themeName];
        onlineThemeModel.downloadStatus = CKOnlineThemeStatusNotDownload;
        [self.themeTableView reloadData];
        return;
    }
    else if(onlineThemeModel.downloadStatus == CKOnlineThemeStatusDownloaded){ // 下载完成
        onlineThemeModel.downloadStatus = CKOnlineThemeStatusOnUse;

        for (CKOnlineThemeModel * model in _onlineThemeModelArray) {
            if (model != onlineThemeModel && model.downloadStatus == CKOnlineThemeStatusOnUse) {
                model.downloadStatus = CKOnlineThemeStatusDownloaded;
            }
        }
        
        if ([onlineThemeModel.themeName isEqualToString:@"rainyday"]) {
            onlineThemeModel.themeName = @"lollipop";
        }
        
        // 友盟统计
        NSDictionary *dict = @{Main_SelectedTheme : onlineThemeModel.themeName};
        [[CKTracker sharedTraker] logWithCategory:CK_APP_CATEGORY
                                           action:Main_SelectedTheme
                                            label:onlineThemeModel.themeName];
        [CKSaveTools saveClickWithEventType:Main_SelectedTheme key:onlineThemeModel.themeName];
        
        [self saveThemeModelToAppGroupWithOnlineModelName:onlineThemeModel.themeName];
        NSUserDefaults * sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUPS];
        [sharedDefaults setObject:[NSDate date] forKey:Last_Save_Theme_Date];
        [sharedDefaults synchronize];
        [self.themeTableView reloadData];
        return;
    }

}

- (void)downloadWithOnlineThemeModel:(CKOnlineThemeModel *)onlineThemeModel downloadButton:(UIButton *)downloadButton{
    
    for (ThemeDownload * isDownloading in self.downloadTaskarray) {
        if (isDownloading.flag == onlineThemeModel.onlineIndex) { // 如果正在下载,不能让他点两次
            return;
        }
    }
    ThemeDownload * download = [[ThemeDownload alloc] init];
    download.flag = onlineThemeModel.onlineIndex;
    download.delegate = self;
    [self createOnlineThemesWithThemeName:onlineThemeModel.themeName];
    [download downloadWithZipURL:[NSURL URLWithString:onlineThemeModel.package] toSaveURL:Online_Theme_URL withThemeName:onlineThemeModel.themeName andThemeIndex:onlineThemeModel.onlineIndex];
    [self.downloadTaskarray addObject:download];
}
- (NSURL *)downloadFolderURL
{
    NSFileManager * myFileManger = [NSFileManager defaultManager];
    NSError * error;
    NSURL * onlineThemeURL = Online_Theme_URL;
    if (![myFileManger fileExistsAtPath:onlineThemeURL.path]) {
        [myFileManger createDirectoryAtURL:onlineThemeURL withIntermediateDirectories:YES attributes:nil error:&error];
    }
    return onlineThemeURL;

}
- (NSURL *)createOnlineThemesWithThemeName:(NSString *)themeName
{
    NSFileManager * myFileManger = [NSFileManager defaultManager];
    NSError * error;
    NSURL * themeURL = [[self downloadFolderURL] URLByAppendingPathComponent:themeName];
    if (![myFileManger fileExistsAtPath:themeURL.path]) {
        [myFileManger createDirectoryAtURL:themeURL withIntermediateDirectories:YES attributes:nil error:&error];
    }
    return themeURL;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _onlineThemeModelArray.count;
}
- (CKOnlineThemeCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CKOnlineCell";
    CKOnlineThemeCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CKOnlineThemeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier ];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CKOnlineThemeModel * onlineThemeModel = _onlineThemeModelArray[indexPath.row];
    onlineThemeModel.onlineIndex = (int)indexPath.row;
    onlineThemeModel.downloadStatus = CKOnlineThemeStatusNotDownload;
    if ([self.hasDownloadedNameArrayM containsObject:onlineThemeModel.themeName]) {// 已下载
        if ([_nowUseThemeModel.themeName isEqualToString:@"rainyday"]) {
            _nowUseThemeModel.themeName = @"lollipop";
        }
        if (_nowUseThemeModel && [_nowUseThemeModel.themeName isEqualToString:onlineThemeModel.themeName]) {
           // 当前正在使用的主题存在且名字与该行主题名字相等
            onlineThemeModel.downloadStatus = CKOnlineThemeStatusOnUse;
        }
        else{
            onlineThemeModel.downloadStatus = CKOnlineThemeStatusDownloaded;
        }
    }
    cell.onlineThemeModel = onlineThemeModel;
    cell.delegate = self;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
        if (iPhone6) {
            return 119 - 10;
        }
        else if (iPhone6s)
        {
            return 128.3 - 10;
        }
        else if (iPad)
        {
            return 210;
        }
        else
            return 106 - 10;
}
- (void)ThemeDownloadProgressDelegate:(CGFloat)progress themeIndex:(int)themeIndex downloadButton:(UIButton *)downloadButton {
//    CKLog(@"%lf===progress",progress);
}

- (void)saveThemeModelToAppGroupWithOnlineModelName:(NSString *)themeName
{
    CKMainThemeModel * current = [self mainThemeModelWithOnlineThemeName:themeName];
    current.online = YES;
    if ([current.themeName isEqualToString:@"rainyday"]) {
        current.themeName = @"lollipop";
    }
    [CKSaveTools saveMainTheme:current toRegion:CKSaveRegionAppGroup];
    _nowUseThemeModel = [CKMainThemeModel nowUseThemeModel];
}

- (CKMainThemeModel *)mainThemeModelWithOnlineThemeName:(NSString *)themeName
{
    NSString * Json = [self jsonWithThemeName:themeName];
    return [CKMainThemeModel mainThemeModelWithJsonString:Json];
}

- (NSString * )jsonWithThemeName:(NSString *)themeName
{
    NSError * error;
    NSURL * url = [Online_Theme_URL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@/Info.txt",themeName]];
    return [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
}

- (void)updateDownloadedArrayWithThemeIndex:(int)themIndex
{
    CKOnlineThemeModel * onlineModel = _onlineThemeModelArray[themIndex];
    NSString * Json = [self jsonWithThemeName:onlineModel.themeName];
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:[NSString dictionaryWithJsonString:Json]];
    if ([dict[@"themeName"] isEqualToString:@"rainyday"]) {
        dict[@"themeName"] = @"lollipop";
    }
    NSURL * arrayURL = [Online_Theme_URL URLByAppendingPathComponent:@"downloadOnlineThemeArray.plist"];
    NSMutableArray * downloadArrayM = [NSMutableArray arrayWithContentsOfURL:arrayURL];
        downloadArrayM = (downloadArrayM == nil)?[NSMutableArray array]:downloadArrayM;
    [downloadArrayM addObject:dict];
    [downloadArrayM writeToURL:arrayURL atomically:YES];
}
- (void)themeUnzipDoneSuccess:(BOOL)success themeIndex:(int)themeIndex downloadButton:(UIButton *)downloadButton
{
    if (success) {
        CKOnlineThemeModel * onlineTheme = _onlineThemeModelArray[themeIndex];
        onlineTheme.downloadStatus = CKOnlineThemeStatusDownloaded;
        if (![self.hasDownloadedNameArrayM containsObject:onlineTheme.themeName]) {
            [self.hasDownloadedNameArrayM addObject:onlineTheme.themeName];
        }
        [self updateDownloadedArrayWithThemeIndex:themeIndex];
        [self.themeTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:themeIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
}

// 在取消下载和删除下载主题的时候将Documents下的文件夹删除
- (void)deleteFoldersInDocuments:(NSString *)folderName
{
    NSURL * folderUrl = [Online_Theme_URL URLByAppendingPathComponent:folderName];
    NSFileManager *myFileManager=[NSFileManager defaultManager];
    NSError * error;
    if ([myFileManager fileExistsAtPath:folderUrl.path]) {
    [myFileManager removeItemAtPath:folderUrl.path error:&error];
    }
}


- (void)loadData:(NSNotification *)note
{
    
    if (_onlineThemeModelArray.count == 0) {
        [self firstThemeInfo];
//        AFNetworkReachabilityManager * ReachabilityManager = [AFNetworkReachabilityManager sharedManager];
//        if (ReachabilityManager.reachable) { // 如果有网络
//        }
//        else
//        {
//            [self showNetworkStatus:@"Network is not stable, please reconnect." deley:2];
//        }
    }
    [_hasDownloadedNameArrayM removeAllObjects];
    _hasDownloadedNameArrayM = nil;
    _nowUseThemeModel = [CKMainThemeModel nowUseThemeModel];
    [_themeTableView reloadData];
    
}

- (void)currenNetWorkStatus:(NSNotification *)note
{
    NSDictionary * dict = note.userInfo;
    NSString * netStatus = dict[AFNetworkingReachabilityNotificationStatusItem];
    switch (netStatus.intValue) {
        case 0: // 没网了
            [self showNetworkStatus:@"Network is not stable, please reconnect." deley:2];
            break;
        case 1:
            [self loadThemeIfNoThemeNow];
            break;
        case 2:
            [self loadThemeIfNoThemeNow];
            break;
        default:
            break;
    }
    
}

- (void)loadThemeIfNoThemeNow // 如果现在还没loadTheme,才执行
{
    [self firstThemeInfo];
    if (!_onlineThemeModelArray.count) {
    }
    else // 如果有数据的话只提示网络重新连接
    {
        [self showNetworkStatus:@"Network connect" deley:1];
    }
}


- (void)firstThemeInfo
{
    HUD = [[MBProgressHUD alloc] initWithView:self];
    [self addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = @"Loading";
    [HUD show:YES];
    [self LoadNewThemeWithURL:CKTHEME_URL];
}

- (void)showNetworkStatus:(NSString *)status deley:(float)time
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = status;
    hud.margin = 10.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:time];
}

-(void)LoadNewThemeWithURL:(NSString*)url
{
    // 1. 请求管理器
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置响应者的可接受内容类型，让响应者能够反序列化HTML文本，修改mimeType
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    // 2. 发起请求
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (responseObject) {
            CKLog(@"%@==ree",responseObject);
            NSMutableArray * tempArray = [NSMutableArray array];
            for (NSArray * array in responseObject[@"data"]) {
                NSDictionary * dict = array.lastObject;
                [tempArray addObject:dict];
            }
            if (tempArray) {
                _onlineThemeModelArray = [NSMutableArray array];
                [_onlineThemeModelArray addObjectsFromArray:[CKOnlineThemeModel allOnlineThemeModelsWithOnlineArray:tempArray]];
                if (_onlineThemeModelArray) {
                    [self OnlineThemeViewShouldAddTableView];
                }
            }
            [HUD hide:YES];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [HUD hide:YES];
        [self showNetworkStatus:@"Network is not stable, please reconnect." deley:2];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        });
    }];
}
@end
