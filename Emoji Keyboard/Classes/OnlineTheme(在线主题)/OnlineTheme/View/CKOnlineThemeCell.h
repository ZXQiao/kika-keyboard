//
//
//  Created by 张赛 on 14-7-28.
//  Copyright (c) 2014年 张赛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKOnlineThemeModel.h"

@protocol CKOnlineThemeCellDelegate <NSObject>
-(void)onlineThemeCellButtonClicked:(UIButton *)button;
@end

@interface CKOnlineThemeCell : UITableViewCell
@property (nonatomic,strong) UIView * backView;
@property (nonatomic, strong) UIImageView * themeImageView;
@property (nonatomic, strong) UILabel * themeNameLabel;
@property (nonatomic, strong) UIButton * downloadTypeButton;
@property (nonatomic, strong) UIButton * selectButton;

@property (nonatomic, strong) CKOnlineThemeModel * onlineThemeModel;
@property (nonatomic, weak) id<CKOnlineThemeCellDelegate> delegate;
@end
