//
//  SHTitleCell.m
//  SweetHeart
//
//  Created by 张赛 on 14-7-28.
//  Copyright (c) 2014年 张赛. All rights reserved.
//


/*
 这里要注意:
     backView.userInteractionEnabled = YES;
     themeImageView.userInteractionEnabled = YES;
 */



#import "CKOnlineThemeCell.h"
#import "Consetting.h"
#import "UIImageView+WebCache.h"
@interface CKOnlineThemeCell()



@end

@implementation CKOnlineThemeCell

- (void)setOnlineThemeModel:(CKOnlineThemeModel *)onlineThemeModel
{
    _onlineThemeModel = onlineThemeModel;
    _themeNameLabel.text = onlineThemeModel.themeName;
    int statusIndex = onlineThemeModel.downloadStatus;
    NSString * downloadStatus = ONLINE_THTME_STATUS_A[statusIndex];
    [_downloadTypeButton setTitle:downloadStatus forState:UIControlStateNormal];
    [_downloadTypeButton setTitleColor:[UIColor colorFromHexString:ONLINE_BUTTON_TITLE_COLOR[statusIndex]] forState:UIControlStateNormal];
    [_downloadTypeButton setBackgroundImage:[UIImage imageNamed:ONLINE_BUTTON_BACK_IMAEG[statusIndex]] forState:UIControlStateNormal];
    _downloadTypeButton.enabled = YES;
    if (statusIndex == 3) {
       _downloadTypeButton.enabled = NO;
    }
    
    _downloadTypeButton.tag = onlineThemeModel.onlineIndex;
    NSURL * imageURL =[NSURL URLWithString:onlineThemeModel.icon];
  
    [self.themeImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRefreshCached];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // 背景图
         UIView * backView = [[UIView alloc] init];
          self.backView = backView;
         [self.contentView addSubview:backView];
        backView.userInteractionEnabled = YES;
        // 大图
        UIImageView * themeImageView = [[UIImageView alloc] init];
        self.themeImageView =  themeImageView;
        [self.backView addSubview:themeImageView];
        themeImageView.layer.cornerRadius = 5;
        themeImageView.layer.masksToBounds = YES;
        themeImageView.userInteractionEnabled = YES;
        
        // 名称
        UILabel * themeNameLabel = [[UILabel alloc] init];
        themeNameLabel.text = @"ThemeName";
        self.themeNameLabel = themeNameLabel;
        self.themeNameLabel.textAlignment = NSTextAlignmentCenter;
        self.themeNameLabel.font = [UIFont systemFontOfSize:16];
        [self.themeImageView addSubview:themeNameLabel];
        themeNameLabel.textColor = [UIColor whiteColor];
        self.themeNameLabel.numberOfLines = 0;

        // 下载状态按钮
        UIButton * downloadTypeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.downloadTypeButton = downloadTypeButton;
        [downloadTypeButton addTarget:self action:@selector(downloadButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.themeImageView addSubview:downloadTypeButton];
        CGFloat fontSize = 13;
        if (iPhone4 || iPhone5) {
            fontSize = 11;
        }
        downloadTypeButton.titleLabel.font = [UIFont systemFontOfSize:fontSize];
        // 选中按钮
        UIButton * selectButton = [[UIButton alloc] init];
        self.selectButton = selectButton;
        [self.themeImageView addSubview:selectButton];
        self.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
        self.clipsToBounds = NO;
        
        downloadTypeButton.adjustsImageWhenDisabled = NO;
        
    }
    return self;
}

- (void)downloadButtonClicked:(UIButton *)button
{
    if ([self.delegate respondsToSelector:@selector(onlineThemeCellButtonClicked:)]) {
        [self.delegate onlineThemeCellButtonClicked:button];
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat topPadding = 10;
    CGFloat leftPadding = self.frame.size.width * 1/ 20;
    CGFloat backViewW = self.frame.size.width - 2 * leftPadding;
    CGFloat backViewH = backViewW / 690 * 182;
    CGFloat backViewX = leftPadding;
    CGFloat backViewY = topPadding;
    self.backView.frame = CGRectMake(backViewX, backViewY, backViewW, backViewH);
    CGFloat padding = .5;
    CGFloat themeImageViewH = backViewH - 2 * padding;
    CGFloat themeImageViewW = themeImageViewH / 182 * 690;
    CGFloat themeImageViewX = padding;
    CGFloat themeImageViewY = padding;
    self.themeImageView.frame = CGRectMake(themeImageViewX, themeImageViewY, themeImageViewW, themeImageViewH);
    
    CGFloat themeNameLabelX = self.frame.size.width * .6;
    CGFloat themeNameLabelY = backViewH / 10;
    CGFloat themeNameLabelW = CGRectGetMaxX(self.themeImageView.frame)- themeNameLabelX - padding;
    CGFloat themeNameLabelH = themeImageViewH / 3;
    self.themeNameLabel.frame = CGRectMake(themeNameLabelX, themeNameLabelY, themeNameLabelW, themeNameLabelH);
    
    CGFloat themeIntroduceLabelX = themeNameLabelX - 15;
    CGFloat themeIntroduceLabelY = themeNameLabelY + themeNameLabelH ;
    CGFloat themeIntroduceLabelW = themeNameLabelW + 15;
    CGFloat themeIntroduceLabelH = backViewH / 3;
    self.downloadTypeButton.frame = CGRectMake(themeIntroduceLabelX, themeIntroduceLabelY, themeIntroduceLabelW - 30, themeIntroduceLabelH);
    self.downloadTypeButton.center = CGPointMake(self.themeNameLabel.center.x, self.downloadTypeButton.center.y);

    CGFloat selectButtonY = padding + themeImageViewH / 3 * 2;
    CGFloat selectButtonH = themeImageViewH / 3;
    CGFloat selectButtonW = selectButtonH;
    CGFloat selectButtonX = CGRectGetMaxX(self.themeImageView.frame) - selectButtonW;

    self.selectButton.frame = CGRectMake(selectButtonX, selectButtonY, selectButtonW, selectButtonH);
}


@end
