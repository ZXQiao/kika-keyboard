//
//  HiFontDownload.m
//  meituSDK
//
//  Created by 张赛 on 15/1/7.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//

#import "ThemeDownload.h"
#import "ZipArchive.h"

//typedef void(^ProgressBlock)(float percent);
@interface ThemeDownload() <NSURLConnectionDataDelegate>
@property (nonatomic, strong) NSMutableData *dataM;
// 保存在沙盒中的文件路径
@property (nonatomic, strong) NSString *cachePath;
@property (nonatomic, assign) long long fileLength;
// 当前下载的文件长度
@property (nonatomic, assign) long long currentLength;
@property (nonatomic, strong)  NSString * fontSize;
@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, assign) int timeCount;
@property (nonatomic,strong) NSTimer * timer;
@property (nonatomic, strong) NSURLConnection *connection;

@property (nonatomic, strong) UIButton * downloadButton;
@property (nonatomic, strong) NSString * currentThemeName;
@property (nonatomic, strong) NSURL * saveToURL;
@property (nonatomic, assign) int themeIndex;

@end

@implementation ThemeDownload


- (NSOperationQueue *)queue
{
    if (!_queue) _queue = [[NSOperationQueue alloc] init];
    return _queue;
}



- (NSMutableData *)dataM
{
    if (!_dataM) {
        _dataM = [NSMutableData data];
    }
    return _dataM;
}

- (void)downloadWithURL:(NSURL *)url themeIndexStr:(NSString *)themeIndexStr downloadButton:(UIButton *)downloadButton
{
    // 1. request GET
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // 2. connection
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [connection setDelegateQueue:[[NSOperationQueue alloc] init]];
    self.connection = connection;
    // 记录当前的触发按钮
    self.downloadButton = downloadButton;
    // 3. 启动连接
    [connection start];
}

- (void)downloadWithZipURL:(NSURL *)zipURL toSaveURL:(NSURL *)saveURL withThemeName:(NSString *)themeName andThemeIndex:(int)index
{
     NSURLRequest *request = [NSURLRequest requestWithURL:zipURL];
    _currentThemeName = themeName;
    _saveToURL = saveURL;
    _themeIndex = index;
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [connection setDelegateQueue:[[NSOperationQueue alloc] init]];
    self.connection = connection;
    [connection start];
}

#pragma mark - 代理方法
// 1. 接收到服务器的响应，服务器执行完请求，向客户端回传数据
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
 
    NSString * suggestedFileName = [NSString stringWithFormat:@"OnlineTheme_%@.zip",_currentThemeName]; // zip包的名字
    NSString * cachePath = _saveToURL.path;
    self.cachePath = [cachePath stringByAppendingPathComponent:suggestedFileName];
    self.fileLength = response.expectedContentLength;
    self.currentLength = 0;
    self.dataM.data = nil;
}

// 2. 接收数据，从服务器接收到数据
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // 拼接数据
    [self.dataM appendData:data];
    
    // 根据data的长度增加当前下载的文件长度
    self.currentLength += data.length;

    float size = self.fileLength;
    if (size > 1024*100)//0.1MB
    {
        self.fontSize = [NSString stringWithFormat:@"%.2fMB", size/(1024*1024)];
    }
    else
    {
        self.fontSize = [NSString stringWithFormat:@"%.1fKB", size/(1024)];
    }
    
    float progress = (float)self.currentLength / self.fileLength;
    
    if (progress) {
        if ([self.delegate respondsToSelector:@selector(ThemeDownloadProgressDelegate:themeIndex:downloadButton:)]) {
            [self.delegate ThemeDownloadProgressDelegate:progress themeIndex:_themeIndex downloadButton:self.downloadButton];
        }
    }
    
    // 解压
    if (progress == 1) {
        NSBlockOperation *op1 = [NSBlockOperation blockOperationWithBlock:^{
            [self checkDownLoadDone];
               }];
        NSBlockOperation *op2 = [NSBlockOperation blockOperationWithBlock:^{
            [self unZipFile];
        }];
        [op2 addDependency:op1];
        [self.queue addOperation:op1];
        [self.queue addOperation:op2];
        
    }
}

- (void)timeCountUp
{
    self.timeCount ++;
}

// 检查是否下载完毕
- (void)checkDownLoadDone
{
     NSFileManager *myFileManager=[NSFileManager defaultManager];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:3 / 10 target:self selector:@selector(timeCountUp) userInfo:nil repeats:NO];
    self.timer = timer;
    self.timeCount = 0;
    while ( ![myFileManager fileExistsAtPath:self.cachePath]) {
        if (self.timeCount > 10) {
            break;
        }
    }
}

// 解压下载完成的文件
- (void)unZipFile
{
    NSFileManager *myFileManager=[NSFileManager defaultManager];
    ZipArchive *za = [[ZipArchive alloc] init];
    NSError *__block error = nil;
    if ([za UnzipOpenFile: self.cachePath]) {
        // 2. 将解压缩的内容写到缓存目录中
        BOOL ret = [za UnzipFileTo:self.saveToURL.path overWrite: YES];
        if (NO == ret){} [za UnzipCloseFile];
            [myFileManager removeItemAtPath:self.cachePath error:&error];
        NSString * dustbinPath = [self.saveToURL.path stringByAppendingPathComponent:@"__MACOSX"];
        [myFileManager removeItemAtPath:dustbinPath error:&error];
        // 这里有UI操作,所以需要在主线程调用代理方法
        [self performSelector:@selector(sendSuccessToView) onThread:[NSThread mainThread] withObject:nil waitUntilDone:YES];
    }
}

- (void)sendSuccessToView
{
    if ([self.delegate respondsToSelector:@selector(themeUnzipDoneSuccess:themeIndex:downloadButton:)]) {
        [self.delegate themeUnzipDoneSuccess:YES themeIndex:_themeIndex downloadButton:self.downloadButton];
    }
}

// 3. 完成接收
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // 写入数据，NSURLConnection底层实现是用磁盘做的缓存
    [self.dataM writeToFile:self.cachePath atomically:YES];
}

// 4. 出现错误
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
}

- (void)cancelDownloadTheme 
{
    if (self.connection) {
        [self.connection cancel];
    }
}
@end
