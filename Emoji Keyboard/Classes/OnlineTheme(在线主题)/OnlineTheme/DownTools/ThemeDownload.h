
//
//  HiFontDownload.h
//  meituSDK
//
//  Created by 张赛 on 15/1/7.
//  Copyright (c) 2015年 zhangsai. All rights reserved.
//



#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol ThemeDownloadDelegate <NSObject>

- (void)ThemeDownloadProgressDelegate:(CGFloat )progress themeIndex:(int)themeIndex downloadButton:(UIButton *)downloadButton
;

- (void)themeUnzipDoneSuccess:(BOOL)success themeIndex:(int)themeIndex downloadButton:(UIButton *)downloadButton;

@end

@interface ThemeDownload : NSObject

- (void)downloadWithURL:(NSURL *)url themeIndexStr:(NSString *)themeIndexStr downloadButton:(UIButton *)downloadButton;
- (void)downloadWithZipURL:(NSURL *)zipURL toSaveURL:(NSURL *)saveURL withThemeName:(NSString *)themeName andThemeIndex:(int)index;

- (void)cancelDownloadTheme;

@property (nonatomic, weak) id<ThemeDownloadDelegate> delegate;

// 用来记录download的序号,便于停止
@property (nonatomic, assign) int flag;


@end
