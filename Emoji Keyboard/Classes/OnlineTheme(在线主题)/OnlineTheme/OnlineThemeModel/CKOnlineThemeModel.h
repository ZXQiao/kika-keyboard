//
//  CKOnlineThemeModel.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/16.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

// online主题的下载状态
typedef NS_ENUM(int, CKOnlineThemeStatus) {
    CKOnlineThemeStatusNotDownload = 0, // 未下载
    CKOnlineThemeStatusIsDownloading, // 正在下载
    CKOnlineThemeStatusDownloaded, // 已下载
    CKOnlineThemeStatusOnUse // 正在使用
};
#define ONLINE_THTME_STATUS_A @[@"Download",@"Cancel",@"Use Theme",@"Current Theme"]
#define ONLINE_BUTTON_TITLE_COLOR @[@"#78c8ff",@"#aaaaaa",@"#ffffff",@"#ffffff"]
#define ONLINE_BUTTON_BACK_IMAEG @[@"Download",@"onlineCancel",@"Use Theme",@"Current Theme"]

@interface CKOnlineThemeModel : NSObject
@property (nonatomic, copy) NSString * package;
@property (nonatomic, copy) NSString * icon;
@property (nonatomic, copy) NSString * themeName;
@property (nonatomic, assign) CKOnlineThemeStatus downloadStatus;
@property (nonatomic, assign) int onlineIndex;


+ (instancetype)onlineThemeModelWithDict:(NSDictionary *)dict;
+ (NSArray *)allOnlineThemeModelsWithOnlineArray:(NSArray *)onlineArray;

@end
