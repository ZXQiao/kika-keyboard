//
//  CKOnlineThemeModel.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/9/16.
//  Copyright © 2015年 sai.zhang. All rights reserved.
//

#import "CKOnlineThemeModel.h"

@implementation CKOnlineThemeModel

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        if (dict[@"package"]) {
            self.package = dict[@"package"];
        }
        if (dict[@"icon"]) {
            self.icon = dict[@"icon"];
        }
        if (dict[@"themeName"]) {
            self.themeName = dict[@"themeName"];
        }
    }
    return self;
}

+ (instancetype)onlineThemeModelWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

+(NSArray *)allOnlineThemeModelsWithOnlineArray:(NSArray *)onlineArray
{
    NSMutableArray * arrayM = [NSMutableArray array];
    for (NSDictionary * dict in onlineArray) {
        CKOnlineThemeModel * onlineTheme = [[CKOnlineThemeModel alloc] initWithDict:dict];
        [arrayM addObject:onlineTheme];
    }
    return (NSArray *)arrayM;
}



@end
