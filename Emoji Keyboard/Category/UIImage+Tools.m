//
//  UIImage+Tools.m
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/24.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import "UIImage+Tools.h"
#import <CoreImage/CoreImage.h>

@implementation UIImage (Tools)
#define Group_Online_Theme_URL [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS]URLByAppendingPathComponent:@"OnlineThemes"]
+ (UIImage *)compressImage:(UIImage *)sourceImage withRatio:(CGFloat)ratio{
    
    return nil;
}

+ (UIImage *)compressImage:(UIImage *)sourceImage withDestionSize:(CGSize)size{
    UIGraphicsBeginImageContext(size);
    [sourceImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage * finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
}
//+ (NSMutableArray *)praseGIFDataToImageArray:(NSData *)data;
//{
//    NSMutableArray *frames = [[NSMutableArray alloc] init];
//    CGImageSourceRef src = CGImageSourceCreateWithData((CFDataRef)data, NULL);
//    CGFloat animationTime = 0.f;
//    if (src) {
//        size_t l = CGImageSourceGetCount(src);
//        frames = [NSMutableArray arrayWithCapacity:l];
//        for (size_t i = 0; i < l; i++) {
//            CGImageRef img = CGImageSourceCreateImageAtIndex(src, i, NULL);
//            NSDictionary *properties = (NSDictionary *)CGImageSourceCopyPropertiesAtIndex(src, i, NULL);
//            NSDictionary *frameProperties = [properties objectForKey:(NSString *)kCGImagePropertyGIFDictionary];
//            NSNumber *delayTime = [frameProperties objectForKey:(NSString *)kCGImagePropertyGIFUnclampedDelayTime];
//            animationTime += [delayTime floatValue];
//            if (img) {
//                [frames addObject:[UIImage imageWithCGImage:img]];
//                CGImageRelease(img);
//            }
//        }
//        CFRelease(src);
//    }
//    return frames;
//}

+ (NSURL *)URLWithThemeName:(NSString *)themeName ExtensionString:(NSString *)extension
{
    NSString * imageName = [NSString stringWithFormat:@"%@/%@%@@2x.png",themeName,themeName,extension];
    return  [Group_Online_Theme_URL URLByAppendingPathComponent:imageName];
}

+ (UIImage *)imageWithThemeName:(NSString *)themeName ImageNameExtension:(NSString *)extension resize:(BOOL)resize
{
    
    if (!resize) {
        return [UIImage imageWithContentsOfFile:[UIImage URLWithThemeName:themeName ExtensionString:extension].path];
    }else
    {
        return [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[UIImage URLWithThemeName:themeName ExtensionString:extension].path]];
    }
}


+ (UIImage *)imageFromMainBundleWithThemeName:(NSString *)themeName ImageNameExtension:(NSString *)extension resize:(BOOL)resize
{
    NSString * imageName = [NSString stringWithFormat:@"%@%@@2x.png",themeName,extension];

    if (!resize) {
    return [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:imageName ofType:nil]];
    }
    else
    {
    return [UIImage resizableImageWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:imageName ofType:nil]]];
    }
}

- (UIImage *) tintImageWithColor:(UIColor *)tintColor
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0, self.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, rect, self.CGImage);
    
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    [tintColor setFill];
    CGContextFillRect(context, rect);
    
    UIImage *coloredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return coloredImage;
}

+ (instancetype)imageWithBundlePath:(NSString *)imageName imageType:(NSString *)type
{
    if (type == nil) {
        type = @"png";
    }
    NSString * path = [[NSBundle mainBundle] pathForResource:imageName ofType:type];
    return [UIImage imageWithContentsOfFile:path];
}
+ (instancetype)resizableImageWithImage:(UIImage *)sourceImage
{
    CGFloat top = sourceImage.size.height / 2.0 ; // 顶端盖高度
    CGFloat bottom = sourceImage.size.height / 2.0; // 底端盖高度
    CGFloat left = sourceImage.size.width /2.0 ; // 左端盖宽度
    CGFloat right = sourceImage.size.width / 2.0; // 右端盖宽度
    UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom,right);
    // 指定为拉伸模式，伸缩后重新赋值
    UIImage * destionImage = [sourceImage resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeTile];
    return destionImage;
}

+ (instancetype)resizableImageNamed:(NSString *)name
{
    return [self resizableImageNamed:name left:0.5 top:0.5];
}

+ (instancetype)resizableImageNamed:(NSString *)name left:(CGFloat)leftRatio top:(CGFloat)topRatio
{
    UIImage *image = [UIImage imageNamed:name];
    CGFloat left = image.size.width * leftRatio;
    CGFloat top = image.size.height * topRatio;
    return [image stretchableImageWithLeftCapWidth:left topCapHeight:top];
    
}

- (UIImage *) imageWithTintColor:(UIColor *)tintColor
{
    //We want to keep alpha, set opaque to NO; Use 0.0f for scale to use the scale factor of the device’s main screen.
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
    [tintColor setFill];
    CGRect bounds = CGRectMake(0, 0, self.size.width, self.size.height);
    UIRectFill(bounds);
    //Draw the tinted image in context
    [self drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:1.0f];
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}

#pragma mark 传入一个按钮,及其imageName,以及监听方法
+ (void)creatBtn:(UIButton *) button imageName:(NSString *)imageName action:(SEL)action target:(id)target
{

    UIImage * imageReturn = [UIImage imageNamed:imageName];
    if ([imageName isEqualToString:@"gifDelete"] || [imageName isEqualToString:@"gifToKeyboard"]) {
        imageReturn = [imageReturn imageWithTintColor:[UIColor blackColor]];
    }
    [button setImage:imageReturn forState:UIControlStateNormal];
    [button setImage:imageReturn forState:UIControlStateHighlighted];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat top = 0;
    CGFloat left = 0;
    CGFloat bottom = 0;
    CGFloat right = 0;
    CGFloat marginInset = button.frame.size.height * .25;
    CGFloat imageW = imageReturn.size.width;
    CGFloat imageH = imageReturn.size.height;
    
    CGFloat showH = button.frame.size.height - 2 * marginInset;
    CGFloat showW = showH / imageH * imageW;
    
    top = marginInset;
    bottom = marginInset;
    left = (button.frame.size.width - showW) / 2;
    right = left;
    
    button.contentEdgeInsets = UIEdgeInsetsMake(top, left,bottom, right);
    button.adjustsImageWhenHighlighted = NO;
    button.showsTouchWhenHighlighted = YES;
}
+ (void)creatBtn:(UIButton *) button image:(UIImage *)image imagePress:(UIImage *)imagePress
{
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:imagePress forState:UIControlStateHighlighted];
    [button setImage:imagePress forState:UIControlStateSelected];
    
    CGFloat top = 0;
    CGFloat left = 0;
    CGFloat bottom = 0;
    CGFloat right = 0;
    CGFloat marginInset = button.frame.size.height * .125;
    CGFloat imageW = image.size.width;
    CGFloat imageH = imagePress.size.height;
    
    CGFloat showH = button.frame.size.height - 2 * marginInset;
    CGFloat showW = showH / imageH * imageW;
    
    top = marginInset;
    bottom = marginInset;
    left = (button.frame.size.width - showW) / 2;
    right = left;
    
    button.contentEdgeInsets = UIEdgeInsetsMake(top, left,bottom, right);
    button.adjustsImageWhenHighlighted = NO;
    button.showsTouchWhenHighlighted = YES;
}

// 传进来一个按钮，和一个图片的名称，返回设置好的按钮；
+(void)buttonMatchImage:(UIButton *)button imageName:(NSString *)imageName color:(UIColor *)color ratio:(CGFloat )ratio
{
    UIImage * image = [UIImage imageNamed:imageName];
    CGFloat buttonW = button.frame.size.width;
    CGFloat buttonH = button.frame.size.height;
    
    // 获得较短边
    CGFloat lessLength = 0;
    CGFloat topPadding = 0;
    CGFloat leftPadding = 0;
    // 宽度小
    if (buttonW <= buttonH) {
        leftPadding = ratio * buttonW;
        lessLength = buttonW - leftPadding * 2;
        topPadding = (buttonH - lessLength) / 2;
        
    }
    if (buttonW > buttonH) {
        topPadding = ratio * buttonH;
        lessLength = buttonH - topPadding * 2;
        leftPadding = (buttonW - lessLength) / 2;
    }
    
    button.contentEdgeInsets = UIEdgeInsetsMake(topPadding, leftPadding, topPadding, leftPadding);
    
    [button setImage:[image imageWithTintColor:color] forState:UIControlStateNormal];
    [button setImage:[image imageWithTintColor:color]forState:UIControlStateHighlighted];
    [button setImage:[image imageWithTintColor:[UIColor whiteColor]] forState:UIControlStateSelected];
}


@end
