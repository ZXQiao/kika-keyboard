//
//  NSString+EKStuff.h
//  EKAlgorithms
//
//  Created by Vittorio Monaco on 26/11/13.
//  Copyright (c) 2013 EvgenyKarkan. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSString (EKStuff)

- (BOOL)isPalindrome;
- (NSString *)reversedString;
- (NSUInteger)numberOfWordsInString;
- (void)countEachCharacterOccurrenceInString;
- (NSUInteger)numberOfOccurrenciesOfString:(NSString *)needle;
+ (NSString *)randomStringWithLength:(NSUInteger)lenght;
- (NSString *)concatenateWithString:(NSString *)secondString;
- (NSInteger)indexOfFirstOccurrenceOfNeedle:(NSString *)needle;
- (NSInteger)indexOfLastOccurrenceOfNeedle:(NSString *)needle;
+ (void)allPermutationsOfCString:(char *)string withFirstCharacterPosition:(int)i lastCharacterPosition:(int)n;
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
/**
 *@param
 *  other: the string for comparing with self
 *@Output
 *  an array of common sequence of the two strings
 */
- (NSArray*)LCS_WithString:(NSString*)other;
/**
 *@param
 *  other: the string for comparing with self
 *@Output
 *  the Levenshtein Distance of the two strings
 */
- (NSInteger)LD_WithString:(NSString*)other;

- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize )maxSize;

+ (instancetype)lastStrWithStr:(NSString *)str;
+ (instancetype)lastStrIncludeLastSpacesWithStr:(NSString *)str;

+(NSString *)GetLastInputStr:(NSString*)str;
-(NSString*)deleteTailSpaceOfStr:(NSString*)str;
+(instancetype)translateCoolFontSting:(NSString *)sourceStirng dict:(NSDictionary *)dict oppositeDict:(NSDictionary *)oppositeDict;
+(instancetype)translateNormalFontString:(NSString *)inputWord ToCoolFontStrWithDict:(NSDictionary *)coolFontDict;
+ (NSString *)lastStrAfterSymbolsWithStr:(NSString *)str dict:(NSDictionary *)dict oppositeDict:(NSDictionary *)oppositeDict;

- (BOOL)isIncludingEmoji ;
-(NSString *)GetStrFromEmojiStr;
-(NSMutableArray *)GetReverseStrFromEmojiStr;
+(NSString*)GetReverseStr:(NSMutableArray*)arr;
+(int)totalCharacterCount:(NSString *)aString;
@end
