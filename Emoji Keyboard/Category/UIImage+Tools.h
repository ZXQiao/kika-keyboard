//
//  UIImage+Tools.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/3/24.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Tools)
+ (instancetype)resizableImageWithImage:(UIImage *)sourceImage;
+ (instancetype)resizableImageNamed:(NSString *)name;

+ (instancetype)resizableImageNamed:(NSString *)name left:(CGFloat)left top:(CGFloat)top;

- (UIImage *) imageWithTintColor:(UIColor *)tintColor;
/**
 *  传进一个按钮,以及按钮的监听方法
 */
+ (void)creatBtn:(UIButton *) button imageName:(NSString *)imageName action:(SEL)action target:(id)target;

+(void)buttonMatchImage:(UIButton *)button imageName:(NSString *)imageName color:(UIColor *)color ratio:(CGFloat )ratio;

+ (void)creatBtn:(UIButton *) button image:(UIImage *)image imagePress:(UIImage *)imagePress;

+ (instancetype)imageWithBundlePath:(NSString *)imageName imageType:(NSString *)type;

- (UIImage *) tintImageWithColor:(UIColor *)tintColor;

+ (NSURL *)URLWithThemeName:(NSString *)themeName ExtensionString:(NSString *)extension;
+ (UIImage *)imageWithThemeName:(NSString *)themeName ImageNameExtension:(NSString *)extension resize:(BOOL)resize;
+ (UIImage *)imageFromMainBundleWithThemeName:(NSString *)themeName ImageNameExtension:(NSString *)extension resize:(BOOL)resize;

+ (UIImage *)compressImage:(UIImage *)sourceImage withRatio:(CGFloat)ratio;
+ (UIImage *)compressImage:(UIImage *)sourceImage withDestionSize:(CGSize)size;
@end
