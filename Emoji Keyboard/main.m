

//  main.m
//  Emoji Keyboard
//
//  Created by 张赛 on 14-9-20.
//  Copyright (c) 2014年 sai.zhang. All rights reserved.
//
//#import <ShareSDK/ShareSDK.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));

    }
}


