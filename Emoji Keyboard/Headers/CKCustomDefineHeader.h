//
//  CKCustomDefineHeader.h
//  Emoji Keyboard
//
//  Created by 张赛 on 15/7/14.
//  Copyright (c) 2015年 sai.zhang. All rights reserved.
//

#ifndef Emoji_Keyboard_CKCustomDefineHeader_h
#define Emoji_Keyboard_CKCustomDefineHeader_h

/**
 *  这个header文件里存放了一些数据变量对应的宏定义,比如某个控件的高度,数据比例等
 *  在Consetting中会Import这个Header,然后Consetting.h又再pch中被引用,
 *  故定义在这里的变量是全空间可用的
 *  但是在定义的时候需要写好注释,保证后期修改和查询的方便
 *
 */


#define _MainScreenFrame   [[UIScreen mainScreen] bounds]
//设备屏幕宽
#define _MainScreen_Width  _MainScreenFrame.size.width
//设备屏幕高 20,表示状态栏高度.如3.5inch 的高,得到的__MainScreenFrame.size.height是480,而去掉电量那条状态栏,我们真正用到的是460;
#define _MainScreen_Height (_MainScreenFrame.size.height)
#define Color(r,g,b,a)  [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define ksaveThemeName  @"ksaveThemeName"
#define kThemeNotificationChange @"kThemeNotificationChange"

#define iPhone4 ([UIScreen mainScreen].bounds.size.height == 480.0)
#define iPhone5 ([UIScreen mainScreen].bounds.size.height == 568.0)
#define iPhone6 ([UIScreen mainScreen].bounds.size.height == 667.0)
#define iPhone6s ([UIScreen mainScreen].bounds.size.height == 736.0)
#define iPad [[UIDevice currentDevice].model isEqualToString: @"iPad"]

#define iPhone4Land (self.frame.size.width == 480.0)
#define iPhone5Land (self.frame.size.width == 568.0)
#define iPhone6Land (self.frame.size.width == 667.0)
#define iPhone6sLand (self.frame.size.width == 736.0)

#define iPhone4Portrait (self.frame.size.width == 320.0)
#define iPhone5Portrait (self.frame.size.width == 320.0)
#define iPhone6Portrait (self.frame.size.width == 375.0)
#define iPhone6sPortrait (self.frame.size.width == 414.0)

// AppGroup的baseURL
#define CKAPP_GROUP_BASE_URL [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:APP_GROUPS]
// 本地主题的数目
#define LOCAL_COUNT 9
/**
 *  KeyboardViewController 中后面要联想,如果输入这些符号,要直接输入纠错内容
 */
#define EndingSymbol @".!?"

#define FLOAT_EPSINON 0.0000001


/*****************************************我是分割线**********************************************/
/**
 *  埋点统计对应的字符串
 */

#define CK_APP_CATEGORY          @"CK_APP_CATEGORY"
#define CK_KEYBOARD_CATEGORY     @"CK_KEYBOARD_CATEGORY"


#define CK_UM_SEND_MESSAGE_TO_CONTANING_APP_NOTIFICATION @"CK_UM_SEND_MESSAGE_TO_CONTANING_APP"

#define Main_Launch @"Main_Launch"

#define Main_SelectedSetting @"Main_SelectedSetting" // 缺少

#define Main_TableSelected @"Main_TableSelected"

#define Main_ThemeType_Selected @"Main_ThemeType_Selected"

#define Main_MemeType_Selected @"Main_MemeType_Selected"

#define Main_SelectedTheme @"Main_SelectedTheme"                                             // 选中主题
#define Main_DownLoadOnlineTheme @"Main_DownLoadOnlineTheme"                                 // 下载在线主题
#define Main_LocalMemeClicked @"Main_LocalMemeClicked"                                       // 本地Meme的点击次数
#define Main_CustomMemeNegativeClicked @"Main_CustomMemeNegativeClicked"                     // 自定义Meme底片的选中次数
#define Main_DownloadFailed @"Main_DownloadFailed"                                            // 下载失败次数(用户点击Cancel)
#define Main_OnlineMemeClicked @"Main_OnlineMemeClicked"                                      // 在线Meme下载到本地后总的点击次数
#define Main_OnlineMemeDownload @"Main_OnlineMemeDownload"                                    // 在线Meme的下载次数
#define Main_AllMemeClicked @"Main_AllMemeClicked"    // 缺少                                          // 所有的本地页面meme点击总次数
#define Main_CustomMemeAddButtonClicked @"Main_CustomMemeAddButtonClicked"                     // 自定义Meme加号按钮的点击次数
#define Main_CustomMemeSaveShare @"Main_CustomMemeSaveShare"                                   // 自定义Meme保存分享次数
#define Main_SoundVoice @"Main_SoundVoice"            //键盘音voice点击次数
#define Main_SoundMusic @"Main_SoundMusic"            //键盘音music点击次数


#define UM_Event_ID @"UM_KB"
#define UM_Event_Key @"event_key"
#define UM_Event_Value @"event_value"

// 下面是键盘事件统计,都加上了KB
#define KB_Lanch @"KB_Lanch"                                                         // 键盘弹起次数
#define KB_CoolFont_Click @"KB_CoolFont_Click"                                       // coolFont按钮点击次数
#define KB_Emoji_Click @"KB_Emoji_Click"                                             // emoji按钮点击次数
#define KB_Meme_Click @"KB_Meme_Click"                                               // 键盘上Meme的点击次数
#define KB_Gif_Click @"KB_Gif_Click"                                     // 自定义emoji按钮点击次数
#define KB_Shit_Click @"KB_Shit_Click"                                                // 💩按钮点击次数
#define KB_Sticker_Click @"KB_Sticker_Click"                                           // Sticker按钮点击次数
#define KB_Theme_Click @"KB_Theme_Click"                                               // 键盘主题点击次数
#define KB_Emoji_Category_Click @"KB_Emoji_Category_Click"
#define KB_Shit_Category_Click @"KB_Shit_Category_Click"
#define KB_Sticker_Category_Click @"KB_Sticker_Category_Click"
#define KB_Gif_Category_Click @"KB_Gif_Category_Click"

#define KB_Table_Click @"KB_Table_Click"
#define KB_Emoji_Class_Click @"KB_Emoji_Class_Click"
#define KB_Category_Click_With_Format @"KB_%@_Category_Click"
#define KB_Click_With_Format @"KB_%@_Click"
#define KB_Jump_To_MainApp_Click @"KB_Jump_To_MainApp_Click"

#define KB_Event_Array @"event_array"

#define KB_Char_Click @"KB_Char_Click"
#define KB_Del_Click @"KB_Del_Click"
#define KB_LongDel_Click @"KB_LongDel_Click"


#define KB_SwitchKeyboard_Click @"KB_SwitchKeyboard_Click"
#define KB_SwitchKeyboard_Emoji_Click @"KB_SwitchKeyboard_Emoji_Click"
#define KB_SwitchKeyboard_EmojiKeyboard_Click @"KB_SwitchKeyboard_EmojiKeyboard_Click"
#define KB_SwitchKeyboard_MemeKeyboard_Click @"KB_SwitchKeyboard_MemeKeyboard_Click"

#define KB_Keyboard_Use_App @"KB_Keyboard_Use_App"
#define KB_Emoji_Use_App @"KB_Emoji_Use_App"
#define KB_Sticker_Use_App @"KB_Sticker_Use_App"
#define KB_Gif_Use_App @"KB_Gif_Use_App"
#define KB_Meme_Use_App @"KB_Meme_Use_App"

#define KB_Predition_Clicked @"KB_Predition_Clicked"

//coolFont分享引导

#define KB_CoolFont_Lock_Rate_Clicked @"KB_CoolFont_Lock_Rate_Clicked"
#define KB_CoolFont_Lock_Share_Clicked @"KB_CoolFont_Lock_Share_Clicked"
#define KB_CoolFont_Share_Clicked @"KB_CoolFont_Share_Clicked"
#define KB_CoolFont_Font_Rate_Clicked @"KB_CoolFont_Font_Rate_Clicked"
#define KB_CoolFont_Cancel_Clicked @"KB_CoolFont_Cancel_Clicked"
#define KB_CoolFont_KeyBoard_Rate_Clicked @"KB_CoolFont_KeyBoard_Rate_Clicked"
#define KB_CoolFont_KeyBoard_Rate_Cancel_Clicked @"KB_CoolFont_KeyBoard_Rate_Cancel_Clicked"

//App中分享
#define KB_APP_Use_Them_Clicked @"KB_APP_Use_Them_Clicked"
#define KB_APP_Use_Them_Cancel_Clicked @"KB_APP_Use_Them_Cancel_Clicked"
#define KB_APP_Download_Meme_Clicked @"KB_APP_Download_Meme_Clicked"
#define KB_APP_Download_Meme_Cancel_Clicked @"KB_APP_Download_Meme_Cancel_Clicked"

//选择meme，emoji未使用

#define KB_Selected_Emoji_Not_Input @"KB_Selected_Emoji_Not_Input"
#define KB_Selected_Meme_Not_Input @"KB_Selected_Emoji_Not_Input"

/*****************************************我是分割线**********************************************/

/**
 *  远程推送
 */

#define UM_PUSH_NOTE_TYPE @"NotificationType"
#define UM_PUSH_THEME @"UM_PUSH_THEME"
#define UM_PUSH_MEME @"UM_PUSH_MEME"
/*****************************************我是分割线**********************************************/

#define Predict_MAX_Count 40
/*****************************************我是分割线**********************************************/
                                                                                        /* 对应plist颜色*/
#define KEYBOARD_PORTRAIT_HEIGHT 216
#define KEYBOARD_LANDSCAPE_HEIGHT 162

#define STR_BEFORE self.textDocumentProxy.documentContextBeforeInput
#define STR_AFTER self.textDocumentProxy.documentContextAfterInput
#define INVALID_STR_BEFORE (@"\U0000FFFC")
// 这个命名的意义是text_manger
#define TXT_M self.textDocumentProxy
#define WORDMANGER self.wordManger
#define UIMANGER self.UIManger
#define HEADSTR self.currentInputStr
#define USER_PREFERENCES_MGR self.userPreferenceMgr


// 是否允许完全访问
typedef NS_ENUM(int, CKUerAlowFullAccess) {
    CKUerAlowFullAccessNO = 0,   // 不允许完全访问
    CKUerAlowFullAccessYES,      // 允许完全访问
};

// 当前手机型号
typedef NS_ENUM(int, CKCurrentiPhoneModel) {
    CKCurrentiPhoneModeliPhone4 = 0,
    CKCurrentiPhoneModeliPhone5,
    CKCurrentiPhoneModeliPhone6,
    CKCurrentiPhoneModeliPhone6Plus,
    CKCurrentiPhoneModelOther
};

#define PHONE_MODEL_IPHONE4 @"iPhone4"
#define PHONE_MODEL_IPHONE5 @"iPhone5"
#define PHONE_MODEL_IPHONE6 @"iPhone6"
#define PHONE_MODEL_IPHONE6PLUS @"iPhone6Plus"
#define PHONE_SCREEN_DIRECTION_LANDSCAPE @"landscape"
#define PHONE_SCREEN_DIRECTION_PORTRAIT @"portrait"


typedef NS_ENUM(int, InitYesObjectType) {
    userPrediction = 0, // 预测
    userAutoCorrect,  // 纠错
};


typedef NS_ENUM(int, CKSoundSType) {
    CKSoundSTypeVoice = 0,   // 音效
    CKSoundSTypeMusic,      // 音乐
    CKSoundSTypeNone
};



#define KNumAndSymbols @[ @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"0", @"-", @"/", @":", @";", @"(", @")", @"¥", @"&", @"@", @"\"", @".", @",", @"?", @"!", @"'",@"[", @"]", @"{", @"}", @"#", @"%", @"^", @"*", @"+", @"=",@"_", @"\\", @"|", @"~", @"<", @">", @"$", @"€", @"£", @"•"]

#define SPECIAL_STR @"-/:;$&@\",.? !()€£¥₩₽¢[]{}#^*+=_\\|~<>¿¡’`…0123456789"
#define SHOULD_CAPS @".?!\n"
#define SENTENCE_END_CHARS @".,?!:;"

#define buttonFontSize 22
#define popupFontSize 30
#define buttonDefaultFontName @"SF UI Text Light"


// 主App TableView的常用背景色
#define COMMON_VIEW_BACK_COLOR [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1]
#define Last_Save_Theme_Date @"lastSaveDate"

#define MemojiScale [UIScreen mainScreen].scale
#define LOCAL_MEME_COUNT 20


// 键盘给主App发送通知
#define MemojiKey_Launch_ContainApp @"MemojiKey_Launch_ContainApp"
#define LaunchURL @"LaunchURL"

/**
 *  我们要统计一项用户行为:
 *  用户点击了Theme,Fonts,Meme,Setting,Emoji按钮,但是什么都没干就出来了,下面这几个宏定义是用来发通知的
 */

#define KB_UserPressButDoNothing @"KB_UserPressButDoNothing"  // 这个是发送的通知名称

#define UserPressedOtherInterfaceButtonButDidNothingAndOutNotification  @"UserPressedOtherInterfaceButtonButDidNothingAndOutNotification"
#define UserHaveDoneSomethingNotification  @"UserHaveDoneSomethingNotification"
#define UserClickBottomBackButtonNotification @"UserClickBottomBackButtonNotification"


#define USER_TYPE @"DetermineUsersType"
#define NEW_USER @"NewUser"
#define OLD_USER @"OldUser"
#define FIRST_LAUNCHED @"FisrtLaunched"
#define RED_DOT_CLICKED @"UserClickedRedDot"
#define LOCAL_USER_TYPE @"UserTypeSaveInExtension"
#define SELECT_INPUT @"SELECT_INPUT"
//定义coolFont加锁范围
#define LOCK_START_INDEX 20

//评价的状态
#define EVALUATE_STATUS @"EvaluatedStatus"

//锁的状态
#define LOCK_STATUS @"LockStatus"

//app中是否评价过
#define EVALUATE_STATUS_APP @"EvaluatedStatusInApp"

//保存评价状态到AppGroup
#define EVALUATE_STATUS_GROUP @"EvaluatedStatusInGroup"
//AppStore的链接地址
#define AppStore_URL @"http://itunes.apple.com/app/id"

#endif
