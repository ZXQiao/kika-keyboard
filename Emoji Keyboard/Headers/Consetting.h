//
//  Consetting.h
//  testAutoLayout
//
//  Created by yuxinrong on 14-6-24.
//  Copyright (c) 2014年 com.xinmei365.autolayout. All rights reserved.
//
//设备屏幕大小

#ifdef __OBJC__

#define  ScreenHeigth  [[UIScreen mainScreen]bounds].size.height
#define  ScreenWidth  [[UIScreen mainScreen]bounds].size.width
#import "VariantsList.h"
#import "AppVariants.h"
#import "UIImage+Tools.h"
#import "Colours.h"
#import "CKCustomDefineHeader.h"
#import "NSString+EKStuff.h"
#import "CKSaveTools.h"
#import "CKSaveTools+CoolFontUse.h"
#import "CKCommonTools.h"
#import "CKBackend.h"
#endif