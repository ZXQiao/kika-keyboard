#ifndef AppVariants_h
#define AppVariants_h

#define APP_VARIANT_NAME APP_VARIANT_MEMOJI
#define APP_NAME (@"Kika Keyboard")
// MemojiKey 84c1ff3e9278 9Keyboard b033f42a6fac New_Emoji_Key 84c1a95e5a2a 
#define SHARE_SDK_APP_KEY @"84c1a95e5a2a"
#define APP_ROOT_URL (@"NewEmojiKey://com.xinmei365.NewEmojiKey")
#define APP_GROUPS  (@"group.com.xinmei365.NewEmojiKey")
#define APP_IDENTIFIER (@"com.xinmei365.NewEmojiKey.keyboard")
#define APP_ITUNES_CONNECTION (@"https://itunes.apple.com/us/app/id1035199024?mt=8")
#define APPID (@"1035199024")
//                         1390517874603487
#define FACEBOOK_APP_ID (@"502444569920824")
#define FACEBOOK_SECRET_ID (@"b23b0071aa98fcc0c4b5e14c9890e586")
#define UMAppKey (@"55dfc053e0f55a6fe800105a")
#define BACKEND_AGENT_KEY ("4a5ae1b93f21afaf199a6b3140e05097")
/***************AppName***********Consumer Key****************Consumer_Secret*********************/
//             MemojiKey      eFSRTEogJIYr21gl6losPgeNT  Ui2rJ75bNaJEyTfvEJrYDL0erEkW0NbYzCtfgiNM9jWmHk5rSe
//             9Keyboard      pzAQEBrz7w7dKYDJlrPvFVtNY  D7zIxJI4xuZ1aqqd8Cyswd7mY8jxxh865MbbJSvLL1dVbZsVtA
//             New_Emoji      KClTsOEu4z9IBhx4aGuKwzIdU  BfZnyEf81J3Sr2XYcZUcrrYgFGMLFlzCO1qeTOFnf6UZTTXHaJ
#define Twitter_Consumer_Key @"KClTsOEu4z9IBhx4aGuKwzIdU"
#define Twitter_Consumer_Secret @"BfZnyEf81J3Sr2XYcZUcrrYgFGMLFlzCO1qeTOFnf6UZTTXHaJ"

// 默认主题相关
#define Default_IOS_Origin_Theme @"pure"
#define Default_Theme_Index 0
#define Default_Theme_Name @"pure" // 默认主题名字,便于修改
#define Default_Predict_Normal_Color @"#242424" // 默认主题的predictionbar文字的正常态颜色   对应:font_normal_color
#define Default_Predict_Highted_Color @"#007bed" // 默认主题的predictionbar文字的高亮态颜色  对应:font_highlight_color
#define Default_Meme_Bottom_Normal_Color @"#ffffff" // MemeBottom文字的正常态  对应:emoji_icon_normal
#define Default_Meme_Bottom_Highted_Color @"#ffffff" // 高亮态                对应:emoji_icon_normal
#define Default_Font_Color @"#000000" // 默认按键字母颜色                       对应:FontColor
#define Default_Setting_Normal_Color @"#4a4e56" // Setting页面正常态颜色       对应:emoji_icon_normal
#define Default_Setting_Selected_Color @"#3c96f1" // Setting页面选中态颜色      对应:emoji_icon_highlight
#define Default_Setting_Font_Color @"#000000"  // Setting页面文字Label的颜色    对应:setting_fontcolor
#define Default_CoolFont_Color @"#000000" // CoolFont页面文字的颜色      对应:coolfont_color
#define Default_Emoji_Normal_Color @"#4a4e56" // Emoji iCon的正常态颜色 对应:emoji_icon_normal
#define Default_Emoji_Highed_Color @"#ffffff" // Emoji iCon的高亮态颜色 对应:emoji_icon_highlight
#define Default_FontName @"Gill Sans Regular" // 默认字体名称                对应:fontName

#endif
